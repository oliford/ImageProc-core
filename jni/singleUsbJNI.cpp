// JNI gateway to SingleUSB V2 SDK

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "imageProc_sources_capture_oceanOptics_SingleUSB.h"

/*
 * Class:     imageProc_sources_capture_oceanOptics_SingleUSB
 * Method:    setBusPortSelection
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_imageProc_sources_capture_oceanOptics_SingleUSB_setBusPortSelection
  (JNIEnv *env, jclass cls, jint bus, jstring jStrPort){

	char str[64];

	if(bus >= 0){
		snprintf(str, sizeof(str), "%d", bus);
		setenv("LIBUSB_SINGLE_BUS", str, -1);
	}else{
		unsetenv("LIBUSB_SINGLE_BUS");
	}

	if(jStrPort != NULL){
		char *cStrPort = (char *)env->GetStringUTFChars(jStrPort, NULL);

		if(cStrPort != NULL && strlen(cStrPort) > 0){
			printf("Set LIBUSB_SINGLE_PORT = %s", cStrPort);
			setenv("LIBUSB_SINGLE_PORT", cStrPort, -1);
		}else{
			unsetenv("LIBUSB_SINGLE_PORT");
		}
		env->ReleaseStringUTFChars(jStrPort, cStrPort);
	}else{
		unsetenv("LIBUSB_SINGLE_PORT");
	}

}


