package imageProc.sources.capture.flir;

import java.lang.reflect.Array;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;

import algorithmrepository.exceptions.NotImplementedException;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ByteBufferImage;
import imageProc.core.PolarizationImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.flir.swt.FLIRCamSWTControl;
import mds.GMDSFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.gmds.GMDSSignal;

/**
 * Image source for FLIR Cameras. Contains the application and image handling
 * side. The image capture and device handling is done by FLIRCamCapture
 * 
 * @author oliford
 */
public class FLIRCamSource extends CaptureSource {
	private PolarizationImage images[];

	private int nCaptured;

	private int lastCaptured;

	/** The low-level capturer, we should only have one of these */
	private FLIRCamCapture capture;

	/**
	 * The current config, should be kept always in sync with camera (if online) and
	 * with controller
	 */
	private FLIRCamConfig config;

	private boolean quadView = false;

	public FLIRCamSource(FLIRCamCapture capture, FLIRCamConfig config) {
		this.capture = capture;
		this.config = (config != null) ? config : new FLIRCamConfig();
	}

	public FLIRCamSource() {
		capture = new FLIRCamCapture(this);
		config = new FLIRCamConfig();
	}

	@Override
	public int getNumImages() {
		return images == null ? 0 : images.length;
	}

	@Override
	public Img getImage(int imgIdx) {
		if (images != null && imgIdx >= 0 && imgIdx < images.length)
			return images[imgIdx];
		else
			return null;
	}

	@Override
	public Img[] getImageSet() {
		return images;
	}

	/** Called by capture. Don't take long over this! */
	public void newImageArray(PolarizationImage images[]) {
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	/** Called by capture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if (imageIndex >= nCaptured)
			nCaptured = imageIndex + 1;
		lastCaptured = imageIndex;

		updateAllControllers();
	}

	@Override
	public FLIRCamSource clone() {
		return new FLIRCamSource(capture, config);
	}

	public void openCamera() {
		if (capture.isOpen()) {
			System.err.println("openCamera(): Camera already open");
			return;
		}
		capture.setConfig(config);
		capture.initCamera();
	}

	public void startCapture() {
		if (capture.isBusy()) {
			System.err.println("startCapture(): Camera thread busy");
			return;
		}

		this.nCaptured = 0;

		capture.setConfig(config);
		capture.startCapture(config, images);
	}

	public void syncConfig() {
		if (capture.isBusy()) {
			System.err.println("syncConfig(): Camera thread busy");
			return;
		}

		capture.setConfig(config);
		this.nCaptured = 0;
		capture.testConfig(config, images);
	}

	public void abort() {
		capture.abort(false);
	}

	public void closeCamera() {
		capture.closeCamera(false);
	}

	public String getSourceStatus() {
		return getNumImages() + " allocated " + "= " + capture.getAllocatedMemory() / 1000000 + " MB. " + nCaptured
				+ " captured " + "(" + lastCaptured + " last). " + "S/W start "
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}

	public String getCaptureStatus() {
		return capture.getStatusString();
	}

	public String getCameraInfo() {
		return "FLIR " + ((config.cameraModelName != null) ? config.cameraModelName : "???") + ": "
				+ ((config.imageWidthMax != 0) ? config.imageWidthMax : "???") + " x "
				+ ((config.imageHeightMax != 0) ? config.imageHeightMax : "???");
	}

	public boolean isActive() {
		return capture.isOpen();
	}

	public boolean isBusy() {
		return capture.isBusy();
	}

	@Override
	public boolean isIdle() {
		return capture == null || !capture.isOpen() || !capture.isBusy();
	}

	public FLIRCamConfig getConfig() { return config; }
	public void setConfig(CaptureConfig config) { 
		this.config = (FLIRCamConfig)config; 
		updateAllControllers(); 
	}

	public void configChanged() {
		updateAllControllers();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if (interfacingClass == Composite.class) {
			controller = new FLIRCamSWTControl((Composite) args[0], (Integer) args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	public void releaseMemory() {
		capture.abort(true);

		if (images != null) {
			for (int i = 0; i < images.length; i++) {
				if (images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;

		notifyImageSetChanged();
		updateAllControllers();
	}

	@Override
	public Status getAcquisitionStatus() {
		return capture.getAcquisitionStatus();
	}

	@Override
	public String getAcquisitionStatusString() {
		return capture.getStatusString();
	}

	@Override
	public int getNumAcquiredImages() {
		return nCaptured;
	}

	@Override
	public void close() {
		abort();
		closeCamera();
	}

	@Override
	public boolean open(long timeoutMS) {

		openCamera();
		long t0 = System.currentTimeMillis();
		while (!capture.isOpen()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}

			if ((System.currentTimeMillis() - t0) > timeoutMS) {
				throw new RuntimeException("Timed out waiting for camera to open. Need a longer warm-up time??");
			}
		}

		// do a config sync as-is
		syncConfig();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}

		return true;
	}

	public void setDiskMemoryLimit(long maxMemory) {
		capture.setDiskMemoryLimit(maxMemory);
	}

	public long getDiskMemoryLimit() {
		return capture.getDiskMemoryLimit();
	}

	public boolean getQuadView() {
		return quadView;
	}

	public void setQuadView(boolean quadView) {
		// Set the source's quadView setting (so it can be communicated to the capture)
		this.quadView = quadView;

		// Loop through all images (if they're available) and set quadView, then update
		// GUI
		if (images != null) {
			for (int i = 0; i < images.length; i++) {
				if (images[i] != null) {
					// Set quad view
					images[i].setReturnQuadView(quadView);

					// Tell all GUI elements to update, so they use the new quad view image.
					// This is best done with the somewhat-poorly-named imageRangesDone function,
					// which really just loops through all the connected image sinks and tells
					// them to update. The image range is not actually recalculated.
					 // weird name, this actually updates all the GUI elements
					
					// ...well, yes and no... The update is specifically to be called (only by Img actually)
					//   once the range calculation is complete, since many things can only work on the new 
					//   image once the min/max is known. This shouldn't be done like this, although I'm not really
					//  sure I can tell you how it IS supposed to be done right now :/
					
					imageRangesDone(i);
				}
			}
		}
	}

	@Override
	public String toShortString() {
		return "FLIRCam";
	}

	@Override
	protected final Capture getCapture() {
		return capture;
	}

	@Override
	public boolean setConfigParameter(String param, String value) {
		if (param.equals("runLength")) {
			double runLengthSecs = Algorithms.mustParseDouble(value);
			config.nImagesToAllocate = (int) (runLengthSecs * 1000.0 / config.frameTimeMS() + 0.5);
			return true;
		}

		return false;
	}

	@Override
	public String getConfigParameter(String param) {
		if (param.equals("runLength")) {
			return String.format("%f", config.nImagesToAllocate * config.frameTimeMS() / 1000.0);

		} else if (param.equals("exposureTime")) {
			return String.format("%f", config.exposureTimeMS() / 1000.0);

		} else if (param.equals("frameTime")) {
			return String.format("%f", config.frameTimeMS() / 1000.0);

		}

		return null;
	}

	@Override
	public int getFrameCount() {
		if (config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			for (int i=0; i < config.multiConfigAutoconf.length; i++) {
				if (config.multiConfigAutoconf[i])
					return config.multiConfigNumImgs[i];
			}
			throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		} else {
			return config.nImagesToAllocate;
		}
	}

	@Override
	public void setFrameCount(int frameCount) {
		if (!config.enableAutoConfig)
			throw new IllegalArgumentException("Autoconfig disabled");

		if (config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			//multi config, find which one (if any) can be autoconfiged
			for (int i=0; i < config.multiConfigAutoconf.length; i++) {
				if (config.multiConfigAutoconf[i]) {
					config.multiConfigNumImgs[i] = frameCount;
					config.nImagesToAllocate = config.totalMultiConfigImages();
					updateAllControllers();
					return;
				}
			}
			throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		} else {
			config.nImagesToAllocate = frameCount;
		}
		updateAllControllers();
	}

	@Override
	public long getFramePeriod() {
		if (config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			for (int i=0; i < config.multiConfigAutoconf.length; i++) {
				if (config.multiConfigAutoconf[i])
					return (long)(1000000 / config.multiConfigFramerate[i]);  // Frame period in us, framerate in fps
			}
			throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		} else {
			return (long)(1000000 / config.framerate);  // Frame period in us, framerate in fps
		}
	}

	@Override
	public void setFramePeriod(long framePeriodUS) {
		if (!config.enableAutoConfig)
			throw new IllegalArgumentException("Autoconfig disabled");

		if (config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			//multi config, find which one (if any) can be autoconfiged
			for (int i=0; i < config.multiConfigAutoconf.length; i++) {
				if (config.multiConfigAutoconf[i]) {
					config.multiConfigFramerate[i] = 1000000 / (float)framePeriodUS;
					updateAllControllers();
					return;
				}
			}
			throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		} else {
			config.framerate = 1000000 / (float)framePeriodUS;
		}
		updateAllControllers();
	}

	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig; }
}
