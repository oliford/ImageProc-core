package imageProc.sources.capture.flir.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.ImgSource;
import imageProc.sources.capture.flir.FLIRCamConfig;
import imageProc.sources.capture.flir.FLIRCamSource;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);

	private FLIRCamSource source;
	private Composite swtGroup;

	private Button continuousButton;

	private Combo timestampModeCombo;
	private Combo triggerModeCombo;

	private Text exposureTimeTextbox;
	private Button exposureMinButton;
	private Button exposureMaxButton;

	private Text frameRateTextbox;
	private Button frameRateMinButton;
	private Button frameRateMaxButton;

	private Button autoGainButton;
	private Text gainTextbox;

	private Spinner roiOffsetXText, roiOffsetYText, roiWidthText, roiHeightText;
	private Button roiMaxButton;

	public SimpleSettingsPanel(Composite parent, int style, FLIRCamSource source) {
		this.source = source;

		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));

		Label lA = new Label(swtGroup, SWT.NONE);
		lA.setText("Acquisition settings");
		lA.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));

		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		Label exposureTimeLabel = new Label(swtGroup, SWT.NONE);
		exposureTimeLabel.setText("Exposure (µs):");
		exposureTimeTextbox = new Text(swtGroup, SWT.NONE);
		exposureTimeTextbox.setText("10000");
		exposureTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		exposureTimeTextbox.addListener(SWT.FocusOut, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		exposureMinButton = new Button(swtGroup, SWT.PUSH);
		exposureMinButton.setText("Min");
		exposureMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		exposureMinButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				exposureMinMaxEvent(false);
			}
		});
		exposureMinButton.setEnabled(false); // not yet implemented

		exposureMaxButton = new Button(swtGroup, SWT.PUSH);
		exposureMaxButton.setText("Max");
		exposureMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		exposureMaxButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				exposureMinMaxEvent(true);
			}
		});
		exposureMaxButton.setEnabled(false); // not yet implemented

		Label frameRateLabel = new Label(swtGroup, SWT.NONE);
		frameRateLabel.setText("Framerate (Hz):");
		frameRateTextbox = new Text(swtGroup, SWT.NONE);
		frameRateTextbox.setText("10");
		frameRateTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		frameRateTextbox.addListener(SWT.FocusOut, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		frameRateMinButton = new Button(swtGroup, SWT.PUSH);
		frameRateMinButton.setText("Min");
		frameRateMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		frameRateMinButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				exposureMinMaxEvent(false);
			}
		});
		frameRateMinButton.setEnabled(false); // not yet implemented

		frameRateMaxButton = new Button(swtGroup, SWT.PUSH);
		frameRateMaxButton.setText("Max");
		frameRateMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		frameRateMaxButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				exposureMinMaxEvent(true);
			}
		});
		frameRateMaxButton.setEnabled(false); // not yet implemented

		autoGainButton = new Button(swtGroup, SWT.CHECK);
		autoGainButton.setText("Automatic gain control");
		autoGainButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		autoGainButton.setToolTipText("If enabled, gain will by varied from image to image to maximize dynamic range. "
			+ "If disabled, gain is fixed to a manually specified value.");
		autoGainButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		Label gainLabel = new Label(swtGroup, SWT.NONE);
		gainLabel.setText("Gain (dB):");
		gainTextbox = new Text(swtGroup, SWT.NONE);
		gainTextbox.setText("0");
		gainTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		gainTextbox.setToolTipText("Fixed gain to use when automatic gain control is disabled. "
			+ "To convert the gain units from dB to electrons/ADU, compute 10^(gain_dB / 20) and multiply by the system gain, "
			+ "which is given in the camera datasheet. For the FLIR BFS-U3-51S5P, the system gain is 1/0.18 = 5.56 ADU/electron.");
		gainTextbox.addListener(SWT.FocusOut, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		Label lR = new Label(swtGroup, SWT.NONE);
		lR.setText("ROI settings");
		lR.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));

		Label lRX = new Label(swtGroup, SWT.NONE);
		lRX.setText("Offset X:");
		roiOffsetXText = new Spinner(swtGroup, SWT.NONE);
		roiOffsetXText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiOffsetXText.setValues(0, 0, 9999, 0, 1, 1);
		roiOffsetXText.setToolTipText("Horizontal offset from the origin to the ROI (in pixels).");
		roiOffsetXText.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		Label lRY = new Label(swtGroup, SWT.NONE);
		lRY.setText("Offset Y:");
		roiOffsetYText = new Spinner(swtGroup, SWT.NONE);
		roiOffsetYText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiOffsetYText.setValues(0, 0, 9999, 0, 1, 1);
		roiOffsetYText.setToolTipText("Vertical offset from the origin to the ROI (in pixels).");
		roiOffsetYText.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		roiMaxButton = new Button(swtGroup, SWT.PUSH);
		roiMaxButton.setText("Max");
		roiMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		roiMaxButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				roiMaxEvent();
			}
		});
		roiMaxButton.setEnabled(false); // not yet implemented

		Label lRW = new Label(swtGroup, SWT.NONE);
		lRW.setText("Width:");
		roiWidthText = new Spinner(swtGroup, SWT.NONE);
		roiWidthText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiWidthText.setTextLimit(4);
		roiWidthText.setValues(0, 0, 9999, 0, 1, 1);
		roiWidthText.setToolTipText("Width of the ROI (in pixels).");
		roiWidthText.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		Label lRH = new Label(swtGroup, SWT.NONE);
		lRH.setText("Height:");
		roiHeightText = new Spinner(swtGroup, SWT.NONE);
		roiHeightText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiHeightText.setTextLimit(4);
		roiHeightText.setValues(0, 0, 9999, 0, 1, 1);
		roiHeightText.setToolTipText("Height of the ROI (in pixels).");
		roiHeightText.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});

		// Empty space to get GUI to line up
		Label lBL2 = new Label(swtGroup, SWT.NONE);
		lBL2.setText("");

		Label lT = new Label(swtGroup, SWT.NONE);
		lT.setText("Timing settings");
		lT.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));

		Label lTS = new Label(swtGroup, SWT.NONE);
		lTS.setText("Timestamp:");
		timestampModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		timestampModeCombo.setItems(FLIRCamConfig.timestampModes);
		timestampModeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		timestampModeCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});
		timestampModeCombo.setEnabled(false); // not yet implemented

		Label lHWT = new Label(swtGroup, SWT.NONE);
		lHWT.setText("H/W Trigger:");
		triggerModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		triggerModeCombo.setItems(FLIRCamConfig.triggerModes);
		triggerModeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		triggerModeCombo.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				guiToConfig();
			}
		});
		triggerModeCombo.setEnabled(false); // not yet implemented

		configToGUI();
	}

	void configToGUI() {
		FLIRCamConfig config = source.getConfig();

		continuousButton.setSelection(config.wrap);

		exposureTimeTextbox.setEnabled(!config.multiConfigEnable);
		exposureTimeTextbox.setText(Double.toString(config.exposureTimeMS() * 1e3));
		frameRateTextbox.setEnabled(!config.multiConfigEnable);
		frameRateTextbox.setText(String.format("%.3f", config.framerate));

		autoGainButton.setSelection(config.autoGainEnable());
		gainTextbox.setEnabled(!config.autoGainEnable());
		gainTextbox.setText(String.format("%.2f", config.gain));

		roiOffsetXText.setSelection(config.roiOffsetX);
		roiOffsetYText.setSelection(config.roiOffsetY);
		roiWidthText.setSelection(config.roiWidth);
		roiHeightText.setSelection(config.roiHeight);

		timestampModeCombo.select(config.timestampMode);
		triggerModeCombo.select(config.triggerMode);
	}

	void guiToConfig() {
		FLIRCamConfig config = source.getConfig();

		config.wrap = continuousButton.getSelection();

		config.exposureTime = (float) Algorithms.mustParseDouble(exposureTimeTextbox.getText());
		config.exposureTimeUnits = FLIRCamConfig.TIMEBASE_MICROSECS;
		config.framerate = (float) Algorithms.mustParseDouble(frameRateTextbox.getText());

		config.setAutoGain(autoGainButton.getSelection());
		gainTextbox.setEnabled(!autoGainButton.getSelection());
		config.gain = (float) Algorithms.mustParseDouble(gainTextbox.getText());

		config.roiOffsetX = (short) (roiOffsetXText.getSelection());
		config.roiOffsetY = (short) (roiOffsetYText.getSelection());
		config.roiWidth = (short) (roiWidthText.getSelection());
		config.roiHeight = (short) (roiHeightText.getSelection());

		config.timestampMode = timestampModeCombo.getSelectionIndex();
		config.triggerMode = (short) triggerModeCombo.getSelectionIndex();
	}

	public void frameTimeMinMaxEvent(boolean max) {
		FLIRCamConfig config = source.getConfig();

		if (true)
			throw new NotImplementedException();
		/*
		 * if(max){ config.setFeatureToMin("FrameRate"); }else{
		 * 
		 * config.setFeatureToMax("FrameRate"); }
		 */
		configToGUI();
	}

	public void exposureMinMaxEvent(boolean max) {
		FLIRCamConfig config = source.getConfig();

		if (true)
			throw new NotImplementedException();
		/*
		 * if(max){ config.setFeatureToMax("ExposureTime"); }else{
		 * 
		 * config.setFeatureToMin("ExposureTime"); }
		 */
		configToGUI();
	}

	public void roiMaxEvent() {
		FLIRCamConfig config = source.getConfig();

		config.roiOffsetX = -1;
		config.roiOffsetY = -1;
		config.roiWidth = -1;
		config.roiHeight = -1;
		roiOffsetXText.setSelection(-1);
		roiOffsetYText.setSelection(-1);
		roiWidthText.setSelection(-1);
		roiHeightText.setSelection(-1);
	}

	public ImgSource getSource() {
		return source;
	}

	public Control getSWTGroup() {
		return swtGroup;
	}
}
