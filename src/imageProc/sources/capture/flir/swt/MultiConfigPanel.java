package imageProc.sources.capture.flir.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.ImgSource;
import imageProc.sources.capture.flir.FLIRCamConfig;
import imageProc.sources.capture.flir.FLIRCamSource;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class MultiConfigPanel {
	public static final double log2 = FastMath.log(2);

	private FLIRCamSource source;
	private Composite swtGroup;

	private Button multiConfigEnableCheckbox;
	private Spinner mcNumImagesSpinner[];
	private Text mcExposureTimeTextbox[];
	private Text mcFramerateTextbox[];
	private Text mcWaitTimeTextbox[];
	private Button mcAutoConfigMaster;
	private Button mcAutoConfigRadio[];

	public MultiConfigPanel(Composite parent, int style, FLIRCamSource source) {
		this.source = source;

		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));

		multiConfigEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		multiConfigEnableCheckbox.setSelection(false); // default to unchecked
		multiConfigEnableCheckbox.setText("Enable");
		multiConfigEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		multiConfigEnableCheckbox.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				multiConfigEvent(event);
			}
		});

		Label lMCa = new Label(swtGroup, SWT.NONE);
		lMCa.setText("No.");
		Label lMCb = new Label(swtGroup, SWT.NONE);
		lMCb.setText("Num images");
		Label lMCc = new Label(swtGroup, SWT.NONE);
		lMCc.setText("Exposure (µs)");
		Label lMCd = new Label(swtGroup, SWT.NONE);
		lMCd.setText("Framerate (Hz)");
		Label lMCe = new Label(swtGroup, SWT.NONE);
		lMCe.setText("Wait time (ms)");
		lMCe.setToolTipText("Time to wait after this multi-config entry is complete before starting next entry");

		mcAutoConfigMaster = new Button(swtGroup, SWT.CHECK);
		mcAutoConfigMaster.setText("Autoconfig");
		mcAutoConfigMaster.setToolTipText("If set allows one of number of images entries to be autoconfigured from other modules (e.g. Pilots)");
		mcAutoConfigMaster.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		mcAutoConfigMaster.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });

		int numMultiCfg = 10;
		mcNumImagesSpinner = new Spinner[numMultiCfg];
		mcExposureTimeTextbox = new Text[numMultiCfg];
		mcFramerateTextbox = new Text[numMultiCfg];
		mcWaitTimeTextbox = new Text[numMultiCfg];
		mcAutoConfigRadio = new Button[numMultiCfg];

		for (int i = 0; i < numMultiCfg; i++) {

			Label lMCn = new Label(swtGroup, SWT.NONE);
			lMCn.setText(i + ":");

			mcNumImagesSpinner[i] = new Spinner(swtGroup, SWT.NONE);
			mcNumImagesSpinner[i].setValues(0, 0, 100000, 0, 1, 10);
			mcNumImagesSpinner[i].setToolTipText("Number of images to capture before moving to next multi-config entry");
			mcNumImagesSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcNumImagesSpinner[i].addListener(SWT.Selection, new Listener() {
				@Override
				public void handleEvent(Event event) {
					multiConfigEvent(event);
				}
			});
			mcNumImagesSpinner[i].setEnabled(false);

			mcExposureTimeTextbox[i] = new Text(swtGroup, SWT.NONE);
			mcExposureTimeTextbox[i].setText("0");
			mcExposureTimeTextbox[i].setToolTipText("Exposure time for each image in this set");
			mcExposureTimeTextbox[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			mcExposureTimeTextbox[i].addListener(SWT.FocusOut, new Listener() {
				@Override
				public void handleEvent(Event event) {
					guiToConfig();
				}
			});
			mcExposureTimeTextbox[i].setEnabled(false);
			
			mcFramerateTextbox[i] = new Text(swtGroup, SWT.NONE);
			mcFramerateTextbox[i].setText("0");
			mcFramerateTextbox[i].setToolTipText("Frame rate of acquisition during this entry");
			mcFramerateTextbox[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			mcFramerateTextbox[i].addListener(SWT.FocusOut, new Listener() {
				@Override
				public void handleEvent(Event event) {
					guiToConfig();
				}
			});
			mcFramerateTextbox[i].setEnabled(false);
			
			mcWaitTimeTextbox[i] = new Text(swtGroup, SWT.NONE);
			mcWaitTimeTextbox[i].setText("0");
			mcWaitTimeTextbox[i].setToolTipText("Time to wait after this multi-config entry is complete before starting next entry");
			mcWaitTimeTextbox[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			mcWaitTimeTextbox[i].addListener(SWT.FocusOut, new Listener() {
				@Override
				public void handleEvent(Event event) {
					guiToConfig();
				}
			});
			mcWaitTimeTextbox[i].setEnabled(false);

			mcAutoConfigRadio[i] = new Button(swtGroup, SWT.RADIO);
			mcAutoConfigRadio[i].setText(Integer.toString(i));
			mcAutoConfigRadio[i].setSelection(false);
			mcAutoConfigRadio[i].setToolTipText("Should the number of images in this block be modified by the autoconfiguration");
			mcAutoConfigRadio[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcAutoConfigRadio[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			mcAutoConfigRadio[i].addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { autoconfClickEvent(event); } });
			mcAutoConfigRadio[i].setEnabled(false);
		}

		configToGUI();
	}

	protected void autoconfClickEvent(Event event) {
		//Helper: if the user tries to click a specific autoconf but the master is off, turn it on and set them
		if(!mcAutoConfigMaster.getSelection()) {
			mcAutoConfigMaster.setSelection(true);
			((Button)event.item).setSelection(true);

			guiToConfig();
		}
	}

	void configToGUI() {
		FLIRCamConfig config = source.getConfig();
		
		multiConfigEnableCheckbox.setSelection(config.multiConfigEnable);
		for (int i = 0; i < mcNumImagesSpinner.length; i++) {
			mcNumImagesSpinner[i].setEnabled(config.multiConfigEnable);
			mcExposureTimeTextbox[i].setEnabled(config.multiConfigEnable);
			mcFramerateTextbox[i].setEnabled(config.multiConfigEnable);
			mcWaitTimeTextbox[i].setEnabled(config.multiConfigEnable);
			mcAutoConfigRadio[i].setEnabled(config.multiConfigEnable);
		}

		boolean anyAutoconf = false;
		if (config.multiConfigAutoconf == null)
			config.multiConfigAutoconf = new boolean[mcNumImagesSpinner.length];
		
		if (config.multiConfigNumImgs != null) {
			for (int i = 0; i < mcNumImagesSpinner.length; i++) {
				mcNumImagesSpinner[i].setSelection(config.multiConfigNumImgs[i]);
				mcExposureTimeTextbox[i].setText(String.format("%.0f", config.multiConfigExposureTime[i]));
				mcFramerateTextbox[i].setText(String.format("%.3f", config.multiConfigFramerate[i]));
				mcWaitTimeTextbox[i].setText(String.format("%d", config.multiConfigWaitTime[i]));
				mcAutoConfigRadio[i].setSelection(config.multiConfigAutoconf[i]);
				anyAutoconf |= config.multiConfigAutoconf[i];
			}
			mcAutoConfigMaster.setSelection(anyAutoconf);
		} else {
			for (int i = 0; i < mcNumImagesSpinner.length; i++) {
				mcNumImagesSpinner[i].setSelection(0);
				mcExposureTimeTextbox[i].setText("0");
				mcFramerateTextbox[i].setText("0");
				mcWaitTimeTextbox[i].setText("0");
				mcAutoConfigRadio[i].setSelection(false);
			}
			mcAutoConfigMaster.setSelection(false);
		}
	}

	void guiToConfig() {
		FLIRCamConfig config = source.getConfig();

		config.multiConfigEnable = multiConfigEnableCheckbox.getSelection();
		if (config.multiConfigEnable) {
			config.multiConfigNumImgs = new int[mcNumImagesSpinner.length];
			config.multiConfigExposureTime = new float[mcNumImagesSpinner.length];
			config.multiConfigFramerate = new float[mcNumImagesSpinner.length];
			config.multiConfigWaitTime = new long[mcNumImagesSpinner.length];
			config.multiConfigAutoconf = new boolean[mcNumImagesSpinner.length];

			boolean anyAutoconf = false;
			for (int i = 0; i < mcNumImagesSpinner.length; i++) {
				config.multiConfigNumImgs[i] = mcNumImagesSpinner[i].getSelection();
				config.multiConfigExposureTime[i] = (float) Algorithms.mustParseDouble(mcExposureTimeTextbox[i].getText());
				config.multiConfigFramerate[i] = (float) Algorithms.mustParseDouble(mcFramerateTextbox[i].getText());
				config.multiConfigWaitTime[i] = Algorithms.mustParseLong(mcWaitTimeTextbox[i].getText());
				config.multiConfigAutoconf[i] = mcAutoConfigMaster.getSelection() && mcAutoConfigRadio[i].getSelection();
				anyAutoconf |= config.multiConfigAutoconf[i];
			}
			config.nImagesToAllocate = config.totalMultiConfigImages();

			// If the master autoconf checkbox is enabled but none is set, set the first one
			if(mcAutoConfigMaster.getSelection() && !anyAutoconf) {
				config.multiConfigAutoconf[0] = true;
				mcAutoConfigRadio[0].setSelection(true);
			}
		}
		source.updateAllControllers(); // update the main GUI panel
	}

	private void multiConfigEvent(Event event) {
		boolean isMultiConfig = multiConfigEnableCheckbox.getSelection();

		for (int i = 0; i < mcNumImagesSpinner.length; i++) {
			mcNumImagesSpinner[i].setEnabled(isMultiConfig);
			mcExposureTimeTextbox[i].setEnabled(isMultiConfig);
			mcFramerateTextbox[i].setEnabled(isMultiConfig);
			mcWaitTimeTextbox[i].setEnabled(isMultiConfig);
			mcAutoConfigRadio[i].setEnabled(isMultiConfig);
		}

		guiToConfig();
	}

	public ImgSource getSource() {
		return source;
	}

	public Control getSWTGroup() {
		return swtGroup;
	}
}
