package imageProc.sources.capture.flir;

import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;

public class FLIRCamConfig extends CaptureConfig {

	/**
	 * If uninitialised, the camera values will be not set but retrieved on the
	 * first sync
	 */
	public boolean initialised = false;

	/** Number of images to allocate */
	public int nImagesToAllocate = 100;

	/**
	 * Continuous mode (start again from image 0 (or 1) when nImagesToAllocate is
	 * reached)
	 */
	public boolean wrap = false;

	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;

	/** Model name returned by FLIR SDK */
	public String cameraModelName;

	/** Serial number returned by FLIR SDK */
	public String cameraSerialNumber;

	/** Timeout in milliseconds for GetNextImage calls */
	public int cameraTimeoutMS = 100;

	public float sensorTemperature;

	/** Timestamp modes: 0 = timestamps from camera, 1 = timestamps recorded by TTE */
	public int timestampMode = 0;
	public final static String timestampModes[] = {"Camera", "TTE"};

	public static final int TIMEBASE_NANOSECS = 0;
	public static final int TIMEBASE_MICROSECS = 1;
	public static final int TIMEBASE_MILLISECS = 2;

	/** Unit of exposure time (0=ns, 1=us, 2=ms) */
	public int exposureTimeUnits = 1;

	public float exposureTime = 10000;

	public String autoGain = "Off";
	public float gain = 0;

	/** Acquisition frame rate in frames per second */
	public float framerate = 10;

	/**
	 * Hardware Trigger mode. 0 = Auto 1 = Software force 2 = External exposure
	 * start 3 = External exposure control
	 * 
	 * See FLIRCam.GetTriggerMode
	 * 
	 * /// Trigger modes: /// - 0 = [auto trigger] /// An exposure of a new image is
	 * started automatically best possible compared to the /// readout of an image.
	 * If a CCD is used, and images are taken in a sequence, then exposures /// and
	 * sensor readout are started simultaneously. Signals at the trigger input
	 * (\<exp trig\>) are irrelevant.
	 * 
	 * /// - 1 = [software trigger]: /// An exposure can only be started by a force
	 * trigger command.
	 * 
	 * /// - 2 = [extern exposure & software trigger]: /// /// A delay / exposure
	 * sequence is started at the RISING or FALLING edge (depending on /// the DIP
	 * switch setting) of the trigger input (\<exp trig\>). /// /// - 3 = [extern
	 * exposure control]: /// The exposure time is defined by the pulse length at
	 * the trigger input(\<exp trig\>). The /// delay and exposure time values
	 * defined by the set/request delay and exposure command /// are ineffective.
	 * (Exposure time length control is also possible for double image mode; the ///
	 * exposure time of the second image is given by the readout time of the first
	 * image.) ///
	 */
	public short triggerMode = 0;
	public static String triggerModes[] = { "Auto", "Software Start", "External frame trigger",
			"External exposure control" };

	/** Size of each image in bytes */
	public int payloadSize;

	/** Total size in bits of a pixel of the image */
	public int pixelSize;

	/** Maximum image width provided by the camera (in pixels) */
	public int imageWidthMax;

	/** Maximum image height provided by the camera (in pixels) */
	public int imageHeightMax;

	/** Image width provided by the camera (in pixels) */
	public int imageWidth;

	/** Image height provided by the camera (in pixels) */
	public int imageHeight;

	/** ROI of image on camera sensor. */
	public int roiOffsetX;
	public int roiOffsetY;
	public int roiWidth;
	public int roiHeight;

	// simple multiconfig for exposure time and framerate, same 'timebase' (units) as main ones
	public boolean multiConfigEnable = false;
	public int multiConfigNumImgs[];
	public float multiConfigExposureTime[];
	public float multiConfigFramerate[];
	public long multiConfigWaitTime[];
	public boolean multiConfigAutoconf[];

	/** Returns total frame time ('COC Time') in milliseconds */
	public double frameTimeMS() {
		return 1000 / framerate;
	}

	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/blackLevelFirstFrame", blackLevelFirstFrame);
		map.put(prefix + "/wrap", wrap);
		map.put(prefix + "/serialNumber", cameraSerialNumber);
		map.put(prefix + "/cameraTimeout", cameraTimeoutMS);
		map.put(prefix + "/cameraModelName", cameraModelName);
		map.put(prefix + "/sensorTemperature", sensorTemperature);
		map.put(prefix + "/timestampMode", timestampMode);
		map.put(prefix + "/timestampModesName", timestampModes[timestampMode]);
		map.put(prefix + "/exp_timebase", exposureTimeUnits);
		map.put(prefix + "/exp_time", exposureTime);
		map.put(prefix + "/autoGainMode", autoGain);
		map.put(prefix + "/gain_dB", gain);
		map.put(prefix + "/triggerMode", triggerMode);
		map.put(prefix + "/triggerModeName", triggerModes[triggerMode]);
		map.put(prefix + "/imageWidth", imageWidth);
		map.put(prefix + "/imageHeight", imageHeight);
		map.put(prefix + "/roiOffsetX", roiOffsetX);
		map.put(prefix + "/roiOffsetY", roiOffsetY);
		map.put(prefix + "/roiWidth", roiWidth);
		map.put(prefix + "/roiHeight", roiHeight);
		map.put(prefix + "/frameTimeMS", frameTimeMS());
		map.put(prefix + "/multiConfigEnable", multiConfigEnable);
		if (multiConfigEnable) {
			for (int i = 0; i < multiConfigNumImgs.length; i++) {
				map.put(prefix + "/multiConfig_" + i + "/numImgs", multiConfigNumImgs[i]);
				map.put(prefix + "/multiConfig_" + i + "/exposureTimeUS", multiConfigExposureTime[i]);
				map.put(prefix + "/multiConfig_" + i + "/frameTimeMS", 1000 / multiConfigFramerate[i]);
				map.put(prefix + "/multiConfig_" + i + "/waitTimeMS", multiConfigWaitTime[i]);
			}
		}
		return map;
	}

	public double exposureTimeMS() {
		switch (exposureTimeUnits) {
		case TIMEBASE_NANOSECS:
			return exposureTime / 1e6;
		case TIMEBASE_MICROSECS:
			return exposureTime / 1e3;
		case TIMEBASE_MILLISECS:
			return exposureTime;
		default:
			throw new IllegalArgumentException("Unknown timebase " + exposureTimeUnits);
		}
	}

	public boolean autoGainEnable() {
		switch (autoGain) {
		case "Continuous":
			return true;
		case "Once":  // This setting is not currently implemented in GUI, but I included it here for future-proofing
			return true;
		case "Off":
			return false;
		default:
			throw new IllegalArgumentException("Unknown autogain setting '" + exposureTimeUnits + "'");
		}
	}

	public void setAutoGain(boolean autoGainEnable) {
		// Note: the "Once" setting is not implemented
		if (autoGainEnable) {
			autoGain = "Continuous";
		}
		else {
			autoGain = "Off";
		}
	}

	public int totalMultiConfigImages() {
		int totalImages = 0;
		for (int i = 0; i < multiConfigNumImgs.length; i++) {
			totalImages += multiConfigNumImgs[i];
		}
		return totalImages;
	}
}
