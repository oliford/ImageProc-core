package imageProc.sources.capture.flir;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import imageProc.core.AcquisitionDevice;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.PolarizationImage;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import flirJNI.FLIRCam;
import flirJNI.FLIRCamSDKException;

/** Capture thread for FLIR SDK Cameras */

public class FLIRCamCapture extends Capture implements Runnable {
	/** Interface to the FLIR Spinnaker SDK */
	private FLIRCam camera;

	private FLIRCamSource source;
	private FLIRCamConfig config;

	private PolarizationImage images[];

	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;

	/** Camera is open and thread is running */
	private boolean cameraOpen = false;

	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;

	/** Current multi-config entry; -1 means multi-config is disabled */
	private int mcCurrentEntry = -1;
	
	/** Image index at which to switch to next multi-config entry; -1 means multi-config is disabled */
	private int mcNextEntryAtImageIdx = -1;
	
	/** Total number of images from previous multi-config entries; 0 if multi-config is disabled */
	private int mcTotalImagesFromPreviousEntries = 0;

	public FLIRCamCapture(FLIRCamSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		// By default never use disk memory for camera
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE);
	}

	@Override
	public void run() {
		threadBusy = true;
		try {
			if (camera != null) {
				deinit();
				throw new RuntimeException("Camera driver already open, aborting");
			}

			setStatus(Status.init, "Capture init");
			initDevice();

			// signalConfigSync = false;
			signalCapture = false;

			setStatus(Status.init, "Init Ready");

			while (!signalDeath) {
				try {
					// Wait until capture is started, config sync is requested,
					// or signal dies (in case of signal death, exit)
					threadBusy = false;
					while (!signalCapture && !signalConfigSync) {
						try {
							Thread.sleep(10); // sleep 10 ms
						} catch (InterruptedException err) {
						}

						if (signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;

					// Sync config and allocate images. This is executed when
					// the "Get config" or "Capture" buttons are pressed.
					//
					// I don't understand why this if statement is here,
					// isn't this statement always true since signalConfigSync
					// or signalCapture had to be true to complete the above
					// loop?
					if (signalConfigSync || signalCapture) {
						signalConfigSync = false;
						syncConfig();
						setStatus(Status.init, "Allocating");
						initImages();
					}
					setStatus(Status.init, "Config + Allocation done");

					// Capture images. This code is executed when the "Capture"
					// button is pressed.
					if (signalCapture) {
						signalCapture = false;
						source.addNonTimeSeriesMetaDataMap(config.toMap("FLIRCam/config"));
						doCapture();
						setStatus(Status.completeOK, "Capture done");
					}

				} catch (CaptureAbortedException err) {
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal", err);
					setStatus(Status.aborted, "Aborted");
					// and just go around the loop
				} catch (RuntimeException err) {
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
				} finally {
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");

		} catch (Throwable err) {
			logr.log(Level.SEVERE, "FLIRCam thread caught exception: ", err);
			setStatus(Status.errored, "Aborted/Errored");

		} finally {
			try {
				deinit();
			} catch (Throwable err) {
				logr.info("FLIRCam thread caught exception while de-initing camera: ");
				err.printStackTrace();
				setStatus(Status.errored, "Aborted/Errored");
			}
		}
	}

	private void syncConfig() {

		// Set up the multi-config internal state
		if (config.multiConfigEnable) {
			mcCurrentEntry = 0;
			mcNextEntryAtImageIdx = config.multiConfigNumImgs[0];
		} else {
			mcCurrentEntry = -1;
			mcNextEntryAtImageIdx = -1;
		}
		mcTotalImagesFromPreviousEntries = 0;

		// Set camera parameters if config is initialised.
		// Config is not initialised upon first opening camera, but is initialised thereafter
		if (config.initialised) {

			// Set the region of interest.
			// TODO: Figure out why it gives an error when this order is reversed
			camera.SetWidth(config.roiWidth);
			camera.SetHeight(config.roiHeight);
			camera.SetROIOffsetX(config.roiOffsetX);
			camera.SetROIOffsetY(config.roiOffsetY);

			// Set the gain
			camera.SetGainAuto(config.autoGain);
			if (!config.autoGainEnable()) {  // Can't set gain unless autogain is disabled
				camera.SetGain(config.gain);
			}

			// Set the exposure time and framerate. This depends on whether the multi-config is enabled
			if(config.multiConfigEnable) {
				int firstFilled = -1;
				for(int i=0; i < config.multiConfigNumImgs.length; i++) {
					if(config.multiConfigNumImgs[i] > 0){
						if(firstFilled < 0)
							firstFilled = i; // Get index of first valid multi-config entry

						// Try to set exposure time and framerate
						camera.SetExposureTime(config.multiConfigExposureTime[i]);
						camera.SetFramerate(config.multiConfigFramerate[i]);
						
						// Get the actual exposure time and framerate that camera will use for this multi-config entry
						config.multiConfigExposureTime[i] = camera.GetExposureTime();
						config.multiConfigFramerate[i] = camera.GetAcquisitionResultingFrameRate();
					}
				}
				// Go back to first valid multi-config settings
				camera.SetExposureTime(config.multiConfigExposureTime[firstFilled]);
				camera.SetFramerate(config.multiConfigFramerate[firstFilled]);
			}
			else { // Multi-config disabled, so use settings from Simple GUI tab
				camera.SetExposureTime(config.exposureTime);
				camera.SetFramerate(config.framerate);
			}
		}
		
		// Load the camera parameters, since the actual parameter values may differ
		// from the desired set values. This way, the config represents what is
		// actually in the camera.
		setStatus(Status.init, "Loading configuration");
		config.sensorTemperature = camera.GetDeviceTemperature();
		config.exposureTime = camera.GetExposureTime();
		config.framerate = camera.GetAcquisitionResultingFrameRate();
		config.autoGain = camera.GetGainAuto();
		config.gain = camera.GetGain();
		config.roiOffsetX = camera.GetROIOffsetX();
		config.roiOffsetY = camera.GetROIOffsetY();
		config.roiWidth = camera.GetWidth();
		config.roiHeight = camera.GetHeight();
		// Currently, the image width and ROI width are the same. This may change in the future
		// if binning or decimation are implemented.
		config.imageWidth = config.roiWidth;
		config.imageHeight = config.roiHeight;
		config.pixelSize = camera.GetPixelSize();
		config.payloadSize = camera.GetPayloadSize();

		// Finishing steps now that config has been retrieved
		config.initialised = true;
		source.configChanged();
	}

	private void initDevice() {
		// Start camera's SDK and connect to camera
		camera = new FLIRCam();
		try {
			camera.InitializeSystem();
			camera.OpenCamera();
			camera.InitializeCamera();
		} catch (FLIRCamSDKException err) {
			logr.info("FLIR camera could not be opened.");
			camera = null;
			throw err;
		}

		// Retrieve camera parameters that are fixed
		config.cameraModelName = camera.GetDeviceModelName();
		config.cameraSerialNumber = camera.GetDeviceSerialNumber();
		config.imageWidthMax = camera.GetWidthMax();
		config.imageHeightMax = camera.GetHeightMax();

		// Set camera parameters that won't be changed by user
		camera.SetPixelFormat("Mono16"); // needed so camera outputs 16 bit images
		camera.SetAcquisitionMode("Continuous"); // needed so ImageProc can control # images acquired
		camera.SetExposureMode("Timed"); // needed so exposure time can be set
		camera.SetExposureAuto("Off"); // needed so exposure time can be set

		cameraOpen = true;
	}

	private void initImages() {

		// Check to ensure that the memory requirements of the image (based on
		// its height, width, and pixel size) match what the camera says
		long imageSize = config.imageWidth * config.imageHeight * config.pixelSize / 8;
		if (config.payloadSize != imageSize) {
			throw new RuntimeException("Camera wants different amount of memory than expected by ImageProc???");
		}

		// Check to ensure that memory required doesn't exceed limit
		if (imageSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");

		// Allocate memory for the images
		try {
			// Create a "template image" to figure out how much memory each
			// of the actual images will need (it might not be just `width *
			// height * pixel size` if the camera has a header or footer)
			int headerSize = 0;
			int footerSize = 0;
			PolarizationImage templateImage = new PolarizationImage(null, -1, config.imageWidth, config.imageHeight,
					config.pixelSize, headerSize, footerSize, false, source.getQuadView());
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);

			// Allocate memory for the actual images (based on template image)
			if (bulkAlloc.reallocate(templateImage, config.nImagesToAllocate)) {
				logr.info(".initImages() Cleared and reallocated " + config.nImagesToAllocate + " images");
				// Need to manually type cast to PolarizationImage because otherwise the type
				// checking gets mad
				images = (PolarizationImage[]) bulkAlloc.getImages();
				source.newImageArray(images);
			} else {
				logr.info(".initImages() Reusing existing " + config.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}
		} catch (OutOfMemoryError err) {
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
	}

	private void doCapture() {
		setStatus(Status.awaitingSoftwareTrigger, "Capture started. Acquisition not started");
		try {
			// Set up time information for all the images
			double time[] = new double[config.nImagesToAllocate]; // seconds since first image
			long timestampNS[] = new long[config.nImagesToAllocate]; // nanosecond timestamp relative to when camera was turned on
			long wallCopyTimeMS[] = new long[config.nImagesToAllocate]; // timestamp according to PC clock
			int imageNumberStamp[] = new int[config.nImagesToAllocate]; // image number since beginning of acquisition
			long nanoTime[] = new long[config.nImagesToAllocate]; // full nanosecond timestamp relative to midnight 1970 UTC
			source.setSeriesMetaData("FLIRCam/timestampNS", timestampNS, true);
			source.setSeriesMetaData("FLIRCam/wallCopyTimeMS", wallCopyTimeMS, true);
			source.setSeriesMetaData("FLIRCam/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("FLIRCam/nanoTime", nanoTime, true);
			source.setSeriesMetaData("FLIRCam/time", time, true);
			long wallCopyTimeMS0 = Long.MIN_VALUE;
			long timestampNS0 = Long.MIN_VALUE;

			// Start image acquisition loop (one image acquired per iteration)
			int nextImgIdx = 0;
			do {
				try {
					// Lock the buffer for this image
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try {
						// Tell sinks they can now use this image
						images[nextImgIdx].invalidate(false);

						// Don't actually start until acquire signal is set
						if (!isAcqusitionStarted()) {
							// Wait until acquisition signal is set
							while (!signalAcquireStart) {
								try {
									Thread.sleep(0, 100000); // Wait 0.1 ms
								} catch (InterruptedException e) {
								}
							}

							// Start acquisition
							camera.BeginAcquisition();
							setStatus(Status.awaitingHardwareTrigger, "Capturing, awaiting hardware trigger");
						}

						// Get image into buffer
						do {
							try {
								// Create length-1 arrays to hold frame ID and timestamp
								int frameID[] = new int[1];
								long timestamp[] = new long[1];
								
								// Tell FLIR SDK to get next image from its System-managed buffer
								// and put it into the ImageProc-managed buffer for this image.
								// This also gets the frame ID and timestamp of this image.
								camera.GetNextImage(
										images[nextImgIdx].getWritableBuffer(writeLock),
										frameID,
										timestamp,
										config.cameraTimeoutMS);
							
								// Record frame ID and time information
								wallCopyTimeMS[nextImgIdx] = System.currentTimeMillis();
								imageNumberStamp[nextImgIdx] = frameID[0] + mcTotalImagesFromPreviousEntries;
								timestampNS[nextImgIdx] = timestamp[0];
								
								setStatus(Status.capturing, "Capturing, Acquisition started");

							} catch (FLIRCamSDKException err) {
								if (err.getErrorCode() == FLIRCamSDKException.FLIR_ERROR_TIMEOUT) {
									continue;
								}
								throw err;
							}

							if (signalDeath || signalAbort) {
								logr.info("Aborting capture loop.");
								throw new CaptureAbortedException();
							}

							// Now that image is acquired, break out of loop
							break;
						} while (true);

						// Save time information for first frame
						if (wallCopyTimeMS0 == Long.MIN_VALUE)
							wallCopyTimeMS0 = wallCopyTimeMS[nextImgIdx];
						
						if (timestampNS0 == Long.MIN_VALUE)
							timestampNS0 = timestampNS[nextImgIdx];
						
						// Calculate relative time since first image and nanosecond timestamp
						time[nextImgIdx] = ((double)(timestampNS[nextImgIdx] - timestampNS0) / 1e9);
						nanoTime[nextImgIdx] = (timestampNS[nextImgIdx] - timestampNS0 + wallCopyTimeMS0*1_000_000L);

					} finally {
						writeLock.unlock();
					}
				} catch (InterruptedException e) {
					logr.info("Interrupted while waiting to write to java image.");
					return;
				}

				// Alert the rest of ImageProc that an image was captured
				source.imageCaptured(nextImgIdx);
				nextImgIdx++;

				// If continuous loop option is enabled, go back to beginning
				if (nextImgIdx >= config.nImagesToAllocate && config.wrap)
					nextImgIdx = config.blackLevelFirstFrame ? 1 : 0;

				// Determine whether next multiconfig group needs to be started
				if (config.multiConfigEnable && (nextImgIdx >= mcNextEntryAtImageIdx))
					nextMultiConfigEntry(nextImgIdx);

			} while (nextImgIdx < config.nImagesToAllocate); // End of image acquisition loop
		} finally {
			camera.EndAcquisition();
		}
	}

	private void nextMultiConfigEntry(int nextImgIdx) {

		camera.EndAcquisition(); // stop acquiring images
		try {
			Thread.sleep(config.multiConfigWaitTime[mcCurrentEntry]); // wait the user-desired time
		} catch (InterruptedException e) {
		}
		
		// Update the variables tracking the current multi-config entry and image index at which
		// to switch to next multi-config entry
		while(nextImgIdx >= mcNextEntryAtImageIdx){
			mcTotalImagesFromPreviousEntries += config.multiConfigNumImgs[mcCurrentEntry];
			mcCurrentEntry++;				
			if(mcCurrentEntry >= config.multiConfigNumImgs.length)
				mcCurrentEntry = 0; // wrap back to first multi-config
			mcNextEntryAtImageIdx += config.multiConfigNumImgs[mcCurrentEntry];				
		}

		// Set the new exposure time and framerate for this multi-confi entry
		camera.SetExposureTime(config.multiConfigExposureTime[mcCurrentEntry]);
		camera.SetFramerate(config.multiConfigFramerate[mcCurrentEntry]);

		camera.BeginAcquisition(); // start acquiring images again
	}

	private void deinit() {
		if (camera != null) {
			camera.DeinitializeCamera();
			camera.CloseCamera();
			camera.DeinitializeSystem();
			camera = null;
		}
	}

	/* -------------- Everything below contacts the outside ------------ */

	/** Start the camera thread and open the camera */
	public void initCamera() {
		if (isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		// thread.setPriority(Thread.MAX_PRIORITY);
		logr.info("Starting FLIR capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}

	/** Signal the camera thread to set/load the config */
	public void testConfig(FLIRCamConfig config, PolarizationImage images[]) {
		if (!isOpen())
			throw new RuntimeException("Camera not open");

		if (isBusy())
			throw new RuntimeException("Camera thread is busy");

		this.config = config;
		this.images = images;
		signalConfigSync = true;
	}

	/** Signal the camera thread to start the capture */
	public void startCapture(FLIRCamConfig config, PolarizationImage images[]) {
		if (!isOpen())
			throw new RuntimeException("Camera not open");

		if (isBusy())
			throw new RuntimeException("Camera thread is busy");

		this.config = config;
		this.images = images;
		signalCapture = true;
	}

	/** Signal the thread to abort the current sync/capture oepration */
	public void abort(boolean awaitStop) {
		signalAbort = true;

		// and kick the thread for good measure
		if (thread != null && thread.isAlive()) {
			thread.interrupt();

			if (awaitStop) {
				System.out.println("FLIRCamCapture: Waiting for thread to abort.");
				while (threadBusy) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				}
				System.out.println("FLIRCamCapture: Thread stopped.");
			}
		}
	}

	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath) {
		if (thread != null && thread.isAlive()) {
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if (awaitDeath) {
				System.out.println("FLIRCamCapture: Waiting for thread to die.");
				while (thread.isAlive()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				}
				System.out.println("FLIRCamCapture: Thread died.");
			}
		}
	}

	public void destroy() {
		closeCamera(false);
	}

	public boolean isOpen() {
		return thread != null && thread.isAlive() && cameraOpen;
	}

	public boolean isBusy() {
		return isOpen() && threadBusy;
	}

	@Override
	protected CaptureSource getSource() {
		return source;
	}

	public void setConfig(FLIRCamConfig config) {
		this.config = config;
	}

	public long getAllocatedMemory() {
		return bulkAlloc.getAllocatedMemory();
	}
}
