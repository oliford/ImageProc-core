package imageProc.sources.capture.base;

import java.util.Map;

/** Base class for configuration of capture devices */
public abstract class CaptureConfig {
	/** @deprecated For pre-JSON settings saving. Not implemented by default. */
	public Map<String, Object> toMap(String prefix){
		throw new RuntimeException("Not implemented");
	}
	
	public boolean beginCaptureOnStartEvent = true;
	
	public boolean abortEventOnCaptureComplete = false;
	
	/** Enable auto-configuration attempts from other modules 
	 *   (via AcquisitionDevice.setFrameCount/Period() */
	public boolean enableAutoConfig = true;
}
