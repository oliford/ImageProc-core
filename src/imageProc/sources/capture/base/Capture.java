package imageProc.sources.capture.base;

import java.util.logging.Logger;

import imageProc.core.AcquisitionDevice;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.EventReciever.Event;
import imageProc.core.AcquisitionDevice.Status;

/** Base class for Capture interface of acquisition devices.
 * These have:
 *  - A thread that runs and deal with the driver.
 *  - Status implementation for AcqusitionDevice (passed through from CaptureSource)
 *  - start/stop/abort etc
 *  - bulkAlloc object for memory allocation
 *  
 *  */
public abstract class Capture {

	protected Logger logr = Logger.getLogger(this.getClass().getName());
	
	protected AcquisitionDevice.Status status = Status.notInitied;
	protected String statusString = "Not inited\n-";
	
	protected BulkImageAllocation<ByteBufferImage> bulkAlloc;
	
	protected Thread thread;
	
	/** Ask the thread to start image allocation, queue buffers and spin in the capture loop*/
	protected boolean signalCapture = false;
	/** Ask the thread to trigger the software capture as
	 	soon as the images are queued */
	protected boolean signalAcquireStart = true;
	/** Ask the thread to abort the capture/config sync */
	protected boolean signalAbort = false;
	/** Ask the thread to abort, close the camera and stop */
	protected boolean signalDeath = false;
	
	/** Output debug info for the high speed debugging stuff */
	//protected final void spinLog(String str){ }
	protected final void spinLog(String str){
		logr.finest("[spin]: " + str);
	}//*/
	
	public final String getStatusString(){ return statusString; }
	
	public final Status getAcquisitionStatus(){ return status; }
	
	public final boolean isAcquireEnabled() {  return 	signalAcquireStart; }
	
	/** Has the acuqisition been started (also true if it has finished successfully) */ 
	protected final boolean isAcqusitionStarted(){ return status == Status.awaitingHardwareTrigger || status == Status.capturing || status == Status.completeOK; } 
	
	/** Signal the camera thread to start the capture */
	public final void enableAcquire(boolean enable) { signalAcquireStart = enable;	}
	
	public final void setDiskMemoryLimit(long maxMemory) { bulkAlloc.setMaxMemoryBufferAlloc(maxMemory); }
	
	public final long getDiskMemoryLimit(){  return bulkAlloc.getMaxMemoryBufferAlloc(); }

	public final void setStatus(Status status, String string){
		
		boolean isReady = (status ==  Status.capturing || status == Status.awaitingHardwareTrigger || status == Status.awaitingSoftwareTrigger);
		boolean wasReady = (this.status ==  Status.capturing || this.status == Status.awaitingHardwareTrigger || this.status == Status.awaitingSoftwareTrigger);
		if(!wasReady && isReady)
			getSource().broadcastEvent(Event.AcquisitionInit);
		
		if(this.status != Status.capturing && status == Status.capturing) 
			getSource().broadcastEvent(Event.AcquisitionStarted);
		
		if(this.status == Status.capturing && status != Status.capturing) 
			getSource().broadcastEvent(Event.AcquisitionStopped);		
		
		this.status = status;
		this.statusString = string;
		
		if(getSource() != null){
			getSource().statusChanged();
		}
	}
	
	protected abstract CaptureSource getSource();
	
	protected class CaptureAbortedException extends RuntimeException { 
		private static final long serialVersionUID = 1L;
		public CaptureAbortedException() { super("Capture aborted by signal"); 	}
	};
}
