package imageProc.sources.capture.base;

import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.TextCommandable;
import imageProc.core.EventReciever.Event;
import imageProc.sources.capture.flir.FLIRCamConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Base class of sources for acqusition devices */ 
public abstract class CaptureSource extends ImgSourceOrSinkImpl implements ImgSource, AcquisitionDevice, EventReciever, ConfigurableByID, TextCommandable {

	protected Logger logr = Logger.getLogger(this.getClass().getName());
	
	protected String lastLoadedConfig;
	
	protected abstract Capture getCapture();
	
	protected abstract CaptureConfig getConfig();
	protected abstract void setConfig(CaptureConfig config);
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	@Override
	public final void enableAcquire(boolean enable) { getCapture().enableAcquire(enable); }
	public final boolean isAcquireEnabled() { return getCapture().isAcquireEnabled(); }
	
	private String statusChangedSyncObj = new String("statusChangedSyncObj");
	public final void statusChanged() {
		ImageProcUtil.ensureFinalUpdate(statusChangedSyncObj, new Runnable() {
				@Override
				public void run() {
					updateAllControllers();
				}
			});
	}
	
	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalStart:
			if(getConfig().beginCaptureOnStartEvent)
				startCapture();
			break;
		case GlobalAbort: 
			abort(); 
			break;
		default:
			break;
		}		
	}
	
	public void captureComplete() {
		if(getConfig().abortEventOnCaptureComplete)
			broadcastEvent(Event.GlobalAbort);
	}

	/** Set response to start event and sending of abort event on complete
	 *  (old 'armTrigger() function)'
	 * @param globalStart Begin acquisition on GlobalSTart event
	 * @param trigAbort Broadcast GlobalAbort when acquisition is complete
	 */
	public void enableEventResponse(boolean globalStart, boolean trigAbort) {
		getConfig().beginCaptureOnStartEvent = globalStart;
		getConfig().abortEventOnCaptureComplete = trigAbort;
	}
	
	public abstract CaptureSource clone();
	

	@Override
	public void loadConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds")) { 
			//specifically for loading configurations from metadata saved in GMDS
			loadConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
		}else if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown/unsupported config type " + id + " for " + this.getClass());
		lastLoadedConfig = id;
	}
	
	public void loadConfigGMDS(String exp, int pulse) {
		throw new RuntimeException("GMDS config not implemented for " + this.getClass());
	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);

		Gson gson = new Gson();

		setConfig(gson.fromJson(jsonString, getConfig().getClass()));

		updateAllControllers();
	}
	
	@Override
	public void saveConfig(String id) {
		if (id.startsWith("gmds:")) {
			throw new RuntimeException("GMDS config saving only supported via metadata in image saving.");
			
		}else if (id.startsWith("jsonfile:")) {
				saveConfigJSON(id.substring(9));
				lastLoadedConfig = id;
		}else {
			throw new RuntimeException("Unknown/unsupported config type " + id + " for " + this.getClass());
		}	
	}

	public void saveConfigJSON(String fileName) {

		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.setPrettyPrinting()
				.create(); 
		String json = gson.toJson(getConfig());

		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public boolean setConfigParameter(String param, String value) {
		if(param.equalsIgnoreCase("FrameCount")){
			setFrameCount(Integer.parseInt(value));
			updateAllControllers();
			return true;
			
		}else if(param.equalsIgnoreCase("FrameTime") || param.equalsIgnoreCase("FramePeriod")){
			setFramePeriod((int)(Double.parseDouble(value) * 1.0e6));
			updateAllControllers();
			return true;
		}
		
		return false;
	}
	
	@Override
	public String getConfigParameter(String param) {
		if(param.equals("runLength")){
			return String.format("%f", getFrameCount() * getFramePeriod() / 1000000.0);
			
		}else if(param.equalsIgnoreCase("frameTime") || param.equalsIgnoreCase("framePeriod")){
			return String.format("%f", getFramePeriod() / 1000000.0); //not sure, but roughly
			
		}else if(param.equalsIgnoreCase("frameCount")){
			return Integer.toString(getFrameCount()); //not sure, but roughly
			
		}
		
		return null;
	}
	
	/** @param frame period in microseconds */
	public void setFramePeriod(long framePeriodUS) {
		throw new RuntimeException("Not implemented");
	}
	
	/** @return frame period in microseconds */
	public long getFramePeriod() {
		throw new RuntimeException("Not implemented");
	}
	
	public void setFrameCount(int frameCount) {
		throw new RuntimeException("Not implemented");
	}
	
	public int getFrameCount() {
		throw new RuntimeException("Not implemented");
	}

	public void setExposureTime(long expTimeUS) {
		throw new RuntimeException("Not implemented");
	}
	
	public long getExposureTime() {
		throw new RuntimeException("Not implemented");
	}
	
	@Override
	public boolean isAutoconfigAllowed() { return false; }
	
	@Override
	/** Text commands e.g. via PilotComm */
	public void command(String command, PrintWriter pwReturn) {
		try {
			
			//separate by ',' '=' or whitespace
			String parts[] = command.split("[\\s=,]");
			
			
			if(parts[0].equalsIgnoreCase("getConfig")) {
				Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.create(); 
				
				String json = gson.toJson(getConfig());

				pwReturn.println("ModuleCommand " + this.getClass().getSimpleName() + " config =" + json);
				
			}else if(parts[0].equalsIgnoreCase("getStatus")) {
				pwReturn.println("ModuleCommand " + this.getClass().getSimpleName() + " status = " + getAcquisitionStatus().toString() + "," + getAcquisitionStatusString());
				
			}else if(parts[0].equalsIgnoreCase("open")) {
				int timeoutMS = parts.length > 1 ? Algorithms.mustParseInt(parts[1]) : 10000;
				boolean openOK = open(timeoutMS);
				
				pwReturn.println("ModuleCommand " + this.getClass().getSimpleName() + " opened = " + openOK);
				
			}else if(parts[0].equalsIgnoreCase("close")) {
				close();
				
				pwReturn.println("ModuleCommand " + this.getClass().getSimpleName() + " closed");
				
			}

			//update GUI for any config changes
			updateAllControllers();
			
		}catch(RuntimeException err){
			logr.log(Level.WARNING, "Error processing module text command." + command, err);
			pwReturn.println("ERROR: " + err.toString());
		}
	}
}
