package imageProc.sources.capture.xclib;


import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;

public class XCLIBConfig extends CaptureConfig {

	/** TODO: List of timestamp modes for the GUI */
	public static String[] timestampModes = { "Off", "Text in image", "Binary in image"};

	/** TODO: List of trigger modes for the GUI */
	public static String[] triggerModes = { "Software", "Start", "Frame" };
	
	/** If uninitialised, the camera values will be not set but retrieved on the first sync */
	public boolean initialised = false;
	
	/** Number of images to allocate */
	public int nImagesToAllocate = 100;
	
	/** Continuous mode (start again from image 0 (or 1) when nImagesToAllocate is reached) */
	public boolean wrap = false;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;

	/** CameraID is the 'unit' number - frame grabber and unit within one multi-head frame grabber */
	public int cameraID = 0;

	/** XCLIB exposure time in ms */
	public int width = 640;

	/** XCLIB exposure time in ms */
	public int height = 480;

	/** XCLIB exposure time in ms */
	public int exposureTime = 50;

	/** XCLIB delay time in ms */
	public int delayTime = 50;

	public int colors;
	public int bitsPerPixel;
	public int frameBuffers;
	public long memorySize;

	/** microsecond period to poll if a new image has arrived */
	public long spinDelayMS = 1;

	/** Driver parameters (other than -DM x which is replaced/set with cameraID field)
	 * See PIXCI Docs */
	public String driverParams = "";

	public String formatFile = null;
	
	/** EXSYNC and PRIN register values set the exposure and frame times (see XCLIB manual). If -1 then not set. */
	public int prin = -1;
		
	/** EXSYNC and PRIN register values set the exposure and frame times (see XCLIB manual). If -1 then not set. */
	public int exsync = -1;
		
	@Override
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put(prefix + "/cameraID", cameraID);
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/blackLevelFirstFrame", blackLevelFirstFrame);
		map.put(prefix + "/wrap", wrap);
		map.put(prefix + "/exposureTime", exposureTime);
		map.put(prefix + "/delayTime", delayTime);

		//TODO: Add other entries that should be saved in metadata
		
		return map;
	}

}
