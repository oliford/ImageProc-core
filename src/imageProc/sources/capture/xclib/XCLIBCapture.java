package imageProc.sources.capture.xclib;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import otherSupport.RandomManager;
import xclibJNI.XCLIB;

/** Capture thread for XCLIB Framegrabbers (PIXCI etc)
 * 
 * I don't really understand how this capture sequencing is supposed to work.
 * The documents never really describe what a frame buffer is or how we can set them up.
 * So for now we just run the camera live into a single buffer and copy out images when we can.
 * This is horrible, but ok for observation cameras for non-scientific stuff
 * 
 * The frame/exposure time settings don't really work. Instead we save a settings file
 * with XCAP and then load it here.
 */

public class XCLIBCapture extends Capture implements Runnable {
	private XCLIBSource source;
	
	private ByteBufferImage images[];
	
	private XCLIBConfig cfg;
		
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;

	
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
			
	/** TODO: Driver access object */
	private XCLIB camera;

	/** XCLIB if a list of available camera should be got on first init attempt */
	public int nAvailableCameras = -1;

	public XCLIBCapture(XCLIBSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera 
	}
		
	@Override
	public void run() {
		threadBusy = true;
		try{
			if(camera != null){				
				deinit();
				throw new RuntimeException("Camera driver already open, aborting");
			}
			
			setStatus(Status.init, "Initialising camera...");
			initDevice();
			
			//signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Camera init done.");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();
					}
								
					
					if(signalCapture){
						signalCapture = false;

						setStatus(Status.init, "Allocating image memory...");		
						
						initImages();
									
						setStatus(Status.init, "Config done, starting capture...");
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("XCLIBCam/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");
					}else {
						
						setStatus(Status.completeOK, "Config done.");						
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal", err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("XCLIBCam thread caught Exception: " + err);
			setStatus(Status.errored, "ERROR: " + err.getMessage());
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				System.err.println("XCLIBCam thread caught exception de-initing camera.");
				err.printStackTrace();
				setStatus(Status.errored, "ERROR: " + err.getMessage());
			}
		}
	}
	
	private void initDevice() {

		
		String camIDParam = "-DM " + (1 << cfg.cameraID);
		if(cfg.driverParams == null) {
			cfg.driverParams = camIDParam;
		}else if(cfg.driverParams.contains("-DM")) {
			cfg.driverParams = cfg.driverParams.replaceAll("-DM [0-9]*", camIDParam); 
		}else {
			cfg.driverParams = cfg.driverParams + camIDParam;
		}
		
		String fmtFile = cfg.formatFile == null ? "" : cfg.formatFile;
			
		camera = new XCLIB(cfg.driverParams, "default", fmtFile, 0);
		
		nAvailableCameras = XCLIB.infoUnits(); //is actually the number open, not available
		
		cfg.width = camera.imageXdims();
		cfg.height = camera.imageYdims();
		cfg.colors = camera.imageCdims();
		cfg.bitsPerPixel = camera.imageCdims()*camera.imageBdims();
		cfg.frameBuffers = camera.imageZdims();
		cfg.memorySize = camera.infoMemsize();
		
		logr.log(Level.INFO, "bitsPerPixel = " + cfg.bitsPerPixel);
		logr.log(Level.INFO, "frameBuffers = " + cfg.frameBuffers);
		logr.log(Level.INFO, "memorySize = " + cfg.memorySize);

		//TODO: set any IDs or S/N etc: 
		//cfg.name = camera.GetName();
		//cfg.serialNumber = camera.GetSerialNumber();
		
		cameraOpen = true;
	}

	private void syncConfig() {
		
		if(cfg.exsync >= 0 && cfg.prin >= 0)
			camera.setExsyncPrin(cfg.exsync, cfg.prin);
		//TODO: Attempt to set everything from config and then re-get it
		
		//camera.SetParameterX(cfg.parameterX)
			
		//camera.applyConfig()
			
		//cfg.parameterX = camera.getParameterX()
		
		//these are in principle for the exposure/frame times, but dont work
		cfg.exsync = camera.getExsync();
		cfg.prin = camera.getPrin();
		
		cfg.initialised = true;
		
		source.configChanged();
	}
	
	
	private void initImages(){
		
		//TODO: Get image size information from camera or from config
		
		int width = cfg.width; // camera.GetImageWidth()
		int height = cfg.height;  // camera.GetImageHeight()
		int bitDepth = cfg.bitsPerPixel;
		int bytesPerPixel = (int)Math.ceil(bitDepth / 8.0);
		long imgDataSize = width*height*bytesPerPixel; // camera.GetImageSizeBytes()
		
		if(imgDataSize < height*width*bytesPerPixel){
			throw new RuntimeException("Camera wants less memory than it needs for the image???");
		}
			
		int footerSize = 0; // sometimes camera gives header/footer with metadata timestamps etc
		
		if(imgDataSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		//allocate the image memory in as big chunks as possible (4GB)
		try{
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
	
	}
	
	private void doCapture() {
		
		
		try{
			
			//Initialise running timestamp and time information arrays
			double time[] = new double[cfg.nImagesToAllocate]; //seconds since first image
			double walltime[] = new double[cfg.nImagesToAllocate]; //seconds since first image (from wall clock)
			long ticks[] = new long[cfg.nImagesToAllocate]; // full nanoSecs since midnight 1970 UTC
			long wallCopyTimeMS[] = new long[cfg.nImagesToAllocate];
			int imageNumberStamp[] = new int[cfg.nImagesToAllocate];
			long nanoTime[] = new long[cfg.nImagesToAllocate];
			
			long ticks0 = Long.MIN_VALUE;
			long ticksLast = Long.MIN_VALUE;
			int tickUnits[] = camera.infoSysTicksUnits();
			long tickUnitUS = ((long)tickUnits[0] / tickUnits[1]);
			
			System.out.println("tickUnit = " + tickUnitUS + " us");
			
			source.setSeriesMetaData("XCLIBCam/ticks", ticks, true);
			source.setSeriesMetaData("XCLIBCam/wallCopyTimeMS", wallCopyTimeMS, true);
			source.setSeriesMetaData("XCLIBCam/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("XCLIBCam/nanoTime", nanoTime, true);
			source.setSeriesMetaData("XCLIBCam/time", time, true);
			source.setSeriesMetaData("XCLIBCam/walltime", walltime, true);
			
			//TODO: Initialise internal camera transfer buffers, if it needs them
			//camera.SetBuffers(...)
						
			//TODO: Begin the camera capture, so long as it has a faster acqusition start/stop which is done below
			//camera.CheckConfigAndInitialiseCapturing()
			
			setStatus(Status.awaitingSoftwareTrigger, "Capturing, acquisition not started.");	
					
			long wallCopyTimeMS0 = Long.MIN_VALUE;
			long timestampNS0 = Long.MIN_VALUE;
			int nextImgIdx = 0;
			do{
				try {
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try{
						images[nextImgIdx].invalidate(false);
						do{
								
							if(!isAcqusitionStarted()){ //don't actually start until acquire signal is set
								while(!signalAcquireStart){
									Thread.sleep(0, 100000);
								}
								
								//get the tick of whatever image is still in the buffer, so we don't start with that one
								ticksLast = camera.capturedSysTicksLong(); 
								
								//start rolling
								
								if(camera.imageZdims() > 1) {
									camera.goLiveSeq(1, camera.imageZdims(), 1, cfg.wrap ? 0 : cfg.nImagesToAllocate, camera.imageIdims());
								}else {
									//it only seems to have 1 buffer. I don't know what that means, so just grab them as they come for now								
									camera.goLive(1);
									
								}
								spinLog("goLive");
								
								
								setStatus(Status.awaitingHardwareTrigger, "Capturing..., (awaiting hardware trigger)");
							
							}
							
							//if acqusition was stopped in the middle of a run, do so
							if(isAcqusitionStarted() && !signalAcquireStart) {
								//TODO: camera.SetRecording(false);
								setStatus(Status.awaitingSoftwareTrigger, "Capturing, but was stopped (intentionally).");
							}
							
							if(signalDeath || signalAbort){
								logr.info("Aborting capture loop.");
								throw new CaptureAbortedException();
							}
							
							try{			
								
								//'system ticks' in microseconds since... something
								long ticksNext = camera.capturedSysTicksLong();
								//spinLog("ticks1 = "+ticks + ", ticksLast=" + ticksLast);
								while(ticksNext == ticksLast) {
									ticksNext = camera.capturedSysTicksLong();
									Thread.sleep(cfg.spinDelayMS);
								}
								//spinLog("ticksNew = "+ticks);
								
								int lastBuf = camera.capturedBuffer();
								//that will be 1, because it always is
								//we don't know if an image really came
								spinLog("lastBuf = "+lastBuf);
								
								ticksLast = ticksNext;
								
								ByteBuffer buf = images[nextImgIdx].getWritableBuffer(writeLock);
								buf.order(ByteOrder.LITTLE_ENDIAN);
								ShortBuffer sbuf = buf.asShortBuffer();
								
								//copy the image
								//since there's only one buffer this could be overwritten in the middle...
								//maybe? I've no idea how this library works
								camera.readushort(lastBuf, 0, 0, -1, -1, sbuf, sbuf.capacity(), "Grey");
								
								//store the local wall time as backup timestamp
								wallCopyTimeMS[nextImgIdx] = System.currentTimeMillis();
								ticks[nextImgIdx] = ticksNext;
								spinLog("wall = " + wallCopyTimeMS[nextImgIdx]);
								
								//make sure the status reflects that an image has arrived
								setStatus(Status.capturing, "Capturing..., acquisition started");

							 }catch(RuntimeException err){
								if(err.toString().startsWith("Timeout"))
									continue;
								throw err;
							
							}
							break;
						}while(true); //timeout loop
						
						//spinLog(".doCapture(): Buffer copied to images[" + nextImgIdx + "]");
						
						if(wallCopyTimeMS0 == Long.MIN_VALUE) {
							wallCopyTimeMS0 = wallCopyTimeMS[nextImgIdx];
							ticks0 = ticks[nextImgIdx];
						}
						
						//TODO: Something like if(cfg.timestampsEnabled) {
						
						//use timestamp to make a quick 'time since first time' time 
						time[nextImgIdx] = (double)((ticks[nextImgIdx] - ticks0) * tickUnitUS) / 1_000_000L;
						
						//time[nextImgIdx] = (double)(timestampNS[nextImgIdx] - timestampNS0);
						walltime[nextImgIdx] = (double)((wallCopyTimeMS[nextImgIdx] - wallCopyTimeMS0)) / 1e3;
						nanoTime[nextImgIdx] = (ticks[nextImgIdx] - ticks0) * tickUnitUS * 1000L + wallCopyTimeMS0*1_000_000L;
					
						
					}finally{ writeLock.unlock(); }
				} catch (InterruptedException e) {
					logr.info("Interrupted while waiting to write to java image.");
					return;
				}
				
				source.imageCaptured(nextImgIdx);
				
				nextImgIdx++;
				if(nextImgIdx >= cfg.nImagesToAllocate && cfg.wrap)
					nextImgIdx = cfg.blackLevelFirstFrame ? 1 : 0;
				
				
			}while(nextImgIdx < cfg.nImagesToAllocate);
			
		}finally{
			spinLog("goAbortLive()");
			camera.goAbortLive();
		}

	}
	
	
	private void processTimestamp(int[] imageNumberStamp, long[] timestamp, int imageIdx){

		//TODO: Process image data and put into timestamps array
		timestamp[imageIdx] = 5; 
		
	}
	
	private void deinit() {
		//Close camera if its open
		if(camera != null){
			
			if(true) { 
				try {
					camera.goAbortLive();
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Error closing camera.", err);
				}
				
				try{
					XCLIB.close();
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Error closing camera.", err);
				}
			}
			//camera.destroy();
			//camera.CleanupLib();
			camera = null;
			
		}
	}
	
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		//thread.setPriority(Thread.MAX_PRIORITY);
		spinLog("Starting PCO capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(XCLIBConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(XCLIBConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thread to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
		
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				System.out.println("XCLIBCamCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("XCLIBCamCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("XCLIBCamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("XCLIBCamCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}
	
	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(XCLIBConfig config) { this.cfg = config;	}

	public int getNumAvailableCameras() { return nAvailableCameras ; }

}
