package imageProc.sources.capture.xclib.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.Algorithms;
import algorithmrepository.NotImplementedException;
import imageProc.core.ImgSource;
import imageProc.sources.capture.xclib.XCLIBConfig;
import imageProc.sources.capture.xclib.XCLIBSource;
import net.jafama.FastMath;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private XCLIBSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Text driverParamsText;
	private Text formatFileText;
	
	private Text widthTextbox;
	private Text heightTextbox;
	
	private Text exSyncTextbox;
	private Text prinTextbox;
	
	private Spinner roiX0Text, roiY0Text, roiX1Text, roiY1Text;
	private Button roiMaxButton;
		
	public SimpleSettingsPanel(Composite parent, int style, XCLIBSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		Label lDP = new Label(swtGroup, SWT.NONE); lDP.setText("Driver params:");
		driverParamsText = new Text(swtGroup, SWT.NONE);
		driverParamsText.setText("");
		driverParamsText.setToolTipText("Driver parameters according to the PIXCI docs (-DM is the cameraID selection and is set automatically)");
		driverParamsText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		driverParamsText.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lFF = new Label(swtGroup, SWT.NONE); lFF.setText("Fromat file:");
		formatFileText = new Text(swtGroup, SWT.NONE);
		formatFileText.setText("");
		formatFileText.setToolTipText("Format file written by XCAP program to set resolution etc of frame grabber.");
		formatFileText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		formatFileText.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
				
		Label lIS = new Label(swtGroup, SWT.NONE); lIS.setText("Image size:");
		
		Label lW = new Label(swtGroup, SWT.NONE); lW.setText("Width:");
		widthTextbox = new Text(swtGroup, SWT.NONE);
		widthTextbox.setText("?");
		widthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		widthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lH = new Label(swtGroup, SWT.NONE); lH.setText("Height:");		
		heightTextbox = new Text(swtGroup, SWT.NONE);
		heightTextbox.setText("?");
		heightTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		heightTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Timing:");
		
		Label lE = new Label(swtGroup, SWT.NONE); lE.setText("Exsync:");
		exSyncTextbox = new Text(swtGroup, SWT.NONE);
		exSyncTextbox.setText("?");
		exSyncTextbox.setToolTipText("Sets the EXSYNC register that controls the exposure/frame time. See XCLIB manual. If -1, the register is not set.");
		exSyncTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		exSyncTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
				
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Prin:");		
		prinTextbox = new Text(swtGroup, SWT.NONE);
		prinTextbox.setText("?");
		prinTextbox.setToolTipText("Sets the PRIN register that controls the exposure/frame time. See XCLIB manual. If -1, the register is not set.");
		prinTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		prinTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lRX = new Label(swtGroup, SWT.NONE); lRX.setText("X:");
		roiX0Text = new Spinner(swtGroup, SWT.NONE);
		roiX0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiX0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lRY = new Label(swtGroup, SWT.NONE); lRY.setText("Y:");
		roiY0Text = new Spinner(swtGroup, SWT.NONE);
		roiY0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		roiMaxButton = new Button(swtGroup, SWT.PUSH);
		roiMaxButton.setText("Max");
		roiMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		roiMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { roiMaxEvent(); } });
		
		Label lRW = new Label(swtGroup, SWT.NONE); lRW.setText("X1:");
		roiX1Text = new Spinner(swtGroup, SWT.NONE);
		roiX1Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiX1Text.setTextLimit(4);
		roiX1Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX1Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lRH = new Label(swtGroup, SWT.NONE); lRH.setText("Y1:");
		roiY1Text = new Spinner(swtGroup, SWT.NONE);
		roiY1Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY1Text.setTextLimit(4);
		roiY1Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY1Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		Label lBL = new Label(swtGroup, SWT.NONE); lBL.setText("");
		
		
		configToGUI();
	}

	void configToGUI(){
		XCLIBConfig config = source.getConfig();
		
		continuousButton.setSelection(config.wrap)	;
		
		if(!driverParamsText.isFocusControl())
			driverParamsText.setText(config.driverParams == null ? "" : config.driverParams);
		
		if(!formatFileText.isFocusControl())
			formatFileText.setText(config.formatFile == null ? "" : config.formatFile);

		exSyncTextbox.setText(Integer.toString(config.exsync));
		prinTextbox.setText(Integer.toString(config.prin));
		widthTextbox.setText(Integer.toString(config.width));
		heightTextbox.setText(Integer.toString(config.height));
	}
	
	void guiToConfig(){
		XCLIBConfig config = source.getConfig();
		
		config.wrap = continuousButton.getSelection();

		config.driverParams = driverParamsText.getText();
		config.exsync = Algorithms.mustParseInt(exSyncTextbox.getText());
		config.prin = Algorithms.mustParseInt(prinTextbox.getText());
		config.formatFile = formatFileText.getText();
		config.width = Algorithms.mustParseInt(widthTextbox.getText());
		config.height = Algorithms.mustParseInt(heightTextbox.getText());
		
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		XCLIBConfig config = source.getConfig();

		if(true)
			throw new NotImplementedException();
		
	}
	
	public void exposureMinMaxEvent(boolean max){
		XCLIBConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
		configToGUI();
	}


	public void roiMaxEvent(){
		XCLIBConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
