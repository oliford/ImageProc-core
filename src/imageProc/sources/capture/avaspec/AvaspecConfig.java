package imageProc.sources.capture.avaspec;

import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;

public class AvaspecConfig extends CaptureConfig {
	
	public String driverSocketAddress = "localhost:5567";

	public int nFrames = 500;
	
	public boolean includeExtra = false;

	/** Start capture on the global start trigger */
	public boolean startTriggerEnable = true;

	public int expTimeMS = 100;
	
	public boolean extTrigger = false;
	
	public boolean continuous = false;
	
	public boolean readDigitalIO = false;
	
}
