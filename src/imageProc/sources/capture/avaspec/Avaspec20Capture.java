package imageProc.sources.capture.avaspec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ShortBuffer;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import imageProc.core.ByteBufferImage;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.core.AcquisitionDevice.Status;
import net.jafama.FastMath;

/** Driver communication with the avaspec v2.0 linux 'driver'.
 * The driver makes a socket and talks a bizarre intermediate ASCII protocol.
 * It's quite awful, and suffers from the misguided notion that it thinks it can handle multiple clients at the same time.
 * 
 * To work properly, it requires a patch to add the '!' command, which means 'stop, please stop, arrrrg die goddammit'
 *    
 * @author oliford
 */
public class Avaspec20Capture extends Capture {
	
	private int requestTimeoutMS = 3000;
	
	private Socket socket = null;
	private PrintStream ps;
	private BufferedReader br;
	
	private int numChannels;
	private int numPixels;
	private int numExtra;
	
	private float nonlinear[][];
	private String ijktime[];
	private float ijking[][];
	/** [size, pixel0, pixel1, a0, a1, a2, a2]  wavelength = a0 + a2*px + a2*px^2 + ... */
	private float calibData[][];
	private int exposureTimeMS;
	
	/** In sequence */
	private int seqNumFrames; 
	private int digitalInStatePerFrame[];
	
	private AvaspecSource source;
	
	public Avaspec20Capture(AvaspecSource source) {
		this.source = source;
		this.seqNumFrames = -1;
		setStatus(Status.notInitied, "Init ready. Not open.");
	}
		
	public void mustOpen(int port){
		try {
			open(port);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void open(int port)  throws IOException {
		open("localhost", port);		
	}
	
	public void open(String host, int port) throws IOException {
		
		socket = new Socket(host, port);		
		socket.setSoTimeout(requestTimeoutMS);		
		ps = new PrintStream(socket.getOutputStream());		
		br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		ps.println("ii");		
		String infoLine = br.readLine();
		if(infoLine.startsWith("E"))
			throw new RuntimeException("Error reported on init:" + infoLine);
		
		String infoParts[] = infoLine.split(" ");
		
		numChannels = Integer.parseInt(infoParts[0]);
		numPixels = Integer.parseInt(infoParts[1]);
		numExtra = Integer.parseInt(infoParts[2]);
		
		setStatus(Status.init, "Opened avaspec with " + numChannels + " channels x" + numPixels + " pixels.");
		System.out.println(statusString);
		
		nonlinear = new float[numChannels][];
		ijktime = new String[numChannels];
		ijking = new float[numChannels][];
		calibData = new float[numChannels][];
				
		for(int iCh=0; iCh < numChannels; iCh++){
			ps.println(iCh + "i");
			
			nonlinear[iCh] = readArray(br);
			ijktime[iCh] = br.readLine();
			ijking[iCh] = readArray(br);
			
			ps.println("i" + iCh); //yes, that's really a different command!
			String line = br.readLine();
			if(line.startsWith("E"))
				throw new RuntimeException("Error reported on calib read:" + line);
			
			String parts[] = line.split(" ");
			int size = Integer.parseInt(parts[0]);
			
			calibData[iCh] = new float[size+2];
			for(int i=0; i < size+2; i++){
				calibData[iCh][i] = Float.parseFloat(parts[i+1]);
			}			
		}
	}
	
	private float[] readArray(BufferedReader br) throws IOException{
		String line = br.readLine();
		if(line.startsWith("E"))
			throw new RuntimeException("Error reported on array read:" + line);
		
		int size = Integer.parseInt(line);
		
		float arr[] = new float[size];
		for(int i=0; i < size; i++){
			line = br.readLine();
			if(line.startsWith("E"))
				throw new RuntimeException("Error reported in array read:" + line);
			
			arr[i] = Integer.parseInt(line);
		}	
		return arr;
	}
	
	public void setExposureTime(int exposureTimeMS){
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		this.exposureTimeMS = exposureTimeMS;
		
		ps.println("t" + exposureTimeMS);
		checkError("set exposure time");
	}
	
	/** Gets a single line and just reports errors if there was one */
	private String checkError(String whatDoing) {
		String line;
		try {
			line = br.readLine();
		} catch(SocketTimeoutException err){
			throw new RuntimeException("Timed out waiting for response to '"+whatDoing+"'");
			
		} catch (IOException err) {			
			throw new RuntimeException(err);
		}
		
		if(line == null)
			throw new RuntimeException("NULL line returned during "+whatDoing+". Did the avaspecd driver die??");
		
		if(line.startsWith("E")){			
			throw new RuntimeException("Error during "+whatDoing+": " + line);
			
		}if(line.startsWith("#") || line.startsWith("newdata") || line.startsWith("data")){
			throw new RuntimeException("Looks like data which we weren't expecting. Suggest reset driver socket. During '"+whatDoing+"'.");
		}
		return line;
	}
	
	public void close() {
		if(socket != null && !socket.isClosed()){
			try {
				socket.close();
				setStatus(status.notInitied, "Closed");
			} catch (IOException e) { }			
		}
	}
	
	private int[] readDigitalIOState(){
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		ps.println("id");
		String line = checkError("readDigitalIOState");
		String parts[] = line.split(" ");
		int ret[] = new int[parts.length];
		for(int i=0; i < parts.length; i++){
			ret[i] = Integer.parseInt(parts[i]);
		}
		return ret;	
	}
	
	public void setDigitalInStateArray(int dinState[]){
		this.digitalInStatePerFrame = dinState;
	}
	
	public void readSpectra(ByteBufferImage images[], double time[], long nanoTime[], int nFrames, boolean includeExtra, boolean continuous){		
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
				
		String frameInfo[] = new String[nFrames];
		double t0 = Double.NEGATIVE_INFINITY;
								
		//ShortBuffer extraBuffer = includeExtra ? buffer : ShortBuffer.allocate(numExtra);
		
		seqNumFrames = nFrames;
		signalAbort = false;
		
		if(digitalInStatePerFrame != null){
			ps.println("D1"); //use this the 5v supply to feed back to dIn0
			checkError("Setting Dout1 high");
		}
		
		try{
			//await acq start
			setStatus(Status.awaitingSoftwareTrigger, "Waiting for acquire enable");
			while(!signalAcquireStart) {
				try {
					Thread.sleep(1);
				}catch(InterruptedException err){
					
				}
			}
			
			if(continuous){
				ps.println("C"); //this starts it too
				checkError("Set C");
			}else{
				ps.println("c"); //disable continuous
				checkError("set c");
				ps.println("r" + nFrames);
				checkError("issuing read spectra command");
			}
			
			//socket.setSoTimeout(FastMath.max(requestTimeoutMS, 1.2*exposureTimeMS)));
			try{
				socket.setSoTimeout(exposureTimeMS + 500);
			}catch(SocketException e){ }
			
			
			
			setStatus(Status.awaitingHardwareTrigger, "Waiting for first frame"); 
			int iF=0;
			while(iF < nFrames){
								
				try{
					WriteLock writeLock = images[iF].writeLock();
					writeLock.lockInterruptibly();
					try{
						ShortBuffer buffer = images[iF].getWritableBuffer(writeLock).asShortBuffer();				
						frameInfo[iF] = waitForFrame(buffer, includeExtra);		
						
					}finally{ writeLock.unlock(); }
					
					setStatus(Status.capturing, "Read frame " + iF + "/ " + nFrames + (continuous ? "(continuous)." : "."));
					
					if(digitalInStatePerFrame != null){
						int idx = frameInfo[iF].indexOf("din=");
						if(idx < 0){
							digitalInStatePerFrame[iF] = -1;
						}else{
							String dinStr = frameInfo[iF].substring(idx+4);
							digitalInStatePerFrame[iF] = Integer.parseInt(dinStr);
						}						
					}
					
					if(time != null){
						double t = extractTime(frameInfo[iF]);
						if(iF == 0){
							t0 = t;
							time[iF] = 0;
							nanoTime[iF] = System.currentTimeMillis() * 1_000_000;
						}else{
							time[iF] = t - t0;
							nanoTime[iF] = nanoTime[0] + (long)(time[iF]*1e9); 
						}
					}
					
					source.frameCaptured(iF);
						
					iF++;
					if(iF == nFrames && continuous)
						iF=0;
					
					if(signalAbort){
						abortAndClearBuffer();
						setStatus(Status.aborted, "Sequence cleanly aborted after " + iF + " of " + nFrames + " frames");					
						
						return;
					}
										
				}catch(InterruptedException err){
					throw new RuntimeException("Interrupted while waiting for image write lock.");					
				}
			}
			
			setStatus(Status.completeOK, "Read sequence complete");
			
		}catch(RuntimeException err){
			setStatus(Status.errored, "ERROR: " + err);
			
			try {
				abortAndClearBuffer();				
			} catch (RuntimeException err2) {
				err.addSuppressed(err2);
			}
			
			throw(err); //and re-throw the causing error
		}finally{
			try {
				socket.setSoTimeout(requestTimeoutMS);
			} catch (SocketException e) {	}			
			seqNumFrames = -1;
			source.statusChanged();
		}
	}
	private double extractTime(String frameInfo){
		int i0=-1;
		char c[] = frameInfo.toCharArray();
		int j=0;
		String s[] = new String[8];
		for(int i=0; i < c.length; i++){
			if(c[i] == '/')
				i0 = i+1;
			if(i0 > 0 && (c[i] == ':' || c[i] == ']')){
				s[j++] = frameInfo.substring(i0, i);
				i0=i+1;
			}
		}
		double time = Integer.parseInt(s[0]) * 3600 + 
				Integer.parseInt(s[1]) * 60 +
				Double.parseDouble(s[2]);
		/*String parts[] = frameInfo.split("[\\[\\]]");
		parts = parts[1].split("/");
		parts = parts[1].split(":");
		double time = Integer.parseInt(parts[0]) * 3600 + 
						Integer.parseInt(parts[1]) * 60 +
						Double.parseDouble(parts[2]);
		*/
		
		return time;
	}
	
	private String waitForFrame(ShortBuffer buffer, boolean includeExtra){
		
		String line, frameInfo;
		try {
			
			line = br.readLine();
			
			if(line.startsWith("E"))
				throw new RuntimeException("Error on frame read:" + line);
			
			if(!line.startsWith("newdata"))
				throw new RuntimeException("Expected 'newdata' but got: " + line);
			
			frameInfo = line.substring(8);

			for(int iCh=0; iCh < numChannels; iCh++){
				line = br.readLine();
	
				if(!line.startsWith("data"))
					throw new RuntimeException("Expected 'data' but got: " + line);
				
				String lineParts[] = line.split(" ");
				int channel = Integer.parseInt(lineParts[1]);
				int min = Integer.parseInt(lineParts[2]);
				int max = Integer.parseInt(lineParts[3]);
				if(channel != iCh)
					throw new RuntimeException("Was expecting channel " + iCh + " but got channel "+channel);
				
				readBlock(buffer, max - min);	
				readBlock(includeExtra ? buffer : null, numExtra); //erm... what is this???
				
			}
			line = br.readLine();
			
			if(!line.startsWith("done"))
				throw new RuntimeException("Expected 'done' but got: " + line);
			
			if(line.length() > 4){
				frameInfo += line.substring(4);
			}
			

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		return frameInfo;
	}
	
	private short[] readBlock(ShortBuffer buf, int total) throws IOException{
		
		short dataOut[] = new short[total];
		for(int k = 0; k < total / 25 + 1; k++){
			String line = br.readLine();
			int num = total - k * 25;
			if (num > 25)
				num = 25;
			
			if (line.length() != 3 * num + 1)			
				throw new RuntimeException("Expecting data line of length " + (3*num+1)+
											" but got " + line.length() + ". Line is: '"+line+"'");
			
			if(buf == null) //if they gave us a null buffer, we are just skipping lines
				continue;
			
			//copied out of avaspec::l_fill_block and avaspec::l_decode. I've no clue what this does
			//it seems to want to code it into an ASCII range. Maybe something like base64??			
			for(int t=0; t < num; t++){
				int i0 = t * 3 + 1;
				
				int mask = (1 << 5) - 1;
				char offset = ' ';
				short val = 0;
				for(int i = 0; i < 3; i++){
					val += ( (line.charAt(i0+i) - offset) & mask) << (5 * i);
				}	
				buf.put(val);				
			}
		}
		
		return dataOut;
	}

	public int getNumPixels(){ 
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		return numPixels;
	}

	public int getNumChannels() {
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		return numChannels;
	}

	public int getNumExtraPixels() {
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		return numExtra;
	}

	public float[][] getCalibrationInfo(){
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		return calibData;
	}
	
	public float[][] getWavelengths(){
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		float ret[][] = new float[numChannels][numPixels];
		for(int iCh=0; iCh < numChannels; iCh++){
			for(int iP=0; iP < numPixels; iP++){				
				if(iP < calibData[iCh][0] || iP >= calibData[iCh][1]){
					ret[iCh][iP] = Float.NaN;
					continue;
				}
				for(int j=0; j < (calibData[iCh].length-2); j++){
					ret[iCh][iP] += FastMath.pow(iP, j) * calibData[iCh][j+2];
				}
			}
		}
		return ret;
		
		/*
		float result = 0;
		float power = 1;
		for (unsigned i = 0;
				i < m_channelinfo[channel].calibration.size ();
				++i)
		{
			result += power * m_channelinfo[channel].calibration[i];
			power *= px;
		}*/
	}

	public boolean isIdle() {
		return (seqNumFrames < 0);
	}

	public void abort() {
		signalAbort = true;		
	}

	public void setExternalTrigger(boolean enable) {
		if(socket == null || socket.isClosed() || !socket.isConnected())
			throw new RuntimeException("Not connected");
		
		ps.println(enable ? "X" : "x");
		checkError("setExternalTrigger");
	}

	public void abortAndClearBuffer() {
		try{
			//now read out everything from the socket until there's a reasonable delay
			//we need to make some attempt to stop the thing, which uses a new command I hacked in to the driver code
			// It's really quite difficult to get this thing back into a state where it's ready to behave itself again
			
			for(int i=0;i < 10; i++){
				ps.println("!"); //abortAll (my driver hack)
				try{ Thread.sleep(100); }catch(InterruptedException err3){ }
				//Ahhh, why won't you die, motherf#*ker??			
				
				socket.setSoTimeout(100);
			
				try{					
					while(br.readLine() != null);
				}catch(SocketTimeoutException err2){ 
					// all ok
				}
				
				socket.setSoTimeout(requestTimeoutMS);
				
				//make sure the driver isn't still throw rubbish at us
				ps.println("ii\n");
				String infoLine = br.readLine();				
					
				if(infoLine.split(" ").length == 3){
					System.err.println("Abort["+i+"]: Re-init OK:" + infoLine);
					break;
				}
				
				System.err.println("Abort["+i+"]: Re-init check failed:" + infoLine);
				//...try again :/
			}
			
		}catch(IOException err){
			throw new RuntimeException(err);
			
		}finally{
			try{
				socket.setSoTimeout(requestTimeoutMS);
			}catch(SocketException err2){ }
		}
	}

	public boolean isOpen() {
		return (socket != null && socket.isConnected() && !socket.isClosed());
	}

	@Override
	protected AvaspecSource getSource() { return source;	}
	
}
