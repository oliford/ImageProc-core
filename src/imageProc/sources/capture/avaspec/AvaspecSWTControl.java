package imageProc.sources.capture.avaspec;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.database.json.JSONFileSettingsControl;

public class AvaspecSWTControl implements ImagePipeController {
	
	private Group swtGroup;
	
	private Label sourceStatusLabel;
	private Label captureStatusLabel;
	
	private Combo addressCombo;
	private Button openButton;
	private Button closeButton;
	
	private Spinner numFramesSpinner;
	private Button allowAutoconfigCheckbox;
	private Button startButton;
	private Button abortButton;
	
	private Spinner exposureTimeMS;
	private Button continuousCheckbox;
	private Button externalTriggerCheckbox;
	private Button readDigitalIOCheckbox;
	
	private Button acquireEnableCheckbox;
	private Button startTriggerCheckbox;
	
	private JSONFileSettingsControl jsonSettings;
	
	private AvaspecSource source;

	public AvaspecSWTControl(Composite parent, int style, AvaspecSource source) {
		this.source = source;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Avaspec Control");
		swtGroup.setLayout(new GridLayout(5, false));

		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("Source status:");
		sourceStatusLabel = new Label(swtGroup, SWT.NONE);
		sourceStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capture status:");
		captureStatusLabel = new Label(swtGroup, SWT.NONE);
		captureStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Port:");		
		addressCombo = new Combo(swtGroup, SWT.MULTI);
		addressCombo.setItems(new String[0]);
		addressCombo.select(0);
		addressCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		openButton = new Button(swtGroup, SWT.PUSH);
		openButton.setText("Open");
		openButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		openButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { openButtonEvent(event); } });
		
		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close");
		closeButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeButtonEvent(event); } });
		
		Label lNI = new Label(swtGroup, SWT.NONE); lNI.setText("no. Frames:");
		numFramesSpinner = new Spinner(swtGroup, SWT.NONE);
		numFramesSpinner.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		numFramesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		numFramesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		allowAutoconfigCheckbox = new Button(swtGroup, SWT.CHECK);
		allowAutoconfigCheckbox.setText("Allow auto");
		allowAutoconfigCheckbox.setToolTipText("Allow autoconfiguration from other modules (e.g. from an autopilot)");
		allowAutoconfigCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		allowAutoconfigCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
				
		
		continuousCheckbox = new Button(swtGroup, SWT.CHECK);
		continuousCheckbox.setText("Continuous");
		continuousCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		continuousCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure time / ms:");
		exposureTimeMS = new Spinner(swtGroup, SWT.NONE);
		exposureTimeMS.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		exposureTimeMS.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		exposureTimeMS.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		externalTriggerCheckbox = new Button(swtGroup, SWT.CHECK);
		externalTriggerCheckbox.setText("Ext H/W Trigger");
		externalTriggerCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		externalTriggerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		readDigitalIOCheckbox = new Button(swtGroup, SWT.CHECK);
		readDigitalIOCheckbox.setText("Read DIO");		
		readDigitalIOCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		readDigitalIOCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		acquireEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		acquireEnableCheckbox.setText("Acquire");		
		acquireEnableCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		acquireEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		startTriggerCheckbox = new Button(swtGroup, SWT.CHECK);
		startTriggerCheckbox.setText("Start on S/W trigger");		
		startTriggerCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		startTriggerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		startButton = new Button(swtGroup, SWT.PUSH);
		startButton.setText("Start");
		startButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startButtonEvent(event); } });
		
		abortButton = new Button(swtGroup, SWT.PUSH);
		abortButton.setText("Abort");
		abortButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		abortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortButtonEvent(event); } });
		
		jsonSettings = new JSONFileSettingsControl();
		jsonSettings.buildControl(swtGroup, SWT.NONE, source);
		jsonSettings.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 5, 1));
		
		doUpdate();
		
	}
	
	protected void abortButtonEvent(Event event) {
		source.abort();
		
	}

	protected void startButtonEvent(Event event) {
		source.startCapture();
	}

	protected void settingsChangedEvent(Event event) {
		AvaspecConfig config = source.getConfig(); 
		
		source.enableAcquire(acquireEnableCheckbox.getSelection());	 //do first, time critical
		config.enableAutoConfig = allowAutoconfigCheckbox.getSelection();
		config.beginCaptureOnStartEvent = startTriggerCheckbox.getSelection();
		config.driverSocketAddress = addressCombo.getText();
		config.nFrames = numFramesSpinner.getSelection();
		config.expTimeMS = exposureTimeMS.getSelection();
		config.extTrigger = externalTriggerCheckbox.getSelection();
		config.continuous = continuousCheckbox.getSelection();
		config.readDigitalIO = readDigitalIOCheckbox.getSelection();		
		
	}

	protected void closeButtonEvent(Event event) {
		source.close();
	}

	protected void openButtonEvent(Event event) {
		source.openSocket();
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}
	
	private void doUpdate() {
		if(swtGroup.isDisposed())
				return;

		AvaspecConfig config = source.getConfig(); 
		
		sourceStatusLabel.setText(source.getSourceStatus());
		captureStatusLabel.setText(source.getCaptureStatus());
		captureStatusLabel.setToolTipText(source.getCaptureStatus());
		
		acquireEnableCheckbox.setSelection(source.isAcquireEnabled());
		
		addressCombo.setText(config.driverSocketAddress == null ? "" : config.driverSocketAddress);
		readDigitalIOCheckbox.setSelection(config.readDigitalIO);
		startTriggerCheckbox.setSelection(config.beginCaptureOnStartEvent);
		numFramesSpinner.setSelection(config.nFrames);
		allowAutoconfigCheckbox.setSelection(config.enableAutoConfig);
		exposureTimeMS.setSelection(config.expTimeMS);
		externalTriggerCheckbox.setSelection(config.extTrigger);		
		continuousCheckbox.setSelection(config.continuous);
	}

	@Override
	public void destroy() {
		source.controllerDestroyed(this);
		swtGroup.dispose();		
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return source; }
	
	
}
