package imageProc.sources.capture.avaspec;

import java.nio.ByteOrder;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.AcquisitionDevice;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.andorCam.AndorCamConfig;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.picam.PicamConfig;
import imageProc.sources.capture.picam.PicamConfig.Parameter;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import picamJNI.PICamDefs;
import picamJNI.PICamROIs;
import picamJNI.PICamROIs.PICamROI;

/** Doesn't really follow the general Capture,Source,Config structure because it was only a hack to run an old spectrometer
 * which of course later became a diagnostic, as ever.
 * 
 * @author oliford
 *
 */
public class AvaspecSource extends CaptureSource implements ConfigurableByID {

	private BulkImageAllocation<ByteBufferImage> bulkAlloc;
	
	private ByteBufferImage images[];
	
	private Avaspec20Capture capture;
	
	private Thread captureThread;
	
	private int nCaptured;
	
	private int lastCaptured;
	
	private AvaspecConfig config = new AvaspecConfig();
	
	public AvaspecSource() {
		this.capture = new Avaspec20Capture(this);
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera
		config.driverSocketAddress = SettingsManager.defaultGlobal().getProperty("imageProc.sources.capture.avaspec.driverSocketAddress", "localhost:5567");
		captureThread = null;
	}
	
	public AvaspecSource(Avaspec20Capture capture) {
		this.capture = capture;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera
		captureThread = null;
		
	}
		
	public void openSocket() {
		String parts[] = config.driverSocketAddress.split(":");		
		try {
			capture.open(parts[0], Integer.parseInt(parts[1]));
		} catch (Exception err) {	
			capture.setStatus(Status.errored, "ERROR during connect/init: " + err);
			err.printStackTrace();
		}
		updateAllControllers();
	}

	public void close() {
		capture.close();
		updateAllControllers();
	}

	public String getStatus() { return capture.getStatusString(); }
	
	/** Called by SensicamCapture. Don't take long over this! */
	public void frameCaptured(int frameIndex) {

		images[frameIndex].imageChanged(true);
		
		if(frameIndex >= nCaptured)
			nCaptured = frameIndex+1;
		lastCaptured = frameIndex;
		
		updateAllControllers();				
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		

	@Override
	public AvaspecSource clone() {
		return new AvaspecSource(capture);
	}

	@Override
	public boolean isIdle() { return capture.isIdle();	}

	public String getSourceStatus(){ 
		return config.nFrames + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). "
				+ "Run time = " + (config.expTimeMS * config.nFrames / 1e3) + "s";
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
		
	@Override
	public boolean open(long timeoutMS) {
		
		//loadConfig(info)
		
		//automated setup call from the pilots
		if(!capture.isOpen()){
			openSocket();
			if(!capture.isOpen()){
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void startCapture() {
		if(captureThread != null && captureThread.isAlive())
			throw new RuntimeException("Capture already active");
		
		captureThread = new Thread(new Runnable() { @Override public void run() { doCapture(); }});
		captureThread.start();
		
	}
	
	private void doCapture(){
		try{
			allocate();
			capture.abortAndClearBuffer();
			capture.setExternalTrigger(config.extTrigger);
			capture.setExposureTime(config.expTimeMS);
			float wavelengths[][] = capture.getWavelengths();
			setSeriesMetaData("Avaspec/wavelengths", wavelengths, false);
			setSeriesMetaData("wavelength", wavelengths[0], false);			
			double time[] = Mat.fillArray(Double.NaN, config.nFrames);
			setSeriesMetaData("time", time, true);
			long nanoTime[] = new long[config.nFrames];
			setSeriesMetaData("Avaspec/nanoTime", nanoTime, true);
			setSeriesMetaData("Avaspec/exposureTimeMS", config.expTimeMS, false);
			setSeriesMetaData("Avaspec/extTrigger", config.extTrigger, false);
			if(config.readDigitalIO){
				int digitalInState[] = Mat.fillIntArray(-2, config.nFrames);
				capture.setDigitalInStateArray(digitalInState);
				setSeriesMetaData("Avaspec/DigitalInState", digitalInState, true);
			}
			capture.readSpectra(images, time, nanoTime, config.nFrames, config.includeExtra, config.continuous);
		}catch(RuntimeException err){
			capture.setStatus(Status.errored, "ERROR in capture: " + err);
			err.printStackTrace();
		}
	}
		
	public void abort() {		
		capture.abort();
		if(captureThread != null && captureThread.isAlive())
			captureThread.interrupt();
	}
		
	private void allocate(){
		int width = capture.getNumPixels();
		int height = capture.getNumChannels();
		int bitDepth = 16;
		int footerSize = 0;
		if(config.includeExtra){
			//this is on every channel so can't go in the footer
			width += capture.getNumExtraPixels();
		}
		
		try{					
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, config.nFrames)){
				System.out.println("allocate(): Cleared and reallocated " + config.nFrames + " images");
				images = bulkAlloc.getImages();
				
			}else{
				System.out.println("allocate(): Reusing existing " + config.nFrames + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}

		notifyImageSetChanged();
		updateAllControllers();
		
	}
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new AvaspecSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}
	
	@Override
	public String toShortString() {
		return "AVASpec";
	}
	
	@Override
	public int getFrameCount() { return config.nFrames; }
	
	@Override
	public void setFrameCount(int frameCount) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.nFrames = frameCount;
		updateAllControllers();
	}
	
	@Override
	public long getFramePeriod() { return config.expTimeMS * 1000L; }
	
	@Override
	public void setFramePeriod(long framePeriodUS) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.expTimeMS = (int)(framePeriodUS / 1000);
		updateAllControllers();
	}

	@Override
	protected Avaspec20Capture getCapture() { return capture; }

	@Override
	protected AvaspecConfig getConfig() { return config; }
	public void setConfig(CaptureConfig config) { 
		this.config = (AvaspecConfig)config; 
		updateAllControllers(); 
	}
	
	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig;	}
}
