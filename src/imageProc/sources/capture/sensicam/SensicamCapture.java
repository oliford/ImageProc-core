package imageProc.sources.capture.sensicam;

import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import imageProc.core.AcquisitionDevice;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.core.AcquisitionDevice.Status;
import sensicamJNI.CamTypes;
import sensicamJNI.SenbufDev;
import sensicamJNI.SencamDef;
import sensicamJNI.SencamDef.cam_values;
import sensicamJNI.Sensicam;
//import sensicamJNI.fake.Sensicam;
import sensicamJNI.SensicamException;

/** Thread runner used to capture images.
 *  
 * It contains all the actual JNI calls to the camera library since
 * we init the library and open the device each time we run a capture series
 * 
 * However, only 1 of these should ever be created (for each board)
 * so that it won't try multiple at the same time.
 * 
 * 
 * ... Since this was very early, it isn't a proper implementor of Capture
 * 
 */
public class SensicamCapture extends Capture implements Runnable {
	
	private SensicamSource source;

	private int nextImgIdx;
	
	private ByteBufferImage images[];
	
	private SensicamConfig cfg; 
		
	/** Internal state */
	private long hDriver = 0;
	private int kernelBufferNo[];
	/** Number of buffers (in camera?, in library?) */
	private int imgDataSize;
	
	private int cameraType;
	private int ccdWidth, ccdHeight;
	private int actWidth, actHeight;
	private int bitDepth;

	private boolean modifyConfig = false;
	
	/** nanosec relative time of return from select call of first and most recent images */ 
	private long firstSelectTimeRelNS, lastSelectTimeRelNS;
	/** millisec time since 1970 of return from select call of first and most recent images */
	private long firstSelectTimeMS, lastSelectTimeMS;	
	/** nanoTime of middle fo frame, based on return from select() call time */
	private long nanoTime[];
	/** as nanoTime, but in secs since first image */
	private double time[];
	
	private HashMap<String,Object> metaDataMap;
	
	@Override
	public void run() {
		try{
			if(hDriver != 0){
				throw new RuntimeException("Camera driver already open, aborting both");
			}
			
			if(cfg.multiConfigNumImgs != null){
				cfg.selectMultiConfig(0);
			}
			
			setStatus(Status.init, "Capture init");
			initCamera(); 				
			setupCOC();
			initBuffers();
			initImages();			
			doCapture();
			setStatus(Status.completeOK, "Capture done");
		}catch(Exception err){
			logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
			setStatus(Status.errored, "ERROR: " + err.getMessage());
		}finally{
			deinit();
			cfg = null;
			source = null;
		}
	}
	
	
	private void initCamera() {
		kernelBufferNo = new int[cfg.nKernelBuffers];
		for(int i=0; i < cfg.nKernelBuffers; i++)
			kernelBufferNo[i] = -1;
		
		/* Setup logging */
		if(cfg.logFile != null){
			Sensicam.sen_enable_message_log(0xffff, cfg.logFile);
			logr.info(".init(): Camera logging to '" + cfg.logFile + "'.");
		}
		if(cfg.enableSyslog)		
			Sensicam.sen_set_syslog_facility(0xffff);
		
		/* Open device */
		long retL[] = Sensicam.sen_initboard(cfg.boardNo); 
		hDriver = retL[1];
		logr.info(".init(): initboard() = " + retL[0] + ", hDriver = " + hDriver);

		int ret = Sensicam.sen_setup_camera(hDriver);
		if(ret < 0) {
			if(System.getenv("PCOPB") == null) 
				throw new SensicamException("sen_setup_camera failed, which is likely because the PCOPB environment variable does exist and specify the path of the PB file e.g. the pb525002.bit");
			else
				throw new SensicamException("sen_setup_camera", ret);
		}
		System.out.println("sen_setup_camera: " + ret);

		SencamDef.cam_param params = new SencamDef.cam_param();
		
		Sensicam.sen_get_cam_param(hDriver, params);
		this.cameraType = params.cam_typ;
		
		logr.info(".setupCOC(): Camera type:" + CamTypes.camTypeToString(cameraType));
		
		switch(cameraType){
			case CamTypes.LONGEXP:				
			case CamTypes.OEM:
			case CamTypes.FASTEXP:
			case CamTypes.LONGEXPI:
			case CamTypes.LONGEXPQE:
			case CamTypes.FASTEXPQE:
			case CamTypes.TESTCAM:
					break;
			case CamTypes.NOCAM:
				throw new SensicamException("Apparently there's no camera. What?");				
			case CamTypes.DICAM:
				throw new SensicamException("DICAM not supported. Please add config here");
			default:
				throw new SensicamException("Unknown camera '" + params.cam_typ + "'. Please add config here");				
		}
	}
	
	private void setupCOC(){

		//Currently this only supports:
		// "���sensicam long exposure���, all ���sensicam qe���, the ���sensicam em��� and ���sensicam uv��� versions of the sensicam camera series."
		// under the 'Long Exposure' settings, which actually support down to 500ns - yea, 'long' then!		
		if(cfg.exposureType != CamTypes.M_LONG)
			throw new SensicamException("Only M_LONG exposure mode supported. Please add config here");
		
		int exposureMode = ((cfg.exposureType & 0xFF) | ((cfg.gainMode & 0xFF) << 8) | ((cfg.exposureSubMode & 0xFF) << 16));
		int trig = cfg.triggerMode;
		
		// ROI bounds are in units of 32 pixels and are 1 based... *GROAN*
		if(cfg.roiW == 0 || cfg.roiH == 0){
			int retI[] = Sensicam.sen_getsizes(hDriver);
			int ret = retI[0];
			ccdWidth = retI[1]; ccdHeight = retI[2];
			actWidth = retI[3]; actHeight = retI[4];
			bitDepth = retI[5];
			
			if(cfg.roiW == 0) cfg.roiW = ccdWidth;		
			if(cfg.roiH == 0) cfg.roiH = ccdHeight;
		}
		//if(cfg.roiX0 % 32 != 0 || cfg.roiY0 % 32 != 0)// || cfg.roiW % 32 != 0 || cfg.roiH % 32 != 0)
			//throw new IllegalArgumentException("ROI must be on 32 pixel boundaries");
		int roiX0b32 = (int)Math.floor((double)cfg.roiX0 / 32.0) + 1;
		int roiX1b32 = roiX0b32 + (int)Math.ceil((double)cfg.roiW / 32.0) - 1;
		int roiY0b32 = cfg.roiY0 / 32 + 1;
		int roiY1b32 = roiY0b32 + (int)Math.ceil((double)cfg.roiH / 32.0) - 1;
		
		String timeValues;		
		
		switch(cfg.exposureSubMode){
			case CamTypes.NORMALLONG: 	timeValues = cfg.delayMS + "," 	+ cfg.exposureMS + ",-1,-1"; break;
			case CamTypes.VIDEO: 		timeValues = "0," 			+ cfg.exposureMS + ",-1,-1"; break;
			case CamTypes.MECHSHUT:				
			case CamTypes.MECHSHUTV: 	
					timeValues = ((cfg.exposureSubMode == CamTypes.MECHSHUTV) ? "0" : cfg.delayMS) + ","
									+ cfg.exposureMS + ","
									+ ((cfg.shutterEndMS < cfg.shutterEndMS) ? "-1,-1" : "0,0,")
									+ cfg.shutterStartMS + "," + cfg.shutterEndMS
									+ ",-1,-1"; 
					break;			
			case CamTypes.QE_FAST: 		timeValues = cfg.delayNS + "," 	+ cfg.exposureNS + ",-1,-1"; break;
			case CamTypes.QE_DOUBLE: 	timeValues = "-1,-1"; break;
			default:
				//all the rest weren't in my documentation
				throw new SensicamException("Unsupported LONG exposure sub mode. Please add config here.");
		}
		
		//modes are just
		// 	CamTypes.LONGEXP / CamTypes.NORMALLONG		"Delay(ms),	Exposure(ms),	-1,-1"
		// 	CamTypes.LONGEXP / CamTypes.VIDEO	 		"0,			Exposure(ms),	-1,-1:
		//	CamTypes.LONGEXP / CamTypes.MECHSHUT 		"Delay(ms),	Exposure(ms),	AV,AV,	START,	STOP,	-1, -1";
		//	CamTypes.LONGEXP / CamTypes.MECHSHUTTV 		"0,			Exposure(ms),	AV,AV,	START,	STOP,	-1, -1";
		//	CamTypes.LONGEXP / CamTypes.QE_FAST			"Delay(ns),	Exposure(ns),	-1,-1"	Exposure min = 500(ns), nearest 100ns will be used
		//	CamTypes.LONGEXP / CamTypes.QE_DOUBLE		"-1,-1"	External trigger only
		
		int ret = Sensicam.sen_set_coc(hDriver, exposureMode, trig, 
										roiX0b32, roiX1b32, roiY0b32, roiY1b32, 
										cfg.binX, cfg.binY, timeValues);
		
		if(ret < 0)
			throw new SensicamException("sen_set_coc", ret);			    
		logr.info(".setupCOC(): sen_set_coc() = " + ret);
	}
	
	private void initBuffers(){

		/* allocate and map the device-buffers with ccdsize */
		int retI[] = Sensicam.sen_getsizes(hDriver);
		int ret = retI[0];
		ccdWidth = retI[1]; ccdHeight = retI[2];
		actWidth = retI[3]; actHeight = retI[4];
		bitDepth = retI[5];

		if(ret < 0)
			throw new SensicamException("sen_getsizes", ret);
		
		for(int i=0;i < cfg.nKernelBuffers; i++)
		{
			kernelBufferNo[i] = -1;
			
			int size=ccdWidth * ccdHeight * 2;	
			retI = Sensicam.sen_allocate_buffer(hDriver, kernelBufferNo[i], size);
			ret = retI[0]; kernelBufferNo[i] = retI[1]; size = retI[2];
			
			if(ret < 0)
				throw new SensicamException("sen_allocate_buffer", ret);
		}
	}
	
	private void initImages(){
		imgDataSize = actWidth * actHeight * (bitDepth == 12 ? 2 : 1);
				
		
		try{
			if(bulkAlloc == null){
				bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
			}
				
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, actWidth, actHeight, bitDepth, 0, 0, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToCapture)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToCapture + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToCapture + " images");
				bulkAlloc.invalidateAll();
			}

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			throw new RuntimeException("Not enough memory for image capture");
		}

	}
	
	private void doCapture() throws InterruptedException{
		
		int ret;
		
		SenbufDev.DEVBUF stat = new SenbufDev.DEVBUF();
		SencamDef.cam_values values = new SencamDef.cam_values();

		setStatus(Status.awaitingSoftwareTrigger, "Capturing, acquisition not started.");	
		//not sure about how best to do acquisition start
		while(!super.signalAcquireStart){
			Thread.sleep(1);
		}		
		setStatus(Status.awaitingHardwareTrigger, "Capturing..., (awaiting hardware trigger)");
		
		startAndEnableBuffers(values);
		int nextBuff = 0;
		
				
		//hang config and timing info to the first image only
		SencamDef.cam_param param = new SencamDef.cam_param();
		Sensicam.sen_get_cam_param(hDriver, param);
		source.addNonTimeSeriesMetaDataMap(param.toMap("Sensicam/param"));
		source.addNonTimeSeriesMetaDataMap(cfg.toMap("Sensicam/config"));
		source.addNonTimeSeriesMetaDataMap(values.toMap("Sensicam/values"));		
		source.setSeriesMetaData("totalFrameTimeMS", values.totalFrameTimeMS(), false);
		
		nextImgIdx = 0;
		int currentMultiConfig = 0;
		
		firstSelectTimeRelNS = Long.MIN_VALUE;
		firstSelectTimeMS = Long.MIN_VALUE;		
		nanoTime = new long[cfg.nImagesToCapture];
		time = new double[cfg.nImagesToCapture];
		
		source.setSeriesMetaData("Sensicam/nanoTime", nanoTime, true);
		source.setSeriesMetaData("Sensicam/time", time, true);		
		
		//for each image in capture sequence
		do{
			
			//If we're doing multiconfig...
			if(cfg.multiConfigNumImgs != null){
				int nImagesPrevSets = 0;
				for(int i=1; i < cfg.multiConfigNumImgs.length; i++){
					nImagesPrevSets += cfg.multiConfigNumImgs[i-1];
					//if(nextImgIdx < nImagesPrevSets)
					//    break;
					//if we've gone over the last image of a set...
					if(nextImgIdx >= nImagesPrevSets && i > currentMultiConfig){
						modifyConfig = true;
						cfg.selectMultiConfig(i);	
						currentMultiConfig = i;
					}
				}
			}

			if(modifyConfig){
				logr.info(".doCapture(): Modifying config... Stopping camera and clearing buffers");
				//we need to clear the buffers so that we don't get old images when we think we have the new config
				
				stopAndClearBuffers();
				
				logr.info(".doCapture().modifyCfg: Camera stopped");
				
				setupCOC();
				startAndEnableBuffers(values);
				modifyConfig = false; // that was the point at which we read the config object, so any further changes, change again
				checkSizesNotModified();
				
				logr.info(".doCapture().modifyCfg: Done, continuing.");	
				
				modifyConfig = false;
			}

			//search for filled buffers, starting from the last one
			int buffIdx = nextBuff;
			do{
				//dumpBufferStats();
				
				if(buffIdx >= kernelBufferNo.length)
					buffIdx=0;
				
				Sensicam.sen_get_buffer_status(hDriver, kernelBufferNo[buffIdx], 0, stat, 4);
				spinLog(".doCapture(): Buffer["+buffIdx+"]: buf = " + buffIdx + ", stat = " + stat);
				
				//if we reach the buffer that is waiting to be read, then we've done all the filled ones
				if(stat.SEN_BUF_STAT_WRITE_DONE() == 0){
					spinLog(".doCapture(): Reached next reading buffer at " + buffIdx);					
					break;
				}				
				
				if(status != Status.capturing)
					setStatus(Status.capturing, "Capturing..., acquisition started");
				
				//if(stat.SEN_BUF_STAT_WRITE_DONE() != 0) { //has some data
				spinLog(".doCapture(): Image complete in buffer "+buffIdx+" with status "+stat);					

				copyFilledBuffer(values, buffIdx, stat);
				
				//dumpBufferStats();
			
				buffIdx++;
				
				//if we've reached the one we started at again, we /may/ have overrun
				if(buffIdx == nextBuff){
					System.err.println("WARNING: Sensicam kernel buffers possible overrun.");
				}
				
				
			}while(true);
			
			nextBuff = buffIdx;
			
			if(signalDeath)break;
			
			//dumpBufferStats();
			
			waitForBufferFill(values.readtime);
			
		}while(nextImgIdx < cfg.nImagesToCapture); //only fires in non-continuous mode
		
		logr.info(".doCapture(): Sequence complete or death requested: " + nextImgIdx + " of " + cfg.nImagesToCapture + ", death = " + signalDeath);
		
		stopAndClearBuffers();
		
		logr.info(".doCapture(): END");		
	}
	
	private void copyFilledBuffer(cam_values values, int buffIdx, SenbufDev.DEVBUF stat){
		int ret;
		
		if(stat.SEN_BUF_STAT_ERROR() != 0){
			spinLog(".doCapture(): Buffer status has error flags set: " + stat.SEN_BUF_STAT_ERROR());
			return;
		}
		
		try {
			WriteLock writeLock = images[nextImgIdx].writeLock();
			writeLock.lockInterruptibly();
			try{
				images[nextImgIdx].invalidate(false);
				ret = Sensicam.sen_copy_buffer(hDriver, kernelBufferNo[buffIdx], images[nextImgIdx].getWritableBuffer(writeLock), imgDataSize, 0);
				
				if(ret <0)
					throw new SensicamException("sen_copy_buffer", ret);
				
				spinLog(".doCapture(): Buffer copied to images[" + nextImgIdx + "]");
				
			}finally{ writeLock.unlock(); }
		} catch (InterruptedException e) {
			logr.info("Interrupted while waiting to write to java image.");
			return;
		}
				
		
		//deal with the timing
		nanoTime[nextImgIdx] = (lastSelectTimeRelNS - firstSelectTimeRelNS) //ns time of select() return, relative to the first
							  - (long)((values.beltime + values.coctime) * 1000)		//bel+coc is the full frame time in us
							  + (long)((values.exptime * 1000) / 2);	// give the time in the middle of the exposure
		
		//that's now nanos since middle first frame, which we use as a friendly time
		time[nextImgIdx] = nanoTime[nextImgIdx] * 1e-9;
		
		//and nanoTime gets converted to a proper nanoTime, using the best absolute time we have
		nanoTime[nextImgIdx] += firstSelectTimeMS * 1_000_000L;
								

		Sensicam.sen_get_buffer_status(hDriver, kernelBufferNo[buffIdx], 0, stat, 4);
		if(stat.SEN_BUF_STAT_QUEUED() == 0)
		{
			spinLog(".doCapture(): Adding buffer "+buffIdx+" (stat "+stat+") back to list.");
			
			ret = Sensicam.sen_add_buffer_to_list(hDriver, kernelBufferNo[buffIdx], imgDataSize, 0, 0);
			if(ret < 0)
				throw new SensicamException("sen_add_buffer_to_list", ret);
		}
		spinLog(".doCapture(): Image["+nextImgIdx+"] done and copied, notifiying source.");
		
		source.imageCaptured(nextImgIdx);
	
		nextImgIdx++;
		if(nextImgIdx >= cfg.nImagesToCapture){
			if(cfg.wrap == SensicamConfig.WRAP_ALL)
				nextImgIdx = 0;
			else if(cfg.wrap == SensicamConfig.WRAP_NOTFIRST && cfg.nImagesToCapture > 1)
				nextImgIdx = 1;
		}
				
		
	}
	
	private void waitForBufferFill(int readTime) throws InterruptedException {
		double maxSelectWait = 0.100; //100ms is a reasonable user response time
		double timeout = readTime / 1000.0 + cfg.captureTimeout;		
		spinLog(".doCapture(): capture timeout = " + timeout + "s, waiting... ");

		int ret;
		
		long selectT0 = System.nanoTime();
		do{
			//use unix select() call to wait nicely for something to happen from device, or for timeout
			//but it can't be interrupted by a java thread interrupt so we have a maximum time 
			//that we will actually sit in select() before checking if we've been interrupted
			ret = Sensicam.select_with_timeout(hDriver, Math.min(timeout, maxSelectWait));
			
			lastSelectTimeRelNS = System.nanoTime();
			lastSelectTimeMS = System.currentTimeMillis(); //this is the closest thing we can get to a real time
			if(firstSelectTimeRelNS == Long.MIN_VALUE){
				firstSelectTimeRelNS = lastSelectTimeRelNS;
				firstSelectTimeMS = lastSelectTimeMS;
			}
			
			if(Thread.interrupted()){
				if(signalDeath){
					System.err.println("Aborted during JNI select");
					break;
				}else
					throw new InterruptedException("Interrupted during JNI select for no apparant reason.");
			}
			
			if(ret == 0 && (lastSelectTimeRelNS - selectT0)*1e-9 > timeout){
				throw new SensicamException("Select timed out. If using HW-trigger mode, check if trigger-signal is connected");					
			}
			
			if(ret < 0)
				throw new SensicamException("Error in select(): " + ret);
		}while(ret == 0); // keep looping if it just timed out on the short timeout
		
	}
	
	private void startAndEnableBuffers(SencamDef.cam_values values) {
		// Load the command into the camera
		int ret = Sensicam.sen_run_coc(hDriver, SencamDef.CAM_CONT);
		if(ret < 0)
			throw new SensicamException("sen_run_coc", ret);
		
		// Mark all the buffers as available for use
		for(int i=0;i<cfg.nKernelBuffers;i++)
		{
			ret = Sensicam.sen_add_buffer_to_list(hDriver, kernelBufferNo[i], imgDataSize, 0, 0);
			if(ret < 0)
				throw new SensicamException("sen_add_buffer_to_list", ret);

			ret = Sensicam.sen_set_buffer_event(hDriver, kernelBufferNo[i], 1);
			if(ret < 0)
				throw new SensicamException("sen_set_buffer_event", ret);
		}
		
		Sensicam.sen_get_cam_values(hDriver, values);
		logr.info(".doCapture(): Times: cocTime = " + values.coctime + ", belTime = " + values.beltime + ", readTime = " + values.readtime);

	}
	
	private void dumpBufferStats() {
		SenbufDev.DEVBUF stat = new SenbufDev.DEVBUF();
		spinLog("Buff status:");
		for(int i=0; i < cfg.nKernelBuffers; i++) {
			Sensicam.sen_get_buffer_status(hDriver, kernelBufferNo[i], 0, stat, 4);
			spinLog(i + ": " + stat);
			
		}
		
	}
		
	private void stopAndClearBuffers() {
		SenbufDev.DEVBUF stat = new SenbufDev.DEVBUF();
		
		int ret = Sensicam.sen_stop_coc(hDriver, 0);
		if(ret < 0)
			throw new SensicamException("sen_stop_camera", ret);		

		//remove device buffers from working list
		
		logr.info(".doCapture(): Camera stopped");
		
		for(int i=0; i < cfg.nKernelBuffers; i++)
		{
			Sensicam.sen_get_buffer_status(hDriver, kernelBufferNo[i], 0, stat, 4);
			//if(SEN_BUF_STAT_QUEUED(&stat))
			{
				logr.info(".doCapture(): Removing buffer "+i+" stat "+stat + " from list. ");
				
				ret = Sensicam.sen_remove_buffer_from_list(hDriver, kernelBufferNo[i]);
				if(ret < 0)
					throw new SensicamException("sen_remove_buffer_from_list", ret);
			}
			//disable select for device buffers
			ret = Sensicam.sen_set_buffer_event(hDriver, kernelBufferNo[i], 0);

			if(ret < 0)
				throw new SensicamException("sen_set_buffer_event", ret);
			
			Sensicam.sen_get_buffer_status(hDriver, kernelBufferNo[i], 0, stat, 4);			
		}
		
	}
	
	private void checkSizesNotModified() {
		int retI[] = Sensicam.sen_getsizes(hDriver);
		int ret = retI[0];
		if(ccdWidth != retI[1] || ccdHeight != retI[2] ||
				actWidth != retI[3] || actHeight != retI[4] ||
				bitDepth != retI[5]){
			throw new SensicamException("Sizes changed in inCaptureModify: " +
					"(ccd="+ccdWidth+"x"+ccdHeight+" , act="+actWidth+"x"+actHeight+", bpp="+bitDepth+")" +
					" --> " +
					"("+retI[1]+"x"+retI[2]+" , "+retI[3]+"x"+retI[4]+", bpp="+retI[5]+")");
		}

	}


	private void deinit(){
		logr.info(".deInit()");
		
		//free buffers
		for(int i=0; i < cfg.nKernelBuffers; i++){
			if(kernelBufferNo[i] >= 0){
				int ret = Sensicam.sen_free_buffer(hDriver, kernelBufferNo[i]);
				if(ret < 0)
					logr.info("deinit(): WARNING: sen_free_buffer returned error: " + ret);
			}
		}

		if(hDriver != 0){
			int ret = Sensicam.sen_closeboard(hDriver);
			logr.info("deinit(): closeboard() = " + ret);
			hDriver = 0;
		}
	}
			
	public void startCapture(SensicamSource source, SensicamConfig cfg, ByteBufferImage images[]) {
		if(isCapturing())
			throw new SensicamException("Capture already active");
		
		//if(thread != null && thread.isAlive()){
		//	throw new SensicamException("Thread already active");
		//}
		
		this.cfg = cfg.clone();
		this.images = images;
		this.nextImgIdx = 0;
		this.source = source;
		setStatus(Status.init, "Thread starting");
		this.cameraType = -1;
		this.signalDeath = false;
		
		thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		System.out.println("Starting sensicam capture thread with priority = " + thread.getPriority());
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> stopCapture(true)));
	}
	
	public void stopCapture(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("SensicamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("SensicamCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { stopCapture(false);	}
	
	public boolean isCapturing(){ return thread != null && thread.isAlive() && !signalDeath; }

	public String getCCDInfo() {
		return CamTypes.camTypeToString(cameraType) + ": " + ccdWidth + " x " + ccdHeight + " x " + bitDepth;
	}

	@Override
	protected CaptureSource getSource() { return source; }
}
