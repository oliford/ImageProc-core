package imageProc.sources.capture.sensicam;

import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;
import sensicamJNI.CamTypes;
import sensicamJNI.SencamDef;

public class SensicamConfig extends CaptureConfig {
	public int boardNo = 0;
	public String logFile = System.getProperty("java.io.tmpdir") + "/sensicam.log";
	public boolean enableSyslog = false;
	
	/** Number of seconds to wait (excluding exposure and read time) for capture to complete. 
	 * For external triggers, set this long enough for the trigger to turn up */
	public double captureTimeout = 3600;
	
	/** One of CamTypes.M_LONG, M_FAST, M_DICAM but only M_LONG is currently supported */
	public int exposureType = CamTypes.M_LONG;
	/** One of CamType.* for M_LONG */
	public int exposureSubMode = CamTypes.NORMALLONG;
	/** 0=normal, 1=extended, 3=low light mode*/
	public int gainMode = 0;
	
	/** In 0-based pixels, but needs to be a multiple of 32 */
	public int roiX0 = 0, roiY0 = 0, roiW = 0, roiH = 0;
	/** Pixel binning, must be a power of 2 */
	public int binX = 1, binY = 1;
	/** Delay and exposure, NS value used for FAST modes and MS for the others */
	public int delayMS = 0, exposureMS = 20;
	public int delayNS = 0, exposureNS = 20000;
	
	/** Shutter output timing relative to exposure start , from MECHSHUT modes.
	 * Set shutterEndMS < shutterStartMS for defaults */
	public int shutterStartMS, shutterEndMS;
	
	/** Number of images before stopping or wrapping back to image 0 */
	public int nImagesToCapture = 1; 
	
	/** Record image sequence, then stop */
	public static final int WRAP_NONE = 0;
	/** Continuous record, wrapping to img 0 */
	public static final int WRAP_ALL = 1;
	/** Record all images once, then continuous record, wrapping to img 1, so img 0 is kept as black level*/
	public static final int WRAP_NOTFIRST = 2;
	
	public int wrap = WRAP_NONE;
	
	/** One of SencamDef.TRIG_* */
	public int triggerMode = SencamDef.TRIG_SEQ_AUTO & SencamDef.TRIG_FRAME_AUTO;
	
	/** Max 32 */
	public int nKernelBuffers = 4;
	
	public int multiConfigNumImgs[];
	public int multiConfigExpMS[];
	public int multiConfigDlyMS[];
	public int multiConfigTriggerMode[];
	
	@Override
	protected SensicamConfig clone() {
		SensicamConfig config = new SensicamConfig();
		config.boardNo = boardNo;
		config.logFile = logFile;
		config.enableSyslog = enableSyslog;
		config.captureTimeout = captureTimeout;
		config.exposureType = exposureType;
		config.exposureSubMode = exposureSubMode;
		config.gainMode = gainMode;
		config.roiX0 = roiX0;			config.roiY0 = roiY0;
		config.roiW = roiW;				config.roiH = roiH;
		config.binX = binX;				config.binY = binY;
		config.delayMS = delayMS;		config.exposureMS = exposureMS;
		config.delayNS = delayNS;		config.exposureNS = exposureNS;
		config.shutterStartMS = shutterStartMS;
		config.shutterEndMS = shutterEndMS;
		config.nImagesToCapture = nImagesToCapture;
		config.wrap = wrap;
		config.nKernelBuffers = nKernelBuffers;
		config.triggerMode = triggerMode;
		if(multiConfigNumImgs != null){
			config.multiConfigNumImgs = multiConfigNumImgs.clone();
			config.multiConfigExpMS = multiConfigExpMS.clone();
			config.multiConfigDlyMS = multiConfigDlyMS.clone();
			config.multiConfigTriggerMode = multiConfigTriggerMode.clone();
		}else{
			config.multiConfigNumImgs = null;
			config.multiConfigExpMS = null;
			config.multiConfigDlyMS = null;
			config.multiConfigTriggerMode = null;
			
		}
		return config;
	}

	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put(prefix + "/boardNo", boardNo);
		map.put(prefix + "/logFile", logFile);
		map.put(prefix + "/enableSyslog", enableSyslog ? -1 : 0);
		map.put(prefix + "/captureTimeout", captureTimeout);
		map.put(prefix + "/exposureType", exposureType);
		map.put(prefix + "/exposureSubMode", exposureSubMode);
		map.put(prefix + "/gainMode", gainMode);
		map.put(prefix + "/roiX0", roiX0);
		map.put(prefix + "/roiY0", roiY0);
		map.put(prefix + "/roiW", roiW);			
		map.put(prefix + "/roiH", roiH);
		map.put(prefix + "/binX", binX);			
		map.put(prefix + "/binY", binY);
		map.put(prefix + "/delayMS", delayMS);		
		map.put(prefix + "/exposureMS", exposureMS);
		map.put(prefix + "/delayNS", delayNS);		
		map.put(prefix + "/exposureNS", exposureNS);
		map.put(prefix + "/shutterStartMS", shutterStartMS);
		map.put(prefix + "/shutterEndMS", shutterEndMS);
		map.put(prefix + "/nImagesToCapture", nImagesToCapture);
		map.put(prefix + "/wrap", wrap);
		map.put(prefix + "/nKernelBuffers", nKernelBuffers);	
		map.put(prefix + "/triggerMode", triggerMode);	
		if(multiConfigNumImgs != null){
			for(int i=0; i < multiConfigNumImgs.length; i++){
				map.put(prefix + "/multiConfig_"+i+"/numImgs"+i, multiConfigNumImgs[i]);
				map.put(prefix + "/multiConfig_"+i+"/expMS"+i, multiConfigExpMS[i]);
				map.put(prefix + "/multiConfig_"+i+"/dlyMS", multiConfigDlyMS[i]);
				map.put(prefix + "/multiConfig_"+i+"/triggerMode"+i, multiConfigTriggerMode[i]);		
			}
		}
		return map;
	}

	public void selectMultiConfig(int i) {
		exposureMS = multiConfigExpMS[i];
		exposureNS = 0;
		delayMS = multiConfigDlyMS[i];
		delayNS = 0;
		triggerMode = multiConfigTriggerMode[i];	
	}
}
