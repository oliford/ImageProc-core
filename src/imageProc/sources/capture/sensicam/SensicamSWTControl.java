package imageProc.sources.capture.sensicam;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import net.jafama.FastMath;
import sensicamJNI.CamTypes;
import sensicamJNI.SencamDef;

public class SensicamSWTControl implements ImagePipeController {
	public static final double log2 = FastMath.log(2);
	
	private static final int exposureSubModes[] = new int[]{
		CamTypes.NORMALLONG,
		CamTypes.VIDEO, 
		CamTypes.MECHSHUT, 
		CamTypes.MECHSHUTV, 
		CamTypes.QE_FAST, 
		CamTypes.QE_DOUBLE
	};
	
	private static final int triggerModes[][] = new int[][]{
		{ SencamDef.TRIG_SEQ_AUTO, SencamDef.TRIG_SEQ_RISING, SencamDef.TRIG_SEQ_FALLING },
		{ SencamDef.TRIG_FRAME_AUTO, SencamDef.TRIG_FRAME_RISING, SencamDef.TRIG_FRAME_FALLING }			
	};
	private static final String triggerModeNames[] = new String[]{ "Auto", "Rising", "Falling" };

	private SensicamSource source;
	private Group swtGroup;
	
	private Label sourceStatusLabel;
	private Label captureStatusLabel;
	private Label ccdInfoLabel;
	private Label timingInfoLabel;
	
	private Button continuousButton;
	private Button blackLevelButton;
	private Spinner numImagesSpinner;
	private Spinner numKBuffersSpinner;
	private Combo exposureMode;
	private Combo gainMode;
	private Combo seqTriggerMode;
	private Combo frameTriggerMode;	
			
	private Spinner delayLengthSpinner;
	private Spinner exposureLengthSpinner;
	private Spinner binningXSpinner, binningYSpinner;
	private Spinner roiX0Text, roiY0Text, roiWText, roiHText;
	
	private Button softStartEnableCheckbox;
	private Button enableAcquireCheckbox;
	private Button startCaptureButton;
	private Button abortCaptureButton;
	private Button releaseMemoryButton;
	
	private Group multiConfigGroup;
	private Button multiConfigEnableCheckbox;
	private Spinner mcNumImagesSpinner[];
	private Spinner mcExpTimeSpinner[];
	private Spinner mcDelayTimeSpinner[];
	private Combo mcHardwareTriggerCombo[];
	
	private SWTSettingsControl settingsCtrl;
	
	private boolean inhibitUpdate = false;
	
	public SensicamSWTControl(Composite parent, int style, SensicamSource source) {
		this.source = source;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Sensicam Control");
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("Src:");
		sourceStatusLabel = new Label(swtGroup, SWT.NONE);
		sourceStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capt:");
		captureStatusLabel = new Label(swtGroup, SWT.NONE);
		captureStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCI = new Label(swtGroup, SWT.NONE); lCI.setText("Cam:");
		ccdInfoLabel = new Label(swtGroup, SWT.NONE);
		ccdInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lTI = new Label(swtGroup, SWT.NONE); lTI.setText("Times:");
		timingInfoLabel = new Label(swtGroup, SWT.NONE);
		timingInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lNI = new Label(swtGroup, SWT.NONE); lNI.setText("Imgs:");
		numImagesSpinner = new Spinner(swtGroup, SWT.NONE);
		numImagesSpinner.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		numImagesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		numImagesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		continuousButton.setToolTipText("Continuous capture: Keep reading images, wrapping");
		
		blackLevelButton = new Button(swtGroup, SWT.CHECK);
		blackLevelButton.setText("Black level");
		blackLevelButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		blackLevelButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		Label lEM = new Label(swtGroup, SWT.NONE); lEM.setText("Exp.:");
		exposureMode = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		for(Integer subMode : exposureSubModes)
			exposureMode.add(CamTypes.exposureModeToString(CamTypes.M_LONG, subMode));
		exposureMode.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		exposureMode.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
				
		Label lGM = new Label(swtGroup, SWT.NONE); lGM.setText("Gain:");
		gainMode = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		gainMode.setItems(new String[]{ "Normal", "Extended", "Low Light"});
		gainMode.setLayoutData(new GridData(SWT.END, SWT.BEGINNING, false, false, 2, 1));
		gainMode.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNB = new Label(swtGroup, SWT.NONE); lNB.setText("Bufs:");
		numKBuffersSpinner = new Spinner(swtGroup, SWT.NONE);
		numKBuffersSpinner.setValues(1, 1, 999, 0, 1, 10);
		numKBuffersSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		numKBuffersSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));		
		
		Label lHWT = new Label(swtGroup, SWT.NONE); lHWT.setText("H/W Trigger:");
		
		Label lST = new Label(swtGroup, SWT.NONE); lST.setText("Seq:");
		seqTriggerMode = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		seqTriggerMode.setItems(triggerModeNames);
		seqTriggerMode.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		seqTriggerMode.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lFT = new Label(swtGroup, SWT.NONE); lFT.setText("Frame:");
		frameTriggerMode = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		frameTriggerMode.setItems(triggerModeNames);
		frameTriggerMode.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		frameTriggerMode.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Times:");
		Label lDT = new Label(swtGroup, SWT.NONE); lDT.setText("Dly:");
		delayLengthSpinner = new Spinner(swtGroup, SWT.NONE);
		delayLengthSpinner.setValues(1, 0, 100000, 0, 1, 1);
		delayLengthSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		delayLengthSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exp:");
		exposureLengthSpinner = new Spinner(swtGroup, SWT.NONE);
		exposureLengthSpinner.setValues(1, 1, 100000, 0, 1, 10);
		exposureLengthSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		exposureLengthSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Bining:");
		Label lBX = new Label(swtGroup, SWT.NONE); lBX.setText("X:");
		binningXSpinner = new Spinner(swtGroup, SWT.NONE);
		binningXSpinner.setValues(1, 1, 256, 0, 1, 10);
		binningXSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		binningXSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lBY = new Label(swtGroup, SWT.NONE); lBY.setText("Y:");
		binningYSpinner = new Spinner(swtGroup, SWT.NONE);
		binningYSpinner.setValues(1, 1, 256, 0, 1, 10);
		binningYSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		binningYSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lR = new Label(swtGroup, SWT.NONE); lR.setText("ROI:");
		Label lRX = new Label(swtGroup, SWT.NONE); lRX.setText("X:");
		roiX0Text = new Spinner(swtGroup, SWT.NONE);
		roiX0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiX0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX0Text.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lRY = new Label(swtGroup, SWT.NONE); lRY.setText("Y:");
		roiY0Text = new Spinner(swtGroup, SWT.NONE);
		roiY0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY0Text.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		Label lR2 = new Label(swtGroup, SWT.NONE); lR2.setText("");
		Label lRW = new Label(swtGroup, SWT.NONE); lRW.setText("W:");
		roiWText = new Spinner(swtGroup, SWT.NONE);
		roiWText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiWText.setTextLimit(4);
		roiWText.setValues(0, 0, 9999, 0, 1, 1);
		roiWText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		Label lRH = new Label(swtGroup, SWT.NONE); lRH.setText("H:");
		roiHText = new Spinner(swtGroup, SWT.NONE);
		roiHText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiHText.setTextLimit(4);
		roiHText.setValues(0, 0, 9999, 0, 1, 1);
		roiHText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		softStartEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		softStartEnableCheckbox.setText("S/W Start");
		softStartEnableCheckbox.setToolTipText("When selected, camera will start acquiring if the global start event is broadcast (i.e. the Start All button)");
		softStartEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		softStartEnableCheckbox.setSelection(false);
		softStartEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		enableAcquireCheckbox = new Button(swtGroup, SWT.CHECK);
		enableAcquireCheckbox.setText("Acquire");
		enableAcquireCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		enableAcquireCheckbox.setSelection(false);
		enableAcquireCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
//*/
		startCaptureButton = new Button(swtGroup, SWT.PUSH);
		startCaptureButton.setText("Capture");
		startCaptureButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		startCaptureButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startCaptureButtonEvent(event); } });

		releaseMemoryButton = new Button(swtGroup, SWT.PUSH);
		releaseMemoryButton.setText("Release Mem");
		releaseMemoryButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		releaseMemoryButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { releaseMemoryButtonEvent(event); } });

		abortCaptureButton = new Button(swtGroup, SWT.PUSH);
		abortCaptureButton.setText("Abort");
		abortCaptureButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		abortCaptureButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortCaptureButtonEvent(event); } });
		
		multiConfigGroup = new Group(swtGroup, SWT.BORDER);
		multiConfigGroup.setText("Multi Config");
		multiConfigGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		multiConfigGroup.setLayout(new GridLayout(5, false));
		
		multiConfigEnableCheckbox = new Button(multiConfigGroup, SWT.CHECK);
		multiConfigEnableCheckbox.setText("Enable");
		multiConfigEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		multiConfigEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
				
		
		Label lMCa = new Label(multiConfigGroup, SWT.NONE); lMCa.setText("No.");
		Label lMCb = new Label(multiConfigGroup, SWT.NONE); lMCb.setText("Num Imgs");
		Label lMCc = new Label(multiConfigGroup, SWT.NONE); lMCc.setText("Exposure");
		Label lMCd = new Label(multiConfigGroup, SWT.NONE); lMCd.setText("Delay");
		Label lMCe = new Label(multiConfigGroup, SWT.NONE); lMCe.setText("Trigger");
		
		int numMultiCfg = 4;
		mcNumImagesSpinner = new Spinner[numMultiCfg];
		mcExpTimeSpinner = new Spinner[numMultiCfg];
		mcDelayTimeSpinner = new Spinner[numMultiCfg];
		mcHardwareTriggerCombo = new Combo[numMultiCfg];
		
		for(int i=0; i < numMultiCfg; i++){
			
			Label lMCn = new Label(multiConfigGroup, SWT.NONE); lMCn.setText(i + ":");
			
			mcNumImagesSpinner[i] = new Spinner(multiConfigGroup, SWT.NONE);
			mcNumImagesSpinner[i].setValues(0, 0, 100000, 0, 1, 10);
			mcNumImagesSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcNumImagesSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcExpTimeSpinner[i] = new Spinner(multiConfigGroup, SWT.NONE);
			mcExpTimeSpinner[i].setValues(1, 1, 100000, 0, 1, 10);
			mcExpTimeSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcExpTimeSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcDelayTimeSpinner[i] = new Spinner(multiConfigGroup, SWT.NONE);
			mcDelayTimeSpinner[i].setValues(1, 1, 100000, 0, 1, 10);
			mcDelayTimeSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcDelayTimeSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcHardwareTriggerCombo[i] = new Combo(multiConfigGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
			mcHardwareTriggerCombo[i].setItems(triggerModeNames);
			mcHardwareTriggerCombo[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			
		}

		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, source);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));

		generalControllerUpdate();		
	}
	
	private void multiConfigEvent(Event event) {
		boolean isMultiConfig = multiConfigEnableCheckbox.getSelection();
		

		numImagesSpinner.setEnabled(!isMultiConfig);
		seqTriggerMode.setEnabled(!isMultiConfig);
		frameTriggerMode.setEnabled(!isMultiConfig);
		
		for(int i=0; i < mcNumImagesSpinner.length; i++){
			mcNumImagesSpinner[i].setEnabled(isMultiConfig);
			mcExpTimeSpinner[i].setEnabled(isMultiConfig);
			mcDelayTimeSpinner[i].setEnabled(isMultiConfig);
			mcHardwareTriggerCombo[i].setEnabled(isMultiConfig);
		}
		
		if(isMultiConfig){
			numImagesSpinner.setEnabled(false);
			seqTriggerMode.setEnabled(false);;
			frameTriggerMode.setEnabled(false);
			
			int nImagesTotal = 0;
			for(int i=0; i < mcNumImagesSpinner.length; i++){
				nImagesTotal += mcNumImagesSpinner[i].getSelection();
			}
			numImagesSpinner.setValues(nImagesTotal, 1, Integer.MAX_VALUE, 0, 1, 10);
		}
	}
			
	private void startCaptureButtonEvent(Event event){
		guiToConfig();			
		source.startCapture();
	}
	
	private void abortCaptureButtonEvent(Event event){
		source.abort();
		doUpdate(); //force an update, so the user can keep clicking until the driver stops being active
	}

	private void releaseMemoryButtonEvent(Event event){
		source.releaseMemory();
	}

	private void settingsChangedEvent(Event event) {
		guiToConfig();	
	}
		
	private void configToGUI(){
		if(inhibitUpdate)
			return;
		try {
			inhibitUpdate = true;
			SensicamConfig config = source.getConfig();
			//exposureSubModes[i]	
	
			numImagesSpinner.setSelection(config.nImagesToCapture);
			numKBuffersSpinner.setSelection(config.nKernelBuffers);
			int expModeSel = -1;
			for(int i=0; i < exposureSubModes.length; i++){
				if(exposureSubModes[i] == config.exposureSubMode){
					expModeSel = i; break;
				}
			}
			exposureMode.select(expModeSel);
			seqTriggerMode.select((config.triggerMode & 0x100) / 0x100);
			frameTriggerMode.select(config.triggerMode & 0x001);
			 
			gainMode.select(config.gainMode);
			continuousButton.setSelection(config.wrap != SensicamConfig.WRAP_NONE); 
			if(config.wrap != SensicamConfig.WRAP_NONE)
				blackLevelButton.setSelection(config.wrap == SensicamConfig.WRAP_NOTFIRST);
			roiX0Text.setSelection(config.roiX0); 
			roiY0Text.setSelection(config.roiY0); 
			roiWText.setSelection(config.roiW);
			roiHText.setSelection(config.roiH); 
			binningXSpinner.setSelection(config.binX); 
			binningYSpinner.setSelection(config.binY);
	
			setGUIExposureBox(config);
			multiConfigEnableCheckbox.setSelection(config.multiConfigNumImgs != null);
			if(config.multiConfigNumImgs != null){
				for(int i=0; i < mcNumImagesSpinner.length; i++){
					mcNumImagesSpinner[i].setSelection(config.multiConfigNumImgs[i]);
					mcExpTimeSpinner[i].setSelection(config.multiConfigExpMS[i]);
					mcDelayTimeSpinner[i].setSelection(config.multiConfigDlyMS[i]);
					mcHardwareTriggerCombo[i].select((config.multiConfigTriggerMode[i] & 0x100) / 0x100);
				}
	
			}else{
				for(int i=0; i < mcNumImagesSpinner.length; i++){
					mcNumImagesSpinner[i].setSelection(0);
					mcExpTimeSpinner[i].setSelection(0);
					mcDelayTimeSpinner[i].setSelection(0);
					mcHardwareTriggerCombo[i].select(0);				
				}
			}
			multiConfigEvent(null); //update enables
		}finally {
			inhibitUpdate = false;
		}
		
	}
	
	private void setGUIExposureBox(SensicamConfig config){

		if(config.exposureSubMode == CamTypes.QE_FAST){
			delayLengthSpinner.setSelection(config.delayNS);		
			exposureLengthSpinner.setSelection(config.exposureNS);			
		}else{
			delayLengthSpinner.setSelection(config.delayMS);
			exposureLengthSpinner.setSelection(config.exposureMS);			
		}
	}
	
	private void guiToConfig(){
		if(inhibitUpdate)
			return;
		
		try {
			inhibitUpdate = true;
			
			source.enableAcquire(enableAcquireCheckbox.getSelection());
			
			SensicamConfig config = source.getConfig();
			
			//leave these to the default
			//config.boardNo = 0; //meh
			//config.logFile = logFile;
			//config.enableSyslog = enableSyslog;
			//config.captureTimeout = captureTimeout;
			
			config.nImagesToCapture = numImagesSpinner.getSelection();
			config.nKernelBuffers = numKBuffersSpinner.getSelection();
			config.exposureType = CamTypes.M_LONG;
			int expModeSel = exposureMode.getSelectionIndex();
			config.exposureSubMode = (expModeSel >= 0 && expModeSel < exposureSubModes.length) 
											? exposureSubModes[expModeSel] : -1;
			config.gainMode = gainMode.getSelectionIndex();
			config.wrap = !continuousButton.getSelection() ? SensicamConfig.WRAP_NONE :
							(blackLevelButton.getSelection() ? SensicamConfig.WRAP_NOTFIRST : SensicamConfig.WRAP_ALL);
			
			config.triggerMode = triggerModes[0][seqTriggerMode.getSelectionIndex()] |
									triggerModes[1][frameTriggerMode.getSelectionIndex()];
			
			config.roiX0 = roiX0Text.getSelection();			
			config.roiY0 = roiY0Text.getSelection();
			config.roiW = roiWText.getSelection();
			config.roiH = roiHText.getSelection();
			config.binX = binningXSpinner.getSelection();
			config.binY = binningYSpinner.getSelection();
			
			if(config.exposureSubMode == CamTypes.QE_FAST){
				config.delayNS = delayLengthSpinner.getSelection();		
				config.exposureNS = exposureLengthSpinner.getSelection();
				config.delayMS = -1;
				config.exposureMS = -1;
			}else{
				config.delayMS = delayLengthSpinner.getSelection();
				config.exposureMS = exposureLengthSpinner.getSelection();
				config.delayNS = -1;		
				config.exposureNS = -1;
			}
	
			if(multiConfigEnableCheckbox.getSelection()){
				config.multiConfigNumImgs = new int[mcNumImagesSpinner.length];
				config.multiConfigExpMS = new int[mcNumImagesSpinner.length];
				config.multiConfigDlyMS = new int[mcNumImagesSpinner.length];
				config.multiConfigTriggerMode = new int[mcNumImagesSpinner.length];
				for(int i=0; i < mcNumImagesSpinner.length; i++){
					config.multiConfigNumImgs[i] = mcNumImagesSpinner[i].getSelection();
					config.multiConfigExpMS[i] = mcExpTimeSpinner[i].getSelection();
					config.multiConfigDlyMS[i] = mcDelayTimeSpinner[i].getSelection();
					config.multiConfigTriggerMode[i] = triggerModes[0][mcHardwareTriggerCombo[i].getSelectionIndex()];
				}
			}else{
				config.multiConfigNumImgs = null;
				config.multiConfigExpMS = null;
				config.multiConfigDlyMS = null;
				config.multiConfigTriggerMode = null;			
			}
			
			source.configChanged();
		}finally {
			inhibitUpdate = false;
		}
	}
	
	public ImgSource getSource() { return source;	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}
	
	private void doUpdate() {
		if(swtGroup.isDisposed())
				return;
	
		sourceStatusLabel.setText(source.getSourceStatus());
		captureStatusLabel.setText(source.getCaptureStatus());
		captureStatusLabel.setToolTipText(source.getCaptureStatus());
		ccdInfoLabel.setText(source.getCCDInfo());
		
		enableAcquireCheckbox.setSelection(source.isAcquireEnabled());
		
		configToGUI();
	
		Number num;
		num = (Number)source.getSeriesMetaData("totalFrameTimeMS"); double totalTimeMS = (num == null) ? 0 : num.doubleValue(); 
		num = (Number)source.getSeriesMetaData("Sensicam/values/readtime"); double readTimeMS = (num == null) ? 0 : num.doubleValue();
		num = (Number)source.getSeriesMetaData("Sensicam/values/coctime"); double coctime = (num == null) ? 0 : num.doubleValue()/1000;
		num = (Number)source.getSeriesMetaData("Sensicam/values/beltime"); double beltime = (num == null) ? 0 : num.doubleValue()/1000;
		num = (Number)source.getSeriesMetaData("Sensicam/values/exptime"); double exptime = (num == null) ? 0 : num.doubleValue()/1000;
		num = (Number)source.getSeriesMetaData("Sensicam/values/deltime"); double deltime = (num == null) ? 0 : num.doubleValue()/1000;
		
		num = (Number)source.getSeriesMetaData("Sensicam/config/nImagesToCapture"); int nImages = (num == null) ? 0 : num.intValue();
		
		double runTimeMS = totalTimeMS * nImages;
		timingInfoLabel.setText(String.format("%.3f ms [read=%.3f, coc=%.3f, bel=%.3f, exp=%.3f, del=%.3f], run = %.3f s", 
				totalTimeMS, readTimeMS, coctime, beltime, exptime, deltime, runTimeMS/1000.0));

	}
	
	@Override
	public void destroy() {
		source.controllerDestroyed(this);
		swtGroup.dispose();
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return source; }
}
