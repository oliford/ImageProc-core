package imageProc.sources.capture.sensicam;

import org.eclipse.swt.widgets.Composite;

import imageProc.core.AcquisitionDevice;
import imageProc.core.ByteBufferImage;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.pco.PCOCamConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Sensicam Image Source. 
 * All the actual driver'ey code is in the thread in SensicamCapture
 *  
 * @author oliford
 */
public class SensicamSource extends CaptureSource {
	
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	/** The image capturer, we should only have one of these */  
	private SensicamCapture capture;
	
	/** The config used/to be used to capture the images in this source */
	private SensicamConfig config;
	
	private boolean autoExposure = false;
	
	private int lastCaptured;
	
	public SensicamSource(SensicamCapture capture, SensicamConfig config) {
		this.capture = capture;
		this.config = (config != null) ? config : new SensicamConfig();
	}
	
	public SensicamSource() {
		capture = new SensicamCapture();
		config = new SensicamConfig();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by SensicamCapture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	/** Called by SensicamCapture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		lastCaptured = imageIndex;
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		
		updateAllControllers();
	}

	@Override
	public SensicamSource clone() {
		return new SensicamSource(capture, config.clone());
	}

	public void startCapture(){		
		this.nCaptured = 0;
		
		capture.startCapture(this, config, images);
	}
		
	public void abort(){ capture.stopCapture(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToCapture + " allocated. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). S/W start "				
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	public String getCCDInfo(){ return capture.getCCDInfo(); }
	
	public boolean isCapturing(){ return capture.isCapturing(); }
	
	public SensicamConfig getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (SensicamConfig)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new SensicamSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	public void setAutoExposure(boolean enable){ this.autoExposure = enable; }
	public boolean getAutoExposure(){ return this.autoExposure; }
	
	public void releaseMemory() {
		capture.stopCapture(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}
	
	/** Get's the post-exposure time of the last initiated capture seqeunce, in ms */
	public double getLastPostExpTimeMS(){
		Double d = (Double)getSeriesMetaData("totalFrameTimeMS");
		return (d != null) ? d : Double.NaN;
	}

	@Override
	public boolean isIdle() { return capture == null || !capture.isCapturing(); }

	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	
	@Override
	public void close() {
		abort();		
	}

	@Override
	public boolean open(long timeoutMS) {
		startCapture();
		return true; //erm, its probably ok
	}
	
	@Override
	public String toShortString() {
		return "SensiCam";
	}
		
	@Override
	protected final Capture getCapture() { return capture; }
	
	@Override
	public boolean setConfigParameter(String param, String value) {
		if(param.equals("runLength")){
			double runLengthSecs = Algorithms.mustParseDouble(value);
			config.nImagesToCapture = (int)(runLengthSecs * 1000.0 / bestGuessFrameTimeMS() + 0.5);
			return true;
		}
		
		return false;
	}
	
	private double bestGuessFrameTimeMS(){
		//out best guess at frame time comes from the last config/capture read-back
		//otherwise the exposure time
		Number num = (Number)getSeriesMetaData("totalFrameTimeMS");
		if(num != null){
			return (num == null) ? 0 : num.doubleValue(); 
		}else{
			return (config.exposureMS * 1000000L + config.exposureNS) / 1e6;
		}		
	}
	
	@Override
	public String getConfigParameter(String param) {
		
		if(param.equals("runLength")){
			return String.format("%f", config.nImagesToCapture * bestGuessFrameTimeMS() / 1000.0);
			
		}else if(param.equals("exposureTime")){
			return String.format("%f", (config.exposureMS * 1000000L + config.exposureNS) / 1e9);
			
		}else if(param.equals("frameTime")){
			return String.format("%f", bestGuessFrameTimeMS() / 1000.0);
			
		}
		
		return null;
	}
}
