package imageProc.sources.capture.pylon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.picam.PicamConfig.Parameter;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Configuration for Pylon SDK cameras
 *  
 */
public class PylonConfig extends CaptureConfig {

	/** General stuff that's outside of the camera's own config */
	
	/** Number of images to allocate and to capture 
	 * if in non-continuous mode */	 
	public int nImagesToAllocate = 100;

	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;
	
	public int payloadSize;
	
	public int exposureTimeUS = 10000;
		
	public boolean continuous = false;
	
	/** Frame time, current just delay in software before capture request */
	public int frameTimeUS = 100000;


	public String model = "??";
	
	public int imageSizeBytes(){ return payloadSize; }
	

	/** Final derived image width and height */
	public int imageWidth = -1, imageHeight = -1;
	
	public double exposureTimeMS() { return exposureTimeUS / 1000; }
	
	/** Creates a simple HashMap of java number/string objects etc */
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
			
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/exposureTimeUS", exposureTimeUS);
		return map;
	}

}
