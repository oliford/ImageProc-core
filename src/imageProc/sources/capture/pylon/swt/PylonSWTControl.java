package imageProc.sources.capture.pylon.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.sources.capture.pylon.PylonConfig;
import imageProc.sources.capture.pylon.PylonSource;
import net.jafama.FastMath;

public class PylonSWTControl implements ImagePipeController {
	public static final double log2 = FastMath.log(2);
			
	private PylonSource source;
	private Group swtGroup;
	
	private Label sourceStatusLabel;
	private Label captureStatusLabel;
	private Label ccdInfoLabel;
	private Label timingInfoLabel;
			
	private Button blackLevelButton;
	private Button diskMappedCheckbox;
	private Button allowDemoCamera;
	private Spinner numImagesSpinner;

	private SWTSettingsControl settingsCtrl; 
	
	private Button openButton;
	private Button closeButton;
	private Button syncConfigButton;
	private Button softStartEnableCheckbox;
	private Button releaseMemoryButton;
	private Button abortTrigEnableCheckbox;
	
	private Button abortButton;
	private Button startCaptureButton;
	private Button enableAcquireCheckbox;
	
	private CTabFolder swtTabFoler;	
	private CTabItem swtSimpleTab;
	private CTabItem swtFullTab;
	private CTabItem swtROIsTab;
	
	private SimpleSettingsPanel simplePanel;
		
	public PylonSWTControl(Composite parent, int style, PylonSource source) {
		this.source = source;
				swtGroup = new Group(parent, style);
		swtGroup.setText("Pylon Control");
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("Source status:");
		sourceStatusLabel = new Label(swtGroup, SWT.NONE);
		sourceStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capture status:");
		captureStatusLabel = new Label(swtGroup, SWT.NONE);
		captureStatusLabel.setText("init\ninit");
		captureStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCI = new Label(swtGroup, SWT.NONE); lCI.setText("Camera:");
		ccdInfoLabel = new Label(swtGroup, SWT.NONE);
		ccdInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lTI = new Label(swtGroup, SWT.NONE); lTI.setText("Times:");
		timingInfoLabel = new Label(swtGroup, SWT.NONE);
		timingInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lNI = new Label(swtGroup, SWT.NONE); lNI.setText("Images:");
		numImagesSpinner = new Spinner(swtGroup, SWT.NONE);
		numImagesSpinner.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		numImagesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		numImagesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		blackLevelButton = new Button(swtGroup, SWT.CHECK);
		blackLevelButton.setText("Keep frame 0 as BG in continuous mode");
		blackLevelButton.setToolTipText("When active, continuous mode will loop to frame 1, leaving frame 0, which can be used as a background frame");
		blackLevelButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		blackLevelButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		diskMappedCheckbox = new Button(swtGroup, SWT.CHECK);
		diskMappedCheckbox.setText("Disk Memory");
		diskMappedCheckbox.setToolTipText("If activated, the SDK simulated camera is used  when no physical camera can be found");
		diskMappedCheckbox.setEnabled(true);
		diskMappedCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { diskMappedCheckboxEvent(event); } });
		diskMappedCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		allowDemoCamera = new Button(swtGroup, SWT.CHECK);
		allowDemoCamera.setText("Allow Simulated camera");
		allowDemoCamera.setToolTipText("If activated, the SDK simulated camera is used  when no physical camera can be found");
		allowDemoCamera.setEnabled(true);
		allowDemoCamera.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		allowDemoCamera.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		swtTabFoler = new CTabFolder(swtGroup, SWT.BORDER);
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 5, 1));
		
		simplePanel = new SimpleSettingsPanel(swtTabFoler, SWT.NONE, source);
		swtSimpleTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSimpleTab.setControl(simplePanel.getSWTGroup());
		swtSimpleTab.setText("Simple");
        		
		swtTabFoler.setSelection(swtSimpleTab);
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, source);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		openButton = new Button(swtGroup, SWT.PUSH);
		openButton.setText("Open Camera");
		openButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		openButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { openCameraButtonEvent(event); } });

		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close Camera");
		closeButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeCameraButtonEvent(event); } });

		syncConfigButton = new Button(swtGroup, SWT.PUSH);
		syncConfigButton.setText("Apply config");
		syncConfigButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		syncConfigButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { syncConfigButtonEvent(event); } });

		releaseMemoryButton = new Button(swtGroup, SWT.PUSH);
		releaseMemoryButton.setText("Release Mem");
		releaseMemoryButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		releaseMemoryButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { releaseMemoryButtonEvent(event); } });
		
		softStartEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		softStartEnableCheckbox.setText("S/W Trigger");
		softStartEnableCheckbox.setToolTipText("When selected, camera will start acquiring if the global start event is broadcast (i.e. the Start All button)");
		softStartEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		softStartEnableCheckbox.setSelection(false);
		softStartEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		abortButton = new Button(swtGroup, SWT.PUSH);
		abortButton.setText("Abort");
		abortButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		abortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortButtonEvent(event); } });
	
		startCaptureButton = new Button(swtGroup, SWT.PUSH);
		startCaptureButton.setText("Capture");
		startCaptureButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		startCaptureButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startCaptureButtonEvent(event); } });
		
		enableAcquireCheckbox = new Button(swtGroup, SWT.CHECK);
		enableAcquireCheckbox.setText("Acquire");
		enableAcquireCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		enableAcquireCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { enableAcquireCheckboxEvent(event); } });
		
		abortTrigEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		abortTrigEnableCheckbox.setText("Trigger end abort");
		abortTrigEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		abortTrigEnableCheckbox.setSelection(false);
		abortTrigEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		generalControllerUpdate();
		configToGui();
		simplePanel.configToGUI();
	}
	
	public void guiToConfig(){
		PylonConfig config = source.getConfig();
		
		config.nImagesToAllocate = numImagesSpinner.getSelection();
		config.blackLevelFirstFrame = blackLevelButton.getSelection();
		config.beginCaptureOnStartEvent = softStartEnableCheckbox.getSelection();
		config.abortEventOnCaptureComplete = abortTrigEnableCheckbox.getSelection();
	}
		
	public void configToGui(){
		PylonConfig config = source.getConfig();
		
		numImagesSpinner.setSelection(config.nImagesToAllocate);
		blackLevelButton.setSelection(config.blackLevelFirstFrame);
		enableAcquireCheckbox.setSelection(source.isAcquireEnabled());
		softStartEnableCheckbox.setSelection(config.beginCaptureOnStartEvent);
		abortTrigEnableCheckbox.setSelection(config.abortEventOnCaptureComplete);
	}

	private void diskMappedCheckboxEvent(Event e){
		source.setDiskMemoryLimit(diskMappedCheckbox.getSelection() ? Long.MIN_VALUE : Long.MAX_VALUE);
	}
	
	private void openCameraButtonEvent(Event event){		
		source.openCamera();
	}
	
	private void closeCameraButtonEvent(Event event){		
		source.closeCamera();
	}
	
	private void syncConfigButtonEvent(Event event){		
		source.syncConfig();
	}
			
	private void startCaptureButtonEvent(Event event){		
		source.startCapture();
	}

	private void enableAcquireCheckboxEvent(Event event){		
		source.enableAcquire(enableAcquireCheckbox.getSelection());
	}

	private void abortButtonEvent(Event event){
		source.abort();
	}

	private void releaseMemoryButtonEvent(Event event){
		source.releaseMemory();
	}
		
	public ImgSource getSource() { return source;	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}
	
	private void doUpdate() {
		if(swtGroup.isDisposed())
				return;
		
		settingsCtrl.doUpdate();
	
		sourceStatusLabel.setText(source.getSourceStatus());
		captureStatusLabel.setText(source.getCaptureStatus());
		captureStatusLabel.setToolTipText(source.getCaptureStatus());
		ccdInfoLabel.setText(source.getCCDInfo());
				
		PylonConfig config = source.getConfig();
		
		numImagesSpinner.setSelection(config.nImagesToAllocate);
		
		DecimalFormat timeFmt = new DecimalFormat("#.###");
		//double frameTimeMS = config.frameTimeMS();
		double expTimeMS = config.exposureTimeMS();

		/*double readoutTimeMS = ((Number)config.getFeature("ReadoutTimeCalculation").value).doubleValue();
		double runTime = config.isContinuous() ? Double.POSITIVE_INFINITY : (frameTimeMS * config.nImagesToAllocate / 1000.0);
		timingInfoLabel.setText("Exp=" + timeFmt.format(expTimeMS) + 
								"ms, Readout = " + timeFmt.format(readoutTimeMS) + 
								"ms, Frame = " + timeFmt.format(frameTimeMS) +
								"ms, Run = " + timeFmt.format(runTime) + 
								"s @ " + timeFmt.format(config.doubleFeature("FrameRateCalculation")) + " Hz");
		*/						
		boolean active = source.isBusy();
		
		startCaptureButton.setEnabled(!active);

		configToGui();
		simplePanel.configToGUI();		
	}
	
	@Override
	public void destroy() {
		source.controllerDestroyed(this);
		swtGroup.dispose();
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return source; }

	public void forceImageRedraw() {		
		// force update of GUI windows connected to the image source
		for (ImgSink sink : source.getConnectedSinks()) {
			if (sink instanceof ImagePanel) {
				((ImagePanel) sink).redraw();
			}
		}
	}

}
