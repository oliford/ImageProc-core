package imageProc.sources.capture.pylon.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;


import imageProc.core.ImgSource;
import imageProc.sources.capture.pylon.PylonConfig;
import imageProc.sources.capture.pylon.PylonSource;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private PylonSource source;
	private Composite swtGroup;
				
	private Button continuousButton;
	
	private Text exposureLengthTextbox;
	//private Button exposureMinButton; 
	private Button exposureMaxButton;

	private Text frameTimeTextbox;

	private Button unsetAllButton; 
	private Button defaultAllButton; 
	
	public SimpleSettingsPanel(Composite parent, int style, PylonSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure Time / ms:");
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("10");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		/*exposureMinButton = new Button(swtGroup, SWT.PUSH);
		exposureMinButton.setText("Min");
		exposureMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		//exposureMinButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(false); } });
		*/
		exposureMaxButton = new Button(swtGroup, SWT.PUSH);
		exposureMaxButton.setText("Max");
		exposureMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		exposureMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(true); } });
		
		Label lFT = new Label(swtGroup, SWT.NONE); lFT.setText("Frame Time / ms:");
		frameTimeTextbox = new Text(swtGroup, SWT.NONE);
		frameTimeTextbox.setText("10");
		frameTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		frameTimeTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
		lB.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("All parameters:");
		unsetAllButton = new Button(swtGroup, SWT.PUSH);
		unsetAllButton.setText("Unset/Read");
		unsetAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		//unsetAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(false); } });
		
		defaultAllButton = new Button(swtGroup, SWT.PUSH);
		defaultAllButton.setText("Set default");
		defaultAllButton.setEnabled(false);
		defaultAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		//defaultAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(true); } });
		
		configToGUI();
	}

	void configToGUI(){
		PylonConfig config = source.getConfig();
	

		continuousButton.setSelection(config.continuous);
		
		exposureLengthTextbox.setText(Integer.toString(config.exposureTimeUS));
		frameTimeTextbox.setText(Integer.toString(config.frameTimeUS));
	}
	
	void guiToConfig(){
		PylonConfig config = source.getConfig();
				
		config.continuous = continuousButton.getSelection();
		
		config.exposureTimeUS = Algorithms.mustParseInt(exposureLengthTextbox.getText());
		config.frameTimeUS = Algorithms.mustParseInt(frameTimeTextbox.getText());
		
	}
	
	public void exposureMinMaxEvent(boolean max){
		PylonConfig config = source.getConfig();
		
		if(max){
		//	config.exposureTime = config.maximumExposureTime;
		}else{
			// ??
		}
		configToGUI();
	}

	
	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
