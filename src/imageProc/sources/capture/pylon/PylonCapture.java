package imageProc.sources.capture.pylon;

import java.nio.ByteOrder;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import otherSupport.SettingsManager;
import pylonJNI.Pylon;
import pylonJNI.PylonSDKException;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;



/** Thread runner used to copy in images.
 *  
 * It contains all the actual JNI calls to the camera library 
 * 
 * I think the SDK is thread safe but not entirely sure.
 */
public class PylonCapture extends Capture implements Runnable {
		
	private PylonSource source;
		
	private ByteBufferImage images[];
	
	private PylonConfig cfg;
		
	private Thread thread;
	
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;
		
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
		
	/** Completely reset the configuration to defaults */
	private boolean resetConfig = false;
	
	private long wallCopyTimeMS0 = Long.MIN_VALUE;
	private long wallCopyTimeMS[];
	
	private long nanoTimeStart = Long.MIN_VALUE;
		
	private long nanoTime[];
	private double time[];
	
	private String[] availableCameras;
	
	public PylonCapture(PylonSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera 
	}
		
	@Override
	public void run() {
		threadBusy = true;
		try{
			if(cameraOpen){
				Pylon.deinit();
				throw new RuntimeException("Camera driver already open, aborting both");
			}
			
			setStatus(Status.init, "Capture init");
			initDevice();
			
			signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Init Ready");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();					
					}
					
					if(!signalCapture){
						setStatus(Status.completeOK, "Config done.");						
						
					}else{
						signalCapture = false;
									
						setStatus(Status.init, "Allocating");		
						
						initImages();
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("Pylon/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");					
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal: " + err, err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			logr.log(Level.SEVERE, "Pylon thread caught Exception:" + err, err);
			setStatus(Status.errored, "Aborted/Errored:" + err);
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				System.err.println("Pylon thread caught exception de-initing camera.");
				err.printStackTrace();
			}
		}
	}
	
	
	private void initDevice() {
		setStatus(Status.init, "Initialising library... (May take a few minutes)");
		Pylon.init();
		
		/*Pylon.Initialize(SettingsManager.defaultGlobal().getPathProperty("andorV2.andorConfigPath", "/usr/local/etc/andor"));
		
		//get list of camera serial numbers
		int nCameras = AndorV2.GetAvailableCameras();
		
		logr.info("Enumerating "+nCameras+" cameras (or until found selected camera)");

		int selectedCameraHandle = Integer.MIN_VALUE;	
		availableCameras = new String[nCameras];
		
		for(int i=0; i < nCameras; i++){
			int cameraHandle = AndorV2.GetCameraHandle(i);
			AndorV2.SetCurrentCamera(cameraHandle);
			
			int serial = AndorV2.GetCameraSerialNumber();
			String model = AndorV2.GetHeadModel();
			availableCameras[i] = model + ":" + serial;
					
			logr.info("Camera "+i+":" + ", Head model=" + model + "S/N=" + serial);  
			
			if(cfg.selectSerialNumber == -1 || cfg.selectSerialNumber == serial){
				selectedCameraHandle = cameraHandle;
				cfg.cameraIndex = i;
				cfg.cameraSerialNumber = serial;
				cfg.headModel = model;
			}
		}
		
		if(selectedCameraHandle == Integer.MIN_VALUE){
			AndorV2.ShutDown();
			throw new AndorV2Exception("Could not find camera with serial '"+cfg.selectSerialNumber+"'");
		}
		
		//get info
		cfg.capabilities = AndorV2.GetCapabilities();		
					    
		logr.info(".initCamera(): Opened camera: Type = " + cfg.capabilities.getCameraTypeName() 
					+ ", Model = " + cfg.headModel 
					+ ", S/N=" + cfg.cameraSerialNumber
					+ ", " + cfg.capabilities);
		*/
		cameraOpen = true;				
		
	}
	
	private void syncConfig(){
		
		if(cfg.continuous && cfg.nImagesToAllocate <= 1){
			throw new IllegalArgumentException("nImage=1 and contiuous is on. This is a ridiculous thing to ask for!");
		}
		
		/*if(cfg.imageWidth < 0){ //may need to initialise image size on default config
			int iRet[] = AndorV2.GetDetector();
			cfg.detectorWidth = iRet[0]; cfg.detectorHeight = iRet[1];

			cfg.imageWidth = cfg.detectorWidth;
			cfg.imageHeight = cfg.detectorHeight;

			//default 'image' size = full chip
			cfg.imageModeHBin = 1;
			cfg.imageModeVBin = 1;
			cfg.imageModeHStart = 1;
			cfg.imageModeVStart = 1;
			cfg.imageModeHEnd = cfg.imageWidth;
			cfg.imageModeVEnd = cfg.imageHeight;

		}
		
		//ensure consistency of number of images
		cfg.nKineticScans = cfg.nImagesToAllocate;
*/
		writeConfig();

		readConfig();
		
		
		
		source.configChanged();
		logr.info(".syncConfig(): config sync done");
	}
		
	private void writeConfig(){
		Pylon.setExposureTime(cfg.exposureTimeUS);
		/*
		AndorV2.SetReadMode(cfg.readMode);
		AndorV2.SetAcquisitionMode(cfg.acqusitionMode);
		AndorV2.SetOutputAmplifier(cfg.outputAmplifier);		
		AndorV2.SetVSSpeed(cfg.vertShiftSpeedIndex);
		AndorV2.SetHSSpeed(0, cfg.horizShiftSpeedIndex);		
		AndorV2.SetExposureTime(cfg.exposureTime);
		AndorV2.SetPreAmpGain(cfg.preAmpGainIndex);
		AndorV2.SetTemperature(cfg.targetTemp);
		if(cfg.coolerOn)
			AndorV2.CoolerON();
		else
			AndorV2.CoolerOFF();
		
		AndorV2.SetShutter(cfg.shutterType, cfg.shutterMode, cfg.shutterClosingTime, cfg.shutterOpeningTime);
		AndorV2.SetMetaData(cfg.enableMetaData);
		AndorV2.SetFrameTransferMode(cfg.frameTransferMode);
		AndorV2.SetTriggerMode(cfg.triggerMode);
		
		if(cfg.readMode == AndorV2Defs.READMODE_RANDOM_TRACK){
			AndorV2.SetComplexImage(cfg.rois.toAreasArray());
		}else if(cfg.readMode == AndorV2Defs.READMODE_IMAGE){
			AndorV2.SetImage(cfg.imageModeHBin, cfg.imageModeVBin, cfg.imageModeHStart, cfg.imageModeHEnd, cfg.imageModeVStart, cfg.imageModeVEnd);
		}else{
			throw new RuntimeException("Read mode not yet implemented.");
		}
		
		
		AndorV2.SetNumberAccumulations(cfg.nAccumulations);
		if(cfg.acqusitionMode == AndorV2Defs.ACQUSITIONMODE_ACCUMULATE ||
				cfg.acqusitionMode == AndorV2Defs.ACQUSITIONMODE_KINETICS)
			AndorV2.SetAccumulationCycleTime(cfg.accumulationCycleTime);
		
		if(cfg.acqusitionMode == AndorV2Defs.ACQUSITIONMODE_KINETICS ||
				cfg.acqusitionMode == AndorV2Defs.ACQUSITIONMODE_FAST_KINETICS )
			AndorV2.SetNumberKinetics(cfg.nKineticScans);
		AndorV2.SetKineticCycleTime(cfg.kineticCycleTime);
		
		cfg.imageWidth = cfg.imageModeHEnd - cfg.imageModeHStart + 1;
		cfg.imageHeight = cfg.imageModeVEnd - cfg.imageModeVStart + 1;
		*/
	}
	
	private void readConfig(){
		/*
		int iRet[] = AndorV2.GetDetector();
		cfg.detectorWidth = iRet[0]; cfg.detectorHeight = iRet[1];

		float fRet[] = AndorV2.GetAcquisitionTimings();
		cfg.exposureTime = fRet[0];
		cfg.accumulationCycleTime = fRet[1];
		cfg.kineticCycleTime = fRet[2];
		
		cfg.horizShiftSpeeds = AndorV2.getHSSpeeds(0, 0);
		cfg.vertShiftSpeeds = AndorV2.getVSSpeeds();
		cfg.preAmpGains = AndorV2.getPreAmpGains();
		
		iRet = AndorV2.GetTemperatureRange();
		cfg.minTemp = iRet[0];
		cfg.maxTemp = iRet[1];
		
		fRet = AndorV2.GetTemperatureF();
		cfg.currentTemp = fRet[0];
		cfg.tempControlStatus = (int)fRet[1];
		
		cfg.maximumExposureTime = AndorV2.GetMaximumExposure();
		*/
	}
		
	private void initImages(){
			
		
		int bitDepth = 16;// cfg.capabilities.getBitDepth();
		cfg.imageWidth = 2048;
		cfg.imageHeight = 2048;
		cfg.payloadSize = Pylon.getPayloadSize();
		long frameSize = cfg.payloadSize;
		
		if(cfg.imageWidth * cfg.imageHeight * (bitDepth/8) != cfg.payloadSize)
			throw new RuntimeException("Size mismatches");
		
		
		System.out.println("Frame size = " + (frameSize/(bitDepth/8)) + " pixels");
				
		//handle ROIs
			
		int footerSize = 0;//(int)(imgDataSize - (width * height * bytesPerPixel));
		
		if(frameSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
			
		try{
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, cfg.imageWidth, cfg.imageHeight, bitDepth, 0, (int)footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
		
	}
	
	private void doCapture() {
		
		/*int status = AndorV2.getStatus();
		if(status != AndorV2Exception.DRV_IDLE){
			throw new RuntimeException("Acquisition already running, or error.", new AndorV2Exception(status));
		}*/
		
		setStatus(Status.awaitingSoftwareTrigger, "Starting Aquisition");		
		
		try{

			for(int i=0; i < images.length; i++)
				images[i].invalidate(false);
			
			time = new double[cfg.nImagesToAllocate]; //seconds since first image
			wallCopyTimeMS = new long[cfg.nImagesToAllocate];
			nanoTime = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("Pylon/wallCopyTimeMS", wallCopyTimeMS, true);
			//source.setSeriesMetaData("Pylon/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("Pylon/nanoTime", nanoTime, true);
			source.setSeriesMetaData("Pylon/time", time, true);
			
			wallCopyTimeMS0 = Long.MIN_VALUE;
			nanoTimeStart = Long.MIN_VALUE;
			
			//wait for acuisition start signal
			while(!signalAcquireStart){
				try{ Thread.sleep(0, 100000); }catch(InterruptedException err){ }
				
				if(signalDeath || signalAbort){
					logr.info("Aborting capture loop.");
					throw new CaptureAbortedException();
				}
				/*status = AndorV2.getStatus();
				if(status != AndorV2Exception.DRV_IDLE){
					throw new RuntimeException("Acquisition already running, or error.", new AndorV2Exception(status));
				}*/
				
			}
			
			//AndorV2.StartAcquisition();
			//wait for it to be not idle
			/*while(status == AndorV2Exception.DRV_IDLE){
				status = AndorV2.getStatus();
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) { }
			}
				
				*/		
			setStatus(Status.awaitingHardwareTrigger, "Capturing, Acquisition started");

			int nextImgIdx = 0;
			int frameTimeMS = cfg.frameTimeUS / 1000;
			long lastCaptureTime = Long.MIN_VALUE;
			do{ //loop until all images captured, or forever if continuous

				

				/*status = AndorV2.getStatus();
				if(status != AndorV2Exception.DRV_ACQUIRING){
					throw new RuntimeException("Acquisition stopped or failed.", new AndorV2Exception(status));
				}

				try{
					AndorV2.WaitForAcquisitionTimeOut(cfg.grabberTimeoutMS);					
				}catch(AndorV2Exception err){
					if(err.getErrorCode() == AndorV2Exception.DRV_NO_NEW_DATA)
						continue;
					else
						throw(err);
				}*/
				
				while(System.currentTimeMillis() < (lastCaptureTime + frameTimeMS)){
					if(signalDeath || signalAbort){
						logr.info("Aborting capture loop.");
						throw new CaptureAbortedException();
					}
					
					try{
						Thread.sleep(1);
					}catch(InterruptedException err){ 
						logr.info("Interrupted while waiting for frame time.");
					}
					
				}

				int nImagesTotal;
				try {
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try{
						images[nextImgIdx].invalidate(false);

						//nImagesTotal = AndorV2.GetTotalNumberImagesAcquired();
						//AndorV2.GetMostRecentImage16(images[nextImgIdx].getWritableBuffer(writeLock));
						//AndorV2.GetOldestImage16(images[nextImgIdx].getWritableBuffer(writeLock));
						Pylon.captureImage(images[nextImgIdx].getWritableBuffer(writeLock));
						

					}finally{ writeLock.unlock(); }
				} catch (InterruptedException e) {
					logr.info("Interrupted while waiting to write to java image.");
					return;
				}
				lastCaptureTime = System.currentTimeMillis();
				
				spinLog("Got image " + nextImgIdx);// + ", total = " + nImagesTotal);
				
				processTime(nextImgIdx);
				
				source.imageCaptured(nextImgIdx);

				nextImgIdx++;
				if(nextImgIdx >= cfg.nImagesToAllocate && cfg.continuous)
					nextImgIdx = cfg.blackLevelFirstFrame ? 1 : 0;


			}while(nextImgIdx < cfg.nImagesToAllocate);		
		
			logr.info("Acquisition complete.");
			setStatus(Status.completeOK, "Acquisition complete.");
			
			/*status = AndorV2.getStatus();
			if(status == AndorV2Exception.DRV_ACQUIRING){
				throw new RuntimeException("Acquisition still running.", new AndorV2Exception(status));
			}*/
		}finally{
			/*status = AndorV2.getStatus();
			if(status != AndorV2Exception.DRV_IDLE)				
				AndorV2.AbortAcquisition();
				*/				
		}

	}
	
	private void processTime(int idx){
		//catch up with the acqusition, this should really only ever be 1 image
	
		nanoTime[idx] = Long.MIN_VALUE;
		/*
		if(cfg.enableMetaData){
			
			if(nanoTimeStart == Long.MIN_VALUE){
				
				short startTimeData[] = AndorV2.GetMetaDataInfoStartTime();

				short wYear = startTimeData[0];				
				short wMonth = startTimeData[1];
				short wDayOfWeek = startTimeData[2];
				short wDay = startTimeData[3];
				short wHour = startTimeData[4];
				short wMinute = startTimeData[5];
				short wSecond = startTimeData[6];
				short wMilliseconds = startTimeData[7];
				 				
				Date date = new Date(wYear, wMonth, wDay, wHour, wMinute, wSecond);
				
				nanoTimeStart = (date.getTime() + wMilliseconds) * 1000000L;
			}
			
			float frameRelTimeMS = AndorV2.GetMetaDataInfoRelativeFrameTime(idx);
			nanoTime[idx] = nanoTimeStart + (long)(frameRelTimeMS * 1e6);

			//this one's anyway relative. Assume 'startTime' from SDK is the first frame time
			time[idx] = frameRelTimeMS / 1e3;
		}
			*/	
		if(nanoTime[idx] == Long.MIN_VALUE){
			//no timestamp, so our wall copy time is the best we can do							
			time[idx] = (wallCopyTimeMS[idx] - wallCopyTimeMS0) / 1e3;
			 //half an exposure back from copy time, ignore readout time
			nanoTime[idx] = wallCopyTimeMS[idx] * 1_000_000L - (long)(cfg.exposureTimeMS() * 0.5 * 1e6);			
		}
				
		
	}
		
	private void deinit(){
		logr.info(" deInit()");
		
		try{
			//AndorV2.AbortAcquisition();
			Pylon.deinit();
		}catch(PylonSDKException err){
			err.printStackTrace();
		}
		//AndorV2.ShutDown();
		
		cameraOpen = false;
	}
			
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		logr.info("Starting AndorV2 capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(PylonConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(PylonConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thrad to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
				
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				System.out.println("AndorV2Capture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("AndorV2Capture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("AndorV2Capture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("AndorV2Capture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}

	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(PylonConfig config) { this.cfg = config; }

}
