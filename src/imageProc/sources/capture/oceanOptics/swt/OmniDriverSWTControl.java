package imageProc.sources.capture.oceanOptics.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.sources.capture.oceanOptics.OmniDriverConfig;
import imageProc.sources.capture.oceanOptics.OmniDriverSource;

public class OmniDriverSWTControl implements ImagePipeController {
	
	private Group swtGroup;

	private Label sourceStatusLabel;
	private Label captureStatusLabel;
	private Label openedSpectrometerLabel;
	private Text singleBusTextbox;
	private Text singlePortTextbox;
	
	private Combo spectrometerClassCombo;
	private Spinner deviceIndexSpinner;
	
	private Combo spectrometerCombo;
	private Button openButton;
	private Button closeButton;
		
	private Spinner numFramesSpinner;
	private Button allowAutoconfigCheckbox;
	private Combo convertTypeCombo;
	
	private Button startButton;
	private Button abortButton;
	
	private Spinner exposureTimeUS;
	private Button continuousCheckbox;	
	private Combo externalTriggerCombo;

	private Button acquireEnableCheckbox;
	private Button startTriggerCheckbox;

	private SWTSettingsControl settingsCtrl;
	
	private OmniDriverSource source;

	private boolean inhibitSet = false;

	
	public OmniDriverSWTControl(Composite parent, int style, OmniDriverSource source) {
		this.source = source;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("OmniDriver Control");
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("Source status:");
		sourceStatusLabel = new Label(swtGroup, SWT.NONE);
		sourceStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capture status:");
		captureStatusLabel = new Label(swtGroup, SWT.NONE);
		captureStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lUB = new Label(swtGroup, SWT.NONE); lUB.setText("USB:  Bus:");
		singleBusTextbox = new Text(swtGroup, SWT.NONE); 
		singleBusTextbox.setText("Any");
		singleBusTextbox.setToolTipText("Only enumerate devices on given USB bus (requires hacked libusb)");
		singleBusTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		singleBusTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lUP = new Label(swtGroup, SWT.NONE); lUP.setText("Port:");
		singlePortTextbox = new Text(swtGroup, SWT.NONE); 
		singlePortTextbox.setText("Any");
		singlePortTextbox.setToolTipText("Only enumerate devices on given USB ports of each bus (requires hacked libusb)");
		singlePortTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		singlePortTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		Label lSC = new Label(swtGroup, SWT.NONE); lSC.setText("Class:");		
		spectrometerClassCombo = new Combo(swtGroup, SWT.MULTI);
		spectrometerClassCombo.setItems(new String[]{ " -- init --" });
		spectrometerClassCombo.select(spectrometerClassCombo.getItemCount()-1);
		spectrometerClassCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		spectrometerClassCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lDI = new Label(swtGroup, SWT.NONE); lDI.setText("Index:");
		deviceIndexSpinner = new Spinner(swtGroup, SWT.NONE);
		deviceIndexSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		deviceIndexSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		deviceIndexSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		openButton = new Button(swtGroup, SWT.PUSH);
		openButton.setText("Open");
		openButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		openButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { openButtonEvent(event); } });
		
		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close");
		closeButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeButtonEvent(event); } });
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Spectrometer:");		
		spectrometerCombo = new Combo(swtGroup, SWT.MULTI);
		spectrometerCombo.setItems(new String[]{ " -- No Spectrometers Found -- " });
		spectrometerCombo.select(0);
		spectrometerCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		spectrometerCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		Label lOS = new Label(swtGroup, SWT.NONE); lOS.setText("Selected spectrometer:");
		openedSpectrometerLabel = new Label(swtGroup, SWT.NONE);
		openedSpectrometerLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lNI = new Label(swtGroup, SWT.NONE); lNI.setText("no. Frames:");
		numFramesSpinner = new Spinner(swtGroup, SWT.NONE);
		numFramesSpinner.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		numFramesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		numFramesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		allowAutoconfigCheckbox = new Button(swtGroup, SWT.CHECK);
		allowAutoconfigCheckbox.setText("Allow auto");
		allowAutoconfigCheckbox.setToolTipText("Allow autoconfiguration from other modules (e.g. from an autopilot)");
		allowAutoconfigCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		allowAutoconfigCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		continuousCheckbox = new Button(swtGroup, SWT.CHECK);
		continuousCheckbox.setText("Continuous");
		continuousCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		continuousCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure time / us:");
		exposureTimeUS = new Spinner(swtGroup, SWT.NONE);
		exposureTimeUS.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		exposureTimeUS.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		exposureTimeUS.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lXT = new Label(swtGroup, SWT.NONE); lXT.setText("External Trigger:");		
		externalTriggerCombo = new Combo(swtGroup, SWT.MULTI);
		externalTriggerCombo.setItems(OmniDriverConfig.triggerModes);
		externalTriggerCombo.select(OmniDriverConfig.TRIGGERMODE_NORMAL);
		externalTriggerCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		externalTriggerCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lCT = new Label(swtGroup, SWT.NONE); lCT.setText("Convert type:");		
		convertTypeCombo = new Combo(swtGroup, SWT.MULTI);
		convertTypeCombo.setItems(OmniDriverConfig.convertTypesNames);
		convertTypeCombo.setToolTipText("Force conversion to the given type. Otherwise it comes as a 64-bit double which sometimes has only integer values. This is slow and slows down the acquisition. Recommended to do this during saving/streaming instead.");
		convertTypeCombo.select(0);
		convertTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		convertTypeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		acquireEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		acquireEnableCheckbox.setText("Acquire");		
		acquireEnableCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		acquireEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		startTriggerCheckbox = new Button(swtGroup, SWT.CHECK);
		startTriggerCheckbox.setText("Start on S/W trigger");		
		startTriggerCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		startTriggerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		startButton = new Button(swtGroup, SWT.PUSH);
		startButton.setText("Start");
		startButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startButtonEvent(event); } });
		
		abortButton = new Button(swtGroup, SWT.PUSH);
		abortButton.setText("Abort");
		abortButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		abortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortButtonEvent(event); } });
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, source);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 6, 1));
		
		doUpdate();
		
		swtGroup.pack();
		
	}
	
	protected void abortButtonEvent(Event event) {
		source.abort();
		
	}

	protected void startButtonEvent(Event event) {
		source.startCapture();
	}

	protected void settingsChangedEvent(Event event) {
		if(inhibitSet)
			return;
		
		//do acquire enable first, might be time critical
		source.enableAcquire(acquireEnableCheckbox.getSelection());
		
		OmniDriverConfig config = source.getConfig();

		try{ config.singleUSBBus = Integer.parseInt(singleBusTextbox.getText()); }catch(NumberFormatException err) { config.singleUSBBus = -1; }
		config.singleUSBPort = singlePortTextbox.getText().equalsIgnoreCase("Any") ? null : singlePortTextbox.getText();
		
		singleBusTextbox.setText((config.singleUSBBus < 0) ? "Any" : Integer.toString(config.singleUSBBus));
		singlePortTextbox.setText((config.singleUSBPort == null) ? "Any" : config.singleUSBPort);

		config.beginCaptureOnStartEvent = startTriggerCheckbox.getSelection();
		//config.abortEventOnCaptureComplete = abortTrigEnableCheckbox.getSelection();
		
		config.nImagesToAllocate = numFramesSpinner.getSelection();
		config.enableAutoConfig = allowAutoconfigCheckbox.getSelection();
		config.integrationTimeUS = exposureTimeUS.getSelection();
		config.externalTriggerMode = externalTriggerCombo.getSelectionIndex();
		config.convertType = OmniDriverConfig.convertTypes[convertTypeCombo.getSelectionIndex()];			
		config.wrap = continuousCheckbox.getSelection();
		
		String className = spectrometerClassCombo.getText();
		
		spectrometerClassCombo.setItems(new String[]{ " -- Enumerate Spectrometers (slow) --" });
		for(String specClass : source.getSpectrometerClassList())
			spectrometerClassCombo.add(specClass);
		
		if(!className.startsWith(" --")){			
			config.spectrometerClass = className;
			config.deviceIndex = deviceIndexSpinner.getSelection();
			
		}else{
			//enumerated mode
			config.spectrometerClass = null;
		}
		
		String selected = spectrometerCombo.getText();
		config.selectSpectrometer = selected.equals(" - Any - ") ? null : selected;
		
		source.configChanged();
	}

	protected void closeButtonEvent(Event event) {
		source.close();
	}

	protected void openButtonEvent(Event event) {
		source.openLibrary();
	}
	

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}
	
	private void doUpdate() {
		if(swtGroup.isDisposed())
				return;
		inhibitSet = true;
		try{
			settingsCtrl.doUpdate();
			
			sourceStatusLabel.setText(source.getSourceStatus());
			captureStatusLabel.setText(source.getCaptureStatus());
			captureStatusLabel.setToolTipText(source.getCaptureStatus());

			OmniDriverConfig cfg = source.getConfig();
			
			singleBusTextbox.setText((cfg.singleUSBBus < 0) ? "Any" : Integer.toString(cfg.singleUSBBus));
			singleBusTextbox.setEnabled(false);
			singlePortTextbox.setText((cfg.singleUSBPort == null) ? "Any" : cfg.singleUSBPort);
			singlePortTextbox.setEnabled(false);
			
			if(cfg.spectrometerClass != null){
				spectrometerClassCombo.setText(cfg.spectrometerClass);
			}else{
				spectrometerClassCombo.select(0);
			}
			deviceIndexSpinner.setSelection(cfg.deviceIndex);
			
			if(!numFramesSpinner.isFocusControl())numFramesSpinner.setSelection(cfg.nImagesToAllocate);
			if(!exposureTimeUS.isFocusControl())exposureTimeUS.setSelection(cfg.integrationTimeUS);
			allowAutoconfigCheckbox.setSelection(cfg.enableAutoConfig);
			continuousCheckbox.setSelection(cfg.wrap);
			externalTriggerCombo.select(cfg.externalTriggerMode);
			convertTypeCombo.select(0);
			for(int i=0; i < OmniDriverConfig.convertTypes.length; i++)
				if(cfg.convertType == OmniDriverConfig.convertTypes[i])
					convertTypeCombo.select(i);
			
			acquireEnableCheckbox.setSelection(source.isAcquireEnabled());
			startTriggerCheckbox.setSelection(cfg.beginCaptureOnStartEvent);
			//abortTrigEnableCheckbox.setSelection(cfg.abortEventOnCaptureComplete);
			
			String specList[] = source.getSpectrometerList();
			spectrometerCombo.setItems(new String[] { " - Any - " });
			
			if(specList != null){
				for(String s : specList){
					spectrometerCombo.add(s);
				}
			}
			
			if(cfg.selectSpectrometer == null) {
				spectrometerCombo.select(0);
			}else {
				spectrometerCombo.setText(cfg.selectSpectrometer);
			}
			
			openedSpectrometerLabel.setText("Selected spectrometer S/N: " + (cfg.serialNumber == null ? "null" : cfg.serialNumber));
			
		}finally{
			inhibitSet = false;
		}
	}

	@Override
	public void destroy() {
		source.controllerDestroyed(this);
		swtGroup.dispose();		
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return source; }
	
	
}
