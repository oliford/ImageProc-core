package imageProc.sources.capture.oceanOptics;

import com.oceanoptics.omnidriver.api.wrapper.Wrapper;
import com.oceanoptics.omnidriver.constants.USBProductInfo;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class TestOmniDriver {
	public static void main(String[] args) {
	
		System.out.println(System.getProperty("os.name"));
		//System.exit(0);
		try{
			
			Wrapper wrapper = new Wrapper();
			int n = wrapper.openAllSpectrometers();
		
			System.out.println(n);
			
			System.out.println(wrapper.getName(0));
			
			double d[] = wrapper.getSpectrum(0);
			Mat.dumpArray(d);
			
			wrapper.closeAllSpectrometers();
			
			
			
		}catch(Exception err){
			err.printStackTrace();
		}
	}
}

