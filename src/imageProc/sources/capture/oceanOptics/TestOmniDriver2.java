package imageProc.sources.capture.oceanOptics;

import java.lang.reflect.Field;
import java.util.ArrayList;

import com.oceanoptics.omnidriver.api.wrapper.Wrapper;
import com.oceanoptics.omnidriver.constants.USBProductInfo;
import com.oceanoptics.omnidriver.spectrometer.SpectrometerAssembly;
import com.oceanoptics.omnidriver.spectrometer.blazer.BlazeUSB;
import com.oceanoptics.omnidriver.spectrometer.flameur.FlameU;
import com.oceanoptics.omnidriver.spectrometer.flamex.FlameXUSB;
import com.oceanoptics.omnidriver.spectrometer.hr4000.HR4000;
import com.oceanoptics.omnidriver.spectrometer.sts.STS;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class TestOmniDriver2 {
		
	public static void main(String[] args) {
		System.loadLibrary("NatUSB");
		System.out.println(System.getProperty("os.name"));
		//System.exit(0);
		try{
			
			Wrapper wrapper = new Wrapper();
			
			//HR4000 spec = new HR4000(1);
			//STS spec = new STS(4);
			//FlameXUSB spec = new FlameXUSB(5);
			//FlameU spec = new FlameU(4);
			BlazeUSB spec = new BlazeUSB(1);

			//spec = new HR4000();
			
			//minimalistic init of library wrapper - just init that specific spectrometer
			// the proper public way is to call openAllSpectrometers(), but it is horrifically slow and 
			// then we can't use the others from other applications
			Field f = Wrapper.class.getDeclaredField("spectrometerAssemblyCollection"); //NoSuchFieldException
			f.setAccessible(true);
			ArrayList spectrometerAssemblyCollection = (ArrayList)f.get(wrapper); //IllegalAccessException
			spectrometerAssemblyCollection.add(new SpectrometerAssembly(spec));
			
			
			int n = wrapper.openAllSpectrometers();
			System.out.println(n + " spectrometers found");
			if(n == 0)
				return;
			//wrapper.setUSBTimeout(0, 1000);
			wrapper.setIntegrationTime(0, 100000);
			wrapper.setExternalTriggerMode(0, 0);
			
			for(int i=0;i < 10; i++) {
				double d[] = wrapper.getSpectrum(0);
				Mat.dumpArray(d);
			}
			
			wrapper.closeAllSpectrometers();
			
		}catch(Exception err){
			err.printStackTrace();
		}
	}
}
