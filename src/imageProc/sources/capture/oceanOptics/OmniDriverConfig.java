package imageProc.sources.capture.oceanOptics;

import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;

public class OmniDriverConfig extends CaptureConfig {


	/** Only enumerate devices on given USB bus (requires hacked libusb) */
	public int singleUSBBus = -1;
	
	/** Only enumerate devices on given USB port of each bus (requires hacked libusb) */
	public String singleUSBPort = null;

	
	/** Number of images in full buffer */
	public int nImagesToAllocate = 500;
	
	/** Open only the given spectrometer device using the library hack.
	 * * 
	 * Null = open all spectrometers.
	 * 
	 * If the device is specified here explicity, it avoids the long wait while
	 * the NatUSB library tries to search for a million non-existent USB devices.
	 * 
	 * It also allows other program instances to open other spectrometers.
	 * 
	 *  Full canonical classname.
	 */
	public String spectrometerClass = null;
	/** Device index of a given spectrometer class.
	 * -1 to search, in which case deviceIndexUsed is set to the one actually used */
	public int deviceIndex = -1;
	
	/** 'device index' actually used. */ 
	public int deviceIndexUsed = -1;
	public int deviceIndexSearchMax = 10; // never seems to go above this
	
	/** Various details of spectrometer used for capture*/
	public String apiVersion;
	public String firmwareModel;
	public String firmwareVersion;
	public int buildNumber;
	public String name;
	
	/** if not-null, select this spectrometer */
	public String selectSpectrometer;
	
	public String serialNumber;
	
	/** Only open the spectrometer with this serieal number */
	public String acceptSerialNumber;
		
	/** Integration time in US */
	public int integrationTimeUS = 100000;

	/** This parameter controls whether a light source is turned on or off. Ocean Optics offers a number of light
sources that can be controlled by your OmniDriver application. These light sources attach directly to an
electrical connector on your spectrometer. You can then call the setStrobeEnable() function to turn
the light source on and off. */
	public boolean strobeEnable = false;
	
	/** When auto-toggling is enabled (and strobeEnable() is set to true), the lamp will be
automatically turned on for the duration of each spectrum acquisition, and then turned off until the next
spectrum is acquired */
	public boolean strobeAutoToggle = true;
	
	/** Boxcar smoothing “smoothes” the spectrum pixel values (i.e., reduces noise) by averaging a range of
	 * contiguous pixels together. Each pixel in the spectrum is averaged with n pixels on either side of it. */
	public int boxcarWidth = 0;
	
	/** All Ocean Optics spectrometers are calibrated at the factory to maximize accuracy. One of the
	 * calibrations performed is to correct for detector nonlinearity. ... */
	public boolean correctDetectorNonlinearity = true;
	
	public boolean correctDarkCurrent = true;
	
	public int nScansToAverage = 0;
	
	/** Free running */
	public static final int TRIGGERMODE_NORMAL = 0;
	/** Free running when trigger is high. */
	
	public static final int TRIGGERMODE_GATE = 1;
	/** In this mode, spectra acquisition is initiated by an external synchronizing TTL trigger signal. The
	 * 	purpose of this mode is to allow multiple spectrometers to be synchronized in terms of the start of their
	 * 	acquisition period, and the duration of the integration period. The integration time is determined based on
	 * 	an average of the length of time between the input trigger signal pulses. See your spectrometer’s
	 * 	documentation for the allowed frequency range for this input signal. */
	
	public static final int TRIGGERMODE_SYNC = 2;

	/** External trigger starts each frame.
	 * NOTE: Once you put the spectrometer in this mode, it is impossible to programmatically return
	 * 	to normal Hardware Trigger mode. You must power-cycle the spectrometer by
	 * 	unplugging it from your computer. 
	 */
	public static final int TRIGGERMODE_HARDWARE = 3;
	
	/** Erm... I've no idea what this does. Read the manual! */
	public static final int TRIGGERMODE_SINGLE_SHOT = 4;
	
	public static final String triggerModes[] = { "Free running", "Gated free running", "Syncronised", "Hardware triggered (forever!)", "Single Shot" };
	
	public static final Class[] convertTypes = { null, Byte.class, Short.class, Integer.class };
	public static final String[] convertTypesNames = { "None", "Byte", "Short", "Integer" };

	/** External trigger mode. One of TRIGGERMODE_xxx */
	public int externalTriggerMode = TRIGGERMODE_NORMAL;

	/** Number of pixels  = image width*/
	public int nPixelsTotal;

	/** Number of channels = image height */
	public int nChannels;

	/** Number of dark pixels */
	public int nPixelsDark;
	
	/** USB Read timeout, needs to be used when waiting for external triggers */
	public int readTimeoutMS = 500; 	

	/** continuous acquisition - wrap back to image 0 */
	public boolean wrap;
	
	/** If not null, convert the double value to the given type as acqusition time */  
	public Class convertType = null;

	public class GPIOState {
		/** GPIO pin index */ 
		int index;
		/** 0=in, 1=out */
		boolean output;
		/** Value retrieved, or should be set */
		int value;
		/** Time that this GPIO state was last updates */
		long lastUpdateMS;
	}
	public GPIOState[] gpios = null;

	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);		
		map.put(prefix + "/selectSpectrometer", selectSpectrometer);
		map.put(prefix + "/spectrometerClass", spectrometerClass);
		map.put(prefix + "/deviceIndex", deviceIndex);		
		map.put(prefix + "/deviceIndexUsed", deviceIndexUsed);	
		map.put(prefix + "/apiVersion", apiVersion);
		map.put(prefix + "/firmwareModel", firmwareModel);
		map.put(prefix + "/firmwareVersion", firmwareVersion);
		map.put(prefix + "/buildNumber", buildNumber);
		map.put(prefix + "/name", name);
		map.put(prefix + "/serialNumber", serialNumber);
		map.put(prefix + "/acceptSerialNumber", acceptSerialNumber);		
		map.put(prefix + "/integrationTimeUS", integrationTimeUS);
		map.put(prefix + "/strobeEnable", strobeEnable);
		map.put(prefix + "/strobeAutoToggle", strobeAutoToggle);
		map.put(prefix + "/boxcarWidth", boxcarWidth);
		map.put(prefix + "/correctDetectorNonlinearity", correctDetectorNonlinearity);
		map.put(prefix + "/correctDarkCurrent", correctDarkCurrent);
		map.put(prefix + "/nScansToAverage", nScansToAverage);		
		map.put(prefix + "/externalTriggerMode",  externalTriggerMode);
		map.put(prefix + "/nPixelsTotal", nPixelsTotal);
		map.put(prefix + "/nChannels", nChannels);
		map.put(prefix + "/nPixelsDark", nPixelsDark);
		map.put(prefix + "/wrap", wrap); 

		return map;
	}
	
}
