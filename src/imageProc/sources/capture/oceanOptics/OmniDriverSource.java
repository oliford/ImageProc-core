package imageProc.sources.capture.oceanOptics;

import java.nio.ByteOrder;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.AcquisitionDevice;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.database.gmds.GMDSUtil;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.flir.FLIRCamConfig;
import imageProc.sources.capture.oceanOptics.swt.OmniDriverSWTControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class OmniDriverSource extends CaptureSource {
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	private int lastCaptured;
	
	/** The low-level capturer, we should only have one of these */  
	private OmniDriverCapture capture;
		/** The current config, should be kept always in sync with camera (if online)
	 * and with controller */
	private OmniDriverConfig config;
	
	public OmniDriverSource(OmniDriverCapture capture, OmniDriverConfig config) {
		this.capture = capture;
		this.config = (config != null) ? config : new OmniDriverConfig();
	}
	
	public OmniDriverSource() {
		capture = new OmniDriverCapture(this);
		config = new OmniDriverConfig();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by capture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	
	/** Called by capture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		lastCaptured = imageIndex;
		
		updateAllControllers();
				
	}
	
	@Override
	public OmniDriverSource clone() {
		return new OmniDriverSource(capture, config);
	}

	public void openLibrary(){
		if(capture.isOpen()){
			System.err.println("openLibrary(): Library already open");
			return;
		}		
		capture.setConfig(config);
		capture.initLibrary();
	}

	@Override
	public void startCapture(){
		if(capture.isBusy()){
			System.err.println("start(): Capture thread busy");
			return;
		}
		
		this.nCaptured = 0;		

		capture.setConfig(config);
		capture.startCapture(config, images);
	}
		
	public void abort(){ capture.abort(false); }

	public void close(){ capture.closeLibrary(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToAllocate + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). "
				+ "Run time = " + (config.integrationTimeUS * config.nImagesToAllocate / 1e6) + "s";
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	public String getCCDInfo(){
		String camModel = config.name + " / " + config.serialNumber;
				
		return "OceanOptics " + ((camModel != null) ? camModel : "???") +
				": " + config.nPixelsTotal + " x " + config.nChannels;
	}
	
	public boolean isActive(){ return capture.isOpen(); }
	
	public boolean isBusy(){ return capture.isBusy(); }
	
	@Override
	public boolean isIdle() { return capture == null || !capture.isOpen() || !capture.isBusy(); }

	public OmniDriverConfig getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (OmniDriverConfig)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new OmniDriverSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	public void releaseMemory() {
		capture.abort(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}
	
	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	
	public void closeCamera(){ capture.closeLibrary(false); }

	@Override
	public boolean open(long timeoutMS) {
			
		if(!capture.isOpen())
			capture.initLibrary();

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) { }

		
		return true;
	}


	public void setDiskMemoryLimit(long maxMemory) { capture.setDiskMemoryLimit(maxMemory); }
	
	public long getDiskMemoryLimit(){  return capture.getDiskMemoryLimit(); }
	
	@Override
	public String toShortString() {
		if(capture.isOpen() && config.spectrometerClass != null){
			String parts[] = config.spectrometerClass.split(".");
			return "OmniDriver[" + ((parts.length<2) ? config.spectrometerClass : parts[parts.length-1]) + ":"+config.deviceIndex+"]";
		}else
			return "OmniDriver";
	}

	public String[] getSpectrometerList() { return capture.getSpectrometerList(); }

	public String[] getSpectrometerClassList() { return capture.getSpectrometerClassList(); }

	@Override
	protected final Capture getCapture() { return capture; }
	
	@Override
	public int getFrameCount() { return config.nImagesToAllocate; }
	
	@Override
	public void setFrameCount(int frameCount) {  
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.nImagesToAllocate = frameCount; 
		updateAllControllers();
	}
	
	@Override
	public long getFramePeriod() { return config.integrationTimeUS; }
	
	@Override
	public void setFramePeriod(long framePeriodUS) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.integrationTimeUS = (int)framePeriodUS;
		updateAllControllers();
	}
	
	public List<String> getSerialNumberList() { return capture.getSerialNumberList(); }

	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig;	}
}
