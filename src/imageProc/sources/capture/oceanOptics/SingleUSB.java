package imageProc.sources.capture.oceanOptics;

import otherSupport.SettingsManager;

public abstract class SingleUSB {
	
	static {
		System.load(SettingsManager.defaultGlobal().getPathProperty("singleUSB.jniLibPath", "/usr/lib/singleUsbJNI.so"));
	}
	
	/** Sets the environment variables LIBUSB_SINGLE_BUS and LIBUSB_SINGLE_PORT that
	 * are read by the hacked libusb and used to limit the Andor SDK's enumeration of 
	 * USB cameras
	 * 
	 * @param singleUSBBus Bus number, or -1 for all
	 * @param singleUSBPort Port tree specification e.g. "9.3.3"
	 */
	public static native void setBusPortSelection(int singleUSBBus, String singleUSBPort);
	
	public static void main(String[] args) {
		setBusPortSelection(1, "4");
	}
}
