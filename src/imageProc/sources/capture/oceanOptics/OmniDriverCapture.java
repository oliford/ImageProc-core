package imageProc.sources.capture.oceanOptics;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import com.oceanoptics.omnidriver.api.wrapper.Wrapper;
import com.oceanoptics.omnidriver.api.wrapper.WrapperExtensions;
import com.oceanoptics.omnidriver.constants.USBProductInfo;
import com.oceanoptics.omnidriver.features.gpio.GPIO;
import com.oceanoptics.omnidriver.spectrometer.SpectrometerAssembly;
import com.oceanoptics.omnidriver.spectrometer.USBSpectrometer;
import com.oceanoptics.omnidriver.spectrometer.hr4000.HR4000;

import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.oceanOptics.OmniDriverConfig.GPIOState;
import imageProc.sources.capture.pco.PCOCamConfig;
import imageProc.sources.capture.pco.PCOCamSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** The capture thread and interfacing with OmniDriver 
 * for using OceanOptics USB spectrometers.
 * 
 * open/close refers to the library being connect 
 * 
 *  */
public class OmniDriverCapture extends Capture implements Runnable {
	
	private OmniDriverSource source;
	
	private OmniDriverConfig cfg;
	
	private ArrayList<String> serialNumbers = new ArrayList<String>();
	
	/** Thread copy of spectrometer index, in case someone changes it in the config */
	private int specIdx;
	
	private static Wrapper wrapper = null;
	private static ConcurrentLinkedQueue<OmniDriverCapture> capturesOpen = new ConcurrentLinkedQueue<>();
	
	static{ 
		//Hack to shorten the list of spectrometers the library will enumerate
		// to just the ones we used. It takes ~15seconds PER ENTRY!
		
		
		HashMap products = USBProductInfo.products;
		products.clear();
		
		USBProductInfo ourProducts[] = {
			new USBProductInfo("USB4000", "com.oceanoptics.omnidriver.spectrometer.usb4000.USB4000", 9303, 4130),
			new USBProductInfo("HR4000", "com.oceanoptics.omnidriver.spectrometer.hr4000.HR4000", 9303, 4114),
			new USBProductInfo("USB650", "com.oceanoptics.omnidriver.spectrometer.usb650.USB650", 9303, 4116),
			new USBProductInfo("QE65000", "com.oceanoptics.omnidriver.spectrometer.qe65000.QE65000", 9303, 4120),
			new USBProductInfo("STS", "com.oceanoptics.omnidriver.spectrometer.sts.STS", 9303, 16384),	
			new USBProductInfo("Blaze", "com.oceanoptics.omnidriver.spectrometer.blazer.BlazeUSB", 9303, 8195),
		    
			/*new USBProductInfo("USB2000", "com.oceanoptics.omnidriver.spectrometer.usb2000.USB2000", 9303, 4098),
		     new USBProductInfo("ADC1000-USB", "com.oceanoptics.omnidriver.spectrometer.adc1000usb.ADC1000USB", 9303, 4100),
		     new USBProductInfo("HR2000", "com.oceanoptics.omnidriver.spectrometer.hr2000.HR2000", 9303, 4106),
		     new USBProductInfo("HR2000+", "com.oceanoptics.omnidriver.spectrometer.hr2000plus.HR2000Plus", 9303, 4118),
		     new USBProductInfo("NIR512", "com.oceanoptics.omnidriver.spectrometer.nir.nir512.NIR512", 9303, 4108),
		     new USBProductInfo("NIR256", "com.oceanoptics.omnidriver.spectrometer.nir.nir256.NIR256", 9303, 4112),
		     new USBProductInfo("SAS", "com.oceanoptics.omnidriver.spectrometer.sas.SAS", 9303, 4102),
		     new USBProductInfo("USB2000+", "com.oceanoptics.omnidriver.spectrometer.usb2000plus.USB2000Plus", 9303, 4126),
		     new USBProductInfo("USB325", "com.oceanoptics.omnidriver.spectrometer.usb325.USB325", 9303, 4132),
		     new USBProductInfo("USB250", "com.oceanoptics.omnidriver.spectrometer.usb250.USB250", 9303, 4144),
		     new USBProductInfo("USB500", "com.oceanoptics.omnidriver.spectrometer.usb500.USB500", 9303, 4150),
		     new USBProductInfo("Jaz", "com.oceanoptics.omnidriver.spectrometer.jaz.JazUSB", 9303, 8192),
		     new USBProductInfo("S8378", "com.oceanoptics.omnidriver.spectrometer.s8378Prototype.S8378", 9303, 4146),
		     new USBProductInfo("MMS-Raman", "com.oceanoptics.omnidriver.spectrometer.mmsraman.ramanspectrometer.MMSRaman", 6220, 0),
		     new USBProductInfo("MayaPro2000", "com.oceanoptics.omnidriver.spectrometer.mayapro2000.MayaPro2000", 9303, 4138),
		     new USBProductInfo("Maya2000", "com.oceanoptics.omnidriver.spectrometer.maya2000.Maya2000", 9303, 4140),
		     new USBProductInfo("NIRQuest-256", "com.oceanoptics.omnidriver.spectrometer.nirquest.nirquest256.NIRQuest256", 9303, 4136),
		     new USBProductInfo("NIRQuest-512", "com.oceanoptics.omnidriver.spectrometer.nirquest.nirquest512.NIRQuest512", 9303, 4134),
		     new USBProductInfo("Pancake", "com.oceanoptics.omnidriver.spectrometer.sts.Pancake", 9303, 16896),
		     new USBProductInfo("FluxData", "com.oceanoptics.omnidriver.spectrometer.sts.FluxData", 11047, 55297),
		     new USBProductInfo("QE-PRO", "com.oceanoptics.omnidriver.spectrometer.qepro.QEPro", 9303, 16388),
		     new USBProductInfo("Torus", "com.oceanoptics.omnidriver.spectrometer.torus.Torus", 9303, 4160),
		     new USBProductInfo("Apex", "com.oceanoptics.omnidriver.spectrometer.apex.Apex", 9303, 4164),
		     new USBProductInfo("Ventana", "com.oceanoptics.omnidriver.spectrometer.ventana.Ventana", 9303, 20480),
		     new USBProductInfo("Maya LSL", "com.oceanoptics.omnidriver.spectrometer.mayalsl.MayaLSL", 9303, 4166),
		     new USBProductInfo("1064M", "com.oceanoptics.omnidriver.spectrometer.w1064m.W1064m", 9303, 20481),
		     */			
				
		};
		
		for(USBProductInfo upi : ourProducts){
			Long key = new Long(upi.vendorID << 16 | upi.productID);
			products.put(key, upi);
		} 
		
	}
	
	
	/** Library is open and thread is running */
	private boolean libraryOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
	
	private ByteBufferImage images[];

	/** List of spectrometers the library can access */
	private String spectrometerList[] = new String[0];
	
	public OmniDriverCapture(OmniDriverSource source) {
		this.source = source;
		setStatus(Status.notInitied, "init");
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera
	}
	
	
	public void enumSpectrometers(Wrapper wrapper) {
		
		int n = wrapper.getNumberOfSpectrometersFound();
		
		spectrometerList = new String[n];
		
		for(int i=0; i < n; i++){
			String name = wrapper.getName(i);
			String serialNum = wrapper.getSerialNumber(i);
			
			spectrometerList[i] = name + " / " + serialNum;
			System.out.println("Found spectrometer '"+spectrometerList[i]+"'");
		}		
	}	

	
	@Override
	public void run() {
		threadBusy = true;
		try{
			
			if(wrapper == null) {
				wrapper = initWrapper();
			}
			capturesOpen.add(this);
			logr.info("Wrapper is now opened from " + capturesOpen.size() + " capture objects");
			libraryOpen = true;
			
			enumSpectrometers(wrapper);
			
			signalCapture = false;
			
			setStatus(Status.init, "Init Ready");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture){
						
						updateGPIOState();
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalCapture){
											
						signalCapture = false;
						
						syncConfig(wrapper);
						
						setStatus(Status.init, "Allocating");		
						
						initImages();
							
						setStatus(Status.init, "Config+Alloc done.");
													
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("OmniDriver/config"));
						
						captureLoop(wrapper);
						
						setStatus(Status.completeOK, "Capture done");
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.info("Aborted by signal");
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.info("Aborted by exception: " + err);
					setStatus(Status.errored, "Aborted by error: " + err);
					err.printStackTrace();
				}finally{
					signalCapture = false;					
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("OmniDriver thread caught Exception: " + err);
			setStatus(Status.errored, "Aborted/Errored: " + err );
			
		}finally{
			try{
				if(wrapper != null) {					
					capturesOpen.remove(this);
					logr.info("Wrapper is now opened from " + capturesOpen.size() + " capture objects");
					if(capturesOpen.size() == 0) {
						
						wrapper.closeAllSpectrometers();
						wrapper = null;
					}
				}
				libraryOpen = false;
				
			}catch(Throwable err){
				System.err.println("OmniDriver thread caught exception de-initing camera.");
				err.printStackTrace();
				setStatus(Status.errored, "Aborted/Errored: " + err);
			}
		}			
		
	}
	
	private void updateGPIOState() {
		if(cfg.gpios == null)
			return;
		
		if(!wrapper.isFeatureSupportedGPIO(specIdx))
			throw new RuntimeException("GPIOs enabled in config but not supported by spectrometer");
			
		
		GPIO ctrl = wrapper.getFeatureControllerGPIO(specIdx);
		try {
			for(GPIOState gpio : cfg.gpios) {
				ctrl.setDirectionBit(gpio.index, gpio.output);
				ctrl.setMuxBit(gpio.index, false); //enable
				if(gpio.output) {
					ctrl.setValueBit(gpio.index, (gpio.value != 0));
				}else {
					gpio.value = ctrl.getValueBit(gpio.index);
				}
				gpio.lastUpdateMS = System.currentTimeMillis();
			}
		
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		
	}
	
	/** Unused, may need it later */
	/*private void releaseUnusedSpectrometers(Wrapper wrapper){
		//realease other spectrometers
		for(int i=0; i < spectrometerList.length; i++){
			if(i != cfg.spectrometerIndex && !spectrometerList[i].endsWith("[CLOSED]")){
				wrapper.closeSpectrometer(i);
				spectrometerList[i] += "[CLOSED]";
			}
		}
	}*/
	
	/** Initilize the library wrapper in whatever way is asked for */
	private Wrapper initWrapper(){
		
		Wrapper wrapper = new Wrapper();
		
		//SingleUSB.setBusPortSelection(cfg.singleUSBBus, cfg.singleUSBPort);			
		
		if(cfg.spectrometerClass != null){						
			//minimalistic init of library wrapper - just init that specific spectrometer, possibly searching the deviceIndex a bit
			
			int devIdx0 = (cfg.deviceIndex > 0) ? cfg.deviceIndex : 0;
			int devIdx1 = (cfg.deviceIndex > 0) ? cfg.deviceIndex : cfg.deviceIndexSearchMax;
			
			cfg.deviceIndexUsed = -1;			
			Exception lastError = null;
			Class specClass;
			try{
				specClass = Class.forName(cfg.spectrometerClass);
			}catch(ClassNotFoundException err){
				throw new RuntimeException(err);
			}
			
			for(int devId = devIdx0; devId <= devIdx1; devId++){
				try {				
					
					setStatus(Status.init, "Library init: Opening " + specClass.getSimpleName() + " index " + devId);
								
					logr.info("Attempting to initialise spectrometer class '"+specClass.getCanonicalName() + "', index " + devId);
				
					//USBSpectrometer spec = (USBSpectrometer)specClass.newInstance();
					Constructor constr = specClass.getConstructor(int.class);
					USBSpectrometer spec = (USBSpectrometer)constr.newInstance(devId);
					
					spec.getSerialNumber(); //test it
				
					//and now fill the Wrapper's list of spectrometers with just this one
					Field f = Wrapper.class.getDeclaredField("spectrometerAssemblyCollection");			
					f.setAccessible(true);
					ArrayList spectrometerAssemblyCollection = (ArrayList)f.get(wrapper); //IllegalAccessException
					spectrometerAssemblyCollection.add(new SpectrometerAssembly(spec));
					
					String serialNumber = spec.getSerialNumber();
					serialNumbers.add(serialNumber);
					
					
					if(cfg.acceptSerialNumber != null && !cfg.acceptSerialNumber.equals(serialNumber)) {
						spec.closeSpectrometer();
						specClass = null;
						System.out.println("Skipping spectrometer with non matching serial number '"+serialNumber+"'");
						
					}else {
					
						cfg.deviceIndexUsed = devId;
						//break; //was successful
					}
					
					
					
					
				} catch (Exception err) {
					lastError = err;
				} 
			}
			
			if(cfg.deviceIndexUsed < 0){
				throw new RuntimeException("Couldn't initialise any OmniDriver device for " + cfg.spectrometerClass + ":" + cfg.deviceIndex, lastError);
			}
			
			System.out.println("Opened device " + specClass.getSimpleName() + " index " + cfg.deviceIndexUsed);
			setStatus(Status.init, "Opened device " + specClass.getSimpleName() + " index " + cfg.deviceIndexUsed);
			
		}else{
			// the proper public way is to call openAllSpectrometers(), but it is horrifically slow and 
			// then we can't use the others from other applications
						
			setStatus(Status.init, "Library init: Enumerating spectrometers");
			wrapper.openAllSpectrometers();
			
			//setStatus(Status.init, "Library init: Opening network spectrometer");			
			//wrapper.openNetworkSpectrometer("Ocean ")
		}
		
		libraryOpen = true;
				
		return wrapper;
	}
	
	
	private void syncConfig(Wrapper wrapper) {
		//specIDx is 0 if we loaded 1 specific spectrometer
		specIdx = -1;
		if(cfg.selectSpectrometer == null) {
			specIdx = 0;
		} else {
			for(int i=0; i < spectrometerList.length; i++) {
				if(cfg.selectSpectrometer.equals(spectrometerList[i])) {
					specIdx = i;
					break;
				}
			}
		}
		
		if(specIdx < 0)
			throw new RuntimeException("Selected spectrometer not found");
		
		//attempt to set config
		/*wrapper.setBoxcarWidth(specIdx, cfg.boxcarWidth);
		
		wrapper.setStrobeEnable(specIdx, cfg.strobeEnable ? -1 : 0);
		wrapper.setAutoToggleStrobeLampEnable(specIdx, cfg.strobeAutoToggle);
		
		wrapper.setCorrectForElectricalDark(specIdx, cfg.correctDarkCurrent ? -1 : 0);
		wrapper.setCorrectForDetectorNonlinearity(specIdx, cfg.correctDetectorNonlinearity ? -1 : 0);
		wrapper.setScansToAverage(specIdx, cfg.nScansToAverage);
		wrapper.setExternalTriggerMode(specIdx, cfg.externalTriggerMode);
		*/
		wrapper.setExternalTriggerMode(specIdx, cfg.externalTriggerMode);
		//if OmniDriver > 2.73 needs try{}catch
		try {
			wrapper.setIntegrationTime(specIdx, cfg.integrationTimeUS);
			if(false)
				throw new IOException("Only here to keep the catch block for omniDriver-2.56");
		} catch (IOException err) {
			logr.log(Level.WARNING, "Unable to set integration time", err);
		}//*/
		
		//wrapper.setUSBTimeout(specIdx, cfg.readTimeoutMS);{
		cfg.readTimeoutMS=0;
		
		//get spectrometer identity info
		cfg.name = wrapper.getName(specIdx);
		cfg.serialNumber = wrapper.getSerialNumber(specIdx);
		cfg.apiVersion = wrapper.getApiVersion();
		cfg.firmwareModel = wrapper.getFirmwareModel(specIdx);
		cfg.firmwareVersion = wrapper.getFirmwareVersion(specIdx);
		cfg.buildNumber = wrapper.getBuildNumber();
		
		
		// and re-get config as it actually is
		
		cfg.boxcarWidth = wrapper.getBoxcarWidth(specIdx);
		cfg.externalTriggerMode = wrapper.getExternalTriggerMode(specIdx);
		cfg.nScansToAverage = wrapper.getScansToAverage(specIdx);
		cfg.correctDetectorNonlinearity = wrapper.getCorrectForDetectorNonlinearity(specIdx);
		cfg.correctDarkCurrent = wrapper.getCorrectForElectricalDark(specIdx);
		cfg.integrationTimeUS = wrapper.getIntegrationTime(specIdx);
		cfg.strobeEnable = (wrapper.getStrobeEnable(specIdx) != 0);
		//int a = wrapper.getBoxcarWidth(specIdx);
		//cfg.strobeAutoToggle = wrapper.getAuto... doesn't exist
		
		//get spectrometer config
		cfg.nPixelsTotal = wrapper.getNumberOfPixels(specIdx);
		cfg.nPixelsDark = wrapper.getNumberOfDarkPixels(specIdx);		
		cfg.nChannels = 1; //wrapper.getN /... ???
		
	}
	
	private void initImages() {
		
		int width = cfg.nPixelsTotal;
		int height = cfg.nChannels;				
		int bytesPerPixel, bitDepth;
		if(cfg.convertType == null || cfg.convertType == Double.class) {
			bytesPerPixel = Double.BYTES;
			bitDepth = ByteBufferImage.DEPTH_DOUBLE;
		}else if(cfg.convertType == Byte.class) {
			bytesPerPixel = 1;
			bitDepth = 8;
		}else if(cfg.convertType == Short.class) {
			bytesPerPixel = 2;
			bitDepth = 16;
		}else if(cfg.convertType == Integer.class) {
			bytesPerPixel = 4;
			bitDepth = 32;
		}else {
			throw new IllegalArgumentException("Type conversion to " + cfg.convertType + " not supported.");
		}
		
		long imgDataSize = width * height * bytesPerPixel;
			
		int footerSize = 0;
		
		if(imgDataSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		try{
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				System.out.println("initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				System.out.println("initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			System.out.println("initImages() Not enough memory for images.");
			bulkAlloc.destroy();			throw new RuntimeException("Not enough memory for image capture", err);
		}
	}

	private void captureLoop(Wrapper wrapper) {
		
		double wavelen[] = wrapper.getWavelengths(specIdx);
		source.setSeriesMetaData("wavelength", wavelen, false);
				
		setStatus(Status.awaitingSoftwareTrigger, "Awaiting acquire enable. specIdx=" +specIdx);		
		
		try{
			
		
			double time[] = new double[cfg.nImagesToAllocate]; //seconds since first image
			long wallRelNanoTime[] = new long[cfg.nImagesToAllocate];
			long nanoTime[] = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("OmniDriver/wallRelativeTimeNS", wallRelNanoTime, true);
			source.setSeriesMetaData("OmniDriver/nanoTime", nanoTime, true);
			source.setSeriesMetaData("OmniDriver/time", time, true);
						
			int nextImgIdx = 0;
			long firstRelNanoTimeNS = Long.MIN_VALUE;
			long firstWallCopyTimeMS = Long.MIN_VALUE;
			do{
				
				try {
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try{
						images[nextImgIdx].invalidate(false);
						
						if(!isAcqusitionStarted()){ 
							//we don't actually do anything to the spectrometer here
							//it's just that we now start requesting the spectra
						
							while(!signalAcquireStart){
								Thread.sleep(0, 100000);
							}
							setStatus(Status.awaitingHardwareTrigger, "Acquire started. Awaiting first frame");
						}
						
						
						double spec[];
						do { //spin on timeout if waiting for trigger
							spec = wrapper.getSpectrum(specIdx);
							//with overridden libusb-compat-0.1 timeout
						}while(!wrapper.isSpectrumValid(specIdx) && !signalAbort && !signalDeath);
						//}while((cfg.readTimeoutMS > 0 && wrapper.isUSBTimeout(specIdx)) && !signalAbort && !signalDeath);
						
						if(signalAbort || signalDeath)
							break;
						
						updateGPIOState();
						
						wallRelNanoTime[nextImgIdx] = System.nanoTime();
						if(firstWallCopyTimeMS == Long.MIN_VALUE){
							firstWallCopyTimeMS = System.currentTimeMillis(); //only have this accurate to ms
							firstRelNanoTimeNS = wallRelNanoTime[nextImgIdx];
						}
						
						setStatus(Status.capturing, "Capturing.");			
						
						if(cfg.convertType == null || cfg.convertType == Double.class) {
							DoubleBuffer buff = images[nextImgIdx].getWritableBuffer(writeLock).asDoubleBuffer();
							buff.put(spec);
						}else if(cfg.convertType == Byte.class){
							ByteBuffer buff = images[nextImgIdx].getWritableBuffer(writeLock);
							for(int i=0; i < spec.length; i++)
								buff.put((byte)spec[i]);
							
						}else if(cfg.convertType == Short.class){
							ShortBuffer buff = images[nextImgIdx].getWritableBuffer(writeLock).asShortBuffer();
							for(int i=0; i < spec.length; i++)
								buff.put((short)spec[i]);
							
						}else if(cfg.convertType == Byte.class){
							IntBuffer buff = images[nextImgIdx].getWritableBuffer(writeLock).asIntBuffer();
							for(int i=0; i < spec.length; i++)
								buff.put((int)spec[i]);
						}
						
					}finally{ writeLock.unlock(); }
				} catch (InterruptedException e) {
					System.out.println("Interrupted while waiting to write to java image.");
					return;
				}
				
				//best we can do for timing is using wall times
				//relative: wall nano time (probably better than microsec precision)
				//absolute: wall milisecond time
				nanoTime[nextImgIdx] = wallRelNanoTime[nextImgIdx] - firstRelNanoTimeNS + firstWallCopyTimeMS*1_000_000L;
				time[nextImgIdx] = (double)(wallRelNanoTime[nextImgIdx] /1000L - firstRelNanoTimeNS/1000L) / 1e6;
				source.imageCaptured(nextImgIdx);
				
				nextImgIdx++;
				if(nextImgIdx >= cfg.nImagesToAllocate && cfg.wrap)
					nextImgIdx = 0; //cfg.blackLevelFirstFrame ? 1 : 0;	
			
			}while(nextImgIdx < cfg.nImagesToAllocate && !signalAbort && !signalDeath);
			
		}finally{
			//stop acquiring ... nothing todo
		}
	}
	
	/** Start the camera thread and open the camera */
	public void initLibrary() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		//thread.setPriority(Thread.MAX_PRIORITY);
		spinLog("Starting OmniDriver capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.setName("OmniDriver Capture");
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeLibrary(true)));
	}
	
	/** Close the camera and kill the thread completely */
	public void closeLibrary(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("AndorCamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("AndorCamCapture: Thread died.");					
			}	
		}
	}
	
	/** Signal the thread to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
		
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				System.out.println("OmniDriverCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("OmniDriverCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(OmniDriverConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	
	public void destroy() { closeLibrary(false);	}

	public boolean isOpen(){ return thread != null && thread.isAlive() && libraryOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }

	public void setConfig(OmniDriverConfig config) {
		this.cfg = config;
	}

	public String[] getSpectrometerList() { return spectrometerList; }

	/** Gets list of specific spectrometer possibility, always first index ':0' */	
	public String[] getSpectrometerClassList() {
		ArrayList<String> specTypeList = new ArrayList<String>();
		for(Object o : USBProductInfo.products.values()){
			USBProductInfo specType = (USBProductInfo)o;			
			specTypeList.add(specType.className);			
		}
		
		return specTypeList.toArray(new String[specTypeList.size()]);		
	}


	public List<String> getSerialNumberList() {
		return serialNumbers;
	}

}
