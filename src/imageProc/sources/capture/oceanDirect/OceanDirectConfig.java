package imageProc.sources.capture.oceanDirect;


import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;

public class OceanDirectConfig extends CaptureConfig {

	/** TODO: List of timestamp modes for the GUI */
	public static String[] timestampModes = { "Off", "Text in image", "Binary in image"};

	/** TODO: List of trigger modes for the GUI */
	public static String[] triggerModes = { "Software", "Start", "Frame" };
	
	/** If uninitialised, the camera values will be not set but retrieved on the first sync */
	public boolean initialised = false;
	
	/** Number of images to allocate */
	public int nImagesToAllocate = 100;
	
	/** Continuous mode (start again from image 0 (or 1) when nImagesToAllocate is reached) */
	public boolean wrap = false;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;

	public long cameraID;

	/* TODO: Add camera specific configuation entries here */

	/** Image width (pixels) */
	public int width = 640;

	/** OceanDirect exposure time in ms */
	public long exposureTime = 50;

	/** OceanDirect delay time in ms */
	public long delayTime = 50;

	/** Serial number from spectrometer */
	public String serialNumber;

	/** Name from spectrometer */
	public String deviceName;
	
	@Override
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put(prefix + "/cameraID", cameraID);
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/blackLevelFirstFrame", blackLevelFirstFrame);
		map.put(prefix + "/wrap", wrap);
		map.put(prefix + "/exposureTime", exposureTime);
		map.put(prefix + "/delayTime", delayTime);

		//TODO: Add other entries that should be saved in metadata
		
		return map;
	}

}
