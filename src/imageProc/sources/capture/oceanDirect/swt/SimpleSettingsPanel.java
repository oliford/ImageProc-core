package imageProc.sources.capture.oceanDirect.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.Algorithms;
import algorithmrepository.NotImplementedException;
import imageProc.core.ImgSource;
import imageProc.sources.capture.oceanDirect.OceanDirectConfig;
import imageProc.sources.capture.oceanDirect.OceanDirectSource;
import net.jafama.FastMath;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private OceanDirectSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo shutterModeCombo;
	private Combo triggerModeCombo;
	
	private Text exposureLengthTextbox;
	private Text delayTimeTextbox;
		
	public SimpleSettingsPanel(Composite parent, int style, OceanDirectSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		Label lEM = new Label(swtGroup, SWT.NONE); lEM.setText("Shutter:");
		shutterModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		shutterModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		shutterModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lHWT = new Label(swtGroup, SWT.NONE); lHWT.setText("H/W Trigger:");
		triggerModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		triggerModeCombo.setItems(OceanDirectConfig.triggerModes);
		triggerModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		triggerModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Delay time:");		
		delayTimeTextbox = new Text(swtGroup, SWT.NONE);
		delayTimeTextbox.setText("?");
		delayTimeTextbox.setEnabled(false);
		delayTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		delayTimeTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT2 = new Label(swtGroup, SWT.NONE); lT2.setText("Exposure time:");
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("100");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		configToGUI();
	}

	void configToGUI(){
		OceanDirectConfig config = source.getConfig();
		
		continuousButton.setSelection(config.wrap);
				
		exposureLengthTextbox.setText(Long.toString(config.exposureTime));
		delayTimeTextbox.setText(Long.toString(config.delayTime));
	}
	
	void guiToConfig(){
		OceanDirectConfig config = source.getConfig();
		
		config.wrap = continuousButton.getSelection();
		
		config.exposureTime = Algorithms.mustParseInt(exposureLengthTextbox.getText());
		config.delayTime = Algorithms.mustParseInt(delayTimeTextbox.getText());
		
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		OceanDirectConfig config = source.getConfig();

		if(true)
			throw new NotImplementedException();
		
	}
	
	public void exposureMinMaxEvent(boolean max){
		OceanDirectConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
		configToGUI();
	}


	public void roiMaxEvent(){
		OceanDirectConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
