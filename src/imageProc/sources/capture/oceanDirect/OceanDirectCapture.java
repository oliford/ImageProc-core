package imageProc.sources.capture.oceanDirect;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import oceanDirectJNI.OceanDirect;
import otherSupport.RandomManager;
import otherSupport.SettingsManager;

/** Capture thread for DC1394 Cameras */

public class OceanDirectCapture extends Capture implements Runnable {
	private OceanDirectSource source;
	
	private ByteBufferImage images[];
	
	private OceanDirectConfig cfg;
		
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;

	
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
	
	private long cameraID = -1;

	/** oceanDirect if a list of available camera should be got on first init attempt */
	public String[] availableCameras;

	public OceanDirectCapture(OceanDirectSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera 
	}
	

	static {
		try {
			System.load(SettingsManager.defaultGlobal().getPathProperty("oceanDirect.jniLibPath", "/usr/lib/oceanDirectJNI.so"));
			OceanDirect.initialize();
		}catch(UnsatisfiedLinkError err) {
			Logger.getLogger(OceanDirectCapture.class.getName()).log(Level.WARNING, "Couldn't load library for oceanDirect", err);
		}

	}
	
	@Override
	public void run() {
		
		threadBusy = true;
		try{
			if(cameraID >= 0){				
				deinit();
				throw new RuntimeException("Camera driver already open, aborting");
			}
			
			setStatus(Status.init, "Initialising camera...");
			initDevice();
			
			//signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Camera init done.");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();
					}
								
					
					if(signalCapture){
						signalCapture = false;

						setStatus(Status.init, "Allocating image memory...");		
						
						initImages();
									
						setStatus(Status.init, "Config done, starting capture...");
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("OceanDirect/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");
					}else {
						
						setStatus(Status.completeOK, "Config done.");						
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal", err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("OceanDirect thread caught Exception: " + err);
			setStatus(Status.errored, "ERROR: " + err.getMessage());
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				System.err.println("OceanDirect thread caught exception de-initing camera.");
				err.printStackTrace();
				setStatus(Status.errored, "ERROR: " + err.getMessage());
			}
		}
	}
	
	private void initDevice() {
		
		int nDevs = OceanDirect.probe_devices();
		long[] devIDs = new long[nDevs]; 
		OceanDirect.get_device_ids(devIDs);

		availableCameras = new String[nDevs];
		for(int i=0; i < nDevs; i++) {
			availableCameras[i] = Long.toString(devIDs[i]);
		}
		
		if(cfg.cameraID < 0) {
			cameraID = -1;
			throw new RuntimeException("No camera selecetd");
		}
		
		OceanDirect.open_device(cfg.cameraID);
		cameraID = cfg.cameraID; //mark as open
		
		cfg.serialNumber = OceanDirect.get_serial_number(cameraID);
		//cfg.deviceName = OceanDirect.get_device_name(nDevs);
		
		cameraOpen = true;
	}

	private void syncConfig() {

		OceanDirect.set_integration_time_micros(cameraID, cfg.exposureTime);
		OceanDirect.set_acquisition_delay_microseconds(cameraID, cfg.delayTime);
				
		cfg.initialised = true;
		
		cfg.exposureTime = OceanDirect.get_integration_time_micros(cameraID);
		cfg.delayTime = OceanDirect.get_acquisition_delay_microseconds(cameraID);
		
		int spectrumLength = OceanDirect.get_formatted_spectrum_length(cameraID);
        
        cfg.width = spectrumLength;		
		
		source.configChanged();
	}
	
	
	private void initImages(){
		
		int width = cfg.width; // camera.GetImageWidth()
		int height = 1;  // camera.GetImageHeight()
		int bitDepth = -64; // camera.GetImageBitDepth()
		double bytesPerPixel = bitDepth / 8;
		long imgDataSize = width*height*2; // camera.GetImageSizeBytes()
		
		if(imgDataSize < height*width*bytesPerPixel){
			throw new RuntimeException("Camera wants less memory than it needs for the image???");
		}
			
		int footerSize = 0; // sometimes camera gives header/footer with metadata timestamps etc
		
		if(imgDataSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		//allocate the image memory in as big chunks as possible (4GB)
		try{
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
	
	}
	
	private void doCapture() {
		
		
		try{
			
			//Initialise running timestamp and time information arrays
			double time[] = new double[cfg.nImagesToAllocate]; //seconds since first image
			long timestampNS[] = new long[cfg.nImagesToAllocate]; // full nanoSecs since midnight 1970 UTC
			long wallCopyTimeMS[] = new long[cfg.nImagesToAllocate];
			int imageNumberStamp[] = new int[cfg.nImagesToAllocate];
			long nanoTime[] = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("OceanDirect/timestampNS", timestampNS, true);
			source.setSeriesMetaData("OceanDirect/wallCopyTimeMS", wallCopyTimeMS, true);
			source.setSeriesMetaData("OceanDirect/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("OceanDirect/nanoTime", nanoTime, true);
			source.setSeriesMetaData("OceanDirect/time", time, true);
			
			//TODO: Initialise internal camera transfer buffers, if it needs them
			//camera.SetBuffers(...)
						
			//TODO: Begin the camera capture, so long as it has a faster acqusition start/stop which is done below
			//camera.CheckConfigAndInitialiseCapturing()
			
			setStatus(Status.awaitingSoftwareTrigger, "Capturing, acquisition not started.");	
					
			long wallCopyTimeMS0 = Long.MIN_VALUE;
			long timestampNS0 = Long.MIN_VALUE;
			int nextImgIdx = 0;
			do{
				int imgIdx = -1;
				try {
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try{
						images[nextImgIdx].invalidate(false);
						do{
								
							if(!isAcqusitionStarted()){ //don't actually start until acquire signal is set
								while(!signalAcquireStart){
									Thread.sleep(0, 100000);
								}
								
								//TODO: Start the camera
								//camera.SetRecording(true);
								
								setStatus(Status.awaitingHardwareTrigger, "Capturing..., (awaiting hardware trigger)");
							
							}
							
							//if acquisition was stopped in the middle of a run, do so
							if(isAcqusitionStarted() && !signalAcquireStart) {
								//TODO: camera.SetRecording(false);
								setStatus(Status.awaitingSoftwareTrigger, "Capturing, but was stopped (intentionally).");
							}
							
							if(signalDeath || signalAbort){
								logr.info("Aborting capture loop.");
								throw new CaptureAbortedException();
							}
							
							try{
								
								//simulate images
								if(signalAcquireStart)
								{
									
									ByteBuffer buf = images[nextImgIdx].getWritableBuffer(writeLock);
									buf.rewind();
									buf.order(ByteOrder.LITTLE_ENDIAN);
									buf.limit(buf.capacity());

									int pixelsAcquired = OceanDirect.get_formatted_spectrum(cameraID, buf);
																		
									if(pixelsAcquired != cfg.width) {
										throw new RuntimeException("Acquired " + pixelsAcquired + " but width is " + cfg.width);
									}
									
									imgIdx = nextImgIdx;
								}else {
									continue;
								}							
								
								//store the local wall time as backup timestamp
								wallCopyTimeMS[imgIdx] = System.currentTimeMillis();								
								
								//make sure the status reflects that an image has arrived
								setStatus(Status.capturing, "Capturing..., acquisition started");
								
								//TODO: Catch timeout errors of type e.g OceanDirecteraException
							 }catch(RuntimeException err){
							 

								if(err.toString().startsWith("Timeout"))
									continue;
								throw err;
							
							}
							
							
							
							
							
							break;
						}while(true); //timeout loop
						
						//spinLog(".doCapture(): Buffer copied to images[" + nextImgIdx + "]");
						
						if(wallCopyTimeMS0 == Long.MIN_VALUE)
							wallCopyTimeMS0 = wallCopyTimeMS[imgIdx];
						
						//TODO: Something like if(cfg.timestampsEnabled) {
						if(false) {
							processTimestamp(imageNumberStamp, timestampNS, imgIdx);
							
							if(timestampNS0 == Long.MIN_VALUE){
								timestampNS0 = timestampNS[imgIdx];
							}
							
							//use timestamp to make a quick 'time since first time' time 
							time[imgIdx] = (double)((timestampNS[imgIdx] - timestampNS0) / 1000) / 1e6;
							nanoTime[imgIdx] = (timestampNS[imgIdx] - timestampNS0 + wallCopyTimeMS0*1_000_000L);
						}else{
							
							//no timestamp, so our wall copy time is the best we can do							
							time[imgIdx] = (wallCopyTimeMS[imgIdx] - wallCopyTimeMS0) / 1e3;
							nanoTime[imgIdx] = wallCopyTimeMS[imgIdx] * 1_000_000L; 
						}
						
					}finally{ writeLock.unlock(); }
				} catch (InterruptedException e) {
					logr.info("Interrupted while waiting to write to java image.");
					return;
				}
				
				source.imageCaptured(imgIdx);
				
				if(imgIdx != nextImgIdx)
					logr.warning("Got image " + imgIdx + " but we were expecting " + nextImgIdx);
				nextImgIdx = imgIdx;
				
				nextImgIdx++;
				if(nextImgIdx >= cfg.nImagesToAllocate && cfg.wrap)
					nextImgIdx = cfg.blackLevelFirstFrame ? 1 : 0;
				
				
			}while(nextImgIdx < cfg.nImagesToAllocate);
			
		}finally{
			//TODO: Stop the camera and free any buffers etc
			//camera.CancelImages();
			//camera.SetRecording(false);
		}

	}
	
	
	private void processTimestamp(int[] imageNumberStamp, long[] timestamp, int imageIdx){

		//TODO: Process image data and put into timestamps array
		timestamp[imageIdx] = 5; 
		
	}
	
	private void deinit() {
		//TODO: Close camera if its open
		if(cameraID >= 0){
			
			try {
				OceanDirect.close_device(cameraID);
				
			}catch(RuntimeException err) {
				logr.log(Level.WARNING, "Error closing camera.", err);
			}
						
			cameraID = -1;
			
		}
	}
	
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		//thread.setPriority(Thread.MAX_PRIORITY);
		spinLog("Starting PCO capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(OceanDirectConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(OceanDirectConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thread to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
		
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				System.out.println("OceanDirectCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("OceanDirectCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("OceanDirectCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("OceanDirectCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}
	
	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(OceanDirectConfig config) { this.cfg = config;	}

	public String[] getAvailableCameras() { return availableCameras ; }

}
