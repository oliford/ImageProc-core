package imageProc.sources.capture.oceanDirect;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.Algorithms;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.oceanDirect.swt.OceanDirectSWTControl;

/** Image source for oceanDirect camera.
 * Contains the application and image handling side.
 * The image capture and device handling is done by OceanDirectCapture 
 * 
 * @author oliford
 */
public class OceanDirectSource extends CaptureSource implements ConfigurableByID {
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	private int lastCaptured;
	
	/** The low-level capturer, we should only have one of these */  
	private OceanDirectCapture capture;
	
	/** The current config, should be kept always in sync with camera (if online)
	 * and with controller */
	private OceanDirectConfig config;
	
	public OceanDirectSource(OceanDirectCapture capture, OceanDirectConfig config) {
		this.capture = capture;
		this.config = (config != null) ? config : new OceanDirectConfig();
	}
	
	public OceanDirectSource() {
		capture = new OceanDirectCapture(this);
		config = new OceanDirectConfig();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by capture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	
	/** Called by capture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		lastCaptured = imageIndex;
		
		updateAllControllers();
				
	}
	
	@Override
	public OceanDirectSource clone() {
		return new OceanDirectSource(capture, config);
	}

	public void openCamera(){
		if(capture.isOpen()){
			System.err.println("openCamera(): Camera already open");
			return;
		}
		capture.setConfig(config);
		capture.initCamera();
	}

	public void startCapture(){
		if(capture.isBusy()){
			System.err.println("startCapture(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		

		capture.setConfig(config);
		capture.startCapture(config, images);
	}
	
	public void syncConfig(){
		if(capture.isBusy()){
			System.err.println("syncConfig(): Camera thread busy");
			return;
		}

		capture.setConfig(config);
		this.nCaptured = 0;
		capture.testConfig(config, images);
	}
		
	public void abort(){ capture.abort(false); }
	
	public void closeCamera(){ capture.closeCamera(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToAllocate + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). S/W start "				
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	public String getCCDInfo(){
		
		return "OceanDirectera"; //TODO: Add some info from cfg.* here
	}
	
	public boolean isActive(){ return capture.isOpen(); }
	
	public boolean isBusy(){ return capture.isBusy(); }
	
	@Override
	public boolean isIdle() { return capture == null || !capture.isOpen() || !capture.isBusy(); }

	public OceanDirectConfig getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (OceanDirectConfig)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new OceanDirectSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	//public void setAutoExposure(boolean enable){ this.autoExposure = enable; }
	//public boolean getAutoExposure(){ return this.autoExposure; }

	public void releaseMemory() {
		capture.abort(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}

	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	@Override
	public void close() {
		abort();
		closeCamera();		
	}

	@Override
	public boolean open(long timeoutMS) {
		
		openCamera();
		long t0 = System.currentTimeMillis();
		while(!capture.isOpen()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) { }
			
			if((System.currentTimeMillis() - t0) > timeoutMS){
				throw new RuntimeException("Timed out waiting for camera to open. Need a longer warm-up time??");
			}
		}
		
		//do a config sync as-is
		syncConfig();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) { }

		
		return true;
	}


	public void setDiskMemoryLimit(long maxMemory) { capture.setDiskMemoryLimit(maxMemory); }
	
	public long getDiskMemoryLimit(){  return capture.getDiskMemoryLimit(); }
	
	@Override
	public String toShortString() { return "OceanDirect"; }
	
	@Override
	protected final Capture getCapture() { return capture; }
	
	
	@Override
	public int getFrameCount() { 
		
		return config.nImagesToAllocate;		
	}
	
	@Override
	public void setFrameCount(int frameCount) {  
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		
		config.nImagesToAllocate = frameCount;
		
		updateAllControllers();
	}
	
	@Override
	public long getFramePeriod() { 
		
		return config.exposureTime + config.delayTime;		
	}
	
	@Override
	public void setFramePeriod(long framePeriodUS) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		
		config.exposureTime = (long)(framePeriodUS - config.delayTime);
				
		updateAllControllers();
	}

	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig;	}

	public String[] getAvailableCameras() { return capture.getAvailableCameras(); }
}
