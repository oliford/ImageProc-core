package imageProc.sources.capture.example.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.sources.capture.example.ExampleConfig;
import imageProc.sources.capture.example.ExampleSource;
import net.jafama.FastMath;

public class ExampleSWTControl  implements ImagePipeController {
	public static final double log2 = FastMath.log(2);
			
	private ExampleSource source;
	private Group swtGroup;
	
	private Label sourceStatusLabel;
	private Label captureStatusLabel;
	private Label ccdInfoLabel;
	private Label timingInfoLabel;
	
	private Combo cameraSelectionCombo; 
	
	private Button blackLevelButton;
	private Button diskMappedCheckbox;
	private Spinner numImagesSpinner;		
	private Button allowAutoconfigCheckbox;
	private Spinner hackDelaySpinner;

	private SWTSettingsControl settingsCtrl;
	
	private Button openButton;
	private Button closeButton;
	private Button syncConfigButton;
	private Button softStartEnableCheckbox;
	private Button releaseMemoryButton;
	private Button abortTrigEnableCheckbox;
	private Button enableAcquireCheckbox;
	
	private Button abortButton;
	private Button startCaptureButton;
	
	private CTabFolder swtTabFoler;	
	private CTabItem swtSimpleTab;
	private CTabItem swtMultiTab;
	private CTabItem swtFullTab;
	
	private SimpleSettingsPanel simplePanel;
		
	public ExampleSWTControl(Composite parent, int style, ExampleSource source) {
		this.source = source;
				
		swtGroup = new Group(parent, style);
		swtGroup.setText("ExampleCam");
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("Source status:");
		sourceStatusLabel = new Label(swtGroup, SWT.NONE);
		sourceStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capture status:");
		captureStatusLabel = new Label(swtGroup, SWT.NONE);
		captureStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCI = new Label(swtGroup, SWT.NONE); lCI.setText("Camera:");
		ccdInfoLabel = new Label(swtGroup, SWT.NONE);
		ccdInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lTI = new Label(swtGroup, SWT.NONE); lTI.setText("Times:");
		timingInfoLabel = new Label(swtGroup, SWT.NONE);
		timingInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lC = new Label(swtGroup, SWT.NONE); lC.setText("Camera:");
		cameraSelectionCombo = new Combo(swtGroup, SWT.NONE);
		cameraSelectionCombo.setItems(new String[] { " -- None -- " });
		cameraSelectionCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		cameraSelectionCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { selectCameraEvent(); } });
		
		
		Label lNI = new Label(swtGroup, SWT.NONE); lNI.setText("Images:");
		numImagesSpinner = new Spinner(swtGroup, SWT.NONE);
		numImagesSpinner.setValues(100, 1, Integer.MAX_VALUE, 0, 1, 10);
		numImagesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		numImagesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		allowAutoconfigCheckbox = new Button(swtGroup, SWT.CHECK);
		allowAutoconfigCheckbox.setText("Allow auto");
		allowAutoconfigCheckbox.setToolTipText("Allow autoconfiguration from other modules (e.g. from an autopilot)");
		allowAutoconfigCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		allowAutoconfigCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lHD = new Label(swtGroup, SWT.NONE); lHD.setText("Hack delay/us:");
		hackDelaySpinner = new Spinner(swtGroup, SWT.NONE);
		hackDelaySpinner.setValues(100, 0, Integer.MAX_VALUE, 0, 1, 100);
		hackDelaySpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		hackDelaySpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		blackLevelButton = new Button(swtGroup, SWT.CHECK);
		blackLevelButton.setText("Frame 0 as BG (keep in live mode)");
		blackLevelButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		blackLevelButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		diskMappedCheckbox = new Button(swtGroup, SWT.CHECK);
		diskMappedCheckbox.setText("Disk Memory");
		diskMappedCheckbox.setEnabled(true);
		diskMappedCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { diskMappedCheckboxEvent(event); } });
		diskMappedCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		swtTabFoler = new CTabFolder(swtGroup, SWT.BORDER);
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 5, 1));
		
		simplePanel = new SimpleSettingsPanel(swtTabFoler, SWT.NONE, source);
		swtSimpleTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSimpleTab.setControl(simplePanel.getSWTGroup());
		swtSimpleTab.setText("Simple");
				
        
		//fullPanel = new FullSettingsPanel(swtTabFoler, SWT.NONE, source);
		//swtFullTab = new CTabItem(swtTabFoler, SWT.NONE);
		//swtFullTab.setControl(fullPanel.getSWTGroup());
		//swtFullTab.setText("Full");
		
		swtTabFoler.setSelection(swtSimpleTab);
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, source);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));

		openButton = new Button(swtGroup, SWT.PUSH);
		openButton.setText("Open Camera");
		openButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		openButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { openCameraButtonEvent(event); } });

		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close Camera");
		closeButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeCameraButtonEvent(event); } });

		syncConfigButton = new Button(swtGroup, SWT.PUSH);
		syncConfigButton.setText("Get config");
		syncConfigButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		syncConfigButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { syncConfigButtonEvent(event); } });

		releaseMemoryButton = new Button(swtGroup, SWT.PUSH);
		releaseMemoryButton.setText("Release Mem");
		releaseMemoryButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		releaseMemoryButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { releaseMemoryButtonEvent(event); } });
		
		softStartEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		softStartEnableCheckbox.setText("S/W Start");
		softStartEnableCheckbox.setToolTipText("When selected, camera will start acquiring if the global start event is broadcast (i.e. the Start All button)");
		softStartEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		softStartEnableCheckbox.setSelection(false);
		softStartEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		abortButton = new Button(swtGroup, SWT.PUSH);
		abortButton.setText("Abort");
		abortButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		abortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortButtonEvent(event); } });
	
		startCaptureButton = new Button(swtGroup, SWT.PUSH);
		startCaptureButton.setText("Capture");
		startCaptureButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		startCaptureButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startCaptureButtonEvent(event); } });
		
		enableAcquireCheckbox = new Button(swtGroup, SWT.CHECK);
		enableAcquireCheckbox.setText("Acquire");
		enableAcquireCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		enableAcquireCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { enableAcquireCheckboxEvent(event); } });
		
		abortTrigEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		abortTrigEnableCheckbox.setText("Trigger end abort");
		abortTrigEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		abortTrigEnableCheckbox.setSelection(false);
		abortTrigEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		generalControllerUpdate();
		
		simplePanel.configToGUI();
		//fullPanel.configToGUI();
		
		ImageProcUtil.addRevealWriggler(swtGroup);
	}
	
	protected void selectCameraEvent() {
		ExampleConfig config = source.getConfig();
		
		if(cameraSelectionCombo.getText().equals(" -- Retrieve list --")) {
			config.cameraID = null;
		}else {
			String parts[] = cameraSelectionCombo.getText().split(":");
			config.cameraID = parts[0];
		}
		
		fillCameraSelectionCombo();
	}

	public void guiToConfig(){
		ExampleConfig config = source.getConfig();
		
		config.nImagesToAllocate = numImagesSpinner.getSelection();
		config.enableAutoConfig = allowAutoconfigCheckbox.getSelection();
		config.blackLevelFirstFrame = blackLevelButton.getSelection();
		config.beginCaptureOnStartEvent = softStartEnableCheckbox.getSelection();
		config.abortEventOnCaptureComplete = abortTrigEnableCheckbox.getSelection();
		
		numImagesSpinner.setForeground(swtGroup.getDisplay().getSystemColor(config.enableAutoConfig ? SWT.COLOR_GREEN : SWT.COLOR_WIDGET_FOREGROUND));
		
	}
	
	public void fillCameraSelectionCombo() {
		ExampleConfig config = source.getConfig();
		
		String[] cams = source.getAvailableCameras();
		cameraSelectionCombo.setItems(new String[] { " -- Retrieve list --" });
		cameraSelectionCombo.select(0);
		boolean serialInList = false;
		if(cams != null) {
			for(int i=0; i < cams.length; i++) {
				cameraSelectionCombo.add(cams[i]);
				if(cams[i] == config.cameraID) {
					cameraSelectionCombo.select(cameraSelectionCombo.getItemCount()-1);
					serialInList = true;
				}
			}
		}
		
		if(config.cameraID != null && !serialInList){
			cameraSelectionCombo.add(config.cameraID + ": ??? (Camera not found)");
			cameraSelectionCombo.select(cameraSelectionCombo.getItemCount() - 1);			
		}
		
	}
		
	public void configToGui(){
		ExampleConfig config = source.getConfig();
		
		fillCameraSelectionCombo();
		
		numImagesSpinner.setSelection(config.nImagesToAllocate);
		allowAutoconfigCheckbox.setSelection(config.enableAutoConfig);
		blackLevelButton.setSelection(config.blackLevelFirstFrame);
		softStartEnableCheckbox.setSelection(config.beginCaptureOnStartEvent);
		abortTrigEnableCheckbox.setSelection(config.abortEventOnCaptureComplete);
		
		numImagesSpinner.setForeground(swtGroup.getDisplay().getSystemColor(config.enableAutoConfig ? SWT.COLOR_GREEN : SWT.COLOR_WIDGET_FOREGROUND));
		
	}

	private void enableAcquireCheckboxEvent(Event event){		
		source.enableAcquire(enableAcquireCheckbox.getSelection());
	}
		
	private void diskMappedCheckboxEvent(Event e){
		source.setDiskMemoryLimit(diskMappedCheckbox.getSelection() ? Long.MIN_VALUE : Long.MAX_VALUE);
	}
	
	private void openCameraButtonEvent(Event event){		
		source.openCamera();
	}
	
	private void closeCameraButtonEvent(Event event){		
		source.closeCamera();
	}
	
	private void syncConfigButtonEvent(Event event){		
		source.syncConfig();
	}
			
	private void startCaptureButtonEvent(Event event){		
		source.startCapture();
	}

	private void abortButtonEvent(Event event){
		source.abort();
	}

	private void releaseMemoryButtonEvent(Event event){
		source.releaseMemory();
	}
		
	public ImgSource getSource() { return source;	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}
	
	private void doUpdate() {
		if(swtGroup.isDisposed())
				return;
		
		settingsCtrl.doUpdate();
	
		sourceStatusLabel.setText(source.getSourceStatus());
		ImageProcUtil.setColorByStatus(source.getSourceStatus(), sourceStatusLabel);
		
		captureStatusLabel.setText(source.getCaptureStatus());
		ImageProcUtil.setColorByStatus(source.getCaptureStatus(), captureStatusLabel);
		captureStatusLabel.setToolTipText(source.getCaptureStatus());
		
		ccdInfoLabel.setText(source.getCCDInfo());
		
		enableAcquireCheckbox.setSelection(source.isAcquireEnabled());
		
		ExampleConfig config = source.getConfig();

		DecimalFormat timeFmt = new DecimalFormat("#.###");
		double frameTimeMS = config.exposureTime + config.delayTime;
		double expTimeMS = config.exposureTime;
		double runTime = config.wrap ? Double.POSITIVE_INFINITY : (frameTimeMS * config.nImagesToAllocate / 1000.0);
		timingInfoLabel.setText("Exp=" + timeFmt.format(expTimeMS) + 
								"ms, Read+Dly = " + timeFmt.format(frameTimeMS - expTimeMS) + 
								"ms, Frame = " + timeFmt.format(frameTimeMS) +
								"ms, Run = " + timeFmt.format(runTime) + "s");
									
		startCaptureButton.setEnabled(true);

		configToGui();
		simplePanel.configToGUI();
		//fullPanel.configToGUI();
	}
	
	@Override
	public void destroy() {
		source.controllerDestroyed(this);
		swtGroup.dispose();
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return source; }
}