package imageProc.sources.capture.example.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.Algorithms;
import algorithmrepository.NotImplementedException;
import imageProc.core.ImgSource;
import imageProc.sources.capture.example.ExampleConfig;
import imageProc.sources.capture.example.ExampleSource;
import net.jafama.FastMath;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private ExampleSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo shutterModeCombo;
	private Combo timestampModeCombo;
	private Combo triggerModeCombo;
	private Button sensorCoolingButton;
	
	private Text exposureLengthTextbox;
	private Text delayTimeTextbox;
	private Text widthTextbox;
	private Text heightTextbox;
	
	private Combo binningCombo;
	private Spinner roiX0Text, roiY0Text, roiX1Text, roiY1Text;
	private Button roiMaxButton;
		
	public SimpleSettingsPanel(Composite parent, int style, ExampleSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		sensorCoolingButton = new Button(swtGroup, SWT.CHECK);
		sensorCoolingButton.setText("Cooling (T = ????????)");
		sensorCoolingButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		sensorCoolingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		sensorCoolingButton.setEnabled(false); //not implemented yet
		
		Label lEM = new Label(swtGroup, SWT.NONE); lEM.setText("Shutter:");
		shutterModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		shutterModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		shutterModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lTS = new Label(swtGroup, SWT.NONE); lTS.setText("Timestamp:");
		timestampModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		timestampModeCombo.setItems(ExampleConfig.timestampModes);
		timestampModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		timestampModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lHWT = new Label(swtGroup, SWT.NONE); lHWT.setText("H/W Trigger:");
		triggerModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		triggerModeCombo.setItems(ExampleConfig.triggerModes);
		triggerModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		triggerModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lIS = new Label(swtGroup, SWT.NONE); lIS.setText("Image size:");
		
		Label lW = new Label(swtGroup, SWT.NONE); lW.setText("Width:");
		widthTextbox = new Text(swtGroup, SWT.NONE);
		widthTextbox.setText("?");
		widthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		widthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lH = new Label(swtGroup, SWT.NONE); lH.setText("Height:");		
		heightTextbox = new Text(swtGroup, SWT.NONE);
		heightTextbox.setText("?");
		heightTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		heightTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Delay time:");		
		delayTimeTextbox = new Text(swtGroup, SWT.NONE);
		delayTimeTextbox.setText("50");
		delayTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		delayTimeTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT2 = new Label(swtGroup, SWT.NONE); lT2.setText("Exposure time:");
		//lT2.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("100");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Bining:");
		binningCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		binningCombo.setItems(new String[]{ "1x1", "2x2", "3x3", "4x4" });
		binningCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		binningCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lRX = new Label(swtGroup, SWT.NONE); lRX.setText("X:");
		roiX0Text = new Spinner(swtGroup, SWT.NONE);
		roiX0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiX0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lRY = new Label(swtGroup, SWT.NONE); lRY.setText("Y:");
		roiY0Text = new Spinner(swtGroup, SWT.NONE);
		roiY0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		roiMaxButton = new Button(swtGroup, SWT.PUSH);
		roiMaxButton.setText("Max");
		roiMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		roiMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { roiMaxEvent(); } });
		
		Label lRW = new Label(swtGroup, SWT.NONE); lRW.setText("X1:");
		roiX1Text = new Spinner(swtGroup, SWT.NONE);
		roiX1Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiX1Text.setTextLimit(4);
		roiX1Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX1Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lRH = new Label(swtGroup, SWT.NONE); lRH.setText("Y1:");
		roiY1Text = new Spinner(swtGroup, SWT.NONE);
		roiY1Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY1Text.setTextLimit(4);
		roiY1Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY1Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		Label lBL = new Label(swtGroup, SWT.NONE); lBL.setText("");
		
		
		configToGUI();
	}

	void configToGUI(){
		ExampleConfig config = source.getConfig();
		
		continuousButton.setSelection(config.wrap)	;
		//sensorCoolingButton.setSelection(config.cooling);
		
		//TODO: Set the GUI elements from the config 
		// timestampModeCombo.select(config.timestampMode);
		// triggerModeCombo.select(config.triggerMode);

		widthTextbox.setText(Integer.toString(config.width));
		heightTextbox.setText(Integer.toString(config.height));
		exposureLengthTextbox.setText(Integer.toString(config.exposureTime));
		delayTimeTextbox.setText(Integer.toString(config.delayTime));
	}
	
	void guiToConfig(){
		ExampleConfig config = source.getConfig();
		
		config.wrap = continuousButton.getSelection();
		
		//TODO: Set the config from the GUI elements 
		// config.timestampMode = timestampModeCombo.getSelectionIndex();
		// config.triggerMode = (short)triggerModeCombo.getSelectionIndex();

		config.width = Algorithms.mustParseInt(widthTextbox.getText());
		config.height = Algorithms.mustParseInt(heightTextbox.getText());
		config.exposureTime = Algorithms.mustParseInt(exposureLengthTextbox.getText());
		config.delayTime = Algorithms.mustParseInt(delayTimeTextbox.getText());
		
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		ExampleConfig config = source.getConfig();

		if(true)
			throw new NotImplementedException();
		
	}
	
	public void exposureMinMaxEvent(boolean max){
		ExampleConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
		configToGUI();
	}


	public void roiMaxEvent(){
		ExampleConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
