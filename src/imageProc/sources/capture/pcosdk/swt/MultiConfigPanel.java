package imageProc.sources.capture.pcosdk.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImgSource;
import imageProc.sources.capture.pcosdk.PCO_SDKConfig;
import imageProc.sources.capture.pcosdk.PCO_SDKSource;
import net.jafama.FastMath;
import pcosdkJNI.PCO_SDK;

public class MultiConfigPanel {
	public static final double log2 = FastMath.log(2);
			
	private PCO_SDKSource source;
	private Composite swtGroup;
			
	private Button multiConfigEnableCheckbox;
	private Spinner mcNumImagesSpinner[];
	private Spinner mcExpTimeSpinner[];
	private Spinner mcDelayTimeSpinner[];
	private Label mcRunTimeLabel[];
	private Button mcAutoConfigMaster;
	private Button mcAutoConfigRadio[];
	private Label exposureLabel;
	private Label delayLabel;
	
	public MultiConfigPanel(Composite parent, int style, PCO_SDKSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		multiConfigEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		multiConfigEnableCheckbox.setText("Enable");
		multiConfigEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		multiConfigEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
				
		Label lMCa = new Label(swtGroup, SWT.NONE); lMCa.setText("No.");
		Label lMCb = new Label(swtGroup, SWT.NONE); lMCb.setText("Num Imgs");
		exposureLabel = new Label(swtGroup, SWT.NONE); 
		exposureLabel.setText("Exposure [??]");
		exposureLabel.setToolTipText("Exposure units are the same as the main setting in the Simple tab");
		
		delayLabel = new Label(swtGroup, SWT.NONE); 
		delayLabel.setText("Delay [??]");
		delayLabel.setToolTipText("Delay units are the same as the main setting in the Simple tab");
		
		Label lMCr = new Label(swtGroup, SWT.NONE); lMCr.setText("Run [s]");
		
		mcAutoConfigMaster = new Button(swtGroup, SWT.CHECK);
		mcAutoConfigMaster.setText("Autoconfig");
		mcAutoConfigMaster.setToolTipText("If set allows one of number of images entries to be autoconfigured from other modules (e.g. Pilots)");
		mcAutoConfigMaster.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		mcAutoConfigMaster.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
		
		int numMultiCfg = 10;
		mcNumImagesSpinner = new Spinner[numMultiCfg];
		mcExpTimeSpinner = new Spinner[numMultiCfg];
		mcDelayTimeSpinner = new Spinner[numMultiCfg];
		mcRunTimeLabel = new Label[numMultiCfg];
		mcAutoConfigRadio = new Button[numMultiCfg];
		
		for(int i=0; i < numMultiCfg; i++){
			
			Label lMCn = new Label(swtGroup, SWT.NONE); lMCn.setText(i + ":");
			
			mcNumImagesSpinner[i] = new Spinner(swtGroup, SWT.NONE);
			mcNumImagesSpinner[i].setValues(0, 0, 100000, 0, 1, 10);
			mcNumImagesSpinner[i].setToolTipText("Number of images to capture before moving to next multi-config entry");
			mcNumImagesSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcNumImagesSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcExpTimeSpinner[i] = new Spinner(swtGroup, SWT.NONE);
			mcExpTimeSpinner[i].setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
			mcExpTimeSpinner[i].setToolTipText("Exposure time for each image in this set");
			mcExpTimeSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcExpTimeSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcDelayTimeSpinner[i] = new Spinner(swtGroup, SWT.NONE);
			mcDelayTimeSpinner[i].setValues(0, 0, Integer.MAX_VALUE, 0, 1, 10);
			mcDelayTimeSpinner[i].setToolTipText("Delay time for each image in this set");
			mcDelayTimeSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcDelayTimeSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcRunTimeLabel[i] = new Label(swtGroup, SWT.NONE);
			mcRunTimeLabel[i].setText("???????????????");
			mcRunTimeLabel[i].setToolTipText("How long this config entry should run for");
			mcRunTimeLabel[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			
			mcAutoConfigRadio[i] = new Button(swtGroup, SWT.RADIO);
			mcAutoConfigRadio[i].setText(Integer.toString(i));
			mcAutoConfigRadio[i].setSelection(false);
			mcAutoConfigRadio[i].setToolTipText("Should the number of images in this block be modified by the autoconfiguration");
			mcAutoConfigRadio[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcAutoConfigRadio[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			mcAutoConfigRadio[i].addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { autoconfClickEvent(event); } });
			
			
		}
		
		configToGUI();
	}

	protected void autoconfClickEvent(Event event) {
		//Helper: if the user tries to click a specific autoconf but the master is off, turn it on and set them
		if(!mcAutoConfigMaster.getSelection()) {
			mcAutoConfigMaster.setSelection(true);
			((Button)event.item).setSelection(true);
			
			guiToConfig();
		}
			
	}

	void configToGUI(){
		PCO_SDKConfig config = source.getConfig();
		
		exposureLabel.setText("Exposure [" + PCO_SDKConfig.units[config.exp_timebase] + "]");
		delayLabel.setText("Delay [" + PCO_SDKConfig.units[config.delay_timebase] + "]");
		
		multiConfigEnableCheckbox.setSelection(config.multiConfigNumImgs != null);
		boolean anyAutoconf = false;
		if(config.multiConfigAutoconf == null)
			config.multiConfigAutoconf = new boolean[mcNumImagesSpinner.length];
		if(config.multiConfigNumImgs != null){
			for(int i=0; i < mcNumImagesSpinner.length; i++){
				mcNumImagesSpinner[i].setSelection(config.multiConfigNumImgs[i]);
				mcExpTimeSpinner[i].setSelection(config.multiConfigExp[i]);
				mcDelayTimeSpinner[i].setSelection(config.multiConfigDly[i]);
				mcAutoConfigRadio[i].setSelection(config.multiConfigAutoconf[i]);
				anyAutoconf |= config.multiConfigAutoconf[i];

				double unit;
				switch(config.exp_timebase) {
					case PCO_SDK.TIMEBASE_NS: unit = 1e9; break;
					case PCO_SDK.TIMEBASE_US: unit = 1e6; break;
					case PCO_SDK.TIMEBASE_MS: unit = 1e3; break;
					default:
						unit = Double.NaN;
				}
				mcRunTimeLabel[i].setText(String.format("%.3fs", (config.multiConfigExp[i] + config.multiConfigDly[i]) * config.multiConfigNumImgs[i] / unit));
				
				mcNumImagesSpinner[i].setEnabled(true);
				mcExpTimeSpinner[i].setEnabled(true);
				mcDelayTimeSpinner[i].setEnabled(true);
				mcAutoConfigRadio[i].setEnabled(true);
			
			}
			mcAutoConfigMaster.setSelection(anyAutoconf);
		}else{
			for(int i=0; i < mcNumImagesSpinner.length; i++){
				mcNumImagesSpinner[i].setSelection(0);
				mcExpTimeSpinner[i].setSelection(0);
				mcDelayTimeSpinner[i].setSelection(0);	
				mcAutoConfigRadio[i].setSelection(false);
				mcRunTimeLabel[i].setText("            ");	
				
				mcNumImagesSpinner[i].setEnabled(false);
				mcExpTimeSpinner[i].setEnabled(false);
				mcDelayTimeSpinner[i].setEnabled(false);
				mcAutoConfigRadio[i].setEnabled(false);
			}
			mcAutoConfigMaster.setSelection(false);
		}
	}
	
	void guiToConfig(){
		PCO_SDKConfig config = source.getConfig();
		
		if(multiConfigEnableCheckbox.getSelection()){
			config.multiConfigNumImgs = new int[mcNumImagesSpinner.length];
			config.multiConfigExp = new int[mcNumImagesSpinner.length];
			config.multiConfigDly = new int[mcNumImagesSpinner.length];
			config.multiConfigAutoconf = new boolean[mcNumImagesSpinner.length];
			int totalImages = 0;
			
			boolean anyAutoconf = false;
			for(int i=0; i < mcNumImagesSpinner.length; i++){
				config.multiConfigNumImgs[i] = mcNumImagesSpinner[i].getSelection();
				config.multiConfigExp[i] = mcExpTimeSpinner[i].getSelection();
				config.multiConfigDly[i] = mcDelayTimeSpinner[i].getSelection();
				config.multiConfigAutoconf[i] = mcAutoConfigMaster.getSelection() && mcAutoConfigRadio[i].getSelection();
				anyAutoconf |= config.multiConfigAutoconf[i];
				totalImages += config.multiConfigNumImgs[i];
			}
			config.nImagesToAllocate = totalImages;
			
			//if the master autoconf checkbox is enabled but none is set, set the first one
			if(mcAutoConfigMaster.getSelection() && !anyAutoconf) {
				config.multiConfigAutoconf[0] = true;
				mcAutoConfigRadio[0].setSelection(true);
			}
			for(int i=0; i < mcNumImagesSpinner.length; i++){
				mcNumImagesSpinner[i].setEnabled(true);
				mcExpTimeSpinner[i].setEnabled(true);
				mcDelayTimeSpinner[i].setEnabled(true);
				mcAutoConfigRadio[i].setEnabled(mcAutoConfigMaster.getSelection());
			}
			
			//and we need to update the main GUI panel (via the sink)
			source.updateAllControllers();
		}else{
			config.multiConfigNumImgs = null;
			config.multiConfigExp = null;
			config.multiConfigDly = null;	
			config.multiConfigAutoconf = null;	
			
			for(int i=0; i < mcNumImagesSpinner.length; i++){
				mcNumImagesSpinner[i].setEnabled(false);
				mcExpTimeSpinner[i].setEnabled(false);
				mcDelayTimeSpinner[i].setEnabled(false);
				mcAutoConfigRadio[i].setEnabled(false);
			}
		}
		
	}
	
	private void multiConfigEvent(Event event) {
		boolean isMultiConfig = multiConfigEnableCheckbox.getSelection();

		guiToConfig();	
		
	}
	
	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
