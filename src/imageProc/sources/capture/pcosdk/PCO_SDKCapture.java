package imageProc.sources.capture.pcosdk;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import andorJNI.AndorSDKException;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.pcosdk.PCO_SDKConfig.ShutterMode;
import otherSupport.bufferControl.DirectBufferControl;
import pcosdkJNI.PCODevice;
import pcosdkJNI.PCO_SDK;
import pcosdkJNI.PCO_SDKException;
import test.Grab_AddBufferExtern;

/** Capture thread for PCO SDK Cameras */

public class PCO_SDKCapture extends Capture implements Runnable {
	private PCO_SDKSource source;
	
	private ByteBufferImage images[];
	
	private PCO_SDKConfig cfg;
		
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;

	
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
	
	private int mcCurrentEntry = -1;
	private int mcNextEntryAtImageIdx = -1;
	
	private void syncConfig() {
	
		//stop the camera and clear it's internal config to default
		camera.SetRecordingState(0); 
		camera.ResetSettingsToDefault();
		
		//sort out the multiconfig first
		if(cfg.multiConfigNumImgs != null){
			
			mcCurrentEntry = 0;
			cfg.delay_time = cfg.multiConfigDly[0];
			cfg.exp_time = cfg.multiConfigExp[0];
			mcNextEntryAtImageIdx = cfg.multiConfigNumImgs[0];
			
		}else{
			mcCurrentEntry = -1;
			mcNextEntryAtImageIdx = -1;
		}
		
		//fetch read-only/information things first
		cfg.description = camera.GetCameraDescription();
	
		cfg.cameraInfo = camera.GetInfo(PCO_SDK.INFO_STRING_CAMERA);
		cfg.sensorInfo = camera.GetInfo(PCO_SDK.INFO_STRING_SENSOR);
		
		short setup_id[] = new short[10];
		int setup_flag[] = new int[10];
		
		setup_flag[0] = cfg.shutterMode.sdkValue();
		
		camera.SetCameraSetup(setup_id[0], setup_flag);
		
		
		short temps[] = camera.GetTemperature();
		cfg.cameraTemperature = temps[0];
		cfg.sensorTemperature = temps[1] / 10.0;
		cfg.powerSupplyTemperature = temps[2];
		
		PCO_SDKException configFailed = null;
		int nFailedLast = Integer.MAX_VALUE;
		do{
			String errorCollect = "";
			int nFailed = 0;			
			try{
				//if the config is set-up, attempt to set it
				if(cfg.initialised){
					setStatus(Status.init, "Applying configuration...");
					
					//grabber.Set_Grabber_Timeout(cfg.grabberTimeoutMS);
					
					//camera.SetCameraToCurrentTime();
					
					cfg.storageMode = PCO_SDKConfig.STORAGEMODE_FIFO;
					try{
						camera.SetStorageMode(cfg.storageMode);
					}catch(RuntimeException err2){
						err2.printStackTrace();
					}
					
					camera.SetTimestampMode(cfg.timestampMode);
					//camera.SetTimebase(cfg.delay_timebase, cfg.exp_timebase);	 
					try{ camera.SetDelayExposureTime(cfg.delay_time, cfg.exp_time, cfg.delay_timebase, cfg.exp_timebase); }catch(PCO_SDKException err) { errorCollect += "SetDelayExposureTime(): " + err.getMessage() + "\n"; nFailed++; }
					try{ camera.SetBinning(cfg.binningX, cfg.binningY); }catch(PCO_SDKException err) { errorCollect += "SetBinning(): " + err.getMessage() + "\n"; nFailed++; }
					try{ camera.SetNoiseFilterMode(cfg.noiseFilterMode); }catch(PCO_SDKException err) { errorCollect += "SetNoiseFilterMode(): " + err.getMessage() + "\n"; nFailed++; }
					try{ camera.SetTriggerMode(cfg.triggerMode);  }catch(PCO_SDKException err) { errorCollect += "SetTriggerMode(): " + err.getMessage() + "\n"; nFailed++; }
					try{ camera.SetAcquireMode(cfg.acquireMode); }catch(PCO_SDKException err) { errorCollect += "SetAcquireMode(): " + err.getMessage() + "\n"; nFailed++; }
					try{ camera.SetConversionFactor((short)(cfg.conversionFactor * 100)); }catch(PCO_SDKException err) { errorCollect += "SetConversionFactor(): " + err.getMessage() + "\n"; nFailed++; }
					
					//the camera just fails on invalid ROIs and doesn't tell us the min/max, so we make some attempt to clamp them here
					if(cfg.roiX0 < 1) cfg.roiX0 = 1;
					if(cfg.roiY0 < 1) cfg.roiY0 = 1;
					short maxX1 = (short)(cfg.sensorWidth() / cfg.binningX);
					short maxY1 = (short)(cfg.sensorHeight() / cfg.binningY);
					if(cfg.roiX1 > maxX1) cfg.roiX1 = maxX1;
					if(cfg.roiY1 > maxY1) cfg.roiY1 = maxY1;
					try{ camera.SetROI(cfg.roiX0, cfg.roiY0, cfg.roiX1, cfg.roiY1);  }catch(PCO_SDKException err) { errorCollect += err.getMessage() + "\n"; nFailed++; }				
					
			
					/*if(cfg.description.wNumADCsDESC > 1){
						cfg.adcOperationMode = 2;
						camera.SetADCOperation(cfg.adcOperationMode);
					}*/
			
					cfg.pixelrate = camera.GetPixelRate();
			
					camera.SetBitAlignment(cfg.bitAlignment);
					
				}
				setStatus(Status.init, "Getting configuration...");
	
				//test every exp/dly pair
				if(cfg.multiConfigNumImgs != null){
					int firstFilled = -1;
					for(int i=0; i < cfg.multiConfigNumImgs.length; i++){
						if(cfg.multiConfigNumImgs[i] > 0){
							if(firstFilled < 0)
								firstFilled = i;
							try{ 
								camera.SetDelayExposureTime(cfg.multiConfigDly[i], cfg.multiConfigExp[i], cfg.delay_timebase, cfg.exp_timebase);
							}catch(PCO_SDKException err) { 
								errorCollect += "Delay/ExposureTime[Multi "+i+"]: " + err.getMessage() + "\n";
								nFailed++;
							}
							//camera.ArmCamera(); //don't think this is needed to check exposure/delay times
							
						}
					}
					try{ 
						camera.SetDelayExposureTime(cfg.multiConfigDly[firstFilled], cfg.multiConfigExp[firstFilled], cfg.delay_timebase, cfg.exp_timebase); //go back to the starting one
					}catch(PCO_SDKException err) { 
						errorCollect += err.getMessage() + "\n";
						nFailed++;
					}
				}
				
				
			}catch(PCO_SDKException err){				
				errorCollect += "Set parameter failed: " + err.toString() + "\n";
				nFailed++;
			}
			if(nFailed == nFailedLast){ //doesn't seem to improve
				logr.log(Level.WARNING, "PCO_SDKException: Still " + nFailed + " config sync failures, giving up: ", errorCollect);
				configFailed = new PCO_SDKException(errorCollect); //will get thrown after the get
				break;
			}
			nFailedLast = nFailed;
		}while(nFailedLast > 0);
		
		try {
			setStatus(Status.init, "Applying configuration...");
			camera.ArmCamera(); //will throw PCOCameraSDKException if config is invalid
			
			//camera.GetCameraHealthStatus();
		}catch(PCO_SDKException err) {
			//configFailed = err;
			err.printStackTrace();
		}	
		
		
		//now re-get everything, so that our config represents whats actually in the camera
		setup_flag = camera.GetCameraSetup(setup_id);
		switch(setup_flag[0]) {
			case PCO_SDK.PCO_EDGE_SETUP_ROLLING_SHUTTER: cfg.shutterMode = ShutterMode.Rolling_Shutter; break;
			case PCO_SDK.PCO_EDGE_SETUP_GLOBAL_SHUTTER: cfg.shutterMode = ShutterMode.Global_Shutter; break;
			case PCO_SDK.PCO_EDGE_SETUP_GLOBAL_RESET: cfg.shutterMode = ShutterMode.Global_Reset; break;
			default:
				cfg.shutterMode = ShutterMode.Unknown;
				logr.log(Level.WARNING, "Unrecognised shutter mode for SDK value " + setup_flag[0]);
		
		}
		
		int ret[];
		short sRet[];
		
		try{
			cfg.storageMode = camera.GetStorageMode();
		}catch(RuntimeException err2){
			err2.printStackTrace();
			cfg.storageMode = Short.MIN_VALUE;
		}
		cfg.bitAlignment = camera.GetBitAlignment();
		cfg.triggerMode = camera.GetTriggerMode();
		cfg.acquireMode = camera.GetAcquireMode();
		cfg.noiseFilterMode = camera.GetNoiseFilterMode();
		cfg.conversionFactor = camera.GetConversionFactor() / 100.0;
		
		ret = camera.GetActualSize(); cfg.imageWidth = ret[0]; cfg.imageHeight = ret[1];
		ret = camera.GetDelayExposure(); cfg.delay_time = ret[0]; cfg.exp_time = ret[1]; cfg.delay_timebase = ret[2]; cfg.exp_timebase = ret[3];
		ret = camera.GetCOCRuntime(); cfg.frameTimeS = ret[0]; cfg.frameTimeNS = ret[1];
		sRet = camera.GetROI(); cfg.roiX0 = sRet[0]; cfg.roiY0 = sRet[1]; cfg.roiX1 = sRet[2]; cfg.roiY1 = sRet[3];
		sRet = camera.GetBinning(); cfg.binningX = sRet[0]; cfg.binningY = sRet[1];
		
		
		if(configFailed != null)
			throw configFailed;
				
		cfg.initialised = true;
		
		source.configChanged();
	}
	
	/** Output debug info for the high speed debugging stuff */
	private boolean spinDebug = true;
		
	/** Internal state */
	private PCO_SDK camera;

	private PCODevice[] availableCameras = null;	

	public PCO_SDKCapture(PCO_SDKSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera 
	}
		
	@Override
	public void run() {
		threadBusy = true;
		try{
			if(camera != null){				
				deinit();
				throw new RuntimeException("Camera driver already open, aborting");
			}
			
			
			setStatus(Status.init, "Initialising camera...");
			initDevice();
			

			
			//signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Camera init done.");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();
					}
								
					
					if(signalCapture){
						signalCapture = false;

						setStatus(Status.init, "Allocating image memory...");		
						
						initImages();
									
						setStatus(Status.init, "Config done, starting capture...");
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("PCOCam/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");
					}else {
						
						setStatus(Status.completeOK, "Config done.");						
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal", err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			logr.log(Level.WARNING, "PCOCam thread caught Exception: ", err);
			setStatus(Status.errored, "ERROR: " + err.getMessage());
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				logr.log(Level.WARNING, "PCOCam thread caught exception de-initing camera.", err);				
				setStatus(Status.errored, "ERROR: " + err.getMessage());
			}
		}
	}
	
	private void initDevice() {

		camera = new PCO_SDK();
		
		if(availableCameras == null || cfg.serialNumber < 0)
			availableCameras = camera.ScanCameras();
		
		if(cfg.serialNumber < 0)
			throw new RuntimeException("No camera selected.");
		
		PCODevice devFound = null; 
		for(PCODevice dev : availableCameras) {
			if(dev.SerialNumber == cfg.serialNumber)
				devFound = dev;
		}
		if(devFound == null)
			throw new RuntimeException("No camera with serial number " + cfg.serialNumber + " found");
		
		
		camera.OpenCameraDevice(devFound.id);

		int retSerialNumber[] = new int[1];
		cfg.camType = camera.GetCameraType(retSerialNumber);
		cfg.serialNumber = retSerialNumber[0];
		cfg.interfaceName = new String(devFound.interfaceName, StandardCharsets.UTF_8);
		cameraOpen = true;
	}
	
	
	private void initImages(){
		
		long imgDataSize = cfg.imageSizeBytes();
		int width = cfg.imageWidth; //.grabberWidth;
		int height = cfg.imageHeight; //.grabberHeight;
		int bitDepth = 16;
		double bytesPerPixel = bitDepth / 8;
		
		if(imgDataSize < height*width*bytesPerPixel){
			throw new RuntimeException("Camera wants less memory than it needs for the image???");
		}
			
		int footerSize = 0;
		
		if(imgDataSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		try{
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
	
	}
	
	private void doCapture() {
		//if(camera.GetRecordingState() != 1)
		//	throw new RuntimeException("Camera isn't in recording state");
		camera.CancelImages();
		camera.SetRecordingState(0);
		
		setStatus(Status.awaitingSoftwareTrigger, "Capture started. Acquisition not started (awaiting soft start)");	
		
		IntBuffer statusBuffer = null;
		
		try{
			
			double time[] = new double[cfg.nImagesToAllocate]; //seconds since first image
			long timestampNS[] = new long[cfg.nImagesToAllocate]; // full nanoSecs since midnight 1970 UTC
			long wallCopyTimeMS[] = new long[cfg.nImagesToAllocate];
			int imageNumberStamp[] = new int[cfg.nImagesToAllocate];
			long nanoTime[] = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("PCOCam/timestampNS", timestampNS, true);
			source.setSeriesMetaData("PCOCam/wallCopyTimeMS", wallCopyTimeMS, true);
			source.setSeriesMetaData("PCOCam/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("PCOCam/nanoTime", nanoTime, true);
			source.setSeriesMetaData("PCOCam/time", time, true);
			
			statusBuffer = DirectBufferControl.allocateDirect(cfg.nImagesToAllocate * 4).asIntBuffer();
			for(int i=0; i < cfg.nImagesToAllocate; i++) {				
				statusBuffer.put(i, PCO_SDK.PCO_ERROR_WRONGVALUE);
			}
			
			camera.SetImageParameters((short)cfg.imageWidth, (short)cfg.imageHeight, PCO_SDK.IMAGEPARAMETERS_READ_WHILE_RECORDING);
			//camera.SetRecordingState(1);
			
			boolean addBuffersOnlyWhenRecording;
			switch(cfg.addBuffersTiming) {
				case BeforeRecording: addBuffersOnlyWhenRecording = false; break;
				case DuringRecording: addBuffersOnlyWhenRecording = true; break;
				default:
					if (cfg.interfaceName.contains("USB"))
						addBuffersOnlyWhenRecording = true;
					else if(cfg.interfaceName.contains("CLHS"))
						addBuffersOnlyWhenRecording = false;
					else
						throw new RuntimeException("Unable to automatically determine if buffers should be added before or during recording"
								+ "because there is no 'USB' or 'CLHS' in camera name.");
			}
			
			if(!addBuffersOnlyWhenRecording) {
				for (int b = 0; b < cfg.nImagesToAllocate; ++b) {
					//FIXME: No write lock!
					camera.AddBufferExtern((short)1, 0, 0, 0, images[b].getReadOnlyBuffer(), statusBuffer, b);
				}
			}
			
			long wallCopyTimeMS0 = Long.MIN_VALUE;
			long timestampNS0 = Long.MIN_VALUE;
			int nextImgIdx = 0;
			do{
				int imgIdx = -1;
				try {
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try{
						images[nextImgIdx].invalidate(false);
						do{
								
							if(!isAcqusitionStarted()){ //don't actually start until acquire signal is set
								while(!signalAcquireStart){
									Thread.sleep(0, 100000);
								}
								
								try {
									camera.SetRecordingState(1);
									/* for some reason, for the USB camera this sometimes fails with "arm is not possible while record active".
									 At this point the camera isn't running and this isn't the Arm call, but ... yea, nevermind, we just skip it.
									 
									 WaitForNextBuffer will fail with 'read impossible' too but then when we do and then fail, it will work next time
									 for some reason. 

									This all seems to happen when the X size of the image isn't on some boundary.
									  */
								}catch(PCO_SDKException err) {
									if((err.getErrorCode() & PCO_SDK.PCO_ERROR_CODE_MASK) 
													== (PCO_SDK.PCO_ERROR_FIRMWARE_RECORD_MUST_BE_OFF & PCO_SDK.PCO_ERROR_CODE_MASK)){
										logr.warning("Driver complains that camera is already running, ignorings");
										//camera.SetRecordingState(0);
										
										//long addr = camera.WaitforNextBufferAdr(null, cfg.grabberTimeoutMS);
										
										//camera.CancelImages();
										
										//camera.SetRecordingState(1);
										//throw new RuntimeException("Everything is broken :(");
									}else
										throw err;
									
								}
								
								setStatus(Status.awaitingHardwareTrigger, "Capturing..., (awaiting hardware trigger)");
								if(addBuffersOnlyWhenRecording) {
									for (int b = 0; b < cfg.nImagesToAllocate; ++b) {
										//FIXME: No write lock!
										camera.AddBufferExtern((short)1, 0, 0, 0, images[b].getReadOnlyBuffer(), statusBuffer, b);
									}
								}
																
								int statuses[] = camera.GetCameraHealthStatus();
								logr.log(Level.FINE, "Camera health status: "
														+ "warn = " + statuses[0] 
														+ ", error = " + statuses[1]
														+ ", status = " + statuses[2]);
							}
							
							if(isAcqusitionStarted() && !signalAcquireStart) {
								camera.SetRecordingState(0);
								setStatus(Status.awaitingSoftwareTrigger, "Capturing, but was stopped (intentionally).");
							}
							
							if(signalDeath || signalAbort){
								logr.info("Aborting capture loop.");
								throw new CaptureAbortedException();
							}
							
							try{
								cfg.grabberTimeoutMS=200;
								long addr = camera.WaitforNextBufferAdr(null, cfg.grabberTimeoutMS);
								long wallCopyTime = System.currentTimeMillis();
								
								imgIdx = bulkAlloc.getImageIndex(addr);
								//spinLog("Got buffer at " + addr + ", which is image " + imgIdx);
								wallCopyTimeMS[imgIdx] = wallCopyTime;								
								
								if(status != Status.capturing) //don't call this unnecessarily
									setStatus(Status.capturing, "Capturing..., acquisition started");
								
							}catch(PCO_SDKException err){

								//timeout is fine, we just loop back and run WaitForNextBufferAdr again
								if((err.getErrorCode() & PCO_SDK.PCO_ERROR_CODE_MASK) == (PCO_SDK.PCO_ERROR_TIMEOUT & PCO_SDK.PCO_ERROR_CODE_MASK))
									continue;
									
								//A buffer was cancelled? what? why? ignore it!
								if((err.getErrorCode() & PCO_SDK.PCO_ERROR_CODE_MASK) == (0x80332010 & PCO_SDK.PCO_ERROR_CODE_MASK)) {
									logr.log(Level.WARNING, "Buffer was cancelled - ignored");
									continue;
								}
								throw err;
							}
							
							int status = statusBuffer.get(imgIdx);

							// always check driver status for errors:
							if (status != PCO_SDK.PCO_NOERROR) {
								throw new PCO_SDKException("BufferStatus", status);
							}

							if(cfg.grabberWaitDelayUS != 0){
								try {
									//cfg.grabberWaitDelayMS = 1;
									int delayMS = (int)(cfg.grabberWaitDelayUS / 1000);
									Thread.sleep(delayMS, (cfg.grabberWaitDelayUS - delayMS*1000) * 1000);
								} catch (InterruptedException e) { }
							}
							
							break;
						}while(true); //timeout loop
						
						//spinLog(".doCapture(): Buffer copied to images[" + nextImgIdx + "]");
						
						if(wallCopyTimeMS0 == Long.MIN_VALUE)
							wallCopyTimeMS0 = wallCopyTimeMS[imgIdx];
						
						if(cfg.timestampMode == 1 || cfg.timestampMode == 2){
							processBCDTimestamp(imageNumberStamp, timestampNS, imgIdx);
							
							if(timestampNS0 == Long.MIN_VALUE){
								timestampNS0 = timestampNS[imgIdx];
							}
							
							//use timestamp to make a quick 'time since first time' time 
							time[imgIdx] = (double)((timestampNS[imgIdx] - timestampNS0) / 1000) / 1e6;
							nanoTime[imgIdx] = (timestampNS[imgIdx] - timestampNS0 + wallCopyTimeMS0*1_000_000L);
						}else{
							
							//no timestamp, so our wall copy time is the best we can do							
							time[imgIdx] = (wallCopyTimeMS[imgIdx] - wallCopyTimeMS0) / 1e3;
							nanoTime[imgIdx] = wallCopyTimeMS[imgIdx] * 1_000_000L; 
						}
						
					}finally{ writeLock.unlock(); }
				} catch (InterruptedException e) {
					logr.info("Interrupted while waiting to write to java image.");
					return;
				}
				
				source.imageCaptured(imgIdx);
				
				if(imgIdx != nextImgIdx)
					logr.warning("Got image " + imgIdx + " but we were expecting " + nextImgIdx);
				nextImgIdx = imgIdx;
				
				nextImgIdx++;
				if(nextImgIdx >= cfg.nImagesToAllocate && cfg.wrap) {
					nextImgIdx = cfg.blackLevelFirstFrame ? 1 : 0;
					//nasty hack, rebuffer
					for (int b = 0; b < cfg.nImagesToAllocate; ++b) {
						//FIXME: No write lock!
						camera.AddBufferExtern((short)1, 0, 0, 0, images[b].getReadOnlyBuffer(), statusBuffer, b);
					}
				}
				
				if(cfg.multiConfigNumImgs != null && nextImgIdx >= mcNextEntryAtImageIdx){
					nextMultiConfigEntry(nextImgIdx);
				}
				
			}while(nextImgIdx < cfg.nImagesToAllocate);
			
		}finally{
			//grabber.Stop_Acquire();
			camera.CancelImages();
			camera.SetRecordingState(0);
			
			if(statusBuffer != null) {
				//hard deallocate because we can't trust java to do this right
				//although this one doesn't seem to @have a cleaner", whatever that means
				DirectBufferControl.freeBuffer(statusBuffer);
			}				
		}

	}
	
	private void nextMultiConfigEntry(int nextImgIdx) {
		
		while(nextImgIdx >= mcNextEntryAtImageIdx){
			mcCurrentEntry++;				
			if(mcCurrentEntry >= cfg.multiConfigNumImgs.length)
				mcCurrentEntry = 0; //wrap, maybe the user wants that
				
			mcNextEntryAtImageIdx += cfg.multiConfigNumImgs[mcCurrentEntry];				
		}

		cfg.delay_time = cfg.multiConfigDly[mcCurrentEntry];
		cfg.exp_time = cfg.multiConfigExp[mcCurrentEntry];		
		camera.SetDelayExposureTime(cfg.delay_time, cfg.exp_time, cfg.delay_timebase, cfg.exp_timebase);
	}
	
	/**
	 * Fills in Metadata arrays from timestamp in image
	 * 
		px0: image counter (MSB) (00...99)  
		px1: image counter (00...99)  
		px2: image counter (00...99) 
		px3: image counter (LSB) (00...99)
		
		px4: year (MSB) (20) 
		px5: year (LSB) (15...99) 
		px6: month (01...12)
		px7: day (01...31) 
		px8: h (00...23)
		px9: min (00...59) 
		px10: s (00...59) 
		px11: us * 10000 (00...99) 
		px12: us * 100 (00...99) 
		px13: us (00...99) 
		
		but, we have to ask... was Jesus born negative???
	 */
	private void processBCDTimestamp(int[] imageNumberStamp, long[] timestamp, int imageIdx){

		int shift = cfg.getShift(); //adjustment for weird bit alignment

		int vals[] = new int[14];
		
		ByteBuffer buffer = images[imageIdx].getReadOnlyBuffer();
		
		for(int i=0; i < vals.length; i++){
			byte b = buffer.get(2*i);
			b >>= shift;
			vals[i] = ((b & 0xF0)>>4)*10 + (b & 0x0F);
		}
		
		imageNumberStamp[imageIdx] = vals[0] * 1000000 
									+ vals[1] * 10000 
									+ vals[2] * 100 
									+ vals[3];
		
		// process the stupid date formatted pixels BCD nonsense into a nanoTime
		
		int year = vals[4] * 100;
		year += vals[5];
		int month = vals[6];
		int day = vals[7];
		int hour = vals[8];
		int min = vals[9];
		int secs = vals[10];
		int microsecs = vals[11] * 10000 +
						vals[12] * 100 +
						vals[13] * 1;
		
		Date d = new Date(year, month, day, hour, min, secs);
		long milisecs = d.getTime();
		
		long nanoTime = milisecs * 1_000_000L + microsecs * 1000L;
		
		//microseconds since midnight
		timestamp[imageIdx] = nanoTime; 
		/*
			System.out.println(String.format("%02X --  %02X %02X %02X %02X %d", 
					vals[9],vals[10],vals[11],vals[12],vals[13],
					timestamp[imageIdx]));
			*/
		//spinLog(".processTimestamp(): Timestamp data for images[" + imageIdx + "]: imageNum = "+imageNumberStamp[imageIdx] + ", us since midnight = " + timestamp[imageIdx]);
	

	}
	
	private void deinit() {
		logr.log(Level.FINE, "Deinitialising camera");
		if(camera != null){
			if(camera.isHandleValid()) {
				try {
					camera.CloseCamera();
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Error closing camera.", err);
				}
			}
			//camera.destroy();
			camera.cleanupLib();
			camera = null;
			
		}
	}
	
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		//thread.setPriority(Thread.MAX_PRIORITY);
		spinLog("Starting PCO capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(PCO_SDKConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(PCO_SDKConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thread to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
		
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				System.out.println("PCOCamCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("PCOCamCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("PCOCamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("PCOCamCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}
	
	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(PCO_SDKConfig config) { this.cfg = config;	}

	public PCODevice[] getAvailableCameras() { return availableCameras ; }

}
