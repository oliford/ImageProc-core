package imageProc.sources.capture.pcosdk;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.Algorithms;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.pcosdk.swt.PCO_SDK_SWTControl;
import pcosdkJNI.PCODevice;

/** Image source for PCO Cameras.
 * Contains the application and image handling side.
 * The image capture and device handling is done by PCOCamCapture 
 * 
 * @author oliford
 */
public class PCO_SDKSource extends CaptureSource implements ConfigurableByID {
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	private int lastCaptured;
	
	/** The low-level capturer, we should only have one of these */  
	private PCO_SDKCapture capture;
	
	/** The current config, should be kept always in sync with camera (if online)
	 * and with controller */
	private PCO_SDKConfig config;
	
	public PCO_SDKSource(PCO_SDKCapture capture, PCO_SDKConfig config) {
		this.capture = capture;
		this.config = (config != null) ? config : new PCO_SDKConfig();
	}
	
	public PCO_SDKSource() {
		capture = new PCO_SDKCapture(this);
		config = new PCO_SDKConfig();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by capture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	
	/** Called by capture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		lastCaptured = imageIndex;
		
		updateAllControllers();
				
	}
	
	@Override
	public PCO_SDKSource clone() {
		return new PCO_SDKSource(capture, config);
	}

	public void openCamera(){
		if(capture.isOpen()){
			System.err.println("openCamera(): Camera already open");
			return;
		}
		capture.setConfig(config);
		capture.initCamera();
	}

	public void startCapture(){
		if(capture.isBusy()){
			System.err.println("startCapture(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		

		capture.setConfig(config);
		capture.startCapture(config, images);
	}
	
	public void syncConfig(){
		if(capture.isBusy()){
			System.err.println("syncConfig(): Camera thread busy");
			return;
		}

		capture.setConfig(config);
		this.nCaptured = 0;
		capture.testConfig(config, images);
	}
		
	public void abort(){ capture.abort(false); }
	
	public void closeCamera(){ capture.closeCamera(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToAllocate + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). S/W start "				
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	public String getCCDInfo(){
		String camModel = config.cameraInfo;
				
		return "PCO " + ((camModel != null) ? camModel : "???") +
				": " + config.sensorWidth() + " x " + config.sensorHeight();
	}
	
	public boolean isActive(){ return capture.isOpen(); }
	
	public boolean isBusy(){ return capture.isBusy(); }
	
	@Override
	public boolean isIdle() { return capture == null || !capture.isOpen() || !capture.isBusy(); }

	public PCO_SDKConfig getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (PCO_SDKConfig)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new PCO_SDK_SWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	//public void setAutoExposure(boolean enable){ this.autoExposure = enable; }
	//public boolean getAutoExposure(){ return this.autoExposure; }

	public void releaseMemory() {
		capture.abort(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}

	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	@Override
	public void close() {
		abort();
		closeCamera();		
	}

	@Override
	public boolean open(long timeoutMS) {
		
		openCamera();
		long t0 = System.currentTimeMillis();
		while(!capture.isOpen()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) { }
			
			if((System.currentTimeMillis() - t0) > timeoutMS){
				throw new RuntimeException("Timed out waiting for camera to open. Need a longer warm-up time??");
			}
		}
		
		//do a config sync as-is
		syncConfig();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) { }

		
		return true;
	}


	public void setDiskMemoryLimit(long maxMemory) { capture.setDiskMemoryLimit(maxMemory); }
	
	public long getDiskMemoryLimit(){  return capture.getDiskMemoryLimit(); }
	
	@Override
	public String toShortString() { return "PCO.SDK"; }
	
	@Override
	protected final Capture getCapture() { return capture; }
	
	@Override
	public boolean setConfigParameter(String param, String value) {
		if(param.equals("runLength")){
			double runLengthSecs = Algorithms.mustParseDouble(value);
			config.nImagesToAllocate = (int)(runLengthSecs * 1000.0 / config.frameTimeMS() + 0.5);
			return true;
		}
		
		return false;
	}
	
	@Override
	public String getConfigParameter(String param) {
		if(param.equals("runLength")){
			return String.format("%f", config.nImagesToAllocate * config.frameTimeMS() / 1000.0);
			
		}else if(param.equals("exposureTime")){
			return String.format("%f", config.exposureTimeMS() / 1000.0);
			
		}else if(param.equals("frameTime")){
			return String.format("%f", config.frameTimeMS() / 1000.0);
			
		}
		
		return null;
	}
	
	@Override
	public int getFrameCount() { 
		if(config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			for(int i=0; i < config.multiConfigAutoconf.length; i++) {
				if(config.multiConfigAutoconf[i])
					return config.multiConfigNumImgs[i];
			}
			throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		}else {
			return config.nImagesToAllocate;
		}
	}
	
	@Override
	public void setFrameCount(int frameCount) {  
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		
		if(config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			boolean entryFound = false;
			int totalImages = 0;
			
			//multi config, find which one (if any) can be autoconfiged
			for(int i=0; i < config.multiConfigAutoconf.length; i++) {
				if(config.multiConfigAutoconf[i]) {
					config.multiConfigNumImgs[i] = frameCount;
					updateAllControllers();
					entryFound = true;
				}
				totalImages += config.multiConfigNumImgs[i];
				
			}
			config.nImagesToAllocate = totalImages;
			if(!entryFound)
				throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		}else {
			config.nImagesToAllocate = frameCount;
		}
		
		updateAllControllers();
	}
	
	@Override
	public long getFramePeriod() { 
		if(config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			for(int i=0; i < config.multiConfigAutoconf.length; i++) {
				if(config.multiConfigAutoconf[i]) {
					long framePeriodUS = 0;
					switch(config.exp_timebase) {
						case 0: framePeriodUS += (long)(config.multiConfigExp[i] * 1e-3); break;
						case 1: framePeriodUS += (long)(config.multiConfigExp[i] * 1); break;
						case 2: framePeriodUS += (long)(config.multiConfigExp[i] * 1e3); break;
						default: throw new IllegalArgumentException("Unknown timebase exposure " + config.exp_timebase);
					}
				
					switch(config.delay_timebase) {
						case 0: framePeriodUS += (long)(config.multiConfigDly[i] * 1e-3); break;
						case 1: framePeriodUS += (long)(config.multiConfigDly[i] * 1); break;
						case 2: framePeriodUS += (long)(config.multiConfigDly[i] * 1e3); break;
						default: throw new IllegalArgumentException("Unknown timebase delay " + config.exp_timebase);
					}
					
					return framePeriodUS;
				}
			}
			throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		}else {
			return (long)((config.exposureTimeMS() + config.delayTimeMS()) * 1000.0);
		} 
	}
	
	@Override
	public void setFramePeriod(long framePeriodUS) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		
		if(config.multiConfigNumImgs != null && config.multiConfigAutoconf != null) {
			//multi config, find which one (if any) can be autoconfiged
			for(int i=0; i < config.multiConfigAutoconf.length; i++) {
				if(config.multiConfigAutoconf[i]) {
					config.multiConfigExp[i] = (int)framePeriodUS - config.multiConfigDly[i];
					updateAllControllers();
					return;
				}
			}
			throw new RuntimeException("MultiConfig is enabled but no multiconfig entries are marked for autoconfiguration");
		}else {
			long exp_timeUS = framePeriodUS - (long)(config.delayTimeMS()*1000.0);
			switch(config.exp_timebase) {
				case 0: config.exp_time = (int)(exp_timeUS * 1e3); break;
				case 1: config.exp_time = (int)(exp_timeUS * 1); break;
				case 2: config.exp_time = (int)(exp_timeUS * 1e-3); break;
				default: throw new IllegalArgumentException("Unknown timebase " + config.exp_timebase);
			}
		}
		
		updateAllControllers();
	}

	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig;	}

	public PCODevice[] getAvailableCameras() { return capture.getAvailableCameras(); }
}
