package imageProc.sources.capture.ebus;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

import ebusJNI.EBus;
import ebusJNI.EBusException;
import ebusJNI.EBusParameter;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import otherSupport.SettingsManager;


/** Thread runner used to copy in images.
 *  
 * It contains all the actual JNI calls to the camera library 
 * 
 * I think the SDK is thread safe but not entirely sure.
 */
public class EBusCapture extends Capture implements Runnable {
		
	private EBusSource source;
		
	private ByteBufferImage images[];
	
	private EBusConfig cfg;
		
	private Thread thread;
	
	/** Internal state */
	private EBus ebus;
	
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;
		
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
		
	/** Completely reset the configuration to defaults */
	private boolean resetConfig = false;
	
	private long wallCopyTimeMS0 = Long.MIN_VALUE;
	private long wallCopyTimeMS[];
	
	private long timestampStart0 = Long.MIN_VALUE;
	private long timestampStart[], timestampEnd[];
	
	private long nanoTime[];
	private double time[];
	
	public EBusCapture(EBusSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera
		
	}
		
	@Override
	public void run() {
		threadBusy = true;
		try{
			if(ebus != null){
				ebus.close();
				throw new RuntimeException("Camera driver already open, aborting both");
			}
			
			setStatus(Status.init, "Capture init");
			initDevice();
			
			signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Init Ready");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();					
					}
					
					if(!signalCapture){
						setStatus(Status.completeOK, "Config done.");						
						
					}else{
						signalCapture = false;
									
						setStatus(Status.init, "Allocating");		
						
						initImages();
						
						//source.addNonTimeSeriesMetaDataMap(cfg.toMap("Ebus/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");					
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal: " + err, err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
					err.printStackTrace();
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("EBus thread caught Exception:" + err);
			setStatus(Status.errored, "Aborted/Errored:" + err);
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				System.err.println("EBus thread caught exception de-initing camera.");
				err.printStackTrace();
			}
		}
	}
	
	
	private void initDevice() {
		setStatus(Status.init, "Initialising library... (May take a few minutes)");
		 
		setStatus(Status.init, "Opening camera... (May take a few minutes)");
		ebus = new EBus(cfg.connectionID);
		
		logr.info(".initCamera(): Opened camera: Model = " + ebus.getStringParameter("DeviceModelName")
					+ ", ID=" +  ebus.getStringParameter("DeviceID") 
					+ ", Version=" + ebus.getStringParameter("DeviceVersion") );
		
		cameraOpen = true;				
		
	}
	
	private void syncConfig(){
		
		//check consistency. The ROI 'setting' has to be a bit weird to allow ROIs to be stored that arn't loaded to camera
		/*if(cfg.enableROIs){
			Parameter roisParam = cfg.getFeature("Rois");
			if(roisParam != null)
				roisParam.toSet = true;
		}*/
		
		if(cfg.isContinuous() && cfg.nImagesToAllocate <= 1){
			throw new IllegalArgumentException("nImage=1 and contiuous is on. This is a ridiculous thing to ask for!");
		}
		
		writeConfig();
		
		/*int failed[] = camera.CommitParameters();
		if(failed.length > 0){
			StringBuilder strB = new StringBuilder("Invalid parameters: ");
			
			for(int paramID : failed){
				strB.append(EBusParameters.getName(paramID));
				strB.append(", ");
			}
			
			String str = strB.toString();
			logr.warning("Ebus.syncConfig(): " + str);
			
			throw new RuntimeException(str);
		}*/
		
		readConfig();
		
		//ensure consistency of number of images
		//if(!cfg.isContinuous()){
			//cfg.nImagesToAllocate = (int)cfg.intFeature("AcquisitionFrameCount");
		//}
		
		
		source.configChanged();
		logr.info(".syncConfig(): config sync done");
	}
	
	private void writeConfig(){
		//set the frame count parameter to the number we want to acquire, or it's maximum
		//it doesn't really seem to matter since acquisition continues anyway 
		EBusParameter f = cfg.getFeature("AcquisitionFrameCount");		
		cfg.setFeature("AcquisitionFrameCount", (f == null || f.maxValue == null || ((Integer)f.maxValue > cfg.nImagesToAllocate)) 
															? cfg.nImagesToAllocate : (Integer)f.maxValue);
		
		
		EBusException caughtErr;
		String firstFailedParamName;
		String previousFailedParamName = null;
		do {
			caughtErr = null;
			firstFailedParamName = null;
			
			for(EBusParameter param : cfg.getAllFeatures().values()){
				try{
					if(!param.toSet || !param.isImplemented || !param.isWritable || !param.isFilled)
						continue;
					
					switch(param.type){
						case EBusParameter.TYPE_INTEGER:
							int valI = (int)param.value;
							ebus.setIntegerParameter(param.name, valI);					
							logr.fine("Set '"+param.name+"' = " + valI + " (int)");
							break;
						case EBusParameter.TYPE_ENUM:
				    		int valE = (int)param.value;
				    		ebus.setEnumParameterValue(param.name, valE);
				    		logr.fine("Set '"+param.name+"' = " + valE + " (Enum " + param.enumGetSelectedName());
				    		break;
						case EBusParameter.TYPE_BOOLEAN:
							boolean valB = (boolean)param.value; 
							ebus.setBooleanParameter(param.name, valB);					 
							logr.fine("Set '"+param.name+"' = " + param.value + " (bool)");
							break;
							
						case EBusParameter.TYPE_STRING:
							String valS = param.value.toString();
							ebus.setStringParameter(param.name, valS);
							break;
						case EBusParameter.TYPE_FLOAT:
							float valF =  param.value instanceof Double 
											? (float)(double)(Double)param.value //java can really silly sometimes :/
											: (float)param.value;
							ebus.setFloatParameter(param.name, valF);					
							logr.fine("Set '"+param.name+"' = " + valF + " (float)");
							break;
							
						case EBusParameter.TYPE_COMMAND:
						case EBusParameter.TYPE_REGISTER:
						default:
			    			logr.warning("Unrecognised parameter type '"+param.type+" for param '"+param.name+"'");
					}
				}catch(EBusException err){
					System.out.println("Caught " + err.getMessage() + " on " + param.name);
					if(firstFailedParamName == null) {
						firstFailedParamName = param.name;
						caughtErr = err;
					}
					continue; //try all other parameters first
				}
			}

			//if the same parameter fails again, give up
			if(firstFailedParamName != null && firstFailedParamName.equals(previousFailedParamName))
				throw new RuntimeException("Repeatedly caught exception setting parameter '"+firstFailedParamName+".", caughtErr);
			
			previousFailedParamName = firstFailedParamName; //save to check next time
			
		}while(firstFailedParamName != null);
		
		//got through with no failures, great!
		
	}
	
	private void readConfig(){
		//get all params and update/add them to our params list
		String[] params = ebus.getParameterNames();
				
		//read back
		for(int i=0; i < params.length; i++){
			String name = params[i];
			if(name.equals("DeviceModelName"))
				System.out.println("fart");
			
			EBusParameter param = cfg.getFeature(name);
			if(param == null){
				int type = ebus.getParameterType(name);
				param = new EBusParameter(name, 0, type);
				cfg.addParameter(param);
			}
			param.setFlags(ebus.getParameterFlags(name));
			if(!param.isReadable)
				continue;

			switch(param.type){
	
				case EBusParameter.TYPE_INTEGER:
					param.value = ebus.getIntegerParameter(param.name);
					param.minValue = ebus.getIntegerParameterMin(param.name);
					param.maxValue = ebus.getIntegerParameterMax(param.name);
					param.isFilled = true;
					logr.fine("Get '"+param.name+"' = " + param.value + " (int): " + param.minValue + " < x < " + param.maxValue);
					break;
				case EBusParameter.TYPE_ENUM:
		    		int valE = ebus.getEnumParameterValue(param.name);
					param.value = valE;
					
					if(!param.isFilled)	{
						ebus.fillEnumEntries(param);
						param.isFilled = true;
					}
						
		    		logr.fine("Get '"+param.name+"' = " + valE + " (Enum, " + param.enumGetSelectedName() + ")");
		    		break;
				case EBusParameter.TYPE_BOOLEAN:
					boolean valB = ebus.getBooleanParameter(param.name);
					param.value = valB;
					param.isFilled = true;
					logr.fine("Get '"+param.name+"' = " + param.value + " (bool)");
					break;
				case EBusParameter.TYPE_STRING:
					String valS = ebus.getStringParameter(param.name);
					param.value = valS;
					param.isFilled = true;
					logr.fine("Get '"+param.name+"' = " + param.value + " (string)");
					break;
				case EBusParameter.TYPE_FLOAT:
					param.value = ebus.getFloatParameter(param.name);
					param.minValue = ebus.getFloatParameterMin(param.name);
					param.maxValue = ebus.getFloatParameterMax(param.name);
					param.isFilled = true;
					logr.fine("Get '"+param.name+"' = " + param.value + " (int): " + param.minValue + " < x < " + param.maxValue);
					break;
	    		default:
	    			logr.warning("Unrecognised parameter type '"+param.type+" for param '"+param.name+"'");
			}
			
			
		}	
	}
		
	private void initImages(){

		int frameSize = cfg.intFeature("PayloadSize");
		
		int width = cfg.imageWidth();
		int height = cfg.imageHeight();
		
		int bitDepth = cfg.bitDepth();
		int footerSize = 0;
			
		double bytesPerPixel = bitDepth / 8;	
		
		logr.info("Frame size = " + (frameSize/bytesPerPixel) + " pixels");
			
		EBusParameter readoutModeFeature = cfg.getFeature("ReadoutControlMode");
		
		if(frameSize < width*height*bytesPerPixel){
			throw new RuntimeException("Camera wants less memory than it needs for the image???");
		}
			
		//int footerSize = (int)(imgDataSize - (width * height * bytesPerPixel));
		
		if(frameSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		ebus.destroyBuffers();
		
		try{					
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, (int)footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
		
		ByteBuffer[] allocBuffers = bulkAlloc.getAllocationBuffers();
		if(allocBuffers.length > 1)
			throw new RuntimeException("Multiple buffers not yet supported (>2GB image memory)");
		
	
	}
	
	private void doCapture() {
		
		setStatus(Status.awaitingSoftwareTrigger, "Starting Aquisition");		
		
		try{

			for(int i=0; i < images.length; i++)
				images[i].invalidate(false);
			
			time = new double[cfg.nImagesToAllocate]; //seconds since first image
			wallCopyTimeMS = new long[cfg.nImagesToAllocate];
			nanoTime = new long[cfg.nImagesToAllocate];
			long nanoTime0 = Long.MIN_VALUE;
			
			source.setSeriesMetaData("EBus/wallCopyTimeMS", wallCopyTimeMS, true);
			//source.setSeriesMetaData("EBus/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("EBus/nanoTime", nanoTime, true);
			source.setSeriesMetaData("EBus/time", time, true);
			
			ebus.executeCommand("GevTimestampControlLatch");
			long timestampInitTicks = ebus.getIntegerParameter("GevTimestampValue");
			long wallClockInitMS = System.currentTimeMillis();
			//execute command "GevTimestampControlLatch"
			//store local wall clock
			//get int param GevTimestampValue
			
			long timestampFreqHz = ebus.getIntegerParameter("GevTimestampTickFrequency"); 
			
			wallCopyTimeMS0 = Long.MIN_VALUE;
			timestampStart0 = Long.MIN_VALUE;
			
			ByteBuffer[] allocBuffers = bulkAlloc.getAllocationBuffers();
			ebus.createBuffers(cfg.nImagesToAllocate, allocBuffers[0]);
			
			int maxBufferQueue = ebus.getMaxBufferQueue();
			
			ArrayList<Integer> queuedBuffers = new ArrayList<>();
			int nextImgToQueue = 0;
			for(int i=0; i < Math.min(maxBufferQueue, cfg.nImagesToAllocate); i++) {
				ebus.queueBuffer(i); //TODO: Should be specific image buffer with write lock cycling
				queuedBuffers.add(i);
				spinLog("Queued buffer " + i);
				nextImgToQueue++;
			}
			if(cfg.isContinuous() && nextImgToQueue >= cfg.nImagesToAllocate)
				nextImgToQueue = 0;
			
			ebus.enableStream();
			
			//wait for acquisition start signal
			while(!signalAcquireStart){
				try{ Thread.sleep(0, 100000); }catch(InterruptedException err){ }
				
				if(signalDeath || signalAbort){
					logr.info("Aborting capture loop.");
					throw new CaptureAbortedException();
				}
				
			}			
			
			ebus.executeCommand("AcquisitionStart");
			setStatus(Status.awaitingHardwareTrigger, "Capturing, Acquisition started. Awaiting first image.");
			
			
			int toProcess = -1; //image to be processed
acqLoop:	do{ //loop until all images captured, or forever if continuous
				
				do{ //for each image in queue
					if(signalDeath || signalAbort){
						logr.info("Aborting capture loop.");
						throw new CaptureAbortedException();
					}
					
					try {
						toProcess = ebus.retrieveBuffer();
						wallCopyTimeMS[toProcess] = System.currentTimeMillis();
						spinLog("Retrieved buffer " + toProcess);
						
						queuedBuffers.remove((Integer)toProcess);
						
						long tsTick = ebus.getImageTimestamp(toProcess) - timestampInitTicks;
						nanoTime[toProcess] = (tsTick * 1_000_000_000L / timestampFreqHz) + wallClockInitMS * 1_000_000L;
						if(nanoTime0 == Long.MIN_VALUE) {
							nanoTime0 = nanoTime[toProcess];
							wallCopyTimeMS0 = wallCopyTimeMS[toProcess];
						}
						//make nanotime using cameras, but relative to wall time of first image received
						time[toProcess] = (nanoTime[toProcess] - nanoTime0) / 1e9;
						nanoTime[toProcess] = (wallCopyTimeMS0 * 1_000_000L) + (nanoTime[toProcess] - nanoTime0);
						
						if(nextImgToQueue < cfg.nImagesToAllocate && queuedBuffers.size() < maxBufferQueue) {
							spinLog("Queuing " + nextImgToQueue);
							
							ebus.queueBuffer(nextImgToQueue);
							queuedBuffers.add(nextImgToQueue);
							nextImgToQueue++;
							if(cfg.isContinuous() && nextImgToQueue >= cfg.nImagesToAllocate)
								nextImgToQueue = 0;
						}
						
						
					}catch(EBusException err) {
						if(!err.getMessage().startsWith("MORE_ENTRY")) {
							spinLog("No image");
						}else
							throw err;
					}
					
					setStatus(Status.capturing, "Capturing, Acquisition started. Images arriving.");
					
					spinLog("Got image to process: " + toProcess);
					if(toProcess >= 0)
						gotImage(toProcess);
					
					if(status == Status.awaitingHardwareTrigger)
						setStatus(Status.capturing, "Acquiuring.");		
					
					if(!cfg.isContinuous() && toProcess == (cfg.nImagesToAllocate-1))
						break acqLoop; //all done
					
				}while(toProcess > 0);

				//nothing to process, wait a bit
				try{ Thread.sleep(50); }catch(InterruptedException err){ }
								
			}while(true);
						
			spinLog("Acquisition complete.");
			
		}finally{
			ebus.executeCommand("AcquisitionStop");
			ebus.disableStream();
		}

	}
	
	private void gotImage(int idx){
		//catch up with the acqusition, this should really only ever be 1 image
	
		/*nanoTime[idx] = Long.MIN_VALUE;
		
		int footerSize = images[idx].getFooterSize(); 
		if(footerSize > 0){			
			//copy footer to metadata
			ByteBuffer buff = images[idx].getReadOnlyBuffer();
			
			buff.position(buff.capacity() - footerSize);
			byte footer[] = new byte[footerSize];
			buff.get(footer);
			source.setImageMetaData("EBus/footerData", idx, footer);
		
			
		}
		*/
		if(nanoTime[idx] == Long.MIN_VALUE){
			//no timestamp, so our wall copy time is the best we can do							
			time[idx] = (wallCopyTimeMS[idx] - wallCopyTimeMS0) / 1e3;
			 //half an exposure back from copy time, ignore readout time
			nanoTime[idx] = wallCopyTimeMS[idx] * 1_000_000L - (long)(cfg.exposureTimeMS() * 0.5 * 1e6);			
		}
		
		images[idx].invalidate(false);
		source.imageCaptured(idx);
				
		
	}
	
	
	private void deinit(){
		logr.info(" deInit()");
		
		if(ebus != null)
			ebus.close();
		ebus = null;
		cameraOpen = false;
	}
			
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		logr.info("Starting EBus capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(EBusConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(EBusConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thrad to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
				
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				logr.info("EBusCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.info("EbusCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				logr.info("EbusCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.info("EbusCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}

	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(EBusConfig config) { this.cfg = config; }

}
