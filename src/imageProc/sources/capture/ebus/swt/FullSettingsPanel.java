package imageProc.sources.capture.ebus.swt;

import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import ebusJNI.EBusParameter;
import ebusJNI.EBusParameter.EnumEntry;
import imageProc.core.ImgSource;
import imageProc.sources.capture.ebus.EBusConfig;
import imageProc.sources.capture.ebus.EBusSource;
import net.jafama.FastMath;

public class FullSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private EBusSource source;
	private Composite swtGroup;
	
	private final static String colNames[] = new String[]{ "Parameter", "Type", "Value", "Min", "Max", "Set", "Persistent", "Implmented", "Available", "Writable", "Readable", "Streamable" };
	private final static int COL_NAME = 0;
	private final static int COL_TYPE = 1;
	private final static int COL_VALUE = 2;
	private final static int COL_MIN = 3;
	private final static int COL_MAX = 4;
	private final static int COL_SET = 5;

	private Table featuresTable;
	private TableEditor featuresTableEditor;
	
	public FullSettingsPanel(Composite parent, int style, EBusSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		featuresTable = new Table(swtGroup, SWT.BORDER);				
		//featuresTable.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, true, 5, 0));
		featuresTable.setHeaderVisible(true);
		featuresTable.setLinesVisible(true);	
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(featuresTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]); 
			cols[i].pack();
		}
		
		TableItem blankItem = new TableItem(featuresTable, SWT.NONE);
		for(int i=0; i < colNames.length; i++)
			blankItem.setText(i, "     ");		

		for(int i=0; i < colNames.length; i++){
			cols[i].pack();
		}
		
		featuresTableEditor = new TableEditor(featuresTable);
		featuresTableEditor.horizontalAlignment = SWT.LEFT;
		featuresTableEditor.grabHorizontal = true;
		featuresTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		featuresTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });

		swtGroup.pack();
		
	}
	
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event){
		
		Rectangle clientArea = featuresTable.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = featuresTable.getTopIndex ();
		while (index < featuresTable.getItemCount ()) {
			boolean visible = false;
			final TableItem item = featuresTable.getItem (index);
			for (int i=0; i < featuresTable.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;

					final EBusParameter feature = source.getConfig().getFeature(item.getText(0));
					
					if(column == COL_SET){ //the 'set' column.
						//Which we just toggle if it's writable
						feature.toSet = !feature.toSet;
						item.setText(COL_SET, feature.toSet ? "Y" : "");
						break;
					}
					
					if(!feature.isFilled || !feature.isImplemented || !feature.isWritable){
						break;
					}
										
					if(column != COL_VALUE) //otherweise only set the value
						break;
					
					
					if(feature.type == EBusParameter.TYPE_ENUM){
						final Combo combo = new Combo(featuresTable, SWT.DROP_DOWN | SWT.MULTI | SWT.READ_ONLY);
						combo.setItems(feature.enumGetNames(true, true).toArray(new String[0]));
												
						Listener comboListener = new Listener () {
							public void handleEvent (final Event e) {
								switch (e.type) {
								case SWT.FocusIn:
									System.out.println("SWT.FocusIn isFocus = " + combo.isFocusControl());

									if(true)break;
								case SWT.FocusOut:
									
									System.out.println("SWT.FocusOut isFocus = " + combo.isFocusControl());
									
									//combo.dispose(); //this also kills it when the user clicks the expand button :(
									if(true)break;
								case SWT.Selection:
									System.out.println("SWT.Selection");
									
									String value = combo.getText();
									item.setText(column, value);
									feature.setValueByString(value);
									feature.toSet = true;
									item.setText(COL_SET, "Y");
									//checkTable();
									combo.dispose();
									break;
								case SWT.Traverse:
									System.out.println("SWT.Traverse");
									switch (e.detail) {
									case SWT.TRAVERSE_RETURN:
										String value2 = combo.getText();
										item.setText(column, value2);
										feature.setValueByString(value2); 
										feature.toSet = true;
										item.setText(COL_SET, "Y");
										//checkTable();
										//FALL THROUGH
									case SWT.TRAVERSE_ESCAPE:
										System.out.println("SWT.TRAVERSE_ESCAPE");
										combo.dispose();
										e.doit = false;
									}
									break;
								}
							}
						};
						combo.addListener (SWT.FocusOut, comboListener);
						combo.addListener (SWT.Traverse, comboListener);
						combo.addListener (SWT.Selection, comboListener);
						featuresTableEditor.setEditor(combo, item, i);
						combo.setText(item.getText(i));
						//combo.selectAll();
						combo.setFocus();
						// hacks for SWT4.4 (under linux GTK at least)
						// Textbox won't display if it's a child of the table
						// so we make it a child of the table's parent, but now need
						// to adjust the location
						{
							combo.moveAbove(featuresTable);
							final Point p0 = combo.getLocation();
							Point p1 = featuresTable.getLocation();
							p0.x += p1.x - clientArea.x;
							p0.y += p1.y;// + editBox.getSize().y;
							// p0.x = pt.x + p1.x;
							// p0.y = pt.y + p1.y;
							combo.setLocation(p0);
							combo.addListener(SWT.Move, new Listener() {
								@Override
								public void handleEvent(Event event) {
									combo.setLocation(p0); // TableEditor keeps
																// moving it to
																// relative to the
																// table, so move it
																// back
								}
							});
						}
						return;
						
						
					}else{
						final Text text = new Text(featuresTable, SWT.NONE);
						Listener textListener = new Listener () {
							public void handleEvent (final Event e) {
								switch (e.type) {
								case SWT.FocusOut:
									String value = text.getText();
									item.setText(column, value);
									feature.setValueByString(value);
									feature.toSet = true;
									item.setText(COL_SET, "Y");
									//checkTable();
									text.dispose ();
									break;
								case SWT.Traverse:
									switch (e.detail) {
									case SWT.TRAVERSE_RETURN:
										String value2 = text.getText();										
										feature.setValueByString(value2);
										item.setText(column, feature.valueAsString());										
										feature.toSet = true;
										item.setText(COL_SET, "Y");
										//checkTable();
										//FALL THROUGH
									case SWT.TRAVERSE_ESCAPE:
										text.dispose();
										e.doit = false;
									}
									break;
								}
							}
						};
						text.addListener (SWT.FocusOut, textListener);
						text.addListener (SWT.Traverse, textListener);
						featuresTableEditor.setEditor (text, item, i);
						text.setText (item.getText (i));
						text.selectAll ();
						text.setFocus ();
						// hacks for SWT4.4 (under linux GTK at least)
						// Textbox won't display if it's a child of the table
						// so we make it a child of the table's parent, but now need
						// to adjust the location
						{
							text.moveAbove(featuresTable);
							final Point p0 = text.getLocation();
							Point p1 = featuresTable.getLocation();
							p0.x += p1.x - clientArea.x;
							p0.y += p1.y;// + editBox.getSize().y;
							// p0.x = pt.x + p1.x;
							// p0.y = pt.y + p1.y;
							text.setLocation(p0);
							text.addListener(SWT.Move, new Listener() {
								@Override
								public void handleEvent(Event event) {
									text.setLocation(p0); // TableEditor keeps
																// moving it to
																// relative to the
																// table, so move it
																// back
								}
							});
						}
						return;
					}
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}
	
	void configToGUI(){
		EBusConfig config = source.getConfig();
		
		featuresTable.removeAll();
		
		for(Entry<String, EBusParameter> entry : config.getAllFeatures().entrySet()){
			String nameKey = entry.getKey();
			EBusParameter feature = entry.getValue(); 
			
			TableItem item = new TableItem(featuresTable, SWT.NONE);
			item.setText(COL_NAME, feature.name);
			item.setText(COL_TYPE, feature.typeAsString());
			item.setText(COL_VALUE, feature.valueAsString());
			item.setText(COL_MIN, feature.minValueAsString());
			item.setText(COL_MAX, feature.maxValueAsString());
			item.setText(COL_SET, feature.toSet ? "Y" : "");	
			item.setText(6, feature.isPersistent ? "Y" : "");
			item.setText(7, feature.isImplemented ? "Y" : "");
			item.setText(8, feature.isAvailable ? "Y" : "");
			item.setText(9, feature.isWritable ? "Y" : "");
			item.setText(10, feature.isReadable ? "Y" : "");
			item.setText(11, feature.isStreamable ? "Y" : "");
			
		}
		

		for(int i=0; i < colNames.length; i++){
			featuresTable.getColumn(i).pack();
		}
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
