package imageProc.sources.capture.ebus.swt;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImgSource;
import imageProc.sources.capture.ebus.EBusConfig;
import imageProc.sources.capture.ebus.EBusSource;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import ebusJNI.EBusParameter;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private EBusSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Text exposureLengthTextbox;
	private Button exposureMinButton; 
	private Button exposureMaxButton; 

	private Button unsetAllButton; 
	private Button defaultAllButton; 
	
	public SimpleSettingsPanel(Composite parent, int style, EBusSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		
		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure Time / ms:");
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("10");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		exposureMinButton = new Button(swtGroup, SWT.PUSH);
		exposureMinButton.setText("Min");
		exposureMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		exposureMinButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(false); } });
		
		exposureMaxButton = new Button(swtGroup, SWT.PUSH);
		exposureMaxButton.setText("Max");
		exposureMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		exposureMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(true); } });
		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
		lB.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("All parameters:");
		unsetAllButton = new Button(swtGroup, SWT.PUSH);
		unsetAllButton.setText("Unset/Read");
		unsetAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		unsetAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(false); } });
		
		defaultAllButton = new Button(swtGroup, SWT.PUSH);
		defaultAllButton.setText("Set default");
		defaultAllButton.setEnabled(false);
		defaultAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		defaultAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(true); } });
		
		configToGUI();
	}

	void configToGUI(){
		EBusConfig config = source.getConfig();
		
		
		continuousButton.setSelection(config.isContinuous());
		EBusParameter setPointParam = config.getFeature("SensorTemperatureSetPoint");
		
		exposureLengthTextbox.setText(Double.toString(config.doubleFeature("ExposureTime")));
		
	}
	
	public void fillCombo(Combo combo, EBusParameter param){
		
 		combo.removeAll();
 		
 		ArrayList<String> enumStrings = param.enumGetNames(true, true);
		
 		int selIdx = 0;
		for(int i=0; i < enumStrings.size(); i++){
			if(enumStrings.get(i) == null)
				continue;
			
			combo.add(enumStrings.get(i));			
			if((param.type == EBusParameter.TYPE_ENUM && i == ((Number)param.value).intValue())
				|| (param.type == EBusParameter.TYPE_FLOAT && 
				     Algorithms.mustParseDouble(enumStrings.get(i)) == ((Number)param.value).doubleValue())){
				combo.select(selIdx);
			}
			selIdx++;
		}
		
	}
	
	void guiToConfig(){
		EBusConfig config = source.getConfig();
		
		config.continuous = continuousButton.getSelection();
				
		double expTime = Algorithms.mustParseDouble(exposureLengthTextbox.getText());
		config.setFeature("ExposureTime", expTime);
		exposureLengthTextbox.setText(Double.toString(expTime));
		
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		EBusConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMin("FrameRate");
		}else{

			config.setFeatureToMax("FrameRate");
		}
		configToGUI();
	}
	
	public void exposureMinMaxEvent(boolean max){
		EBusConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMax("ExposureTime");
		}else{

			config.setFeatureToMin("ExposureTime");
		}
		configToGUI();
	}

	private void allParamsButton(boolean setDefault) {
		EBusConfig config = source.getConfig();
		
		for(EBusParameter param : config.getAllFeatures().values()){
			if(setDefault){
				// ???
			}else{
				param.toSet = false;
			}
		}

		configToGUI();
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
