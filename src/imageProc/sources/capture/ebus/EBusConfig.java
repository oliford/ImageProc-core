package imageProc.sources.capture.ebus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import imageProc.sources.capture.base.CaptureConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import ebusJNI.EBusParameter;
import ebusJNI.EBusROIs;
import ebusJNI.EBusROIs.EBusROI;

/** Configuration for PICam cameras */
public class EBusConfig extends CaptureConfig {

	/** General stuff that's outside of the camera's own config */
	
	/** Number of images to allocate and to capture 
	 * if in non-continuous mode */	 
	public int nImagesToAllocate = 100;
	
	/** Keep taking images, cycling through the allocated images
	 * for as long as the device hands them to us.
	 * 
	 * This is honoured regardless of the state of the camera's own parameters 
	 * e.g. AcquisitionMode AcquisitionFrameCount etc */
	public boolean continuous = false;
	
	/** connection identity */
	public String connectionID;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame;
	
	/** Timeout for WaitForAcquisitionUpdate() call */
	public int grabberTimeoutMS = 100;
	
	public long waitTimeout = 1000;
	
	public EBusConfig() {
		
		//params get added by Capture when the camera is initialised
		
		//defaults
		nImagesToAllocate = 20;
		blackLevelFirstFrame = false;
	
	}
	
	private void createForcedParameter(String name, Object value, int type) {

		EBusParameter p = new EBusParameter(name, 0, type);
		p.value = value;
		p.toSet = true;
		p.isImplemented = true;
		p.isFilled = true;
		p.isWritable = true;
		/*if(PICamParameters.getType(paramID) == PICamDefs.valueType_Enumeration){
			int n = ((int)value)+1;
			p.enumStrings = new String[n];
			for(int i=0; i < n; i++)
				p.enumStrings[i] = "INIT_" + n;
		}*/
		addParameter(p);
	}
	

	//we just store everything that the SDK presents as 'features'
	private TreeMap<String, EBusParameter> parameters = new TreeMap<String, EBusParameter>();

	/** Enable setting of all ROIs. When disabled the ROI configuration is kept in this class
	 * but not written to the camera */	
	public boolean enableROIs;

	public void addParameter(EBusParameter param){		
		parameters.put(param.name, param);
	}
	
	
	public TreeMap<String, EBusParameter> getAllFeatures() { return parameters;  }

	public int intFeature(String name) {
		EBusParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return -1;
		return (int)((Integer)feature.value); //ffs with the casting 
	}
		
	public int imageSizeBytes(){ return intFeature("PayloadSize"); }
	
	public int imageWidth() { return intFeature("Width"); }
	public int imageHeight(){ return intFeature("Height"); }
	public double frameTimeMS() { return 1000.0 / doubleFeature("AcquisitionRate");	}
	public double exposureTimeMS() { return doubleFeature("ExposureTime"); }
	
	public EBusParameter getFeature(String name) { return parameters.get(name); }

	public boolean isContinuous() { return continuous;	}

	public boolean featureMatchesString(String name, String value) {
		EBusParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return false;
		return feature.value.equals(value); 
	}

	public boolean boolFeature(String name) {
		EBusParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return false;
		return (Boolean)feature.value; 
	}

	public double doubleFeature(String name) {
		EBusParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return Double.NaN;
		
		return (feature.value instanceof Float) 
					? (Double)(double)(float)feature.value 
					: (Double)feature.value; 
	}

	/**These set the feature in our config and mark them to be set */
	
	public void setFeature(String name, Integer value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Double value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Long value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Boolean value) { setFeatureToObj(name, value); }
	public void setFeature(String name, String value) { setFeatureToObj(name, value); }
	
	public void setFeatureToObj(String name, Object value) {
		EBusParameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = value;
			feature.toSet = true;
		}
	}

	public void dontSetFeature(String name) {
		EBusParameter feature = parameters.get(name);
		if(feature != null){		
			feature.toSet = false;
		}
	}
	
	public void setFeatureToMin(String name) {
		EBusParameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = feature.minValue;
			feature.toSet = true;
		}
	}
	
	public void setFeatureToMax(String name) {
		EBusParameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = feature.maxValue;
			feature.toSet = true;
		}
	}

	/** Return list of features ordered according to their 'order' value, for setting */
	public Collection<EBusParameter> getSetOrderedFeatures() {

		ArrayList<EBusParameter> al = new ArrayList<EBusParameter>(parameters.values());
		
		Collections.sort(al, new Comparator<EBusParameter>() {
			@Override
			public int compare(EBusParameter o1, EBusParameter o2) {				
				return Integer.compare(o1.order, o2.order);
			}
		});
		
		return al;
		
	}

	public int bitDepth() {
		String pixelFormat = getFeature("PixelFormat").enumGetSelectedName();	
		
		if("Mono8".equals(pixelFormat)) {
			return 8;
		}else if("Mono16".equals(pixelFormat)) {
			return 16;			
		}else if("PvPixelRGB8".equals(pixelFormat)) {
			return 24;			
		}else {
			throw new RuntimeException("Unrecognised PixelFormat " + pixelFormat);
		}
	}

	public String stringFeature(String name) {
		EBusParameter param = getFeature(name); 
		return (param != null && param.value != null) ? param.value.toString() : "";
	}

}
