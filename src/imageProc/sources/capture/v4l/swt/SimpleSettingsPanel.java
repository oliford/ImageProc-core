package imageProc.sources.capture.v4l.swt;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImgSource;
import imageProc.sources.capture.v4l.V4LConfig;
import imageProc.sources.capture.v4l.V4LParameter;
import imageProc.sources.capture.v4l.V4LSource;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import au.edu.jcu.v4l4j.ImageFormat;
import au.edu.jcu.v4l4j.ResolutionInfo;
import au.edu.jcu.v4l4j.ResolutionInfo.DiscreteResolution;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private V4LSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo formatsCombo;
	private Combo resolutionCombo;
	
	private Text xOffsetTextbox;
	private Text yOffsetTextbox;
	private Text widthTextbox;
	private Text heightTextbox;
	
	private Button unsetAllButton; 
	private Button defaultAllButton; 
	
	public SimpleSettingsPanel(Composite parent, int style, V4LSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Keep reading images, wrapping back to image index 0.");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		Label lIF = new Label(swtGroup, SWT.NONE); lIF.setText("Image format:");
		formatsCombo = new Combo(swtGroup, SWT.NONE);
		formatsCombo.setText("?");
		formatsCombo.setToolTipText("Select image format the camera will return. List of camera formats are enumerated from camera.");
		formatsCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		formatsCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lIR = new Label(swtGroup, SWT.NONE); lIR.setText("Resolution:");
		resolutionCombo = new Combo(swtGroup, SWT.NONE);
		resolutionCombo.setText("?");
		resolutionCombo.setToolTipText("Resolution selected in camera. List of resolutions for the given format are enumerated from camera.");
		resolutionCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		resolutionCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lOX = new Label(swtGroup, SWT.NONE); lOX.setText("Offset: X:");
		xOffsetTextbox = new Text(swtGroup, SWT.NONE);
		xOffsetTextbox.setText("?");
		xOffsetTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		xOffsetTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		Label lOY = new Label(swtGroup, SWT.NONE); lOY.setText("Y:");
		yOffsetTextbox = new Text(swtGroup, SWT.NONE);
		yOffsetTextbox.setText("?");
		yOffsetTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		yOffsetTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		

		Label lW = new Label(swtGroup, SWT.NONE); lW.setText("Size: W:");
		widthTextbox = new Text(swtGroup, SWT.NONE);
		widthTextbox.setText("?");
		widthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		widthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		

		Label lH = new Label(swtGroup, SWT.NONE); lH.setText("H:");
		heightTextbox = new Text(swtGroup, SWT.NONE);
		heightTextbox.setText("0");
		heightTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		heightTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
		lB.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("All parameters:");
		unsetAllButton = new Button(swtGroup, SWT.PUSH);
		unsetAllButton.setText("Unset/Read");
		unsetAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		unsetAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(false); } });
		
		defaultAllButton = new Button(swtGroup, SWT.PUSH);
		defaultAllButton.setText("Set default");
		defaultAllButton.setEnabled(false);
		defaultAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		defaultAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(true); } });
		
		configToGUI();
	}

	void configToGUI(){
		V4LConfig config = source.getConfig();
		
		continuousButton.setSelection(config.isContinuous());
		
		if(config.nativeFormats == null) {
			formatsCombo.setItems(new String[] { " --- Empty --- " } );
		}else {
			formatsCombo.removeAll();
			int i=0;
			for(ImageFormat fmt : config.nativeFormats) {
				formatsCombo.add(fmt.getName());
				if(config.format != null && config.format.equals(fmt.getName())) {
					formatsCombo.select(i);
					
					resolutionCombo.removeAll();
					ResolutionInfo resInfo = fmt.getResolutionInfo();
					int j=0;
					for(DiscreteResolution res : resInfo.getDiscreteResolutions()) {
						resolutionCombo.add(res.toString());
					
						if(config.resolution != null && config.resolution.equals(res.toString())) {
							resolutionCombo.select(j);
						}
						j++;
					}
				}
				i++;
			}
		}
		formatsCombo.setText(config.format == null ? " --- None --- " : config.format);
		
		xOffsetTextbox.setText(Integer.toString(config.offsetX));
		yOffsetTextbox.setText(Integer.toString(config.offsetY));
		widthTextbox.setText(Integer.toString(config.width));
		heightTextbox.setText(Integer.toString(config.height)); 
				
		
	}
	
	public void fillCombo(Combo combo, V4LParameter param){
		
 		combo.removeAll();
 		
 		ArrayList<String> enumStrings = param.enumGetNames(true, true);
		
 		int selIdx = 0;
		for(int i=0; i < enumStrings.size(); i++){
			if(enumStrings.get(i) == null)
				continue;
			
			combo.add(enumStrings.get(i));			
			if((param.type == V4LParameter.TYPE_ENUM && i == ((Number)param.value).intValue())
				|| (param.type == V4LParameter.TYPE_FLOAT && 
				     Algorithms.mustParseDouble(enumStrings.get(i)) == ((Number)param.value).doubleValue())){
				combo.select(selIdx);
			}
			selIdx++;
		}
		
	}
	
	void guiToConfig(){
		V4LConfig config = source.getConfig();
		
		config.continuous = continuousButton.getSelection();
		config.format = formatsCombo.getText();
		config.resolution = resolutionCombo.getText();
		config.offsetX = Algorithms.mustParseInt(xOffsetTextbox.getText());
		config.offsetY = Algorithms.mustParseInt(yOffsetTextbox.getText());
		config.width = Algorithms.mustParseInt(widthTextbox.getText());
		config.height = Algorithms.mustParseInt(heightTextbox.getText());
				
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		V4LConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMin("FrameRate");
		}else{

			config.setFeatureToMax("FrameRate");
		}
		configToGUI();
	}
	
	public void exposureMinMaxEvent(boolean max){
		V4LConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMax("ExposureTime");
		}else{

			config.setFeatureToMin("ExposureTime");
		}
		configToGUI();
	}

	private void allParamsButton(boolean setDefault) {
		V4LConfig config = source.getConfig();
		
		for(V4LParameter param : config.getAllFeatures().values()){
			if(setDefault){
				// ???
			}else{
				param.toSet = false;
			}
		}

		configToGUI();
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
