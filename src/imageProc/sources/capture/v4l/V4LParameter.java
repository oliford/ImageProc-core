package imageProc.sources.capture.v4l;

import java.util.ArrayList;

import algorithmrepository.Algorithms;
import ebusJNI.EBusParameter;
import ebusJNI.EBusParameter.EnumEntry;

public class V4LParameter  {
	
	public final static int TYPE_INTEGER = 0;
	public final static int TYPE_ENUM = 1;
	public final static int TYPE_BOOLEAN = 2;
	public final static int TYPE_STRING = 3;
	public final static int TYPE_COMMAND = 4;
	public final static int TYPE_FLOAT = 5;
	public final static int TYPE_REGISTER = 6;
	public final static int TYPE_UNDEFINED = 999;

	public String name;
	
	public int type;
	
	public Object value;

	public boolean isImplemented;
	
	public Object minValue;
	public Object maxValue;	
	
	public static class EnumEntry {
		public String name;
		public int value;
		public boolean isAvailable;
	}
	
	private ArrayList<EnumEntry> enumEntries; 
	
	/** Order index. Features are set to camera lower order first */
	public int order;
	
	/** Should we attempt to set it on the next config sync? */
	public boolean toSet = false;
	
	/** Was filled with parameter info (e.g. enums) */
	public boolean isFilled;		
	
	public V4LParameter(String name, int order, int type) { 					
		this.name = name;
		this.order = order;
		this.toSet = false;
		this.isImplemented = false;
		this.type = type;
		this.enumEntries = new ArrayList<V4LParameter.EnumEntry>();
	}		

	public String typeAsString() {
		if(!isImplemented)
			return "not impl";
				
		switch(type){
			case TYPE_INTEGER: return "int";
			case TYPE_ENUM: return "enum";
			case TYPE_BOOLEAN: return "bool";
			case TYPE_STRING: return "string";
			case TYPE_COMMAND: return "command";
			case TYPE_FLOAT: return "float";
			case TYPE_REGISTER: return "register";
			default: return "?";
		}
	}
	
	public Class typeAsPrimitiveClass() {
		if(!isImplemented || type <= 0)
			return null;
		
		switch(type){
			case TYPE_INTEGER: return int.class;
			case TYPE_ENUM: return String[].class;
			case TYPE_BOOLEAN: return boolean.class;
			case TYPE_STRING: return String.class;
			case TYPE_COMMAND: return null;
			case TYPE_FLOAT: return double.class;
			case TYPE_REGISTER: return null;
			default: return null;
		}
	}
	
	public Class typeAsObjectClass() {
		if(!isImplemented || type <= 0)
			return null;
		
		switch(type){
			case TYPE_INTEGER: return Integer.class;
			case TYPE_ENUM: return String[].class;
			case TYPE_BOOLEAN: return Boolean.class;
			case TYPE_STRING: return String.class;
			case TYPE_COMMAND: return null;
			case TYPE_FLOAT: return Double.class;
			case TYPE_REGISTER: return null;
			default: return null;
		}
	}

	public String valueAsString() {
		if(!isImplemented)
			return "-";			
		switch(type){
			case TYPE_INTEGER: return value == null ? "" : ((Integer)value).toString();
			case TYPE_ENUM: return enumGetSelectedName();
			case TYPE_BOOLEAN: return value == null ? "" : ((Boolean)value).toString();
			case TYPE_STRING:
			case TYPE_COMMAND:
			case TYPE_FLOAT: 
			case TYPE_REGISTER: 
				return value == null ? "" : value.toString();
				
			default: return "?";
		}
	}
	
	public String minValueAsString() {
			
		if(!isImplemented)
			return "-";
		
		switch(type){
			case TYPE_INTEGER:return minValue == null ? "" : ((Integer)minValue).toString();
			default: return "?";
		}
	}
	
	public String maxValueAsString() {
		
		if(!isImplemented)
			return "-";
		
		switch(type){
			case TYPE_INTEGER:return maxValue == null ? "" : ((Integer)maxValue).toString();
			default: return "?";
		}
	}

	public void setValueByString(String value) {
		if(!isImplemented)
			return;
		switch(type){
			case TYPE_INTEGER: this.value = (Integer)Algorithms.mustParseInt(value); break;
			case TYPE_ENUM: enumSetByName(value); break;
			case TYPE_BOOLEAN: this.value = (Boolean)(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("T") || value.equalsIgnoreCase("TRUE")); break;
			case TYPE_STRING: this.value = value; break;
			case TYPE_FLOAT: this.value = (Double)Algorithms.mustParseDouble(value); break;
			case TYPE_COMMAND: 
			case TYPE_REGISTER:
			default: // !?
		}
	}


	public int enumNumEntries() { return enumEntries.size(); }
	
	public ArrayList<String> enumGetNames() { return enumGetNames(false, false); }
	
	public ArrayList<String> enumGetNames(boolean includeUnavailable, boolean markUnavailable) {
		ArrayList<String> ret = new ArrayList<>(enumEntries.size());
		for(EnumEntry entry : enumEntries) {
			if(entry.isAvailable)		
				ret.add(entry.name);
			else if(includeUnavailable)
				ret.add(markUnavailable ? ("-x- " + entry.name + " -x-") : entry.name);
		}
		
		return ret;
	}
	
	public String enumGetSelectedName() {
		for(EnumEntry entry : enumEntries)
			if(entry.value == (Integer)value)
				return entry.name;
		return "*INVALID*";
	}

	public void enumSetByName(String name) {
		
		for(EnumEntry entry : enumEntries) {
			if(name.equals(entry.name)) {
				value = entry.value;
				return;
			}
		}
		value = -1;
	}

	public ArrayList<EnumEntry> enumGetEntries() { return enumEntries; }


}


