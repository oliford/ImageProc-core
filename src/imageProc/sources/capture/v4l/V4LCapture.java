package imageProc.sources.capture.v4l;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import au.edu.jcu.v4l4j.CaptureCallback;
import au.edu.jcu.v4l4j.Control;
import au.edu.jcu.v4l4j.ControlList;
import au.edu.jcu.v4l4j.DeviceInfo;
import au.edu.jcu.v4l4j.FrameGrabber;
import au.edu.jcu.v4l4j.FrameInterval.DiscreteInterval;
import au.edu.jcu.v4l4j.ImageFormat;
import au.edu.jcu.v4l4j.ImageFormatList;
import au.edu.jcu.v4l4j.ResolutionInfo;
import au.edu.jcu.v4l4j.V4L4JConstants;
import au.edu.jcu.v4l4j.VideoDevice;
import au.edu.jcu.v4l4j.VideoFrame;
import au.edu.jcu.v4l4j.ResolutionInfo.DiscreteResolution;
import au.edu.jcu.v4l4j.exceptions.ControlException;
import au.edu.jcu.v4l4j.exceptions.StateException;
import au.edu.jcu.v4l4j.exceptions.UnsupportedMethod;
import au.edu.jcu.v4l4j.exceptions.V4L4JException;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import otherSupport.SettingsManager;


/** Thread runner used to copy in images.
 *  
 * It contains all the actual JNI calls to the camera library 
 * 
 * I think the SDK is thread safe but not entirely sure.
 */
public class V4LCapture extends Capture implements Runnable, CaptureCallback {
		
	private V4LSource source;
		
	private ByteBufferImage images[];
	
	private V4LConfig cfg;
		
	private Thread thread;
	
	/** Internal state */
	private VideoDevice		videoDevice;
	private FrameGrabber	frameGrabber;
	
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;
		
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
		
	/** Completely reset the configuration to defaults */
	private boolean resetConfig = false;
	
	private long wallCopyTimeMS0 = Long.MIN_VALUE;
	private long wallCopyTimeMS[];
		
	private long nanoTime[];
	private double time[];
	
	public V4LCapture(V4LSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera
		
	}
		
	@Override
	public void run() {
		threadBusy = true;
		try{
				
			
			setStatus(Status.init, "Initialising camera...");
			initDevice();
			
			signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Camera init done.");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();					
					}
					
					if(!signalCapture){
						setStatus(Status.completeOK, "Config done.");						
						
					}else{
						signalCapture = false;
									
						setStatus(Status.init, "Allocating...");		
						
						initImages();
						
						setStatus(Status.init, "Config done, starting capture...");
						
						//source.addNonTimeSeriesMetaDataMap(cfg.toMap("V4L/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");					
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal: " + err, err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
					err.printStackTrace();
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("V4L thread caught Exception:" + err);
			setStatus(Status.errored, "Aborted/Errored:" + err);
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				System.err.println("V4L thread caught exception de-initing camera.");
				err.printStackTrace();
			}
		}
	}
	
	
	private void initDevice() throws V4L4JException {
		setStatus(Status.init, "Initialising library... (May take a few minutes)");
		 
		setStatus(Status.init, "Opening camera... ");
		
		videoDevice = new VideoDevice(cfg.deviceFile);
		
		DeviceInfo devInfo  = videoDevice.getDeviceInfo();
		ImageFormatList fList = devInfo.getFormatList();
		cfg.nativeFormats = fList.getNativeFormats();
		cfg.rgbFormats = fList.getRGBEncodableFormats();
		cfg.deviceName = devInfo.getName();
		
		logr.info(".initCamera(): Opened camera: Model = " + cfg.deviceName);
		
		cameraOpen = true;				
		
	}
	
	private void syncConfig(){
				
		if(cfg.isContinuous() && cfg.nImagesToAllocate <= 1){
			throw new IllegalArgumentException("nImage=1 and contiuous is on. This is a ridiculous thing to ask for!");
		}
		
		writeConfig();
				
		readConfig();
				
		
		source.configChanged();
		logr.info(".syncConfig(): config sync done");
	}
	
	private void writeConfig(){
		setStatus(Status.init, "Applying configuration...");

		ControlList cLister = videoDevice.getControlList();
		try {
			for(V4LParameter param : cfg.getAllFeatures().values()){
				try{
					if(!param.toSet || !param.isImplemented || !param.isFilled)
						continue;
					
					Control c = cLister.getControl(param.name);
					
					switch(param.type){
						case V4LParameter.TYPE_INTEGER:
							int valI = (int)param.value;
							if(valI < (int)param.minValue || valI > (int)param.maxValue)
								throw new RuntimeException("Out of range");
							c.setValue(valI);				
							logr.fine("Set '"+param.name+"' = " + valI + " (int)");
							break;					
						default:
			    			logr.warning("Unrecognised parameter type '"+param.type+" for param '"+param.name+"'");
					}
				}catch(Exception err){
					throw new RuntimeException("Caught exception setting parameter '"+param.name+".", err);					
				}
			}
		}finally {
			videoDevice.releaseControlList();
		}
	}
	
	private void readConfig(){
		setStatus(Status.init, "Getting configuration...");

		ControlList cLister = videoDevice.getControlList();
		try {
			List<Control> cList = cLister.getList();
			
			//read back
			for(Control c : cList) {
				String name = c.getName();
				try {
					
					V4LParameter param = cfg.getFeature(name);
					if(param == null){
						param = new V4LParameter(name, 0, V4LParameter.TYPE_INTEGER);
						cfg.addParameter(param);
					}
		
					param.isImplemented = true;
					
					switch(param.type){
			
						case V4LParameter.TYPE_INTEGER:
							param.value = c.getValue();
							param.isFilled = true;
							param.minValue = c.getMinValue();
							param.maxValue = c.getMaxValue();						
							logr.fine("Get '"+param.name+"' = " + param.value + " (int)");
							break;
			    		default:
			    			logr.warning("Unrecognised parameter type '"+param.type+" for param '"+param.name+"'");
					}
					
				}catch(ControlException | UnsupportedMethod err) {
					logr.log(Level.WARNING, "Error reading parameter " + name, err);
				}
			}	
		}finally {
			videoDevice.releaseControlList();
		}
	}
		
	private void initImages(){

		int width = cfg.width;
		int height = cfg.height;
		int nFields = 1;
		int bitDepth = 8;
		
		if(cfg.format == null)
			throw new IllegalArgumentException("Image format not set");
		else if(cfg.format.contains("YUYV")) {
			nFields = 3;
			bitDepth = 8;
		}else if(cfg.format.contains("Y16")) {
			nFields = 1;
			bitDepth = 16;			
		}else {
			throw new RuntimeException("Unknown format " + cfg.format);
		}
		
		
		int frameSize = width * height * bitDepth;
			
		double bytesPerPixel = bitDepth / 8;	
		
		logr.info("Frame size = " + (frameSize/bytesPerPixel) + " pixels");
		
		
		
		
		if(frameSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		//V4L.destroyBuffers();
		
		try{					
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, nFields, bitDepth, 0, 0, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
		
		ByteBuffer[] allocBuffers = bulkAlloc.getAllocationBuffers();
		if(allocBuffers.length > 1)
			throw new RuntimeException("Multiple buffers not yet supported (>2GB image memory)");
		
	
	}
	
	 
	
	private void doCapture() throws V4L4JException {
		
		setStatus(Status.awaitingSoftwareTrigger, "Capturing, acquisition not started.");
		
		try{

			for(int i=0; i < images.length; i++)
				images[i].invalidate(false);
			
			time = new double[cfg.nImagesToAllocate]; //seconds since first image
			wallCopyTimeMS = new long[cfg.nImagesToAllocate];
			nanoTime = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("V4L/wallCopyTimeMS", wallCopyTimeMS, true);
			//source.setSeriesMetaData("V4L/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("V4L/nanoTime", nanoTime, true);
			source.setSeriesMetaData("V4L/time", time, true);
			
			DeviceInfo info = videoDevice.getDeviceInfo();
			for (ImageFormat format: info.getFormatList().getNativeFormats())
			{
				logr.log(Level.INFO, "device supported format " + format.getName());
				for (DiscreteResolution res : format.getResolutionInfo().getDiscreteResolutions())
				{
					for (DiscreteInterval interval : res.getFrameInterval().getDiscreteIntervals()) {

					logr.log(Level.FINE, String.format("\t possible interval num/denom %d/%d",
							interval.getNum(), interval.getDenom()));
					}

				}
			}
			
			
//			ControlList controls = videoDevice.getControlList();
//			for (Control ctrl : controls.getList())
//			{
//				for ( String name : ctrl.getDiscreteValueNames() )
//				{
//					logr.log(Level.INFO, "device control " + name);
//				}
//			}
//			videoDevice.releaseControlList();
			
			
			//TODO: timestamp setup
			wallCopyTimeMS0 = Long.MIN_VALUE;
			
			//TODO: Any stream init
			nextImage = 0;
			
			//wait for acquisition start signal
			while(!signalAcquireStart){
				try{ Thread.sleep(0, 100000); }catch(InterruptedException err){ }
				
				if(signalDeath || signalAbort){
					logr.info("Aborting capture loop.");
					throw new CaptureAbortedException();
				}
				
			}			
			
			ImageFormat selectedFormat = null;
			for(ImageFormat fmt : cfg.nativeFormats) {
				if(cfg.format.equals(fmt.getName())) {
					selectedFormat = fmt;
					break;
				}
			}
			if(selectedFormat == null)
				throw new IllegalArgumentException("Image format '"+cfg.format+"' not in list of supposed native formats");
			logr.log(Level.INFO, "selected video format " + selectedFormat.getName());
			for(DiscreteResolution res : selectedFormat.getResolutionInfo().getDiscreteResolutions()) {
				for (DiscreteInterval interval : res.getFrameInterval().getDiscreteIntervals()) {
					logr.log(Level.FINE, String.format("possible interval num/denom %d/%d", interval.getNum(), interval.getDenom()));
				}
			}
//			System.setProperty("v4l4j.num_driver_buffers", "20000"); // the actual number of buffers is decided by v4l
			frameGrabber = videoDevice.getRawFrameGrabber(cfg.width, cfg.height, cfg.channel, cfg.standard, selectedFormat);
			if(frameGrabber.getHeight() != cfg.height || frameGrabber.getWidth() != cfg.width) 
				throw new RuntimeException("Framegrabber has resolution " + frameGrabber.getWidth() + " x " + frameGrabber.getHeight() 
											+ " but " + cfg.width + " x " + cfg.height + " was configured");		
//			frameGrabber.setFrameInterval(1, 30);
			DiscreteInterval interval = frameGrabber.getFrameInterval();
			logr.log(Level.INFO,  String.format("current interval num/denom %d/%d", interval.getNum(), interval.getDenom()));
			logr.log(Level.INFO, String.format("driver buffers %d", frameGrabber.getNumberOfVideoFrames()));
			frameGrabber.setCaptureCallback(this);
			
			frameGrabber.startCapture();
			
			setStatus(Status.awaitingHardwareTrigger, "Capturing..., (awaiting hardware trigger)");
			
			do {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) { }				
					
			}while(!signalAbort && (cfg.isContinuous() || nextImage < cfg.nImagesToAllocate));
				
		
			spinLog("Acquisition complete.");
			
		}finally{
			if(frameGrabber != null) {
				try {
					frameGrabber.stopCapture();
				}catch(StateException err) {
					if(!err.getMessage().contains("not started")){
						throw err;
					}
				}
				videoDevice.releaseFrameGrabber();
				frameGrabber = null;
			}
		}

	}
	
	private void gotImage(int idx){
		//catch up with the acqusition, this should really only ever be 1 image
		
		setStatus(Status.capturing, "Capturing..., acquisition started");
	
		/*if(nanoTime[idx] == Long.MIN_VALUE){
			//no timestamp, so our wall copy time is the best we can do							
			time[idx] = (wallCopyTimeMS[idx] - wallCopyTimeMS0) / 1e3;
			 //half an exposure back from copy time, ignore readout time
			nanoTime[idx] = wallCopyTimeMS[idx] * 1_000_000L - (long)(cfg.exposureTimeMS() * 0.5 * 1e6);			
		}*/
		
		images[idx].invalidate(false);
		source.imageCaptured(idx);
				
		
	}
	
	
	private void deinit(){
		logr.info(" deInit()");
		
		if(frameGrabber != null) {
			try {
				frameGrabber.stopCapture();
			} catch (StateException ex) {
				// the frame grabber may be already stopped, so we just ignore
				// any exception and simply continue.
			}
		}

		// release the frame grabber and video device
		videoDevice.releaseFrameGrabber();
		videoDevice.release();
		videoDevice = null;
		
		cameraOpen = false;
	}
			
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		logr.info("Starting V4L capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(V4LConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(V4LConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thrad to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
				
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				logr.info("V4LCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.info("V4LCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				logr.info("V4LCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.info("V4LCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}

	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(V4LConfig config) { this.cfg = config; }
	
	int nextImage = 0;

	@Override
	public void nextFrame(VideoFrame frame) {
//		long startTime = System.currentTimeMillis();
//		long frameNumber = frame.getSequenceNumber();
//		long captureTime = frame.getCaptureTime();
		try {
			if(nextImage == cfg.nImagesToAllocate) {
				if(cfg.isContinuous()) {
					nextImage = 0;
				}else {
					return;
				}	
			}
				
			if(signalAcquireStart) {
				int imgIdx = nextImage++;
				wallCopyTimeMS[imgIdx] = System.currentTimeMillis();
				spinLog("Got image" + imgIdx);
				
				byte bytes[] = frame.getBytes();
					
				WriteLock writeLock = images[imgIdx].writeLock();;
				writeLock.lockInterruptibly();
				try {
					ByteBuffer b = images[imgIdx].getWritableBuffer(writeLock);
					if(bytes.length != b.capacity()) 
						throw new RuntimeException("Frame has " + bytes.length + " bytes but we were expecting " + b.capacity());
					
						
					b.rewind();
					
					if(cfg.format.equals("YUYV")) {
						//this is probably horrifically slow. There is almost certainly a better way
						int nPixels = cfg.height*cfg.width;
						for(int i=0; i < nPixels; i+=2) {
							byte Y0 = bytes[i*2];
							byte U = bytes[i*2+1];
							byte Y1 = bytes[i*2+2];
							byte V = bytes[i*2+3];
							
							b.put(0*nPixels + i, Y0);
							b.put(1*nPixels + i, U);
							b.put(2*nPixels + i, V);
							
							b.put(0*nPixels + i + 1, Y1);
							b.put(1*nPixels + i + 1, U);
							b.put(2*nPixels + i + 1, V);
							
							
						}
						
					}else {
						b.put(bytes);
					}
				}finally {
					writeLock.unlock();
				}
				
				frame.recycle(); //can already throw the frame buffer back
				
				if(wallCopyTimeMS0 == Long.MIN_VALUE) {
					wallCopyTimeMS0 = wallCopyTimeMS[imgIdx];
				}
				
				nanoTime[imgIdx] = wallCopyTimeMS[imgIdx] * 1_000_000L; 
				time[imgIdx] = (wallCopyTimeMS[imgIdx] - wallCopyTimeMS0) / 1000.0;
				
				gotImage(imgIdx);
			}
		}catch(Throwable t) {
			logr.log(Level.SEVERE, "Exception in frame recieve", t);
			frameGrabber.stopCapture();
			setStatus(Status.errored, "ERROR:" + t.toString());
		}
//		long stopTime = System.currentTimeMillis();
//		logr.log(Level.WARNING, String.format("frame parsed in %d ms, frame number %d, frame time %d", stopTime - startTime, frameNumber, captureTime));
	}

	@Override
	public void exceptionReceived(V4L4JException e) {
		logr.log(Level.WARNING, "V4L error", e);
	}

}
