package imageProc.sources.capture.v4l;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import imageProc.sources.capture.base.CaptureConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import au.edu.jcu.v4l4j.ImageFormat;
import au.edu.jcu.v4l4j.V4L4JConstants;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Configuration for PICam cameras */
public class V4LConfig extends CaptureConfig {

	/** General stuff that's outside of the camera's own config */
	
	/** Number of images to allocate and to capture 
	 * if in non-continuous mode */	 
	public int nImagesToAllocate = 50;
	
	/** device file */
	public String deviceFile = "/dev/video0";
	
	/** device name from driver */
	public String deviceName;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;
	
	/** Timeout for WaitForAcquisitionUpdate() call */
	public int grabberTimeoutMS = 100;
	
	public long waitTimeout = 1000;
	
	public int offsetX = 0;
	
	public int offsetY = 0;
	
	public int width = 640;
	
	public int height = 480;
	
	public int channel = 0;
	
	public int standard = V4L4JConstants.STANDARD_WEBCAM;
	
	public List<ImageFormat> nativeFormats;
	public List<ImageFormat> rgbFormats;
	
	public String format = "YUYV";
	
	public String resolution;

	public boolean continuous = false;
	
	public V4LConfig() {
			
	}
	
	private void createForcedParameter(String name, Object value, int type) {

		V4LParameter p = new V4LParameter(name, 0, type);
		p.value = value;
		p.toSet = true;
		p.isImplemented = true;
		p.isFilled = true;
		
		addParameter(p);
	}
	
	//we just store everything that the SDK presents as 'features'
	private TreeMap<String, V4LParameter> parameters = new TreeMap<String, V4LParameter>();

	/** Enable setting of all ROIs. When disabled the ROI configuration is kept in this class
	 * but not written to the camera */	
	public boolean enableROIs;


	public void addParameter(V4LParameter param){		
		parameters.put(param.name, param);
	}
	
	
	public TreeMap<String, V4LParameter> getAllFeatures() { return parameters;  }

	public int intFeature(String name) {
		V4LParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return -1;
		return (int)((Integer)feature.value); //ffs with the casting 
	}
		
	public int imageSizeBytes(){ return intFeature("PayloadSize"); }
	
	public int imageWidth() { return intFeature("Width"); }
	public int imageHeight(){ return intFeature("Height"); }
	public double frameTimeMS() { return 1000.0 / doubleFeature("AcquisitionRate");	}
	public double exposureTimeMS() { return doubleFeature("ExposureTime"); }
	
	public V4LParameter getFeature(String name) { return parameters.get(name); }

	public boolean isContinuous() {
		return continuous;
	}

	public boolean featureMatchesString(String name, String value) {
		V4LParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return false;
		return feature.value.equals(value); 
	}

	public boolean boolFeature(String name) {
		V4LParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return false;
		return (Boolean)feature.value; 
	}

	public double doubleFeature(String name) {
		V4LParameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return Double.NaN;
		
		return (feature.value instanceof Float) 
					? (Double)(double)(float)feature.value 
					: (Double)feature.value; 
	}

	/**These set the feature in our config and mark them to be set */
	
	public void setFeature(String name, Integer value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Double value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Long value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Boolean value) { setFeatureToObj(name, value); }
	public void setFeature(String name, String value) { setFeatureToObj(name, value); }
	
	public void setFeatureToObj(String name, Object value) {
		V4LParameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = value;
			feature.toSet = true;
		}
	}

	public void dontSetFeature(String name) {
		V4LParameter feature = parameters.get(name);
		if(feature != null){		
			feature.toSet = false;
		}
	}
	
	public void setFeatureToMin(String name) {
		V4LParameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = feature.minValue;
			feature.toSet = true;
		}
	}
	
	public void setFeatureToMax(String name) {
		V4LParameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = feature.maxValue;
			feature.toSet = true;
		}
	}

	/** Return list of features ordered according to their 'order' value, for setting */
	public Collection<V4LParameter> getSetOrderedFeatures() {

		ArrayList<V4LParameter> al = new ArrayList<V4LParameter>(parameters.values());
		
		Collections.sort(al, new Comparator<V4LParameter>() {
			@Override
			public int compare(V4LParameter o1, V4LParameter o2) {				
				return Integer.compare(o1.order, o2.order);
			}
		});
		
		return al;
		
	}

	public int bitDepth() {
		String pixelFormat = getFeature("PixelFormat").enumGetSelectedName();	
		
		if("Mono8".equals(pixelFormat)) {
			return 8;
		}else if("PvPixelRGB8".equals(pixelFormat)) {
			return 24;			
		}else {
			throw new RuntimeException("Unrecognised PixelFormat " + pixelFormat);
		}
	}

	public String stringFeature(String name) {
		V4LParameter param = getFeature(name); 
		return (param != null && param.value != null) ? param.value.toString() : "";
	}

}
