package imageProc.sources.capture.andorCam;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import andorJNI.AndorCam;
import andorJNI.AndorSDKException;
import imageProc.core.AcquisitionDevice;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature;
import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature.FeatureType;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;


/** Thread runner used to copy in images.
 *  
 * It contains all the actual JNI calls to the camera library 
 * 
 * I think the SDK is thread safe but not entirely sure.
 */
public class AndorCamCapture extends Capture implements Runnable  {
		
	private AndorCamSource source;
	
	private ByteBufferImage images[];
	
	private AndorCamConfig cfg;
		
	/** Running temperature while idle */	
	private long lastIdleTempTime;
	private int idleTempIndex = 0;
	private double[][] idleTemperature;
	
	/** Internal state */
	private AndorCam camDrv;
	
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;
	
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
	
	/** Timing info used in doCapture() and processTicks() */
	private byte ticksData[][];
	private long ticksLong[];
	private long nanoTime[];
	private double time[];
	private long ticksLongFirst;
	private double offsetFromTrigger;
	private long walltimeFirstImageReadMS = -1;
	
	public AndorCamCapture(AndorCamSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera 
	}
		
	@Override
	public void run() {
		threadBusy = true;
		lastIdleTempTime = -1;
		
		try{
			if(camDrv != null){
				camDrv.close();
				throw new RuntimeException("Camera driver already open, aborting both");
			}
			
			setStatus(Status.init, "Capture init...");
			initDevice();
			
			signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Init Ready.");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(cfg.idleTempPollPeriodMS > 0 && (System.currentTimeMillis() - lastIdleTempTime) > cfg.idleTempPollPeriodMS){
							double tempVal = camDrv.getFloat("SensorTemperature");
							lastIdleTempTime = System.currentTimeMillis();
							
							if(idleTemperature == null || idleTempIndex >= idleTemperature.length){
								int oldLen = (idleTemperature == null ? 0 : idleTemperature.length);
								int newLen = oldLen + 100;
								double newArr[][] = new double[newLen][];
								if(oldLen > 0)
									System.arraycopy(idleTemperature, 0, newArr, 0, oldLen);
								
								for(int i=oldLen; i < newLen; i++)
									newArr[i] = new double[]{ Double.NaN, Double.NaN };
								
								idleTemperature = newArr;
								
							}
							
							idleTemperature[idleTempIndex][0] = lastIdleTempTime;
							idleTemperature[idleTempIndex][1] = tempVal;
							idleTempIndex++;
							
							source.setSeriesMetaData("AndorCam/idleTemperatureLog", idleTemperature, false);
							
						}
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();					
					}
					
					if(!signalCapture){
						setStatus(Status.completeOK, "Config done.");						
						
					}else{
						signalCapture = false;
									
						setStatus(Status.init, "Allocating images...");		
						
						initImages();
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("AndorCam/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");					
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal", err);
					setStatus(Status.aborted, "Aborted by signal");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.WARNING, "Aborted by signal", err);
					setStatus(Status.errored, "Aborted by error: " + err);
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			logr.log(Level.SEVERE, "AndorCam thread caught Exception:\n" + err, err);
			setStatus(Status.errored, "Aborted/Errored:\n" + err);
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				logr.log(Level.WARNING, "AndorCam thread caught exception de-initing camera.", err);
			}
		}
	}
	
	
	private void initDevice() {
		setStatus(Status.init, "Initialising library...");
		camDrv = new AndorCam();
		
		setStatus(Status.init, "Opening camera...");
		camDrv.open(cfg.cameraIndex);
		
		String model = camDrv.getString("CameraModel");
		
		logr.info(".initCamera(): Opened camera "+cfg.cameraIndex+", Model:" + model);
		
		if(model.contains("SIMCAM") && !cfg.allowDemoCamera){
			throw new RuntimeException("Opened SIMCAM. Probably a problem with the camera.");
		}
		
		cameraOpen = true;				
		
		
	}
	
	private void syncConfig(){
		
		AndorSDKException configFailed = null;
		
		int nFailedLast = Integer.MAX_VALUE;
		do{
			String errorCollect = "";
			int nFailed = 0;			
			//set anything that wants setting
			for(AndorCamConfig.AndorFeature feature : cfg.getSetOrderedFeatures()){
				if(signalDeath)
					return;
				
				if(!feature.toSet)
					continue;
				
				try{				
					feature.isImplemented = camDrv.isImplemented(feature.name);							
					feature.isWritable = feature.isImplemented && camDrv.isWritable(feature.name);
									
					if(!feature.isImplemented || !feature.isWritable || feature.value == null){
						logr.log(Level.FINE, "Can't set entry '" + feature.name + "' because it's not implemented, not writable or value to set is null");
						feature.toSet = false;
						continue;
					}
				
			
					//try to set the value first
					switch(feature.type){
						case BOOL: camDrv.setBool(feature.name, (Boolean)feature.value); break;
						case INT: camDrv.setInt(feature.name, (Long)feature.value); break;
						case FLOAT: camDrv.setFloat(feature.name, (Double)feature.value); break;
						case ENUM: camDrv.setEnumString(feature.name, (String)feature.value); break;
						case STRING: camDrv.setString(feature.name, (String)feature.value); break;
						default: 
							logr.log(Level.WARNING, "Type + " +feature.type +" not supported for feature '"+feature.name+"'");
					}
					
				}catch(AndorSDKException err){
					if(err.getMessage().contains("17: COMM")){
						throw err;
					}
					//with a special exception for the frame rate being almost but not exactly within range 
					//due to it using frameRate instead of frameTime (grumble)
					if("FrameRate".equals(feature.name)){
						double val = (Double)feature.value;
						double max = (Double)feature.maxValue;
						double min = (Double)feature.minValue;
						if(val <= min && (min - val) < 1e-4*min){
							logr.log(Level.INFO, "FrameRate set "+val+" failed, nudging above minimum "+min+".");
							feature.value = min + 1e-4 * min;
							
						}else if(val >= max && (val - max) < 1e-4*max){
							logr.log(Level.INFO, "FrameRate set "+val+" failed, nudging below maximum "+max+".");
							feature.value = max - 1e-4 * max;
						}
						//hopefully pick that up on the next round
					}
					
					errorCollect += "Set("+feature.name+"): " + feature.valueAsString() + ", AndorSDKException " + err.getErrorString() + "\n";
					nFailed++; 
									
				}catch(RuntimeException err){
					errorCollect += "Set("+feature.name+"): " + feature.valueAsString() + ", RuntimeException = " + err.toString() + "\n";
					nFailed++;
				}
			}
			if(nFailed == nFailedLast){ //doesn't seem to improve
				logr.log(Level.WARNING, "AndorCamCapture: Still " + nFailed + " config sync failures, giving up: ", errorCollect);
				configFailed = new AndorSDKException(errorCollect); //will get thrown after the get
				break;
			}
			nFailedLast = nFailed;
		}while(nFailedLast > 0);
				
		setStatus(Status.init, "Reading config...");
		//and re-get the value to check (or just to load)
		for(AndorCamConfig.AndorFeature feature : cfg.getAllFeatures().values()){
			if(signalDeath)
				return;
			
			try{		
				
				feature.isImplemented = camDrv.isImplemented(feature.name);				
				feature.isWritable = feature.isImplemented && camDrv.isWritable(feature.name);
								
				feature.isFilled = true;
				
				if(!feature.isImplemented) //don't get if it it's to be set every time
					continue;
				
				Object val;
				switch(feature.type){
					case BOOL: 
						val = (Boolean)camDrv.getBool(feature.name);
						feature.minValue = null;
						feature.maxValue = null;
						break;
					case INT: 
						val = (Long)camDrv.getInt(feature.name);
						feature.minValue = (Long)camDrv.getIntMin(feature.name);
						feature.maxValue = (Long)camDrv.getIntMax(feature.name);
						break;
					case FLOAT: 
						val = (Double)camDrv.getFloat(feature.name);
						feature.minValue = (Double)camDrv.getFloatMin(feature.name);
						feature.maxValue = (Double)camDrv.getFloatMax(feature.name);
						break;
					case STRING: 
						val = camDrv.getString(feature.name);
						feature.minValue = null;
						feature.maxValue = null;
						break;

					case ENUM: 
						int nEnum = camDrv.getEnumCount(feature.name);
						feature.enumStrings = new String[nEnum];
						feature.enumIsImplemented = new boolean[nEnum];
												
						for(int i=0; i < nEnum; i++){
							feature.enumStrings[i] = camDrv.getEnumStringByIndex(feature.name, i);
							feature.enumIsImplemented[i] = camDrv.isEnumIndexImplemented(feature.name, i);
						}
												
						int index = camDrv.getEnumIndex(feature.name);
						val = feature.enumStrings[index];
						feature.minValue = null;
						feature.maxValue = null;
						break;
					default: 
						logr.log(Level.WARNING, "Type + " +feature.type +" not supported for feature '"+feature.name+"'");
						val = null;
				}
				
				//only actually set the value into the local config if the setting workded, otherwise
				//we'll overwrite the user's desire with the old config
				if(!feature.toSet && val != null && configFailed == null){ 
					feature.value = val;
				}
			}catch(AndorSDKException err){
				logr.log(Level.WARNING, "WARNING: Failed to get config feature '"+feature.name+"'.", err);
			}
		}
		
		source.configChanged();
		
		if(configFailed != null) //and throw the config failed exception if there was one
			throw configFailed;
		
		logr.info(".syncConfig(): config sync done");
	}
		
	private void initImages(){
		
		long imgDataSize = cfg.imageSizeBytes();
		int width = cfg.imageWidth();
		int height = cfg.imageHeight();
		double bytesPerPixel = ((Double)cfg.getFeature("BytesPerPixel").value);
		int bitDepth = (int)(bytesPerPixel * 8.0);
		int stride = ((Long)(cfg.getFeature("AOIStride").value)).intValue();
		width = (int)(stride / bytesPerPixel);
		
		if(imgDataSize < height*stride){
			throw new RuntimeException("Camera wants less memory than it needs for the image???");
		}
			
		int footerSize = (int)(imgDataSize - (width * height * bytesPerPixel));
		
		if(imgDataSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		// imgDataSize = actWidth * actHeight * (bitDepth == 12 ? 2 : 1);
		
		try{
					
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.log(Level.SEVERE, ".initImages() Not enough memory for images.", err);
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
	
	}
	
	private void doCapture() {
		setStatus(Status.init, "Capture setup...");
		
		boolean hasMetadata = cfg.boolFeature("MetadataEnable");
		
		long imgDataSize = cfg.imageSizeBytes();
		int timeout = 100;
		
		//int height = cfg.imageHeight();
		//int stride = ((Long)(cfg.getFeature("AOIStride").value)).intValue();
		
		source.setSeriesMetaData("AndorCam/walltimeCaptureStart", (Long)System.currentTimeMillis(), false);
										
		int nextIndexToQueue = 0; //cameraq's queue head
		int nextIndexToGet = 0; //camera's queue tial
		int nInQueue = 0; 
		int nToQueue = (cfg.nImagesToQueue > 0 && cfg.nImagesToQueue < cfg.nImagesToAllocate) ? cfg.nImagesToQueue : cfg.nImagesToAllocate;		
		
		walltimeFirstImageReadMS = -1;
		ticksLongFirst = Long.MIN_VALUE;
		
		WriteLock writeLock[] = new WriteLock[cfg.nImagesToAllocate];
		
		long lastImageLoadTime = System.currentTimeMillis();
		
		ticksData = new byte[cfg.nImagesToAllocate][];
		ticksLong = new long[cfg.nImagesToAllocate];
		nanoTime = new long[cfg.nImagesToAllocate];
		time = new double[cfg.nImagesToAllocate];
		
		//clear time metadata so we dont have old info in there
		source.setSeriesMetaData("AndorCam/walltimeFirstImageRead", walltimeFirstImageReadMS, false);
		source.setSeriesMetaData("AndorCam/ticksData", ticksData, true);
		source.setSeriesMetaData("AndorCam/ticksLong", ticksLong, true);
		source.setSeriesMetaData("AndorCam/nanoTime", nanoTime, true);
		source.setSeriesMetaData("AndorCam/time", time, true);
		
		//the trigger-frame timing here is only accurate to ~ms.
		offsetFromTrigger = Double.NaN; // time difference between start trigger and central exposure time of the average (1/4 and 3/4) row.
		AndorCamConfig.AndorFeature shutterFeature = cfg.getFeature("ElectronicShutteringMode"); 
		if(shutterFeature != null){
			double expTime = cfg.doubleFeature("ExposureTime");
			if(shutterFeature.valueAsString().equalsIgnoreCase("Rolling")){
				offsetFromTrigger = 2.0 * expTime;
				
			}else if(shutterFeature.valueAsString().equalsIgnoreCase("Global")){
				offsetFromTrigger = 0.5 * expTime;
			}
		}
		
		source.setSeriesMetaData("AndorCam/offsetFromTrigger", offsetFromTrigger, false);
		if(Double.isNaN(offsetFromTrigger)){
			logr.log(Level.WARNING, "AndorCamCapture: WARNING: offsetFromTrigger is NaN, will use 0 offset for frame times");
		}
		
		setStatus(Status.awaitingSoftwareTrigger, "Capturing, but acquisition not started.");
				
		AndorSDKException queueingError = null;
		try{
			try{
				do{
					//if we've queued the minimum number, start the acquisition when commanded
					if(nInQueue >= nToQueue && !isAcqusitionStarted() && signalAcquireStart){
						source.setSeriesMetaData("AndorCam/walltimeAcquisitionStart", (Long)System.currentTimeMillis(), false);						
						camDrv.command("Acquisition Start");
						
						setStatus(Status.awaitingHardwareTrigger, "Capturing... (Acquisition started)");
					}
					
					
					//queue up as many as we have room for
					try{
						while(nextIndexToQueue >= 0 && nInQueue < nToQueue){
							if(writeLock[nextIndexToQueue] != null){
								logr.log(Level.SEVERE, "SANITY FAIL: We already have a write lock for image "+nextIndexToQueue+" but are trying to get it again!!");
							}else{
								try{							
									writeLock[nextIndexToQueue] = images[nextIndexToQueue].writeLock();
									writeLock[nextIndexToQueue].lockInterruptibly();														
								}catch(InterruptedException err){
									logr.log(Level.FINE, "Interrupted getting write lock for image " + nextIndexToQueue);
									//we assume that the lock wasn't acquired, and need to mark that
									//to stop follow on sanity failures
									writeLock[nextIndexToQueue] = null;
									if(signalDeath || signalAbort)
										throw new CaptureAbortedException();
									continue; //skip this image
								}
							}
							images[nextIndexToQueue].invalidate(false);
							camDrv.queueBuffer(images[nextIndexToQueue].getWritableBuffer(writeLock[nextIndexToQueue]), (Integer)nextIndexToQueue, (int)imgDataSize);
							if((nextIndexToQueue % 10) == 0)
								spinLog("Queued image " + nextIndexToQueue);
							nInQueue++;
							nextIndexToQueue++;
							if(nextIndexToQueue >= cfg.nImagesToAllocate){
								nextIndexToQueue = cfg.isContinuous() ? (cfg.blackLevelFirstFrame ? 1 : 0) : -1; //wrap queue
							}
						}
						
					}catch (AndorSDKException e) {
						writeLock[nextIndexToQueue].unlock();
						writeLock[nextIndexToQueue] = null;	
						throw e;
					}

					if(isAcqusitionStarted() && !signalAcquireStart) {
						camDrv.command("Acquisition Stop");
						setStatus(Status.awaitingSoftwareTrigger, "Capturing, but was stopped (intentionally).");
					}
					
					if(signalDeath || signalAbort) {
						//camDrv.command("Acquisition Stop");
						throw new CaptureAbortedException();
					}
					
					//get one image, if it's there
					try{
						
						//every now and then () poke the camera and see if it errors (to see if e.g. the power has been cut)
						if(nextIndexToGet == 0 //which also trap the live roll-around case... arg
								&& status != Status.capturing // so don't do it while actually running
								&& cfg.cameraHealthCheckPeriodMS > 0 //is enabled?
								&& (System.currentTimeMillis() - lastImageLoadTime) > cfg.cameraHealthCheckPeriodMS){
							camDrv.setBool("SensorCooling", (Boolean)cfg.getFeature("SensorCooling").value);
							lastImageLoadTime = System.currentTimeMillis();
							logr.fine("AndorCamCapture: Health check OK.");
						}
												
						int doneIndex = (Integer)camDrv.waitBuffer(timeout);
						if(doneIndex >= 0){
							if(walltimeFirstImageReadMS < 0) 
								walltimeFirstImageReadMS = System.currentTimeMillis();								
							
							
							this.status = Status.capturing; //set status quickly (don't trigger updates)
							
							//before dropping the write lock on the buffer
							// get the metadata, if it has it
							if(hasMetadata){
								
								ByteBuffer buff = images[doneIndex].getWritableBuffer(writeLock[doneIndex]);
								
								//this doesnt' quite seem to follow the documentation
								//so we try to get the nonsense.
								//The ticks CIS block seems to be the end one anyway
								int nextLengthPos = (int)(imgDataSize - 4);
								int blockLength;
								do{
									blockLength = buff.getInt(nextLengthPos);
									int cid = buff.getInt(nextLengthPos - 4);
									if(cid == 1 && blockLength > 0){ //CID for ticks
										ticksData[doneIndex] = new byte[blockLength - 4];
										int ticksPos = nextLengthPos - blockLength;
										buff.position(ticksPos);
										buff.get(ticksData[doneIndex]);
									
										break;
										
									}
									nextLengthPos -= blockLength;									
								}while(nextLengthPos > 0 && blockLength > 0);
								
								processTicksData(doneIndex);								
							}							
							
							if(writeLock[doneIndex] == null){
								logr.log(Level.SEVERE, "SANITY FAIL: We don't have the write lock for image "+doneIndex+" but we should!!");
							}else{								
								writeLock[doneIndex].unlock();
								writeLock[doneIndex] = null;
							}							
							//images[doneIndex].imageChanged(true);
							if(doneIndex % 10 == 0)
								spinLog("Recieved image " + doneIndex);
							nInQueue--;
							if(doneIndex != nextIndexToGet)
								throw new RuntimeException("Retrieved image had index " + doneIndex + ", but we were expecting " + nextIndexToGet + " next.");
							
							nextIndexToGet++;							
							if(nextIndexToGet >= cfg.nImagesToAllocate){
								nextIndexToGet = cfg.isContinuous() ? (cfg.blackLevelFirstFrame ? 1 : 0) : -1; //wrap queue
							}
														
							source.imageCaptured(doneIndex);
						}
						
					}catch(AndorSDKException err){
						//ignore timeout 'errors'
						if(err.getErrorCode() != AndorSDKException.AT_ERR_TIMEDOUT &&
								err.getErrorCode() != AndorSDKException.AT_ERR_NODATA)
							throw err;
					}
					
				}while(!signalDeath && !signalAbort && (cfg.isContinuous() || nextIndexToGet > -1)); //all done??

				if(!signalAbort && !signalDeath){
					//tell the source we're done successfully
					source.captureComplete();
				}
				
			}finally{ 
				
				try{
					camDrv.command("Acquisition Stop");
				}catch(AndorSDKException err){ 
					logr.log(Level.WARNING, "Acquisition stop in abort failed", err);
				}
				
				//flush the cameras queues and drop writing lock to everything in it
				camDrv.flush();
				while(nextIndexToGet != nextIndexToQueue){
					if(writeLock[nextIndexToGet] == null){
						logr.log(Level.SEVERE, "SANITY FAIL: We don't have the write lock for image "+nextIndexToGet+" but we should!!");
					}else{
						writeLock[nextIndexToGet].unlock();
						writeLock[nextIndexToGet] = null;
					}
					//images[nextIndexToGet].imageChanged(true);
					images[nextIndexToGet].invalidate();
					spinLog("Aborted image " + nextIndexToGet);
					nextIndexToGet++;
					if(nextIndexToGet >= cfg.nImagesToAllocate){
						nextIndexToGet = cfg.isContinuous() ? (cfg.blackLevelFirstFrame ? 1 : 0) : -1; //wrap queue
					}
				}

				source.setSeriesMetaData("AndorCam/walltimeFirstImageRead", walltimeFirstImageReadMS, false);
			}
		}finally{ ///final sanity failure catch: never ever ever leave with a write lock in place!!!
			
			for(int i=0; i < cfg.nImagesToAllocate; i++){
				if(writeLock[i] != null){
					logr.log(Level.SEVERE, "SANITY FAIL: We still have the write lock for image "+i+" but we shouldn't!! Dropping it now.");
					writeLock[i].unlock();
					writeLock[i] = null;
				}
			}			
		}
	}
	
	private void processTicksData(int i) {

		source.setImageMetaData("AndorCam/ticksData", i, ticksData[i]);
		 
		ByteBuffer buff = ByteBuffer.wrap(ticksData[i]);
		buff.order(ByteOrder.LITTLE_ENDIAN);
		//Mat.dumpArray(ticksData);
		
		ticksLong[i] = buff.getLong(0);
		if(ticksLong[i] < 0)
			ticksLong[i] = Long.MAX_VALUE + ticksLong[i] + 1;
		
		logr.finer("ticksLong["+i+"] = " + ticksLong);
		
		if(ticksLongFirst == Long.MIN_VALUE){
			ticksLongFirst = ticksLong[i];
		}
		
		double tickDbl = 0;
		double mul = 25e-9; //ticks are 25 microsecs										
		for(int j = 0; j < ticksData[i].length; j++){
			double val = (ticksData[i][j] < 0) ? (256 + ticksData[i][j]) : ticksData[i][j]; 
			tickDbl += val * mul;
			mul *= 256;
			
		}
		logr.finer("ticksDbl["+i+"] = " + tickDbl);
		source.setImageMetaData("AndorCam/ticksDouble", i, tickDbl);
		
		double ticksDoubleDiff = (ticksLong[i] - ticksLongFirst) * 25e-9  + offsetFromTrigger;
		
		//best we can do for nanotime at the moment is the camera tick, but relative to
		//whenever we realised it had started running
		nanoTime[i] = (ticksLong[i] - ticksLongFirst) * 25 + walltimeFirstImageReadMS * 1_000_000;
		time[i] = (ticksLong[i] - ticksLongFirst) * 25e-9;
		
	}

	private void deinit(){
		logr.info(" deInit()");
		
		if(camDrv != null)
			camDrv.close();
		camDrv = null;
		cameraOpen = false;
	}
			
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		spinLog("Starting AndorCam capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting...");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(AndorCamConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(AndorCamConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	
	/** Signal the thrad to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
		
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				logr.info("AndorCamCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.info("AndorCamCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				logr.info("AndorCamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.fine("AndorCamCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}

	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
		
	public void setConfig(AndorCamConfig config) { this.cfg = config; }

	@Override
	protected CaptureSource getSource() { return source; }
	
	
}
