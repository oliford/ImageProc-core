package imageProc.sources.capture.andorCam.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImgSource;
import imageProc.sources.capture.andorCam.AndorCamConfig;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private AndorCamSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo shutterModeCombo;
	private Combo gainModeCombo;
	private Combo triggerModeCombo;
	private Button sensorCoolingButton;
	
	private Text exposureLengthTextbox;
	private Button exposureMinButton; 
	private Button exposureMaxButton; 
	private Button overlappedModeCheckbox;
	
	private Text frameTimeTextbox;
	private Button frameTimeSetCheckbox;
	private Button frameTimeMinButton; 
	private Button frameTimeMaxButton; 
	
	private Combo binningCombo;
	private Button roiSetCheckbox; 
	private Spinner roiX0Text, roiY0Text, roiWText, roiHText;
	private Button roiMaxButton;
	
	public SimpleSettingsPanel(Composite parent, int style, AndorCamSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		sensorCoolingButton = new Button(swtGroup, SWT.CHECK);
		sensorCoolingButton.setText("Cooling (T = ????????)");
		sensorCoolingButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		sensorCoolingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lEM = new Label(swtGroup, SWT.NONE); lEM.setText("Shutter:");
		shutterModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		shutterModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		shutterModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lGM = new Label(swtGroup, SWT.NONE); lGM.setText("Gain:");
		gainModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		gainModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		gainModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lHWT = new Label(swtGroup, SWT.NONE); lHWT.setText("H/W Trigger:");
		triggerModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		triggerModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		triggerModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Frame/µs:");
		
		
		frameTimeSetCheckbox = new Button(swtGroup, SWT.CHECK);
		frameTimeSetCheckbox.setText("Set");
		frameTimeSetCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		frameTimeSetCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		frameTimeTextbox = new Text(swtGroup, SWT.NONE);
		frameTimeTextbox.setText("50000");
		frameTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		frameTimeTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		frameTimeMinButton = new Button(swtGroup, SWT.PUSH);
		frameTimeMinButton.setText("Min");
		frameTimeMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		frameTimeMinButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { frameTimeMinMaxEvent(false); } });
		
		frameTimeMaxButton = new Button(swtGroup, SWT.PUSH);
		frameTimeMaxButton.setText("Max");
		frameTimeMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		frameTimeMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { frameTimeMinMaxEvent(true); } });
		
		Label lT2 = new Label(swtGroup, SWT.NONE); lT2.setText("Exp/µs");
		overlappedModeCheckbox = new Button(swtGroup, SWT.CHECK);
		overlappedModeCheckbox.setText("Overlap");
		overlappedModeCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		overlappedModeCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("10000");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		exposureMinButton = new Button(swtGroup, SWT.PUSH);
		exposureMinButton.setText("Min");
		exposureMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		exposureMinButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(false); } });
		
		exposureMaxButton = new Button(swtGroup, SWT.PUSH);
		exposureMaxButton.setText("Max");
		exposureMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		exposureMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(true); } });
		
		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Bining:");
		binningCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		binningCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		binningCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		roiSetCheckbox = new Button(swtGroup, SWT.CHECK);
		roiSetCheckbox.setText("ROI:");
		roiSetCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		roiSetCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lRX = new Label(swtGroup, SWT.NONE); lRX.setText("X:");
		roiX0Text = new Spinner(swtGroup, SWT.NONE);
		roiX0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiX0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lRY = new Label(swtGroup, SWT.NONE); lRY.setText("Y:");
		roiY0Text = new Spinner(swtGroup, SWT.NONE);
		roiY0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		roiMaxButton = new Button(swtGroup, SWT.PUSH);
		roiMaxButton.setText("Max");
		roiMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		roiMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { roiMaxEvent(); } });
		
		Label lRW = new Label(swtGroup, SWT.NONE); lRW.setText("W:");
		roiWText = new Spinner(swtGroup, SWT.NONE);
		roiWText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiWText.setTextLimit(4);
		roiWText.setValues(0, 0, 9999, 0, 1, 1);
		roiWText.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lRH = new Label(swtGroup, SWT.NONE); lRH.setText("H:");
		roiHText = new Spinner(swtGroup, SWT.NONE);
		roiHText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiHText.setTextLimit(4);
		roiHText.setValues(0, 0, 9999, 0, 1, 1);
		roiHText.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		configToGUI();
	}

	void configToGUI(){
		AndorCamConfig config = source.getConfig();
		
		
		continuousButton.setSelection(config.featureMatchesString("CycleMode", "Continuous"))	;
		sensorCoolingButton.setSelection(config.boolFeature("SensorCooling"));
		
		double temp = config.doubleFeature("SensorTemperature");
		String tempStr = (new DecimalFormat("##.###")).format(temp);
		sensorCoolingButton.setText("Cooling (T="+tempStr+")");
		
		AndorFeature shutterModeFeature = config.getFeature("ElectronicShutteringMode");
		if(shutterModeFeature != null && shutterModeFeature.isFilled && shutterModeFeature.enumStrings != null){
			shutterModeCombo.setItems(shutterModeFeature.enumStrings);
			shutterModeCombo.select(shutterModeFeature.getEnumIndex("ElectronicShutteringMode"));
			shutterModeCombo.setEnabled(true);
		}else{
			shutterModeCombo.setText("No Data");
			shutterModeCombo.setEnabled(false);
		}
		
		AndorFeature gainModeFeature = config.getFeature("SimplePreAmpGainControl");
		if(gainModeFeature != null && gainModeFeature.isFilled && gainModeFeature.enumStrings != null){
			gainModeCombo.setItems(gainModeFeature.enumStrings);
			gainModeCombo.select(gainModeFeature.getEnumIndex("SimplePreAmpGainControl"));
			gainModeCombo.setEnabled(true);
		}else{
			gainModeCombo.setText("No Data");
			gainModeCombo.setEnabled(false);
		}
		
		AndorFeature triggerModeFeature = config.getFeature("TriggerMode");
		if(triggerModeFeature != null && triggerModeFeature.isFilled && triggerModeFeature.enumStrings != null){
			triggerModeCombo.setItems(triggerModeFeature.enumStrings);
			triggerModeCombo.select(triggerModeFeature.getEnumIndex("TriggerMode"));
			triggerModeCombo.setEnabled(true);
		}else{
			triggerModeCombo.setText("No Data");
			triggerModeCombo.setEnabled(false);
		}
		
		overlappedModeCheckbox.setSelection(config.boolFeature("Overlap"));
		exposureLengthTextbox.setText(Double.toString(config.doubleFeature("ExposureTime") * 1e6));
		
		AndorFeature frameRateFeature = config.getFeature("FrameRate");
		if(frameRateFeature != null && frameRateFeature.isFilled && frameRateFeature.value != null){
			
			frameTimeTextbox.setText(Double.toString((1e6 / (Double)frameRateFeature.value)));
			frameTimeSetCheckbox.setSelection(frameRateFeature.toSet);
			frameTimeTextbox.setEnabled(frameRateFeature.toSet);
			if((Double)frameRateFeature.value < (Double)frameRateFeature.minValue ||
				(Double)frameRateFeature.value > (Double)frameRateFeature.maxValue){
				frameTimeTextbox.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_RED));
			}else{
				frameTimeTextbox.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLUE));
			}
		}
		
		
		AndorFeature binningFeature = config.getFeature("AOIBinning");
		if(binningFeature != null && binningFeature.isFilled && binningFeature.enumStrings != null){
			binningCombo.setItems(binningFeature.enumStrings);
			binningCombo.select(binningFeature.getEnumIndex("AOIBinning"));
			binningCombo.setEnabled(true);
		}else{
			binningCombo.setText("No Data");
			binningCombo.setEnabled(false);
		}
		
		//roi top/left are 1 based in config (which is stupid)
		roiX0Text.setSelection(config.intFeature("AOILeft") - 1);
		roiWText.setSelection(config.intFeature("AOIWidth"));
		roiY0Text.setSelection(config.intFeature("AOITop") - 1);
		roiHText.setSelection(config.intFeature("AOIHeight"));
		
		AndorFeature aoiFeature = config.getFeature("AOILeft");
		if(aoiFeature != null){
			roiSetCheckbox.setEnabled(true);
			roiSetCheckbox.setSelection(aoiFeature.toSet);
		}
		
	}
	
	void guiToConfig(){
		AndorCamConfig config = source.getConfig();
		
		config.setFeature("CycleMode", continuousButton.getSelection() ? "Continuous" : "Fixed");
		
		config.setFeature("SensorCooling", sensorCoolingButton.getSelection());
		config.setFeature("ElectronicShutteringMode", shutterModeCombo.getText());
		config.setFeature("SimplePreAmpGainControl", gainModeCombo.getText());
		config.setFeature("TriggerMode", triggerModeCombo.getText());
		double expTime = Algorithms.mustParseDouble(exposureLengthTextbox.getText());
		config.setFeature("ExposureTime", 1e-6 * expTime);
		exposureLengthTextbox.setText(Double.toString(expTime));
		config.setFeature("Overlap", overlappedModeCheckbox.getSelection());

		double frameTime = Algorithms.mustParseDouble(frameTimeTextbox.getText());
		
		config.setFeature("FrameRate", (Double)(1e6 / frameTime));
		frameTimeTextbox.setText(Double.toString(frameTime));
		if(frameTimeSetCheckbox.getSelection()){
			frameTimeTextbox.setEnabled(true);
			AndorFeature frameRateFeature = config.getFeature("FrameRate");
			if((Double)frameRateFeature.value < (Double)frameRateFeature.minValue ||
					(Double)frameRateFeature.value > (Double)frameRateFeature.maxValue){
					frameTimeTextbox.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_RED));
				}else{
					frameTimeTextbox.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLUE));
				}
			
		}else{
			frameTimeTextbox.setEnabled(false);
			config.dontSetFeature("FrameRate");
		}
						
		config.setFeature("AOIBinning", binningCombo.getText());
		
		if(roiSetCheckbox.getSelection()){
			//roi top/left are 1 based in config (which is stupid)
			config.setFeature("AOILeft", roiX0Text.getSelection() + 1);
			config.setFeature("AOIWidth", roiWText.getSelection());
			config.setFeature("AOITop", roiY0Text.getSelection() + 1);
			config.setFeature("AOIHeight", roiHText.getSelection());
		}else{
			config.dontSetFeature("AOILeft");
			config.dontSetFeature("AOIWidth");
			config.dontSetFeature("AOITop");
			config.dontSetFeature("AOIHeight");
		}
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		AndorCamConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMin("FrameRate");
		}else{

			config.setFeatureToMax("FrameRate");
		}
		configToGUI();
	}
	
	public void exposureMinMaxEvent(boolean max){
		AndorCamConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMax("ExposureTime");
		}else{

			config.setFeatureToMin("ExposureTime");
		}
		configToGUI();
	}


	public void roiMaxEvent(){
		AndorCamConfig config = source.getConfig();

		config.setFeatureToMax("AOIWidth");
		config.setFeatureToMax("AOIHeight");
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
