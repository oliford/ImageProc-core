package imageProc.sources.capture.andorCam.swt;

import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImgSource;
import imageProc.sources.capture.andorCam.AndorCamConfig;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature;
import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature.FeatureType;
import net.jafama.FastMath;

public class FullSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private AndorCamSource source;
	private Composite swtGroup;
	
	private final static String colNames[] = new String[]{ "Feature", "Type", "Value", "Min", "Max", "Writable", "Set" };			
	
	private Table featuresTable;
	private TableEditor featuresTableEditor;
	
	public FullSettingsPanel(Composite parent, int style, AndorCamSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new FillLayout());
		
		featuresTable = new Table(swtGroup, SWT.BORDER);				
		//featuresTable.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, true, 5, 0));
		featuresTable.setHeaderVisible(true);
		featuresTable.setLinesVisible(true);	
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(featuresTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]); 
			cols[i].pack();
		}
		
		TableItem blankItem = new TableItem(featuresTable, SWT.NONE);
		for(int i=0; i < colNames.length; i++)
			blankItem.setText(i, "     ");		

		for(int i=0; i < colNames.length; i++){
			cols[i].pack();
		}
		
		featuresTableEditor = new TableEditor(featuresTable);
		featuresTableEditor.horizontalAlignment = SWT.LEFT;
		featuresTableEditor.grabHorizontal = true;
		featuresTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });

		swtGroup.pack();
		
	}
	
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event){
		
		Rectangle clientArea = featuresTable.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = featuresTable.getTopIndex ();
		while (index < featuresTable.getItemCount ()) {
			boolean visible = false;
			final TableItem item = featuresTable.getItem (index);
			for (int i=0; i < featuresTable.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;

					final AndorCamConfig.AndorFeature feature = source.getConfig().getFeature(item.getText(0));
					
					if(column == 6){ //the 'set' column.
						//Which we just toggle if it's writable
						feature.toSet = !feature.toSet;
						item.setText(6, feature.toSet ? "Y" : "");
						break;
					}
					
					if(!feature.isFilled || !feature.isImplemented || !feature.isWritable){
						break;
					}
										
					if(column != 2) //otherweise only set the value
						break;
					
					
					if(feature.type == FeatureType.ENUM){
						final Combo combo = new Combo(featuresTable, SWT.DROP_DOWN | SWT.MULTI | SWT.READ_ONLY);
						for(int j=0; j < feature.enumStrings.length; j++){
							combo.add(feature.enumIsImplemented[j] 
									? feature.enumStrings[j] 
									: "-x- " + feature.enumStrings[j] + " -x-");
						}
												
						Listener comboListener = new Listener () {
							public void handleEvent (final Event e) {
								switch (e.type) {
								case SWT.FocusOut:
									System.out.println("SWT.FocusOut");
									combo.dispose();
									if(true)break;
								case SWT.Selection:								
								case SWT.Traverse:
									switch (e.detail) {
									case SWT.TRAVERSE_RETURN:
										String value2 = combo.getText();
										item.setText(column, value2);
										feature.setValueByString(value2); 
										feature.toSet = true;
										item.setText(6, "Y");
										//checkTable();
										//FALL THROUGH
									case SWT.TRAVERSE_ESCAPE:
										combo.dispose();
										e.doit = false;
									}
									break;
								}
							}
						};
						combo.addListener (SWT.FocusOut, comboListener);
						combo.addListener (SWT.Traverse, comboListener);
						combo.addListener (SWT.Selection, comboListener);
						featuresTableEditor.setEditor(combo, item, i);
						combo.setText(item.getText(i));
						//combo.selectAll();
						combo.setFocus();
						return;
						
						
					}else{
						final Text text = new Text(featuresTable, SWT.NONE);
						Listener textListener = new Listener () {
							public void handleEvent (final Event e) {
								switch (e.type) {
								case SWT.FocusOut:
									String value = text.getText();
									item.setText(column, value);
									feature.setValueByString(value);
									feature.toSet = true;
									item.setText(6, "Y");
									//checkTable();
									text.dispose ();
									break;
								case SWT.Traverse:
									switch (e.detail) {
									case SWT.TRAVERSE_RETURN:
										String value2 = text.getText();
										item.setText(column, value2);
										feature.setValueByString(value2);
										feature.toSet = true;
										item.setText(6, "Y");
										//checkTable();
										//FALL THROUGH
									case SWT.TRAVERSE_ESCAPE:
										text.dispose();
										e.doit = false;
									}
									break;
								}
							}
						};
						text.addListener (SWT.FocusOut, textListener);
						text.addListener (SWT.Traverse, textListener);
						featuresTableEditor.setEditor (text, item, i);
						text.setText (item.getText (i));
						text.selectAll ();
						text.setFocus ();
						return;
					}
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}
	
	void configToGUI(){
		AndorCamConfig config = source.getConfig();
		
		featuresTable.removeAll();
		
		for(Entry<String, AndorFeature> entry : config.getAllFeatures().entrySet()){
			String nameKey = entry.getKey();
			AndorFeature feature = entry.getValue(); 
			
			TableItem item = new TableItem(featuresTable, SWT.NONE);
			item.setText(0, feature.name);
			item.setText(1, feature.typeAsString());
			item.setText(2, feature.valueAsString());
			item.setText(3, feature.minValueAsString());
			item.setText(4, feature.maxValueAsString());
			item.setText(5, feature.isWritable ? "Y" : "");
			item.setText(6, feature.toSet ? "Y" : "");	
		}
		

		for(int i=0; i < colNames.length; i++){
			featuresTable.getColumn(i).pack();
		}
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
