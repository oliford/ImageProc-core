package imageProc.sources.capture.andorCam;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.EventReciever.Event;
import imageProc.database.gmds.GMDSUtil;
import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature;
import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature.FeatureType;
import imageProc.sources.capture.andorCam.swt.AndorCamSWTControl;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.flir.FLIRCamConfig;
import imageProc.sources.capture.picam.PicamConfig;
import imageProc.sources.capture.picam.PicamConfig.Parameter;
import mds.GMDSFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import picamJNI.PICamDefs;
import picamJNI.PICamROIs;
import picamJNI.PICamROIs.PICamROI;
import signals.gmds.GMDSSignal;

/** Sensicam Image Source. 
 * All the actual driver'ey code is in the thread in SensicamCapture
 *  
 * @author oliford
 */
public class AndorCamSource extends CaptureSource {
	
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	private int lastCaptured;
	
	/** The low-level capturer, we should only have one of these */  
	private AndorCamCapture capture;
	
	/** The current config, should be kept always in sync with camera (if online)
	 * and with controller */
	private AndorCamConfig config;
	
	public AndorCamSource(AndorCamCapture capture, AndorCamConfig config) {
		this.capture = capture;
		this.config = (config != null) ? config : new AndorCamConfig();
	}
	
	public AndorCamSource() {
		capture = new AndorCamCapture(this);
		config = new AndorCamConfig();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by SensicamCapture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	
	/** Called by SensicamCapture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		lastCaptured = imageIndex;
		
		updateAllControllers();
		
	}
	
	private long lastAutoExposure = 0;
	private long autoExposureMinPeriod = 50; //ms
	private double autoExposureMinLevel = 0.6;

	private double maxSum = 0;
	private int samplesInSum;

	@Override
	public AndorCamSource clone() {
		return new AndorCamSource(capture, config);
	}

	public void openCamera(){
		if(capture.isOpen()){
			System.err.println("openCamera(): Camera already open");
			return;
		}
		capture.setConfig(config);
		capture.initCamera();
	}

	public void startCapture(){
		if(capture.isBusy()){
			System.err.println("startCapture(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		
		lastAutoExposure = System.currentTimeMillis();

		capture.setConfig(config);
		capture.startCapture(config, images);
	}
	
	public void syncConfig(){
		if(capture.isBusy()){
			System.err.println("syncConfig(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		
		lastAutoExposure = System.currentTimeMillis();	
		capture.setConfig(config);	
		capture.testConfig(config, images);
	}
		
	public void abort(){ capture.abort(false); }
	
	public void closeCamera(){ capture.closeCamera(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToAllocate + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). S/W Trigger"				
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	public String getCCDInfo(){
		AndorFeature camModel = config.getFeature("CameraModel");
				
		return "Andor " + ((camModel.value != null && camModel.value instanceof String) 
							? (String)camModel.value : "???") +
				": " + config.intFeature("SensorWidth") + " x " + config.intFeature("SensorHeight");
	}
	
	public int getNumCaptured(){ return nCaptured; }
	
	public boolean isActive(){ return capture.isOpen(); }
	
	public boolean isBusy(){ return capture.isBusy(); }
	
	@Override
	public boolean isIdle() { return capture == null || !capture.isOpen() || !capture.isBusy(); }

	@Override
	public AndorCamConfig getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (AndorCamConfig)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new AndorCamSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	public void releaseMemory() {
		capture.abort(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}
	
	@Override
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, AndorCamConfig.class);
		
		//json messes up the types of many of the parameters
		//try to fix them
		
		for(AndorFeature param : config.getAllFeatures().values()){
			switch(param.type){
				case INT: 
					param.value = (param.value == null) ? null : ((Number)param.value).longValue();
					param.minValue = (param.minValue == null) ? null : ((Number)param.minValue).longValue();
					param.maxValue = (param.maxValue == null) ? null : ((Number)param.maxValue).longValue();
					break;
				case FLOAT:
					param.value = (param.value == null) ? null : ((Number)param.value).doubleValue();
					param.minValue = (param.minValue == null) ? null : ((Number)param.minValue).doubleValue();
					param.maxValue = (param.maxValue == null) ? null : ((Number)param.maxValue).doubleValue();
					break;
			}
		}
		
		updateAllControllers();	
	}
	
	@Override
	public void loadConfigGMDS(String exp, int pulse) {
		if(pulse == -1 && connectedSource != null){
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		}	
		
		
		GMDSFetcher gmds = GMDSUtil.globalGMDS();
		
		// look for the new one first
		String rootPath = "RAW/seriesData/AndorCam/config";
		String sigsPaths[] = gmds.dumpTree(exp, pulse, rootPath, true);

		for (String signalPath : sigsPaths) {
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, exp,
					rootPath + "/" + signalPath);

			try {
				GMDSSignal sig = (GMDSSignal) gmds.getSig(sigDesc);
				Object data = sig.getData();
				
				if(data.getClass().isArray() && Array.getLength(data) == 1){
					data = Array.get(data, 0); //grumble
				}
				
				signalPath = signalPath.replaceAll("/", "");
				
				if(signalPath.equals("cameraIndex")){ continue; } 
				if(signalPath.equals("nImagesToQueue")){ config.nImagesToQueue = (Integer)data; continue; }
				if(signalPath.equals("nImagesToAllocate")){ config.nImagesToAllocate = (Integer)data; continue; }
				
				AndorFeature feature = config.getFeature(signalPath);
				
				if(feature == null){
					System.err.println("AndroCamSource.loadSettings("+exp + "," + pulse+"): No feature named '" + signalPath + "'.");
					continue;
				}
				
				if(!feature.isImplemented || !feature.isWritable){
					System.err.println("AndroCamSource.loadSettings("+exp + "," + pulse+"): Feature '" + signalPath + "' is not implemented or not writable.");
					continue;
				}
				
				if(data.getClass() == feature.typeAsObjectClass() ||
						(feature.type == AndorFeature.FeatureType.ENUM && data.getClass() == String.class)){
					if(!data.equals(feature.value)){
						feature.value = data;
						feature.toSet = true;
					}
					
				}else if(feature.typeAsPrimitiveClass() == boolean.class && data.getClass() == Integer.class){
					//allow conversion of int to boolean, since GMDS doesn't save booleans
					feature.value = (boolean)(((Integer)data) != 0);
					feature.toSet = true;
					
				}else{
					System.err.println("AndroCamSource.loadSettings("+exp + "," + pulse+"): Type of feature '"
										+feature.name+" doesn't match incoming "+data.getClass().getSimpleName());
				}
				
			} catch (RuntimeException err) {
				System.err.println("Couldn't read AndorCam config from signal '" + sigDesc
						+ "': " + err.getMessage());
			}
		}
			
		updateAllControllers();
	}
	
	@Override
	public void saveConfig(String id) {
		if(id.startsWith("jsonfile:")) {
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}

	public void saveConfigJSON(String fileName) {	
		
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.setPrettyPrinting()
				.create(); 
		String json = gson.toJson(config);
	
		OneLiners.textToFile(fileName, json);	
	}


	@Override
	public void close() {
		abort();
		closeCamera();		
	}

	@Override
	public boolean open(long timeoutMS) {
		
		openCamera();
		long t0 = System.currentTimeMillis();
		while(!capture.isOpen()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted while waiting for camera to open.");
			}
			
			if((System.currentTimeMillis() - t0) > timeoutMS){
				throw new RuntimeException("Timed out waiting for camera to open. Need a longer warm-up time??");
			}
		}
		
		
		//do a config sync as-is
		syncConfig();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) { 
			throw new RuntimeException("Interrupted while waiting for camera to open.");
		}

		return true;
	}


	public void setDiskMemoryLimit(long maxMemory) { capture.setDiskMemoryLimit(maxMemory); }
	
	public long getDiskMemoryLimit(){  return capture.getDiskMemoryLimit(); }

	@Override
	public String toShortString() {
		return "Andor3-CMOS"; 
	}

	public boolean isStartTriggerArmed() { return config.beginCaptureOnStartEvent;	}

	@Override
	protected final Capture getCapture() { return capture; }
	
	@Override
	public int getFrameCount() { return config.nImagesToAllocate; }
	
	@Override
	public void setFrameCount(int frameCount) {  
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.nImagesToAllocate = frameCount;
		config.setFeature("FrameCount", config.nImagesToAllocate);
		updateAllControllers();
	}
	
	@Override
	public long getFramePeriod() { return (long)(1.0e6 / config.doubleFeature("FrameRate")); }
	
	@Override
	public void setFramePeriod(long framePeriodUS) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.setFeature("FrameRate", 1.0e6 / framePeriodUS);
		updateAllControllers();
	}
	
	@Override
	public void setExposureTime(long exposureTimeUS) {
		config.setFeature("ExposureTime", (double)(exposureTimeUS/1e6));		
		updateAllControllers();
	}
	
	@Override
	public long getExposureTime() {
		return (long)(config.exposureTimeMS() * 1000);
	}	

	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig;	}
}
