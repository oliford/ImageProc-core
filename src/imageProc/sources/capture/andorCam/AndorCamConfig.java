package imageProc.sources.capture.andorCam;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import imageProc.sources.capture.andorCam.AndorCamConfig.AndorFeature.FeatureType;
import imageProc.sources.capture.base.CaptureConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class AndorCamConfig extends CaptureConfig {

	/** General stuff that's outside of the camera's own config */
	
	/** Number of images to allocate and to capture 
	 * if in non-continuous mode */	 
	public int nImagesToAllocate;

	/** Index of camera to use */
	public int cameraIndex;
	
	/** Number of buffers to queue at one time. -1 for all of them.  */
	public int nImagesToQueue;

	/** Open a simulated camera if none exist */
	public boolean allowDemoCamera = false;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame;
	
	/** Period at which to poke the camera to see if it's a live while waiting for external trigger */
	public long cameraHealthCheckPeriodMS = 5000;
	
	/** Period to poll temperature when idle */
	public int idleTempPollPeriodMS = 10000;
	
	public static class AndorFeature {
		public static enum FeatureType { UNKNOWN, BOOL, INT, FLOAT, STRING, ENUM };
		
		public String name = null;
		public boolean isFilled = false;
		public boolean isImplemented = false;
		public boolean isWritable = false;
		public Object value;
		public Object minValue;
		public Object maxValue;
		public FeatureType type;		
		public String[] enumStrings;
		public boolean[] enumIsImplemented;
		
		/** Order index. Features are set to camera lower order first */
		private int order;
		
		/** Should we attempt to set it on the next config sync? */
		public boolean toSet = false;
		
		
		public static void addFeature(TreeMap<String, AndorFeature> map, String name, FeatureType type, int order){
			map.put(name, new AndorFeature(name, type, order));
		}
		
		public AndorFeature(String name, FeatureType type, int order) {
			this.name = name; 
			this.type = type;
			this.order = order;
			this.toSet = false;
		}

		public String typeAsString() {
			if(!isImplemented)
				return "not impl";
			
			if(type == null)
				return "?";
			
			switch(type){
				case BOOL: return "bool";
				case INT: return "int";
				case FLOAT: return "float";
				case STRING: return "string";
				case ENUM: return "enum";
				default: return "unknown";
			}
		}
		
		public Class typeAsPrimitiveClass() {
			if(!isImplemented || type == null)
				return null;
			
			switch(type){
				case BOOL: return boolean.class;
				case INT: return long.class;
				case FLOAT: return double.class;
				case STRING: return String.class;
				case ENUM: return String[].class;
					default: return null;
			}
		}
		
		public Class typeAsObjectClass() {
			if(!isImplemented || type == null)
				return null;
			
			switch(type){
				case BOOL: return Boolean.class;
				case INT: return Long.class;
				case FLOAT: return Double.class;
				case STRING: return String.class;
				case ENUM: return String[].class;
					default: return null;
			}
		}

		public String valueAsString() {
			if(!isImplemented || type == null)
				return "-";
			switch(type){
				case BOOL: return value == null ? "" : ((Boolean)value).toString();
				case INT: return value == null ? "" : ((Long)value).toString();
				case FLOAT: return value == null ? "" : ((Double)value).toString();
				case STRING: return value == null ? "" : (String)value;
				case ENUM: return value == null ? "" : (String)value;
				default: return "?";
			}
		}
		
		public String minValueAsString() {
			if(!isImplemented || type == null)
				return "-";
			switch(type){
				case BOOL: return minValue == null ? "" : ((Boolean)minValue).toString();
				case INT: return minValue == null ? "" : ((Long)minValue).toString();
				case FLOAT: return minValue == null ? "" : ((Double)minValue).toString();
				case STRING: return minValue == null ? "" : (String)minValue;
				case ENUM: return minValue == null ? "" : (String)minValue;
				default: return "?";
			}
		}
		
		public String maxValueAsString() {
			if(!isImplemented || type == null)
				return "-";
			switch(type){
				case BOOL: return maxValue == null ? "" : ((Boolean)maxValue).toString();
				case INT: return maxValue == null ? "" : ((Long)maxValue).toString();
				case FLOAT: return maxValue == null ? "" : ((Double)maxValue).toString();
				case STRING: return maxValue == null ? "" : (String)maxValue;
				case ENUM: return maxValue == null ? "" : (String)maxValue;
				default: return "?";
			}
		}

		public void setValueByString(String value) {
			if(!isImplemented || type == null)
				return;
			switch(type){
				case BOOL: this.value = (Boolean)(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("T") || value.equalsIgnoreCase("TRUE")); break;
				case INT: this.value = (Long)Algorithms.mustParseLong(value); break;
				case FLOAT:  this.value = (Double)Algorithms.mustParseDouble(value); break;
				case STRING: this.value = value;
				case ENUM: this.value = value;
				default: // !?
			}
		}

		public int getEnumIndex(String name){
			if(value == null || enumStrings == null)
				return -1;
			
			for(int i=0; i < enumStrings.length; i++){
				if(value.equals(enumStrings[i]))
					return i;
			}
			return -1;
		}
	
	}
	
	public AndorCamConfig() {
		/*AndorFeature.addFeature(features, "ControllerID", FeatureType.INT);
		AndorFeature.addFeature(features, "CameraModel", FeatureType.STRING);
		AndorFeature.addFeature(features, "FrameCount", FeatureType.INT);
		AndorFeature.addFeature(features, "AOI Width", FeatureType.INT);
		AndorFeature.addFeature(features, "AOI Height", FeatureType.INT);
		AndorFeature.addFeature(features, "Sensor Width", FeatureType.INT);
		AndorFeature.addFeature(features, "Sensor Height", FeatureType.INT);
		AndorFeature.addFeature(features, "SimplePreAmpGainControl", FeatureType.ENUM);
		AndorFeature.addFeature(features, "PixelEncoding", FeatureType.ENUM);
		AndorFeature.addFeature(features, "Actual Exposure Time", FeatureType.INT);
		AndorFeature.addFeature(features, "ImageSizeBytes", FeatureType.INT);
		AndorFeature.addFeature(features, "AOIStride", FeatureType.INT);
		AndorFeature.addFeature(features, "TemperatureStatus", FeatureType.ENUM);		
		AndorFeature.addFeature(features, "ImageSizeBytes", FeatureType.INT);
		*/
		
		//Full set from SDK docs
		AndorFeature.addFeature(features, "AccumulateCount", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "AOIBinning", FeatureType.ENUM, 100); 		
		AndorFeature.addFeature(features, "AOIHBin", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "AOIHeight", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "AOILeft", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "AOIStride", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "AOITop", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "AOIVBin", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "AOIWidth", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "AuxiliaryOutSource", FeatureType.ENUM, 100); 
		AndorFeature.addFeature(features, "BaselineLevel", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "BitDepth", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "BufferOverflowEvent", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "BytesPerPixel", FeatureType.FLOAT, 100);
		AndorFeature.addFeature(features, "CameraAcquiring", FeatureType.BOOL, 100);  
		AndorFeature.addFeature(features, "CameraModel", FeatureType.STRING, 100);
		AndorFeature.addFeature(features, "ControllerID", FeatureType.STRING, 100);
		AndorFeature.addFeature(features, "CycleMode", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "DeviceCount", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "DeviceVideoIndex", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "ElectronicShutteringMode", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "EventEnable", FeatureType.BOOL, 100);
		AndorFeature.addFeature(features, "EventsMissedEvent", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "EventSelector", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "ExposureTime", FeatureType.FLOAT, 100);
		AndorFeature.addFeature(features, "ExposureEndEvent", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "ExposureStartEvent", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "FanSpeed", FeatureType.ENUM, 100); 
		AndorFeature.addFeature(features, "FirmwareVersion", FeatureType.STRING, 100); 
		AndorFeature.addFeature(features, "FrameCount", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "FrameRate", FeatureType.FLOAT, 100);
		AndorFeature.addFeature(features, "FullAOIControl", FeatureType.BOOL, 100); 
		AndorFeature.addFeature(features, "ImageSizeBytes", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "IOInvert", FeatureType.BOOL, 100); 
		AndorFeature.addFeature(features, "IOSelector", FeatureType.ENUM, 100); 
		AndorFeature.addFeature(features, "LUTIndex", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "LUTValue", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "MaxInterfaceTransferRate", FeatureType.FLOAT, 100); 
		AndorFeature.addFeature(features, "MetadataEnable", FeatureType.BOOL, 100); 
		AndorFeature.addFeature(features, "MetadataFrame", FeatureType.BOOL, 100); 
		AndorFeature.addFeature(features, "MetadataTimestamp", FeatureType.BOOL, 100); 
		AndorFeature.addFeature(features, "Overlap", FeatureType.BOOL, 90); 
		//AndorFeature.addFeature(features, "PixelCorrection", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "PixelEncoding", FeatureType.ENUM, 100); 
		AndorFeature.addFeature(features, "PixelHeight", FeatureType.FLOAT, 100);
		AndorFeature.addFeature(features, "PixelReadoutRate", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "PixelWidth", FeatureType.FLOAT, 100);
		AndorFeature.addFeature(features, "PreAmpGain", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "PreAmpGainChannel", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "PreAmpGainControl", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "PreAmpGainSelector", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "ReadoutTime", FeatureType.FLOAT, 100);
		AndorFeature.addFeature(features, "RowNExposureEndEvent", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "RowNExposureStartEvent", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "SensorCooling", FeatureType.BOOL, 100);
		AndorFeature.addFeature(features, "SensorHeight", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "SensorTemperature", FeatureType.FLOAT, 100); 
		AndorFeature.addFeature(features, "SensorWidth", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "SerialNumber", FeatureType.STRING, 100);
		AndorFeature.addFeature(features, "SimplePreAmpGainControl", FeatureType.ENUM, 100);   
		AndorFeature.addFeature(features, "SoftwareVersion", FeatureType.STRING, 100);
		AndorFeature.addFeature(features, "SpuriousNoiseFilter", FeatureType.BOOL, 100); 
		AndorFeature.addFeature(features, "Synchronous Triggering", FeatureType.BOOL, 100);
		AndorFeature.addFeature(features, "TargetSensor Temperature", FeatureType.FLOAT, 100);
		AndorFeature.addFeature(features, "Temperature Control", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "Temperature Status", FeatureType.ENUM, 100);
		AndorFeature.addFeature(features, "TimestampClock", FeatureType.INT, 100);
		AndorFeature.addFeature(features, "TimestampClock Frequency", FeatureType.INT, 100); 
		AndorFeature.addFeature(features, "TriggerMode", FeatureType.ENUM, 100);	
		
		//defaults
		cameraIndex = 0;
		nImagesToAllocate = 20;
		blackLevelFirstFrame = false;
		setFeature("FrameCount", nImagesToAllocate);
		nImagesToQueue = 2;
		setFeature("SensorCooling", true);
		setFeature("MetadataEnable", true);
		setFeature("MetadataTimestamp", true);
		
	}
	

	//we just store everything that the SDK presents as 'features'
	private TreeMap<String, AndorFeature> features = new TreeMap<String, AndorFeature>();

	
	
	public TreeMap<String, AndorFeature> getAllFeatures() { return features;  }

	public int intFeature(String name) {
		AndorFeature feature = features.get(name);
		if(feature == null || feature.value == null)
			return -1;
		return (int)(long)((Long)feature.value); //ffs with the casting 
	}
	
	//public int nImagesToCapture() { return intFeature("FrameCount"); }
	public int imageSizeBytes(){ return intFeature("ImageSizeBytes"); }
	public int imageWidth() { 
		int x = intFeature("AOIWidth");
		return x > 0 ? x : intFeature("SensorWidth");
	}
	public int imageHeight(){
		int x = intFeature("AOIHeight");
		return x > 0 ? x : intFeature("SensorHeight");
	}
	public double frameTimeMS() { return 1000.0 / doubleFeature("FrameRate");	}
	public double exposureTimeMS() { return 1000.0 * doubleFeature("ExposureTime"); }
	
	public AndorFeature getFeature(String name) { return features.get(name); }

	public boolean isContinuous() {
		return "Continuous".equals(getFeature("CycleMode").value);
	}

	public boolean featureMatchesString(String name, String value) {
		AndorFeature feature = features.get(name);
		if(feature == null || feature.value == null)
			return false;
		return feature.value.equals(value); 
	}

	public boolean boolFeature(String name) {
		AndorFeature feature = features.get(name);
		if(feature == null || feature.value == null)
			return false;
		return (Boolean)feature.value; 
	}

	public double doubleFeature(String name) {
		AndorFeature feature = features.get(name);
		if(feature == null || feature.value == null)
			return Double.NaN;
		return (Double)feature.value; 
	}

	/**These set the feature in our config and mark them to be set */
	
	public void setFeature(String name, Integer value) { setFeature(name, (long)value); }
	public void setFeature(String name, Double value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Long value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Boolean value) { setFeatureToObj(name, value); }
	public void setFeature(String name, String value) { setFeatureToObj(name, value); }
	
	private void setFeatureToObj(String name, Object value) {
		AndorFeature feature = features.get(name);
		if(feature != null){			
			feature.value = value;
			feature.toSet = true;
		}
	}

	public void dontSetFeature(String name) {
		AndorFeature feature = features.get(name);
		if(feature != null){		
			feature.toSet = false;
		}
	}
	
	public void setFeatureToMin(String name) {
		AndorFeature feature = features.get(name);
		if(feature != null){			
			feature.value = feature.minValue;
			feature.toSet = true;
		}
	}
	
	public void setFeatureToMax(String name) {
		AndorFeature feature = features.get(name);
		if(feature != null){			
			feature.value = feature.maxValue;
			feature.toSet = true;
		}
	}

	/** Creates a simple HashMap of java number/string objects etc */
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
	
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/nImagesToQueue", nImagesToQueue);
		map.put(prefix + "/cameraIndex", cameraIndex);
		map.put(prefix + "/allowDemoCamera", allowDemoCamera);
		
		for(AndorFeature feature : features.values()){
			if(feature.isFilled && feature.value != null)
				map.put(prefix + "/" + feature.name, feature.value);
		}
		
		return map;
	}

	/** Return list of features ordered according to their 'order' value, for setting */
	public Collection<AndorFeature> getSetOrderedFeatures() {

		ArrayList<AndorFeature> al = new ArrayList<AndorFeature>(features.values());
		
		Collections.sort(al, new Comparator<AndorFeature>() {
			@Override
			public int compare(AndorFeature o1, AndorFeature o2) {				
				return Integer.compare(o1.order, o2.order);
			}
		});
		
		return al;
		
	}

		
}
