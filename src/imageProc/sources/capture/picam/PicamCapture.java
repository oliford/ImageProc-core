package imageProc.sources.capture.picam;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

import picamJNI.PICam;
import picamJNI.PICamCollectionConstraint;
import picamJNI.PICam.AcquisitionStatus;
import picamJNI.PICamAcquisitionUpdatedCallback;
import picamJNI.PICamDefs;
import picamJNI.PICamEnumerations;
import picamJNI.PICamParameters;
import picamJNI.PICamROIs;
import picamJNI.PICamROIs.PICamROI;
import picamJNI.PICamRangeConstraint;
import picamJNI.PICamSDKException;
import picamJNI.PicamCameraID;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.picam.PicamConfig.Parameter;
import otherSupport.SettingsManager;


/** Thread runner used to copy in images.
 *  
 * It contains all the actual JNI calls to the camera library 
 * 
 * I think the SDK is thread safe but not entirely sure.
 */
public class PicamCapture extends Capture implements Runnable {
		
	private PicamSource source;
		
	private ByteBufferImage images[];
	
	private PicamConfig cfg;
		
	private Thread thread;
	
	/** Internal state */
	private PICam camera;
	
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;
		
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
		
	/** Completely reset the configuration to defaults */
	private boolean resetConfig = false;
	
	private long wallCopyTimeMS0 = Long.MIN_VALUE;
	private long wallCopyTimeMS[];
	
	private long timestampStart0 = Long.MIN_VALUE;
	private long timestampStart[], timestampEnd[];
	
	private long nanoTime[];
	private double time[];
	
	public PicamCapture(PicamSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera
		
		//on Ubuntu 20.04, this needs to be called early, otherwise it Segfaults
		//It didnt do that in Ubuntu 16.04
		//erm....
		//this perhaps isnt needed as it only crashes after JFreeChartGraph hast been instantiated.
		//why that has to do with the price of fish I don't know but yea, workaround is to not create the graph until later
		
		if(false && Boolean.parseBoolean(SettingsManager.defaultGlobal().getProperty("imageProc.source.capture.picam.preInitializeLibrary","false"))) {
			logr.info("Pre-initialising library (because it segfaults on Ubuntu 20.04 if done later)...");
			PICam.InitializeLibrary();
			logr.info("Done.");
		}
	}
		
	@Override
	public void run() {
		threadBusy = true;
		try{
			if(camera != null){
				camera.close();
				throw new RuntimeException("Camera driver already open, aborting both");
			}
			
			setStatus(Status.init, "Initialising camera...");
			initDevice();
			
			signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Camera init done.");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();					
					}
					
					if(!signalCapture){
						setStatus(Status.completeOK, "Config done.");						
						
					}else{
						signalCapture = false;
									
						setStatus(Status.init, "Allocating image memory...");		
						
						initImages();
						
						setStatus(Status.init, "Config done, starting capture...");
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("PICam/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");					
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal: " + err, err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
					err.printStackTrace();
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("PICam thread caught Exception:" + err);
			setStatus(Status.errored, "Aborted/Errored:" + err);
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				System.err.println("PICam thread caught exception de-initing camera.");
				err.printStackTrace();
			}
		}
	}
	
	
	private void initDevice() {
		setStatus(Status.init, "Initialising library... (May take a few minutes)");
		PICam.InitializeLibrary();
		
		PicamCameraID id;
		 
		setStatus(Status.init, "Opening camera... (May take a few minutes)");
		try{
	    	camera = PICam.OpenFirstCamera();
			//PicamCameraID targetID = new PicamCameraID("ProEMHS512BExcelon", PicamCameraID.computerInterface_GigabitEthernet, 
			//										"12361416", "E2V 512 x 512 (CCD 97)(B)(eXcelon)");
			
			//camera = PICam.OpenCamera(targetID);
	    	id = camera.getCameraID();
	    	
	    }catch(PICamSDKException err){
	    	if(cfg.allowDemoCamera){
		        id = PICam.ConnectDemoCamera(PicamCameraID.getModelID("Pixis100F"), "0008675309");
		        //camera = PICam.OpenCamera(id);
		        camera = PICam.Advanced_OpenCameraDevice(id);
		        logr.info("No Camera Detected, Creating Demo Camera");
	    	}else{
	    		throw new RuntimeException("Unable to open camera ("+err.getMessage()+")", err);
	    	}
	    }
		
		cfg.id = id;
	    	    
		logr.info(".initCamera(): Opened camera: Model = " + id.getModelName() 
					+ ", S/N=" + id.getSerialNumber() 
					+ ", Sensor=" + id.getSensorName());
		
		cameraOpen = true;				
		
	}
	
	private void syncConfig(){
		
		//check consistency. The ROI 'setting' has to be a bit weird to allow ROIs to be stored that arn't loaded to camera
		if(cfg.enableROIs){
			Parameter roisParam = cfg.getFeature("Rois");
			if(roisParam != null)
				roisParam.toSet = true;
		}
		
		if(cfg.isContinuous() && cfg.nImagesToAllocate <= 1){
			throw new IllegalArgumentException("nImage=1 and contiuous is on. This is a ridiculous thing to ask for!");
		}
		
		writeConfig();
		
		int failed[] = camera.CommitParameters();
		if(failed.length > 0){
			StringBuilder strB = new StringBuilder("Invalid parameters: ");
			
			for(int paramID : failed){
				strB.append(PICamParameters.getName(paramID));
				strB.append(", ");
			}
			
			String str = strB.toString();
			logr.warning("PICam.syncConfig(): " + str);
			
			throw new RuntimeException(str);
		}
		
		readConfig();
		
		//ensure consistency of number of images
		if(!cfg.isContinuous()){
			cfg.nImagesToAllocate = (int)cfg.longFeature("ReadoutCount");
		}
		
		
		source.configChanged();
		logr.info(".syncConfig(): config sync done");
	}
	
	private void writeConfig(){
		for(Parameter param : cfg.getAllFeatures().values()){
			try{
				if(!param.toSet || !param.isImplemented || !param.isWritable || !param.isFilled)
					continue;
				
				switch(param.type){
					case PICamDefs.valueType_FloatingPoint:
						float valF =  param.value instanceof Double 
										? (float)(double)(Double)param.value //java can really silly sometimes :/
										: (float)param.value;
						camera.setParameterFloatingPointValue(param.id, valF);					
						logr.fine("Set '"+param.name+"' ("+param.id+") = " + valF + " (float)");
						break;
						
					case PICamDefs.valueType_Integer:
						int valI = (int)param.value;
						camera.setParameterIntegerValue(param.id, valI);					
						logr.fine("Set '"+param.name+"' ("+param.id+") = " + valI + " (int)");
						break;
						
					case PICamDefs.valueType_LargeInteger:
						long valL = (long) param.value;
						camera.setParameterLargeIntegerValue(param.id, valL);					
						logr.fine("Set '"+param.name+"' ("+param.id+") = " + valL + " (long)");
						break;
						
					case PICamDefs.valueType_Enumeration:
			    		int valE = (int)param.value;
			    		camera.setParameterIntegerValue(param.id, valE);
			    		logr.fine("Set '"+param.name+"' ("+param.id+") = " + valE + " (Enum " 
			    		+ ((valE < 0 || valE >= param.enumStrings.length) ? "(INVALID)" : param.enumStrings[valE]) + ")");
			    		break;
			    		
					case PICamDefs.valueType_Boolean:
						int valB = (boolean)param.value ? -1 : 0; 
						camera.setParameterIntegerValue(param.id, valB);					 
						logr.fine("Set '"+param.name+"' ("+param.id+") = " + param.value + " (bool)");
						break;
						
					case PICamDefs.valueType_Rois:				
						if(!cfg.enableROIs){ //force full frame
							PICamROI roi = new PICamROI();
							roi.name = "FullFrame";
							roi.enabled = true;
							roi.x = 0;
							roi.width = camera.getParameterIntegerValue(PICamParameters.SensorActiveWidth);
							roi.x_binning = 1;
							roi.y = 0;
							roi.height= camera.getParameterIntegerValue(PICamParameters.SensorActiveHeight);
							roi.y_binning = 1;
							PICamROIs rois = new PICamROIs();
							rois.addROI(roi);
							camera.setParameterRoisValue(param.id, (PICamROIs)rois);
														
							logr.fine("Setting full frame ROI for '"+param.name+"' ("+param.id+") because enableROIs=false");
							
						}else if(param.value != null && ((PICamROIs)param.value).getROIListCopy().size() > 0){
							camera.setParameterRoisValue(param.id, (PICamROIs)param.value);
							logr.fine("Set '"+param.name+"' ("+param.id+") = " + param.value + " (ROIs)");
							
						}else{							
							logr.warning("Not setting '"+param.name+"' ("+param.id+") because value is null or empty");
						}
												
						break;
						
		    		default:
		    			logr.warning("Unrecognised parameter type '"+param.type+" for param '"+param.name+"' (id="+param.id+")");
				}
			}catch(PICamSDKException err){
				throw new RuntimeException("Caught exception setting parameter '"+param.name+".", err);
			}
		}
		
	}
	
	private void readConfig(){
		//get all params and update/add them to our params list
		int paramIDs[] = camera.getParameters();
				
		//read back
		for(int i=0; i < paramIDs.length; i++){
			String name = PICamParameters.getName(paramIDs[i]);
			if(name == null)
				name = "Unknown_" + paramIDs[i];
			
			Parameter param = cfg.getFeature(name);
			if(param == null){
				param = new Parameter(paramIDs[i], name, 0);
				cfg.addParameter(param);
				
			}
			param.isImplemented = true;
			param.isWritable = PICamParameters.getConstraintType(paramIDs[i]) != PICamDefs.constraintType_None;
			param.isFilled = true;
			
			switch(param.type){
				case PICamDefs.valueType_FloatingPoint:
					float valF = camera.getParameterFloatingPointValue(param.id);
					param.value = valF;
					logr.fine("Get '"+param.name+"' ("+param.id+") = " + valF + " (float)");
					break;
					
				case PICamDefs.valueType_Integer:
					int valI = camera.getParameterIntegerValue(param.id);
					param.value = valI;
					logr.fine("Get '"+param.name+"' ("+param.id+") = " + valI + " (int)");
					break;
					
				case PICamDefs.valueType_LargeInteger:
					long valL = camera.getParameterLargeIntegerValue(param.id);
					param.value = valL;
					logr.fine("Get '"+param.name+"' ("+param.id+") = " + valL + " (long)");
					break;
					
				case PICamDefs.valueType_Enumeration:
		    		int valE = camera.getParameterIntegerValue(param.id);
					param.value = valE;
							    		
					if(true || param.enumStrings == null){
						int enumType = camera.GetParameterEnumeratedType(param.id);
						String enumStrings[] = new String[PICamDefs.MAX_ENUM_STRINGS];
						boolean enumIsImplemented[] = new boolean[PICamDefs.MAX_ENUM_STRINGS];
						//scan through enums. Assume first failure is after highest
						//sometime 0 doesn't exist
						int n;
						for(n=0; n < 100; n++){
			    			try{
			    				enumStrings[n] = camera.GetEnumerationString(enumType, n);
			    				enumIsImplemented[n] = true;
			    				
			    			}catch(PICamSDKException err){
			    				if(err.getErrorCode() != PICamSDKException.ENUM_NOT_DEFINED)
			    					throw err;
			    				if(n == 0){
			    					enumStrings[n] = null;
			    					enumIsImplemented[n] = false;
			    				}
			    				if(n > 0)
			    					break;
			    			}
			    		}
						
						param.enumStrings = Arrays.copyOf(enumStrings, n);						
						param.enumIsImplemented = Arrays.copyOf(enumIsImplemented, n);						
						
					}
		    		int enumType = camera.GetParameterEnumeratedType(param.id);
		    		
		    		String str = camera.GetEnumerationString(enumType, valE);
		    		logr.fine("Get '"+param.name+"' ("+param.id+") = " + valE + " (Enum, type="+enumType+", str=" + str + ")");
		    		break;
		    		
				case PICamDefs.valueType_Boolean:
					int valB = camera.getParameterIntegerValue(param.id);
					param.value = new Boolean(valB != 0);
					logr.fine("Get '"+param.name+"' ("+param.id+") = " + param.value + " (bool)");
					break;
					
				case PICamDefs.valueType_Rois:
					if(param.value == null || ((PICamROIs)param.value).getROIListCopy().size() == 0){
						param.value = camera.getParameterRoisValue(param.id);
						cfg.enableROIs = true;
						logr.fine("Get '"+param.name+"' ("+param.id+") = " + param.value + " (ROIs)");
					}else{
						logr.fine("Get '"+param.name+"' ("+param.id+"): Not overwriting config ROIs");
					}
					break;
					
	    		default:
	    			logr.warning("Unrecognised parameter type '"+param.type+" for param '"+param.name+"' (id="+param.id+")");
			}
			

    		switch(PICamParameters.getConstraintType(param.id)){
    		
    		case PICamDefs.constraintType_Range:
    			PICamRangeConstraint range = camera.getParameterRangeConstraint(param.id, PICamDefs.constraintCategory_Required);
    			switch(param.type){
					case PICamDefs.valueType_FloatingPoint: 
		    			param.minValue = range.minimum;
		    			param.maxValue = range.maximum;
						break;					
					case PICamDefs.valueType_Integer:
		    			param.minValue = (int)range.minimum;
		    			param.maxValue = (int)range.maximum;
		    			break;
					case PICamDefs.valueType_LargeInteger:
		    			param.minValue = (long)range.minimum;
		    			param.maxValue = (long)range.maximum;
		    			break;
    			}
				break;
    		case PICamDefs.constraintType_Collection:
    			PICamCollectionConstraint collection = camera.getParameterCollectionConstraint(param.id, PICamDefs.constraintCategory_Required);
    			logr.fine("CollectionConstraint: " + collection);
    			
    			if(param.type != PICamDefs.valueType_Enumeration){
	    			if(collection.values != null && collection.values.length > 0){
	    				param.enumStrings = new String[collection.values.length];
	    				for(int j=0; j < collection.values.length; j++){
	    					param.enumStrings[j] = Double.toString(collection.values[j]);
	    				}
	    			}
    			}
    			
    			break;
    			
    		}
    		
			
			
			
		}	
	}
		
	private void initImages(){

		long readoutStride= cfg.intFeature("ReadoutStride"); //2099208
		long frameStride = cfg.intFeature("FrameStride");    //2097168
		long frameSize = cfg.intFeature("FrameSize");        //2097152
		
		if(readoutStride != frameStride)
			System.err.println("WARNING: FrameStride("+frameStride+")  != ReadoutStride ("+readoutStride+") not implemented");
			//throw new IllegalArgumentException("FrameStride != ReadoutStride not implemented");
		
		long footerSize = readoutStride - frameSize;
		
		int width = cfg.imageWidth();
		int height = cfg.imageHeight();
		int bitDepth = cfg.intFeature("PixelBitDepth");
		double bytesPerPixel = bitDepth / 8;	
		
		logr.info("Frame size = " + (frameSize/bytesPerPixel) + " pixels");
			
		Parameter readoutModeFeature = cfg.getFeature("ReadoutControlMode");
		
		if(readoutModeFeature != null && ((int)readoutModeFeature.value) == PICamEnumerations.readoutControlMode_SpectraKinetics){
			//In this mode the frameSize is set to a single 'frame' which is basically just one row
			//Each 'Readout' which we read as a frame here is the fixed full height of the chip (528 for the 512x512 camera)
			
			if(!cfg.enableROIs)
				throw new IllegalArgumentException("Must have ROIs enabled for spectra kinetics mode");
			
			ArrayList<PICamROI> rois = cfg.getROIs().getROIListCopy();
			boolean foundOne = false;
			for(PICamROI roi : rois){
				if(roi.enabled){
					if(foundOne)
						throw new RuntimeException("Must have exactly one ROI enabled for spectra kinetics mode");
					
					
					width = roi.width / roi.x_binning;
					
					int windowHeight = roi.height;
					
					Parameter kineticsWindowHeightFeature = cfg.getFeature("KineticsWindowHeight");
					if(kineticsWindowHeightFeature == null || ((int)kineticsWindowHeightFeature.value) != windowHeight)
						throw new RuntimeException("Single ROI has hieght '"+windowHeight+"' but KineticsWindowHeight is "+kineticsWindowHeightFeature.valueAsString()+".");
					
					height = 529; //Fixed height hacked for 512x512 camera for now
					
					foundOne = true;
				}
			}
			
			
		}else if(cfg.enableROIs){
			int smallestWidth = width;
			for(PICamROI roi : cfg.getROIs().getROIListCopy()){
				if(!roi.enabled)
					continue;
				int rowWidth = roi.width / roi.x_binning;
				if(rowWidth < smallestWidth){
					smallestWidth = rowWidth;
				}
			}
			logr.info("Smallest ROI width = " + smallestWidth);
			width = smallestWidth;
			height = (int)(frameSize / width / bytesPerPixel);
			if(frameSize != width * height * bytesPerPixel){
				throw new RuntimeException("Can't make a rectangular image out of image data size and smallest ROI size.");
			}
		
		}else if(frameSize < width*height*bytesPerPixel){
			throw new RuntimeException("Camera wants less memory than it needs for the image???");
		}
			
		//int footerSize = (int)(imgDataSize - (width * height * bytesPerPixel));
		
		if(frameSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		
		try{
					
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, (int)footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
		
		ByteBuffer[] allocBuffers = bulkAlloc.getAllocationBuffers();
		if(allocBuffers.length > 1)
			throw new RuntimeException("Multiple buffers not yet supported (>2GB image memory)");
		
		camera.SetAcquisitionBuffer(allocBuffers[0]);
	
	}
	
	private class UpdateTracker implements PICamAcquisitionUpdatedCallback {
		public int imgSizeBytes = 0;
		
		/** Next image to be acquired */
		public int head = 0;
		
		/** Next image to be processed */
		public int tail = 0;
		
		public int nImages = 0;
		
		public boolean isContinuous;
		
		public boolean hasStopped = false;
		
		public RuntimeException error = null;
		
		public AcquisitionStatus camStatus = new AcquisitionStatus();
		@Override
		public void acquisitionUpdatedCallback(int bufferPos, int readout_count, boolean running, int errorsMask, int readout_rate) {
			
		
			
			synchronized (this) {
				int imgIdx = bufferPos / imgSizeBytes;
				
				spinLog("head = " + head
						+ ", tail = " + tail
						+ ", bufferPos = " + bufferPos
						+ ", readout_count = " + readout_count
						+ ", running = " + running
						+ ", errors = " + errorsMask
						+ ", rate = " + readout_rate
						+ ", imgIdx = " + imgIdx);
				
				if(bufferPos < 0 || readout_rate < 0){					
					spinLog("Acquisition appears to have stopped");
					hasStopped = true;
					return;
				}
				
				if(imgIdx != head){
					error = new RuntimeException("acquisitionUpdatedCallback() called with imgIdx="+imgIdx+" but head is at" + head + ". Aborting capture.");
					return;
				}
							
				
				long wallTimeMS = System.currentTimeMillis();
				if(wallCopyTimeMS0 == Long.MIN_VALUE)
					wallCopyTimeMS0 = wallTimeMS;
				
				for(int i=0; i < readout_count; i++){
					wallCopyTimeMS[head] = wallTimeMS;
					
					head++; //move head up
					
					//wrap queue, but not for the case of a single image
					//where we can't really have a ring buffer.
					//in that case we just leave it at the end, and refuse flatly
					//to deal with continuous mode
					if(head >= nImages && isContinuous) 						
						head = 0;
					
					if(head == tail){ //about to overrun
						error = new RuntimeException("acquisitionUpdatedCallback() overran processing. Aborting.");
						return;
					}
				}
				
				camStatus.running = running;
				camStatus.errorsMask = errorsMask;
				camStatus.readout_rate = readout_rate;
				
				if(!running || errorsMask != 0){
					error = new RuntimeException("acquisitionUpdatedCallback() detected bad state. running="+running+", errorsMask="+errorsMask);
					return;
				}
			}
		}
	}
	
	private void doCapture() {
		
		if(camera.isAcquisitionRunning()){
			throw new RuntimeException("Acquisition already running!");
		}
		
		setStatus(Status.awaitingSoftwareTrigger, "Capturing, acquisition not started.");	
		
		try{

			for(int i=0; i < images.length; i++)
				images[i].invalidate(false);
			
			time = new double[cfg.nImagesToAllocate]; //seconds since first image
			wallCopyTimeMS = new long[cfg.nImagesToAllocate];
			nanoTime = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("PICam/wallCopyTimeMS", wallCopyTimeMS, true);
			//source.setSeriesMetaData("PICam/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("PICam/nanoTime", nanoTime, true);
			source.setSeriesMetaData("PICam/time", time, true);
			
			int timestampsMask = cfg.intFeature("TimeStamps");
			timestampStart = ((timestampsMask | PICamDefs.timeStampsMask_ExposureStarted) != 0) ? new long[cfg.nImagesToAllocate] : null;
			timestampEnd = ((timestampsMask | PICamDefs.timeStampsMask_ExposureEnded) != 0) ? new long[cfg.nImagesToAllocate] : null;
			source.setSeriesMetaData("PICam/timestampStart", timestampStart, true);
			source.setSeriesMetaData("PICam/timestampEnd", timestampEnd, true);
			
			
			wallCopyTimeMS0 = Long.MIN_VALUE;
			timestampStart0 = Long.MIN_VALUE;
			
			UpdateTracker tracker = new UpdateTracker();
			tracker.imgSizeBytes = cfg.imageSizeBytes();
			tracker.isContinuous = cfg.isContinuous();
			tracker.nImages = cfg.nImagesToAllocate;
			tracker.hasStopped = false;
			camera.registerForAcquisitionUpdated(tracker);
			
			//wait for acquisition start signal
			while(!signalAcquireStart){
				try{ Thread.sleep(0, 100000); }catch(InterruptedException err){ }
				
				if(signalDeath || signalAbort){
					logr.info("Aborting capture loop.");
					throw new CaptureAbortedException();
				}
				if(tracker.error != null)
					throw tracker.error;
			}
			
			camera.startAcquisition();
			
			setStatus(Status.awaitingHardwareTrigger, "Capturing..., (awaiting hardware trigger)");
			
			
			int toProcess = -1; //image to be processed
acqLoop:	do{ //loop until all images captured, or forever if continuous
				
				do{ //for each image in queue
					if(signalDeath || signalAbort){
						logr.info("Aborting capture loop.");
						throw new CaptureAbortedException();
					}
					if(tracker.error != null)
						throw tracker.error;
					
					synchronized (tracker) {					
						spinLog("Tracker head = " + tracker.head + ", tail = " + tracker.tail);
						if(tracker.tail != tracker.head){
							toProcess = tracker.tail;
							tracker.tail++;
							if(tracker.tail >= tracker.nImages) //wrap queue
								tracker.tail = 0;
						}else{
							toProcess = -1;
							break;
						}
							
					}
					
					if(status != Status.capturing)
						setStatus(Status.capturing, "Capturing..., acquisition started");
					
					spinLog("Got image to process: " + toProcess);
					
					gotImage(toProcess);
					
					if(status == Status.awaitingHardwareTrigger)
						setStatus(Status.capturing, "Acquiring.");		
					
					
					if(!cfg.isContinuous() && toProcess == (tracker.nImages-1))
						break acqLoop; //all done
					
				}while(toProcess > 0);

				if(tracker.hasStopped)
					throw new RuntimeException("Acquisition stopped prematurely.");
					
				//nothing to process, wait a bit
				try{ Thread.sleep(50); }catch(InterruptedException err){ }
								
			}while(true);
						
			spinLog("Acquisition complete.");
			
		}finally{
			camera.stopAcquisition();		
			camera.unregisterForAcquisitionUpdated();	
		}

	}
	
	private void gotImage(int idx){
		//catch up with the acqusition, this should really only ever be 1 image
	
		nanoTime[idx] = Long.MIN_VALUE;
		
		int footerSize = images[idx].getFooterSize(); 
		if(footerSize > 0){			
			//copy footer to metadata
			ByteBuffer buff = images[idx].getReadOnlyBuffer();
			
			buff.position(buff.capacity() - footerSize);
			byte footer[] = new byte[footerSize];
			buff.get(footer);
			source.setImageMetaData("PICam/footerData", idx, footer);
		
			
			//extract timestamps
			buff.order(ByteOrder.LITTLE_ENDIAN);
			buff.position(buff.capacity() - footerSize);
			int timestampsMask = cfg.intFeature("TimeStamps");
			int timestampBitDepth = cfg.intFeature("TimeStampBitDepth");
			long timestampResolution = cfg.longFeature("TimeStampResolution");
			
			for(int i=1; i < 3; i++){ //for [start, end]timestamp
				long timestamp = -1;
				if((timestampsMask & i) != 0){
					switch(timestampBitDepth){
						case 8: timestamp = buff.get(); break;
						case 16: timestamp = buff.getShort(); break;
						case 32: timestamp = buff.getInt(); break;
						case 64: timestamp = buff.getLong(); break;
					}
				
					timestamp *= (1_000_000_000L / timestampResolution); //convert to ns if it wasn't
					if(i==1)
						timestampStart[idx] = timestamp;
					else 
						timestampEnd[idx] = timestamp;
				}
				
			}
			
						
			if((timestampsMask & (PICamDefs.timeStampsMask_ExposureStarted | PICamDefs.timeStampsMask_ExposureEnded)) != 0){
				//with both timestamps, we can make a fairly good nanoTime
				if(timestampStart0 == Long.MIN_VALUE)
					timestampStart0 = timestampStart[idx];
				
				nanoTime[idx] = (timestampStart[idx] + timestampEnd[idx])/2;
				nanoTime[idx] -= timestampStart0; //time since start of first frame
				
				time[idx] = nanoTime[idx] / 1e9;
						
				nanoTime[idx] += wallCopyTimeMS0 * 1_000_000L; //full nanoTime
				
			}
		}
		
		if(nanoTime[idx] == Long.MIN_VALUE){
			//no timestamp, so our wall copy time is the best we can do							
			time[idx] = (wallCopyTimeMS[idx] - wallCopyTimeMS0) / 1e3;
			 //half an exposure back from copy time, ignore readout time
			nanoTime[idx] = wallCopyTimeMS[idx] * 1_000_000L - (long)(cfg.exposureTimeMS() * 0.5 * 1e6);			
		}
		
		images[idx].invalidate(false);
		source.imageCaptured(idx);
				
		
	}
	
	/*
		do{
			int nImagesAcquired = 0;						
			try {
				//lock only the next image to be captured.
				//The acquisition might run ahead of that, but err.. tough
				WriteLock writeLock = images[tracker.head].writeLock();
				writeLock.lockInterruptibly();
				try{
					images[tracker.head].invalidate(false);
					
					if(!isAcqusitionStarted()){ //don't actually start until acquire signal is set
						while(!signalAcquireStart){
							Thread.sleep(0, 100000);
						}
						inStartAcquisition = System.currentTimeMillis();							
						camera.startAcquisition();						
						inStartAcquisition = -1;
						setStatus(Status.awaitingHardwareTrigger, "Capturing, Acquisition started");
					}
					
					do{
						
						synchronized (tracker) {
							
							spinLog("Tracker head = " + tracker.head + ", tail = " + tracker.tail);
						}
						if(nImagesAcquired > 0)
							break;
						
						try{ Thread.sleep(10); }catch(InterruptedException err){ }
						
						
						
						/*AcquisitionStatus camStatus = new AcquisitionStatus();
						try{
							
							nImagesAcquired = camera.waitForAcquisitionUpdate(cfg.grabberTimeoutMS, camStatus);
							spinLog("WaitForAcquisitionUpdate returned: "
									+ "nImagesAcquired = " + nImagesAcquired 
									+ ", nextImgIdx=" + nextImgIdx
									+ ", running = " + camStatus.running 
									+", err = " + camStatus.errorsMask
									+", rate = " + camStatus.readout_rate);
							
							if(camStatus.running == false || camStatus.errorsMask != 0)
								throw new PICamSDKException("WaitForAcquisitionUpdate not running or errored.");
							
							wallCopyTimeMS[nextImgIdx] = System.currentTimeMillis();
						}catch(PICamSDKException err){								
							if(err.getErrorCode() == PICamSDKException.TIMEOUT){
								spinLog("WaitForAcquisitionUpdate timed out");
								continue;									
							}
							spinLog("WaitForAcquisitionUpdate threw error: " + err);
							throw err;
						}finally{
							
							if(signalDeath || signalAbort){
								logr.info("Aborting capture loop.");
								throw new CaptureAbortedException();
							}
						}
						
						break;
					}while(true); //timeout loop
					
					//spinLog(".doCapture(): Buffer copied to images[" + nextImgIdx + "]");
					
					
				}finally{ writeLock.unlock(); }
			} catch (InterruptedException e) {
				logr.info("Interrupted while waiting to write to java image.");
				return;
			}
			
							
		}while(nextImgIdx < cfg.nImagesToAllocate);
	}
	*/
	
	private void deinit(){
		logr.info(" deInit()");
		
		if(camera != null)
			camera.close();
		camera = null;
		cameraOpen = false;
	}
			
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		thread.setPriority(Thread.MAX_PRIORITY);
		logr.info("Starting PICam capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(PicamConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(PicamConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thrad to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
				
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				logr.info("PICamCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.info("PICamCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				logr.info("PICamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				logr.info("PICamCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}

	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(PicamConfig config) { this.cfg = config; }

}
