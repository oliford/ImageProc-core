package imageProc.sources.capture.picam.swt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import imageProc.core.swt.EditableTable;
import imageProc.core.swt.EditableTable.EditType;
import imageProc.sources.capture.picam.PicamConfig;
import imageProc.sources.capture.picam.PicamSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import picamJNI.PICamROIs;
import picamJNI.PICamROIs.PICamROI;

public class ROIConfigPanel {
	
	private final static String colNames[] = new String[]{ "-", "Name", "Enable", "X0", "Width", "X bin", "Y0", "Height", "Y bin" };			
	
	private EditableTable table;
	
	private PicamSource source;
	private PicamSWTControl controller;
	private Composite swtGroup;
	
	private Button roiEnableROIs;
	private Button drawBoxesCheckbox;
	private Button setBoxesCheckbox;
	private Button orderByNameCheckbox;

	private Button jogLeftButton;
	private Button jogRightButton;
	private Button jogUpButton;
	private Button jogDownButton;
		
	private Button copyAllButton;	
	private Button pasteAllButton;
	
	
	public ROIConfigPanel(Composite parent, int style, PicamSource source, PicamSWTControl parentController) {
		this.source = source;
		this.controller = parentController;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(5, false));
		
		roiEnableROIs = new Button(swtGroup, SWT.CHECK);
		roiEnableROIs.setText("Enable ROIs - Disable and capture to visualise/edit ROIs on image.");
		roiEnableROIs.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		roiEnableROIs.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { roiConfigEvent(event); } });
		
		drawBoxesCheckbox = new Button(swtGroup, SWT.CHECK);
		drawBoxesCheckbox.setText("Draw boxes");
		drawBoxesCheckbox.setToolTipText("Draws ROIs on full-frame image. All in white, selected in magenta.");
		drawBoxesCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		setBoxesCheckbox = new Button(swtGroup, SWT.CHECK);
		setBoxesCheckbox.setText("Set boxes");
		setBoxesCheckbox.setToolTipText("Allows drag-drawing of boxes on image for selected entry");
		setBoxesCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		orderByNameCheckbox = new Button(swtGroup, SWT.CHECK);
		orderByNameCheckbox.setText("Order by name");
		orderByNameCheckbox.setToolTipText("Reorders entries according to name - also reorders in image.");
		orderByNameCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Table:");
		copyAllButton = new Button(swtGroup, SWT.PUSH);
		copyAllButton.setText("Copy");
		copyAllButton.setToolTipText("Copy all entries in table to clipboard in a format that is recognised by LibreOffice/Excel");
		copyAllButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		copyAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { copyAllButtonEvent(event); } });

		pasteAllButton = new Button(swtGroup, SWT.PUSH);
		pasteAllButton.setText("Paste");
		pasteAllButton.setToolTipText("Paste all entries from clipboard into the table, e.g. after modification in LibreOffice/Excel");
		pasteAllButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		pasteAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pasteAllButtonEvent(event); } });

		Group swtJogGroup = new Group(swtGroup, SWT.BORDER);
		swtJogGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		swtJogGroup.setLayout(new GridLayout(3, false));
		
		Label lJA = new Label(swtJogGroup, SWT.NONE);
		lJA.setText("Jog:");
		lJA.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		jogUpButton = new Button(swtJogGroup, SWT.PUSH);
		jogUpButton.setText("^");
		jogUpButton.setToolTipText("Jog all or selected ROIs up 1 pixel");
		jogUpButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogUpButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, 0, -1); } });
		
		Label lJB = new Label(swtJogGroup, SWT.NONE);
		lJB.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		jogLeftButton = new Button(swtJogGroup, SWT.PUSH);
		jogLeftButton.setText("<");
		jogLeftButton.setToolTipText("Jog all or selected ROIs left 1 pixel");
		jogLeftButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogLeftButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, -1, 0); } });

		jogDownButton = new Button(swtJogGroup, SWT.PUSH);
		jogDownButton.setText("v");
		jogDownButton.setToolTipText("Jog all or selected ROIs down 1 pixel");
		jogDownButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogDownButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, 0, +1); } });

		jogRightButton = new Button(swtJogGroup, SWT.PUSH);
		jogRightButton.setText(">");
		jogRightButton.setToolTipText("Jog all or selected ROIs right 1 pixel");
		jogRightButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogRightButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, +1, 0); } });

		table = new EditableTable(swtGroup, SWT.BORDER | SWT.MULTI);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);	
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(table, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]);
		}
		table.setEditTypeAll(EditType.EditBox);
		table.setEditType(2, EditType.Click);
		
		configToGUI();
		
		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		table.setTableModifyListener(new EditableTable.TableModifyListener() {		
				@Override public void tableModified(TableItem item, int column, String text) { tableColumnModified(item, column, text); }
			});
		table.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ImageProcUtil.redrawImagePanel(source); } });
		
		swtGroup.pack();
	}

	protected void jogButtonEvent(Event event, int dx, int dy) {
		
		PicamConfig config = source.getConfig();
		TableItem[] selectedItems = table.getSelection();
		if(selectedItems != null && selectedItems.length >=1) {
			for (TableItem item : selectedItems) {
				
				PICamROI roi = config.getROIs().getROI(item.getText(1));
				if(roi != null)
					config.jogROI(roi, dx, dy);
			}
		}else {
			for(PICamROI roi : config.getROIs().getROIListCopy()) {
				config.jogROI(roi, dx, dy);				
			}
		}
		configToGUI();
	}

	private void tableColumnModified(TableItem item, int column, String text) {

		PicamConfig config = source.getConfig();
		PICamROIs rois = config.getROIs();
		
		if(item == null || column < 0) {
			controller.generalControllerUpdate();
			controller.forceImageRedraw();
			return;
		}		
		
		synchronized (rois) {
			if (item.isDisposed())
				return;

			String roiName = item.getText(1);

			PICamROI roi = (roiName.length() > 0) ? rois.getROI(roiName) : null;
			if(roi == null && column != 1){
				//somethings broken. Queue an update
				controller.generalControllerUpdate();
				return;
			}
			
			switch (column) {
				case 1: //name
	
					if (roi == null && (roiName.length() <= 0 || roiName.equals("<new>"))) { 
						roi = new PICamROI();
						roi.name = text;
						rois.addROI(roi);
	
					} else if (text.length() <= 0) { // deleting point
						rois.remove(roi);
	
					} else { // just changing name
						roi.name = text;
					}
	
					item.setText(column, text);
	
					break;
					
				case 2: roi.enabled = !Algorithms.mustParseBoolean(item.getText(2));	item.setText(2, roi.enabled ? "Y" : ""); break;
				case 3: roi.x = Algorithms.mustParseInt(text); item.setText(3, Integer.toString(roi.x)); break;
				case 4: roi.width = Algorithms.mustParseInt(text); item.setText(4, Integer.toString(roi.width)); break;
				case 5: roi.x_binning = Algorithms.mustParseInt(text); item.setText(5, Integer.toString(roi.x_binning)); break;
				
				case 6: roi.y = Algorithms.mustParseInt(text); item.setText(6, Integer.toString(roi.y)); break;
				case 7: roi.height = Algorithms.mustParseInt(text); item.setText(7, Integer.toString(roi.height)); break;
				case 8: roi.y_binning = Algorithms.mustParseInt(text); item.setText(8, Integer.toString(roi.y_binning)); break;
				
			}

			TableItem lastItem = table.getItem(table.getItemCount() - 1);
			if (!lastItem.getText(1).equals("<new>")) {
				TableItem blankItem = new TableItem(table, SWT.NONE);
				blankItem.setText(0, "o");
				blankItem.setText(1, "<new>");
			}

		}
	}

	
	
	void configToGUI(){
		PicamConfig config = source.getConfig();
		roiEnableROIs.setSelection(config.enableROIs);
				
		PICamROIs rois = config.getROIs();
		
				
		ArrayList<PICamROI> roiList;
		synchronized (rois) {
	
			if(orderByNameCheckbox.getSelection()){
				rois.sortROIsList();
			}
			roiList = rois.getROIListCopy();
		}


		int selected[] = table.getSelectionIndices();

		// use existing table entries, so it doesnt scroll around
		for (int i = 0; i < roiList.size(); i++) {
			PICamROI roi = roiList.get(i);

			if (roi.name.length() <= 0)
				continue;

			TableItem item;
			if (i < table.getItemCount())
				item = table.getItem(i);
			else
				item = new TableItem(table, SWT.NONE);

			item.setText(0, "o");
			item.setText(1, roi.name);
			item.setText(2, roi.enabled ? "Y" : "");

			item.setText(3, Integer.toString(roi.x));
			item.setText(4, Integer.toString(roi.width));
			item.setText(5, Integer.toString(roi.x_binning));

			item.setText(6, Integer.toString(roi.y));
			item.setText(7, Integer.toString(roi.height));
			item.setText(8, Integer.toString(roi.y_binning));

		}

		while (table.getItemCount() > roiList.size()) {
			table.remove(roiList.size());
		}

		// and finally a blank item for creating new point
		TableItem blankItem = new TableItem(table, SWT.NONE);
		blankItem.setText(0, "o");
		blankItem.setText(1, "<new>");


	}
		
	private void roiConfigEvent(Event event) {
		PicamConfig config = source.getConfig();
		config.enableROIs = roiEnableROIs.getSelection();		
	}
	
	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }

	public void drawOnImage(GC gc, double[] scale, int imageWidth, int imageHeight, boolean asSource) {
		if (swtGroup.isDisposed() || !drawBoxesCheckbox.getSelection())
			return;

		PicamConfig config = source.getConfig();
				
		//must be full image to draw/select ROIs
		if(imageWidth != config.intFeature("SensorActiveWidth")
				|| imageHeight != config.intFeature("SensorActiveHeight")){	
			return;
		}
		
		PICamROIs rois = config.getROIs();
		List<PICamROI> roiList = rois.getROIListCopy();
		
		int i = 0;
		for (PICamROI roi : roiList) {

			TableItem[] selectedItems = table.getSelection();
			boolean isSelected = false;
			for (TableItem item : selectedItems) {
				if (item.getText(1).equals(roi.name))
					isSelected = true;
			}
			if (isSelected) {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_MAGENTA));
				gc.setLineWidth(3);
			} else {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
				gc.setLineWidth(1);
			}

			int boxCoords[] = new int[]{ roi.x, roi.y, roi.x + roi.width, roi.y + roi.height };

			if (boxCoords[0] >= 0 && boxCoords[1] >= 0 && boxCoords[2] <= imageWidth && boxCoords[3] <= imageHeight
					&& boxCoords[2] > boxCoords[0] && boxCoords[3] > boxCoords[1]) {

				gc.drawRectangle((int) (boxCoords[0] * scale[0]), (int) (boxCoords[1] * scale[1]),
						(int) ((boxCoords[2] - boxCoords[0]) * scale[0]),
						(int) ((boxCoords[3] - boxCoords[1]) * scale[1]));
			}

			i++;
		}

	}

	public void setRect(int x0, int y0, int width, int height) {		
		if (swtGroup.isDisposed() || !setBoxesCheckbox.getSelection())
			return;

		PicamConfig config = source.getConfig();
		
		//must be full image to draw/select ROIs
		int imageWidth = source.getImage(0).getWidth();
		int imageHeight = source.getImage(0).getHeight(); 
		if(imageWidth != config.intFeature("SensorActiveWidth")
				|| imageHeight != config.intFeature("SensorActiveHeight")){	
			return;
		}
		
		
		TableItem[] selected = table.getSelection();
		if (selected.length == 1) {
			String roiName = selected[0].getText(1);

			PICamROI roi = config.getROIs().getROI(roiName);
			
			if(roi.x_binning <= 0) roi.x_binning = 1;
			if(roi.y_binning <= 0) roi.y_binning = 1;

			roi.x = x0;
			roi.width = (int)(width / roi.x_binning) * roi.x_binning;
			
			roi.y = y0;
			roi.height = (int)(height / roi.y_binning) * roi.y_binning;
			
			source.configChanged();
		}
	}

	private void copyAllButtonEvent(Event event){
		
		StringBuilder strB = new StringBuilder();
		
		PicamConfig config = source.getConfig();
		
		//Set<String> keySet = config.specEntries.get(0).toMap().keySet();
		String[] keySet = new String[]{  
				"name", "enabled", "x", "width", "x_binning", "y", "height", "y_binning", 
			};
		
		for(String fieldName : keySet){
			strB.append("\"" + fieldName + "\"\t");
		}
		strB.append("\n");
		
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.create(); 
		
		for(PICamROI roi : config.getROIs().getROIListCopy()){
			JsonElement jsonElem = gson.toJsonTree(roi);
			JsonObject o = jsonElem.getAsJsonObject();
			
			for(String fieldName : keySet){
				Object o2 = o.get(fieldName);
				strB.append((o2 == null ? "null" : o2.toString()) + "\t");
			}
			strB.append("\n");
		}
		
		System.out.println(strB.toString());
		
		Clipboard cb = new Clipboard(swtGroup.getDisplay());
		TextTransfer tt = TextTransfer.getInstance();
		cb.setContents(new Object[]{ strB.toString() }, new Transfer[]{ tt });
		cb.dispose();
	}


	private void pasteAllButtonEvent(Event event){

		Clipboard cb = new Clipboard(swtGroup.getDisplay());

		try{
			String plainText = (String)cb.getContents(TextTransfer.getInstance());
			System.out.println("Pasted: " + plainText);
			String lines[] = plainText.split("\n");
			String fieldNames[] = lines[0].split("\t");
	
			Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.create(); 
		
			PicamConfig config = source.getConfig();
			config.getROIs().clear();
			
			for(int i=1; i < lines.length; i++){
				
				String jsonStr = "{";
				String fields[] = lines[i].split("\t");
				if(fields.length != fieldNames.length)
					throw new RuntimeException("Row " +i+" of pasted text had " + fields.length +" columns but row 0 (names) had " + fieldNames.length);
				
				for(int j=0; j < fields.length; j++){
					if(j > 0)
						jsonStr += ","; 
					jsonStr += "\"" + fieldNames[j] + "\":";
							
					if(fields[j].equals("null")){
						jsonStr += "null";
					}else if(fields[j].startsWith("[") && fields[j].endsWith("]")){ 
						//looks like a JSON array (e.g. ignoreX0Ranges
						jsonStr += fields[j];
					}else{
						jsonStr += "\"" + fields[j] + "\"";
					}
				}
				jsonStr += "}";
				System.out.println(jsonStr);
	
				PICamROI thing = (PICamROI)gson.fromJson(jsonStr, PICamROI.class);
	
				config.getROIs().addROI(thing);
				
			}
			
			cb.dispose();
			
			System.out.println("Pasted "+config.getROIs().getROIListCopy().size()+" entries.");
			
		}catch(RuntimeException err){
			err.printStackTrace();
			System.err.println("Error parsing pasted text:" + err.getMessage());
		}
		
		
		controller.generalControllerUpdate();
	}
		
}
