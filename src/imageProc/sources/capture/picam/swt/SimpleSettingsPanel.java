package imageProc.sources.capture.picam.swt;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImgSource;
import imageProc.sources.capture.picam.PicamConfig;
import imageProc.sources.capture.picam.PicamSource;
import imageProc.sources.capture.picam.PicamConfig.Parameter;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import picamJNI.PICamDefs;
import picamJNI.PICamParameters;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private PicamSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo triggerModeCombo;	
	private Combo shutterModeCombo;
	private Combo adcQualityCombo;
	private Combo adcAnalogGainCombo;
	private Spinner adcEMGainSpinner;
	private Combo adcSpeedCombo;
	
	private Spinner coolingSetTempSpinner;
	private Label coolingCurrentTempLabel;

	private Combo readoutModeCombo;
	
	private Text exposureLengthTextbox;
	private Button exposureMinButton; 
	private Button exposureMaxButton; 

	private Button unsetAllButton; 
	private Button defaultAllButton; 
	
	public SimpleSettingsPanel(Composite parent, int style, PicamSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		Label lST = new Label(swtGroup, SWT.NONE); lST.setText("Cooling: Set:");
		coolingSetTempSpinner = new Spinner(swtGroup, SWT.CHECK);
		coolingSetTempSpinner.setValues(25, -100, 100, 0, 1, 10);
		coolingSetTempSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		coolingSetTempSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		coolingCurrentTempLabel = new Label(swtGroup, SWT.NONE);
		coolingCurrentTempLabel.setText("Current: ????????");
		coolingCurrentTempLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
		
		Label lSHT = new Label(swtGroup, SWT.NONE); lSHT.setText("Shutter:");
		shutterModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		shutterModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		shutterModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lHWT = new Label(swtGroup, SWT.NONE); lHWT.setText("H/W Trigger:");
		triggerModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		triggerModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		triggerModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lAQ = new Label(swtGroup, SWT.NONE); lAQ.setText("ADC Quality:");
		adcQualityCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		adcQualityCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		adcQualityCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lAA = new Label(swtGroup, SWT.NONE); lAA.setText("Analog Gain:");
		adcAnalogGainCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		adcAnalogGainCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		adcAnalogGainCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
				
		Label lAG = new Label(swtGroup, SWT.NONE); lAG.setText("EM Gain:");
		adcEMGainSpinner = new Spinner(swtGroup, SWT.DROP_DOWN);
		adcEMGainSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		adcEMGainSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lAS = new Label(swtGroup, SWT.NONE); lAS.setText("ADC Speed:");
		adcSpeedCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		adcSpeedCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		adcSpeedCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		

		Label lRM = new Label(swtGroup, SWT.NONE); lRM.setText("Readout Mode:");
		readoutModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		readoutModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		readoutModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure Time / ms:");
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("10");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		exposureMinButton = new Button(swtGroup, SWT.PUSH);
		exposureMinButton.setText("Min");
		exposureMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		exposureMinButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(false); } });
		
		exposureMaxButton = new Button(swtGroup, SWT.PUSH);
		exposureMaxButton.setText("Max");
		exposureMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		exposureMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(true); } });
		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
		lB.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("All parameters:");
		unsetAllButton = new Button(swtGroup, SWT.PUSH);
		unsetAllButton.setText("Unset/Read");
		unsetAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		unsetAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(false); } });
		
		defaultAllButton = new Button(swtGroup, SWT.PUSH);
		defaultAllButton.setText("Set default");
		defaultAllButton.setEnabled(false);
		defaultAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		defaultAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(true); } });
		
		configToGUI();
	}

	void configToGUI(){
		PicamConfig config = source.getConfig();
		
		
		continuousButton.setSelection(config.isContinuous());
		Parameter setPointParam = config.getFeature("SensorTemperatureSetPoint");
		
		if(setPointParam == null || setPointParam.value == null ){
			coolingSetTempSpinner.setSelection(0);
		}else{
			coolingSetTempSpinner.setSelection(((Number)setPointParam.value).intValue());
		}
		double temp = config.doubleFeature("SensorTemperatureReading");
		
		coolingCurrentTempLabel.setText("Current: " + String.format("%5.3f", temp));
		
		Parameter gainFeature = config.getFeature("AdcEMGain");
		if(gainFeature != null){
			adcEMGainSpinner.setValues((int)((Number)gainFeature.value).doubleValue() * 100, 
					(int)((Number)gainFeature.minValue) * 100,
					(int)((Number)gainFeature.maxValue) * 100,
					2, 10, 100);
		}else{
			adcEMGainSpinner.setValues(100, 100, 100, 2, 1, 10);
		}
		
		Parameter shutterModeFeature = config.getFeature("ShutterTimingMode");
		if(shutterModeFeature != null && shutterModeFeature.isFilled && shutterModeFeature.enumStrings != null){
			fillCombo(shutterModeCombo, shutterModeFeature);
			shutterModeCombo.setEnabled(true);
		}else{
			shutterModeCombo.setText("No Data");
			shutterModeCombo.setEnabled(false);
		}
		
		Parameter triggerModeFeature = config.getFeature("TriggerResponse");
		if(triggerModeFeature != null && triggerModeFeature.isFilled && triggerModeFeature.enumStrings != null){
			fillCombo(triggerModeCombo, triggerModeFeature);
			triggerModeCombo.setEnabled(true);
		}else{
			triggerModeCombo.setText("No Data");
			triggerModeCombo.setEnabled(false);
		}
		
		Parameter adcQualityFeature = config.getFeature("AdcQuality");
		if(adcQualityFeature != null && adcQualityFeature.isFilled && adcQualityFeature.enumStrings != null){
			fillCombo(adcQualityCombo, adcQualityFeature);
			adcQualityCombo.setEnabled(true);
		}else{
			adcQualityCombo.setText("No Data");
			adcQualityCombo.setEnabled(false);
		}
		
		Parameter adcAnalogGainFeature = config.getFeature("AdcAnalogGain");
		if(adcAnalogGainFeature != null && adcAnalogGainFeature.isFilled && adcAnalogGainFeature.enumStrings != null){
			fillCombo(adcAnalogGainCombo, adcAnalogGainFeature);
			adcAnalogGainCombo.setEnabled(true);
		}else{
			adcAnalogGainCombo.setText("No Data");
			adcAnalogGainCombo.setEnabled(false);
		}				
		
		Parameter adcSpeedFeature = config.getFeature("AdcSpeed");
		if(adcSpeedFeature != null && adcSpeedFeature.isFilled && adcSpeedFeature.enumStrings != null){
			fillCombo(adcSpeedCombo, adcSpeedFeature);
			adcSpeedCombo.setEnabled(true);
		}else{
			adcSpeedCombo.setText("No Data");
			adcSpeedCombo.setEnabled(false);
		}
		
		Parameter readoutModeFeature = config.getFeature("ReadoutControlMode");
		if(readoutModeFeature != null && readoutModeFeature.isFilled && readoutModeFeature.enumStrings != null){
			fillCombo(readoutModeCombo, readoutModeFeature);
			readoutModeCombo.setEnabled(true);
		}else{
			readoutModeCombo.setText("No Data");
			readoutModeCombo.setEnabled(false);
		}
		
		exposureLengthTextbox.setText(Double.toString(config.doubleFeature("ExposureTime")));
		
	}
	
	public void fillCombo(Combo combo, Parameter param){
		
 		combo.removeAll();
		
 		int selIdx = 0;
		for(int i=0; i < param.enumStrings.length; i++){
			if(param.enumStrings[i] == null)
				continue;
			
			combo.add(param.enumStrings[i]);			
			if((param.type == PICamDefs.valueType_Enumeration && i == ((Number)param.value).intValue())
				|| (param.type == PICamDefs.valueType_FloatingPoint && 
				     Algorithms.mustParseDouble(param.enumStrings[i]) == ((Number)param.value).doubleValue())){
				combo.select(selIdx);
			}
			selIdx++;
		}
		
	}
	
	void guiToConfig(){
		PicamConfig config = source.getConfig();
		
		config.setFeature("ReadoutCount", continuousButton.getSelection() ? 0L : (long)config.nImagesToAllocate);
		
		Parameter setPointParam = config.getFeature("SensorTemperatureSetPoint");
		int val = coolingSetTempSpinner.getSelection();
		switch(setPointParam.type){
			case PICamDefs.valueType_Integer: setPointParam.value = new Integer(val); setPointParam.toSet = true; break;
			case PICamDefs.valueType_LargeInteger: setPointParam.value = new Long(val); setPointParam.toSet = true; break;
			case PICamDefs.valueType_FloatingPoint: setPointParam.value = new Double(val); setPointParam.toSet = true; break;
			default: //can't set
		}
		
		Parameter shutterModeFeature = config.getFeature("ShutterTimingMode");
		if(shutterModeFeature != null && shutterModeFeature.enumStrings != null){
			shutterModeFeature.value = shutterModeFeature.getEnumIndex(shutterModeCombo.getText());
			shutterModeFeature.toSet = true;
		}
		
		Parameter triggerModeFeature = config.getFeature("TriggerResponse");
		if(triggerModeFeature != null && triggerModeFeature.enumStrings != null){
			triggerModeFeature.value = triggerModeFeature.getEnumIndex(triggerModeCombo.getText());
			triggerModeFeature.toSet = true;
		}
		
		Parameter adcQualityFeature = config.getFeature("AdcQuality");
		if(adcQualityFeature != null && adcQualityFeature.enumStrings != null){
			adcQualityFeature.value = adcQualityFeature.getEnumIndex(adcQualityCombo.getText());
			adcQualityFeature.toSet = true;
		}
		
		Parameter adcAnalogGainFeature = config.getFeature("AdcAnalogGain");
		if(adcAnalogGainFeature != null && adcAnalogGainFeature.enumStrings != null){
			adcAnalogGainFeature.value = adcAnalogGainFeature.getEnumIndex(adcAnalogGainCombo.getText());
			adcAnalogGainFeature.toSet = true;
		}
		
		Parameter adcSpeedFeature = config.getFeature("AdcSpeed");
		if(adcSpeedFeature != null && adcSpeedFeature.enumStrings != null){
			adcSpeedFeature.value = Algorithms.mustParseDouble(adcSpeedCombo.getText());
			adcSpeedFeature.toSet = true;
		}
				
		config.setFeature("AdcEMGain", ((int)adcEMGainSpinner.getSelection()) / 100);
		
		Parameter readoutModeFeature = config.getFeature("ReadoutControlMode");
		if(readoutModeFeature != null && readoutModeFeature.enumStrings != null){
			readoutModeFeature.value = readoutModeFeature.getEnumIndex(readoutModeCombo.getText());
			readoutModeFeature.toSet = true;
		}
		
		double expTime = Algorithms.mustParseDouble(exposureLengthTextbox.getText());
		config.setFeature("ExposureTime", expTime);
		exposureLengthTextbox.setText(Double.toString(expTime));
		
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		PicamConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMin("FrameRate");
		}else{

			config.setFeatureToMax("FrameRate");
		}
		configToGUI();
	}
	
	public void exposureMinMaxEvent(boolean max){
		PicamConfig config = source.getConfig();
		
		if(max){
			config.setFeatureToMax("ExposureTime");
		}else{

			config.setFeatureToMin("ExposureTime");
		}
		configToGUI();
	}

	private void allParamsButton(boolean setDefault) {
		PicamConfig config = source.getConfig();
		
		for(Parameter param : config.getAllFeatures().values()){
			if(setDefault){
				// ???
			}else{
				param.toSet = false;
			}
		}

		configToGUI();
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
