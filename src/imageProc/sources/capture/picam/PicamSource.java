package imageProc.sources.capture.picam;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.flir.FLIRCamConfig;
import imageProc.sources.capture.picam.PicamConfig.Parameter;
import imageProc.sources.capture.picam.swt.PicamSWTControl;
import mds.GMDSFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import picamJNI.PICamDefs;
import picamJNI.PICamROIs;
import picamJNI.PICamROIs.PICamROI;
import signals.gmds.GMDSSignal;

/** Princeton Instruments PICam Image Source.
 *  
 * All the actual driver'ey code is in the thread in PICamCapture
 *  
 * @author oliford
 */
public class PicamSource extends CaptureSource {
	
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	private int lastCaptured;
	
	/** The low-level capturer, we should only have one of these */  
	private PicamCapture capture;
	
	/** The current config, should be kept always in sync with camera (if online)
	 * and with controller */
	private PicamConfig config;
	
	public PicamSource(PicamCapture capture, PicamConfig config) {
		this.capture = capture;
		this.config = (config != null) ? config : new PicamConfig();
	}
	
	public PicamSource() {
		capture = new PicamCapture(this);
		config = new PicamConfig();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by SensicamCapture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	
	/** Called by SensicamCapture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		lastCaptured = imageIndex;
		
		updateAllControllers();
		
	}
	
	private long lastAutoExposure = 0;
	private long autoExposureMinPeriod = 50; //ms
	private double autoExposureMinLevel = 0.6;

	private double maxSum = 0;
	private int samplesInSum;

	@Override
	public PicamSource clone() {
		return new PicamSource(capture, config);
	}

	public void openCamera(){
		if(capture.isOpen()){
			System.err.println("openCamera(): Camera already open");
			return;
		}
		capture.setConfig(config);
		capture.initCamera();
	}

	public void startCapture(){
		if(capture.isBusy()){
			System.err.println("startCapture(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		
		lastAutoExposure = System.currentTimeMillis();

		capture.setConfig(config);
		capture.startCapture(config, images);
	}
		
	public void syncConfig(){
		if(capture.isBusy()){
			System.err.println("syncConfig(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		
		lastAutoExposure = System.currentTimeMillis();	
		capture.setConfig(config);	
		capture.testConfig(config, images);
	}
		
	public void abort(){ capture.abort(false); }
	
	public void closeCamera(){ capture.closeCamera(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToAllocate + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). S/W start "				
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	public String getCCDInfo(){
		if(config.id == null)
			return "PICam (NULL) ???";
		
						
		return "PICam " +config.id.getModelName() + ", (" + config.id.getModelID() 
				+ "), S/N: " + config.id.getSerialNumber()  
				+ ", Sensor: " + config.id.getSensorName()
				+ ", IF: " + config.id.getComputerInterface();
				
	}
	
	public int getNumCaptured(){ return nCaptured; }
	
	public boolean isActive(){ return capture.isOpen(); }
	
	public boolean isBusy(){ return capture.isBusy(); }
	
	@Override
	public boolean isIdle() { return capture == null || !capture.isOpen() || !capture.isBusy(); }

	public PicamConfig getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (PicamConfig)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new PicamSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	public void releaseMemory() {
		capture.abort(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, PicamConfig.class);
		
		//json messes up the types of many of the parameters
		//try to fix them
		for(Parameter param : config.getAllFeatures().values()){
			switch(param.type){
				case PICamDefs.valueType_LargeInteger: 
					param.value = (param.value == null) ? null : ((Number)param.value).longValue();
					param.minValue = (param.minValue == null) ? null : ((Number)param.minValue).longValue();
					param.maxValue = (param.maxValue == null) ? null : ((Number)param.maxValue).longValue();
					break;
				case PICamDefs.valueType_Integer:
				case PICamDefs.valueType_Enumeration:
					param.value = (param.value == null) ? null : ((Number)param.value).intValue();
					param.minValue = (param.minValue == null) ? null : ((Number)param.minValue).intValue();
					param.maxValue = (param.maxValue == null) ? null : ((Number)param.maxValue).intValue();
					break;
			}
		}
		
		//json puts the ArrayLists in PICamROIs in a weird linked tree thing
		//we need to fix it so the PICam SDK JNI layer can process it
		Object jsonRegurgitation = config.getFeature("Rois").value;
		if(jsonRegurgitation != null){
			LinkedTreeMap<String, Object> thing = (LinkedTreeMap<String, Object>)jsonRegurgitation;
			
			PICamROIs rois = new PICamROIs();
			ArrayList<LinkedTreeMap> listOfThings = (ArrayList<LinkedTreeMap>)thing.get("rois");
			
			for(LinkedTreeMap mapThing : listOfThings){
				PICamROI roi = new PICamROI();
				roi.name = (String)mapThing.get("name");
				roi.enabled = (Boolean)mapThing.get("enabled");

				roi.x = ((Number)mapThing.get("x")).intValue();
				roi.width = ((Number)mapThing.get("width")).intValue();
				roi.x_binning = ((Number)mapThing.get("x_binning")).intValue();

				roi.y = ((Number)mapThing.get("y")).intValue();
				roi.height = ((Number)mapThing.get("height")).intValue();
				roi.y_binning = ((Number)mapThing.get("y_binning")).intValue();
				
				rois.addROI(roi);
			}
			
			config.setFeatureToObj("Rois", rois);
		}
		
		updateAllControllers();	
	}
	
	public void loadConfigGMDS(String exp, int pulse) {
		if(pulse == -1 && connectedSource != null){
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		}		
		
		GMDSFetcher gmds = GMDSUtil.globalGMDS();
		
		// look for the new one first
		String rootPath = "RAW/seriesData/PICam/config";
		String sigsPaths[] = gmds.dumpTree(exp, pulse, rootPath, true);

		for (String signalPath : sigsPaths) {
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, exp,
					rootPath + "/" + signalPath);

			try {
				GMDSSignal sig = (GMDSSignal) gmds.getSig(sigDesc);
				Object data = sig.getData();
				
				if(data.getClass().isArray() && Array.getLength(data) == 1){
					data = Array.get(data, 0); //grumble
				}
				
				signalPath = signalPath.replaceAll("/", "");
				
				if(signalPath.equals("cameraIndex")){ continue; } 
				if(signalPath.equals("nImagesToAllocate")){ config.nImagesToAllocate = (Integer)data; continue; }
				
				Parameter feature = config.getFeature(signalPath);
				
				if(feature == null){
					System.err.println("PicamSource.loadSettings("+exp + "," + pulse+"): No feature named '" + signalPath + "'.");
					continue;
				}
				
				if(!feature.isImplemented || !feature.isWritable){
					System.err.println("PicamSource.loadSettings("+exp + "," + pulse+"): Feature '" + signalPath + "' is not implemented or not writable.");
					continue;
				}
				
				if(data.getClass() == feature.typeAsObjectClass() ||
						(feature.type == PICamDefs.valueType_Enumeration && data.getClass() == String.class)){
					if(!data.equals(feature.value)){
						feature.value = data;
						feature.toSet = true;
					}
					
				}else if(feature.typeAsPrimitiveClass() == boolean.class && data.getClass() == Integer.class){
					//allow conversion of int to boolean, since GMDS doesn't save booleans
					feature.value = (boolean)(((Integer)data) != 0);
					feature.toSet = true;
					
				}else{
					System.err.println("PicamSource.loadSettings("+exp + "," + pulse+"): Type of feature '"
										+feature.name+" doesn't match incoming "+data.getClass().getSimpleName());
				}
				
			} catch (RuntimeException err) {
				System.err.println("Couldn't read PIcam config from signal '" + sigDesc
						+ "': " + err.getMessage());
			}
		}
			
		updateAllControllers();
	}
	
	@Override
	public void close() {
		abort();
		closeCamera();		
	}

	@Override
	public boolean open(long timeoutMS) {
					
		openCamera();
		long t0 = System.currentTimeMillis();
		while(!capture.isOpen()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted while waiting for camera to open.");
			}
			
			if((System.currentTimeMillis() - t0) > timeoutMS){
				throw new RuntimeException("Timed out waiting for camera to open. Need a longer warm-up time??");
			}
		}
		
		
		//do a config sync as-is
		syncConfig();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) { 
			throw new RuntimeException("Interrupted while waiting for camera to open.");
		}
		
		return true;
	}


	public void setDiskMemoryLimit(long maxMemory) { capture.setDiskMemoryLimit(maxMemory); }
	
	public long getDiskMemoryLimit(){  return capture.getDiskMemoryLimit(); }

	@Override
	public String toShortString() {
		return "PICam"; 
	}

	@Override
	protected final Capture getCapture() { return capture; }
	
	@Override
	public int getFrameCount() { return config.nImagesToAllocate; }
	
	@Override
	public void setFrameCount(int frameCount) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.nImagesToAllocate = frameCount;
		config.setFeature("ReadoutCount", config.isContinuous()? 0L : (long)config.nImagesToAllocate);
		updateAllControllers();
	}
	
	@Override
	public long getFramePeriod() { return (long)(1.0e6 / config.doubleFeature("FrameRateCalculation")); }
	
	@Override
	public void setFramePeriod(long framePeriodUS) {
		throw new RuntimeException("Not implemented. Need to think about how to set frame period for a CCD");
		//config.setFeature("FrameRate", 1.0e6 / framePeriodUS);
		//updateAllControllers();
	}

	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig;	}	
}
