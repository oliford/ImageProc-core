package imageProc.sources.capture.picam;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import imageProc.sources.capture.base.CaptureConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import picamJNI.PICamDefs;
import picamJNI.PICamParameters;
import picamJNI.PICamROIs;
import picamJNI.PICamROIs.PICamROI;
import picamJNI.PicamCameraID;

/** Configuration for PICam cameras */
public class PicamConfig extends CaptureConfig {

	/** General stuff that's outside of the camera's own config */
	
	/** Number of images to allocate and to capture 
	 * if in non-continuous mode */	 
	public int nImagesToAllocate;

	/** Index of camera to use */
	public int cameraIndex;
	
	/** Open a simulated camera if none exist */
	public boolean allowDemoCamera = false;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame;
	
	/** Period at which to poke the camera to see if it's a live while waiting for external trigger */
	public long cameraHealthCheckPeriod = 10000;
	
	/** Timeout for WaitForAcquisitionUpdate() call */
	public int grabberTimeoutMS = 100;
	
	public long waitTimeout = 1000;
	
	/** Parameters for PICAM, like Features for Andor */
	public static class Parameter {
				
		/** API ident (integer). One of PICamDefs.parameter__xxx */		
		public int id = -1;
		public int type = -1; //one of PICamDefs.valueType_*;
		public String name = null;
		public boolean isFilled = false;
		public boolean isImplemented = false;
		public boolean isWritable = false;
		public Object value;
		public Object minValue;
		public Object maxValue;	
		public String[] enumStrings;
		public boolean[] enumIsImplemented;		
		
		/** Order index. Features are set to camera lower order first */
		private int order;
		
		/** Should we attempt to set it on the next config sync? */
		public boolean toSet = false;		
		
		public Parameter(int paramID, String name, int order) { 					
			this.name = name;
			this.id = paramID;
			this.order = order;
			this.toSet = false;
			this.isImplemented = false;
			this.type = PICamParameters.getType(paramID);
		}		


		public String typeAsString() {
			if(!isImplemented)
				return "not impl";
			
			if(type <= 0)
				return "?";
			
			switch(type){
				case PICamDefs.valueType_Boolean: return "bool";
				case PICamDefs.valueType_Integer: return "int";
				case PICamDefs.valueType_LargeInteger: return "long";
				case PICamDefs.valueType_FloatingPoint: return "float";
				case PICamDefs.valueType_Enumeration: return "enum";
				default: return "unknown";
			}
		}
		
		public Class typeAsPrimitiveClass() {
			if(!isImplemented || type <= 0)
				return null;
			
			switch(type){
				case PICamDefs.valueType_Boolean: return boolean.class;
				case PICamDefs.valueType_Integer: return int.class;
				case PICamDefs.valueType_LargeInteger: return long.class;
				case PICamDefs.valueType_FloatingPoint: return double.class;
				case PICamDefs.valueType_Enumeration: return String[].class;
					default: return null;
			}
		}
		
		public Class typeAsObjectClass() {
			if(!isImplemented || type <= 0)
				return null;
			
			switch(type){
				case PICamDefs.valueType_Boolean: return Boolean.class;
				case PICamDefs.valueType_Integer: return Integer.class;
				case PICamDefs.valueType_LargeInteger: return Long.class;
				case PICamDefs.valueType_FloatingPoint: return Double.class;
				case PICamDefs.valueType_Rois: return PICamROIs.class;
				case PICamDefs.valueType_Enumeration: return String[].class;
					default: return null;
			}
		}

		public String valueAsString() {
			if(!isImplemented || type <= 0)
				return "-";			
			switch(type){
				case PICamDefs.valueType_Boolean: return value == null ? "" : ((Boolean)value).toString();
				case PICamDefs.valueType_Integer: return value == null ? "" : ((Integer)value).toString();
				case PICamDefs.valueType_LargeInteger: return value == null ? "" : ((Long)value).toString();
				case PICamDefs.valueType_FloatingPoint: return value == null ? "" : value.toString();
				case PICamDefs.valueType_Rois: return value == null ? "" : "ROIs  (See ROIs Tab)";
				case PICamDefs.valueType_Enumeration: 
					return (value == null || (Integer)value < 0 || enumStrings == null || (Integer)value >= enumStrings.length) ? "INVALID" : enumStrings[(Integer)value];
				default: return "?";
			}
		}
		
		public String minValueAsString() {
			if(!isImplemented || type <= 0)
				return "-";
			switch(type){
				case PICamDefs.valueType_Boolean: return minValue == null ? "" : ((Boolean)minValue).toString();
				case PICamDefs.valueType_Integer: return minValue == null ? "" : ((Integer)minValue).toString();
				case PICamDefs.valueType_LargeInteger: return minValue == null ? "" : ((Long)minValue).toString();
				case PICamDefs.valueType_FloatingPoint: return minValue == null ? "" : ((Double)minValue).toString();
				case PICamDefs.valueType_Enumeration: return "??";
				default: return "?";
			}
		}
		
		public String maxValueAsString() {
			if(!isImplemented || type <= 0)
				return "-";
			switch(type){
				case PICamDefs.valueType_Boolean: return maxValue == null ? "" : ((Boolean)maxValue).toString();
				case PICamDefs.valueType_Integer: return maxValue == null ? "" : ((Integer)maxValue).toString();
				case PICamDefs.valueType_LargeInteger: return maxValue == null ? "" : ((Long)maxValue).toString();
				case PICamDefs.valueType_FloatingPoint: return maxValue == null ? "" : ((Double)maxValue).toString();
				case PICamDefs.valueType_Enumeration: return "??";
				default: return "?";
			}
		}

		public void setValueByString(String value) {
			if(!isImplemented || type <= 0)
				return;
			switch(type){
				case PICamDefs.valueType_Boolean: this.value = (Boolean)(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("T") || value.equalsIgnoreCase("TRUE")); break;
				case PICamDefs.valueType_Integer: this.value = (Integer)Algorithms.mustParseInt(value); break;
				case PICamDefs.valueType_LargeInteger: this.value = (Long)Algorithms.mustParseLong(value); break;
				case PICamDefs.valueType_FloatingPoint:  this.value = (Double)Algorithms.mustParseDouble(value); break;
				case PICamDefs.valueType_Enumeration: 
					for(int i=0; i < enumStrings.length; i++){
						if(value.equals(enumStrings[i])){
							this.value = i;
							break;
						}
					}
				default: // !?
			}
		}

		public int getEnumIndex(String name){
			if(value == null || enumStrings == null)
				return -1;
			
			for(int i=0; i < enumStrings.length; i++){
				if(name.equals(enumStrings[i]))
					return i;
			}
			return -1;
		}

	}
	
	public PicamConfig() {
		
		//params get added by Capture when the camera is initialised
		
		//PicamParameter.addFeature(features, "TriggerMode", FeatureType.PICamDefs.valueType_Enumeration, 100);	
		
		//defaults
		cameraIndex = 0;
		nImagesToAllocate = 20;
		blackLevelFirstFrame = false;
		//setFeature("FrameCount", nImagesToAllocate);
		//setFeature("SensorCooling", true);
		//setFeature("MetadataEnable", true);
		//setFeature("MetadataTimestamp", true);
		
		//default timestamps on, long
		
		//force some things
		//createForcedParameter(PICamParameters.ReadoutCount, "ReadoutCount", (long)nImagesToAllocate);
		//createForcedParameter(PICamParameters.TimeStamps, "Timestamps", PICamDefs.timeStampsMask_ExposureStarted | PICamDefs.timeStampsMask_ExposureEnded);
		//createForcedParameter(PICamParameters.TimeStampBitDepth, "TimeStampBitDepth", 64);
		//createForcedParameter(PICamParameters.TimeStampResolution, "TimeStampResolution", 1_000_000_000L); //ns
		
	}
	
	private void createForcedParameter(int paramID, String name, Object value) {

		Parameter p = new Parameter(paramID, name, 0);
		p.value = value;
		p.toSet = true;
		p.isImplemented = true;
		p.isFilled = true;
		p.isWritable = true;
		if(PICamParameters.getType(paramID) == PICamDefs.valueType_Enumeration){
			int n = ((int)value)+1;
			p.enumStrings = new String[n];
			for(int i=0; i < n; i++)
				p.enumStrings[i] = "INIT_" + n;
		}
		addParameter(p);
	}
	

	//we just store everything that the SDK presents as 'features'
	private TreeMap<String, Parameter> parameters = new TreeMap<String, Parameter>();

	/** Enable setting of all ROIs. When disabled the ROI configuration is kept in this class
	 * but not written to the camera */	
	public boolean enableROIs;

	/** Camera identity */
	public PicamCameraID id;

	public void addParameter(Parameter param){		
		parameters.put(param.name, param);
	}
	
	
	public TreeMap<String, Parameter> getAllFeatures() { return parameters;  }

	public int intFeature(String name) {
		Parameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return -1;
		return (int)((Integer)feature.value); //ffs with the casting 
	}
	
	public long longFeature(String name) {
		Parameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return -1;
		return (long)((Long)feature.value); //ffs with the casting 
	}
	
	public int imageSizeBytes(){ return intFeature("ReadoutStride"); }
	
	public int imageWidth() {
		//TODO: ROI
		//int x = intFeature("AOIWidth");
		//return x > 0 ? x : intFeature("ActiveWidth");
		return intFeature("ActiveWidth");
	}
	public int imageHeight(){
		//TODO: ROI
		//int x = intFeature("AOIHeight");
		//return x > 0 ? x : intFeature("ActiveHeight");
		return intFeature("ActiveHeight");
	}
	public double frameTimeMS() { return 1000.0 / doubleFeature("FrameRateCalculation");	}
	public double exposureTimeMS() { return doubleFeature("ExposureTime"); }
	
	public Parameter getFeature(String name) { return parameters.get(name); }

	public boolean isContinuous() {
		return longFeature("ReadoutCount") <= 0;
	}

	public boolean featureMatchesString(String name, String value) {
		Parameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return false;
		return feature.value.equals(value); 
	}

	public boolean boolFeature(String name) {
		Parameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return false;
		return (Boolean)feature.value; 
	}

	public double doubleFeature(String name) {
		Parameter feature = parameters.get(name);
		if(feature == null || feature.value == null)
			return Double.NaN;
		
		return (feature.value instanceof Float) 
					? (Double)(double)(float)feature.value 
					: (Double)feature.value; 
	}

	/**These set the feature in our config and mark them to be set */
	
	public void setFeature(String name, Integer value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Double value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Long value) { setFeatureToObj(name, value); }
	public void setFeature(String name, Boolean value) { setFeatureToObj(name, value); }
	public void setFeature(String name, String value) { setFeatureToObj(name, value); }
	
	public void setFeatureToObj(String name, Object value) {
		Parameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = value;
			feature.toSet = true;
		}
	}

	public void dontSetFeature(String name) {
		Parameter feature = parameters.get(name);
		if(feature != null){		
			feature.toSet = false;
		}
	}
	
	public void setFeatureToMin(String name) {
		Parameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = feature.minValue;
			feature.toSet = true;
		}
	}
	
	public void setFeatureToMax(String name) {
		Parameter feature = parameters.get(name);
		if(feature != null){			
			feature.value = feature.maxValue;
			feature.toSet = true;
		}
	}

	/** Creates a simple HashMap of java number/string objects etc */
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
	
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/cameraIndex", cameraIndex);
		map.put(prefix + "/enableROIs", enableROIs);
		map.put(prefix + "/allowDemoCamera", allowDemoCamera);
		
		for(Parameter feature : parameters.values()){
			if(feature.isFilled && feature.value != null){
				if(feature.type == PICamDefs.valueType_Rois){
					map.put(prefix + "/" + feature.name + "_configString",
							(feature.value == null) ? "NULL" :
							((PICamROIs)feature.value).toString());
					
					if(feature.value != null && enableROIs)
						((PICamROIs)feature.value).addArraysToMap(map, prefix + "/" + feature.name + "_arrays" , true);

				}else{
					map.put(prefix + "/" + feature.name, feature.value);
				}
			}
		}
		
		return map;
	}

	/** Return list of features ordered according to their 'order' value, for setting */
	public Collection<Parameter> getSetOrderedFeatures() {

		ArrayList<Parameter> al = new ArrayList<Parameter>(parameters.values());
		
		Collections.sort(al, new Comparator<Parameter>() {
			@Override
			public int compare(Parameter o1, Parameter o2) {				
				return Integer.compare(o1.order, o2.order);
			}
		});
		
		return al;
		
	}


	public PICamROIs getROIs() {
		Parameter param = getFeature("Rois"); 
		if(param == null){ //sometimes we need this to exist before the camera is first sync'ed
			param = new Parameter(PICamParameters.Rois, "Rois", 0);
			parameters.put("Rois", param);
		}
		if(param.value == null){
			param.value = new PICamROIs();
		}
			
		return (PICamROIs)param.value;
	}


	/** Jog all ROIs by given amount, keep edges at edges and adjust width/height and binning accordingly */
	public void jogROI(PICamROI roi, int dx, int dy) {
		getROIs().jog(roi, intFeature("ActiveWidth"), intFeature("ActiveHeight"), dx, dy);
	}
}
