package imageProc.sources.capture.as5216;

import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;

public class AvantesAS5216Config extends CaptureConfig {

	/** Number of images in full buffer */
	public int nImagesToAllocate = 500;
	
	public int numberOfAverages = 1;
	
	/** Integration time in US */
	public int integrationTimeMS = 100;

	/** continuous acquisition - wrap back to image 0 */
	public boolean wrap;

	/** List of serial numbers of the devices corresponding to each row */
	public String[] deviceSerialNumbers;

	public int delayTimeMS;

	/** Number of pixels for each device/row */
	public int[] numPixels;
	
	/** Image width and height
	 * width is largest number of pixels of any detector
	 * height is number of detectors
	 */
	public int imageWidth, imageHeight;
	
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put(prefix + "/deviceSerialNumbers", deviceSerialNumbers);
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);		
		map.put(prefix + "/numberOfAverages", numberOfAverages);
		map.put(prefix + "/integrationTimeMS", integrationTimeMS);
		map.put(prefix + "/delayTimeMS", delayTimeMS);
		map.put(prefix + "/numPixels", numPixels);
		map.put(prefix + "/imageWidth", imageWidth);
		map.put(prefix + "/imageHeight", imageHeight);
		map.put(prefix + "/wrap", wrap); 

		return map;
	}
	
}
