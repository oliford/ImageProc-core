package imageProc.sources.capture.as5216;

import java.nio.ByteOrder;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.AcquisitionDevice;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.EventReciever.Event;
import imageProc.database.gmds.GMDSUtil;
import imageProc.sources.capture.andorV2.AndorV2Config;
import imageProc.sources.capture.as5216.swt.AvantesAS5216SWTControl;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.oceanOptics.swt.OmniDriverSWTControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class AvantesAS5216Source extends CaptureSource implements ImgSource, ConfigurableByID {
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	private int lastCaptured;
	
	/** The low-level capturer, we should only have one of these */  
	private AvantesAS5216Capture capture;
	
	/** The current config, should be kept always in sync with camera (if online)
	 * and with controller */
	private AvantesAS5216Config config;
	
	public AvantesAS5216Source(AvantesAS5216Capture capture, AvantesAS5216Config config) {
		this.capture = capture;
		this.config = (config != null) ? config : new AvantesAS5216Config();
	}
	
	public AvantesAS5216Source() {
		capture = new AvantesAS5216Capture(this);
		config = new AvantesAS5216Config();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by capture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	
	/** Called by capture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		lastCaptured = imageIndex;
		
		updateAllControllers();
				
	}
	
	@Override
	public AvantesAS5216Source clone() {
		return new AvantesAS5216Source(capture, config);
	}

	public void openLibrary(){
		if(capture.isOpen()){
			System.err.println("openLibrary(): Library already open");
			return;
		}		
		capture.setConfig(config);
		capture.initLibrary();
	}

	@Override
	public void startCapture(){
		if(capture.isBusy()){
			System.err.println("start(): Capture thread busy");
			return;
		}
		
		this.nCaptured = 0;		

		capture.setConfig(config);
		capture.startCapture(config, images);
	}
		
	public void abort(){ capture.abort(false); }

	public void close(){ capture.closeLibrary(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToAllocate + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). S/W Trigger"				
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	public String getCCDInfo(){
		/*String camModel = config.name + " / " + config.serialNumber;
				
		return "AS5216 " + ((camModel != null) ? camModel : "???") +
				": " + config.nPixelsTotal + " x " + config.nChannels;
				*/
		return "AS5216: ????";
	}
	
	public boolean isActive(){ return capture.isOpen(); }
	
	public boolean isBusy(){ return capture.isBusy(); }
	
	@Override
	public boolean isIdle() { return capture == null || !capture.isOpen() || !capture.isBusy(); }

	@Override
	public AvantesAS5216Config getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (AvantesAS5216Config)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new AvantesAS5216SWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	public void releaseMemory() {
		capture.abort(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}

	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	
	public void closeCamera(){ capture.closeLibrary(false); }

	@Override
	public boolean open(long timeoutMS) {
			
		if(!capture.isOpen())
			capture.initLibrary();

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) { }
		
		return true;
	}


	public void setDiskMemoryLimit(long maxMemory) { capture.setDiskMemoryLimit(maxMemory); }
	
	public long getDiskMemoryLimit(){  return capture.getDiskMemoryLimit(); }
	
	@Override
	public String toShortString() {
		/*if(capture.isOpen() && config.spectrometerClass != null){
			String parts[] = config.spectrometerClass.split(".");
			return "AS5216[" + ((parts.length<2) ? config.spectrometerClass : parts[parts.length-1]) + ":"+config.deviceIndex+"]";
		}else
			return "AS5216[Closed]";
		*/
		return "AS5216";
	}

	@Override
	protected final Capture getCapture() { return capture; }
	
	@Override
	public boolean setConfigParameter(String param, String value) {
		if(param.equals("runLength")){
			double runLengthSecs = Algorithms.mustParseDouble(value);
			config.nImagesToAllocate = (int)(runLengthSecs / 00000000 + 0.5);
			return true;
		}
		
		return false;
	}
	
	@Override
	public String getConfigParameter(String param) {
		if(param.equals("runLength")){
			return String.format("%f", config.nImagesToAllocate * 0000000000);
			
		}else if(param.equals("exposureTime")){
			return null;
			
		}else if(param.equals("frameTime")){
			return null;
			
		}
		
		return null;
	}

	public String[] getOpenDevices() { return capture.getOpenDevices(); }

}
