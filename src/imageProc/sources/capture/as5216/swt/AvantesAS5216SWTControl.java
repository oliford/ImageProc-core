package imageProc.sources.capture.as5216.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.sources.capture.as5216.AvantesAS5216Config;
import imageProc.sources.capture.as5216.AvantesAS5216Source;

public class AvantesAS5216SWTControl implements ImagePipeController {
	
	private Group swtGroup;

	private Label sourceStatusLabel;
	private Label captureStatusLabel;
		
	private Button openButton;
	private Button closeButton;
	

	private List devicesList;
	
	private Spinner numFramesSpinner;
	private Spinner numAveragesSpinner;
	private Button startButton;
	private Button abortButton;
	
	private Spinner exposureTimeMS;
	private Button continuousCheckbox;	
	private Combo externalTriggerCombo;

	private Button acquireEnableCheckbox;
	private Button globalStartEnableCheckbox;

	private JSONFileSettingsControl jsonSettings;
		
	private AvantesAS5216Source source;
	
	public AvantesAS5216SWTControl(Composite parent, int style, AvantesAS5216Source source) {
		this.source = source;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("AS5216 Control");
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("Source status:");
		sourceStatusLabel = new Label(swtGroup, SWT.NONE);
		sourceStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capture status:");
		captureStatusLabel = new Label(swtGroup, SWT.NONE);
		captureStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
				
		openButton = new Button(swtGroup, SWT.PUSH);
		openButton.setText("Open");
		openButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		openButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { openButtonEvent(event); } });
		
		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close");
		closeButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeButtonEvent(event); } });
		
		Label lDL = new Label(swtGroup, SWT.NONE); lDL.setText("Devices:");
		devicesList = new List(swtGroup, SWT.NONE);
		devicesList.setItems(new String[]{ " ", " ", " ", " ", " ", " ", " ", " ", " " });
		devicesList.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lNI = new Label(swtGroup, SWT.NONE); lNI.setText("no. Frames:");
		numFramesSpinner = new Spinner(swtGroup, SWT.NONE);
		numFramesSpinner.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		numFramesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		numFramesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		continuousCheckbox = new Button(swtGroup, SWT.CHECK);
		continuousCheckbox.setText("Continuous");
		continuousCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		continuousCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNA = new Label(swtGroup, SWT.NONE); lNA.setText("no. Averages:");
		numAveragesSpinner = new Spinner(swtGroup, SWT.NONE);
		numAveragesSpinner.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		numAveragesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		numAveragesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure time / ms:");
		exposureTimeMS = new Spinner(swtGroup, SWT.NONE);
		exposureTimeMS.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		exposureTimeMS.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		exposureTimeMS.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lXT = new Label(swtGroup, SWT.NONE); lXT.setText("External Trigger:");		
		externalTriggerCombo = new Combo(swtGroup, SWT.MULTI);
		externalTriggerCombo.setItems(new String[0]);
		//externalTriggerCombo.setItems(AvantesAS5216Config.triggerModes);
		//externalTriggerCombo.select(AvantesAS5216Config.TRIGGERMODE_NORMAL);
		externalTriggerCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		externalTriggerCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { openButtonEvent(event); } });
		
		acquireEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		acquireEnableCheckbox.setText("Acquire");		
		acquireEnableCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		acquireEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		globalStartEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		globalStartEnableCheckbox.setText("Start on S/W trigger");		
		globalStartEnableCheckbox.setToolTipText("When selected, camera will start acquiring if the global start event is broadcast (i.e. the Start All button)");
		globalStartEnableCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		globalStartEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		startButton = new Button(swtGroup, SWT.PUSH);
		startButton.setText("Start");
		startButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startButtonEvent(event); } });
		
		abortButton = new Button(swtGroup, SWT.PUSH);
		abortButton.setText("Abort");
		abortButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		abortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortButtonEvent(event); } });

		Label lFill = new Label(swtGroup, SWT.NONE);
		lFill.setText("");
		lFill.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		
		
		jsonSettings = new JSONFileSettingsControl();
		jsonSettings.buildControl(swtGroup, SWT.NONE, source);
		jsonSettings.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 5, 1));
		
		doUpdate();
	}
	
	protected void abortButtonEvent(Event event) {
		source.abort();
		
	}

	protected void startButtonEvent(Event event) {
		source.startCapture();
	}

	protected void settingsChangedEvent(Event event) {
		
		//do acquire enable first, might be time critical
		source.enableAcquire(acquireEnableCheckbox.getSelection());
		source.enableEventResponse(globalStartEnableCheckbox.getSelection(), false);
		
		AvantesAS5216Config config = source.getConfig();
		
		config.nImagesToAllocate = numFramesSpinner.getSelection();
		config.numberOfAverages = numAveragesSpinner.getSelection();
		
		config.integrationTimeMS = exposureTimeMS.getSelection();
		//config.externalTriggerMode = externalTriggerCombo.getSelectionIndex();
		config.wrap = continuousCheckbox.getSelection();
		
	}

	protected void closeButtonEvent(Event event) {
		source.close();
	}

	protected void openButtonEvent(Event event) {
		source.openLibrary();
	}
	

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}
	
	private void doUpdate() {
		if(swtGroup.isDisposed())
				return;
		
		sourceStatusLabel.setText(source.getSourceStatus());
		captureStatusLabel.setText(source.getCaptureStatus());
		captureStatusLabel.setToolTipText(source.getCaptureStatus());
		
		AvantesAS5216Config cfg = source.getConfig();
		numFramesSpinner.setSelection(cfg.nImagesToAllocate);
		exposureTimeMS.setSelection(cfg.integrationTimeMS);
		continuousCheckbox.setSelection(cfg.wrap);
		//externalTriggerCombo.select(cfg.externalTriggerMode);
		
		acquireEnableCheckbox.setSelection(source.isAcquireEnabled());
		globalStartEnableCheckbox.setSelection(cfg.beginCaptureOnStartEvent);
		
		devicesList.setItems(source.getOpenDevices());
	}

	@Override
	public void destroy() {
		source.controllerDestroyed(this);
		swtGroup.dispose();		
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return source; }
	
	
}
