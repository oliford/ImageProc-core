package imageProc.sources.capture.as5216.swt;

import java.util.HashMap;

public class AS5216Driver {
	private final static HashMap<Integer, String> errors = new HashMap<Integer, String>();
	static{
		errors.put(0x00, "unknown");
		errors.put(0x01, "invalid parameter");
		errors.put(0x02, "invalid password");
		errors.put(0x03, "invalid command");
		errors.put(0x04, "invalid size");
		errors.put(0x05, "meas pending");
		errors.put(0x06, "invalid pixel range");
		errors.put(0x07, "invalid int. time");
		errors.put(0x08, "operation not supported");
		errors.put(0x09, "invalid combination");
		errors.put(0x0A, "no buffer avail.");
		errors.put(0x0B, "no spectra avail.");
		errors.put(0x0C, "invalid state");
		errors.put(0x0D, "unexpected dma int");
		errors.put(0x0E, "invalid fpga file");
	}

	//commands
	public final static byte get_device_configuration = 0x01; //Request from PC to spectrometer to send the device configuration structure.
	public final static byte set_device_configuration = 0x02; // Request from PC to overwrite configuration the device configuration structure
	public final static byte prepare_measurement = 0x05; // Request from PC to spectrometer to prepare a measurement with the specified configuration
	public final static byte start_measurement = 0x06; //Request from PC to spectrometer to start measurement and send measurement data in response.
	public final static byte get_stored_meas = 0x07; //- Request to send measurement data of first unread measurement in RAM 
	public final static byte get_avg_stored_meas = 0x08; //- Request to send averaged measurement data of first unread averaged measurement in RAM
	public final static byte set_SD_card = 0x09; //SDConfigData Request to overwrite SD configuration data (enable/disable writing spectra to SD card)
	public final static byte get_digital_in = 0x0A; //InputId Request to send status of the specified digital input
	public final static byte set_digital_output = 0x0B; //OutputId, Request to set the specified OutputValue digital output
	public final static byte get_analog_in = 0x0C; //AnInputId Request to send status of the specified analog input
	public final static byte set_analog_out = 0x0D; //AnOutputId, Request to set the specified AnOutputValue analog output
	public final static byte get_version_info = 0x0E; //- Request to send the version info of microcontroller and FPGA
	public final static byte stop_measurement = 0x0F; //- Request to stop pending measurements
	public final static byte set_pwm = 0x10; //OutputId, Request to set pwm signal on Freq, output PercHigh,
	public final static byte special_measurement = 0x11; //IntTime Request to do a single measurement with different integration time
	public final static byte get_reset_reason = 0x12; //0 Request to send and clear the reset reason 
	public final static byte get_ident = 0x13; //- Request to send identification information
	public final static byte get_file = 0x14; //Filename Request to send file from SD Card 
	public final static byte get_filelen = 0x15; //Filename Request to send length of file from SD Card
	public final static byte get_firstfile = 0x16; //- Request to search for file on SD Card and return the first file in the root directory
	public final static byte get_nextfile = 0x17; //Filename Request to search for next file on SD Card 
	public final static byte delete_file = 0x18; //Filename Request to delete file on SD Card 
	public final static byte set_sync_master = 0x19; //SyncMaster Request to enable/disable synchronisation 
	public final static byte set_trigger_mode = 0x1A; //HwTriggerMode Request to set hardware or software trigger mode, is used to support edge triggered sync mode.
	
	//responses
	public final static byte error_response = 0x00; //ErrorReason Error response on one of the previously described requests 
	public final static byte get_device_configuration_response = (byte)0x81; //ConfigData Response from spectrometer configuration_response on get_device_configuration request 
	public final static byte set_device_configuration_response = (byte)0x82; //None Ok response after deviceconfiguration_response configuration overwritten
	public final static byte prepare_measurement_response = (byte)0x85; //None Ok response after prepare response measurement request
	public final static byte start_measurement_response = (byte)0x86; //None OK response after start response measurement request
	public final static byte get_stored_meas_response = (byte)0x87; //TimeStamp, TimeLabel last pixel MeasData Spectrum data
	public final static byte get_avg_stored_meas_response = (byte)0x88; //TimeStamp, TimeLabel last pixel MeasAvgData Summed spectrum data
	public final static byte set_SD_card_response = (byte)0x89; //None Ok response after set SD card configuration overwritten
	public final static byte get_digital_in_response = (byte)0x8A; //InValue Status of digital input
	public final static byte set_digital_output_response = (byte)0x8B; //None Ok response, output is set. 
	public final static byte get_analog_in_response = (byte)0x8C; //AnInValue Value of analog input
	public final static byte set_analog_out_response = (byte)0x8D; //None Ok response, output is set.
	public final static byte get_version_info_response = (byte)0x8E; //VersionInfo FPGA, Firmware and hardware version
	public final static byte stop_measurement_response = (byte)0x8F; //- Ok response
	public final static byte set_pwm_response = (byte)0x90; //None Ok response
	public final static byte special_measurement_response = (byte)0x91; //None Ok response 
	public final static byte get_reset_reason_response = (byte)0x92; //ResetReason OK response, with value of reset reason
	public final static byte get_ident_response = (byte)0x93; //Identification OK response with identification information
	public final static byte get_file_response = (byte)0x94; //Filedata OK response after binary info read from file
	public final static byte get_filelen_response = (byte)0x95; //FileSize OK response after filesize determined
	public final static byte get_firstfile_response = (byte)0x96; //FileName OK response with first file on SD Card
	public final static byte get_nextfile_response = (byte)0x97; //FileName OK response with next file on SD Card
	public final static byte delete_file_response = (byte)0x98; // - Ok response, file deleted
	public final static byte set_sync_master_response = (byte)0x99; // - Ok response, synchronised response operation enabled/disabled
	public final static byte set_trigger_mode_response = (byte)0x9A; // - Ok response, trigger mode
	public final static byte measurement_data = (byte)0xB0; // TimeStamp, Measurement data sent after MeasData measurement finished if Navg =1
	public final static byte measurement_avg_data = (byte)0xB1; // TimeStamp, Measurement data sent after MeasAvgData measurement finished if Navg > 1. TimeStamp is of the last measurement
	public final static byte meas_stored_to_ram = (byte)0xB2; // MeasCounter Message sent after Stored to Ram is finished
	public final static byte meas_avg_stored_to_ram = (byte)0xB3; // MeasCounter Message sent after Stored to Ram is finished if Navg > 1
	public final static byte meas_error_meas = (byte)0xB5; // // Message sent after error detected during measurement response changed

	public final static byte PACKET_TYPE_SOLLICITED = 0x20; //first byte of packet for command and responses
	public final static byte PACKET_TYPE_UNSOLLICITED = 0x21; //first byte of packet for spontaneous data from spectrometer
	
	public final static byte RS232_DLE = 0x10; //packet framing byte for RS232 (doubled if really there)
	public final static byte RS232_STX = 0x02;
	public final static byte RS232_ETX = 0x03;
	
	//RS232: DLE, STX, ...., DLE, ETX 
	
	public byte lastSequenceNumber = -1;
	
	private void sendPacket(byte command, byte commandData[]){
		int dataLen = (commandData == null) ? 2 : (commandData.length + 2);
		byte packetData[] = new byte[dataLen + 4];
		packetData[0] = PACKET_TYPE_SOLLICITED;
		packetData[1] = ++lastSequenceNumber;
		packetData[2] = (byte)(dataLen & 0x00FF);
		packetData[3] = (byte)((dataLen & 0xFF00) >> 8);
		//'data'...
		packetData[4] = command;
		packetData[5] = 0x00; //seq, or 'reserved'
		
		if(commandData != null)
			System.arraycopy(commandData, 0, packetData, 6, commandData.length);
		
		//send packetData
	}
			
	
	public class AS5126Identity {
		
	};
	
	private AS5126Identity getIdent() {
		sendPacket(get_ident, null);
		
		//wait for response
		
		//extract string 
		AS5126Identity id = new AS5126Identity();
		
		return id;
	}
	
	
	
	private byte[] getResponsePacket(){
		// ...
		byte packetData[] = null;
		
		
		//if(packetData[0] != PACKET_TYPE_SOLLICITED) ...
		//packetData[1] = 0x00   = reserved
		
		int dataLen = packetData[3] << 8 + packetData[2];
		//if(packetData[0] != 0x00
		byte sequenceNum = packetData[5]; 
		
		
		return null;
	}
}
