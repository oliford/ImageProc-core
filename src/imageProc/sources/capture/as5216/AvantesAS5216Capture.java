package imageProc.sources.capture.as5216;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.usb4java.Context;
import org.usb4java.DeviceHandle;
import org.usb4java.LibUsb;


import binaryMatrixFile.BinaryMatrixFile;
import as5216.AS5216;
import as5216.AvsIdentityType;
import as5216.DeviceConfigType;
import as5216.MeasConfigType;
import as5216.SonyMeasDataType;
import as5216.SonyMultiMeasDataType;
import as5216.SonySingleMeasDataType;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.sources.capture.as5216.swt.AS5216Driver;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;

/** The capture thread and interfacing with OmniDriver 
 * for using OceanOptics USB spectrometers.
 * 
 * open/close refers to the library being connect 
 * 
 *  */
public class AvantesAS5216Capture extends Capture implements Runnable {
	
	private AvantesAS5216Source source;
	
	private AvantesAS5216Config cfg;
	
	/** Thread copy of spectrometer index, in case someone changes it in the config */
	private int specIdx;
		
	/** Library is open and thread is running */
	private boolean libraryOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;

	private ByteBufferImage images[];

	/** List of spectrometers the library can access */
	private String spectrometerList[] = new String[0];
	
	public AvantesAS5216Capture(AvantesAS5216Source source) {
		this.source = source;
		setStatus(Status.notInitied, "init");
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera
	}
	
	public class AS5216Device {
		public DeviceHandle handle;
		public AvsIdentityType avs_id;
		public int numPixels;
		
	}
	private List<AS5216Device> openDevices = new ArrayList<AS5216Device>();

	private Context libUSBContext;

	private String[] openDeviceList = new String[0];
	
	@Override
	public void run() {
		threadBusy = true;
		
				
		libUSBContext = AS5216.createLibUSBContext();
		libraryOpen = true;
				
		try{
			initDevices();
			if(openDevices.size() == 0)
				throw new RuntimeException("No devices opened!");
						
			signalCapture = false;
			
			setStatus(Status.init, "Init Ready");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalCapture){
											
						signalCapture = false;
						
						syncConfig();
						
						setStatus(Status.init, "Allocating");		
						
						initImages();
							
						setStatus(Status.init, "Config+Alloc done.");
													
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("OmniDriver/config"));
						
						captureLoop();
						
						setStatus(Status.completeOK, "Capture done");
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.info("Aborted by signal");
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.info("Aborted by exception: " + err);
					setStatus(Status.errored, "Aborted by error: " + err);
					err.printStackTrace();
				}finally{
					signalCapture = false;					
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("OmniDriver thread caught Exception: " + err);
			setStatus(Status.errored, "Aborted/Errored: " + err );
			
		}finally{
			try{
				
				if(openDevices != null){
					Iterator<AS5216Device> it = openDevices.iterator();
					while(it.hasNext()){
						try{
							AS5216Device dev = it.next();
							LibUsb.resetDevice(dev.handle);
							LibUsb.close(dev.handle);
						}catch(Throwable err){
							err.printStackTrace();
						}
					    it.remove();
					}
					openDeviceList = new String[0];
				}
				
				LibUsb.exit(libUSBContext);
				libraryOpen = false;
				
			}catch(Throwable err){
				System.err.println("OmniDriver thread caught exception de-initing camera.");
				err.printStackTrace();
				setStatus(Status.errored, "Aborted/Errored: " + err);
			}
		}			
		
	}
	
	/** Unused, may need it later */
	/*private void releaseUnusedSpectrometers(Wrapper wrapper){
		//realease other spectrometers
		for(int i=0; i < spectrometerList.length; i++){
			if(i != cfg.spectrometerIndex && !spectrometerList[i].endsWith("[CLOSED]")){
				wrapper.closeSpectrometer(i);
				spectrometerList[i] += "[CLOSED]";
			}
		}
	}*/
	
	/** Initilize the library wrapper in whatever way is asked for */
	private void initDevices(){
		List<DeviceHandle> devices = AS5216.openAllDevices(libUSBContext);
		
		
		for(DeviceHandle dev : devices){
			try{
				AS5216Device asDev = new AS5216Device();
				asDev.handle = dev;
				asDev.avs_id = AS5216.getIdent(dev);
				openDevices.add(asDev);
				logr.info("Opened device " + asDev.avs_id.serialNumber);
			}catch(RuntimeException err){
				logr.info("Error getting ident from device " + dev);
			}
			
		}
		
		Collections.sort(openDevices, new Comparator<AS5216Device>() {
			@Override
			public int compare(AS5216Device o1, AS5216Device o2) {
				return o1.avs_id.serialNumber.compareTo(o2.avs_id.serialNumber);
			}
		});
	
		String deviceSerialNumbers[] = new String[openDevices.size()];
		for(int i=0; i < openDevices.size(); i++){
			AS5216Device dev = openDevices.get(i);
				
			deviceSerialNumbers[i] = dev.avs_id.serialNumber;
	 	}
		openDeviceList = deviceSerialNumbers;
	}
	
	
	private void syncConfig() {
		
		//request all the info
		
		cfg.deviceSerialNumbers = new String[openDevices.size()];
		cfg.numPixels = new int[openDevices.size()];
		
		for(int i=0; i < openDevices.size(); i++){
			AS5216Device dev = openDevices.get(i);
			
			DeviceConfigType devcon = AS5216.getDeviceConfigType(dev.handle);
			
			dev.numPixels = devcon.getDetectorNumPixels();
			
			cfg.numPixels[i] = dev.numPixels;
			cfg.deviceSerialNumbers[i] = dev.avs_id.serialNumber;
	
			
		}
	    
	}
	
	private void initImages() {
		
		int width = 0;
		int height = 0;
		
		
		for(AS5216Device dev : openDevices){
			if(dev.numPixels > width)
				width = dev.numPixels;
			height++;
		}
		
		cfg.imageWidth = width;
		cfg.imageHeight = height;
		
		int bytesPerPixel = Short.BYTES;
		int bitDepth = 16; 
		long imgDataSize = width * height * bytesPerPixel;
			
		int footerSize = 0;
		
		if(imgDataSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		try{
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				System.out.println("initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				System.out.println("initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			System.out.println("initImages() Not enough memory for images.");
			bulkAlloc.destroy();			throw new RuntimeException("Not enough memory for image capture", err);
		}
	}

	private void captureLoop() {
		setStatus(Status.init, "Getting wavelength calibration");
		double wavelen[][] = new double[cfg.imageHeight][];
		
		for(int i=0; i < openDevices.size(); i++){
			AS5216Device dev = openDevices.get(i);
			wavelen[i] = AS5216.getCalibration(dev.handle);
			if(wavelen[i].length != cfg.imageWidth)
				wavelen[i] = Arrays.copyOf(wavelen[i], cfg.imageWidth);
		}
		
		
		
		source.setSeriesMetaData("wavelength", wavelen, false);
				
		setStatus(Status.awaitingSoftwareTrigger, "Awaiting acquire enable. specIdx=" +specIdx);		
		
		try{
		
			for(AS5216Device dev : openDevices){
				MeasConfigType measData = new MeasConfigType();	
				
			    measData.m_StartPixel = 0;
				measData.m_StopPixel = (short)(dev.numPixels - 1);	    
			    measData.m_IntegrationTime = cfg.integrationTimeMS;
			    measData.m_IntegrationDelay = cfg.delayTimeMS; 
			    measData.m_NrAverages = cfg.numberOfAverages;
			    measData.m_CorDynDark_Enable = 0;
			    measData.m_CorDynDark_ForgetPercentage = 0;
			    measData.m_Smoothing_SmoothPix = 0;
			    measData.m_Smoothing_SmoothModel = 0;
			    measData.m_SaturationDetection = 0;
			    measData.m_Trigger_Mode = 0;
			    measData.m_Trigger_Source = 0;
			    measData.m_Trigger_SourceType = 0;
			    measData.m_Control_StrobeControl = 0;
			    measData.m_Control_LaserDelay = 0;
			    measData.m_Control_LaserWidth = 0;
			    measData.m_Control_LaserWaveLength = 0;
			    measData.m_Control_StoreToRam = 0;
			    
			    AS5216.prepareMeasurement(dev.handle, measData);
			}
			
			
		
			double time[] = new double[cfg.nImagesToAllocate]; //seconds since first image
			long wallRelNanoTime[] = new long[cfg.nImagesToAllocate];
			long nanoTime[] = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("OmniDriver/wallRelativeTimeNS", wallRelNanoTime, true);
			source.setSeriesMetaData("OmniDriver/nanoTime", nanoTime, true);
			source.setSeriesMetaData("OmniDriver/time", time, true);
			
			int bytesPerPixel = images[0].getBytesPerPixel();
			int nextImgIdx = 0;
			long firstRelNanoTimeNS = Long.MIN_VALUE;
			long firstWallCopyTimeMS = Long.MIN_VALUE;
			do{
				
				try {
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try{
						images[nextImgIdx].invalidate(false);
						
						if(!isAcqusitionStarted()){ 
							//we don't actually do anything to the spectrometer here
							//it's just that we now start requesting the spectra
						
							while(!signalAcquireStart){
								Thread.sleep(0, 100000);
							}
							
							for(AS5216Device dev : openDevices){
								AS5216.startMeasurement(dev.handle, (short)cfg.nImagesToAllocate);
							}
							
								
							setStatus(Status.awaitingHardwareTrigger, "Acquire started. Awaiting first frame");
						}
						
						ByteBuffer buff = images[nextImgIdx].getWritableBuffer(writeLock);												
						
						int request_size = (cfg.numberOfAverages <= 1) ? SonySingleMeasDataType.sizeOf() : SonyMultiMeasDataType.sizeOf();
						
						
						for(int i=0; i < openDevices.size(); i++){
							AS5216Device dev = openDevices.get(i);							
							
							SonyMeasDataType sony_meas = AS5216.getMeasurement(dev.handle, request_size);
								
							if(sony_meas instanceof SonyMultiMeasDataType && ((SonyMultiMeasDataType)sony_meas).getAverages() != cfg.numberOfAverages) 
								throw new RuntimeException("Error in Number of Averages ["+dev.avs_id.serialNumber+"]");
								
							int timestamp = sony_meas.getTimestamp();
							spinLog("Timestamp["+dev.avs_id.serialNumber+"] = " + sony_meas.getTimestamp());
								
							buff.position(i * cfg.imageWidth * bytesPerPixel);
							// sony_meas.transferPixels(buff);
							ShortBuffer sBuff = buff.asShortBuffer();
							short[] s = sony_meas.getPixels();
							sBuff.put(s);
							
							AS5216.ackMeasurement(dev.handle);				
						}
						
						
						wallRelNanoTime[nextImgIdx] = System.nanoTime();
						if(firstWallCopyTimeMS == Long.MIN_VALUE){
							firstWallCopyTimeMS = System.currentTimeMillis(); //only have this accurate to ms
							firstRelNanoTimeNS = wallRelNanoTime[nextImgIdx];
						}
						
						setStatus(Status.capturing, "Capturing.");
												
						
					}finally{ writeLock.unlock(); }
				} catch (InterruptedException e) {
					System.out.println("Interrupted while waiting to write to java image.");
					return;
				}
				
				//best we can do for timing is using wall times
				//relative: wall nano time (probably better than microsec precision)
				//absolute: wall milisecond time
				nanoTime[nextImgIdx] = wallRelNanoTime[nextImgIdx] - firstRelNanoTimeNS + firstWallCopyTimeMS*1_000_000L;
				time[nextImgIdx] = (double)(wallRelNanoTime[nextImgIdx] /1000L - firstRelNanoTimeNS/1000L) / 1e6;
				source.imageCaptured(nextImgIdx);
				
				nextImgIdx++;
				if(nextImgIdx >= cfg.nImagesToAllocate && cfg.wrap)
					nextImgIdx = 0; //cfg.blackLevelFirstFrame ? 1 : 0;	
			
			}while(nextImgIdx < cfg.nImagesToAllocate);
			
		}finally{
			//stop acquiring ... nothing todo
			for(AS5216Device dev : openDevices){
				AS5216.stopMeasurement(dev.handle);
			}
		}
	}
	
	/** Start the camera thread and open the camera */
	public void initLibrary() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		//thread.setPriority(Thread.MAX_PRIORITY);
		spinLog("Starting OmniDriver capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeLibrary(true)));
	}
	
	/** Close the camera and kill the thread completely */
	public void closeLibrary(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("AndorCamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("AndorCamCapture: Thread died.");					
			}	
		}
	}
	
	/** Signal the thread to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
		
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				System.out.println("OmniDriverCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("OmniDriverCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(AvantesAS5216Config cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	
	public void destroy() { closeLibrary(false);	}

	public boolean isOpen(){ return thread != null && thread.isAlive() && libraryOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }

	public void setConfig(AvantesAS5216Config config) {
		this.cfg = config;
	}

	public String[] getOpenDevices() { return openDeviceList ; }
}
