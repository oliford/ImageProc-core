package imageProc.sources.capture.andorV2;

import java.util.HashMap;
import java.util.Map;

import andor2JNI.AndorV2Capabilities;
import andor2JNI.AndorV2Defs;
import andor2JNI.AndorV2ROIs;
import andor2JNI.AndorV2ROIs.AndorV2ROI;
import imageProc.sources.capture.base.CaptureConfig;

/** Configuration for Andor SDK2 (non-CMOS) cameras
 *  
 */
public class AndorV2Config extends CaptureConfig {

	/** General stuff that's outside of the camera's own config */
	
	/** Number of images to allocate and to capture 
	 * if in non-continuous mode */	 
	public int nImagesToAllocate = 100;
	
	/** If not -1, only open camera with given S/N */
	public int selectSerialNumber = -1;
	
	/** Serial number of camera opened */
	public int cameraSerialNumber = -1;

	/** Index of camera that was opened*/
	public int cameraIndex = 0;
	
	public String headModel;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;
		
	/** Timeout for WaitForAcquisitionUpdate() call */
	public int grabberTimeoutMS = 100;
	
	/** Size of circular buffer of images read from camera */
	public int sizeOfCircularBuffer = -1;
	
	public long waitTimeout = 1000;
		
	/** One of  AndorV2Defs.READMODE_*
	 * 0 Full Vertical Binning
		1 Multi-Track
		2 Random-Track
		3 Single-Track
		4 Image
		*/
	public int readMode = AndorV2Defs.READMODE_IMAGE;
	
	public boolean frameTransferMode;
		
	/** One of AndorV2Defs.ACQUSITIONMODE_*
		1 Single Scan
		2 Accumulate
		3 Kinetics
		4 Fast Kinetics
		5 Run till abort
	 */	
	public int acqusitionMode = AndorV2Defs.ACQUSITIONMODE_KINETICS;
	
	/** Number of accumulations for accumlating modes */
	public int nAccumulations = 1;
	
	/** Kinetic cycle time for accumulation/kinetic modes (i.e. frame Time) */
	public float accumulationCycleTimeSecs = 0;
	
	/** Number of kinetic scans, (e.g. number of images) */
	public int nKineticScans = 100;
	
	/** Kinetic cycle time for kinetic modes (i.e. frame Time) */
	public float kineticCycleTimeSecs = (float)0.000;
	
	/** Exposure time in seconds */
	public float exposureTimeSecs = (float)0.050;
	
	public float maximumExposureTime = Float.NaN;
	
	public int detectorWidth;
	
	public int detectorHeight;
	
	/** One of AndorV2Defs.SHUTTERTYPE_*
		0 Output TTL low signal to open shutter
		1 Output TTL high signal to open shutter
	*/
	public int shutterType = AndorV2Defs.SHUTTERTYPE_OPEN_HIGH; 
	
	/** One of AndorV2Defs.SHUTTERMODE_*
		0 Fully Auto
		1 Permanently Open
		2 Permanently Closed
		4 Open for FVB series
		5 Open for any series
	*/
	public int shutterMode = AndorV2Defs.SHUTTERMODE_OPEN;
	
	public int shutterClosingTime;
	
	public int shutterOpeningTime;
	
	public int outputAmplifier = AndorV2Defs.OUTPUTAMPLIFIER_EMCCD;
	
	
	/** Horizontal binning */
	public int imageModeHBin = 1;
	
	/** Vertical binning */
	public int imageModeVBin = 1;
	
	/** Appears to be 1 based */
	public int imageModeHStart = 1;
	
	/** Inclusive. Appears to be 1 based. Default not valid. */
	public int imageModeHEnd = -1;	
	
	/** Appears to be 1 based */
	public int imageModeVStart = 1;
	
	/** Inclusive. Appears to be 1 based. Default not valid. */
	public int imageModeVEnd = -1;
	
	/** Final derived image width and height */
	public int imageWidth = -1, imageHeight = -1;
	
	/** Available and selected horizontal shift speeds */
	public float horizShiftSpeeds[];
	public int horizShiftSpeedIndex = 0;
	
	/** Available and selected vertical shift speeds */
	public float vertShiftSpeeds[];
	public int vertShiftSpeedIndex = 0;
	
	/** Available and selected vertical shift amplitudes */
	public String vertShiftAmplitudes[];
	public int vertShiftAmplitudeIndex = 0;
	
	/** Available and selected preAmp gain */
	public float preAmpGains[];
	public int preAmpGainIndex = 0;	
	
	/** EMCCD gain */	
	public int emccdGain = 0;	
	public int emccdGainMode = 0;
	public int emccdGainRange[];

	/** Cooling and temperature */ 
	public int targetTemp = 20; //safe
	public boolean coolerOn = false;	
	public int minTemp;
	public int maxTemp;	
	public float currentTemp;
	
	/** One of AndorV2Defs.DRV_TEMP_ */
	public int tempControlStatus = -1;
	
	/** Metadata (timestamps) */
	public boolean enableMetaData = true;
	
	/** Use */
	public boolean useWallStartTime = true;
	
	
	/** Trigger mode. One of AndorV2Defs.TRIGGERMODE_* */
	public int triggerMode = AndorV2Defs.TRIGGERMODE_INTERNAL;
		
	public AndorV2Config() {		
		cameraIndex = 0;
		nImagesToAllocate = 20;
		blackLevelFirstFrame = false;
	}
	
	/** Enable setting of all ROIs. When disabled the ROI configuration is kept in this class
	 * but not written to the camera */	
	//public boolean enableROIs;
	public void setEnableROIs(boolean enable){
		readMode = enable ? AndorV2Defs.READMODE_RANDOM_TRACK : AndorV2Defs.READMODE_IMAGE;
	}
	
	public boolean isEnabledROIs(){
		return readMode != AndorV2Defs.READMODE_IMAGE;
	}
	
	public AndorV2ROIs rois;

	public int imageSizeBytes(){ return imageWidth*imageHeight*capabilities.getBitDepth()/8; }
	
	public double exposureTimeMS() { return exposureTimeSecs * 1000; }
	
	/** Creates a simple HashMap of java number/string objects etc */
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
			
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/selectSerialNumber", selectSerialNumber);
		map.put(prefix + "/cameraSerialNumber", cameraSerialNumber);
		map.put(prefix + "/cameraIndex", cameraIndex);
		map.put(prefix + "/headModel", headModel);
		map.put(prefix + "/blackLevelFirstFrame", blackLevelFirstFrame);
		map.put(prefix + "/grabberTimeoutMS", grabberTimeoutMS);
		map.put(prefix + "/sizeOfCircularBuffer", sizeOfCircularBuffer);
		map.put(prefix + "/waitTimeout", waitTimeout);
		map.put(prefix + "/readMode", readMode);
		map.put(prefix + "/frameTransferMode", frameTransferMode);
		map.put(prefix + "/acqusitionMode", acqusitionMode);
		map.put(prefix + "/nAccumulations", nAccumulations);
		map.put(prefix + "/accumulationCycleTime", accumulationCycleTimeSecs);
		map.put(prefix + "/nKineticScans", nKineticScans);
		map.put(prefix + "/kineticCycleTime", kineticCycleTimeSecs);
		map.put(prefix + "/exposureTime", exposureTimeSecs);
		map.put(prefix + "/maximumExposureTime", maximumExposureTime);
		map.put(prefix + "/detectorWidth", detectorWidth);
		map.put(prefix + "/detectorHeight", detectorHeight);
		map.put(prefix + "/shutterType", shutterType);
		map.put(prefix + "/shutterMode", shutterMode);
		map.put(prefix + "/shutterClosingTime", shutterClosingTime);
		map.put(prefix + "/shutterOpeningTime", shutterOpeningTime);
		map.put(prefix + "/outputAmplifier", outputAmplifier);
		map.put(prefix + "/imageModeHBin", imageModeHBin);
		map.put(prefix + "/imageModeVBin", imageModeVBin);
		map.put(prefix + "/imageModeHStart", imageModeHStart);
		map.put(prefix + "/imageModeHEnd", imageModeHEnd);
		map.put(prefix + "/imageModeVStart", imageModeVStart);
		map.put(prefix + "/imageModeVEnd", imageModeVEnd);
		map.put(prefix + "/imageWidth", imageWidth);
		map.put(prefix + "/imageHeight", imageHeight);
		map.put(prefix + "/horizShiftSpeedIndex", horizShiftSpeedIndex);
		map.put(prefix + "/vertShiftSpeedIndex", vertShiftSpeedIndex);
		map.put(prefix + "/vertShiftAmplitudeIndex", vertShiftAmplitudeIndex);
		map.put(prefix + "/preAmpGainIndex", preAmpGainIndex);
		map.put(prefix + "/emccdGain", emccdGain);
		map.put(prefix + "/emccdGainMode", emccdGainMode);
		map.put(prefix + "/coolerOn", coolerOn);
		map.put(prefix + "/minTemp", minTemp);
		map.put(prefix + "/maxTemp", maxTemp);
		map.put(prefix + "/currentTemp", currentTemp);
		map.put(prefix + "/tempControlStatus", tempControlStatus);
		map.put(prefix + "/enableMetaData", enableMetaData);
		map.put(prefix + "/triggerMode", triggerMode);
		map.put(prefix + "/useWallStartTime", useWallStartTime);
				
		rois.addArraysToMap(map, prefix + "/Rois_arrays" , true);
		return map;
	}
	
	public boolean isContinuous() { return acqusitionMode == AndorV2Defs.ACQUSITIONMODE_RUN_TILL_ABORT; }
	
	public void setContinuous(boolean enable){
		acqusitionMode = enable ? AndorV2Defs.ACQUSITIONMODE_RUN_TILL_ABORT : AndorV2Defs.ACQUSITIONMODE_KINETICS;
	}


	public AndorV2Capabilities capabilities;

	/** Only enumerate devices on given USB bus (requires hacked libusb) */
	public int singleUSBBus = -1;
	
	/** Only enumerate devices on given USB port of each bus (requires hacked libusb) */
	public String singleUSBPort = null;

	public AndorV2ROIs getROIs() {
		if(rois == null)
			rois = new AndorV2ROIs();
		return rois;
	}

	public void jogROI(AndorV2ROI roi, int dx, int dy) {
		roi.x += dx;
		roi.y += dy;
	}
}
