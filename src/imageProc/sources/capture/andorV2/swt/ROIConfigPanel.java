package imageProc.sources.capture.andorV2.swt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import andor2JNI.AndorV2ROIs;
import andor2JNI.AndorV2ROIs.AndorV2ROI;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import imageProc.sources.capture.andorV2.AndorV2Config;
import imageProc.sources.capture.andorV2.AndorV2Source;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class ROIConfigPanel {
	
	private final static String colNames[] = new String[]{ "-", "Name", "Enable", "X0", "Width", "X bin", "Y0", "Height", "Y bin" };			
	
	private Table table;
	private TableEditor tableEditor;
	private TableItem tableItemEditing = null;
	private Text tableEditBox = null;	
	
	private AndorV2Source source;
	private AndorV2SWTControl controller;
	private Composite swtGroup;
	
	private Button roiEnableROIs;
	private Button setPositionsCheckbox;

	private Button jogLeftButton;
	private Button jogRightButton;
	private Button jogUpButton;
	private Button jogDownButton;
	
	public ROIConfigPanel(Composite parent, int style, AndorV2Source source, AndorV2SWTControl parentController) {
		this.source = source;
		this.controller = parentController;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		roiEnableROIs = new Button(swtGroup, SWT.CHECK);
		roiEnableROIs.setText("Enable ROIs - Disable and capture to visualise/edit ROIs on image.");
		roiEnableROIs.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		roiEnableROIs.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { roiConfigEvent(event); } });
		
		
		Group swtJogGroup = new Group(swtGroup, SWT.BORDER);
		swtJogGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 2));
		swtJogGroup.setLayout(new GridLayout(3, false));
		
		Label lJA = new Label(swtJogGroup, SWT.NONE);
		lJA.setText("Jog:");
		lJA.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		jogUpButton = new Button(swtJogGroup, SWT.PUSH);
		jogUpButton.setText("^");
		jogUpButton.setToolTipText("Jog all or selected ROIs up 1 pixel");
		jogUpButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogUpButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, 0, -1); } });
		
		Label lJB = new Label(swtJogGroup, SWT.NONE);
		lJB.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		jogLeftButton = new Button(swtJogGroup, SWT.PUSH);
		jogLeftButton.setText("<");
		jogLeftButton.setToolTipText("Jog all or selected ROIs left 1 pixel");
		jogLeftButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogLeftButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, -1, 0); } });

		jogDownButton = new Button(swtJogGroup, SWT.PUSH);
		jogDownButton.setText("v");
		jogDownButton.setToolTipText("Jog all or selected ROIs down 1 pixel");
		jogDownButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogDownButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, 0, +1); } });

		jogRightButton = new Button(swtJogGroup, SWT.PUSH);
		jogRightButton.setText(">");
		jogRightButton.setToolTipText("Jog all or selected ROIs right 1 pixel");
		jogRightButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogRightButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, +1, 0); } });

		setPositionsCheckbox = new Button(swtGroup, SWT.CHECK);
		setPositionsCheckbox.setText("Enable ROI drawing - Select entry and draw box on full-frame image");
		setPositionsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		
		table = new Table(swtGroup, SWT.BORDER | SWT.MULTI);				
		//featuresTable.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, true, 5, 0));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);	
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(table, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]);
		}
		
		configToGUI();
		
		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		table.addListener(SWT.MouseDoubleClick, new Listener() {
			@Override
			public void handleEvent(Event event) {
				configToGUI();
				for (int i = 0; i < colNames.length; i++)
					cols[i].pack();
			}
		});
		
		tableEditor = new TableEditor(table);
		tableEditor.horizontalAlignment = SWT.LEFT;
		tableEditor.grabHorizontal = true;
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		table.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		table.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ImageProcUtil.redrawImagePanel(source); } });
		
		swtGroup.pack();
	}

	/**
	 * Copied from 'Snippet123' Copyright (c) 2000, 2004 IBM Corporation and
	 * others. [ org/eclipse/swt/snippets/Snippet124.java ]
	 */
	private void tableMouseDownEvent(Event event) {
		
		Rectangle clientArea = table.getClientArea();
		Point pt = new Point(clientArea.x + event.x, event.y);
		int index = table.getTopIndex();
		while (index < table.getItemCount()) {
			boolean visible = false;
			final TableItem item = table.getItem(index);
			for (int i = 0; i < table.getColumnCount(); i++) {
				Rectangle rect = item.getBounds(i);
				if (rect.contains(pt)) {
					final int column = i;
					if (column == 0) {
						// pointsTable.setSelection(index); //make sure its
						// selected
						controller.generalControllerUpdate();
						controller.forceImageRedraw();
						return;
						
					}else if(column == 2){ //enable, just toggles
						AndorV2ROI roi = source.getConfig().getROIs().getROI(item.getText(1));
						if(roi != null){
							roi.enabled = !roi.enabled;
							item.setText(2, roi.enabled ? "Y" : "");
						}
						return;
					}
					
					if (tableEditBox != null) {
						tableEditBox.dispose(); // oops, one left over
					}
					tableEditBox = new Text(table.getParent(), SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener() {
						public void handleEvent(final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, tableEditBox.getText());
								tableEditBox.dispose();
								tableEditBox = null;
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, tableEditBox.getText());
									// FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									tableEditBox.dispose();
									tableEditBox = null;
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					tableEditBox.addListener(SWT.FocusOut, textListener);
					tableEditBox.addListener(SWT.Traverse, textListener);
					tableEditor.setEditor(tableEditBox, item, i);
					tableEditBox.setText(item.getText(i));
					tableEditBox.selectAll();
					tableEditBox.setFocus();
					// hacks for SWT4.4 (under linux GTK at least)
					// Textbox won't display if it's a child of the table
					// so we make it a child of the table's parent, but now need
					// to adjust the location
					{
						tableEditBox.moveAbove(table);
						final Point p0 = tableEditBox.getLocation();
						Point p1 = table.getLocation();
						p0.x += p1.x - clientArea.x;
						p0.y += p1.y;// + editBox.getSize().y;
						// p0.x = pt.x + p1.x;
						// p0.y = pt.y + p1.y;
						tableEditBox.setLocation(p0);
						tableEditBox.addListener(SWT.Move, new Listener() {
							@Override
							public void handleEvent(Event event) {
								tableEditBox.setLocation(p0); // TableEditor keeps
															// moving it to
															// relative to the
															// table, so move it
															// back
							}
						});
					}
					return;
				}
				if (!visible && rect.intersects(clientArea)) {
					visible = true;
				}
			}
			if (!visible)
				return;
			index++;
		}
	}

	private void tableColumnModified(TableItem item, int column, String text) {

		AndorV2Config config = source.getConfig();
		AndorV2ROIs rois = config.getROIs();
		
		synchronized (rois) {
			if (item.isDisposed())
				return;

			String roiName = item.getText(1);

			AndorV2ROI roi = (roiName.length() > 0) ? rois.getROI(roiName) : null;
			if(roi == null && column != 1){
				//somethings broken. Queue an update
				controller.generalControllerUpdate();
				return;
			}
			
			switch (column) {
				case 1: //name
	
					if (roi == null && (roiName.length() <= 0 || roiName.equals("<new>"))) { 
						roi = new AndorV2ROI();
						roi.name = text;
						rois.addROI(roi);
	
					} else if (text.length() <= 0) { // deleting point
						rois.remove(roi);
	
					} else { // just changing name
						roi.name = text;
					}
	
					item.setText(column, text);
	
					break;
	
				case 3: roi.x = Algorithms.mustParseInt(text); item.setText(3, Integer.toString(roi.x)); break;
				case 4: roi.width = Algorithms.mustParseInt(text); item.setText(4, Integer.toString(roi.width)); break;
				case 5: roi.x_binning = Algorithms.mustParseInt(text); item.setText(5, Integer.toString(roi.x_binning)); break;
				
				case 6: roi.y = Algorithms.mustParseInt(text); item.setText(6, Integer.toString(roi.y)); break;
				case 7: roi.height = Algorithms.mustParseInt(text); item.setText(7, Integer.toString(roi.height)); break;
				case 8: roi.y_binning = Algorithms.mustParseInt(text); item.setText(8, Integer.toString(roi.y_binning)); break;
				
			}

			TableItem lastItem = table.getItem(table.getItemCount() - 1);
			if (!lastItem.getText(1).equals("<new>")) {
				TableItem blankItem = new TableItem(table, SWT.NONE);
				blankItem.setText(0, "o");
				blankItem.setText(1, "<new>");
			}

		}
	}

	
	
	void configToGUI(){
		AndorV2Config config = source.getConfig();
		roiEnableROIs.setSelection(config.isEnabledROIs());
				
		AndorV2ROIs rois = config.getROIs();
		
		ArrayList<AndorV2ROI> roiList;
		synchronized (rois) {
			roiList = rois.getROIListCopy();
		}

		/*Collections.sort(roiList, new Comparator<PICamROI>() {
			/*@Override
			public int compare(PICamROI r1, PICamROI r2) {
				return Integer.compare(r1.y, r2.y);
			}
			@Override
			public int compare(PICamROI r1, PICamROI r2) {
				return r1.name.compareTo(r2.name);
			}
			
		});//*/


		int selected[] = table.getSelectionIndices();

		// use existing table entries, so it doesnt scroll around
		for (int i = 0; i < roiList.size(); i++) {
			AndorV2ROI roi = roiList.get(i);
			
			if (roi == null || roi.name == null || roi.name.length() <= 0)
				continue;

			TableItem item;
			if (i < table.getItemCount())
				item = table.getItem(i);
			else
				item = new TableItem(table, SWT.NONE);

			item.setText(0, "o");
			item.setText(1, roi.name);
			item.setText(2, roi.enabled ? "Y" : "");

			item.setText(3, Integer.toString(roi.x));
			item.setText(4, Integer.toString(roi.width));
			item.setText(5, Integer.toString(roi.x_binning));

			item.setText(6, Integer.toString(roi.y));
			item.setText(7, Integer.toString(roi.height));
			item.setText(8, Integer.toString(roi.y_binning));

		}

		while (table.getItemCount() > roiList.size()) {
			table.remove(roiList.size());
		}

		// and finally a blank item for creating new point
		TableItem blankItem = new TableItem(table, SWT.NONE);
		blankItem.setText(0, "o");
		blankItem.setText(1, "<new>");


	}
	
	protected void jogButtonEvent(Event event, int dx, int dy) {
		
		AndorV2Config config = source.getConfig();
		TableItem[] selectedItems = table.getSelection();
		if(selectedItems != null && selectedItems.length >=1) {
			for (TableItem item : selectedItems) {
				
				AndorV2ROI roi = config.getROIs().getROI(item.getText(1));
				if(roi != null)
					config.jogROI(roi, dx, dy);
			}
		}else {
			for(AndorV2ROI roi : config.getROIs().getROIListCopy()) {
				config.jogROI(roi, dx, dy);				
			}
		}
		configToGUI();
	}

	private void roiConfigEvent(Event event) {
		AndorV2Config config = source.getConfig();
		config.setEnableROIs(roiEnableROIs.getSelection());		
	}
	
	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }

	public void drawOnImage(GC gc, double[] scale, int imageWidth, int imageHeight, boolean asSource) {
		if (swtGroup.isDisposed() || !setPositionsCheckbox.getSelection())
			return;

		AndorV2Config config = source.getConfig();
				
		//must be full image to draw/select ROIs
		if(imageWidth != config.detectorWidth
				|| imageHeight != config.detectorHeight){	
			return;
		}
		
		AndorV2ROIs rois = config.getROIs();
		List<AndorV2ROI> roiList = rois.getROIListCopy();
		
		int i = 0;
		for (AndorV2ROI roi : roiList) {
			if(roi == null || roi.name == null)
				continue;

			TableItem[] selectedItems = table.getSelection();
			boolean isSelected = false;
			for (TableItem item : selectedItems) {
				if (item.getText(1).equals(roi.name))
					isSelected = true;
			}
			if (isSelected) {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_MAGENTA));
				gc.setLineWidth(3);
			} else {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
				gc.setLineWidth(1);
			}

			int boxCoords[] = new int[]{ roi.x, roi.y, roi.x + roi.width, roi.y + roi.height };

			if (boxCoords[0] >= 0 && boxCoords[1] >= 0 && boxCoords[2] <= imageWidth && boxCoords[3] <= imageHeight
					&& boxCoords[2] > boxCoords[0] && boxCoords[3] > boxCoords[1]) {

				gc.drawRectangle((int) (boxCoords[0] * scale[0]), (int) (boxCoords[1] * scale[1]),
						(int) ((boxCoords[2] - boxCoords[0]) * scale[0]),
						(int) ((boxCoords[3] - boxCoords[1]) * scale[1]));
			}

			i++;
		}

	}

	public void setRect(int x0, int y0, int width, int height) {		
		if (swtGroup.isDisposed() || !setPositionsCheckbox.getSelection())
			return;

		AndorV2Config config = source.getConfig();
		
		//must be full image to draw/select ROIs
		int imageWidth = source.getImage(0).getWidth();
		int imageHeight = source.getImage(0).getHeight(); 
		if(imageWidth != config.detectorWidth
				|| imageHeight != config.detectorHeight){	
			return;
		}
		
		
		TableItem[] selected = table.getSelection();
		if (selected.length == 1) {
			String roiName = selected[0].getText(1);

			AndorV2ROI roi = config.getROIs().getROI(roiName);
			
			if(roi.x_binning <= 0) roi.x_binning = 1;
			if(roi.y_binning <= 0) roi.y_binning = 1;

			roi.x = x0;
			roi.width = (int)(width / roi.x_binning) * roi.x_binning;
			
			roi.y = y0;
			roi.height = (int)(height / roi.y_binning) * roi.y_binning;
			
			source.configChanged();
		}
	}
	
}
