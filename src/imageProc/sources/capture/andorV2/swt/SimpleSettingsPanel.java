package imageProc.sources.capture.andorV2.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import andor2JNI.AndorV2;
import andor2JNI.AndorV2Defs;
import imageProc.core.ImgSource;
import imageProc.sources.capture.andorV2.AndorV2Config;
import imageProc.sources.capture.andorV2.AndorV2Source;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private AndorV2Source source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo triggerModeCombo;	
		
	private Combo preAmpGainCombo;	
	private Label emccdGainLabel;
	private Text emccdGainTextbox;
	private Combo emccdGainModeCombo;
	
	private Combo hsSpeedCombo;	
	private Combo vsSpeedCombo;	
	private Combo vsSpeedAmplitudeCombo;
	
	private Button coolingCheckbox;
	private Spinner coolingSetTempSpinner;
	private Label coolingCurrentTempLabel;

	private Button shutterTTLCheckbox;
	private Combo shutterModeCombo;
	
	private Button frameTransferCheckbox;
	private Button enableMetadataCheckbox;
	private Combo readModeCombo;
	private Combo acquisitionModeCombo;
		
	private Text exposureLengthTextbox;
	//private Button exposureMinButton; 
	private Button exposureMaxButton;

	private Text kineticCycleTimeTextbox;
	private Spinner numAccumulationsSpinner;
	private Text accumulateCycleTimeTextbox;

	private Button unsetAllButton; 
	private Button defaultAllButton; 
	
	public SimpleSettingsPanel(Composite parent, int style, AndorV2Source source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { continuousCheckboxEvent(event); } });

		Label lST = new Label(swtGroup, SWT.NONE); lST.setText("Cooling: Set:");
		coolingSetTempSpinner = new Spinner(swtGroup, SWT.CHECK);
		coolingSetTempSpinner.setValues(25, -100, 100, 0, 1, 10);
		coolingSetTempSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		coolingSetTempSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		coolingCheckbox = new Button(swtGroup, SWT.CHECK);
		coolingCheckbox.setText("On");
		coolingCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		coolingCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
				
		coolingCurrentTempLabel = new Label(swtGroup, SWT.NONE);
		coolingCurrentTempLabel.setText("Current: ????????");
		coolingCurrentTempLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));

		Label lSM = new Label(swtGroup, SWT.NONE); lSM.setText("Shutter:");
		shutterModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		shutterModeCombo.setItems(AndorV2Defs.shutterModes);
		shutterModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		shutterModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		shutterTTLCheckbox = new Button(swtGroup, SWT.CHECK);
		shutterTTLCheckbox.setText("Active Low (Open)");		
		shutterTTLCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		shutterTTLCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lFT = new Label(swtGroup, SWT.NONE); lFT.setText("");
		frameTransferCheckbox = new Button(swtGroup, SWT.CHECK);
		frameTransferCheckbox.setText("Frame transfer");
		frameTransferCheckbox.setToolTipText("This function will set whether an acquisition will readout in Frame Transfer Mode. If the acquisition mode is Single Scan or Fast Kinetics this call will have no affect.");		
		frameTransferCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		frameTransferCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		enableMetadataCheckbox = new Button(swtGroup, SWT.CHECK);
		enableMetadataCheckbox.setText("Enable metadata (timestamps)");
		enableMetadataCheckbox.setToolTipText("Enables storage of metadata timestamps in camera, which are then used by this program to produce 'time' and 'nanoTime' arrays.");
		enableMetadataCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		enableMetadataCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
				
		Label lRM = new Label(swtGroup, SWT.NONE); lRM.setText("Read mode:");
		readModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		readModeCombo.setItems(AndorV2Defs.readModes);
		readModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		readModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lAM = new Label(swtGroup, SWT.NONE); lAM.setText("Acquisition mode:");
		acquisitionModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		acquisitionModeCombo.setItems(AndorV2Defs.acqusitionModes);
		acquisitionModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		acquisitionModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
				
		Label lTM = new Label(swtGroup, SWT.NONE); lTM.setText("Trigger mode:");
		triggerModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		triggerModeCombo.setItems(AndorV2Defs.triggerModes);
		triggerModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		triggerModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lPAG = new Label(swtGroup, SWT.NONE); lPAG.setText("PreAmp Gain:");
		preAmpGainCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		preAmpGainCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		preAmpGainCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		emccdGainLabel = new Label(swtGroup, SWT.NONE); emccdGainLabel.setText("EMCCD Gain (?? - ??):");
		emccdGainTextbox = new Text(swtGroup, SWT.NONE);
		emccdGainTextbox.setToolTipText("Allows the user to change the gain value. The valid range for the gain depends on what gain mode the camera is operating in. See SetEMGainMode to set the mode and GetEMGainRange to get the valid range to work with. To access higher gain values (>x300) see SetEMAdvanced.");
		emccdGainTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		emccdGainTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lEMGM = new Label(swtGroup, SWT.NONE); lEMGM.setText("Mode:");
		emccdGainModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		emccdGainModeCombo.setItems(AndorV2Defs.emccdGainModes);
		emccdGainModeCombo.setToolTipText("Set the EM Gain mode to one of the following possible settings. Mode 0: The EM Gain is controlled by DAC settings in the range 0-255. Default mode. 1: The EM Gain is controlled by DAC settings in the range 0-4095. 2: Linear mode. 3: Real EM gain.");
		emccdGainModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		emccdGainModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lHSS = new Label(swtGroup, SWT.NONE); lHSS.setText("Speeds: Horiz:");
		hsSpeedCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		hsSpeedCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		hsSpeedCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		hsSpeedCombo.setToolTipText("The speed at which the pixels are shifted into the output node during the readout phase of an acquisition. Typically your camera will be capable of operating at several horizontal shift speeds. To get the actual speed that an index corresponds to use the GetHSSpeed function.");
		
		Label lVSS = new Label(swtGroup, SWT.NONE); lVSS.setText("Vert:");
		vsSpeedCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		vsSpeedCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		vsSpeedCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lVSA = new Label(swtGroup, SWT.NONE); lVSA.setText("Vert Shfit Amplitude:");
		vsSpeedAmplitudeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		vsSpeedAmplitudeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		vsSpeedAmplitudeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure Time / ms:");
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("10");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		/*exposureMinButton = new Button(swtGroup, SWT.PUSH);
		exposureMinButton.setText("Min");
		exposureMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		//exposureMinButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(false); } });
		
		exposureMaxButton = new Button(swtGroup, SWT.PUSH);
		exposureMaxButton.setText("Max");
		exposureMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		exposureMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(true); } });
		*/
		

		Label lKT = new Label(swtGroup, SWT.NONE); lKT.setText("Kinetic Cycle Time / ms:");
		kineticCycleTimeTextbox = new Text(swtGroup, SWT.NONE);
		kineticCycleTimeTextbox.setText("10");
		kineticCycleTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		kineticCycleTimeTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lAN = new Label(swtGroup, SWT.NONE); lAN.setText("Accumulations: Number:");
		numAccumulationsSpinner = new Spinner(swtGroup, SWT.DROP_DOWN);
		numAccumulationsSpinner.setValues(1, 1, Integer.MAX_VALUE, 0, 1, 10);
		numAccumulationsSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		numAccumulationsSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lAT = new Label(swtGroup, SWT.NONE); lAT.setText("Time / ms:");
		accumulateCycleTimeTextbox = new Text(swtGroup, SWT.NONE);
		accumulateCycleTimeTextbox.setText("10");
		accumulateCycleTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		accumulateCycleTimeTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
		lB.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("All parameters:");
		unsetAllButton = new Button(swtGroup, SWT.PUSH);
		unsetAllButton.setText("Unset/Read");
		unsetAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		//unsetAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(false); } });
		
		defaultAllButton = new Button(swtGroup, SWT.PUSH);
		defaultAllButton.setText("Set default");
		defaultAllButton.setEnabled(false);
		defaultAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		//defaultAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { allParamsButton(true); } });
		
		configToGUI();
	}

	protected void continuousCheckboxEvent(Event event) {
		AndorV2Config config = source.getConfig();
		config.setContinuous(continuousButton.getSelection());
		
		acquisitionModeCombo.select(config.acqusitionMode);
		
	}

	void configToGUI(){
		AndorV2Config config = source.getConfig();
				
		continuousButton.setSelection(config.isContinuous());
		
		readModeCombo.select(config.readMode);
		acquisitionModeCombo.select(config.acqusitionMode);
		frameTransferCheckbox.setSelection(config.frameTransferMode);
		enableMetadataCheckbox.setSelection(config.enableMetaData);
		shutterModeCombo.select(config.shutterMode);
		shutterTTLCheckbox.setSelection(config.shutterType == AndorV2Defs.SHUTTERTYPE_OPEN_LOW);
		triggerModeCombo.select(config.triggerMode);
		exposureLengthTextbox.setText(Float.toString(config.exposureTimeSecs * 1000));
		kineticCycleTimeTextbox.setText(Float.toString(config.kineticCycleTimeSecs * 1000));
		accumulateCycleTimeTextbox.setText(Float.toString(config.accumulationCycleTimeSecs * 1000));
		numAccumulationsSpinner.setSelection(config.nAccumulations);
		
		hsSpeedCombo.removeAll();
		if(config.horizShiftSpeeds != null){
			for(int i=0; i < config.horizShiftSpeeds.length; i++)
				hsSpeedCombo.add(Float.toString(config.horizShiftSpeeds[i]));
			hsSpeedCombo.select(config.horizShiftSpeedIndex);
		}
		
		vsSpeedCombo.removeAll();
		if(config.vertShiftSpeeds != null){
			for(int i=0; i < config.vertShiftSpeeds.length; i++)
				vsSpeedCombo.add(Float.toString(config.vertShiftSpeeds[i]));
			vsSpeedCombo.select(config.vertShiftSpeedIndex);
		}
	
		vsSpeedAmplitudeCombo.removeAll();
		if(config.vertShiftAmplitudes != null){
			for(int i=0; i < config.vertShiftAmplitudes.length; i++)
				vsSpeedAmplitudeCombo.add(config.vertShiftAmplitudes[i]);
			vsSpeedAmplitudeCombo.select(config.vertShiftAmplitudeIndex);
		}		
		
		preAmpGainCombo.removeAll();
		if(config.preAmpGains != null){
			for(int i=0; i < config.preAmpGains.length; i++)
				preAmpGainCombo.add(Float.toString(config.preAmpGains[i]));
			preAmpGainCombo.select(config.preAmpGainIndex);
		}

		emccdGainModeCombo.select(config.emccdGainMode);
		emccdGainLabel.setText("EMCCD Gain ("+
					((config.emccdGainRange != null && config.emccdGainRange.length > 1)
					 ? (config.emccdGainRange[0]+" - "+config.emccdGainRange[1]+"):")
					: "?? - ??):"));
		emccdGainTextbox.setText(Integer.toString(config.emccdGain));
		
		coolingCheckbox.setSelection(config.coolerOn);
		coolingSetTempSpinner.setValues(config.targetTemp, config.minTemp, config.maxTemp, 0, 1, 10);
		
		coolingCurrentTempLabel.setText("Status: " + config.currentTemp + "'C (" + 
								AndorV2Defs.tempCodeToString(config.tempControlStatus) + ")");
		
	}
	
	void guiToConfig(){
		AndorV2Config config = source.getConfig();
				
		config.readMode = readModeCombo.getSelectionIndex();
		config.acqusitionMode = acquisitionModeCombo.getSelectionIndex();
		config.frameTransferMode = frameTransferCheckbox.getSelection();
		config.enableMetaData = enableMetadataCheckbox.getSelection();
		config.shutterType = shutterTTLCheckbox.getSelection() 
								? AndorV2Defs.SHUTTERTYPE_OPEN_LOW : AndorV2Defs.SHUTTERTYPE_OPEN_HIGH;		
		config.shutterMode = shutterModeCombo.getSelectionIndex();
		config.triggerMode = triggerModeCombo.getSelectionIndex();
		config.preAmpGainIndex = preAmpGainCombo.getSelectionIndex();
		config.emccdGainMode = emccdGainModeCombo.getSelectionIndex();
		config.emccdGain = Algorithms.mustParseInt(emccdGainTextbox.getText());
		
		config.horizShiftSpeedIndex = hsSpeedCombo.getSelectionIndex();
		config.vertShiftSpeedIndex = vsSpeedCombo.getSelectionIndex();
		config.vertShiftAmplitudeIndex = vsSpeedAmplitudeCombo.getSelectionIndex();
				
		config.exposureTimeSecs = (float)(Algorithms.mustParseDouble(exposureLengthTextbox.getText()) / 1000.0);
		config.kineticCycleTimeSecs = (float)(Algorithms.mustParseDouble(kineticCycleTimeTextbox.getText()) / 1000.0);
		config.nAccumulations = numAccumulationsSpinner.getSelection();
		config.accumulationCycleTimeSecs = (float)(Algorithms.mustParseDouble(accumulateCycleTimeTextbox.getText()) / 1000.0);
		
		config.coolerOn = coolingCheckbox.getSelection();
		config.targetTemp = coolingSetTempSpinner.getSelection();
		
	}
	
	public void exposureMinMaxEvent(boolean max){
		AndorV2Config config = source.getConfig();
		
		if(max){
			config.exposureTimeSecs = config.maximumExposureTime;
		}else{
			// ??
		}
		configToGUI();
	}

	
	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
