package imageProc.sources.capture.andorV2;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import algorithmrepository.exceptions.NotImplementedException;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.EventReciever.Event;
import imageProc.database.gmds.GMDSUtil;
import imageProc.sources.capture.andorV2.swt.AndorV2SWTControl;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureConfig;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.flir.FLIRCamConfig;
import mds.GMDSFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.gmds.GMDSSignal;

/** Sensicam Image Source.
 *  
 * All the actual driver'ey code is in the thread in SensicamCapture
 *  
 * @author oliford
 */
public class AndorV2Source extends CaptureSource implements ImgSource, ConfigurableByID {
	
	private ByteBufferImage images[];
	
	private int nCaptured;
	
	private int lastCaptured;
	
	/** The low-level capturer, we should only have one of these */  
	private AndorV2Capture capture;
	
	/** The current config, should be kept always in sync with camera (if online)
	 * and with controller */
	private AndorV2Config config;
		
	public AndorV2Source(AndorV2Capture capture, AndorV2Config config) {
		this.capture = capture;
		this.config = (config != null) ? config : new AndorV2Config();
	}
	
	public AndorV2Source() {
		capture = new AndorV2Capture(this);
		config = new AndorV2Config();
	}

	@Override	
	public int getNumImages() { return images == null ? 0 : images.length; }

	@Override
	public Img getImage(int imgIdx) {		
		if(images != null && imgIdx >= 0 && imgIdx < images.length )
			return images[imgIdx];
		else
			return null;
	}
	
	@Override
	public Img[] getImageSet() { return images; }
		
	/** Called by SensicamCapture. Don't take long over this! */
	public void newImageArray(ByteBufferImage images[]){
		this.images = images;
		notifyImageSetChanged();
		updateAllControllers();
	}

	
	/** Called by SensicamCapture. Don't take long over this! */
	public void imageCaptured(int imageIndex) {

		images[imageIndex].imageChanged(true);
		if(imageIndex >= nCaptured)
			nCaptured = imageIndex+1;
		lastCaptured = imageIndex;
		
		updateAllControllers();
		
	}
	
	private long lastAutoExposure = 0;
	private long autoExposureMinPeriod = 50; //ms
	private double autoExposureMinLevel = 0.6;

	private double maxSum = 0;
	private int samplesInSum;

	@Override
	public AndorV2Source clone() {
		return new AndorV2Source(capture, config);
	}

	public void openCamera(){
		if(capture.isOpen()){
			System.err.println("openCamera(): Camera already open");
			return;
		}
		capture.setConfig(config);
		capture.initCamera();
	}

	public void startCapture(){
		if(capture.isBusy()){
			System.err.println("startCapture(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		
		lastAutoExposure = System.currentTimeMillis();

		capture.setConfig(config);
		capture.startCapture(config, images);
	}
		
	public void syncConfig(){
		if(capture.isBusy()){
			System.err.println("syncConfig(): Camera thread busy");
			return;
		}
		
		this.nCaptured = 0;		
		lastAutoExposure = System.currentTimeMillis();	
		capture.setConfig(config);	
		capture.testConfig(config, images);
	}
		
	public void abort(){ capture.abort(false); }
	
	public void closeCamera(){ capture.closeCamera(false); }
	
	public String getSourceStatus(){ 
		return config.nImagesToAllocate + " allocated = " + "???"
				+ "MB. "
				+ nCaptured + " captured ("+
				+ lastCaptured + " last). S/W start "				
				+ (config.beginCaptureOnStartEvent ? "armed" : "not armed");
	}	

	public String getCaptureStatus(){ return capture.getStatusString(); }
	
	@Override
	public Status getAcquisitionStatus(){ return capture.getAcquisitionStatus(); }
	@Override
	public String getAcquisitionStatusString(){ return capture.getStatusString(); }
	@Override
	public int getNumAcquiredImages() { return nCaptured; }
	
	public String getCCDInfo(){
						
		return "AndorV2 " +config.headModel + ", "
				+ "S/N: " + config.cameraSerialNumber;
				
	}
	
	public int getNumCaptured(){ return nCaptured; }
	
	public boolean isActive(){ return capture.isOpen(); }
	
	public boolean isBusy(){ return capture.isBusy(); }
	
	@Override
	public boolean isIdle() { return capture == null || !capture.isOpen() || !capture.isBusy(); }

	@Override
	public AndorV2Config getConfig() { return config;	}
	public void setConfig(CaptureConfig config) { 
		this.config = (AndorV2Config)config; 
		updateAllControllers(); 
	}
	
	public void configChanged() { 
		updateAllControllers();
	}
	
	
	@Override @SuppressWarnings("rawtypes")
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new AndorV2SWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	public void releaseMemory() {
		capture.abort(true);

		if(images != null){
			for(int i=0; i < images.length; i++){
				if(images[i] != null)
					images[i].destroy();
			}
		}
		System.gc();
		images = null;
		
		notifyImageSetChanged();
		updateAllControllers();
	}

	@Override
	public void loadConfigGMDS(String exp, int pulse) {
		if(pulse == -1 && connectedSource != null){
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		}
		
		GMDSFetcher gmds = GMDSUtil.globalGMDS();
		
		// look for the new one first
		String rootPath = "RAW/seriesData/AndorV2/config";
		String sigsPaths[] = gmds.dumpTree(exp, pulse, rootPath, true);

		for (String signalPath : sigsPaths) {
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, exp,
					rootPath + "/" + signalPath);

			try {
				GMDSSignal sig = (GMDSSignal) gmds.getSig(sigDesc);
				Object data = sig.getData();
				
				if(data.getClass().isArray() && Array.getLength(data) == 1){
					data = Array.get(data, 0); //grumble
				}
				
				signalPath = signalPath.replaceAll("/", "");
				
				if(signalPath.equals("cameraIndex")){ continue; } 
				if(signalPath.equals("nImagesToAllocate")){ config.nImagesToAllocate = (Integer)data; continue; }
				
				throw new NotImplementedException();
				
			} catch (RuntimeException err) {
				System.err.println("Couldn't read AndorV2 config from signal '" + sigDesc
						+ "': " + err.getMessage());
			}
		}
			
		updateAllControllers();
	}
	
	@Override
	public void close() {
		abort();
		closeCamera();		
	}

	@Override
	public boolean open(long timeoutMS) {
			
		openCamera();
		long t0 = System.currentTimeMillis();
		while(!capture.isOpen()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted while waiting for camera to open.");
			}
			
			if((System.currentTimeMillis() - t0) > timeoutMS){
				throw new RuntimeException("Timed out waiting for camera to open. Need a longer warm-up time??");
			}
		}
		
		
		//do a config sync as-is
		syncConfig();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) { 
			throw new RuntimeException("Interrupted while waiting for camera to open.");
		}

		return true;
	}


	public void setDiskMemoryLimit(long maxMemory) { capture.setDiskMemoryLimit(maxMemory); }
	
	public long getDiskMemoryLimit(){  return capture.getDiskMemoryLimit(); }

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());		  
		return "Andor2-CCD [" + hhc.substring(hhc.length()-3, hhc.length()) + "]";
	}	

	@Override
	protected final AndorV2Capture getCapture() { return capture; }

	public String[] getAvailableCameras() { return capture.getAvailableCameras(); }


	@Override
	public int getFrameCount() { return config.nImagesToAllocate; }
	
	@Override
	public void setFrameCount(int frameCount) {  
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.nImagesToAllocate = frameCount; 
		updateAllControllers();
	}
	
	@Override
	public long getFramePeriod() { return (int)(1.0e6 * config.accumulationCycleTimeSecs); }
	
	@Override
	public void setFramePeriod(long framePeriodUS) { 
		if(!config.enableAutoConfig) 
			throw new IllegalArgumentException("Autoconfig disabled");
		config.accumulationCycleTimeSecs = (float)(framePeriodUS / 1e6);
		updateAllControllers();
	}

	@Override
	public boolean isAutoconfigAllowed() { return config.enableAutoConfig;	}
}