package imageProc.sources.capture.flirSciCam.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.Algorithms;
import algorithmrepository.NotImplementedException;
import imageProc.core.ImgSource;
import imageProc.sources.capture.flirSciCam.FLIRSciCamConfig;
import imageProc.sources.capture.flirSciCam.FLIRSciCamSource;
import net.jafama.FastMath;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private FLIRSciCamSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo timestampModeCombo;
	
	private Text integrationTimeText;
	private Text frameTimeText;
	private Spinner selectedPresetSpinner;
	
	private Button boundToIntTimeCheckbox;
	private Text x0Textbox;
	private Text y0Textbox;
	private Text widthTextbox;
	private Text heightTextbox;
		
	public SimpleSettingsPanel(Composite parent, int style, FLIRSciCamSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false,6, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		
		Label lTS = new Label(swtGroup, SWT.NONE); lTS.setText("Timestamp:");
		timestampModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		timestampModeCombo.setItems(FLIRSciCamConfig.timestampModes);
		timestampModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		timestampModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lIS = new Label(swtGroup, SWT.NONE); lIS.setText("Image region:");
		
		Label lX0 = new Label(swtGroup, SWT.NONE); lX0.setText("x0:");
		x0Textbox = new Text(swtGroup, SWT.NONE);
		x0Textbox.setText("?");
		x0Textbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		x0Textbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lY0 = new Label(swtGroup, SWT.NONE); lY0.setText("y0:");		
		y0Textbox = new Text(swtGroup, SWT.NONE);
		y0Textbox.setText("?");
		y0Textbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		y0Textbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lWS = new Label(swtGroup, SWT.NONE); lWS.setText("");
		Label lW = new Label(swtGroup, SWT.NONE); lW.setText("Width:");
		widthTextbox = new Text(swtGroup, SWT.NONE);
		widthTextbox.setText("?");
		widthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		widthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lH = new Label(swtGroup, SWT.NONE); lH.setText("Height:");		
		heightTextbox = new Text(swtGroup, SWT.NONE);
		heightTextbox.setText("?");
		heightTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		heightTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Preset:");		
		selectedPresetSpinner = new Spinner(swtGroup, SWT.NONE);
		selectedPresetSpinner.setValues(0, 0, 100, 0, 1, 3);
		selectedPresetSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		selectedPresetSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Frame time [ms]:");		
		frameTimeText = new Text(swtGroup, SWT.NONE);
		frameTimeText.setText("?");
		frameTimeText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		frameTimeText.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		
		Label lT2 = new Label(swtGroup, SWT.NONE); lT2.setText("Exposure time [ms]:");
		
		integrationTimeText = new Text(swtGroup, SWT.NONE);
		integrationTimeText.setText("?");
		integrationTimeText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		integrationTimeText.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		Label lWS2 = new Label(swtGroup, SWT.NONE); lWS2.setText("");		
		boundToIntTimeCheckbox = new Button(swtGroup, SWT.CHECK);
		boundToIntTimeCheckbox.setText("Bound frame rate to integration time");
		boundToIntTimeCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		boundToIntTimeCheckbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		configToGUI();
	}

	void configToGUI(){
		FLIRSciCamConfig config = source.getConfig();
		
		continuousButton.setSelection(config.wrap)	;
		
		x0Textbox.setText(Integer.toString(config.x0));
		y0Textbox.setText(Integer.toString(config.y0));
		widthTextbox.setText(Integer.toString(config.width));
		heightTextbox.setText(Integer.toString(config.height));
		
		selectedPresetSpinner.setSelection(config.presetIdx);
		integrationTimeText.setText(Double.toString(config.integrationTime()));
		frameTimeText.setText(Double.toString(config.frameTime() * 1000.0));
		boundToIntTimeCheckbox.setSelection(config.presets[config.presetIdx].boundToIntTime);
	}
	
	void guiToConfig(){
		FLIRSciCamConfig config = source.getConfig();
		
		config.wrap = continuousButton.getSelection();

		config.x0 = Algorithms.mustParseInt(x0Textbox.getText());
		config.y0 = Algorithms.mustParseInt(y0Textbox.getText());
		config.width = Algorithms.mustParseInt(widthTextbox.getText());
		config.height = Algorithms.mustParseInt(heightTextbox.getText());
		
		config.presetIdx = selectedPresetSpinner.getSelection();
		
		/*config.presets[config.presetIdx].integrationTime = Algorithms.mustParseDouble(integrationTimeText.getText());
		config.presets[config.presetIdx].frameRate = 1000.0 / Algorithms.mustParseDouble(frameTimeText.getText());		
		
		config.presets[config.presetIdx].boundToIntTime = boundToIntTimeCheckbox.getSelection();
		*/
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		FLIRSciCamConfig config = source.getConfig();

		if(true)
			throw new NotImplementedException();
		
	}
	
	public void exposureMinMaxEvent(boolean max){
		FLIRSciCamConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
		configToGUI();
	}


	public void roiMaxEvent(){
		FLIRSciCamConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
