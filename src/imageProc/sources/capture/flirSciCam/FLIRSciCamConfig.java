package imageProc.sources.capture.flirSciCam;


import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;

public class FLIRSciCamConfig extends CaptureConfig {

	/** TODO: List of timestamp modes for the GUI */
	public static String[] timestampModes = { "Off", "Text in image", "Binary in image"};

	/** TODO: List of trigger modes for the GUI */
	public static String[] triggerModes = { "Software", "Start", "Frame" };
	
	/** If uninitialised, the camera values will be not set but retrieved on the first sync */
	public boolean initialised = false;
	
	/** Number of images to allocate */
	public int nImagesToAllocate = 100;
	
	/** Continuous mode (start again from image 0 (or 1) when nImagesToAllocate is reached) */
	public boolean wrap = false;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;

	/* Camera identifier, from the availableCameras list */
	public String cameraID;

	/** ROI offset in x */
	public int x0 = 0;
	
	/** ROI offset in y */
	public int y0 = 0;

	/** Image width (-1 to determine from camera) */
	public int width = -1;

	/** Image height (-1 to determine from camera) */
	public int height = -1;
	
	/** Selected preset */
	public int presetIdx = 0;
	
	public static class Preset {
		public double integrationTime = 50.0;
		
		public double frameRate = 10.0;
		
		public boolean boundToIntTime = false;
		
		public double maxFrameRate = Double.NaN;
	}
	
	public Preset[] presets = { new Preset(),new Preset(),new Preset(),new Preset(),new Preset(),new Preset(),new Preset(),new Preset(),new Preset(), }; 

	public double integrationTime() { return presets[presetIdx].integrationTime; }
		
	public double frameRate() { return presets[presetIdx].frameRate; }
	
	public double frameTime() { return 1.0 / frameRate(); }
		
	/** Timeout for grabbing images in ms */
	public int timeout = 100;

	public boolean isFactoryCalibrated = false;
	
	public double minIntegrationTime = Double.NaN;

	public double maxIntegrationTime = Double.NaN;
	
	public double minFrameRate = Double.NaN;
	
	/** Number of presets that we allow the camera to use during acqusition (this is < numberOfPreset) */
	public int numAvailablePresets = -1;

	/** Total number of presets in the camera */
	public int numberOfPresets = -1;

	public boolean canSetFrameRate;

	int minWidth;
	int maxWidth;
	int minHeight;
	int maxHeight;
	int xInc;
	int yInc; 
		
	@Override
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put(prefix + "/cameraID", cameraID);
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/blackLevelFirstFrame", blackLevelFirstFrame);
		map.put(prefix + "/wrap", wrap);
		for(int i=0; i < presets.length; i++) {			
			map.put(prefix + "/preset"+i+"/integrationTimes", presets[i].integrationTime);			
			map.put(prefix + "/preset"+i+"/frameRate", presets[i].frameRate);			
		}

		return map;
	}

}
