package imageProc.sources.capture.pco;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import imageProc.core.AcquisitionDevice;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.sources.capture.base.Capture;
import imageProc.sources.capture.base.CaptureSource;
import pcoJNI.PCOCameraSDKException;
import pcoJNI.PCOComm;
import pcoJNI.PCOGrabber;
import pcoJNI.SC2CameraDescriptionResponse;

/** Capture thread for PCO SDK Cameras */

public class PCOCamCapture extends Capture implements Runnable {
	private PCOCamSource source;
	
	private ByteBufferImage images[];
	
	private PCOCamConfig cfg;
		
	/** Ask the thread to start a config sync only */
	private boolean signalConfigSync = false;

	
	/** Camera is open and thread is running */
	private boolean cameraOpen = false;	
	/** Thread is doing something (sync or capture) */
	private boolean threadBusy = false;
	
	private int mcCurrentEntry = -1;
	private int mcNextEntryAtImageIdx = -1;
	
	private void syncConfig() {
	
		//stop the camera and clear it's internal config to default
		camera.SetRecordingState(0); 
		camera.ResetSettingsToDefault();
		
		//sort out the multiconfig first
		if(cfg.multiConfigNumImgs != null){
			
			mcCurrentEntry = 0;
			cfg.delay_time = cfg.multiConfigDly[0];
			cfg.exp_time = cfg.multiConfigExp[0];
			mcNextEntryAtImageIdx = cfg.multiConfigNumImgs[0];
			
		}else{
			mcCurrentEntry = -1;
			mcNextEntryAtImageIdx = -1;
		}
		
		//fetch read-only/information things first
		byte[] descData = camera.GetCameraDescriptor();
		cfg.description = new SC2CameraDescriptionResponse(descData);
	
		cfg.cameraInfo = camera.GetInfo(PCOComm.INFO_STRING_CAMERA);
		cfg.sensorInfo = camera.GetInfo(PCOComm.INFO_STRING_SENSOR);
		
		//short a[] = new short[10];
		//camera.GetCameraSetup(a);
		
		short temps[] = camera.GetTemperature();
		cfg.cameraTemperature = temps[0];
		cfg.sensorTemperature = temps[1] / 10.0;
		cfg.powerSupplyTemperature = temps[2];
		
		PCOCameraSDKException err = null;
		try{
			//if the config is set-up, attempt to set it
			if(cfg.initialised){
				
				grabber.Set_Grabber_Timeout(cfg.grabberTimeoutMS);
	
				camera.SetCameraToCurrentTime();
				
				cfg.storageMode = 1;
				try{
					camera.SetStorageMode(cfg.storageMode);
				}catch(RuntimeException err2){
					err2.printStackTrace();
				}
			
				camera.SetTimestampMode(cfg.timestampMode);
				camera.SetTimebase(cfg.delay_timebase, cfg.exp_timebase);	 
				camera.SetDelayExposure(cfg.delay_time, cfg.exp_time);
				camera.SetBinning(cfg.binningX, cfg.binningY);
				camera.SetNoiseFilterMode(cfg.noiseFilterMode);
				camera.SetTriggerMode(cfg.triggerMode);				
				camera.SetConversionFactor((short)(cfg.conversionFactor * 100));
				
				//the camera just fails on invalid ROIs and doesn't tell us the min/max, so we make some attempt to clamp them here
				if(cfg.roiX0 < 1) cfg.roiX0 = 1;
				if(cfg.roiY0 < 1) cfg.roiY0 = 1;
				short maxX1 = (short)(cfg.sensorWidth() / cfg.binningX);
				short maxY1 = (short)(cfg.sensorHeight() / cfg.binningY);
				if(cfg.roiX1 > maxX1) cfg.roiX1 = maxX1;
				if(cfg.roiY1 > maxY1) cfg.roiY1 = maxY1;
				camera.SetROI(cfg.roiX0, cfg.roiY0, cfg.roiX1, cfg.roiY1);				
				
		
				if(cfg.description.wNumADCsDESC > 1){
					cfg.adcOperationMode = 2;
					camera.SetADCOperation(cfg.adcOperationMode);
				}
		
				cfg.pixelrate = camera.GetPixelRate();
		
				camera.SetBitAlignment(cfg.bitAlignment);
				
			}
			setStatus(Status.init, "Loading configuration...");

			//test every exp/dly pair
			if(cfg.multiConfigNumImgs != null){
				int firstFilled = -1;
				for(int i=0; i < cfg.multiConfigNumImgs.length; i++){
					if(cfg.multiConfigNumImgs[i] > 0){
						if(firstFilled < 0)
							firstFilled = i;
						camera.SetDelayExposure(cfg.multiConfigDly[i], cfg.multiConfigExp[i]);
						//camera.ArmCamera(); //don't think this is needed to check exposure/delay times
						
					}
				}
				camera.SetDelayExposure(cfg.multiConfigDly[firstFilled], cfg.multiConfigExp[firstFilled]); //go back to the starting one
			}
			
			camera.ArmCamera(); //will throw PCOCameraSDKException if config is invalid
			
		}catch(PCOCameraSDKException err2){
			err = err2;
		}
		
		//now re-get everything, sot hat our config represents whats actually in the camera
		int ret[];
		short sRet[];
		
		try{
			cfg.storageMode = camera.GetStorageMode();
		}catch(RuntimeException err2){
			err2.printStackTrace();
			cfg.storageMode = Short.MIN_VALUE;
		}
		cfg.bitAlignment = camera.GetBitAlignment();
		cfg.triggerMode = camera.GetTriggerMode();
		cfg.noiseFilterMode = camera.GetNoiseFilterMode();
		cfg.conversionFactor = camera.GetConversionFactor() / 100.0;
		
		ret = camera.GetActualSize(); cfg.imageWidth = ret[0]; cfg.imageHeight = ret[1];
		ret = camera.GetDelayExposure(); cfg.delay_time = ret[0]; cfg.exp_time = ret[1];
		ret = camera.GetCOCRuntime(); cfg.frameTimeS = ret[0]; cfg.frameTimeNS = ret[1];
		sRet = camera.GetROI(); cfg.roiX0 = sRet[0]; cfg.roiY0 = sRet[1]; cfg.roiX1 = sRet[2]; cfg.roiY1 = sRet[3];
		sRet = camera.GetBinning(); cfg.binningX = sRet[0]; cfg.binningY = sRet[1];
		
		grabber.PostArm();
		
		cfg.grabberTimeoutMS = grabber.Get_Grabber_Timeout();
		ret = grabber.Get_actual_size(); cfg.grabberWidth = ret[0]; cfg.grabberHeight = ret[1]; cfg.grabberBPP = ret[2];
				
		if(err != null)
			throw err;
				
		cfg.initialised = true;
		
		source.configChanged();
	}
	
	/** Output debug info for the high speed debugging stuff */
	private boolean spinDebug = true;
		
	/** Internal state */
	private PCOComm camera;
	private PCOGrabber grabber;
	

	public PCOCamCapture(PCOCamSource source) {
		this.source = source;
		bulkAlloc = new BulkImageAllocation<ByteBufferImage>(source);
		bulkAlloc.setMaxMemoryBufferAlloc(Long.MAX_VALUE); //by default never use disk memory for camera 
	}
		
	@Override
	public void run() {
		threadBusy = true;
		try{
			if(camera != null || grabber != null){				
				deinit();
				throw new RuntimeException("Camera and/or grabber driver already open, aborting both");
			}
			
			setStatus(Status.init, "Initialising camera...");
			
			initDevice();
			
			//signalConfigSync = false;
			signalCapture = false;
			
			setStatus(Status.init, "Camera init done.");
			
			while(!signalDeath){
				try{					
					threadBusy = false;				
					while(!signalCapture && !signalConfigSync){
						try{
							Thread.sleep(10);
						}catch(InterruptedException err){ }
						
						if(signalDeath)
							throw new CaptureAbortedException();
					}
					threadBusy = true;
					signalAbort = false;
				
					if(signalConfigSync || signalCapture){
						signalConfigSync = false;
						syncConfig();
						
						setStatus(Status.init, "Allocating image memory...");		
						
						initImages();
						
					}
								
					
					if(signalCapture){
						signalCapture = false;
									
						setStatus(Status.init, "Config done, starting capture...");
						
						source.addNonTimeSeriesMetaDataMap(cfg.toMap("PCOCam/config"));
						
						doCapture();
						
						setStatus(Status.completeOK, "Capture done");
					}else {
						
						setStatus(Status.completeOK, "Config+Alloc done.");						
					}
					
				}catch(CaptureAbortedException err){
					signalAbort = false;
					logr.log(Level.WARNING, "Aborted by signal", err);
					setStatus(Status.aborted, "Aborted");
					//and just go around the loop
						
				}catch(RuntimeException err){
					logr.log(Level.SEVERE, "Aborted by exception: " + err, err);
					setStatus(Status.errored, "Aborted by error: " + err);
				}finally{
					signalCapture = false;
					signalConfigSync = false;
				}
			}

			setStatus(status, "Closed");
			
		}catch(Throwable err){
			err.printStackTrace();
			logr.info("PCOCam thread caught Exception: " + err);
			setStatus(Status.errored, "Aborted/Errored");
			
		}finally{
			try{
				deinit();
			}catch(Throwable err){
				System.err.println("PCOCam thread caught exception de-initing camera.");
				err.printStackTrace();
				setStatus(Status.errored, "Aborted/Errored");
			}
		}
	}
	
	private void initDevice() {

		camera = new PCOComm();
		System.out.printf("BoardNo: %d%n" , cfg.boardNo);
		camera.Open_Cam(cfg.boardNo);	

		int retSerialNumber[] = new int[1];
		cfg.camType = camera.GetCameraType(retSerialNumber);
		cfg.serialNumber = retSerialNumber[0];

		grabber = new PCOGrabber(camera);
		grabber.Open_Grabber(cfg.boardNo);
		
		cameraOpen = true;
	}
	
	
	private void initImages(){
		
		long imgDataSize = cfg.imageSizeBytes();
		int width = cfg.grabberWidth;
		int height = cfg.grabberHeight;
		int bitDepth = cfg.grabberBPP;
		double bytesPerPixel = bitDepth / 8;
		
		if(imgDataSize < height*width*bytesPerPixel){
			throw new RuntimeException("Camera wants less memory than it needs for the image???");
		}
			
		int footerSize = 0;
		
		if(imgDataSize >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
				
		try{
			
			ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, bitDepth, 0, footerSize, false);
			templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
			
			if(bulkAlloc.reallocate(templateImage, cfg.nImagesToAllocate)){
				logr.info(".initImages() Cleared and reallocated " + cfg.nImagesToAllocate + " images");
				images = bulkAlloc.getImages();
				source.newImageArray(images);
				
			}else{
				logr.info(".initImages() Reusing existing " + cfg.nImagesToAllocate + " images");
				bulkAlloc.invalidateAll();
			}			

		}catch(OutOfMemoryError err){
			logr.info(".initImages() Not enough memory for images.");
			bulkAlloc.destroy();
			throw new RuntimeException("Not enough memory for image capture", err);
		}
	
	}
	
	private void doCapture() {
		//if(camera.GetRecordingState() != 1)
		//	throw new RuntimeException("Camera isn't in recording state");

		setStatus(Status.awaitingSoftwareTrigger, "Capture started. Acquisition not started (awaiting soft start)");	
				
		try{
			
			double time[] = new double[cfg.nImagesToAllocate]; //seconds since first image
			long timestampNS[] = new long[cfg.nImagesToAllocate]; // full nanoSecs since midnight 1970 UTC
			long wallCopyTimeMS[] = new long[cfg.nImagesToAllocate];
			int imageNumberStamp[] = new int[cfg.nImagesToAllocate];
			long nanoTime[] = new long[cfg.nImagesToAllocate];
			
			source.setSeriesMetaData("PCOCam/timestampNS", timestampNS, true);
			source.setSeriesMetaData("PCOCam/wallCopyTimeMS", wallCopyTimeMS, true);
			source.setSeriesMetaData("PCOCam/imageNumberStamp", imageNumberStamp, true);
			source.setSeriesMetaData("PCOCam/nanoTime", nanoTime, true);
			source.setSeriesMetaData("PCOCam/time", time, true);
			
			long wallCopyTimeMS0 = Long.MIN_VALUE;
			long timestampNS0 = Long.MIN_VALUE;
			int nextImgIdx = 0;
			do{
				
				try {
					WriteLock writeLock = images[nextImgIdx].writeLock();
					writeLock.lockInterruptibly();
					try{
						images[nextImgIdx].invalidate(false);
						
						if(!isAcqusitionStarted()){ //don't actually start until acquire signal is set
							while(!signalAcquireStart){
								Thread.sleep(0, 100000);
							}
							grabber.Start_Acquire();
							camera.SetRecordingState(1);
							setStatus(Status.awaitingHardwareTrigger, "Capturing..., (awaiting hardware trigger)");
						}
						
						do{
							try{
								grabber.Wait_For_Next_Image(images[nextImgIdx].getWritableBuffer(writeLock), cfg.grabberTimeoutMS);
								
								wallCopyTimeMS[nextImgIdx] = System.currentTimeMillis();
								
								if(status != status.capturing)
									setStatus(Status.capturing, "Capturing..., acquisition started");
							}catch(PCOCameraSDKException err){
								//if(err.getErrorCode() == PCOCameraSDKException.PCO_ERROR_TIMEOUT)
								if(err.getErrorCode() == 0xA0332005)
									continue;
								throw err;
							}

							if(cfg.grabberWaitDelayUS != 0){
								try {
									//cfg.grabberWaitDelayMS = 1;
									int delayMS = (int)(cfg.grabberWaitDelayUS / 1000);
									Thread.sleep(delayMS, (cfg.grabberWaitDelayUS - delayMS*1000) * 1000);
								} catch (InterruptedException e) { }
							}
							
							if(signalDeath || signalAbort){
								logr.info("Aborting capture loop.");
								throw new CaptureAbortedException();
							}
							
							break;
						}while(true); //timeout loop
						
						//spinLog(".doCapture(): Buffer copied to images[" + nextImgIdx + "]");
						
						if(wallCopyTimeMS0 == Long.MIN_VALUE)
							wallCopyTimeMS0 = wallCopyTimeMS[nextImgIdx];
						
						if(cfg.timestampMode == 1 || cfg.timestampMode == 2){
							processBCDTimestamp(imageNumberStamp, timestampNS, nextImgIdx);
							
							if(timestampNS0 == Long.MIN_VALUE){
								timestampNS0 = timestampNS[nextImgIdx];
							}
							
							//use timestamp to make a quick 'time since first time' time 
							time[nextImgIdx] = (double)((timestampNS[nextImgIdx] - timestampNS0) / 1000) / 1e6;
							nanoTime[nextImgIdx] = (timestampNS[nextImgIdx] - timestampNS0 + wallCopyTimeMS0*1_000_000L);
						}else{
							
							//no timestamp, so our wall copy time is the best we can do							
							time[nextImgIdx] = (wallCopyTimeMS[nextImgIdx] - wallCopyTimeMS0) / 1e3;
							nanoTime[nextImgIdx] = wallCopyTimeMS[nextImgIdx] * 1_000_000L; 
						}
						
					}finally{ writeLock.unlock(); }
				} catch (InterruptedException e) {
					logr.info("Interrupted while waiting to write to java image.");
					return;
				}
				
				source.imageCaptured(nextImgIdx);
				
				nextImgIdx++;
				if(nextImgIdx >= cfg.nImagesToAllocate && cfg.wrap)
					nextImgIdx = cfg.blackLevelFirstFrame ? 1 : 0;
				
				if(cfg.multiConfigNumImgs != null && nextImgIdx >= mcNextEntryAtImageIdx){
					nextMultiConfigEntry(nextImgIdx);
				}
				
			}while(nextImgIdx < cfg.nImagesToAllocate);
			
		}finally{
			grabber.Stop_Acquire();
			camera.SetRecordingState(0);
		}

	}
	
	private void nextMultiConfigEntry(int nextImgIdx) {
		
		while(nextImgIdx >= mcNextEntryAtImageIdx){
			mcCurrentEntry++;				
			if(mcCurrentEntry >= cfg.multiConfigNumImgs.length)
				mcCurrentEntry = 0; //wrap, maybe the user wants that
				
			mcNextEntryAtImageIdx += cfg.multiConfigNumImgs[mcCurrentEntry];				
		}

		cfg.delay_time = cfg.multiConfigDly[mcCurrentEntry];
		cfg.exp_time = cfg.multiConfigExp[mcCurrentEntry];		
		camera.SetDelayExposure(cfg.delay_time, cfg.exp_time);
	}
	
	/**
	 * Fills in Metadata arrays from timestamp in image
	 * 
		px0: image counter (MSB) (00...99)  
		px1: image counter (00...99)  
		px2: image counter (00...99) 
		px3: image counter (LSB) (00...99)
		
		px4: year (MSB) (20) 
		px5: year (LSB) (15...99) 
		px6: month (01...12)
		px7: day (01...31) 
		px8: h (00...23)
		px9: min (00...59) 
		px10: s (00...59) 
		px11: us * 10000 (00...99) 
		px12: us * 100 (00...99) 
		px13: us (00...99) 
		
		but, we have to ask... was Jesus born negative???
	 */
	private void processBCDTimestamp(int[] imageNumberStamp, long[] timestamp, int imageIdx){

		int shift = cfg.getShift(); //adjustment for weird bit alignment

		int vals[] = new int[14];
		
		ByteBuffer buffer = images[imageIdx].getReadOnlyBuffer();
		
		for(int i=0; i < vals.length; i++){
			byte b = buffer.get(2*i);
			b >>= shift;
			vals[i] = ((b & 0xF0)>>4)*10 + (b & 0x0F);
		}
		
		imageNumberStamp[imageIdx] = vals[0] * 1000000 
									+ vals[1] * 10000 
									+ vals[2] * 100 
									+ vals[3];
		
		// process the stupid date formatted pixels BCD nonsense into a nanoTime
		
		int year = vals[4] * 100;
		year += vals[5];
		int month = vals[6];
		int day = vals[7];
		int hour = vals[8];
		int min = vals[9];
		int secs = vals[10];
		int microsecs = vals[11] * 10000 +
						vals[12] * 100 +
						vals[13] * 1;
		
		Date d = new Date(year, month, day, hour, min, secs);
		long milisecs = d.getTime();
		
		long nanoTime = milisecs * 1_000_000L + microsecs * 1000L;
		
		//microseconds since midnight
		timestamp[imageIdx] = nanoTime; 
		/*
			System.out.println(String.format("%02X --  %02X %02X %02X %02X %d", 
					vals[9],vals[10],vals[11],vals[12],vals[13],
					timestamp[imageIdx]));
			*/
		//spinLog(".processTimestamp(): Timestamp data for images[" + imageIdx + "]: imageNum = "+imageNumberStamp[imageIdx] + ", us since midnight = " + timestamp[imageIdx]);
	

	}
	
	private void deinit() {
		if(grabber != null){
			grabber.Close_Grabber();
			grabber.destroy();
			grabber = null;
		}
		if(camera != null){
			camera.Close_Cam();
			camera.destroy();
			camera = null;
		}
	}
	
	/** Start the camera thread and open the camera */
	public void initCamera() {
		if(isOpen())
			throw new RuntimeException("Camera thread already active");

		thread = new Thread(this);
		//thread.setPriority(Thread.MAX_PRIORITY);
		spinLog("Starting PCO capture thread with priority = " + thread.getPriority());

		setStatus(Status.init, "Thread starting");
		this.signalDeath = false;
		thread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> closeCamera(true)));
	}
	
	/** Signal the camera thread to set/load the config */
	public void testConfig(PCOCamConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalConfigSync = true;
		
	}
	
	/** Signal the camera thread to start the capture */
	public void startCapture(PCOCamConfig cfg, ByteBufferImage images[]) {
		if(!isOpen())
			throw new RuntimeException("Camera not open");
	
		if(isBusy())
			throw new RuntimeException("Camera thread is busy");
		
		this.cfg = cfg;
		this.images = images;
		signalCapture = true;
		
	}
	
	/** Signal the thread to abort the current sync/capture oepration */
	public void abort(boolean awaitStop){
		signalAbort = true;
		
		//and kick the thread for good measure 
		if(thread != null && thread.isAlive()){
			thread.interrupt();
			
			if(awaitStop){
				System.out.println("PCOCamCapture: Waiting for thread to abort.");
				while(threadBusy){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("PCOCamCapture: Thread stopped.");					
			}	
		}
	}
	
	/** Close the camera and kill the thread completely */
	public void closeCamera(boolean awaitDeath){
		if(thread != null && thread.isAlive()){
			signalDeath = true;
			signalAbort = true;
			thread.interrupt();
			if(awaitDeath){
				System.out.println("PCOCamCapture: Waiting for thread to die.");
				while(thread.isAlive()){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
				System.out.println("PCOCamCapture: Thread died.");					
			}	
		}
	}
	
	public void destroy() { closeCamera(false);	}
	
	public boolean isOpen(){ return thread != null && thread.isAlive() && cameraOpen; }
	
	public boolean isBusy(){ return isOpen() && threadBusy; }
	
	@Override
	protected CaptureSource getSource() { return source; }
	
	public void setConfig(PCOCamConfig config) { this.cfg = config;	}

}
