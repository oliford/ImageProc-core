package imageProc.sources.capture.pco;

import java.util.HashMap;
import java.util.Map;

import imageProc.sources.capture.base.CaptureConfig;
import pcoJNI.PCOComm;
import pcoJNI.SC2CameraDescriptionResponse;

public class PCOCamConfig extends CaptureConfig {
	
	/** If uninitialised, the camera values will be not set but retrieved on the first sync */
	public boolean initialised = false;
	
	/** Device index of camera/board */
	public int boardNo;
	
	/** Number of images to allocate */
	public int nImagesToAllocate = 100;
	
	/** Continuous mode (start again from image 0 (or 1) when nImagesToAllocate is reached) */
	public boolean wrap = false;
	
	/** Only read frame 0 once, as a black level */
	public boolean blackLevelFirstFrame = false;

	/** Camera type returned by PCO SDK */
	public short camType;

	/** Serial number returned by PCO SDK */
	public int serialNumber;

	/** Timeout for grabber Wait_For_Next_Image() call */
	public int grabberTimeoutMS = 100;
	
	/** (HACK!) Time to delay before recalling Wait_For_Next_Image(). 
	 * If this is too low, the grabber gets stuck */
	public int grabberWaitDelayUS = 1;

	/** Camera description returned by PCO SDK */
	public SC2CameraDescriptionResponse description;

	/** Camera info string returned by PCO SDK */
	public String cameraInfo;

	/** Sensor info string returned by PCO SDK */
	public String sensorInfo;
	
	/** Rolling or global shutter mode for PCO edge */
	public enum ShutterMode {
		Unknown, Rolling_Shutter, Global_Shutter, Global_Reset
	}
	public ShutterMode shutterMode = ShutterMode.Unknown;
	
	public int cameraTemperature;

	public double sensorTemperature;

	public int powerSupplyTemperature;
		
	public short storageMode = STORAGEMODE_RECORDER;
	
	public static final int STORAGEMODE_RECORDER = 0;
	public static final int STORAGEMODE_FIFO = 1;
	
	/**  
	 * /// - 0x0000 = no stamp in image
	 * 1 = BCD coded stamp in the first 14 pixel
	 * 2 = BCD coded stamp in the first 14 pixel + ASCII text
	 * 3 = ASCII text only (see descriptor for availability)
	 */
	public int timestampMode = 1;
	public final static String timestampModes[] = { "None", "14 pixels BCD", "14 pixels BCD + ASCII Text", "ASCII Text" };

	public static final int TIMEBASE_NANOSECS = 0;
	public static final int TIMEBASE_MICROSECS = 1;
	public static final int TIMEBASE_MILLISECS = 2;
	
	/** Unit of delay time (0=ns, 1=us, 2=ms) */
	public int delay_timebase = 1;

	/** Unit of exposure time (0=ns, 1=us, 2=ms) */
	public int exp_timebase = 1;

	public int delay_time = 0;

	public int exp_time = 10000;

	/**
	 * 
	/// Analog-digital-converter (ADC) operation for reading the image sensor data. Pixel data can be
	/// read out using one ADC (better linearity) or in parallel using two ADCs (faster). This option is
	/// only available for some camera models. If the user sets 2ADCs he must center and adapt the ROI
	/// to symmetrical values, e.g. pco.1600: x1,y1,x2,y2=701,1,900,500 (100,1,200,500 is not possible).
	///
	/// The input data has to be filled with the following parameter:
	/// - operation to be set:
	///   - 0x0001 = 1 ADC or
	///   - 0x0002 = 2 ADCs should be used...
	/// - the existence of the number of ADCs can be checked with the values defined in the camera description
	///
	 */
	public short adcOperationMode;

	public int pixelrate;

	public short bitAlignment;

	/** Hardware Trigger mode.
	 * 0 = Auto
	 * 1 = Software force
	 * 2 = External exposure start
	 * 3 = External exposure control
	 * 
	 * See PCOComm.GetTriggerMode
	 *  
	/// Trigger modes:
	/// - 0 = [auto trigger]
	///   	An exposure of a new image is started automatically best possible compared to the
	///   	readout of an image. If a CCD is used, and images are taken in a sequence, then exposures
	///   	and sensor readout are started simultaneously. Signals at the trigger input (\<exp trig\>) are irrelevant.
	 * 
	/// - 1 = [software trigger]:
	///   	An exposure can only be started by a force trigger command.
	 * 
	/// - 2 = [extern exposure & software trigger]:
	///
	///   	A delay / exposure sequence is started at the RISING or FALLING edge (depending on
	///   	the DIP switch setting) of the trigger input (\<exp trig\>).
	///
	/// - 3 = [extern exposure control]:
	///   	The exposure time is defined by the pulse length at the trigger input(\<exp trig\>). The
	///   	delay and exposure time values defined by the set/request delay and exposure command
	///   	are ineffective. (Exposure time length control is also possible for double image mode; the
	///  	 exposure time of the second image is given by the readout time of the first image.)
	///
	 */
	public short triggerMode = 0;
	public static String triggerModes[] = { "Auto", "Software Start", "External frame trigger", "External exposure control" }; 

	/** Width of sensor /standard area/ in pixels */
	public int sensorWidth(){ return (description != null) ? description.wMaxHorzResStdDESC : -1; }
	/** Height of sensor /standard area/ in pixels */
	public int sensorHeight(){ return (description != null) ? description.wMaxVertResStdDESC : -1; }

	/** Image width/height reported by camera/comm object */
	public int imageWidth, imageHeight;

	/** Total frame time ('COC Time') of camera. Seconds */
	public int frameTimeS;

	/** Total frame time ('COC Time') of camera. Nanosecond part */
	public int frameTimeNS;

	/** Image width/height reported by grabber object */
	public int grabberWidth, grabberHeight;

	/** Bits per pixel reported by grabber object */
	public int grabberBPP;
	
	/** ROI of image on camera sensor. */
	public short roiX0, roiY0, roiX1, roiY1;
	
	/** Binning in X and Y directions */
	public short binningX, binningY;

	/**
	/// Noise filter mode. See the camera descriptor for availability of this feature.
	///
	/// Parameter:
	/// - 0x0000 = [OFF]
	/// - 0x0001 = [ON]
	/// - 0x0101 = [ON + Hot Pixel correction]
	///
	*/
	public short noiseFilterMode;
	
	/**
	/// Current conversion factor in electrons/count
	///
	/// conversion factor must be valid as defined in the camera description
	 * This used to be called 'photoelectronsPerCount' in this config but
	 * doesnt seem to really be that.
	*/
	//public double photoelectronsPerCount;
	public double conversionFactor;
	
	
	//simple multiconfig for delay and exposure, same 'timebase' (units) as main ones
	public int multiConfigNumImgs[];
	public int multiConfigExp[];
	public int multiConfigDly[];
	public boolean multiConfigAutoconf[];
		
	public int getShift(){ return (bitAlignment != PCOComm.BIT_ALIGNMENT_LSB) ? 16-description.wDynResDESC : 0; }
	
	public long imageSizeBytes() { return grabberWidth * grabberHeight * grabberBPP / 8; }

	public double frameTimeMS() { return frameTimeS * 1000.0 + frameTimeNS / 1e6; }
	
	public double frameRateHz() { return 1000.0 / frameTimeMS(); }
	
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put(prefix + "/boardNo", boardNo);
		map.put(prefix + "/nImagesToAllocate", nImagesToAllocate);
		map.put(prefix + "/blackLevelFirstFrame", blackLevelFirstFrame);
		map.put(prefix + "/wrap", wrap);
		map.put(prefix + "/camType", camType);
		map.put(prefix + "/serialNumber", serialNumber);
		map.put(prefix + "/grabberTimeout", grabberTimeoutMS);
		map.put(prefix + "/cameraInfo", cameraInfo);
		map.put(prefix + "/sensorInfo", sensorInfo);
		map.put(prefix + "/cameraTemperature", cameraTemperature);
		map.put(prefix + "/sensorTemperature", sensorTemperature);
		map.put(prefix + "/powerSupplyTemperature", powerSupplyTemperature);
		map.put(prefix + "/timestampMode", timestampMode);
		map.put(prefix + "/timestampModesName", timestampModes[timestampMode]); 
		map.put(prefix + "/delay_timebase", delay_timebase);
		map.put(prefix + "/exp_timebase", exp_timebase);
		map.put(prefix + "/delay_time", delay_time);
		map.put(prefix + "/exp_time", exp_time);
		map.put(prefix + "/adcOperationMode", adcOperationMode);
		map.put(prefix + "/pixelrate", pixelrate);
		map.put(prefix + "/bitAlignment", bitAlignment);
		map.put(prefix + "/triggerMode", triggerMode);
		map.put(prefix + "/triggerModeName", triggerModes[triggerMode]);  
		map.put(prefix + "/sensorWidth", sensorWidth());
		map.put(prefix + "/sensorWidth", sensorHeight());
		map.put(prefix + "/imageWidth", imageWidth);
		map.put(prefix + "/imageHeight", imageHeight);
		map.put(prefix + "/frameTimeS", frameTimeS);
		map.put(prefix + "/frameTimeNS", frameTimeNS);
		map.put(prefix + "/grabberWidth", grabberWidth);
		map.put(prefix + "/grabberHeight", grabberHeight);
		map.put(prefix + "/grabberBPP", grabberBPP);
		map.put(prefix + "/roiX0", roiX0);
		map.put(prefix + "/roiY0", roiY0);
		map.put(prefix + "/roiX1", roiX1);
		map.put(prefix + "/roiY1", roiY1);
		map.put(prefix + "/binningX", binningX);
		map.put(prefix + "/binningY", binningY);
		map.put(prefix + "/noiseFilterMode", noiseFilterMode);
		map.put(prefix + "/frameTimeMS", frameTimeMS());
		//map.put(prefix + "/photoelectronsPerCount", photoelectronsPerCount);	
		map.put(prefix + "/conversionFactor", conversionFactor);
		if(multiConfigNumImgs != null){
			for(int i=0; i < multiConfigNumImgs.length; i++){
				map.put(prefix + "/multiConfig_"+i+"/numImgs"+i, multiConfigNumImgs[i]);
				map.put(prefix + "/multiConfig_"+i+"/expMS"+i, multiConfigExp[i]);
				map.put(prefix + "/multiConfig_"+i+"/dlyMS", multiConfigDly[i]);						
			}
		}
		
		//THe description is a lot of noise that never changes but might be useful.
		//So, we put the data in, but don't unpack.
		map.put("SC2DescriptionData", description.data);
		
		
		return map;
	}
	
	public double exposureTimeMS() {
		switch(exp_timebase){
			case 0: return exp_time / 1e6;
			case 1: return exp_time / 1e3;
			case 2: return exp_time;
			default: throw new IllegalArgumentException("Unknown timebase " + exp_timebase);
		}
	}

	public double delayTimeMS() {
		switch(delay_timebase){
			case 0: return delay_time / 1e6;
			case 1: return delay_time / 1e3;
			case 2: return delay_time;
			default: throw new IllegalArgumentException("Unknown timebase " + delay_timebase);
		}
	}


}
