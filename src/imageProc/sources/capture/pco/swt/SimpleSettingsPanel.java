package imageProc.sources.capture.pco.swt;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.ImgSource;
import imageProc.sources.capture.pco.PCOCamConfig;
import imageProc.sources.capture.pco.PCOCamSource;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import pcoJNI.PCOComm;

public class SimpleSettingsPanel {
	public static final double log2 = FastMath.log(2);
			
	private PCOCamSource source;
	private Composite swtGroup;
		
	private Button continuousButton;
	
	private Combo shutterModeCombo;
	private Combo timestampModeCombo;
	private Combo triggerModeCombo;
	private Button sensorCoolingButton;
	
	private Text exposureLengthTextbox;
	private Button exposureMinButton; 
	private Button exposureMaxButton;
	
	private Text delayTimeTextbox;
	private Label delayTimeLabel;	
	
	private Combo binningCombo;
	private Spinner roiX0Text, roiY0Text, roiX1Text, roiY1Text;
	private Button roiMaxButton;
	
	private Text conversionFactorTextbox;
		
	public SimpleSettingsPanel(Composite parent, int style, PCOCamSource source) {
		this.source = source;
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		
		continuousButton = new Button(swtGroup, SWT.CHECK);
		continuousButton.setText("Continuous (loop over images)");
		continuousButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		continuousButton.setToolTipText("Live view: Keep reading images, wrapping");
		continuousButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		sensorCoolingButton = new Button(swtGroup, SWT.CHECK);
		sensorCoolingButton.setText("Cooling (T = ????????)");
		sensorCoolingButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		sensorCoolingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		sensorCoolingButton.setEnabled(false); //not implemented yet
		
		Label lEM = new Label(swtGroup, SWT.NONE); lEM.setText("Shutter:");
		shutterModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		shutterModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		shutterModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lTS = new Label(swtGroup, SWT.NONE); lTS.setText("Timestamp:");
		timestampModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		timestampModeCombo.setItems(PCOCamConfig.timestampModes);
		timestampModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		timestampModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lHWT = new Label(swtGroup, SWT.NONE); lHWT.setText("H/W Trigger:");
		triggerModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		triggerModeCombo.setItems(PCOCamConfig.triggerModes);
		triggerModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		triggerModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Delay/µs:");
		
		delayTimeTextbox = new Text(swtGroup, SWT.NONE);
		delayTimeTextbox.setText("50000");
		delayTimeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		delayTimeTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		delayTimeLabel = new Label(swtGroup, SWT.NONE);
		delayTimeLabel.setText("Frames: length = ???ms, rate = ??? Hz");
		delayTimeLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		
		Label lT2 = new Label(swtGroup, SWT.NONE); lT2.setText("Exp/µs");
		//lT2.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		
		exposureLengthTextbox = new Text(swtGroup, SWT.NONE);
		exposureLengthTextbox.setText("10000");
		exposureLengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		exposureLengthTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		exposureMinButton = new Button(swtGroup, SWT.PUSH);
		exposureMinButton.setText("Min");
		exposureMinButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		exposureMinButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(false); } });
		
		exposureMaxButton = new Button(swtGroup, SWT.PUSH);
		exposureMaxButton.setText("Max");
		exposureMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		exposureMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { exposureMinMaxEvent(true); } });
		
		//not yet implemented
		exposureMinButton.setEnabled(false);
		exposureMaxButton.setEnabled(false);
				
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Bining:");
		binningCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		binningCombo.setItems(new String[]{ "1x1", "2x2", "3x3", "4x4" });
		binningCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		binningCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lRX = new Label(swtGroup, SWT.NONE); lRX.setText("X:");
		roiX0Text = new Spinner(swtGroup, SWT.NONE);
		roiX0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiX0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		Label lRY = new Label(swtGroup, SWT.NONE); lRY.setText("Y:");
		roiY0Text = new Spinner(swtGroup, SWT.NONE);
		roiY0Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY0Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY0Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		roiMaxButton = new Button(swtGroup, SWT.PUSH);
		roiMaxButton.setText("Max");
		roiMaxButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		roiMaxButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { roiMaxEvent(); } });
		
		Label lRW = new Label(swtGroup, SWT.NONE); lRW.setText("X1:");
		roiX1Text = new Spinner(swtGroup, SWT.NONE);
		roiX1Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		roiX1Text.setTextLimit(4);
		roiX1Text.setValues(0, 0, 9999, 0, 1, 1);
		roiX1Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		
		
		Label lRH = new Label(swtGroup, SWT.NONE); lRH.setText("Y1:");
		roiY1Text = new Spinner(swtGroup, SWT.NONE);
		roiY1Text.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		roiY1Text.setTextLimit(4);
		roiY1Text.setValues(0, 0, 9999, 0, 1, 1);
		roiY1Text.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });
		Label lBL = new Label(swtGroup, SWT.NONE); lBL.setText("");
			
		Label lCF = new Label(swtGroup, SWT.NONE); lCF.setText("Conversion Factor:");
		conversionFactorTextbox = new Text(swtGroup, SWT.NONE);
		conversionFactorTextbox.setText("100");
		conversionFactorTextbox.setToolTipText("Claims to be photoelectrons/count, but probably isn't. It's also settable, maybe, for some reason.");		
		conversionFactorTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		conversionFactorTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { guiToConfig(); } });

		
		
		configToGUI();
	}

	void configToGUI(){
		PCOCamConfig config = source.getConfig();
		
		continuousButton.setSelection(config.wrap)	;
		//sensorCoolingButton.setSelection(config.cooling);
		

		String tempStr1 = (new DecimalFormat("##.###")).format(config.sensorTemperature);
		String tempStr2 = (new DecimalFormat("##.###")).format(config.cameraTemperature);
		String tempStr3 = (new DecimalFormat("##.###")).format(config.powerSupplyTemperature);
		sensorCoolingButton.setText("Cooling (Sensor="+tempStr1+", Cam=" + tempStr2 + ", Power="+tempStr3);
		
		timestampModeCombo.select(config.timestampMode);
		triggerModeCombo.select(config.triggerMode);
		
		exposureLengthTextbox.setText(Double.toString(config.exposureTimeMS() * 1e3));
		delayTimeTextbox.setText(Double.toString(config.delayTimeMS() * 1e3));		
		delayTimeLabel.setText(String.format("Frames: length = %5.3f ms, rate = %5.3f Hz", 
											config.frameTimeMS(), config.frameRateHz()));
		
		if(config.binningX != config.binningY || config.binningX > 4)
			throw new IllegalArgumentException("Current binning " + config.binningX + "x" + config.binningY + " not supported by GUI");
		binningCombo.select(config.binningX - 1);
		
		//roi top/left are 1 based in config (which is stupid)
		roiX0Text.setSelection(config.roiX0 - 1);
		roiY0Text.setSelection(config.roiY0 - 1);
		roiX1Text.setSelection(config.roiX1 - 1);
		roiY1Text.setSelection(config.roiY1 - 1);
		
		conversionFactorTextbox.setText(Double.toString(config.conversionFactor));
		
	}
	
	void guiToConfig(){
		PCOCamConfig config = source.getConfig();
		
		config.wrap = continuousButton.getSelection();
		
		//config.setFeature("SensorCooling", sensorCoolingButton.getSelection());
		config.timestampMode = timestampModeCombo.getSelectionIndex();
		config.triggerMode = (short)triggerModeCombo.getSelectionIndex();
		
		double lastExpDly = config.delayTimeMS() + config.exposureTimeMS();
		
		double expTimeUS = Algorithms.mustParseDouble(exposureLengthTextbox.getText());
		if(expTimeUS < Integer.MAX_VALUE){
			config.exp_time = (int)expTimeUS;
			config.exp_timebase = PCOCamConfig.TIMEBASE_MICROSECS;
		}else{
			config.exp_time = (int)(expTimeUS / 1000);
			config.exp_timebase = PCOCamConfig.TIMEBASE_MILLISECS;
		}
		
		double delayTimeUS = Algorithms.mustParseDouble(delayTimeTextbox.getText());		
		if(delayTimeUS < Integer.MAX_VALUE){
			config.delay_time = (int)delayTimeUS;
			config.delay_timebase = PCOCamConfig.TIMEBASE_MICROSECS;
		}else{
			config.delay_time = (int)(delayTimeUS / 1000);
			config.delay_timebase = PCOCamConfig.TIMEBASE_MILLISECS;
		}
				
		if(lastExpDly != (config.delayTimeMS() + config.exposureTimeMS())){
			delayTimeLabel.setText(String.format("Frames: length = ???? ms, rate = ??? Hz"));
		}
		
		config.binningX = (short) (binningCombo.getSelectionIndex() + 1);
		config.binningY = (short) (binningCombo.getSelectionIndex() + 1);
		
		config.roiX0 = (short) (roiX0Text.getSelection() + 1);
		config.roiY0 = (short) (roiY0Text.getSelection() + 1);
		config.roiX1 = (short) (roiX1Text.getSelection() + 1);
		config.roiY1 = (short) (roiY1Text.getSelection() + 1);			
			
		config.conversionFactor = Algorithms.mustParseDouble(conversionFactorTextbox.getText());
	}
	
	public void frameTimeMinMaxEvent(boolean max){
		PCOCamConfig config = source.getConfig();

		if(true)
			throw new NotImplementedException();
		/*if(max){
			config.setFeatureToMin("FrameRate");
		}else{

			config.setFeatureToMax("FrameRate");
		}*/
		configToGUI();
	}
	
	public void exposureMinMaxEvent(boolean max){
		PCOCamConfig config = source.getConfig();
		
		if(true)
			throw new NotImplementedException();
		/*if(max){
			config.setFeatureToMax("ExposureTime");
		}else{

			config.setFeatureToMin("ExposureTime");
		}*/
		configToGUI();
	}


	public void roiMaxEvent(){
		PCOCamConfig config = source.getConfig();

		config.roiX0 = -1;
		config.roiY0 = -1;
		config.roiX1 = -1;
		config.roiY1 = -1;
		roiX0Text.setSelection(-1);
		roiY0Text.setSelection(-1);
		roiX1Text.setSelection(-1);
		roiY1Text.setSelection(-1);
	}

	public ImgSource getSource() { return source;	}

	public Control getSWTGroup() { return swtGroup; }
}
