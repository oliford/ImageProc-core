package imageProc.sources.sim.platesGen;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import fusionOptics.Util;
import fusionOptics.materials.UniaxialFixedIndexGlass;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** SWT Composite for controlling a PlatesGenSource */
public class PlatesGenSWTControl implements ImagePipeController {
	private final static String itemNames1[] = new String[]{ 
			"Num Images",
			"Width / px",
			"Height / px",
			"Pixel Size / μm", 
			"Focal Length / mm",
			"Analysing Polariser Angle / °",
			"Read Noise (NaN= Noise Off)", 
			"Component 1",
			"Component 2",
			"Component 3",
			"Component 4",
			"Component 5",
			"Component 6",
	};
	public final static int firstComponentEntry = 7; 
	
	private final static String colNames2[] = new String[]{ 
		"Plate Num", "Enable", "L/mm", "no", "ne", "axis ang", "axis tilt", "plate tilt ∥", "plate tilt ⟂", "scan θmax" };
	
	private final static String colNames1[] = new String[]{ 
		"Property", "Value / Amp", "Enable", "Wavelen / nm",	"Pol. Ang. °",	"Ellip. Ang" };
	
	private int nPlates = 8;
	
	private final double initProps[][] = new double[][]{
				{ 30 },  
				{ 320 }, { 240 }, 
				{ 19.8 },
				{ 50 },
				{ 45 },
				{ Double.NaN },
				{ 1, 5000.0, 653.1,  0,	0 }, // MSE
				{ 1, 10000.0, 653.5, 90, 0 },
				{ 1, 5000.0, 653.9,	0,	0 },
				{ 0, 8355, 656.193,	0,	35 }, // Zeeman. Wavelengths as R.Wolf's measurements on JET
				{ 0, 3290, 656.279,	90,	0 },
				{ 0, 8355, 656.365,	0,	-35 }
			};
	
	
	/** Tilted LiNb */
	/*private final double initPlates[][] = new double[][]{
			{ 1, 20, 2.282, 2.199, +45, 0, +45, 0 },
			
			{ 1, 20, 2.282, 2.199, -45, 0, +45, 0 },
			{ 1, 7.5, 2.282, 2.199, 0, 0, +45, 0 },
			//{ 1, 0, 2.282, 2.199, 0, 0, 0 },
			//{ 1, 0, 2.282, 2.199, 0, 0, 0 },
			
			{ 1, 0, 2.282, 2.199, 0, 0, 0 ,0 },
			{ 1, 0, 2.282, 2.199, 0, 0, 0 ,0},		
			
			//LiNbO3 No(650nm) = 2.28198458906231
			//LiNbO3 Ne(650nm) = 2.19901659008769
	};//*/
	
	private final static double fWave = Util.calcWaveplateFullWaveThickness(new UniaxialFixedIndexGlass(1.666, 1.549), 653e-9);
	
	/**Proper system */
	private final double initPlates[][] = new double[][]{
			{ 1, 0, 1.666, 1.549, 0, 0, 0, 0, 0 },
			{ 1, 0, 1.666, 1.549, 0, 0, 0, 0, 0 },
			{ 1, fWave/2*1e3, 
				1.666, 1.549, 0, 0, 0, 0, 360 },
			
			{ 1, fWave/4*1e3, 
				1.666, 1.549, +45, 0, 0, 0, 0 },
			
			{ 1, 3.8, 1.666, 1.549, +45, +45, 0, 0, 0 },
			{ 1, 3.8, 1.666, 1.549, -45, +45, 0, 0, 0 },
			
			{ 1, 5.4, 1.666, 1.549, 0, +45, 0, 0, 0 },
			{ 1, 1.2, 1.666, 1.549, 0, 0, 0, 0, 0 },

			{ 1, 0, 1.666, 1.549, 0, 0, 0, 0, 0 },
			{ 1, 0, 1.666, 1.549, 0, 0, 0, 0, 0 },
			
			// αBBO 1.666 1.549
	};
	
	
	private PlatesGenSource source;
	
	private Group swtGroup;
	private Table propsTable, platesTable;
	private TableEditor tableEditor1, tableEditor2;
	private Button swtGenerateButton;
	private Label swtStatusLabel;
	
	public PlatesGenSWTControl(Composite parent, int style, PlatesGenSource source) {
		swtGroup = new Group(parent, style);
		
		this.source = source;
		
		swtGroup.setLayout(new GridLayout(2, false));
		swtGroup.setText("PlatesGen Control");
		
		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status: ");
		swtStatusLabel = new Label(swtGroup, SWT.NONE); 
		swtStatusLabel.setText("init");
		swtStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 0));
		
		propsTable = new Table(swtGroup, SWT.BORDER);				
		propsTable.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 2, 0));
		propsTable.setHeaderVisible(true);
		propsTable.setLinesVisible(true);	
		
		TableColumn cols[] = new TableColumn[colNames1.length];
		for(int i=0; i < colNames1.length; i++){
			cols[i] = new TableColumn(propsTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames1[i]); 
		}
		
		TableItem items[] = new TableItem[itemNames1.length];
		for(int i=0; i < itemNames1.length; i++){
			items[i] = new TableItem(propsTable, SWT.NONE);
			items[i].setText(0, itemNames1[i]);
			
			for(int j=0; j < initProps[i].length; j++){
				items[i].setText(j+1, Double.toString(initProps[i][j]));
			}
				
			
		}
		
		for(int i=0; i < colNames1.length; i++)
			cols[i].pack();
			
		tableEditor1 = new TableEditor(propsTable);
		tableEditor1.horizontalAlignment = SWT.LEFT;
		tableEditor1.grabHorizontal = true;
		propsTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent1(event); } });
		
		
		platesTable = new Table(swtGroup, SWT.BORDER);				
		platesTable.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 2, 0));
		platesTable.setHeaderVisible(true);
		platesTable.setLinesVisible(true);
		cols = new TableColumn[colNames2.length];
		for(int i=0; i < colNames2.length; i++){
			cols[i] = new TableColumn(platesTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames2[i]); 
			
		}
		
		
		items = new TableItem[nPlates];
		for(int i=0; i < nPlates; i++){
			items[i] = new TableItem(platesTable, SWT.NONE);
			items[i].setText(0, Integer.toString(i));
			for(int j=0; j < (colNames2.length-1); j++)
				items[i].setText(j+1, Double.toString(initPlates[i][j]));
		}
		
		for(int i=0; i < colNames2.length; i++)
			cols[i].pack();
			
		tableEditor2 = new TableEditor(platesTable);
		tableEditor2.horizontalAlignment = SWT.LEFT;
		tableEditor2.grabHorizontal = true;
		platesTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent2(event); } });
		
		swtGenerateButton = new Button(swtGroup, SWT.PUSH);
		swtGenerateButton.setText("Generate");
		swtGenerateButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { generateButtonEvent(event); }});
		swtGenerateButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		swtGroup.pack();
	}
	
	
	private void generateButtonEvent(Event event) {
		if(source == null)
			return;
		
		
		
		int nImages = (int)Algorithms.mustParseDouble(propsTable.getItem(0).getText(1));
		int width = (int)Algorithms.mustParseDouble(propsTable.getItem(1).getText(1));
		int height = (int)Algorithms.mustParseDouble(propsTable.getItem(2).getText(1));

		double pixelSize = Algorithms.mustParseDouble(propsTable.getItem(3).getText(1)) * 1e-6;
		double focalLength = Algorithms.mustParseDouble(propsTable.getItem(4).getText(1)) * 1e-3;
		double polAng = Algorithms.mustParseDouble(propsTable.getItem(5).getText(1)) * Math.PI/180;
		double elecNoise = Algorithms.mustParseDouble(propsTable.getItem(6).getText(1));
		
		int nCmpts=0;
		for(int i=0; i < (itemNames1.length-firstComponentEntry); i++) 
			//if(Algorithms.mustParseDouble(propsTable.getItem(firstComponentEntry+i).getText(1)) > 0)
			if(Algorithms.mustParseDouble(propsTable.getItem(firstComponentEntry+i).getText(1)) != 0)
				nCmpts++;
		
		double inputAmp[] = new double[nCmpts];
		double inputWavelen[] = new double[nCmpts];
		double inputPolAng[] = new double[nCmpts];
		double inputEllip[] = new double[nCmpts];
		int k=0;
		for(int i=0; i < (itemNames1.length-firstComponentEntry); i++){
			double en = Algorithms.mustParseDouble(propsTable.getItem(firstComponentEntry+i).getText(1));
			double amp = Algorithms.mustParseDouble(propsTable.getItem(firstComponentEntry+i).getText(2));
			if(en != 0 && amp > 0){
				inputAmp[k] = amp;
				inputWavelen[k] = Algorithms.mustParseDouble(propsTable.getItem(firstComponentEntry+i).getText(3)) * 1e-9;
				inputPolAng[k] = Algorithms.mustParseDouble(propsTable.getItem(firstComponentEntry+i).getText(4)) * Math.PI/180;
				inputEllip[k] = Algorithms.mustParseDouble(propsTable.getItem(firstComponentEntry+i).getText(5))  * Math.PI/180;
				k++;
			}
		}
		
		double plateInfo[][] = new double[nPlates][colNames2.length-1];
		
		for(int i=0; i < nPlates; i++){
			TableItem tableItem = platesTable.getItem(i);
						
			for(int j=0; j < (colNames2.length-1); j++)
				plateInfo[i][j] = Algorithms.mustParseDouble(tableItem.getText(j+1));
			
			plateInfo[i][1] *= 1e-3; //in mm
			plateInfo[i][4] *= Math.PI / 180;
			plateInfo[i][5] *= Math.PI / 180;
			plateInfo[i][6] *= Math.PI / 180;		
			plateInfo[i][7] *= Math.PI / 180;
			plateInfo[i][8] *= Math.PI / 180;
		};
		
		source.setAll(nImages, width, height, focalLength, pixelSize, polAng, elecNoise, plateInfo,
				inputAmp, inputWavelen, inputPolAng, inputEllip);
		
	}
	
	private void tableMouseDownEvent1(Event event){ tableMouseDownEvent(event, propsTable, tableEditor1);	}
	
	private void tableMouseDownEvent2(Event event){ tableMouseDownEvent(event, platesTable, tableEditor2); }
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event, Table table, TableEditor tableEditor){
		
		Rectangle clientArea = table.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = table.getTopIndex ();
		while (index < table.getItemCount ()) {
			boolean visible = false;
			final TableItem item = table.getItem (index);
			for (int i=0; i<table.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;
					
					if(table == propsTable && index < firstComponentEntry && column != 1)
						break;
					final Text text = new Text(table.getParent(), SWT.BORDER);
					Listener textListener = new Listener () {
						public void handleEvent (final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								item.setText(column, Double.toString(Algorithms.mustParseDouble(text.getText())));
								//checkTable();
								text.dispose ();
								System.out.println("Text destroy");
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									item.setText(column, Double.toString(Algorithms.mustParseDouble(text.getText())));
									//checkTable();
									//FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									text.dispose ();
									System.out.println("Text destroy");
									e.doit = false;
								}
								break;
							case SWT.Modify:
								item.setText(column, Double.toString(Algorithms.mustParseDouble(text.getText())));
								break;							
							}
						}
					};
					text.addListener (SWT.FocusOut, textListener);
					text.addListener (SWT.Traverse, textListener);
					text.addListener (SWT.Modify, textListener);
					tableEditor.setEditor (text, item, i);
					text.setText (item.getText (i));
					text.selectAll ();
					text.setFocus ();
					//hacks for SWT4.4 (under linux GTK at least)
					//Textbox won't display if it's a child of the table
					//so we make it a child of the table's parent, but now need to adjust the location
					{
						text.moveAbove(table);
						final Point p0 = text.getLocation();
						Point p1 = table.getLocation();
						p0.x += p1.x - clientArea.x;
						p0.y += p1.y + text.getSize().y;
						text.setLocation(p0);	
						text.addListener (SWT.Move, new Listener() {						
							@Override
							public void handleEvent(Event event) {
								text.setLocation(p0);
							}
						});
					}
					//text.redraw();
					//table.redraw();
					System.out.println("Text created");
					return;
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}

	public ImgSource getSource() { return source; }
	
	@Override
	public void generalControllerUpdate(){
		if(!swtGroup.isDisposed()){
			ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
				@Override
				public void run() {
					if(!swtGroup.isDisposed()){
						doUpdate();
					}
				}
			});		
		}
	}

	protected void doUpdate() {
		swtStatusLabel.setText(source.getStatus());
		
	}


	@Override
	public Composite getInterfacingObject() { return swtGroup; }

	@Override
	public void destroy() {
		
		swtGroup.dispose();		
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return source; }
}
