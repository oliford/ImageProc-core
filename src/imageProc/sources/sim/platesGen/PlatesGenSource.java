package imageProc.sources.sim.platesGen;

import java.util.Arrays;

import org.eclipse.swt.widgets.Composite;

import fusionOptics.OpticApprox;
import fusionOptics.Util;
import imageProc.core.DoubleFlatArrayImage;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.RandomManager;

/** Simple simulation of a set of plates, polariser and lens using
 * Veiras phase formula */
public class PlatesGenSource extends ImgSourceOrSinkImpl implements ImgSource {
	private DoubleFlatArrayImage images[];

	private int width;
	private int height;
	private int nImages;
	private double focalLength;
	private double pixelSize;
	private double polariserAng;
	private double elecNoise;
	private double inputAmp[];
	private double inputWavelen[];
	private double inputPolAng[];
	private double inputEllip[];
	
	private double plateInfo[][];
	
	private int lastGeneratedImage = -1;
	
	private boolean isIdle = false;
	
	private boolean settingsChanged = false;
	
	private String status;

		//plates:
	//	optic axis, tilt, surface
	//polariser angle
	//init polarisation
	
		 
	public PlatesGenSource() {
		this.width = -1;
		this.images = null;
	}
	
	private void calc(){
		isIdle = false;
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalc(); } });
	}
	
	private void doCalc() {		
		settingsChanged = false;
		
		int nPlates = plateInfo.length;
		double plateNorm[][] = new double[nPlates][];
		double plateUp[][] = new double[nPlates][];
		double plateRight[][] = new double[nPlates][];
		
		for(int iP=0; iP < nPlates; iP++){
			if(plateInfo[iP][0] == 0)
				continue;
				
			double L = plateInfo[iP][1];
			double no = plateInfo[iP][2];
			double ne = plateInfo[iP][3];
			double axisAng = plateInfo[iP][4];
			double axisTilt = plateInfo[iP][5];
			double plateTiltPara = plateInfo[iP][6];
			double plateTiltPerp = plateInfo[iP][7];
			if(L <= 0)
				continue;
			
			//start with axis up and normal in z
			plateNorm[iP] = new double[]{
					-FastMath.sin(plateTiltPara) * FastMath.sin(axisAng),
					-FastMath.sin(plateTiltPara) * FastMath.cos(axisAng),
					FastMath.cos(plateTiltPara),
				};
			plateUp[iP] = new double[]{
					FastMath.cos(plateTiltPara) * FastMath.sin(axisAng),
					FastMath.cos(plateTiltPara) * FastMath.cos(axisAng),
					FastMath.sin(plateTiltPara),
				};
			plateRight[iP] = Util.cross(plateNorm[iP], plateUp[iP]);
			

			plateNorm[iP] = Algorithms.rotateVectorAroundAxis(plateTiltPerp, plateUp[iP], plateNorm[iP]);
			plateRight[iP] = Algorithms.rotateVectorAroundAxis(plateTiltPerp, plateUp[iP], plateRight[iP]);
		}
		
		
		lastGeneratedImage = -1;
		images = new DoubleFlatArrayImage[nImages];
		
		notifyImageSetChanged();
		
		//BinaryMatrixWriter allOut = new BinaryMatrixWriter("/tmp/blah.bin", 9);
		if(!Double.isNaN(elecNoise)){
			setSeriesMetaData("/Camera/ElectronsPerCount", 1.0, false);
			setSeriesMetaData("/Camera/ReadNoiseElectrons", elecNoise, false);
			setSeriesMetaData("/Camera/BackgroundLevelCounts", 0.0, false);			
		}
		
		double scanAng[] = new double[nImages];
		for(int iT=0; iT < nImages; iT++){
			double uT = (double)iT / nImages;
			scanAng[iT] = uT * 36000;
			
			status = iT + " / " + nImages;
			updateAllControllers();
			
			double imageData[] = new double[width*height];			
			
			for(int iC=0; iC < inputAmp.length; iC++){
				
				double polAng = inputPolAng[iC];// + uT * 2 * Math.PI;
				double s0[] = new double[]{
						FastMath.cos(2*inputEllip[iC]) * FastMath.cos(2*polAng), 
						FastMath.cos(2*inputEllip[iC]) * FastMath.sin(2*polAng), 
						FastMath.sin(2*inputEllip[iC]) 
				};
				
				for(int iY=0; iY < height; iY++){
					
					for(int iX=0; iX < width; iX++){
						
						//if(iX == 160 && iY == 120 && iT == 6)
						//	System.out.print("rar");
						
						double x = (iX  - width/2) * pixelSize; 
						double y = (iY  - height/2) * pixelSize;
						//double d = FastMath.sqrt(x*x + y*y);
						
						double incidenceVec[] = Util.reNorm(new double[]{ x, y, focalLength });
						
							
						double s[] = s0.clone();					
						for(int iP=0; iP < nPlates; iP++){
							if(plateInfo[iP][0] == 0)
								continue;
							if(plateInfo[iP][0] >= 2 && (iT % 2 != plateInfo[iP][0] % 2))
								continue;
								
							double L = plateInfo[iP][1];
							double no = plateInfo[iP][2];
							double ne = plateInfo[iP][3];
							double axisAng = plateInfo[iP][4];
							double axisTilt = plateInfo[iP][5];
							double plateTiltPara = plateInfo[iP][6];
							double plateTiltPerp = plateInfo[iP][7];
							double scanMax = plateInfo[iP][8];
							
							if(false && iX == 160 && iY == 120 && (iP == 3 || iP == 0)){
								System.out.println("stokes:" + 
										", iT = " + iT +
										", iP = " + iP +
										", iC = " + iC +
										", s1 = " + s[0] +
										", s2 = " + s[1] +
										", s3 = " + s[2]);
							}
							
							axisAng += uT * scanMax;
							
							if(L <= 0 || false)
								continue;
							
							double alpha = FastMath.acos(Util.dot(incidenceVec, plateNorm[iP]));
							double u = Util.dot(incidenceVec, plateUp[iP]);
							double r = Util.dot(incidenceVec, plateRight[iP]);
							double delta = FastMath.atan2(r, u);
							
							double opd = OpticApprox.waveplateOPD(1.0, no, ne, axisTilt, delta, alpha, L);
							double phi = 2*Math.PI*opd / inputWavelen[iC];
							
							
							double c2Theta = FastMath.cos(2 * axisAng);
							double s2Theta = FastMath.sin(2 * axisAng);
							double cPhi = FastMath.cos(phi);
							double sPhi = FastMath.sin(phi);
							
							//allOut.writeRow(iT, iC, iP, iX, iY, alpha, delta, opd, phi);
							
							//muller matrix for delay of Phi at angle Theta
							s = new double[]{
									s[0]*(c2Theta*c2Theta + cPhi*s2Theta*s2Theta)		+s[1]*(1.0-cPhi)*s2Theta*c2Theta			-s[2]*sPhi*s2Theta, 
									s[0]*(1.0-cPhi)*s2Theta*c2Theta 			+s[1]*(s2Theta*s2Theta + cPhi*c2Theta*c2Theta)	+s[2]*sPhi*c2Theta,
									s[0]*sPhi*s2Theta						-s[1]*sPhi*c2Theta					+s[2]*cPhi,
							};					
						}
						
						//apply muller matrix of polariser and take just the intensity
						double I = inputAmp[iC] * 0.5 * (1.0 + (FastMath.cos(2*polariserAng) * s[0] + FastMath.sin(2*polariserAng) * s[1]));
						if(!Double.isNaN(elecNoise)){
							 I += RandomManager.instance().nextNormal(0, FastMath.sqrt(I));
							 I += RandomManager.instance().nextNormal(0, elecNoise);
						}
						imageData[iY*width+iX] += I;
					}
				}
				
				if(settingsChanged) //abort and we'll get re-run by ensureFinalUpdate()					
					return;				
			}
			
			images[iT] = new DoubleFlatArrayImage(this, iT, width, height, imageData);
			
			setSeriesMetaData("platesGen/scanAngle", scanAng, true);
		
			lastGeneratedImage = iT;
						
			System.out.print(".");
		}
		
		//allOut.close();
		
		System.out.println("Plates calc done");
		status = "Done";
		updateAllControllers();
		isIdle = true;
	}
			
	@Override
	public int getNumImages() { return nImages; }

	@Override
	public Img getImage(int imgIdx) {
		return (images != null && imgIdx >= 0 && imgIdx <= lastGeneratedImage)
					? images[imgIdx] : null;
	}
	
	@Override
	public Img[] getImageSet() { return lastGeneratedImage >= 0 ? Arrays.copyOf(images, lastGeneratedImage+1) : new Img[0]; }
			
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new PlatesGenSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
		}
		return controller;
	}

	@Override
	public ImgSource clone() {
		return new PlatesGenSource();
	}
	
	public void setAll(int nImages, int width, int height, 
			double focalLength, double pixelSize, double polariserAng, double elecNoise, 
			double plateInfo[][], double inputAmp[], double inputWavelen[], 
			double inputPolAng[], double inputEllip[]){
		this.nImages = nImages;
		this.width = width;
		this.height = height;
		this.focalLength = focalLength;
		this.pixelSize = pixelSize;
		this.polariserAng = polariserAng;
		this.elecNoise = elecNoise;
		this.plateInfo = plateInfo;
		this.inputAmp = inputAmp;
		this.inputWavelen = inputWavelen;
		this.inputPolAng = inputPolAng;
		this.inputEllip = inputEllip;
		this.settingsChanged = true;
		
		calc();
	}

	public boolean isIdle() { return isIdle; }

	public String getStatus() { return status;	};
}
