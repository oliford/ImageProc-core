package imageProc.sources.sim.noiseGen;



import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;

/** SWT Composite for controlling a NoiseGenSource */
public class NoiseGenSWTControl implements ImagePipeController {
	private NoiseGenSource noiseSource;
	
	private Group swtGroup;
	private Button swtActiveButton;
	private Scale swtDelaySlider;
	
	public NoiseGenSWTControl(Composite parent, int style, NoiseGenSource source) {
		swtGroup = new Group(parent, style);
		
		this.noiseSource = source;
		
		swtGroup.setLayout(new GridLayout(2, false));
		swtGroup.setText("Noise Generator Control");
		
		swtActiveButton = new Button(swtGroup, SWT.CHECK);
		swtActiveButton.setText("Active");
		swtActiveButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startStopButtonEvent(event); }});
		swtActiveButton.setSelection(noiseSource != null && noiseSource.isNoiseActive());
		
		swtDelaySlider = new Scale(swtGroup, SWT.NONE);
		swtDelaySlider.setMinimum(1);
		swtDelaySlider.setMaximum(2000);
		swtDelaySlider.setSelection((source == null) ? 1000 : source.getImageChangePeriod());
		
		swtDelaySlider.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { delaySliderEvent(event); }});
		swtDelaySlider.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		swtGroup.pack();
	}
	
	private void delaySliderEvent(Event event) {
		if(noiseSource != null)
			noiseSource.setImageChangePeriod(swtDelaySlider.getSelection());
	}
	
	private void startStopButtonEvent(Event event) {
		if(noiseSource == null)
			return;
			
		if(swtActiveButton.getSelection()){
			noiseSource.startNoise();
		}else{
			noiseSource.stopNoise(false);
		}
	}

	public ImgSource getSource() { return noiseSource; }
	
	@Override
	public void generalControllerUpdate(){
		if(!swtGroup.isDisposed()){
			ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
				@Override
				public void run() {
					if(!swtGroup.isDisposed())
						swtActiveButton.setSelection(noiseSource != null && noiseSource.isNoiseActive());
				}
			});		
		}
	}

	@Override
	public Composite getInterfacingObject() { return swtGroup; }

	@Override
	public void destroy() {
		if(noiseSource != null){
			noiseSource.stopNoise(false); //stop it, just incase no one else is controlling it
		}	
		swtGroup.dispose();		
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return noiseSource; }
}
