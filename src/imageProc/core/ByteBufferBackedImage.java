package imageProc.core;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import otherSupport.bufferControl.DirectBufferControl;

/** Image backed by a (preferably direct) byte buffer 
 * 
 * From Java19, use this: https://openjdk.org/jeps/424
 * */
public abstract class ByteBufferBackedImage extends Img {

	/** The ByteBuffer for this image */
	protected ByteBuffer imageBuffer;  

	/** Bulk allocation collection this image is part of (or null if not) */
	protected BulkImageAllocation<ByteBufferBackedImage> bulkAlloc;
	
	/** Byte order used, or to be used for the buffer */
	protected ByteOrder byteOrder = ByteOrder.LITTLE_ENDIAN;
	
	public ByteBufferBackedImage(ImgSource source, int sourceIndex) {
		super(source, sourceIndex);		
	}
	
	public void setByteOrder(ByteOrder byteOrder) {
		this.byteOrder = byteOrder;
		if(imageBuffer != null)
			imageBuffer.order(byteOrder);
	}
	
	/** Create a new image with the same properties as this one, but in the given buffer */
	public abstract ByteBufferBackedImage createFromTemplate(ImgSource source,  int sourceIndex, BulkImageAllocation<?> bulkAlloc, ByteBuffer buffer, boolean init);
	
	
	public ByteBuffer getReadOnlyBuffer() {
		return (imageBuffer == null) ? null : imageBuffer.asReadOnlyBuffer();
	}
		
	public ByteBuffer getWritableBuffer(WriteLock writeLock) {
		if(!writeLock.isHeldByCurrentThread())
			throw new RuntimeException("getByteBuffer() requires locked write lock.");

		return destroyed ? null : imageBuffer;
	}
	
	/** Expose as public, since the public may fiddle with the data and should tell us */
	public void imageChanged(boolean fast){
		super.imageChanged(fast);
	}
	
	@Override
	public void destroy() {
		if(isDestroyed())
			return;
		//we have to wait for others to stop writing, because it segfaults the JVM
		WriteLock writeLock = lock.writeLock();
		try{			
			writeLock.lockInterruptibly();
		}catch(InterruptedException e){ 
			System.err.println("WARNING: Interrupted during wait for write lock on image destroy, won't destroy it - MEMORY LEAK!");
			destroyed = true; //mark it anyway
			return; //but we can't risk freeing the buffer
		}		
		
		destroyed = true; //mark destroyed regardless of what happens		
		writeLock.unlock(); //we can already unlock, so others can continue, but they'll soon discover it's got no buffer

		//and now we can try to cleanup buffers
		if(bulkAlloc != null){
			bulkAlloc.imageDestroyed(this);

		}else if(imageBuffer != null && imageBuffer.isDirect()){
			DirectBufferControl.freeBuffer(imageBuffer);
		}

		imageBuffer = null;
		super.destroy(); //mark it destroyed   
	}
	
	public void replaceData(ByteBuffer data){
		if(data.capacity() != getImageDataSize())
				throw new IllegalArgumentException("Image data length " +data.capacity()+ 
						" incorrect. Expecting " + getImageDataSize());
		this.imageBuffer = data;
		imageBuffer.position(0);
		imageBuffer.limit(imageBuffer.capacity());
		imageChanged(true);
	}
	
	@Override
	protected void finalize() throws Throwable {		
		destroy();
		super.finalize();
	}
	
	/** Returns the image data size for the image */
	public abstract int getImageDataSize();
	
	public ByteOrder getByteOrder(){ return byteOrder; }
	
	@Override
	public boolean isMemoryCompatible(Img img) {
		return img != null && (getClass().isInstance(this)) && ((ByteBufferBackedImage)img).getImageDataSize() == getImageDataSize();
	}
}
