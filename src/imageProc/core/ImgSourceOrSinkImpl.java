package imageProc.core;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/** This class contains the common implementation code for anything that is either an ImgSource or ImgSink.
 *  Derived classes should implement either ImgSource or ImgSink, or both to become an image pipe. 
 *  (Most image pipes will extend ImgProcPipe) 
 * 
 *  This implements the update processing code for both image source and image sinks. 
 *  
 *  */
public abstract class ImgSourceOrSinkImpl {
	
	/* ---------------- Stuff for all pipes ---------------- */
	protected Logger logr = Logger.getLogger(this.getClass().getName());
	
	protected LinkedList<ImagePipeController> controllers = new LinkedList<ImagePipeController>();
	
	public ImgSourceOrSinkImpl() {
		ImageProcUtil.addImgPipeInstance(this, true);
	}
		
	/** Called by the controller, when destroyed */
	public void controllerDestroyed(ImagePipeController controller) {
		controllers.remove(controller);
		
		//sources can stay alive, as you can reconnect them, but sinks probably should die
		//when they have no controllers left
		// ... no, because we want to be able to close windows without killing the processing chain
		//if(this instanceof ImgSink && !(this instanceof ImgSource) && controllers.size() == 0)
		//	destroy();
	}
	
	/** Creates a controller that can control this source, whos interfacing object will be of the given type */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) { return null; }
		
	private long lastControllersUpdate = -1;
	private static long minControllerUpdatePeriod = 300; //don't update controllers more often than this 
	public void updateAllControllers(){		
		ImageProcUtil.ensureFinalUpdate(controllers, new Runnable() {
			@Override
			public void run() {				
				
				for(ImagePipeController controller : controllers)
					controller.generalControllerUpdate();
				
			
				//If the updates are being triggered rapidly, we can just pause here for a bit
				// to slow them down. ImageProcUtil.ensureFinalUpdate() will ensure that the 
				//requests in between are just dropped. This way we definitely end up with one at the end.
				long t = System.currentTimeMillis();
				if((t - lastControllersUpdate) < minControllerUpdatePeriod){
					try {
						Thread.sleep(minControllerUpdatePeriod - (t - lastControllersUpdate));
					} catch (InterruptedException e) { }
				}
				lastControllersUpdate = System.currentTimeMillis();
				
			}
		});			
	}
	
	@Override
	public String toString() { return getClass().getCanonicalName() + "[" + Integer.toHexString(hashCode()) + "]"; }
	
	public String toShortString() { 
		String hhc = Integer.toHexString(hashCode());
		return getClass().getSimpleName() + "[" + hhc.substring(hhc.length()-3, hhc.length()) + "]"; 
	}
	
	public List<ImagePipeController> getControllers(){ return controllers; }
		
	/* ---------------- Stuff for source ---------------- */
	
	private LinkedList<ImgSink> connectedSinks = new LinkedList<ImgSink>();
	
	protected MetaDataMap seriesMetaData = new MetaDataMap();
		
	private final void assertIsSource(){
		if(!(this instanceof ImgSource))
			throw new RuntimeException("This (" + this.getClass().getSimpleName() + ") isn't an image source");		
	}
	
	public ImagePipeController createSourceController(Class interfacingClass, Object args[]){
		return createPipeController(interfacingClass, args, false);
	}
	
	public void addSink(ImgSink sink){
		assertIsSource();
		if(sink == this)
			throw new RuntimeException("Cannot to connect pipe to itself.");
		connectedSinks.add(sink);
		
	}
	
	public void removeSink(ImgSink sink){
		assertIsSource();
		connectedSinks.remove(sink);		
	}
	
	public List<ImgSink> getConnectedSinks(){
		assertIsSource();
		return connectedSinks;
	}
	
	private String notifyImageSetChangedSyncObj = "notifyImageSetChangedSyncObj";
	protected void notifyImageSetChanged(){
		assertIsSource();
		//we have two of these in this class, so use the list as the hashing object
		ImageProcUtil.ensureFinalUpdate(notifyImageSetChangedSyncObj, new Runnable() {	
			@Override
			public void run() {
				for(ImgSink sink : connectedSinks)
					sink.notifySourceChanged();
			}
		});		
	}
	
	private String notifyImageChangedSyncObj = "notifyImageChangedSyncObj";
	/** Called when the range calculation is complete. Notification is distributed to all sinks */
	public void imageRangesDone(int imgIdx){
		assertIsSource();
		final int idx = imgIdx;
		
		ImageProcUtil.ensureFinalUpdate(notifyImageChangedSyncObj, new Runnable() { 
			@Override
			public void run() {
				for(ImgSink sink : connectedSinks)
					sink.imageChangedRangeComplete(idx);
			}
		});             
	}

	/** Returns meta data. If it doesn't exist in this, we look further up the image chain.
	 * Be careful to clone the object if you try to put it back in */
	public Object getSeriesMetaData(String id){
		Object ourMeta = seriesMetaData.get(id);
		if(ourMeta != null)
			return ourMeta;
		
		//no source, then no data
		if(!(this instanceof ImgSink) || connectedSource == null)
			return null;
		
		//otherwise attempt get from our source
		return connectedSource.getSeriesMetaData(id);
	}
	
	/** Also put meta data in this source */
	public void setSeriesMetaData(String id, Object object, boolean isTimeSeries){		
		if((this instanceof ImgSink) && connectedSource != null && connectedSource.getSeriesMetaData(id) == object && object != null){
			throw new RuntimeException("Attempted to set object for meta data '"+id+"' into img source '"+this.getClass().getSimpleName()+
							"', but object is the same as from further up the source chain. Clone the object!!");
		}		
		seriesMetaData.put(id, object, isTimeSeries);
	}
	
	/** Sets series meta data by image index, creating the array as necessary */
	public void setImageMetaData(String id, int imageIndex, Object data){
		assertIsSource();
		seriesMetaData.setTimeSeriesEntry(id, imageIndex, data, ((ImgSource)this).getNumImages());		
	}
	
	/** Gets image meta data by image index */
	public Object getImageMetaData(String id, int imageIndex){
		assertIsSource();
		Object ourMeta = seriesMetaData.getTimeSeriesEntry(id, imageIndex);
		
		if(ourMeta != null)
			return ourMeta;
		
		//no source, then no data
		if(!(this instanceof ImgSink) || connectedSource == null)
			return null;
		
		//otherwise attempt get from our source
		return connectedSource.getImageMetaData(id, imageIndex);		
	}
	
	/** Creates a NEW map of all metadata in this and all sources up the chain.
	 * Modifying this won't change anything (modifying the objects in it will break things, don't do it) */	
	public MetaDataMap getCompleteSeriesMetaDataMap(){
		if((this instanceof ImgSink) && connectedSource != null){
			MetaDataMap completeMap = connectedSource.getCompleteSeriesMetaDataMap();
			completeMap.putAll(seriesMetaData);
			return completeMap;
		}else{
			return (MetaDataMap)seriesMetaData.clone();
		}
	}
	
	/** Same as getCompleteSeriesMetaDataMap() but returns a normal java map, losing the extra inforamtion 
	 * (i.e. whether the entries are time series or not) */ 
	public Map<String, Object> getCompleteMetaDataAsJavaMap(){
		return getCompleteSeriesMetaDataMap().asJavaMap();
	}

	/** Add a metadata map to the source's meta data map */
	public void addNonTimeSeriesMetaDataMap(Map<String, Object> map){
		seriesMetaData.putAllNonTimeSeries(map);		
	}
	
	/** Add a metadata map to the source's meta data map */
	public void addNonTimeSeriesMetaDataMap(String prefix, Map<String, Object> map){
		seriesMetaData.putAllNonTimeSeries(prefix, map);		
	}
	
	/** Add a metadata map to the source's meta data map */
	public void addMetaDataMap(MetaDataMap map){
		seriesMetaData.putAll(map);		
	}
				
	/* ---------------- Stuff for sink ---------------- */
			
	protected int selectedSourceIndex = -1;
	protected ImgSource connectedSource = null;
	
	/** We can keep the list of images with which we have registered for change notifications.
	 * It should match the source's image array */
	private Img imagesRegisteredWith[] = new Img[0];
	
	private final void assertIsSink(){
		if(!(this instanceof ImgSink))
			throw new RuntimeException("This (" + this.getClass().getSimpleName() + ") isn't an image sink");		
	}
	
	
	public ImagePipeController createSinkController(Class interfacingClass, Object args[]){
		return createPipeController(interfacingClass, args, true);
	}
	
	/** For the sink: Called when the selected source image changes, either 
	 * because the source has changed, or because the selected index has changed */
	protected void selectedImageChanged(){ };

	/** Sets the source (so we need to (un)register with it) */
	public void setSource(ImgSource source){
		assertIsSink();
		if(source == this)
			throw new RuntimeException("Cannot to connect pipe to itself.");
		if(connectedSource != null)
			connectedSource.removeSink((ImgSink)this);
		
		this.connectedSource = source;
		
		if(connectedSource != null){
			connectedSource.addSink((ImgSink)this);
			
		}

		selectedImageChanged();
	}
	
	/** Sink side: Fetches the currently selected image in the connected source 
	 *   for sinks that operate on a single image */
	public Img getSelectedImageFromConnectedSource(){
		assertIsSink();
		if(connectedSource == null || selectedSourceIndex < 0)
			return null;
		return connectedSource.getImage(selectedSourceIndex);
	}
	
	/** Gets the source that this sink is connected to */
	public ImgSource getConnectedSource(){
		assertIsSink();
		return connectedSource;
	}
	
	/** Sets the 'currently selected' image of the source,
	 *   for sinks that operate on a single image */
	public void setSelectedSourceIndex(int index){
		assertIsSink();
		this.selectedSourceIndex = index;
		selectedImageChanged();
	}
	
	/** Get the 'currently selected' image of the source,
	 *   for sinks that operate on a single image */
	public int getSelectedSourceIndex(){
		assertIsSink();
		return this.selectedSourceIndex;
	}
	
	/** defined by ImgSink, called by source to notify us that it has changed */
	public void notifySourceChanged(){
		assertIsSink();
		selectedImageChanged();
	}
	
	public void destroy(){
		if(this instanceof ImgSink){
			if(connectedSource != null){
					connectedSource.removeSink((ImgSink)this);			
			}
			setSource(null);
		}else{
			while(!connectedSinks.isEmpty()){
				connectedSinks.remove().setSource(null);
			}
		}
		
		while(!controllers.isEmpty()){
			controllers.remove().destroy();
		}
	}
	
	public void broadcastEvent(EventReciever.Event event){
		if(this instanceof ImgSource) {
			if(this instanceof EventReciever)
				((EventReciever)this).event(event);
			
			for(ImgSink sink : connectedSinks){
				if(sink instanceof EventReciever)
					((EventReciever)sink).event(event);

				if(sink instanceof ImgSource) 
					((ImgSource)sink).broadcastEvent(event);
			}
		}
	}
	
	/** Empty implementation of ImgSink */
	public void imageChangedLowLevel(int idx) { }
	
	/** Empty implementation of ImgSink */
	public void imageChangedRangeComplete(int idx) { 	}
}
