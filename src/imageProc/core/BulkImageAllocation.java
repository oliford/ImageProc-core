package imageProc.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bouncycastle.util.Arrays;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import otherSupport.bufferControl.DirectBufferControl;

public class BulkImageAllocation<T extends ByteBufferBackedImage> {
	
	protected Logger logr;
	
	private ImgSource source;
	
	/** The images */
	private T[] imgs;
	
	/** List of byte buffers used to allocate the set */ 
	private ByteBuffer[] allocBuffers;
	
	/** Number of images still in each buffer (decreased with image.destroy() )*/
	private int numImagesInBuffer[];
		
	/** Indices of alloc buffer used for each image */
	private int allocBufferIndices[]; 
	
	/** Template image with no buffer, for memory specification */
	private T templateImage;
	
	private long currentTotalBytesAllocedMem, currentTotalBytesAllocedDisk;
	
	/** For very large allocations that won't fit in memory (for all the images in the set),
	 * we can use disk mapped directly by the kernel.
	 * This is slow, and shouldn't be use for e.g. camera acquisition
	 * but does help for large scale processing, e.g. the FFT 
	 * Generally set lower on machines with SSDs.  
	 */
	private long maxMemoryBufferAlloc; //settings:imseProc.maxMemoryBufferAllocMB
	
	/** For each buffer, if mapped disk, the File and FileChannel objects */
	private File mappedBufferFile[];
	private FileChannel mappedBufferFileChan[];
			
	public BulkImageAllocation(ImgSource source) {
		logr = Logger.getLogger(this.getClass().getName() + "(" + source.getClass().getSimpleName() + ")");
		
		this.source = source;
		this.maxMemoryBufferAlloc = Long.parseLong(SettingsManager.defaultGlobal().getProperty("imageProc.maxMemoryBufferAllocMB", "30000")) * 1000000l; 
		imgs = null;
	}
	
	public T[] getImages(){ return imgs;	}
	
	public void setMaxMemoryBufferAlloc(long maxMemoryBufferAlloc) {
		this.maxMemoryBufferAlloc = maxMemoryBufferAlloc;
		//force realloc
		destroy();
	}
	
	public long getMaxMemoryBufferAlloc() { return maxMemoryBufferAlloc; };
	

	/** Recreates all the images.
	 * If the memory size matches, the images are just all invalidated.
	 * 
	 * If not, all the images in the set are destroyed and a new matching set is created.
	 * 
	 * @return true if the images were all actually reallocated */
	public boolean reallocate(T templateImage, int nImgs){
		
		if(imgs != null && imgs.length == nImgs && 	templateImage.isMemoryCompatible(this.templateImage)){
		 	boolean allOK = true;	
			for(int i=0; i < nImgs; i++){
				if(imgs[i] == null){					
					allOK = false;
					break;
				}
				//in case we changed the image size in a way that kept the memory allocation the same
				imgs[i].width = templateImage.width;
				imgs[i].height = templateImage.height;
				imgs[i].headerSize = templateImage.headerSize;
				imgs[i].footerSize = templateImage.footerSize;
				imgs[i].fields = templateImage.fields;
			}
			
			if(allOK) {
				logr.info("Reusing " + nImgs + " that are of same type as requested.");
				return false;
			}

		}
			
		destroy();
			
		//logr.fine("Forcing garbage collect before bulk reallocation.");
		//System.gc(); //good time for this, since the last destory() should have freed lots of stuff
		//hmm, maybe not such a good time since the pilot will time out on camera init 
			
		allocate(templateImage, nImgs, false);
		
		return true;		
	}
	
	private void allocate(T templateImage, int nImgs, boolean init){
		this.templateImage = templateImage;
		imgs = (T[])Array.newInstance(templateImage.getClass(), nImgs);
		
		int bytesPerImage = templateImage.getImageDataSize();
		long totalBytes = (long)bytesPerImage * nImgs;
		
		if(totalBytes <= 0){
			logr.warning("Allocating 0 or empty images.");			
			return; //nothing to do, leave images null or array empty 
		}
				
		
		//path for disk backed memory  
		String cachePath = SettingsManager.defaultGlobal().getPathProperty("minerva.cache.path", "/tmp/minerva");				
		String buffPath = SettingsManager.defaultGlobal().getPathProperty("minerva.cache.mappedMemoryPath", cachePath + "/mappedMemoryBuffers");
		
		// check if we have enough disk space to allocate the WHOLE thing and 50% more
		// This is critical because the entire OS can die if we allocate too much.
		// Generally, it will accept the allocation, but fail later when you try to write to it
		// Unfortunately, creating the file doesn't actually allocate the space, evne though the file is 'that big'.
		// So, we still have a problem if we allocate almost all the remaining space once and then another processor
		// tries to allocate as much again. The first allocation will not actually reduce the free disk space.
		if(totalBytes > maxMemoryBufferAlloc){
			long freeSpace = (new File(buffPath)).getUsableSpace();
			if(freeSpace < (1.5 * totalBytes)){
				throw new OutOfMemoryError("Trying to allocate "+(totalBytes/1e9)+"GB in MappedByteBuffers on a file system with critically low disk space. " +
											"Since this happily incapacitates the linux kernel, this allocation is being preemptively refused. Please free 150% of that.");
			}
		}
		
		//can't do over 4GB per buffer
 		int maxImagesPerBuffer = Integer.MAX_VALUE / bytesPerImage;
		
		int nBuffers = (int)Math.ceil((double)nImgs / maxImagesPerBuffer); //always round up
		
		allocBuffers = new ByteBuffer[nBuffers];
		numImagesInBuffer = new int[nBuffers];
		mappedBufferFile = new File[nBuffers];
		mappedBufferFileChan = new FileChannel[nBuffers];
		
		allocBufferIndices = new int[nImgs];
		
		int nextImage = 0;
		
		Random randGen = new Random();
		
		for(int i=0; i < nBuffers; i++){
			int nImagesInBuffer = Math.min((nImgs - nextImage), maxImagesPerBuffer);
			int bytesInBuffer = nImagesInBuffer * bytesPerImage;
			
			if(totalBytes > maxMemoryBufferAlloc){
				//create a file on disk (not in the temp dir, which might be in memory 
				logr.info("BulkImageAllocation: Allocating "+bytesInBuffer+" bytes using memory mapped I/O");

				String fileName = buffPath + "/allocBuffer-" + source.toShortString() + "-" + i + "-" + randGen.nextInt(); 
										
				OneLiners.makePath(fileName);
				mappedBufferFile[i] = new File(fileName);
				mappedBufferFile[i].delete();
				
				try {
					mappedBufferFileChan[i] = new RandomAccessFile(mappedBufferFile[i], "rw").getChannel();				
					allocBuffers[i] = mappedBufferFileChan[i].map(FileChannel.MapMode.READ_WRITE, 0, bytesInBuffer);				
					allocBuffers[i].put(bytesInBuffer-1, (byte)0xCE); //grow file to required size now
					currentTotalBytesAllocedDisk += bytesInBuffer;
									 
				} catch (FileNotFoundException e) {
					throw new RuntimeException(e);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			
			}else{
				logr.info("BulkImageAllocation: Allocating "+bytesInBuffer+" bytes using ByteBuffer.allocateDirect() ");
				
				//allocate a direct memory buffer
				allocBuffers[i] = DirectBufferControl.allocateDirect((int)bytesInBuffer);
				logr.info("BulkImageAllocation: Alloc "+bytesInBuffer+" bytes using ByteBuffer.allocateDirect() done.");
				
				currentTotalBytesAllocedMem += bytesInBuffer;
			}
			
			for(int j=0; j < nImagesInBuffer; j++){
				allocBuffers[i].limit((j+1)*bytesPerImage);
				allocBuffers[i].position(j*bytesPerImage);
				ByteBuffer imgBuff = allocBuffers[i].slice();
												
				imgs[nextImage] = (T)templateImage.createFromTemplate(source, nextImage, this, imgBuff, init);
				numImagesInBuffer[i]++;
				allocBufferIndices[nextImage] = i;
				nextImage++;
			}
		}
	}

	public void imageDestroyed(T image) {
		//see if no one is using the alloced buffer anymore, and if not, free it
		synchronized (this) {
			int imgIndex = image.getSourceIndex(); //should be the index into imgs[]
			int allocBufferIndex = allocBufferIndices[imgIndex];
			
			imgs[imgIndex] = null;
			numImagesInBuffer[allocBufferIndex]--;
			int bufferSize = allocBuffers[allocBufferIndex].capacity();
			                         
			if(numImagesInBuffer[allocBufferIndex] <= 0){
				if(mappedBufferFileChan[allocBufferIndex] != null){
					try {
						mappedBufferFileChan[allocBufferIndex].close();
						mappedBufferFile[allocBufferIndex].delete();
					} catch (Exception e) { } //don't care
					mappedBufferFileChan[allocBufferIndex] = null;
					mappedBufferFile[allocBufferIndex] = null;
					currentTotalBytesAllocedDisk -= bufferSize;
					if(allocBuffers[allocBufferIndex].isDirect()) {
						try {
							DirectBufferControl.freeBuffer(allocBuffers[allocBufferIndex]); //is this actually needed?
						} catch(RuntimeException err) { 
							Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Unable to deallocate direct byte buffer. (Maybe need to add --add-opens and --add-exports to VM args)", err);
						}
					}
					
				}else if(allocBuffers[allocBufferIndex].isDirect()){ 
					//direct but not mapped
					try {
						DirectBufferControl.freeBuffer(allocBuffers[allocBufferIndex]);
					} catch(RuntimeException err) { 
						Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Unable to deallocate direct byte buffer. (Maybe need to add --add-opens and --add-exports to VM args)", err);
					}
					currentTotalBytesAllocedMem -= bufferSize;
				}
				allocBuffers[allocBufferIndex] = null;
			}
			allocBufferIndices[imgIndex] = -1;
		}
	}
	
	/** Destroy all images and free all memory */
	public void destroy(){
		if(imgs == null)
			return;
		
		for(int i=0; i < imgs.length; i++){
			if(imgs[i] != null)
				imgs[i].destroy();
		}
		//memory will get freed as imageDestroyed[] gets called				
	}

	public void invalidateAll() {
		if(imgs != null){
			for(int i=0; i < imgs.length; i++){
				if(imgs[i] != null)
					imgs[i].invalidate(true);
			}
		}
		
	}
	
	public ByteBuffer[] getAllocationBuffers(){ return allocBuffers; }

	public long getAllocatedMemory() {
		return currentTotalBytesAllocedMem;
	}
	
	/** Find the index into the complete image set of the given address
	 * 
	 * @param bufferAddress
	 * @return
	 */
	public int getImageIndex(long addr) {
		int imageIdx0 = 0; //index of first image in next allocBuffer
		
		for(int i=0; i < allocBuffers.length; i++) {
			long baseAddr = DirectBufferControl.getAddress(allocBuffers[i]);
			long offset = addr - baseAddr;
			
			//is it inside this buffer?
			if(offset >= 0 && offset < allocBuffers[i].capacity()) {
				return imageIdx0 + (int)(offset / imgs[0].getImageDataSize());
				
				
			}else {
				imageIdx0 += numImagesInBuffer[i];
			}
			
		}
		
		throw new RuntimeException("Didn't find an allocBuffer that contained the image for address " + String.format("0x%08x", addr));
	}

	/** Reduce the reported number of images in the set to the given number. 
	 * The memory is NOT deallocated. */
	public void truncateImageSet(int nImagesNew) {
		for(int i=nImagesNew; i < imgs.length; i++) {
			if(imgs[i] != null)
				imageDestroyed(imgs[i]);
		}
		
		T[] newImgs = (T[])Array.newInstance(templateImage.getClass(), nImagesNew);
		System.arraycopy(imgs, 0, newImgs, 0, nImagesNew);
		imgs = newImgs;
	}
}
