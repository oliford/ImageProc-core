package imageProc.core;


/** Anything which interrogates and/or modifies a module.
 * 
 * This is basically either a GUI element, or a CLI module or something
 * 
 * @author oliford
 *
 */
public interface GeneralController {
	/** Returns the interfacing object for the controller
	 * For a SWT gui controller, this is a 'Composite' 
	 * ...etc
	 * @return
	 */
	public Object getInterfacingObject();
	
	/** Tell the controller that the source has changed in some way */
	public void generalControllerUpdate();

	public void destroy();
	
	public Object getControlledObject();
}
