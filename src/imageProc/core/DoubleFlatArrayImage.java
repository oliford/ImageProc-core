package imageProc.core;


import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

/** Image held in a double array */
public class DoubleFlatArrayImage extends Img {
	private double imageData[];
	
	public DoubleFlatArrayImage(ImgSource source, int sourceIndex, int width, int height) {
		super(source, sourceIndex);
		this.width = width;
		this.height = height;
		imageData = new double[width * height];
		replaceData(imageData);
	}
	
	public DoubleFlatArrayImage(ImgSource source, int sourceIndex, int width, int height, double imageData[]) {
		super(source, sourceIndex);
		this.width = width;
		this.height = height;
		replaceData(imageData);
	}
	
	public void replaceData(double imageData[]){
		if(imageData.length != width * height)
				throw new IllegalArgumentException("Image data length " +imageData.length+ 
						" invalid for "+width+" x "+height+" double image");
		this.imageData = imageData;
		imageChanged(true);
	}
	
	public double[] getFlatArray() { return imageData; }

	@Override
	public double getMaxPossibleValue() {
		return Double.POSITIVE_INFINITY;
	}

	@Override
	/** We don't care about the read lock here */
	public double getPixelValue(ReadLock readLock, int x, int y) {
		if(destroyed) return Double.NaN;		
		return imageData[y*width+x];		
	}
	
	public void setPixelValue(int x, int y, double value) {
		imageData[y*width+x] = value;
	}
	
	@Override
	public String toString() {
		return super.toString() + " [" + min + "-" + max + "]";
	}
	
	/** Expose as public, since the public may fiddle with the data and should tell us */
	public void imageChanged(boolean fast){
		super.imageChanged(fast);
	}

	@Override
	public void destroy() {		
		imageData = null;
		super.destroy();
	}
	
	@Override
	public boolean isMemoryCompatible(Img img) {
		return img != null && (img instanceof DoubleFlatArrayImage) && img.getWidth() == width && img.getHeight() == height;
	}
}
