package imageProc.core;

/** Controllers that allow the user to set an ROI to work on */
public interface ImagePipeControllerROISettable extends ImagePipeController {
	/** mouse move etc setting image position */
	public void movingPos(int x, int y);
	
	/** Click etc setting mage position */
	public void fixedPos(int x, int y);
	
	/** Image area selection made */
	public void setRect(int x0, int y0, int width, int height);
	
}
