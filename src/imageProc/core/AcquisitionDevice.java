package imageProc.core;

/** Image sources driving a camera, for control.
 *
 * 
 *  Standard metadata definitions:
 *    AcquisitionDevice/CountsPerUnit - Number of photos/photoelectrons/smallest counted units per value unit in the data.
 *    AcquisitionDevice/IntegrationTime - Time in seconds of integrated light signal
 */ 
public interface AcquisitionDevice {
	
	/** Statuses of the current/last capture/config operation  */
	public static enum Status {
		/** Device has not yet been activated / configured */
		notInitied,
		
		/** Device is currently being initialised / configured */
		init, 
		
		/** Device is initialised and configured but is awaiting the software trigger. */
		awaitingSoftwareTrigger, 
		
		/** Device is initialised,configured and software triggered but has 
		 * not collected any images, so is probably awaiting a hardware trigger. */
		awaitingHardwareTrigger,		
		
		/** Device is has captured at least one image but has not yet finished. */
		capturing, 
		
		/** An error has occured and captuing has aborted. */
		errored, 
		
		/** Last operation was aborted by an external signal (not an internal error) */
		aborted, 
		
		/** Last configure or capture completed successfully */
		completeOK 
	}		
	
	/** Gets the current status of the acquisition device */
	public Status getAcquisitionStatus();

	/** Detailed status/error string for general fault reporting */
	public String getAcquisitionStatusString();
	
	/** Close the device, its about to be turned off */
	public void close();
	
	/** Open the device as if it has been turned off and set everything up again. 
	 * @return true if successful */ 
	public boolean open(long timeoutMS);
	
	/** Begin capture. Configure and allocate but maybe don't start acquiring if enableAcquire is false) */ 
	public void startCapture();
	
	/** Abort capture/acquire and return to idle. This differs from Triggerable.triggerAbort() in that it does NOT trigger abort 
	 * on all connected syncs */
	public void abort();

	/** Returns the number of images successfully captured in the current/last acquisition */
	public int getNumAcquiredImages();
	
	/** Sets the software acquisiotn enable/inhibit flag. This is typically used as a 
	 * kind of software start trigger, to be hit some time after the acquisition initialisation is done. */
	public void enableAcquire(boolean enable);
	
	/** Sets general config parameters of the device.
	 * Common examples:
	 * 		ExposureTime 
	 * 		FramePeriod (a.k.a FrameTime)
	 * 		FrameCount
	 * 
	 * @return true if the parameter was recognised and could be set to the given value  */
	public boolean setConfigParameter(String param, String value);
	
	/** @return Run length of current settings */
	public String getConfigParameter(String param);
	
	public void setFramePeriod(long framePeriodUS);
	
	/** @return framePeriod in US */
	public long getFramePeriod();
	
	public void setFrameCount(int frameCount);
	
	public int getFrameCount();
	
	public void setExposureTime(long exposureTimeUS);
		
	public long getExposureTime();
	
	/** Are calls via setFrameCount/Period going to to anything? */
	public boolean isAutoconfigAllowed();

}
