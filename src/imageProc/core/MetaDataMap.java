package imageProc.core;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/** Maps of metadata
 * Always String name and some object.
 * 
 * Object are returned in their simplest form. In that arrays with one element will be returned as a single Object.
 * 
 * The name is usually a path, so leading slashes are ignored
 * 
 * @author oliford
 */
public class MetaDataMap {
		
	public static class MetaData {
		public MetaData(Object object, boolean isTimeSeries) {
			this.object = object;
			this.isTimeSeries = isTimeSeries;
		}
		public Object object;
		public boolean isTimeSeries;
	}

	private HashMap<String, MetaData> map = new HashMap<String, MetaData>();
	
	private MetaData metaDataObj(String id) {
		id = id.replaceAll("/[/]*", "/").replaceAll("^[/]*", ""); //strip multiple slashes, have none at start
		//try that
		MetaData metaData = map.get(id);
		if(metaData != null)
			return metaData;
		//if not try with a leading /
		return map.get("/" + id);
	}
	
	/** Gets the object associated with the given metadata entry */
	public Object get(String id) {
		MetaData metaData = metaDataObj(id);
		return (metaData == null) ? null : metaData.object;
	}
	
	/** Gets the MetaData entry object */
	public MetaData getMetaDataEntry(String id){
		return metaDataObj(id);
	}
	
	/** @return true if the given metadata entry is a time series array, with entires associated with each image */
	public boolean isTimeSeries(String id){
		return metaDataObj(id).isTimeSeries;
	}
	
	/** Gets the object associated with the given metadata entry, or the only object in it if it's an array of one length.
	 * This us useful for things you know should be a single value, but sometimes end up in single length arrays 
	 * @param id
	 * @return
	 */
	public Object getSingle(String id){
		Object val = get(id);
		//If the thing is a 1 length array, return the thing in the array element 0
		if(val.getClass().isArray()){
			int len = Array.getLength(val);
			if(len == 0)
				return null;
			if(len == 1)
				return Array.get(val, 0);
			else
				throw new RuntimeException("Wanted a single element for metaData '"+id+"', but it is an array length " + len);
		}
		return val;
	}
	
	// Simplified helper functions
	public double getDouble(String id){
		Object val = getSingle(id);
		if(!(val instanceof Number))
			throw new RuntimeException("Wanted a double value for metaData '"+id+"', but it isn't a number, it's a " + id.getClass().getName());
		
		return ((Number)val).doubleValue();
	}
	
	public int getInt(String id){
		Object val = getSingle(id);
		if(!(val instanceof Number))
			throw new RuntimeException("Wanted a int value for metaData '"+id+"', but it isn't a number, it's a " + id.getClass().getName());
		
		return ((Number)val).intValue();
	}
	
	public Object put(String id, Object value, boolean isTimeSeries) {
		id = id.replaceAll("/[/]*", "/").replaceAll("^[/]*", "/"); //strip multiple slashes, have exactly one at start
		MetaData metaData = new MetaData(value, isTimeSeries);
		MetaData metaData2 =map.put(id, metaData);  
		return (metaData2 == null ? null : metaData2.object);
	}
	
	/** Add a map of metadata, none of which is interpreted as a time series */
	public void putAllNonTimeSeries(String prefix, Map<? extends String, ? extends Object> newMap) {
		if(prefix.endsWith("/"))
			prefix = prefix.replaceAll("/*$", "");
		
		for (Map.Entry<? extends String, ? extends Object> entry : newMap.entrySet()) {
			map.put(prefix + "/" + entry.getKey(), new MetaData(entry.getValue(), false));
		}
	}
	
	/** Add a map of metadata, none of which is interpreted as a time series */
	public void putAllNonTimeSeries(Map<? extends String, ? extends Object> newMap) {
		for (Map.Entry<? extends String, ? extends Object> entry : newMap.entrySet())
			map.put(entry.getKey(), new MetaData(entry.getValue(), false));
	}
	
	public void putAll(MetaDataMap newMap) {
		for (Map.Entry<? extends String, ? extends MetaData> entry : newMap.map.entrySet())
			map.put(entry.getKey(), entry.getValue());
	}
	
	public void setTimeSeriesEntry(String id, int index, Object data, int nImagesInSource){
		id = id.replaceAll("/[/]*", "/").replaceAll("^[/]*", "/"); //strip multiple slashes, have exactly one at start
		
		synchronized (this) { //thread-safe all element wise metadata updates			
			Object dataArray = get(id);
			if(dataArray == null || !dataArray.getClass().isArray()){
				dataArray = Array.newInstance(data.getClass(), nImagesInSource);			
				put(id, dataArray, true);
			}
			if(Array.getLength(dataArray) <= index){
				Object dataArray2 = Array.newInstance(data.getClass(), nImagesInSource);
				System.arraycopy(dataArray, 0, dataArray2, 0, index);
				put(id, dataArray2, true);
				dataArray = dataArray2;
			}
				
			Array.set(dataArray, index, data);
		}
	}

	public Object getTimeSeriesEntry(String id, int index) {
		MetaData metaData = metaDataObj(id);
		if(metaData == null)
			return null;
		if(!metaData.isTimeSeries)
			throw new RuntimeException("Trying to read metaData '"+id+"' as time series, but it isn't one");
					
		return (metaData.object != null 
				&& metaData.object.getClass().isArray() 
				&& Array.getLength(metaData.object) > index
				&& index >= 0) 
						? Array.get(metaData.object, index) 
						: null;		
	}

	public Map<String, Object> extractSingleArrayElements() {
		HashMap<String, Object> newMap = new HashMap<String, Object>();
		
		for (Map.Entry<? extends String, ? extends MetaData> entry : map.entrySet()){
			String key = entry.getKey();
			MetaData metaData = entry.getValue();
			Object val = (metaData == null) ? null : metaData.object;
			
			if(val != null && val.getClass().isArray()){
				int len = Array.getLength(val);
				if(len == 0)
					val = null;
				if(len == 1)
					val = Array.get(val, 0);				
			}
			newMap.put(key, val);
		}
		
		return newMap;
	}
	
	@Override
	public MetaDataMap clone() {
		MetaDataMap newMap = new MetaDataMap();
		newMap.map = (HashMap<String, MetaData>)map.clone();
		return newMap;
	}

	public Set<Entry<String, MetaData>> entrySet() { return map.entrySet(); }

	public void clear() { map.clear(); }

	public Set<String> keySet() { return map.keySet(); }

	public Map<String, Object> asJavaMap() {
		HashMap<String, Object> newMap = new HashMap<String, Object>();
		for(Entry<String, MetaData> entry : map.entrySet()){
			newMap.put(entry.getKey(), entry.getValue().object);
		}
		return newMap;
	}

	/** Get object of first meta data that ends with the given string */
	public String findEndsWith(String suffix, boolean timeSeriesOnly) {
		for(Entry<String, MetaData> entry : map.entrySet()){
			if(timeSeriesOnly && !entry.getValue().isTimeSeries)
				continue;
			
			if(entry.getKey().endsWith(suffix) && entry.getValue().object != null){
				return entry.getKey();
			}
		}
		return null;
	}

	public void remove(String id) {
		map.remove(id);
	}
}
