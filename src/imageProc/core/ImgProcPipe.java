package imageProc.core;

import java.lang.reflect.Array;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

/** Standard image processing pipe which is a sink and a source, creating an output image set of a given input image set. 
 * For a large set of images with all the same dimensions. 
 * 
 * This makes an attempt to deal the updating of output images according to their dependence on the input images that have changed.
 * 
 * Processing process goes:
 * 	 checkOutputSet()
 *   preCalc()
 *   for each image:{
 *      if needs update:{
 *      	lock output image
 *   		doCalc()
 *   		unlock output image   		    		
 *   	}
 *   }
 * 
 * */
public abstract class ImgProcPipe extends ImgProcessor implements ImgSource, ImgSink, EventReciever {
	protected static final int controllerUpdatePeriod = 500;
	
	protected Class<Img> imageClass;
	
	protected Img imagesOut[];
	
	/** Sum of change IDs of all input images for each output image */
	protected int sourceChangeIDHash[] = new int[0];
	
	protected int inWidth, inHeight, nFieldsIn, outWidth, outHeight, nFieldsOut;
	
	/** Set to true by doCalc() if the calc failed because dependant output images are
	 * required and not yet calculated. If true at the end of a the calc loop, the whole
	 * loop is repeated. */
	protected boolean dependantImagesPending = false; 
			
	/**
	 * @param initSet Zero length array of target image type
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ImgProcPipe(Class imageClass) {
		this.imageClass = imageClass;
		imagesOut = (Img[])Array.newInstance(imageClass, 0);
	}
	
	@SuppressWarnings({ "rawtypes" })
	public ImgProcPipe(Class imageClass, ImgSource source, int selectedIndex) {
		this(imageClass);
		setSource(source);
		setSelectedSourceIndex(selectedIndex);
		calc();
	}
	
	/** Guaranteed to be called only once at a time */
	@Override
	protected void doCalcScan(){
		abortCalc = false;
		status = "Preparing";
		
		try{
			lastCalcFullyComplete = false;
			
			if(connectedSource == null){
				//source disconnected, wipe everything
				seriesMetaData = new MetaDataMap();
				if(checkOutputSet(0)){
					sourceChangeIDHash = new int[imagesOut.length]; //everything needs to be redone
		        	notifyImageSetChanged();
				}
				status = "No source";
				return;
			}
			
			//make sure we give the same meta data map as the source
			//seriesMetaData = connectedSource.getSeriesMetaDataMap();
			seriesMetaData = new MetaDataMap();
			
			boolean settingsHadChanged = settingsChanged;
			settingsChanged = false;
			
			int nImagesIn = connectedSource.getNumImages();
			
			//first we need to find the width/height
			inWidth = -1;
			
			//try the selected image first
			Img img = getSelectedImageFromConnectedSource();
			if(img != null){
				inWidth = img.getWidth();
				inHeight = img.getHeight();		
				nFieldsIn = img.getNFields();	
			}
			if(inWidth < 0){ //otherwise scan them
				for(int i=0; i < nImagesIn; i++){
					img = connectedSource.getImage(i);
					if(img.getWidth() >= 0){
						inWidth = img.getWidth();
						inHeight = img.getHeight();
						nFieldsIn = img.getNFields();
						break;
					}
				}
			}
			if(inWidth < 0){ //still nothing?
				logr.warning("No valid source images.");
				seriesMetaData = new MetaDataMap();
				if(checkOutputSet(0)){
					sourceChangeIDHash = new int[imagesOut.length]; //everything needs to be redone
		        	 notifyImageSetChanged();
				}
				status = "No valid images in source.";
				return;
			}
			
			if(checkOutputSet(calcSelectedImageOnly ? 1 : nImagesIn)){
				sourceChangeIDHash = new int[imagesOut.length]; //everything needs to be redone
	        	notifyImageSetChanged();
			}
		
			
			preCalc(settingsHadChanged);
			boolean inhibitImagePendingRecalc = false;
			do{
				dependantImagesPending = false;
				scanLoop(settingsHadChanged);
				if(dependantImagesPending && !inhibitImagePendingRecalc){
					logr.warning("Some images were missing dependant images, repeating entire calculation loop.");
					
					//force a complete recalc because we currently have no way of marking images
					//as not yet done other than invalid ranges, which isn't predictable since
					//the ranges are set in another thread					
					settingsHadChanged = true; 
				}
			}while(dependantImagesPending);
			
			postCalc(settingsHadChanged);

			status = "Done";
			
		}catch(Throwable err){
			logr.log(Level.WARNING, "Caught error in ImgProcPipe processing", err);
			lastCalcFullyComplete = false;
			status = "Error: " + err;
			updateAllControllers();
			throw (err instanceof RuntimeException) ? ((RuntimeException)err) : new RuntimeException(err);
		}
				
		updateAllControllers();
	}
	
	/** The actual loop over images */
	protected void scanLoop(boolean settingsHadChanged){
		long t0 = System.currentTimeMillis();
		
outImg:	for(int i=0; i < imagesOut.length; i++){
			status = "calc "+ i + " / " + imagesOut.length;
			updateAllControllers();
			
			//if settings have changed again, drop out and 
			//ensureFinalUpdate() will pick it up again
			if(settingsChanged) {
				lastCalcFullyComplete = false;
				break;
			}
			
			int inIdx[] = sourceIndices(calcSelectedImageOnly ? getSelectedSourceIndex() : i); 
				
			int newChangeHash = 0;	
			Img sourceSet[] = new Img[inIdx.length];
			for(int j=0; j < inIdx.length; j++){
				sourceSet[j] = connectedSource.getImage(inIdx[j]);			
				if(sourceSet[j] == null || sourceSet[j].isDestroyed() || !sourceSet[j].isRangeValid()){
					imagesOut[i].invalidate();
					continue outImg;
				}	
				newChangeHash = 31*newChangeHash + sourceSet[j].getChangeID();				
			}
			
			if(settingsHadChanged || newChangeHash != sourceChangeIDHash[i]){
				attemptCalc(imagesOut[i], sourceSet, settingsHadChanged);
				
				sourceChangeIDHash[i] = newChangeHash;
			}
			
			if(abortCalc){
				lastCalcFullyComplete = false;
				throw new RuntimeException("Aborted at " + i + " / " + imagesOut.length);				
			}
			
			if((System.currentTimeMillis() - t0) > controllerUpdatePeriod){
				updateAllControllers();
				t0 = System.currentTimeMillis();
			}
			
			if(i == imagesOut.length-1 && !dependantImagesPending){
				lastCalcFullyComplete = true;	
			}
		}
	}
	
	@Override
	public boolean wasLastCalcComplete(){ return this.lastCalcFullyComplete; }
	
	/** Calls doCalc() with write lock on the output image and catches interruptions */
	protected void attemptCalc(Img imageOut, Img sourceSet[], boolean settingsHadChanged){
		WriteLock writeLock = imageOut.writeLock();
		try{
			writeLock.lockInterruptibly();
			try{
				if(!doCalc(imageOut, writeLock, sourceSet, settingsHadChanged)){
					imageOut.invalidate();					
					return;
				}
				imageOut.imageChanged(false);
				
			}finally{
				writeLock.unlock();
			}
		}catch (InterruptedException e) {
			logr.log(Level.WARNING, "ImgProcPipe.doCalc(): Interrupted waiting for image lock (maybe reading or writing)", e);
			imageOut.invalidate();
		}		
	}
	
	protected abstract boolean doCalc(Img imageOut, WriteLock writeLock, Img sourceSet[], boolean settingsHadChanged) throws InterruptedException;
	
	/** @return the indices of the source images reuired for a given output image */
	protected abstract int[] sourceIndices(int outIdx);

	/** Reallocate the output set as required based on input set. 
	 * @return true if output set was reallocated */
	protected abstract boolean checkOutputSet(int nImagesIn);
	
	protected void invalidateAllOutput(){
		if(imagesOut == null)
			return;
		for(int i=0; i < imagesOut.length; i++){
			if(imagesOut[i] != null){
				imagesOut[i].invalidate(true);
			}
		}
	}
	
	@Override
	public abstract ImgProcPipe clone();

	@Override
	public void destroy() {
		for(int i=0; i < imagesOut.length; i++){
			if(imagesOut[i] != null && !imagesOut[i].isDestroyed())
				imagesOut[i].destroy();
		}
		
		//relase out memory just in case someone holds on to us
		//but if we actually null these, doCalc()s still in the calc queue can get caught out
		imagesOut = (Img[])Array.newInstance(imageClass, 0);		
		sourceChangeIDHash = new int[0];
		super.destroy();
	}
	
	@Override
	public int getNumImages() {
		return (imagesOut != null) ? imagesOut.length : 0;
	}

	@Override
	public Img getImage(int imgIdx) {
		return (imgIdx >= 0 && imgIdx < imagesOut.length) ? imagesOut[imgIdx] : null;
	}
	
	@Override
	public Img[] getImageSet() { return imagesOut; }
	
	public abstract BulkImageAllocation<ByteBufferBackedImage> getBulkAlloc(); 

	public void setDiskMemoryLimit(long maxMemory) { getBulkAlloc().setMaxMemoryBufferAlloc(maxMemory); }
	
	public long getDiskMemoryLimit(){  return getBulkAlloc().getMaxMemoryBufferAlloc(); }

}
