package imageProc.core;

import java.util.List;
import java.util.Map;

public interface ImgSource {
	
			
	/** Registers a sink that needs to be notified of image set changes (this should be called by the sink itself) */ 
	public void addSink(ImgSink sink);
	
	/** Un-registers a sink (this should be called by the sink itself) */ 
	public void removeSink(ImgSink sink);
	
	/** Returns the list of sinks connected to the source */
	public List<ImgSink> getConnectedSinks();
	
	/** Returns the number of images currently available from the source */
	public int getNumImages();
	
	/** Fills in the image info structure, including the data */
	public Img getImage(int imgIdx);
	
	public Img[] getImageSet();
	
	public String toShortString();

	public ImagePipeController createSourceController(Class interfacingClass, Object args[]);

	/** Returns a list of objects stored as meta data for the whole series */
	public Object getSeriesMetaData(String id);
	public void setSeriesMetaData(String id, Object object, boolean isTimeSeries);
	/** Build a map of all emtadata up the source chain */
	public MetaDataMap getCompleteSeriesMetaDataMap();
	/** Add a metadata map to the source's meta data map */
	public void addMetaDataMap(MetaDataMap map);
	/** Add a map of non-time series metadata to the source's meta data map */
	public void addNonTimeSeriesMetaDataMap(Map<String, Object> map);
	/** Add a map of non-time series metadata to the source's meta data map */
	public void addNonTimeSeriesMetaDataMap(String prefix, Map<String, Object> map);
	
	/** Set/get the meta data by iamge index, creating the array as necessary */
	public void setImageMetaData(String id, int imageIndex, Object data);
	public Object getImageMetaData(String id, int imageIndex);
	
		
	public ImgSource clone();

	/** Broadcasts the given event to all downstream sources and sinks */ 
	public void broadcastEvent(EventReciever.Event event);
	
	public List<ImagePipeController> getControllers();
	
	/** Called by Img only */
	void imageRangesDone(int idx);

	/** True only when nothing has changed and everything is ready and synced */
	public boolean isIdle();
}
