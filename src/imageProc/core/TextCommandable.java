package imageProc.core;

import java.io.PrintWriter;

/** Source/sinks that can accept simply text commands (e.g. via the PilotComm interface) */
public interface TextCommandable {

	/** Receives text commands from other modules, e.g. PilotStatusComm.
	 * @param command Text command as recieved
	 * @param pwReturn Writer connected to the return channel (e.g. TCP/IP socket).
	 * 
	 * This is called from the comms thread, so don't take too long (< 1 second).
	 * If using a worker thread to do something, synchronise on the pwReturn before sending anything. 
	 */
	public void command(String command, PrintWriter pwReturn);
	
}
