package imageProc.core;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import net.jafama.FastMath;


/** Image storing complex values (the magnitude is returned by the default interface) 
 */ 
public class ComplexImage extends ByteBufferBackedImage {
	
	@Override
	public ComplexImage createFromTemplate(ImgSource source,  int sourceIndex, BulkImageAllocation bulkAlloc, ByteBuffer buffer, boolean init) {
		return new ComplexImage(source,  sourceIndex, this, bulkAlloc, buffer, init);
	}
	
	public ComplexImage(ImgSource source, int sourceIndex, ComplexImage template, BulkImageAllocation bulkAlloc, ByteBuffer imageBuffer, boolean init) {
		super(source, sourceIndex);
		
		this.width = template.width;
		this.height = template.height;
		this.headerSize = template.headerSize;
		this.footerSize = template.footerSize;
		
		this.bulkAlloc = bulkAlloc;
		this.imageBuffer = imageBuffer;  
		if(init){
			replaceData(imageBuffer);
		}else{
			min = Double.NaN; 
			max = Double.NaN;
			sum = Double.NaN;
		}
	}
	
	public ComplexImage(ImgSource source, int sourceIndex, int width, int height, boolean init){
		super(source, sourceIndex);
		this.width = width;
		this.height = height;
		this.bulkAlloc = null;
		
		if(source == null){
			this.imageBuffer = null; //template only
			return; 
		}
	}
	
	@Override
	public double getMaxPossibleValue() {
		return Double.POSITIVE_INFINITY;
	}
	
	@Override
	public final double getPixelValue(ReadLock readLock, int x, int y) {
		return getAbs(readLock, x, y);
	}
	
	@Override
	public double getPixelValue(ReadLock readLock, int field, int x, int y) {
		switch(field){
			case 0: return getAbs(readLock, x, y);
			case 1: return getReal(readLock, x, y);
			case 2: return getImag(readLock, x, y);
			default: throw new IndexOutOfBoundsException("Invalid field");
		}
	}
	
	public final double getAbs(ReadLock readLock, int x, int y) {
		if(destroyed) return Double.NaN;
		if(readLock != lock.readLock()){ // we don't actually test it, we did just require that the caller thought about it
			throw new RuntimeException("ComplexImage.getPixelValue(): Reading image with no read or write lock!");
		}
		double r = imageBuffer.getDouble((y*width+x)*16);
		double i = imageBuffer.getDouble((y*width+x)*16+8);
		return FastMath.sqrt(r*r + i*i);		
	}
	
	public final double getReal(ReadLock readLock, int x, int y) {
		if(destroyed) return Double.NaN;
		if(readLock != lock.readLock()){ // we don't actually test it, we did just require that the caller thought about it
			throw new RuntimeException("ComplexImage.getPixelValue(): Reading image with no read or write lock!");
		}
		
		return destroyed ? Double.NaN : imageBuffer.getDouble((y*width+x)*16); 
	}
	
	public final double getImag(ReadLock readLock, int x, int y) {
		if(destroyed) return Double.NaN;
		if(readLock != lock.readLock()){ // we don't actually test it, we did just require that the caller thought about it
			throw new RuntimeException("ComplexImage.getPixelValue(): Reading image with no read or write lock!");
		}
		
		return destroyed ? Double.NaN : imageBuffer.getDouble((y*width+x)*16+8); 
	}

	
	public void setPixelValue(int x, int y, double real, double imag) {
		if(destroyed) return;
		imageBuffer.putDouble((y*width+x)*16, real);
		imageBuffer.putDouble((y*width+x)*16+8, imag);
	}
	
	public void setPixelValue(int x, int y, double val) {
		throw new RuntimeException("Not implemented"); //because it doesn't make sense
	}
	
	public void fillFromJTransformsPackedData(WriteLock writeLock, double[] data, boolean freqSpace) {
		fillFromJTransformsPackedData(writeLock, data, freqSpace, width, height);
	}

	/**
	 * @param data
	 * @param freqSpace If true, the (0,0) frequency is put at the image centre, otherwise the image has (0,0) at (0,0)
	 * @param fftW, fftH 	Width and height of the FFT data from JTransforms 
	 * @throws InterruptedException
	 */
	public void fillFromJTransformsPackedData(WriteLock writeLock, double[] data, boolean freqSpace, int fftW, int fftH) {
		if(destroyed) return;
		
		//JTansforms packet FFT complex data looks like this:
		//pr00,pi00,pr12,pi12...
		//and then rows go [+ve cols, -ve cols, +ve cols ....]
		//and then the block of rows pairs are [ +++++, ------] in y
		
		
		//sorry for the mess...
		if(freqSpace){
			for(int y=0; y < (Math.min(fftH, height)/2); y++){
				for(int x=0; x <(Math.min(fftW, width)/2); x++){
					//+ve x, +ve y
					int i = (y+(height/2))*width+(width/2)+x;
					imageBuffer.putDouble(i*16+0, data[2*y*fftW + x*2]);
					imageBuffer.putDouble(i*16+8, data[2*y*fftW + x*2+1]);
					
					//+ve x, -ve y
					i = (height/2-y-1)*width+(width/2)+x;
					imageBuffer.putDouble(i*16+0, data[2*((fftH/2-y-1)+(fftH/2))*fftW + x*2]);
					imageBuffer.putDouble(i*16+8, data[2*((fftH/2-y-1)+(fftH/2))*fftW + x*2+1]);
					
					//-ve x, +ve y
					i = (y+(height/2))*width+(width/2-x-1);
					imageBuffer.putDouble(i*16+0, data[(2*y+1)*fftW + (fftW/2-x-1)*2]);
					imageBuffer.putDouble(i*16+8, data[(2*y+1)*fftW + (fftW/2-x-1)*2+1]);
					//-ve x, -ve y
					i = (height/2-y-1)*width+(width/2-x-1);
					imageBuffer.putDouble(i*16+0, data[(2*((fftH/2-y-1)+(fftH/2))+1)*fftW + (fftW/2-x-1)*2]);
					imageBuffer.putDouble(i*16+8, data[(2*((fftH/2-y-1)+(fftH/2))+1)*fftW + (fftW/2-x-1)*2+1]);
				}	
			}
		}else{
			for(int y=0; y < Math.min(fftH, height); y++){
				for(int x=0; x <  Math.min(fftW, width); x++){
					//-ve x, -ve y
					imageBuffer.putDouble((y*width+x)*16+0, data[y*fftW*2 + x*2]); //real
					imageBuffer.putDouble((y*width+x)*16+8, data[y*fftW*2 + x*2+1]); //imag
				}
			}
		}
	}


	@Override
	public int getImageDataSize() { return width * height * 16; };

	@Override
	/** nField = 3 for complex images. It's actually kind of 2, but is interlaced in the pixels and we give abs,Re,Im as frames */
	public int getNFields() { return 3; }
}
