package imageProc.core;

/** Something which acts like a database to save data */
public interface DatabaseSink extends ImgSink {
	
	/** Save under given ID */
	public void triggerSave(int id);
	
	/** Save under next free ID */
	public void triggerSave();
	
}
