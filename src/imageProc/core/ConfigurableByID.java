package imageProc.core;


/** For modules with configuration that can be stored/loaded in some kind of database
 * 
 * For the moment, the database type is also included in the ID and knowledge of what is used is needed in the caller and the implementor
 * e.g.
 *  gmds/DB/pulse for GMDS based saving
 *  jsonFile:/path/to/file
 *  w7x:fullDescriptorPath
 *  
 *  In general implementors should assemble things in some kind of java map first, and then save that, so break it up is easier later
 * 
 * @author oliford
 */
public interface ConfigurableByID {

	public void loadConfig(String id);

	public void saveConfig(String id);

	public String getLastLoadedConfig();
	
}
