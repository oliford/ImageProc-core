package imageProc.core;

import imageProc.core.EventReciever.Event;

/**
 * Anything sink that runs through a set of input images, processing them in some way.
 * Implements some basic functions like status, autocalc, etc.
 * 
 * Modules might also be a source that produce an output set of images and these should extend ImgProcPipe or ImgProcPipeMultithread instead.
 * 
 * @author oliford
 */
public abstract class ImgProcessor extends ImgSourceOrSinkImpl implements ImgSink, EventReciever {
	
	/** Automatically process images when a changed image notification is recieved */
	protected boolean autoCalc = true;
	
	/** Anything changed that should cause a full reprocess */
	protected boolean settingsChanged = false;
	
	/** Status, including calc progress */
	protected String status;
	
	/** Flag to ask calculating thread to abort at the next opportunity */
	protected boolean abortCalc = false;
	
	/** Flag to indicate that the last calculation successfully completed. False if in progress or failed */
	protected boolean lastCalcFullyComplete = false;
	
	/** If true, only the 'selected' image of the source is processed
	 * otherwise they all are and the same number of output iamges is given. */
	protected boolean calcSelectedImageOnly = false;
	
	
	public void invalidate() {
		settingsChanged = true;
	}
	
	public void forceCalc(){
		invalidate();
		calc();
	}
		
	@Override
	public void calc() {
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalcScan(); } });
	}
	
	protected void preCalc(boolean settingsHadChanged){ 
		//empty default implementation		
	}
	
	protected void postCalc(boolean settingsHadChanged){ 
		//empty default implementation		
	}
	
	/** Guaranteed to be called only once at a time */
	protected abstract void doCalcScan();
		
	@Override
	public boolean wasLastCalcComplete(){ return this.lastCalcFullyComplete; }
	
	@Override	
	public void setAutoUpdate(boolean autoCalc) {
		if(!this.autoCalc && autoCalc){
			this.autoCalc = true;
			updateAllControllers();
			calc();			
		}else{
			this.autoCalc = autoCalc;
			updateAllControllers();
		}
	}
	
	@Override	
	public boolean getAutoUpdate() { return this.autoCalc; }

	public void setCalcSelectedImageOnly(boolean calcSelectedImageOnly) {
		this.calcSelectedImageOnly = calcSelectedImageOnly;
		updateAllControllers();
		if(autoCalc)
			calc();
	}
	public boolean isCalcSelectedImageOnly() { return this.calcSelectedImageOnly; }

	@Override
	public boolean isIdle() { 
		return !ImageProcUtil.isUpdateInProgress(this);
	}
	
	@Override
	public void event(Event event) {
		if(event == Event.GlobalAbort)
			abortCalc();
	}
	
	@Override
	public void abortCalc(){ 
		abortCalc = true;
	}

	@Override
	public void imageChangedRangeComplete(int idx) { 
		if(autoCalc){
			calc();
		}
 	}

	@Override
	public void notifySourceChanged() {
		super.notifySourceChanged();
		if(autoCalc)
			calc();
	}

	@Override
	public void setSource(ImgSource source) {
		super.setSource(source);
		calc();
	}
	
	public String getStatus() { return status; }

	@Override
	protected void selectedImageChanged() {
		if(calcSelectedImageOnly && autoCalc){
			calc();
		}
	}
	
	@Override
	public abstract ImgProcessor clone();

}
