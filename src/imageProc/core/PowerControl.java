package imageProc.core;

/** Something which allows switching on and off of devices */
public interface PowerControl {
	
	/** Checks that a specific thing is switched on (by given ID)
	 * @param id ID of thing to turn on, or null for everything
	 * @return True if the power was turned on (i.e. was previously off) */
	public boolean switchOn(String id);
	
	/** Checks that a specific thing is switched on (by given ID)
	 * @param id ID of thing to turn off, or null for everything
	 * @return True if the power was turned on (i.e. was previously off) */
	public boolean switchOff(String id);
	
	/** Determines if a specific thing is switched on (by given ID)
	 * @param id ID of thing to check, or null for everything
	 * @return True if the power is on */
	public boolean isOn(String id);
}
