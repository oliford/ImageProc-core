package imageProc.core;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Logger;

import algorithmrepository.exceptions.NotImplementedException;

/** Image described by byte data in a (preferably direct) byte buffer */
public class ByteBufferImage extends ByteBufferBackedImage {
	public static final int DEPTH_DOUBLE = -64;
	public static final int DEPTH_FLOAT = -32;

	/** Bit depth of image, -ve for floating point (so -64 for double, -32 for float) */
	protected int bitDepth;
	
	@Override
	public ByteBufferImage createFromTemplate(ImgSource source,  int sourceIndex, BulkImageAllocation bulkAlloc, ByteBuffer buffer, boolean init) {		
		return new ByteBufferImage(source, sourceIndex, this, bulkAlloc, buffer, init);
	}
	
	public ByteBufferImage(ImgSource source, int sourceIndex, ByteBufferImage template, BulkImageAllocation bulkAlloc, ByteBuffer imageBuffer, boolean init) {
		super(source, sourceIndex);
		
		this.bitDepth = template.bitDepth;
		this.width = template.width;
		this.height = template.height;
		this.fields = template.fields;
		this.headerSize = template.headerSize;
		this.footerSize = template.footerSize;
		this.byteOrder = template.byteOrder;
		
		this.bulkAlloc = bulkAlloc;
		this.imageBuffer = imageBuffer;
		imageBuffer.order(template.byteOrder);
		if(init){
			replaceData(imageBuffer);
		}else{
			min = Double.NaN; 
			max = Double.NaN;
			sum = Double.NaN;
		}
	}

	public ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth) {
		this(source, sourceIndex, width, height, bitDepth, 0, 0, true);
	}
		
	public ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth, int headerSize, int footerSize) {
		this(source, sourceIndex, width, height, bitDepth, headerSize, footerSize, true);
	}

	public ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth, boolean init) {
		this(source, sourceIndex, width, height, 1, bitDepth, 0, 0, init);
	}
	
	public ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int fields, int bitDepth, boolean init) {
		this(source, sourceIndex, width, height, fields, bitDepth, 0, 0, init);
	}
	
	public ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth, int headerSize, int footerSize, boolean init) {
		this(source, sourceIndex, width, height, 1, bitDepth, headerSize, footerSize, init);
	}
	
	public ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int fields, int bitDepth, int headerSize, int footerSize, boolean init) {
		super(source, sourceIndex);

		this.bitDepth = bitDepth;
		this.width = width;
		this.height = height;
		this.fields = fields;
		this.headerSize = headerSize;
		this.footerSize = footerSize;
		this.bulkAlloc = null;
		
		if(source == null){
			this.imageBuffer = null; //template only
			return; 
		}
		
		//imageBuffer = DirectBufferControl.allocateDirect(width * height * getBytesPerPixel());		
		this.imageBuffer = ByteBuffer.allocate(width * height * fields * getBytesPerPixel() + headerSize + footerSize);
		if(init){
			replaceData(imageBuffer);
		}else{
			min = Double.NaN; 
			max = Double.NaN;
			sum = Double.NaN;
		}

	}
	
	protected ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth, int headerSize, int footerSize, 
			ByteBuffer imageBuffer) {
		this(source, sourceIndex, width, height, bitDepth, headerSize, footerSize, imageBuffer, false);
	}

	/** @param noInit If true, ranges are not calculated and no modification notification is made */
	protected ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth, int headerSize, int footerSize, 
			ByteBuffer imageBuffer, boolean init) {
		super(source, sourceIndex);

		this.width = width;
		this.height = height;
		this.fields = 1;
		this.bitDepth = bitDepth;
		this.imageBuffer = imageBuffer;
		this.destroyed = (imageBuffer == null);
		this.headerSize = headerSize;
		this.footerSize = footerSize;
				
		if(init){
			replaceData(imageBuffer);
		}else{
			min = Double.NaN; 
			max = Double.NaN;
			sum = Double.NaN;
		}
	}

	/** Copy constructor used by some derived classes e.g. ComplexImage */
	protected ByteBufferImage(ByteBufferImage img) {
		super(img.source, img.sourceIndex);
		this.width = img.width;
		this.height = img.height;
		this.fields = 1;
		this.bitDepth = img.bitDepth;
		this.imageBuffer = img.imageBuffer;
		this.destroyed = img.destroyed;
		
		throw new NotImplementedException();//erm, yea, we can't share buffers like this anymore
		
		/*if(img.allocBuffer == null){ //them and us are now sharing buffers
			img.allocBuffer = imageBuffer;
			img.allImagesInAlloc = new LinkedList<ByteBufferImage>();
			img.allImagesInAlloc.add(img);
		}               
		this.allocBuffer = img.allocBuffer;
		this.allImagesInAlloc = img.allImagesInAlloc;
		allImagesInAlloc.add(this);*/
	}

	public ByteBufferImage(ImgSource source, int sourceIndex, short shortArray[][]) {
		super(source, sourceIndex);

		this.height = shortArray.length;
		this.width = shortArray[0].length;
		this.fields = 1;
		this.bitDepth = 16;
				
		//ByteBuffer buf = DirectBufferControl.allocateDirect(width*height*getBytesPerPixel());
		ByteBuffer buf = ByteBuffer.allocate(width*height*getBytesPerPixel());
		buf.order(ByteOrder.LITTLE_ENDIAN);
		ShortBuffer sBuff = buf.asShortBuffer();
		for(int iY=0; iY < height; iY++) {			
			sBuff.put(shortArray[iY]);
		}
		replaceData(buf);
	}

	public ByteBufferImage(ImgSource source, int sourceIndex, int intArray[][]) {
		super(source, sourceIndex);

		this.height = intArray.length;
		this.width = intArray[0].length;
		this.fields = 1;
		this.bitDepth = 32;
				
		//ByteBuffer buf = DirectBufferControl.allocateDirect(width*height*getBytesPerPixel());
		ByteBuffer buf = ByteBuffer.allocate(width*height*getBytesPerPixel());
		buf.order(ByteOrder.LITTLE_ENDIAN);
		IntBuffer iBuff = buf.asIntBuffer();
		for(int iY=0; iY < height; iY++) {			
			iBuff.put(intArray[iY]);
		}
		replaceData(buf);
	}

	public ByteBufferImage(ImgSource source, int sourceIndex, double dblArray[][]) {
		super(source, sourceIndex);
		this.height = dblArray.length;
		this.width = dblArray[0].length;
		this.fields = 1;
		this.bitDepth = DEPTH_DOUBLE;

		//ByteBuffer buf = DirectBufferControl.allocateDirect(width*height*getBytesPerPixel());
		ByteBuffer buf = ByteBuffer.allocate(width*height*getBytesPerPixel());
		buf.order(ByteOrder.LITTLE_ENDIAN);
		DoubleBuffer dBuff = buf.asDoubleBuffer();
		for(int iY=0; iY < height; iY++) {                      
			dBuff.put(dblArray[iY]);
		}
		replaceData(buf);
	}

	public ByteBufferImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth, byte data[]) {
		super(source, sourceIndex);

		this.width = width;
		this.height = height;
		this.bitDepth = bitDepth;
		this.fields = 1;
		replaceData(data);
	}
	
	public void replaceData(byte data[]){
		if(data.length != width * height * getBytesPerPixel())
				throw new IllegalArgumentException("Image data length " +data.length+ 
						" invalid for "+width+" x "+height+" x "+bitDepth+" image");
		imageBuffer = ByteBuffer.wrap(data);
		imageBuffer.position(0);
		imageBuffer.limit(imageBuffer.capacity());
		
		imageChanged(true);
	}

	public int getBytesPerPixel() {
		return getBytesPerPixel(bitDepth);
	}
	
	public static int getBytesPerPixel(int bitDepth) {
		if(bitDepth > 0 && bitDepth <= 8){
			return 1;
		}else if(bitDepth > 8 && bitDepth <= 16){
			return 2;
		}else if(bitDepth > 16 && bitDepth <= 32){
			return 4;
		}else if(bitDepth == DEPTH_FLOAT){
			return 4;
		}else if(bitDepth == DEPTH_DOUBLE){
			return 8;
		}else
			throw new IllegalArgumentException("Invalid bit depth " + bitDepth);
	}

	
	public int getBitDepth() {	return bitDepth; }

	@Override
	public double getMaxPossibleValue() {
		if(bitDepth == DEPTH_DOUBLE) return Double.MAX_VALUE;
		if(bitDepth == DEPTH_FLOAT) return Float.MAX_VALUE;
		return (int)(Math.pow(2, bitDepth) - 1);
	}

	public double getPixelValue(WriteLock writeLock, int x, int y) {
		if(writeLock != lock.writeLock())
			throw new RuntimeException("ByteBufferImage.getPixelValue(WriteLock): Write lock is not valid (using it as a read lock)");
		return getPixelValue(lock.readLock(), 0, x, y);
	}
	
	// A valid writeLock is ok for reading too
	public double getPixelValue(WriteLock writeLock, int field, int x, int y) {
		if(writeLock != lock.writeLock())
			throw new RuntimeException("ByteBufferImage.getPixelValue(WriteLock): Write lock is not valid (using it as a read lock)");
		return getPixelValue(lock.readLock(), field, x, y);
	}

	@Override
	public double getPixelValue(ReadLock readLock, int x, int y) {
		return getPixelValue(readLock, 0, x, y);
	}
	
	@Override
	public double getPixelValue(ReadLock readLock, int field, int x, int y) {
		if(destroyed)
			return Double.NaN;
		
		if(readLock != lock.readLock()){ // we don't actually test it, we did just require that the caller thought about it
			throw new RuntimeException("ByteBufferImage.getPixelValue(ReadLock): No read lock or not valid");
		}

		if(bitDepth > 0 && bitDepth <= 8){
			byte signedVal = imageBuffer.get(headerSize + field*width*height +  y * width + x);
			return signedVal < 0 ? (0x100 + signedVal) : signedVal;
			//return (double)imageBuffer.get(field*width*height +  y * width + x);

		}else if(bitDepth > 8 && bitDepth <= 16){
			short signedVal = imageBuffer.getShort(headerSize + (field*width*height + y * width + x) * 2);
			return (double)(signedVal < 0 ? (0x10000 + signedVal) : signedVal);
			//return (double)signedVal;

		}else if(bitDepth > 16 && bitDepth <= 32){
			return (double)imageBuffer.getInt(headerSize +(field*width*height + y * width + x) * 4);

		}else if(bitDepth == DEPTH_FLOAT){
			return (double)imageBuffer.getFloat(headerSize +(field*width*height + y * width + x)*4);

		}else if(bitDepth == DEPTH_DOUBLE){
			return (double)imageBuffer.getDouble(headerSize +(field*width*height + y * width + x)*8);

		}else{
			throw new IllegalArgumentException("Invalid bit depth");
		}		
	}
	   
	public void setPixelValue(WriteLock writeLock, int x, int y, double value) {
		setPixelValue(writeLock, 0, x, y, value);
	}
	
    public void setPixelValue(WriteLock writeLock, int field, int x, int y, double value) {	
    	if(destroyed) return;
    	if(writeLock != lock.writeLock() || !writeLock.isHeldByCurrentThread())
    		throw new RuntimeException("Img.setPixelValue() called without write lock");

    	if(bitDepth > 0 && bitDepth <= 8){
    		imageBuffer.put(headerSize +field*width*height + y * width + x, (byte)value);
			
    	}else if(bitDepth > 8 && bitDepth <= 16){
    		imageBuffer.putShort(headerSize +(field*width*height + y * width + x)*2, (short)value);

    	}else if(bitDepth > 16 && bitDepth <= 32){
    		imageBuffer.putInt(headerSize +(field*width*height + y * width + x)*4, (int)value);

    	}else if(bitDepth == DEPTH_FLOAT){
    		imageBuffer.putFloat(headerSize +(field*width*height + y * width + x)*4, (float)value);

    	}else if(bitDepth == DEPTH_DOUBLE){
    		imageBuffer.putDouble(headerSize +(field*width*height + y * width + x)*8, value);

    	}else{
			throw new IllegalArgumentException("Invalid bit depth");
		}
    	rangeValid = RangeValidity.invalid;
	}
	
	@Override
	/** More or less the same as the default implementation Img.calcRange()
	 * but does integer math in the hope that it's faster */
	public void calcRanges(){
		super.calcRanges(); 
		if(true)
			return;
		
		//need to update the syncing and read locking etc
		if(bitDepth == DEPTH_DOUBLE){
			super.calcRanges(); //it's double, so do the normal one because we used integer arithmetic here
		}

		if(rangeValid == RangeValidity.inCalc)
			logr.fine("calcRanges() called during range calc");
		else if(rangeValid == RangeValidity.valid){
			//logr.fine("calcRanges() called when ranges already valid");
			return;
		}

		if(destroyed){
			min = Double.NaN; 
			max = Double.NaN;
			sum = Double.NaN;
			return;
		}
		
		rangeValid = RangeValidity.inCalc;
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		long sum = 0;

		//do the math, working through the appropriate buffer
		// (we do it all in ints here)
		for(int i=0; i < height*width; i++) {
			int val;
			if(bitDepth > 0 && bitDepth <= 8){
				val = imageBuffer.get(headerSize +i);
			}else if(bitDepth > 8 && bitDepth <= 16){
				
				try{
					val = imageBuffer.getShort(headerSize + i*2);
					val = (int)(val < 0 ? (0x10000 + val) : val);
				}catch(IndexOutOfBoundsException err){
					System.err.println(err.getMessage());
					val=0;
				}
			}else if(bitDepth > 16 && bitDepth <= 32){
				val = imageBuffer.getInt(headerSize +i*4);
				
			}else if(bitDepth == DEPTH_DOUBLE){
				//this should never happen
				synchronized (rangeValid) {				
					if(rangeValid == RangeValidity.inCalc)
						rangeValid = RangeValidity.invalid;
					return;
				}
			
			}else{				
				throw new IllegalArgumentException("Invalid bit depth");
			}

			if(val > max) max = val;
			if(val < min) min = val;
			sum += val;
			
			if(rangeValid != RangeValidity.inCalc)
				return; //give up with this, someone did it, or invalidated it
		}	

		this.min = min;
		this.max = max;
		this.sum = sum;
		synchronized (rangeValid) {
			if(rangeValid == RangeValidity.inCalc) //if it hasn't been invalidated mid-calc
				rangeValid = RangeValidity.valid;
		}
	
	}
	
	@Override
	public String toString() {
		return super.toString() + "x" + bitDepth + " [" + min + "-" + max + "]";
	}

	@Override
	public int getImageDataSize() { 
		return height * width * fields * getBytesPerPixel() + headerSize + footerSize;
	}

	public int getHeaderSize() { return headerSize;	}
	public int getFooterSize() { return footerSize;	}
	
}
