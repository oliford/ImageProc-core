package imageProc.core;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.RandomManager;
import otherSupport.SettingsManager;

/** Multithreaded version of ImgProcPipe.
 * 
 * doCalc() will be called from different threads at the same time,
 * so derived classes must be careful not to conflict
 * their own member variables in doCalc()
 * 
 *  Almost works, but seems to leave a few holes :/
 */
public abstract class ImgProcPipeMultithread extends ImgProcPipe {

	private long lastControllerUpdate;
	
	/** nThreads <= 1 --> singleThreaded */
	private int nThreads;
	private boolean settingsHadChanged;
	
	protected boolean randomiseOrder = true;
	
	private class CalcLoopRunner implements Runnable {
		/** Things for our termination/interruption */
		public boolean death;
		public Thread thread;
		
		@Override
		public void run() {
			death = false;
			try{
				thread = Thread.currentThread();
					
				while(!todoOutIndices.isEmpty() && !death && !abortCalc){
					Integer outIdx = todoOutIndices.poll();
					doSingle(settingsHadChanged, outIdx);
					
				}
			}finally{
				thread = null; //no thread is running this anymore
			}
		}
		
	}
	public ImgProcPipeMultithread(Class imageClass) {
		super(imageClass);
		try{
			String threadsSetting = SettingsManager.defaultGlobal().getProperty("imageProc.ImgProcPipe.nThreads");		
			nThreads = Integer.parseInt(threadsSetting);
			
		}catch(RuntimeException err){
			ThreadPoolExecutor exec = (ThreadPoolExecutor)ImageProcUtil.getThreadPool();
			nThreads = exec.getCorePoolSize() - exec.getActiveCount(); //leave at least 1 free for GUI stuff
			nThreads = Math.min(nThreads, 10); //10 as upper limit
		}
	}
	
	public ImgProcPipeMultithread(Class imageClass, ImgSource source, int selectedIndex) {
		this(imageClass);
		setSource(source);
		setSelectedSourceIndex(selectedIndex);
	}

	/** List of indices of the output images that are still to be done */ 
	ConcurrentLinkedQueue<Integer> todoOutIndices = new ConcurrentLinkedQueue<Integer>();

	ArrayList<CalcLoopRunner> runners = new ArrayList<CalcLoopRunner>();
		
	/** Cancelled all queued indices and interrupts and kills all runners 
	 * @param waitForDeath	Only return once all runners have exited and processing fully stopped
	 * @param kill		True: interrupt and signal abort, false: simply wait for everything to stop
	 */
	private void cancelAndStopAll(boolean waitForDeath, boolean kill) {
		//clear the image queue
		todoOutIndices.clear();
		
		boolean oneStillLives;
		do{
			oneStillLives = false;
			
			for(CalcLoopRunner runner : runners){
				if(kill)
					runner.death = true;				
				if(runner.thread != null && runner.thread.isAlive()){					
					try{
						if(kill)
							runner.thread.interrupt();
						oneStillLives = true;
					}catch(NullPointerException err){ 
						//it dies in the meantime
					}
					
				}
			}
			
		}while(waitForDeath && oneStillLives);
	}
	
	/** The actual loop over images */
	@Override
	protected void scanLoop(boolean settingsHadChanged){
		cancelAndStopAll(true, true); //clear and remaining runners first
		
		this.settingsHadChanged = settingsHadChanged;
		
		if(nThreads > 1){
			
			if(randomiseOrder){
				ArrayList<Integer> idxs = new ArrayList<Integer>();
				for(int i=0; i < imagesOut.length; i++){
					idxs.add(i);
				}
				//always do image 0 first, just cos
				todoOutIndices.add(idxs.remove(0));
				while(!idxs.isEmpty()){
					int  i = (int)RandomManager.instance().nextUniform(0, idxs.size());
					int idx = idxs.remove(i);
					todoOutIndices.add(idx);
				}
			}else{
				for(int i=0; i < imagesOut.length; i++){
					todoOutIndices.add(i);
				}
			}
			
			lastControllerUpdate = System.currentTimeMillis();
			
			ThreadPoolExecutor exec = (ThreadPoolExecutor)ImageProcUtil.getThreadPool();
			for(int i=0; i < nThreads; i++){
				CalcLoopRunner runner = new CalcLoopRunner();
				runners.add(runner);
				exec.execute(runner);
			}
			
			//do some here too
			while(!todoOutIndices.isEmpty() && !abortCalc){
				Integer outIdx = todoOutIndices.poll();
				doSingle(settingsHadChanged, outIdx);
			}
			
			//wait for the remaining processors to finish (cleanly), dont kill them
			cancelAndStopAll(true, false);			
		}else{
			//no multithreading
			for(int i=0; i < imagesOut.length; i++){
				doSingle(settingsHadChanged, i);
				if(abortCalc)
					break;
			}
		}
		
	}
	
	/** Run by multiple threads! */
	private void doSingle(boolean settingsHadChanged, int outIdx){
		status = "calc "+ outIdx + ": " + todoOutIndices.size() + " / " + imagesOut.length + " remaining";
		updateAllControllers();
		
		//if settings have changed again, drop out and 
		//ensureFinalUpdate() will pick it up again
		if(settingsChanged) {
			lastCalcFullyComplete = false;
			return; //this was a break
		}
		
		int inIdx[] = sourceIndices(calcSelectedImageOnly ? getSelectedSourceIndex() : outIdx); 
			
		int newChangeHash = 0;	
		Img sourceSet[] = new Img[inIdx.length];
		for(int j=0; j < inIdx.length; j++){
			sourceSet[j] = connectedSource.getImage(inIdx[j]);			
			if(sourceSet[j] == null || sourceSet[j].isDestroyed() || !sourceSet[j].isRangeValid()){
				Logger.getLogger(this.getClass().getName()).warning("Output image #" + outIdx + " included invalid input image #" + sourceSet[j].getSourceIndex());
				imagesOut[outIdx].invalidate();
				return; //next index
			}	
			newChangeHash = 31*newChangeHash + sourceSet[j].getChangeID();				
		}
		
		if(settingsHadChanged || newChangeHash != sourceChangeIDHash[outIdx]){
			attemptCalc(imagesOut[outIdx], sourceSet, settingsHadChanged);
			
			sourceChangeIDHash[outIdx] = newChangeHash;
		}
		
		if(abortCalc){
			lastCalcFullyComplete = false;
			throw new RuntimeException("Aborted on " + outIdx + ", with " + todoOutIndices.size() + " / " + imagesOut.length + " remaining");
		}
		
		if((System.currentTimeMillis() - lastControllerUpdate) > controllerUpdatePeriod){
			updateAllControllers();
			lastControllerUpdate = System.currentTimeMillis();
		}
		
		if(outIdx == imagesOut.length-1){
			lastCalcFullyComplete = true;					
		}
	}

	@Override
	public void event(Event event) {
		if(event == Event.GlobalAbort)
			cancelAndStopAll(false, true);
		
		super.event(event);
	}

	public int getNumThreads() { return nThreads; }
	
	public void setNumThreads(int nThreads) { this.nThreads = nThreads; }
	
}
