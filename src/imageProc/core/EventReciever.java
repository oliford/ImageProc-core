package imageProc.core;

/** Anything which get commands/events from the source/pilot
 * that are called down the entire source/pipe/sink tree
 *  */ 
public interface EventReciever {
	
	public static enum Event {
		/** Called when the user hits the big global start button */ 
		GlobalStart,
		
		/** Called when:
		 * 1) the user hits the big global abort button 
		 * 2) A pilot wants the module or anything to stop doing things and return to a idle state*/ 
		GlobalAbort,
		
		/** Called when an AcquisitionDevice image source initialises the images */
		AcquisitionInit,
		
		/** Called when an AcquisitionDevice starts collecting images (i.e. the first image has arrived) */
		AcquisitionStarted,
		
		/** Called when an AcquisitionDevice completed or aborts */
		AcquisitionStopped,
		
		/** Called before a save operation */
		PreSave,
		
		/** Called after a save operation */
		PostSave,
		
		/** Called to process */
		Process,		
	}

	public abstract void event(Event event);
}
