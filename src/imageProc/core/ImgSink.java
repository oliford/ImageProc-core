package imageProc.core;

import java.util.List;

public interface ImgSink {
	
	public void setSource(ImgSource source);
	
	public ImgSource getConnectedSource();
	
	public void setSelectedSourceIndex(int index);
	
	public int getSelectedSourceIndex();
	
	/** Notifications from the connected source, that one or more of the
	 * images has changed (to a different image, this isn't called when
	 * an image's content changes) */
	public void notifySourceChanged();
	
	/**
	 * @param interfacingClass	Composite.class for an SWT controller
	 * @param args	Depends on interfacingClass
	 * @param controlSink true is the requester is looking at a sink, false if a source
	 * @return
	 */
	public ImagePipeController createSinkController(Class interfacingClass, Object args[]);
	
	public List<ImagePipeController> getControllers();
	
	/** Called to say an image has been change. This is called from the core when the change is notified
	 * by the source. The ranges will most likely not have been calculated yet and implementations of this 
	 * function should return very quickly after a maximum of a few variable changes since it will be
	 * called from the low-level acqusition code.
	 * 
	 *  For slower things, use imageChangedRangeComplete()
	 * 
	 * @param idx
	 */
	public void imageChangedLowLevel(int idx);
	
	/** Called to say an image in the source has changed and the range calculation is complete.
	 * This is not necessarily called for all changes. i.e. many will be missed if CPU is loaded.
	 * @param idx Image number in source
	 */
	public void imageChangedRangeComplete(int idx);
	
	/** True only when nothing has changed and everything is ready and synced */
	public boolean isIdle();
	
	/** Gets flag to process on change of source */
	public boolean getAutoUpdate();
	
	/** Sets flag to process on change of source */
	public void setAutoUpdate(boolean enable);
	
	/** Start processing now */
	public void calc();
	
	/** Did the last calc complete for all images */
	public boolean wasLastCalcComplete();
	
	/** Abort any calc in progress */
	public void abortCalc();
}
