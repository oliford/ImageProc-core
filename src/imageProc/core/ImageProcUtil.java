package imageProc.core;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;

import com.google.gson.Gson;

import fusionDefs.sensorInfo.SensorInfo;
import fusionDefs.sensorInfo.SensorInfoService;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import otherSupport.SettingsManager;

/** Global/utility stuff for ImageProc */
public abstract class ImageProcUtil {
	
	private static Logger logr = Logger.getLogger(ImageProcUtil.class.getName());

	/** SWT Display object of main message loop */
	private static Display display; 
	
	/** The main log file we are using for the java logger for this JVM instance */
	private static String logFile;
	
	private static Class preferredSettingsControlClass = JSONFileSettingsControl.class;
	
	private static ExecutorService pool;
	private static ScheduledExecutorService scheduledPool;
	private static LinkedBlockingQueue<Runnable> workQueue;
	
	public static ExecutorService getThreadPool(){
		if(pool == null){
			workQueue = new LinkedBlockingQueue<Runnable>(100);
			pool = new ThreadPoolExecutor(
							Math.max(4, Runtime.getRuntime().availableProcessors()),
							Math.max(4, 2*Runtime.getRuntime().availableProcessors()),
							1000,
							TimeUnit.SECONDS,
							workQueue);
		}
		return pool;
	}
	
	public static ScheduledExecutorService getScheduledThreadPool(){
		if(scheduledPool == null){			
			scheduledPool = new ScheduledThreadPoolExecutor(5);
		}
		return scheduledPool;
	}
	
	public static final void queueExec(Runnable runnable){
		getThreadPool().execute(runnable);
	}
		
	public static final void shutdown(){
		if(pool != null)
			pool.shutdownNow();
	}
	
	/** Updates sitting in the queue or in processor of running */
	private static ConcurrentHashMap<Object, ObjectUpdateProc> inProgressUpdates = new ConcurrentHashMap<Object, ImageProcUtil.ObjectUpdateProc>();
	
	/** Queue something to be run because the given object has updated making sure it is run at least once after the final call to this.
	 * 
	 * The runnable will be run as soon as possible.
	 * 
	 * If an update is already in progress, the new update will be run by that thread, once it's finished.
	 * The last queued update is guaranteed to be run, the first might be run if it starts running before the next one is queued
	 *  but any in the middle will not be run. 
	 *  
	 * @param object	Hashing object to identify updates that are updating the 'same thing'. Usually this is the class being updated.	
	 * @param runnable	What to run.
	 */
	public static void ensureFinalUpdate(Object object, Runnable update){
		ensureFinalUpdate(object, update, null);
	}
	
	public static void ensureFinalUpdate(Object object, Runnable update, Display swtUpdateDisplay){
		ensureFinalUpdate(object, update, swtUpdateDisplay, (swtUpdateDisplay != null) ? 10 : 0);	
	}
	
	public static void ensureFinalUpdate(Object object, Runnable update, Display swtUpdateDisplay, int repeatDelay){	
		synchronized (inProgressUpdates) {
			ObjectUpdateProc existing = inProgressUpdates.get(object);
			if(existing != null){
				existing.setFinalUpdate(update);
			}else{
				ObjectUpdateProc newUpdate = new ObjectUpdateProc(object, update, swtUpdateDisplay, repeatDelay);
				inProgressUpdates.put(object, newUpdate);
				try{
					if(swtUpdateDisplay != null){
						//swtUpdateQueue.add(newUpdate);
						swtUpdateDisplay.asyncExec(newUpdate);
						
					}else{
						queueExec(newUpdate);
					}
				}catch(RejectedExecutionException err){
					if(object != null)
						inProgressUpdates.remove(object);
				}
			}
		}
	}
	
	public static boolean isUpdateInProgress(Object object){
		synchronized (inProgressUpdates) {
			ObjectUpdateProc existing = inProgressUpdates.get(object);
			return (existing != null);
		}
	}
	
	private static class ObjectUpdateProc implements Runnable {
		private Runnable update;
		private Runnable finalUpdate;
		private Object object;
		private Display swtDisplay;
		private int repeatDelay;
				
		public ObjectUpdateProc(Object object, Runnable update, Display swtDisplay, int repeatDelay) {
			this.swtDisplay = swtDisplay;
			this.object = object;
			this.update = update;
			this.repeatDelay = repeatDelay;
		}
		
		public void setFinalUpdate(Runnable finalUpdate) {
			this.finalUpdate = finalUpdate;
		}
		
		@Override
		public void run() {
			try{
				update.run();
			}catch(Throwable err){
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Caught exception/error in final update worker.", err);
				
			}finally{
				synchronized (inProgressUpdates) {
					if(finalUpdate == null) {
						//no more updates, we're done
						inProgressUpdates.remove(object);
						return;
					}
					
					//otherwise, queue the final one
					//we don't run it here because it will jump the general execution queue
					update = finalUpdate;
					finalUpdate = null;
					if(swtDisplay != null) {
						if(repeatDelay <= 0) {
							swtDisplay.asyncExec(this);
						}else {
							swtDisplay.timerExec(repeatDelay, this);
						}
					}else {
						try {					
							queueExec(this);
						}catch(RejectedExecutionException err) {
							logr.log(Level.WARNING, "Task " + update.getClass().getName() + " synced against " +
										((object == null) ? "(null)" : (
											(object instanceof String) ? "String(" + ((String)object) + ")" 
													: object.getClass().getName())) + " was rejected");
							inProgressUpdates.remove(object);
						}
					}
					
				}
				/*if(finalUpdate != null) {
					//keep trying to run the 'final' one, until it really is the final one
					Runnable newFinal;
					do{
						newFinal = finalUpdate;
						if(newFinal != null){
							try{
								newFinal.run();
							}catch(Throwable err){
								Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Caught exception/error in final update worker.", err);								
							}
						}
					}while(finalUpdate != newFinal);
				}
				inProgressUpdates.remove(object);
				//*/
			}
		}
	}
	
	private static LinkedList<Class> imgPipeClasses = new LinkedList<Class>();
	public static void addImgPipeClass(Class imageSinkClass){
		if(!imgPipeClasses.contains(imageSinkClass))
			imgPipeClasses.add(imageSinkClass);
	}
	
	public static List<Class> getPipeClasses(){
		return imgPipeClasses;
	}
	
	public static List<Class> getSourceClasses(){
		LinkedList<Class> imgSourceClasses = new LinkedList<Class>();
		for(Class c : imgPipeClasses)
			if(ImgSource.class.isAssignableFrom(c))
				imgSourceClasses.add(c);
		return imgSourceClasses;
	}
	
	public static List<Class> getSinkClasses(){
		LinkedList<Class> imgSinkClasses = new LinkedList<Class>();
		for(Class c : imgPipeClasses)
			if(ImgSink.class.isAssignableFrom(c))
				imgSinkClasses.add(c);
		return imgSinkClasses;
	}	
	
	private static LinkedList<Reference<ImgSourceOrSinkImpl>> imgPipes = new LinkedList<Reference<ImgSourceOrSinkImpl>>();
	private static LinkedList<ImgSourceOrSinkImpl> imgPipesPersistent = new LinkedList<ImgSourceOrSinkImpl>();
	/**@param imgPipe
	 * @param weakReference If true, the source will disappear when disconnected and not used, otherwise it sticks around
	 */
	public static void addImgPipeInstance(ImgSourceOrSinkImpl imgPipe, boolean weakReference){
		if(weakReference)
			imgPipes.add(new WeakReference<ImgSourceOrSinkImpl>(imgPipe));
		else
			imgPipesPersistent.add(imgPipe);
	}
	
	public static LinkedList<Reference<ImgSourceOrSinkImpl>> getAllPipes(){
		return imgPipes;
	}
	
	public static LinkedList<ImgSource> getSourceInstances(){
		LinkedList<ImgSource> imgSources = new LinkedList<ImgSource>();
		for(Reference<ImgSourceOrSinkImpl> ref : imgPipes){
			ImgSourceOrSinkImpl p = ref.get();
			if(p != null && p instanceof ImgSource && !imgSources.contains(p))
				imgSources.add((ImgSource)p);
		}
		for(ImgSourceOrSinkImpl p : imgPipesPersistent){
			if(p != null && p instanceof ImgSource && !imgSources.contains(p))
				imgSources.add((ImgSource)p);
		}
		return imgSources;
	}

	public static LinkedList<ImgSink> getSinkInstances(){
		LinkedList<ImgSink> imgSinks = new LinkedList<ImgSink>();
		for(Reference<ImgSourceOrSinkImpl> ref : imgPipes){
			ImgSourceOrSinkImpl p = ref.get();
			if(p != null && p instanceof ImgSink)
				imgSinks.add((ImgSink)p);
		}
		return imgSinks;
	}
	
	private static ConcurrentHashMap<Object, ObjectUpdateProc> inProgressSWTUpdates = new ConcurrentHashMap<Object, ImageProcUtil.ObjectUpdateProc>();
	//private static ConcurrentLinkedQueue<ObjectUpdateProc> swtUpdateQueue = new ConcurrentLinkedQueue<IMSEProc.ObjectUpdateProc>();
	
	/** Like ensureFinalUpdate, but the callback is called from the main (SWT) thread */
	public static void ensureFinalSWTUpdate(Display display, Object object, Runnable update){
		ensureFinalUpdate(object, update, display);
	}
	
	/** Like ensureFinalUpdate, but the callback is called from the main (SWT) thread */
	public static void ensureFinalSWTUpdate(Display display, Object object, Runnable update, int repeatDelay){
		ensureFinalUpdate(object, update, display, repeatDelay);
	}
	
	public static void msgLoop(Display display, boolean terminateOnGUIEnd){
		//long tLast = System.currentTimeMillis();
        //int nDraw = 0;
		ImageProcUtil.display = display;
		
        while (display.getShells().length > 0) {
        	try{
	          if (!display.readAndDispatch()) {
	        	
	            display.sleep();
	            
	          }
        	}catch(Throwable err){
        		System.err.println("Caught throwable in SWT main message loop:");
        		err.printStackTrace();
        	}
        }

        if(terminateOnGUIEnd){
			shutdown();
			
			display.dispose();
	
			logr.info("GUI thread exited. Terminating JVM to kill all remaining threads.");
			System.exit(0); //really exit, even if threads are running
        }
	}

	public static int getMetaInteger(ImgSource imgSrc, String path){
		if(imgSrc == null)
			return 0;
		Object obj = imgSrc.getSeriesMetaData(path);
		if(obj == null)
			return 0;
		int pulse;		
		if(obj instanceof Integer) 
			pulse = (Integer)obj;
		else if(obj instanceof int[])
			pulse = ((int[])obj)[0];
		else if(obj instanceof Integer[])
			pulse = ((Integer[])obj)[0];
		else{
			System.err.println("MetaData '"+path+"' is not int or int[]");
			return 0;
		}
		
		return pulse;
	}
	
	public static Object singleObjectMeta(ImgSource src, String id){
		Object o = src.getSeriesMetaData(id);
		while(o != null && o.getClass().isArray()){
			o = Array.get(o, 0);
		}
		return o;
	}

	/** Work out electrons per count from Andor/Sensicam camera metaData.
	 * { elecsPerCount, readNoise, backgroundLevel } 
	 * @deprecated Use fusionDefs.cameras.CameraInfoService
	 * */
	public static double[] getCameraSensitivityInfo(ImgSource src) {
		
		Double elecsPerCount = (Double)singleObjectMeta(src, "/Camera/ElectronsPerCount");
		Double readNoise = (Double)singleObjectMeta(src, "/Camera/ReadNoiseElectrons");
		Double backgroundLevel = (Double)singleObjectMeta(src, "/Camera/BackgroundLevelCounts");
		if(elecsPerCount != null){
			return new double[]{ elecsPerCount, readNoise, backgroundLevel };
		}
		
		elecsPerCount = null;
		readNoise = null;
		backgroundLevel = null;
		

		String gainControl = (String)singleObjectMeta(src, "/AndorCam/config/SimplePreAmpGainControl");
		String shutterMode = (String)singleObjectMeta(src, "/AndorCam/config/ElectronicShutteringMode");
		Object o = singleObjectMeta(src, "/AndorCam/config/BaselineLevel");		
		if(o == null)
			backgroundLevel = null;
		else if(o instanceof Double)
			backgroundLevel = (Double)o;
		else if(o instanceof Long)
			backgroundLevel = (double)(long)(Long)o;
		
			
		if(gainControl != null && shutterMode != null){
			if(shutterMode.startsWith("Rolling")){
				if(gainControl.startsWith("12-bit (high well")){
					elecsPerCount = 20.0;
				}else if(gainControl.startsWith("12-bit (low noise")){
					elecsPerCount = 0.6;
				}else if(gainControl.startsWith("16-bit (low noise & high well")){
					elecsPerCount = 0.6;
				}
				readNoise = 1.5; // in electrons, varies for readout rate and anyway isn't really a sigma
			}else if(shutterMode.startsWith("Global")){
				if(gainControl.startsWith("12-bit (high well")){
					elecsPerCount = 20.0;
				}else if(gainControl.startsWith("12-bit (low noise")){
					elecsPerCount = 1.8;
				}else if(gainControl.startsWith("16-bit (low noise & high well")){
					elecsPerCount = 1.8;
				}
				readNoise = 3.0; // in electrons, varies for readout rate and anyway isn't really a sigma
			}

			if(elecsPerCount == null)
				throw new IllegalArgumentException("AndorCam: Unrecognised shutter/gain modes");
		}else{
			//see if it's definitely the sensicam
			Integer sensicamGain = (Integer)singleObjectMeta(src, "/Sensicam/config/gainMode");
						
			if((src.getImage(0).getWidth() % 320) == 0){ //probably Sensicam VGA
				elecsPerCount = 7.5;
				readNoise = 14.0;
				backgroundLevel = 55.0; //roughly
				if(sensicamGain == null)
					System.err.println("IMSEProc.getElectronPerCount(): Not entirely sure that this was a sensicam, but it looks like Sensicam VGA, so assuming 7.5 e- per ADU");
			}else if((src.getImage(0).getWidth() % 344) == 0){ // Sensicam QE is 1376x, so we'll allow down to 1/4
				if(sensicamGain == null){
					System.err.println("No gain mode for Sensicam QE, no error calc");
					elecsPerCount = Double.NaN;
				}else{ 
					if(sensicamGain == 0){
						elecsPerCount = 4.0;						
					}else{
						elecsPerCount = 2.0;
					}
					readNoise = 5.0;
					System.err.println("IMSEProc.getElectronPerCount(): Looks like Sensicam QE with gain mode '"+sensicamGain +"', so assuming "+elecsPerCount+" e- per ADU. **** CHECK THIS!!! ****");
				}				
			}else{
				System.err.println("IMSEProc.getElectronPerCount(): Unrecognised Camera, no error calc");				
			}
			
		}

		src.setSeriesMetaData("/Camera/ElectronsPerCount", elecsPerCount == null ? null : new Double(elecsPerCount), false);
		src.setSeriesMetaData("/Camera/ReadNoiseElectrons", readNoise == null ? null : new Double(readNoise), false);
		src.setSeriesMetaData("/Camera/BackgroundLevelCounts", backgroundLevel == null ? null : new Double(backgroundLevel), false);
		
		return new double[]{ 
				elecsPerCount == null ? Double.NaN : elecsPerCount, 
				readNoise == null ? Double.NaN : readNoise, 
				backgroundLevel == null ? Double.NaN : backgroundLevel
			};
	}

	public static void setPreferredSettings(Class settingsControlClass) {
		preferredSettingsControlClass = settingsControlClass;
	}
	
	public static Class getPreferredSettings(){
		return preferredSettingsControlClass;
	}

	public static SWTSettingsControl createPreferredSettingsControl() {
		try {
			return (SWTSettingsControl) preferredSettingsControlClass.newInstance();
		} catch (InstantiationException | IllegalAccessException err) {
			throw new RuntimeException(err);
		}
	}
	
	

	public static void wriggle(Composite comp) {
		while(!(comp instanceof Shell))
			comp = comp.getParent();
		
		//now get the ImageWindow composite
		comp = (Composite) comp.getChildren()[0];
		
		//comp should now be main imageWindow shell
		//look for the SashForm
		for(Control subComp : comp.getChildren()){
			if(subComp instanceof SashForm){
				wriggleSash((SashForm)subComp);
			}
		}
	}

	//Return the display object of the main message loop
	public static Display getDisplay() { return display; }

	private static String wriggleSyncObject = new String("wriggleSyncObject");

	/** Well... this is quite the most rediculous piece of code I've ever had to write.
	 * Adjusts and readjusts the main sash form weights (position) to force all the GUI to sort itself
	 * out. */
	private static void wriggleSash(SashForm swtSashForm) {
		
		final int w[] = swtSashForm.getWeights();
		w[0]-=5;
		swtSashForm.setWeights(w);
		w[0]+=5;
			
			
		ImageProcUtil.ensureFinalUpdate(wriggleSyncObject, new Runnable() {			
			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) { }
					
				ImageProcUtil.ensureFinalSWTUpdate(swtSashForm.getDisplay(), this, new Runnable() {			
					@Override
					public void run() {						
						swtSashForm.setWeights(w);
					}}); 
			}});
	}

	public static void addRevealWriggler(Composite comp) {
		comp.addListener(SWT.Paint, new Listener() {
			boolean firstPaint = true;
			@Override
			public void handleEvent(Event event) {
				if(firstPaint){
					ImageProcUtil.wriggle(comp);
					firstPaint = false;
				}
			}
		});
	}

	public static String[] getGitCommitInfo(String project) {
		try {

			String classLoaderPath = ImageProcUtil.class.getClassLoader().getResource("").getPath();
			String autoPath = classLoaderPath.replaceAll("(.*)/ImageProc-.*", "$1");		
			String projectsPath = SettingsManager.defaultGlobal().getPathProperty("minerva.code.path", autoPath);
			
			//String command = "git";
			//String params[] = { "log", "--pretty=format:'OK %H %ad'", "-n 1" };
			//String param = "log --pretty=format:'OK %H %ad' -n 1";
	
			String command[] = { "git" ,"log", "--pretty=format:'OK %H %ad'", "-n 1" };
			
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command(command);
			processBuilder.directory(new File(projectsPath + "/" + project));
	
			
			Process process = processBuilder.start();

			BufferedReader reader =
					new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line;
			StringBuffer strBuff = new StringBuffer(64);
			while ((line = reader.readLine()) != null) {
				strBuff.append(line);
			}

			int exitCode = process.waitFor();
			
			//extract hex commitID and date, check for an 'OK' at start, ignore any enclosing quotes
			Matcher m = Pattern.compile("[\"']?OK ([0-9a-f]*) ([^']*)[\"']?").matcher(strBuff.toString());
			if(!m.matches())
				throw new RuntimeException("Didn't understand git command response:" + strBuff.toString());
			
			return new String[]{ m.group(1), m.group(2) };
	
		} catch (Exception e) {
			return new String[]{ "Error", "Error:"+e.getMessage() }; 
		}
		
		
	}

	
	/** Make sure we have generic metadata for the sensor/camera.
	 * (We might also want to modify it according to a calibration here
	 * and future calls to CameraInfoService will pick up what we set here.) 
	 */
	public static SensorInfo getGenericCameraMetadata(ImgSource imgSrc) {
		try{
			MetaDataGetter metaSrc = new MetaDataGetter() {				
				@Override
				public <T> T get(String name, Class<T> desiredType) {
					Object o = imgSrc.getSeriesMetaData(name);
					if(o == null)
						return null;
					
					if(desiredType.isArray()){
						//ugh, this seems to be the easiest way to do this
						Gson g = new Gson();
						String s = g.toJson(o);
						Object o2 = g.fromJson(s, desiredType);
						//Object o2 = Mat.fixWeirdContainers(o, true);
						return (T)o2;
					}else{
						//non arrays
						
						if(desiredType.isAssignableFrom(o.getClass()))
							return (T)o;
						else if(desiredType == Integer.class && o instanceof Number)
							return (T)(Integer)((Number)o).intValue();
						else if(desiredType == Double.class && o instanceof Number)
							return (T)(Double)((Number)o).doubleValue();
						else if(desiredType == String.class && o instanceof Number)
							return (T)(String)((Number)o).toString();
						else
							throw new RuntimeException("Not sure how to convert entry '"+name+"' from "+o.getClass()+" to "+desiredType+".");
					}
				}
			};
			
			SensorInfo camInfo = SensorInfoService.decipher(metaSrc);			
			imgSrc.addNonTimeSeriesMetaDataMap(camInfo.toMap("cameraModeInfo"));
			
			return camInfo;
			
		}catch(RuntimeException err){
			System.err.println("WARNING: Unable to decipher camera photoelectron counting information (gain etc)");
			err.printStackTrace();
			return null;
		}

	}

	public static Logger setupLog(String logPrefix, String className) {
		
		// Setup logging to temp file
		logFile = System.getProperty("java.io.tmpdir") 
									+ "/"+logPrefix+"-"
									+ (new SimpleDateFormat("YYYMMdd-HHmmss")).format(new Date())
									+ ".xml";
		
		System.out.println("Writing to log " + logFile);
		System.out.println(LogManager.getLogManager().getProperty(".level"));
		try {
			//LogManager.getLogManager().readConfiguration();

			String conf = " "
					+ ".level=ALL \n"
					+ "handlers = java.util.logging.FileHandler, java.util.logging.ConsoleHandler \n"
					+ "java.util.logging.FileHandler.pattern="+logFile+" \n"
					+ "java.util.logging.FileHandler.limit=0\n"
					+ "java.util.logging.FileHandler.count=10\n"
					+ "java.util.logging.FileHandler.formatter=java.util.logging.XMLFormatter\n" //java.util.logging.SimpleFormatter\n"
					
					+ "java.util.logging.ConsoleHandler.level = ALL \n"
					+ "java.util.logging.ConsoleHandler.formatter=java.util.logging.SimpleFormatter\n"
					+ "java.util.logging.SimpleFormatter.format=|%1$tH:%1$tM:%1$tS|%3$s|%4$s|%5$s|%6$s|%n\n" 
					
					//we generally want to hear a lot from imageProc and it's modules
					+ "imageProc.level = FINE\n"
					//+ ".level = FINEST \n"
					
					//things we definitely don't want to hear much from
					+ "sun.awt.level = WARNING \n"
					+ "sun.net.level = WARNING \n"
					+ "com.sun.jersey.level = WARNING \n"
					+ "Spectrometer.level = FINE \n" //OceanOptics OmniDriver is noisy and poorly named
					+ "de.mpg.ipp.codac.util.info.ScheduledVersionCheck.level = WARNING \n" //some codac thing which keeps running getStackTrace()
					
			
					;
			
			LogManager.getLogManager().readConfiguration(new ByteArrayInputStream(conf.getBytes()));
			
			logFile += ".0"; //get appended by FileHandler
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(LogManager.getLogManager().getProperty(".level"));
		//Logger.getGlobal().log(Level.INFO, "Log start");
		Logger logr = Logger.getLogger(className);
		logr.log(Level.INFO, "Opened log " + logFile);
		logr.log(Level.INFO, "ImageProc-w7x, last commit: " + String.join(" ", ImageProcUtil.getGitCommitInfo("ImageProc-w7x")));
		logr.log(Level.INFO, "ImageProc-core, last commit: " + String.join(" ", ImageProcUtil.getGitCommitInfo("ImageProc-core")));
		
		return logr;
	}
	
	public static String getLogFile() { return logFile;	}

	/*public static void noisyLogging() {

		Logger rootLogger = LogManager.getLogManager().getLogger("");
		for (Handler h : rootLogger.getHandlers()) {
		    h.setLevel(Level.FINEST);
		}

		Logger imageProcLogger = Logger.getLogger("imageProc");
		imageProcLogger.setLevel(Level.FINEST);
		
		Logger sunLogger = Logger.getLogger("sun.awt");
		sunLogger.setLevel(Level.OFF);
		
	}

	public static void suppressAWTLoggingNoise() {
		//suppress logging of sun GUI and network stuff which we most likely will never care about
		Enumeration<String> a = LogManager.getLogManager().getLoggerNames();
		while(a.hasMoreElements()) {
			String name = a.nextElement();
			System.out.println("LOGGER " + name);
			if(name.contains("sun.awt")
					|| name.contains("sun.net.www")
					|| name.contains("com.sun.jersey")) {
				LogManager.getLogManager().getLogger(name).setLevel(Level.OFF);
			}
		}
	}*/
	
	/** Gets names of all D number metadata that could be used as a timebase */ 
	public static ArrayList<String> getPossibleTimebases(ImgSource src) {	
		ArrayList<String> timebases = new ArrayList<String>();
		
		for(Entry<String, MetaData> entry : src.getCompleteSeriesMetaDataMap().entrySet()){
			Object o = entry.getValue().object;
			if(entry.getValue().isTimeSeries ||
					(o != null && o.getClass().isArray()
					&& Array.getLength(o) >= src.getNumImages())
					){
				if(o == null || !o.getClass().isArray())
					continue;
				Object elem = Array.get(o, 0);
				if(elem ==null)
					continue;
				Class typ = elem.getClass();
				
				if(Number.class.isAssignableFrom(typ)){
					
					String name = entry.getKey();
					if(name.startsWith("/"))
						name = name.replaceAll("^/*", "");					
					timebases.add(name);
				}
			}
		}
		
		return timebases;
	}

	public static String nanotimeLikeProgramID(long timestamp) {
		Date d = new Date(timestamp / 1_000_000L);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));			
		String dateStr = sdf.format(new Date());
		
		SimpleDateFormat sdfTime = new SimpleDateFormat("HHmm");
		sdfTime.setTimeZone(TimeZone.getTimeZone("UTC"));			
		String timeStr = sdfTime.format(d);
		
		return dateStr + ".t_" + timeStr;
	}

	/** Find all ImagePanels attached to the given source and force them to redraw.
	 * Useful for controllers that draw on the image */
	public static void redrawImagePanel(ImgSource src) {
		for(ImgSink sink : src.getConnectedSinks()) {
			if(sink instanceof ImagePanel) {
				((ImagePanel)sink).redraw();
			}
		}
	}

	/** Return SWT color account to status string */
	public static void setColorByStatus(String status, Control control) {
		
		Color cBG = control.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
		boolean isDarkMode = (cBG.getRed() + cBG.getGreen() + cBG.getBlue())/3 < 128;
		
		Color colorError = control.getDisplay().getSystemColor(SWT.COLOR_RED);
		Color colorActive = control.getDisplay().getSystemColor(SWT.COLOR_BLUE);
		Color colorNormal = control.getDisplay().getSystemColor(SWT.COLOR_BLACK);
		
		if(isDarkMode) {
			colorError = new Color(colorError.getDevice(), 255, 128, 128);
			colorActive = new Color(colorError.getDevice(), 0, 192, 255);
			colorNormal = new Color(colorError.getDevice(), 255, 255, 255);
		}
		
		if(status.matches("(?is).*aborted.*") ||
			status.matches("(?is).*exception.*") ||
			status.matches("(?is).*error.*")) {
			
			control.setForeground(colorError);
		}else if(status.contains("...")) {
			control.setForeground(colorActive);
		}else {
			control.setForeground(colorNormal);
		}
	}

	public static void refreshAll(ImgSource src) {
		if(src instanceof ImgSink) {
			//go down the tree first
			refreshAll(((ImgSink)src).getConnectedSource());
			return;
		}else {
			refreshTree(src);
		}
	}
	
	public static void refreshTree(ImgSource src) {
		for(ImgSink sink : src.getConnectedSinks()) {
			//update the controllers of this sink first
			for(ImagePipeController controller : sink.getControllers())
				controller.generalControllerUpdate();
			
			//decend the tree
			if(sink instanceof ImgSource) {
				refreshTree((ImgSource)sink);
			}
		}
	}
}
