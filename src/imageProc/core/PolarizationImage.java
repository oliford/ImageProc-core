package imageProc.core;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

/**
 * Image captured by a camera with a grid of micropolarizers in front of the
 * pixels.
 * 
 * This class is made with the FLIR Blackfly S BFS-U3-51S5P camera in mind. This
 * camera has a 2x2 micropolarizer grid: every pixel has a polarizer in front
 * with an orientation of 0, 45, 90, or 135 degrees.
 *
 */
public class PolarizationImage extends ByteBufferImage {

	/**
	 * Controls the behavior of the getPixelValue method. If false, the values
	 * returned will the reflect the image as it is captured. If true, the values
	 * reeturned will form a quad image, with each quadrant formed from all the
	 * pixels of a particular micropolarizer orientation.
	 */
	private boolean returnQuadView;

	/**
	 * Constructor for creating a new PolarizationImage object based on an existing
	 * template object.
	 */
	public PolarizationImage(ImgSource source, int sourceIndex, PolarizationImage template,
			BulkImageAllocation bulkAlloc, ByteBuffer imageBuffer, boolean init) {
		super(source, sourceIndex, template, bulkAlloc, imageBuffer, init);
		this.returnQuadView = template.returnQuadView;
	}

	/**
	 * Constructor for creating a new PolarizationImage object from scratch.
	 */
	public PolarizationImage(ImgSource source, int sourceIndex, int width, int height, int bitDepth, int headerSize,
			int footerSize, boolean init, boolean returnQuadView) {
		super(source, sourceIndex, width, height, bitDepth, headerSize, footerSize, init);
		this.returnQuadView = returnQuadView;
	}

	/**
	 * Used internally by BulkImageAllocation to create PolarizationImage objects
	 * based on a template object.
	 */
	@Override
	public PolarizationImage createFromTemplate(ImgSource source, int sourceIndex, BulkImageAllocation bulkAlloc,
			ByteBuffer buffer, boolean init) {
		return new PolarizationImage(source, sourceIndex, this, bulkAlloc, buffer, init);
	}

	/**
	 * Returns a pixel based on the requested (x, y) coordinates. If the
	 * returnQuadView setting is false, then the (x, y) coordinate of the original
	 * image will be returned. If it is true, then the pixel returned will be chosen
	 * such that it forms a quad view (each quadrant reflecting one polarization
	 * direction).
	 */
	@Override
	public double getPixelValue(ReadLock readLock, int field, int x, int y) {
		if (returnQuadView) { // return pixel corresponding to quad view (x, y) position
			// Determine size of each quandrant (technically upper left quadrant, since
			// quadrants
			// can have different sizes if the images width or height is an odd number
			int width_quad = (int) Math.ceil((double) this.width / 2);
			int height_quad = (int) Math.ceil((double) this.height / 2);

			// Determine which quadrant this pixel is in
			int x_quad = x / width_quad; // 0: one of the left quadrants, 1: one of the right quadrants
			int y_quad = y / height_quad; // 0: one of the upper quadrants, 1: one of the lower quadrants

			// Determine the (x, y) coords of the pixel to get from the original image
			int x_position = 2 * x + x_quad * (1 - 2 * width_quad);
			int y_position = 2 * y + y_quad * (1 - 2 * height_quad);

			// Get the pixel
			return super.getPixelValue(readLock, field, x_position, y_position);
		} else { // return pixel corresponding to the actual (x, y) position
			return super.getPixelValue(readLock, field, x, y);
		}
	}

	/**
	 * Version of getPixelValue if a WriteLock (instead of a ReadLock) is given and
	 * no field is given.
	 */
	@Override
	public double getPixelValue(WriteLock writeLock, int x, int y) {
		if (writeLock != lock.writeLock())
			throw new RuntimeException(
					"ByteBufferImage.getPixelValue(WriteLock): Write lock is not valid (using it as a read lock)");
		return getPixelValue(lock.readLock(), 0, x, y);
	}

	/**
	 * Version of getPixelValue if a WriteLock (instead of a ReadLock) is given and
	 * a field is given.
	 */
	@Override
	public double getPixelValue(WriteLock writeLock, int field, int x, int y) {
		if (writeLock != lock.writeLock())
			throw new RuntimeException(
					"ByteBufferImage.getPixelValue(WriteLock): Write lock is not valid (using it as a read lock)");
		return getPixelValue(lock.readLock(), field, x, y);
	}

	/**
	 * Version of getPixelValue if no field is given.
	 */
	@Override
	public double getPixelValue(ReadLock readLock, int x, int y) {
		return getPixelValue(readLock, 0, x, y);
	}

	public void setReturnQuadView(boolean returnQuadView) {
		this.returnQuadView = returnQuadView;
	}
}
