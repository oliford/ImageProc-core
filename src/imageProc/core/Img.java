package imageProc.core;

import java.util.Random;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Logger;


/** Core image storage class, which isn't called 'Image' because of the SWT image class 
 *
 * When modifing the image data (either underlying or through setPixelValue(), call 
 * startWriting() and endWriting().
 * 
 * When reading the data, startReading() and endReading() can be called to ensure it
 * won't be written to at the same time. 
 */
public abstract class Img {
	
	protected Logger logr;

	protected static Random randIDGen = new Random();
	
	/** Random number reset every time the image is changed
	 * so that sinks can tell what has actually been modified */
	protected int changeID = randIDGen.nextInt();

	/** Size of image */
	protected int width, height;
	
	/** Number of fields. Usually 1, but 2+ for other images e.g. uncertainty, alpha etc.
	 * Only field 0 is used for ranges etc. */
	protected int fields;
	
	/** Header and tail size for other info not part of image data (metadata) */
	protected int headerSize = 0;
	protected int footerSize = 0;
	
	protected ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
	
	public enum RangeValidity {
		invalid, inCalc, valid
	};	
	protected RangeValidity rangeValid = RangeValidity.invalid;
	protected Thread inProgressRangeCalc = null; 
	
	/** ImgSource from whence this came, for the purposes of notifying the source that the image
	 * has been changed by something (which really should only be the source, err... nvm)
	 * so it can notifyt he sinks. Oh ffs.
	 */
	protected ImgSource source;
	protected int sourceIndex;

	protected double min, max, sum;

	protected boolean destroyed = false;
		
	public Img(ImgSource source, int sourceIndex) {
		this.source = source;
		this.sourceIndex = sourceIndex;
		this.fields = 1;
		logr = Logger.getLogger(this.getClass().getName() + "(" + ((source == null) ? "not-in-source" : source.getClass().getSimpleName()) + ")");
	}
	
	/** Call to say the image data has been internally modified, and that is now complete.
	 * @param fast If true, range calculation is done in a separate thread.
	 */
	public void imageChanged(boolean fast) {
		//do the fast notification of any sinks that need it first
		for(ImgSink sink : source.getConnectedSinks()) {
			sink.imageChangedLowLevel(sourceIndex);
		}
		
		int r;
		do{ 
			r = randIDGen.nextInt();
		}while(r == changeID);
		changeID = r;
		
		if(!fast){
			calcRanges();
			source.imageRangesDone(sourceIndex);
		}else{
			
			ImageProcUtil.ensureFinalUpdate(this, new Runnable() {
				@Override
				public void run() {
					calcRanges();				
					source.imageRangesDone(sourceIndex);			
				}
			});
		}
	}	
	
	/** Returns the theoretical max value per pixel (e.g. 255 for a 8bpp image) */
	public abstract double getMaxPossibleValue();
	
	/** Read lock is only required to prove you though about having one, it might not actually get checked! */
	public abstract double getPixelValue(ReadLock readlock, int x, int y);
	
	/** Multi-field getPixelValue(). Default implementation just returns field 0 or an error if field != 0. */
	public double getPixelValue(ReadLock readLock, int field, int x, int y) {
		if(field == 0)
			return getPixelValue(readLock, x, y);
		else
			throw new IndexOutOfBoundsException("Image only has a single field. (getPixelValue(ReadLock, int, int, int) is not implemented)");
	}
	
	public int getSourceIndex(){ return sourceIndex; }

	/** Returns the max pixel value actually in the image 
	 * This is usually called from imageChanged() from the worker queue. 
	 * but can be called explicitly if the caller has time to spare */
	public void calcRanges(){
		synchronized (rangeValid) {
			//logr.finest(toString() + "-" + Thread.currentThread().toString() + ":calcRanges() entry, changeID = " + changeID);
				
			
			if(rangeValid == RangeValidity.valid && !destroyed){
				//logr.finest(toString() + "-" + Thread.currentThread().toString() + ": Abort, already valid");
				return;  //it's already valid
			}
				
			if(destroyed){ //it's destroyed so just give up
				//logr.finest(toString() + "-" + Thread.currentThread().toString() + ": Abort, destroyed");
				min = Double.NaN; 
				max = Double.NaN;
				sum = Double.NaN;
				rangeValid = RangeValidity.invalid;
				return;
			}
			
			
			//ok, we're working on things now
			rangeValid = RangeValidity.inCalc; //make that we are in calc
			inProgressRangeCalc = Thread.currentThread();
			//logr.finest(toString() + "-" + Thread.currentThread().toString() + ": In calc");
		}
		
		ReadLock readLock = lock.readLock();
		try{ //get the read lock
			readLock.lockInterruptibly();
		}catch(InterruptedException err){
			return;
		}
		
		try{
			//logr.finest(toString() + "-" + Thread.currentThread().toString() + ": Read lock");
			//the image now shouldn't change under us, but another calcRanges() can still get run concurrently
			
			rangeValid = RangeValidity.inCalc;
			double min = Double.POSITIVE_INFINITY;
			double max = Double.NEGATIVE_INFINITY;
			double sum = 0;
			for(int iY = 0; iY < height; iY++){
				for(int iX = 0; iX < width; iX++){
					double val = getPixelValue(readLock, iX, iY);
					if(val > max) max = val;
					if(val < min) min = val;
					sum += val;
				}	
				
				//in the middle, if another calc starts or the ranges get invalidated, give up
				synchronized (rangeValid) {
					
					if(rangeValid == RangeValidity.invalid){
						logr.finest(toString() + "-" + Thread.currentThread().toString() + ": Abort because invalidated");
						min = Double.NaN; 
						max = Double.NaN;
						sum = Double.NaN;
						rangeValid = RangeValidity.invalid;
						return;
					}else if(inProgressRangeCalc != Thread.currentThread()){ 
						logr.finest(toString() + "-" + Thread.currentThread().toString() + ": Abort because wrong calc");
						min = Double.NaN; 
						max = Double.NaN;
						sum = Double.NaN;
						rangeValid = RangeValidity.invalid;
						return;
					}
					
				}
			}
			this.min = min;
			this.max = max;
			this.sum = sum;
			
			synchronized (rangeValid) {
				
				//one last check only we are doing this and nothing has changed
				if(rangeValid == RangeValidity.invalid){
					logr.finest(toString() + "-" + Thread.currentThread().toString() + ": Abort because invalidated");
				}else if(inProgressRangeCalc != Thread.currentThread()){ 
					logr.finest(toString() + "-" + Thread.currentThread().toString() + ": Abort because wrong calc");
				}else{
					rangeValid = RangeValidity.valid;
				}
			}
		
		}finally{ //and release the read lock
			readLock.unlock();			
		}
	}
	
	public int getWidth() { return width; }
	public int getHeight() { return height; }
	public int getNFields() { return fields; }

	/** Whether or not range values represent the current image data */
	public final boolean isRangeValid(){ return (rangeValid == RangeValidity.valid); }
	/** Ranges might be non-NaN though not valid. */
	public final double getMin() { return min; }
	public final double getMax() { return max; }
	public final double getSum() { return sum; }
	public final double getMean() { return (sum / (width * height)); }
	
	@Override
	public String toString() {
		return getClass().getCanonicalName() + "[" + Integer.toHexString(hashCode()) + "]" + " " + width + " x " + height + 
				" x " + fields + "from " + (source == null ? "Null" : source.toShortString()) + "#" + sourceIndex;
	}
	
	public final String getShortID(){
		String hhc = Integer.toHexString(hashCode());
		return hhc.substring(hhc.length()-3, hhc.length());
	}
	
	public String toShortString() {
		return getClass().getSimpleName() + "[" + getShortID() + "]"; 
	}
	
	/** Free any allocated data then report image as changed */
	public void destroy(){ 
		destroyed = true;
		imageChanged(false);
	}
	
	public boolean isDestroyed(){ return destroyed; }

	public ReadLock readLock() { return lock.readLock(); }	
	public WriteLock writeLock() { return lock.writeLock(); }
	
	public void invalidate(){
		invalidate(true);
	}
	
	public void invalidate(boolean notifyChange){
		rangeValid = RangeValidity.invalid;
		//logr.finest(toString() + "-" + Thread.currentThread() + ": invalidate("+notifyChange+") Invalidated");
				
		if(notifyChange){
			ImageProcUtil.ensureFinalUpdate(this, new Runnable() {
				@Override
				public void run() {			
					source.imageRangesDone(sourceIndex);			
				}
			});
		}
	}
	
	public int getChangeID(){ return changeID; }
	
	public abstract boolean isMemoryCompatible(Img img);
}
 