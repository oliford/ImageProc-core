package imageProc.core.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class DispPropsPanel {
	Composite swtImgCfgComposite;
	Button breakAspectCheckbox;
	Scale swtZoomScaleX;
	Scale swtZoomScaleY;
	Scale swtColorLogScale;
	
	private ShellOps shellOps;

	public DispPropsPanel(ShellOps shellOps) {
		this.shellOps = shellOps;
	}
	
	public void build(Composite parent, int style) {
		swtImgCfgComposite = new Composite(parent, SWT.BORDER_SOLID);
		
        swtImgCfgComposite.setLayout(new GridLayout(6, false));
        swtImgCfgComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));
        
        ImagePanel imgWin = shellOps.imagePanel();
        
        Label lZS = new Label(swtImgCfgComposite, 0); lZS.setText("Scale, &X:");
        
        swtZoomScaleX = new Scale(swtImgCfgComposite, 0);
        swtZoomScaleX.setMinimum(0);
    	swtZoomScaleX.setMaximum(500);
    	swtZoomScaleX.setIncrement(1);
    	swtZoomScaleX.setSelection(ImagePanel.scaleToSel(imgWin.getScaleX()));
    	swtZoomScaleX.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        swtZoomScaleX.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { zoomScaleEvent(event); } });
        
        breakAspectCheckbox = new Button(swtImgCfgComposite, SWT.CHECK);
        breakAspectCheckbox.setText("&Y:");
        breakAspectCheckbox.setToolTipText("Allow independant X/Y scaling.");
        breakAspectCheckbox.setSelection(imgWin.getBreakAspect());
        breakAspectCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));        
        breakAspectCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { zoomScaleEvent(event); } });
        
        swtZoomScaleY = new Scale(swtImgCfgComposite, 0);
        swtZoomScaleY.setMinimum(0);
    	swtZoomScaleY.setMaximum(500);
    	swtZoomScaleY.setIncrement(1);
    	swtZoomScaleX.setSelection(ImagePanel.scaleToSel(imgWin.getScaleY()));
    	swtZoomScaleY.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        swtZoomScaleY.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { zoomScaleEvent(event); } });
        swtZoomScaleY.setEnabled(imgWin.getBreakAspect());        
        
        Label lCLS = new Label(swtImgCfgComposite, 0); lCLS.setText("Color:");
        swtColorLogScale = new Scale(swtImgCfgComposite, 0);
        swtColorLogScale.setToolTipText("Logarithmically scales the color map");
        swtColorLogScale.setMinimum(0);
        swtColorLogScale.setMaximum(200);
        swtColorLogScale.setIncrement(1);
        swtColorLogScale.setSelection((int)(Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.gui.colorLogScale", "1.0")) / 0.02));
        swtColorLogScale.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        swtColorLogScale.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { colorScaleEvent(event, false); } });

	}
	
	private void zoomScaleEvent(Event event){
		if(breakAspectCheckbox.getSelection()){
			swtZoomScaleY.setEnabled(true);
		}else{
			swtZoomScaleY.setEnabled(false);
			swtZoomScaleY.setSelection(swtZoomScaleX.getSelection());
		}
		
		shellOps.imagePanel().zoomScaleEvent(event);
	}
	
	
	private void colorScaleEvent(Event event, boolean centreOnMouse){
		shellOps.imagePanel().colorScaleEvent(event, centreOnMouse);		
	}	
}
