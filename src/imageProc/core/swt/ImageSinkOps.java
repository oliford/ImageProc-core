package imageProc.core.swt;

import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.GC;

import imageProc.core.ImgSink;
import imageProc.core.ImgSource;

/** Basically stuff in SplitImgInfoView that I don't know where to put yet.
 * 
 * Part of refactoring to try the eclipse RCP application.
 * 
 * @author oliford
 *
 */
public interface ImageSinkOps {

	public void setRect(int x0, int y0, int w, int h);

	public void drawOnImage(GC gc, double[] scale, int width, int height);

	public void fixedPos(int posX, int posY);

	public void movingPos(int posX, int posY);

	public void addSinkController(ImgSink sink);

	public void setSource(ImgSource source);

	public CTabFolder getTabFolder();

}
