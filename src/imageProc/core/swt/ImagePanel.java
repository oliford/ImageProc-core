package imageProc.core.swt;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.logging.Level;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import algorithmrepository.Interpolation1D;
import imageProc.core.ImageProcUtil;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.fft.FFTProcessor;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.ColorMaps;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.RandomManager;
import otherSupport.SettingsManager;

//async
public class ImagePanel extends ImgSourceOrSinkImpl implements ImgSink {
	private static final int nCols = 1000;
	
	private byte cMap[];
	private int cMapType;
	private double cMapDbl[][];
	private double vMap[];
	private double vMapColourScale = Double.NaN;

	private static final double scaleConst = 1.05;
	private static final double logScaleConst = FastMath.log(scaleConst);
	
	
	
	private Image swtCurImage;
	private Canvas swtImageCanvas;
	
	private ScrolledComposite swtImgScrollComp;
	
	private double scaleX, scaleY;
	private boolean breakAspect = false;
	private int posX = 0, posY = 0;

	private int POS_UNLOCKED = 1;
	private int SEL_UNLOCKED = 2;
	private int POS_SEL_LOCKED = 3;
	private int posSelLock = POS_SEL_LOCKED;
	
	private int selX0, selY0, selW, selH;
	private long selectStartTime = -1;
	
	/** 'live' mode */
	private boolean gotoChanged = false;
	
	/** Syncronising Objects */ 
	private String imageChangeSyncObj = new String("imageChangeSyncObj");
	private String imageSetChangeSyncObj = new String("imageSetChangeSyncObj");
	private String imageConvertSyncObj = new String("imageConvertSyncObj");
	private String imageDisplaySyncObj = new String("imageDisplaySyncObj");

	private ShellOps shellOps;
	
	public ImagePanel(ShellOps shellOps) {
		this.shellOps = shellOps;
		scaleX = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.gui.scaleX", "1.0"));
		scaleY = Algorithms.mustParseDouble(SettingsManager.defaultGlobal().getProperty("imageProc.gui.scaleY", "1.0"));
		breakAspect = (scaleX != scaleY);
	}
	
	public void build(Composite swtParent){
		cMapType = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.gui.colorMap", "0"));
		
        
        swtImgScrollComp = new ScrolledComposite(swtParent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER_SOLID | SWT.BORDER);
        
        swtImageCanvas = new Canvas(swtImgScrollComp, SWT.NO_BACKGROUND);
        swtImageCanvas.addPaintListener(new PaintListener() { @Override public void paintControl(PaintEvent e) { canvasPaintEvent(e); }});
        swtImageCanvas.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event e) { canvasMouseDownEvent(e); }});
        swtImageCanvas.addListener(SWT.MouseUp, new Listener() { @Override public void handleEvent(Event e) { canvasMouseUpEvent(e); }});
        swtImageCanvas.addListener(SWT.MouseMove, new Listener() { @Override public void handleEvent(Event e) { canvasMouseMoveEvent(e); }});
        swtImageCanvas.addListener(SWT.KeyDown, new Listener() { @Override public void handleEvent(Event e) { canvasKeyDownEvent(e); }});
        //swtImageCanvas.addListener(SWT.MouseWheel, new Listener() { @Override public void handleEvent(Event event) { scrollWheelEvent(event); } });
        swtImageCanvas.setToolTipText("Image. Left-click = Set position, Right-click = Release set position, Drag = Draw ROI, Middle-click=Move ROI with cursor.\n"
        								+ "Scroll wheel = up/down, +Shift = left/right +Ctrl = Zoom\n"
        								+ "ASWD = Move graph position");
        
        swtImgScrollComp.setContent(swtImageCanvas);
        swtImgScrollComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        swtImgScrollComp.addListener(SWT.MouseWheel, new Listener() { @Override public void handleEvent(Event event) { scrollWheelEvent(event); } });

        //Composite swtDispPropsComp = new Composite(swtLHSComposite, SWT.BORDER);
        //swtDispPropsComp.setLayout(new GridLayout(7, false));
        
        
        
        createSWTImage();
	}
	
	
	
	/** Add an SWT controller for the given sink to our window */ 
	public void addSinkController(ImgSink sink){
		shellOps.imgSinkOps().addSinkController(sink);
    }
	

	void updateInfoPanel() {
		shellOps.infoPanel().doUpdate();
		
	}
	
	void colorScaleEvent(Event event, boolean centreOnMouse){
		createSWTImage();
		swtImageCanvas.redraw();		
	}
	
	void colorMapEvent(Event event) {
		createSWTImage();
		swtImageCanvas.redraw();	
	}

	void zoomScaleEvent(Event event){
		DispPropsPanel dispPropsPanel = shellOps.dispPropsPanel();
		int newScaleSelX = dispPropsPanel.swtZoomScaleX.getSelection();
		int newScaleSelY = dispPropsPanel.swtZoomScaleY.getSelection();
		
		Rectangle clientArea = swtImgScrollComp.getClientArea();
		rescale(selToScale(newScaleSelX), selToScale(newScaleSelY), clientArea.width / 2, clientArea.height / 2, 
				dispPropsPanel.breakAspectCheckbox.getSelection());
	}
		
	
	public void checkScale() { rescale(scaleX, scaleY, -1, -1, breakAspect); 	}
	
	private final String rescaleSyncObj = new String("rescaleSyncObj");
	/** Changes the scale of the image, maintaining the position of the given image in the window
	 * @param newScaleX,  newScaleY  New scale to set
	 * @param centreX, centreY  position in display coords of point to zoom in/out on
	 */
	public void rescale(double newScaleX, double newScaleY, int centreX, int centreY, boolean breakAspect) {
		
		double lastScaleX = scaleX;
		double lastScaleY = scaleY;

		this.scaleX = newScaleX;
		this.scaleY = newScaleY;
		this.breakAspect = breakAspect;
		
		Img image = getSelectedImageFromConnectedSource();
		
		//SWT won't let us create a canvas with > 64k pixels in either dir
		//  fair enough 
		if(image != null){
				
			if(scaleX * image.getWidth() > 65535) 
				scaleX = 65535 / image.getWidth();
			
			if(scaleY * image.getHeight() > 65535)
				scaleY = 65535 / image.getHeight();
			
			//if the Y scale isn't explicitly set (so should be aspect=1)
			//but the aspect is silly, make it sensible
			if(!breakAspect){
				if(image.getWidth() > image.getHeight() * 20){
					scaleY = scaleX * image.getWidth()/ image.getHeight() / 2 ;
				}
			}
		}

		ImageProcUtil.ensureFinalSWTUpdate(swtImgScrollComp.getDisplay(), rescaleSyncObj, new Runnable() { 
					@Override public void run() { rescaleSWTUpdate(newScaleX, newScaleY, lastScaleX, lastScaleY, centreX, centreY); } 
				});
	}
	
	private void rescaleSWTUpdate(double newScaleX, double newScaleY, double lastScaleX, double lastScaleY, int centreX, int centreY) {
		
		shellOps.dispPropsPanel().swtZoomScaleX.setSelection(scaleToSel(scaleX));
		shellOps.dispPropsPanel().swtZoomScaleY.setSelection(scaleToSel(scaleY));
		
		if(centreX >= 0 && centreY >= 0) {
			Point dOrigin = swtImgScrollComp.getOrigin(); //display units
			
			//position in image of that point (caled using the old scale) 
			double imgZX = (centreX + dOrigin.x) / lastScaleX;
			double imgZY = (centreY + dOrigin.y) / lastScaleY;
			
			//origin of swtScrollComp of that point required to holding that point in the /same place/. 
			double newDispOX =  imgZX * newScaleX - centreX;
			double newDispOY =  imgZY * newScaleY - centreY;
			
			swtImgScrollComp.setOrigin((int)newDispOX, (int)newDispOY);
		}
		
		Img img = getSelectedImageFromConnectedSource();
		if(img != null){
			double scale[] = getScale();
	        swtImageCanvas.setSize(
					(int)(img.getWidth()*scale[0]), 
					(int)(img.getHeight()*scale[1]));
		}
		swtImageCanvas.redraw();
		
	}
	
	private void scrollWheelEvent(Event event){
		//this stop the default of just scrolling vertically
		event.doit =  false;
		if( (event.stateMask & SWT.CTRL) != 0){
			int vX = shellOps.dispPropsPanel().swtZoomScaleX.getSelection();
			int vY = shellOps.dispPropsPanel().swtZoomScaleY.getSelection();
			vX += event.count;
			vY += event.count;
			
			rescale(selToScale(vX), selToScale(vY), event.x, event.y, breakAspect);
	
		}else if( (event.stateMask & SWT.SHIFT) != 0){
			Point pos = swtImgScrollComp.getOrigin();
			pos.x -= swtImgScrollComp.getHorizontalBar().getIncrement() * event.count;
			swtImgScrollComp.setOrigin(pos);		
			swtImageCanvas.redraw();
			
		}else{
			Point pos = swtImgScrollComp.getOrigin();
			pos.y -= swtImgScrollComp.getVerticalBar().getIncrement() * event.count;
			swtImgScrollComp.setOrigin(pos);
			swtImageCanvas.redraw();
		}
	}
	
	private String rerangeAllSyncObj = new String("rerangeAllSyncObj");
	
	private void canvasKeyDownEvent(Event event) {
		Point pos = swtImgScrollComp.getOrigin();
		int sign = 1;
		switch(event.keyCode){
			case SWT.ARROW_UP: sign = -1;
			case SWT.ARROW_DOWN:
				pos.y += sign * (((event.stateMask & SWT.CTRL) != 0) ? swtImgScrollComp.getVerticalBar().getPageIncrement()
															: swtImgScrollComp.getVerticalBar().getIncrement());
				
				break;
			case SWT.ARROW_LEFT: sign=-1;
			case SWT.ARROW_RIGHT:
				pos.x += sign * (((event.stateMask & SWT.CTRL) != 0) ? swtImgScrollComp.getHorizontalBar().getPageIncrement()
																	: swtImgScrollComp.getHorizontalBar().getIncrement());
				
				break;
		}

		switch(event.character){
			case 'a': 		
				setImagePos(posX - 1, posY, false);
				break;
			case 'w': 
				setImagePos(posX, posY - 1, false);
				break;
			case 's':
				setImagePos(posX, posY + 1, false);
				break; 
			case 'd':
				setImagePos(posX + 1, posY, false);				
				break;
			case 'c': //'click'
				setImagePos(posX, posY, true);
				break;
				
			case 'r': // re-calculate range of this image (validate), which will queue and update when done
				getSelectedImageFromConnectedSource().imageChanged(true);				
				break;
				
			case 'R':
				ImageProcUtil.ensureFinalUpdate(rerangeAllSyncObj, () -> {
					// re-calculate all images
					for(int i=0; i < connectedSource.getNumImages(); i++) {
						connectedSource.getImage(i).imageChanged(false);
					}
				});
					
				break;
			default:
				return;
		}
		swtImgScrollComp.setOrigin(pos);		
		swtImageCanvas.redraw();

	}
	
	private void canvasMouseDownEvent(Event event){
		swtImageCanvas.forceFocus();
		if(selectStartTime < 0){
			// start secltion
			selectStartTime = System.currentTimeMillis();
			
			if(event.button != 2)
				setSel(event.x, event.y, true);
			
			swtImageCanvas.redraw(); //redraw without the crosshair lines
		}
				
		
	}
	
	private void canvasMouseUpEvent(Event event){
		
		if((System.currentTimeMillis() - selectStartTime) < 500){
			selectStartTime = -1;
		}
		
		if(selectStartTime < 0){
			switch(event.button){
				case 1: setCanvasPos(event.x, event.y, true); posSelLock = POS_SEL_LOCKED; break;
				case 2: moveSel(event.x, event.y); posSelLock = SEL_UNLOCKED; break;
				case 3: setCanvasPos(event.x, event.y, true); posSelLock = POS_UNLOCKED; break;
			}
		}
		
		if(selectStartTime >= 0){
			// stop selecting
			setSel(event.x, event.y, false);
			selectStartTime = -1;
		}
		
	}
	
	private void canvasMouseMoveEvent(Event event){
		if(selectStartTime >= 0){
			setSel(event.x, event.y, false);
			
		}else if(posSelLock == POS_UNLOCKED){
			setCanvasPos(event.x, event.y, false);		
		}else if(posSelLock == SEL_UNLOCKED){
			moveSel(event.x, event.y);
			setCanvasPos(event.x, event.y, false);		
		}
	}
	
	private void setCanvasPos(int canvasX, int canvasY, boolean click){
		double scale[] = getScale();
		int imageX = (int)(canvasX / scale[0] + 0.5);
		int imageY = (int)(canvasY / scale[1] + 0.5);
		setImagePos(imageX, imageY, click);
	}
	
	public int[] getImagePos() { return new int[] { posX, posY };	}
	
	public void setImagePos(int imageX, int imageY, boolean click){
		
		Img img = getSelectedImageFromConnectedSource();
		if(img == null)
			return;
		posX = imageX; 
		posY = imageY;
		if(posX >= img.getWidth())
			posX = img.getWidth() - 1;
		if(posY >= img.getHeight())
			posY = img.getHeight() - 1;

		if(click){
			shellOps.imgSinkOps().fixedPos(posX, posY);
			
		}else{
			shellOps.imgSinkOps().movingPos(posX, posY);
		}
	
		swtImageCanvas.redraw();
	}
	
	public void redraw(){
		swtImageCanvas.redraw();
	}
	
	private void setSel(int canvasX, int canvasY, boolean init){
			
		double scale[] = getScale();
		int x = (int)(canvasX / scale[0] + 0.5);
		int y = (int)(canvasY / scale[1] + 0.5);
		
		if(init){
			selX0 = x;	selY0 = y;
			selW = 0; 	selH = 0;
			return;
		}
		
		selW = x - selX0;
		selH = y - selY0;
		
		//if(selW != 0 && selH != 0){
			Img img = getSelectedImageFromConnectedSource();
			if(img == null)
				return;
			//flip the x/w, y/h if they're drawing it backwards
			int x0 = selX0;
			int y0 = selY0;
			int w = selW;
			int h = selH;
			if(x0 >= img.getWidth())
				x0 = img.getWidth()-1;
			if(y0 >= img.getHeight())
				y0 = img.getHeight()-1;
			if(w == 0)
				w=1;
			if(h==0)
				h=1;
			if(w < 0){
				x0 = x0 + w + 1; 			
				w = -w;
			}
			if(h < 0){
				y0 = y0 + h + 1;
				h = -h;
			}
			shellOps.imgSinkOps().setRect(x0, y0, w, h);
			
		//}
	
		swtImageCanvas.redraw();
	}
	
	private void moveSel(int canvasX, int canvasY){
		double scale[] = getScale();
		int x = (int)(canvasX / scale[0] + 0.5);
		int y = (int)(canvasY / scale[1] + 0.5);
		if(selW == 0 || selH == 0)
			return;
		selX0 = x - selW/2;
		selY0 = y - selH/2;
		
		//flip the x/w, y/h if they're drawing it backwards
		int x0 = selX0;
		int y0 = selY0;
		int w = selW;
		int h = selH;
		if(w < 0){
			x0 = x0 + w + 1; 			
			w = -w;
		}
		if(h < 0){
			y0 = y0 + h + 1;
			h = -h;
		}
		
		shellOps.imgSinkOps().setRect(x0, y0, w, h);
		

		swtImageCanvas.redraw();
	}
	
	private void canvasPaintEvent(PaintEvent event){
        Rectangle clientArea = swtImageCanvas.getClientArea();
        Img image = getSelectedImageFromConnectedSource();
                
        if(image != null && swtCurImage != null){
        	double scale[] = getScale();
        	int imgX0 = (int)((double)event.x / scale[0]);
        	int imgY0 = (int)((double)event.y / scale[1]);
        	int imgW = (int)((double)event.width / scale[0]) + 2;
        	int imgH = (int)((double)event.height / scale[1]) + 2;
        	if((imgX0 + imgW) >= image.getWidth()){
        		imgW = image.getWidth() - imgX0;
        	}
        	if((imgY0 + imgH) >= image.getHeight()){
        		imgH = image.getHeight() - imgY0;
        	}
        	try{
        		event.gc.setInterpolation(SWT.NONE);
//	        	event.gc.drawImage(swtImage, 0, 0,  image.getWidth(),  image.getHeight(), 
//		        		0, 0, (int)(image.getWidth()*scale), (int)(image.getHeight()*scale));
        		event.gc.drawImage(swtCurImage, imgX0, imgY0, imgW, imgH, 
	        			(int)(imgX0*scale[0]), 
	        			(int)(imgY0*scale[1]), 
	        			(int)(imgW*scale[0]), 
	        			(int)(imgH*scale[1]));
        	}catch(IllegalArgumentException e){
        		System.out.println("DRAW FAILED: " + scale + "\t" + imgX0 +", "+ imgY0+", "+ imgW+", "+ imgH +"\t-->\t" +
    	        		event.x+", "+ event.y+", "+ event.width+", "+ event.height);
        	}        
	        event.gc.setForeground(swtImageCanvas.getDisplay().getSystemColor(SWT.COLOR_BLACK));
	        if(selectStartTime >= 0){
	        	event.gc.setLineStyle(SWT.LINE_SOLID);
	        	event.gc.drawRectangle(
	        			(int)(selX0 * scale[0]), (int)(selY0 * scale[1]),
	        			(int)(selW * scale[0]), (int)(selH * scale[1]));
	        }else{
	        	event.gc.setLineStyle(posSelLock == POS_SEL_LOCKED ? SWT.LINE_DASH : SWT.LINE_DOT);
	        	event.gc.drawLine(0, (int)(posY * scale[1]), clientArea.width, (int)(posY * scale[1]));
		        event.gc.drawLine((int)(posX * scale[0]), 0, (int)(posX * scale[0]), clientArea.height);
	        }
	        
	        event.gc.setLineStyle(SWT.LINE_SOLID);
	        shellOps.imgSinkOps().drawOnImage(event.gc, scale, image.getWidth(), image.getHeight());
	        
	    	        
        }else{
        	event.gc.fillRectangle(clientArea);
        	if(image == null){
        		event.gc.drawText("null image", 0, 0);
        	}else if(image.isDestroyed()){
        		event.gc.drawText("Image Destroyed", 0, 0);
        	}else{
        		event.gc.drawText("Image OK but no swtCurImage!?!", 0, 0);	
        	}
        	
        }
        
	}
	
	private void shellClosedEvent(Event event){

		//setSource(null);
		if(connectedSource != null)
			connectedSource.removeSink(this);
	}
	
	@Override
	/** Image has changed. We really need to deal with this since the image has changed in the buffer
	 * and we'll never be able to recreate the old image.
	 * 
	 * This can come in from an unknown thread (not the SWT one)
	 */
	public void imageChangedRangeComplete(int idx) { 
		//System.out.println("imageChanged("+connectedSource.toShortString()+", idx="+idx);
		Img img = connectedSource.getImage(idx);		
		if(img == null){
			logr.finer("ImageChanged called with null image.");
		}
		if(swtImgScrollComp == null || swtImgScrollComp.isDisposed() || img == null || img.isDestroyed())
			return;
		
		final int changedIdx = idx;
		
		//we need this to be run by the GUI thread in the end, but we 
		//need it to follow the usual 'once only and at least after last'
		// update rulesy
		ImageProcUtil.ensureFinalSWTUpdate(swtImgScrollComp.getDisplay(), imageChangeSyncObj, new Runnable() {
			@Override
			public void run() {
				imageChangedUpdate(changedIdx);
			}
		}, 100);
		
		checkScale();
	}
	
	private void imageChangedUpdate(int changedIdx) {
		//System.out.println("imageChangedUpdate("+connectedSource.toShortString()+", idx="+changedIdx);
		if(!swtImgScrollComp.isDisposed()){
			if(changedIdx != getSelectedSourceIndex() && gotoChanged){
				Img img = connectedSource.getImage(changedIdx);
				if(img != null && img.isRangeValid())
					setSelectedSourceIndex(changedIdx);						
			}
			if(changedIdx == getSelectedSourceIndex()){
				createSWTImage();
			}
		}
	}
	
	/** Generate the SWT image object from the Img object */
	private void createSWTImage(){
		//System.out.println("createSWTImage("+connectedSource.toShortString()+", i="+getSelectedSourceIndex());
		if(swtImgScrollComp == null || swtImgScrollComp.isDisposed())
			return;
		
		final Img image = getSelectedImageFromConnectedSource();
				
		if(image == null || image.isDestroyed()){
			doSetImage(null, 300, 300);			
			return;
		}
		
		// queue the image convert
		
		final double colorScale = (double)shellOps.dispPropsPanel().swtColorLogScale.getSelection() * 0.02;
		final double minCut = (double)shellOps.infoPanel().swtColorMinSpinner.getSelection() / 1000;
		final double maxCut = (double)shellOps.infoPanel().swtColorMaxSpinner.getSelection() / 1000;
		final boolean relMax = shellOps.infoPanel().relativeMinMax.getSelection();
		final double alphaScale = shellOps.infoPanel().alphaBySigma.getSelection();
		final int field = shellOps.topBar().fieldSpinner().getSelection();
		final int colorMap = shellOps.infoPanel().swtColorMapCombo.getSelectionIndex();
		ImageProcUtil.ensureFinalUpdate(imageConvertSyncObj, new Runnable() { @Override public void run() { 
			doImageConvert(image, colorScale, colorMap, minCut, maxCut, relMax, alphaScale, field); 
		} });
	}
	
	/** Convert the given image to a SWT ImageData (non-SWT thread because it takes a while) */ 
	private void doImageConvert(Img image, double colorScale, int colorMap, double minCut, double maxCut, boolean relativeMinMax, double alphaScale, int field){
		//System.out.println("doImageConvert("+connectedSource.toShortString()+", i="+getSelectedSourceIndex());
		if(cMap == null || cMapType != colorMap){
			switch(colorMap){
				case 0: //jet
					 cMapDbl = ColorMaps.jet(nCols);
					 cMap = new byte[nCols*3];
					 for(int i=0; i < nCols; i++){
						 cMap[i*3+0] = (byte)(cMapDbl[i][0]*255);
						 cMap[i*3+1] = (byte)(cMapDbl[i][1]*255);
						 cMap[i*3+2] = (byte)(cMapDbl[i][2]*255);
					 }
					 break;
				case 1: //greyscale					 
					 cMap = new byte[nCols*3];
					 for(int i=0; i < nCols; i++){
						 cMap[i*3+0] = (byte)((i*255)/nCols);
						 cMap[i*3+1] = (byte)((i*255)/nCols);
						 cMap[i*3+2] = (byte)((i*255)/nCols);
					 }
					 break;
				case 2: //matplotlib blue-white-red
					 cMap = new byte[nCols*3];
					 for(int i=0; i < nCols/2; i++){
						 cMap[i*3+0] = (byte)((i*255)*2/nCols);
						 cMap[i*3+1] = (byte)((i*255)*2/nCols);
						 cMap[i*3+2] = (byte)(255);
					 }
					 for(int i=0; i < nCols/2; i++){
						 cMap[(nCols/2+i)*3+0] = (byte)255;
						 cMap[(nCols/2+i)*3+1] = (byte)((256-(i*255))*2/nCols);
						 cMap[(nCols/2+i)*3+2] = (byte)((256-(i*255))*2/nCols);
					 }
					 break;
				default: //matplotlib inferno
					 double[][][] cmapData = {
							 	{//p.cm.inferno
								 	{ 0.00,0.03,0.09,0.18,0.27,0.36,0.44,0.53,0.61,0.69,0.77,0.84,0.90,0.94,0.97,0.99,0.98,0.97,0.95,0.99 },
								 	{ 0.00,0.02,0.05,0.04,0.04,0.07,0.10,0.13,0.16,0.20,0.24,0.29,0.36,0.44,0.53,0.63,0.73,0.84,0.93,1.00 },
								 	{ 0.01,0.11,0.23,0.35,0.41,0.43,0.43,0.42,0.39,0.35,0.31,0.25,0.19,0.12,0.05,0.03,0.12,0.26,0.44,0.64 },
								 },{//p.cm.viridis
									 {0.27,0.28,0.28,0.28,0.28,0.27,0.25,0.24,0.22,0.20,0.19,0.17,0.16,0.15,0.13,0.12,0.12,0.13,0.15,0.19,0.25,0.31,0.39,0.47,0.55,0.64,0.73,0.82,0.92,0.99 },
									 {0.00,0.05,0.10,0.15,0.19,0.23,0.27,0.31,0.35,0.38,0.42,0.45,0.48,0.52,0.55,0.58,0.61,0.65,0.68,0.71,0.74,0.77,0.79,0.82,0.84,0.86,0.87,0.88,0.90,0.91 },
									 {0.33,0.38,0.42,0.46,0.49,0.52,0.53,0.54,0.55,0.55,0.56,0.56,0.56,0.56,0.55,0.55,0.54,0.52,0.51,0.48,0.45,0.42,0.37,0.32,0.28,0.22,0.16,0.11,0.10,0.14 },
								 },{		//		p.cm.plasma
									 {0.05,0.13,0.20,0.26,0.32,0.37,0.42,0.48,0.53,0.58,0.63,0.67,0.71,0.74,0.78,0.81,0.84,0.87,0.90,0.92,0.94,0.96,0.98,0.99,0.99,0.99,0.99,0.98,0.96,0.94 },
									 {0.03,0.02,0.02,0.01,0.01,0.00,0.00,0.01,0.03,0.06,0.10,0.14,0.18,0.22,0.26,0.30,0.34,0.38,0.42,0.46,0.50,0.55,0.60,0.65,0.69,0.75,0.80,0.86,0.92,0.98 },
									 {0.53,0.56,0.59,0.62,0.64,0.65,0.66,0.66,0.65,0.64,0.61,0.58,0.55,0.52,0.49,0.46,0.42,0.39,0.37,0.34,0.31,0.28,0.24,0.21,0.19,0.16,0.15,0.14,0.15,0.13 },
								 },{				//	p.cm.nipy_spectral				 
									 {0.00,0.29,0.49,0.51,0.14,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.29,0.75,0.89,0.97,1.00,1.00,1.00,0.97,0.87,0.82,0.80,0.80 },
									 {0.00,0.00,0.00,0.00,0.00,0.00,0.04,0.37,0.53,0.61,0.66,0.67,0.65,0.60,0.69,0.78,0.87,0.97,1.00,0.99,0.95,0.87,0.76,0.62,0.27,0.00,0.00,0.00,0.30,0.80 },
									 {0.00,0.33,0.56,0.60,0.65,0.76,0.87,0.87,0.87,0.83,0.69,0.59,0.41,0.03,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.30,0.80 },
								 },{ //PiYG		 
									 {0.56,0.62,0.70,0.77,0.81,0.84,0.87,0.90,0.93,0.95,0.97,0.98,0.99,0.98,0.97,0.96,0.93,0.91,0.87,0.80,0.74,0.66,0.59,0.51,0.44,0.38,0.31,0.25,0.20,0.15, },
									 {0.00,0.04,0.07,0.11,0.24,0.37,0.48,0.56,0.65,0.73,0.79,0.85,0.89,0.92,0.95,0.97,0.96,0.96,0.95,0.92,0.89,0.85,0.79,0.74,0.69,0.63,0.58,0.51,0.45,0.39, },
									 {0.32,0.37,0.43,0.49,0.56,0.63,0.69,0.75,0.81,0.86,0.89,0.92,0.94,0.95,0.96,0.94,0.89,0.83,0.76,0.66,0.55,0.46,0.36,0.27,0.22,0.18,0.13,0.12,0.11,0.10, },
								 },{ //PRGn					 
									 {0.25,0.32,0.39,0.47,0.51,0.56,0.61,0.66,0.72,0.78,0.83,0.88,0.91,0.94,0.96,0.95,0.91,0.86,0.81,0.74,0.67,0.58,0.47,0.36,0.29,0.20,0.11,0.07,0.03,0.00, },
									 {0.00,0.05,0.11,0.17,0.27,0.36,0.45,0.52,0.59,0.67,0.73,0.80,0.85,0.90,0.94,0.96,0.95,0.94,0.93,0.90,0.87,0.81,0.75,0.69,0.62,0.55,0.47,0.40,0.33,0.27, },
									 {0.29,0.36,0.44,0.52,0.57,0.63,0.68,0.73,0.78,0.82,0.86,0.89,0.92,0.94,0.96,0.94,0.89,0.84,0.79,0.72,0.65,0.56,0.48,0.39,0.34,0.28,0.22,0.18,0.14,0.11, },
								 },{		//BrBG			 
									 {0.33,0.40,0.48,0.55,0.62,0.69,0.75,0.80,0.84,0.88,0.92,0.95,0.96,0.96,0.96,0.93,0.87,0.80,0.73,0.63,0.53,0.43,0.32,0.22,0.15,0.08,0.01,0.00,0.00,0.00, },
									 {0.19,0.23,0.27,0.32,0.39,0.45,0.52,0.61,0.70,0.78,0.83,0.88,0.92,0.93,0.95,0.95,0.94,0.92,0.90,0.86,0.82,0.75,0.68,0.60,0.54,0.47,0.40,0.35,0.29,0.24, },
									 {0.02,0.03,0.03,0.04,0.09,0.14,0.19,0.30,0.41,0.52,0.61,0.71,0.79,0.86,0.93,0.95,0.93,0.91,0.87,0.82,0.77,0.71,0.64,0.57,0.51,0.44,0.37,0.31,0.24,0.19, },
								 },{ //PuOr	 
									 {0.50,0.56,0.63,0.71,0.77,0.83,0.88,0.92,0.96,0.99,0.99,1.00,0.99,0.98,0.97,0.95,0.90,0.86,0.82,0.77,0.71,0.65,0.58,0.51,0.45,0.39,0.33,0.28,0.22,0.18, },
									 {0.23,0.27,0.31,0.35,0.41,0.46,0.52,0.59,0.67,0.74,0.79,0.85,0.89,0.92,0.95,0.95,0.91,0.87,0.82,0.75,0.69,0.61,0.54,0.46,0.37,0.26,0.16,0.10,0.05,0.00, },
									 {0.03,0.03,0.03,0.02,0.04,0.06,0.09,0.20,0.31,0.42,0.54,0.65,0.74,0.83,0.92,0.96,0.94,0.93,0.90,0.87,0.83,0.79,0.73,0.68,0.64,0.59,0.54,0.45,0.37,0.29, },
								 },{ // RdYlBu			 
									 {0.65,0.71,0.78,0.85,0.89,0.93,0.96,0.97,0.98,0.99,0.99,1.00,1.00,1.00,1.00,0.98,0.94,0.89,0.84,0.76,0.69,0.62,0.54,0.46,0.40,0.34,0.27,0.24,0.22,0.19, },
									 {0.00,0.06,0.13,0.19,0.28,0.36,0.44,0.53,0.62,0.70,0.77,0.84,0.89,0.94,0.98,0.99,0.98,0.96,0.93,0.90,0.86,0.81,0.75,0.69,0.62,0.54,0.46,0.38,0.29,0.21,}, 
									 {0.15,0.15,0.15,0.16,0.19,0.23,0.27,0.31,0.35,0.40,0.46,0.53,0.59,0.65,0.72,0.79,0.87,0.95,0.96,0.94,0.92,0.89,0.86,0.82,0.79,0.75,0.71,0.67,0.62,0.58,}, 
								 }									 
					 };
					 int iMap = colorMap-3;
					 if(iMap >= cmapData.length)
						throw new IllegalArgumentException("Unknown color map: " + colorMap);
					 int nD = cmapData[iMap][0].length;
					 Interpolation1D interpR = new Interpolation1D(Mat.linspace(0, 1, nD), cmapData[iMap][0]);
					 Interpolation1D interpG = new Interpolation1D(Mat.linspace(0, 1, nD), cmapData[iMap][1]);
					 Interpolation1D interpB = new Interpolation1D(Mat.linspace(0, 1, nD), cmapData[iMap][2]);

					 cMap = new byte[nCols*3];
					 for(int i=0; i < nCols; i++){
						 cMap[i*3+0] = (byte)(interpR.eval(i/(double)nCols)*255);
						 cMap[i*3+1] = (byte)(interpG.eval(i/(double)nCols)*255);
						 cMap[i*3+2] = (byte)(interpB.eval(i/(double)nCols)*255);
					 }
					 break;
					
			}
			cMapType = colorMap;
			 
		}

		/* This turns out to be slower, meh
		if(vMap == null || colorScale != vMapColourScale){
			//value map - list of 0.0-1.0 values that match each colour entry
			vMap = new double[nCols];
			for(int i=0; i < nCols; i++){
				double x = (double)i / (nCols-1);			
				vMap[i] = FastMath.exp(FastMath.log(x) / colorScale); 
			}
			vMapColourScale = colorScale;
		}*/
		
		if(image.getNFields() < 2)
			alphaScale = 0;
		double weightNone = 1.0 / 1.00;
		double weightFull = 1.0 / 0.05;
		
		PaletteData palette = new PaletteData(0xFF0000, 0xFF00 , 0xFF);		
		
		ImageData swtImageData = null;
		
		ReadLock readLock = image.readLock();
		try{
			readLock.lockInterruptibly();
		}catch(InterruptedException e){
			System.out.println("ImageWindow.createSWTImage() interuptted waiting for read lock");
			queueSetImage(null, 500, 500);			
			return;
		}
		try{
			
			if(!image.isRangeValid()){
				logr.log(Level.FINE, "ImageWindow: Image " + image + " has invalid range, not actually switching to it.");
				
				swtImageData = new ImageData(500, 500, 24, palette);
				
				for(int y=0; y < 500; y++)	{				
					for(int x=0; x < 500; x++)	{	
						byte v = (byte)(((x % 100 > 50) ^ (y % 10 < 5)) ? 255 : 0);
						swtImageData.data[(y*500+x)*3+0] = v;
						swtImageData.data[(y*500+x)*3+1] = v;
						swtImageData.data[(y*500+x)*3+2] = v;
					}
				}
				
				Image newSWTImage = new Image(swtImgScrollComp.getDisplay(), swtImageData);
				queueSetImage(newSWTImage, 500, 500);
				return;
			}
			
			int w = image.getWidth();
			int h = image.getHeight();		
			
			swtImageData = new ImageData(w, h, 24, palette);
			
			
			double min,max;
			if(relativeMinMax){
				min = image.getMin() + (minCut/1000) * (image.getMax() - image.getMin());
				max = image.getMin() + (maxCut/1000) * (image.getMax() - image.getMin());
			}else{
				min = minCut;
				max = maxCut;
			}
						
	        for(int y=0; y < h; y++){
				for(int x=0; x < w; x++){
					double v = image.getPixelValue(readLock, field, x, y);
					if(Double.isNaN(v)){
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 0] = (byte)255;
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 1] = (byte)255;
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 2] = (byte)255;
						continue;
					}
					
					//scale color
					double f = (v - min) / (max - min);
					
					if(colorScale != 1.0)
						f = FastMath.pow(f, colorScale);
					
					int c = (int)((nCols-1) * f);				
					if(c < 0)c=0;
					if(c >= nCols)c=nCols-1;
					//*/
					
					/* This turns out to be slower... meh
					int c2 = Mat.getNearestIndex(vMap, f);
					/*if(c2 != c){
						System.out.println(f + "\t" + c + "\t" + c2);
					}//*/
					
					if(alphaScale > 0){
						double c0 = cMapDbl[c][0] * 255;
						double c1 = cMapDbl[c][1] * 255;
						double c2 = cMapDbl[c][2] * 255;
						
						double fracError = image.getPixelValue(readLock, 1, x, y) / v;
						double weight = 1.0 / (fracError);
						double alpha = 1.0 - (weight - weightFull) / (weightNone - weightFull);
						if(alpha < 0)
							alpha = 0.0;
						if(alpha > 1.0)
							alpha = 1.0;			
						alpha = FastMath.pow(alpha, 5.0 / (((100 - alphaScale) / 10) + 1.0));
						if(x==250 && y == 250)
							alpha *= 1.0;
						
						c0 = 255 - alpha*(255 - c0);
						c1 = 255 - alpha*(255 - c1);
						c2 = 255 - alpha*(255 - c2);

						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 0] = (byte)c0;
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 1] = (byte)c1;
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 2] = (byte)c2;
					}else{
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 0] = cMap[c*3+0];
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 1] = cMap[c*3+1];
						swtImageData.data[y*swtImageData.bytesPerLine + x*3 + 2] = cMap[c*3+2];					
					}
					
				}	
			}
		}finally{
			readLock.unlock();
		}
        
		if(swtImgScrollComp.isDisposed())
			return;
		
        Image newSWTImage = new Image(swtImgScrollComp.getDisplay(), swtImageData);
        
        //the the actual setting need to be done in the SWT thread
        queueSetImage(newSWTImage, image.getWidth(), image.getHeight());
        
	}
	
	private void queueSetImage(final Image newSWTImage, final int w, final int h){
		ImageProcUtil.ensureFinalSWTUpdate(swtImgScrollComp.getDisplay(), imageDisplaySyncObj, new Runnable() { @Override public void run() { 
        	doSetImage(newSWTImage, w, h); 
        } });
	}
	
	private void doSetImage(Image swtImage, int imgWidth, int imgHeight){
		//System.out.println("doSetImage("+connectedSource.toShortString()+", i="+getSelectedSourceIndex());
		if(swtCurImage != null)
			swtCurImage.dispose();
		
		if(swtImgScrollComp.isDisposed())
			return;
		
		this.swtCurImage = swtImage;
        swtImageCanvas.setSize(
				(int)(imgWidth*getScale()[0]), 
				(int)(imgHeight*getScale()[1]));
		swtImageCanvas.redraw();
		updateInfoPanel();
	}
	
	@Override
	public void notifySourceChanged(){
		//System.out.println("notifySourceChanged("+connectedSource.toShortString()+", i="+getSelectedSourceIndex());
		
		if(swtImgScrollComp == null || swtImgScrollComp.isDisposed()) return;
		//we need this to be run by the GUI thread in the end, but we 
		//need it to follow the usual 'once only and at least after last'
		// update rules
		ImageProcUtil.ensureFinalSWTUpdate(swtImgScrollComp.getDisplay(), imageSetChangeSyncObj,
					new Runnable() { public void run() { imageIndexChanged(); } });
		
		
	}
	

	void imageIndexChanged() {
		MainTopBar topBar = shellOps.topBar();
		//System.out.println("imageIndexChanged("+connectedSource.toShortString()+", i="+getSelectedSourceIndex());
		if(swtImgScrollComp.isDisposed()) return;
			
		int imageIndex = topBar.imageIndexSpinner().getSelection();		
		int nImages = (connectedSource != null) ? connectedSource.getNumImages() : 0;
		
		if(nImages <= 0){
			nImages = 0;
		    imageIndex = -1;
		    topBar.imageIndexSpinner().setValues(-1, -2, -1, 0, 1, 10);
			topBar.imageIndexSpinner().setEnabled(true); //false);
		    
		}else{
			//if we now have more images that we allowed for before, we have a new one
			if((nImages-1) >  topBar.imageIndexSpinner().getMaximum() 
					&& (shellOps.infoPanel().swtAcceptNewButton.getSelection())){
				//don't switch to to it if hasn't finished yet
				Img newImg = connectedSource.getImage(nImages-1);
				if(newImg != null && newImg.isRangeValid())				
					imageIndex = nImages-1;
			}else if(imageIndex < 0){
				imageIndex = 0;
			}
			topBar.imageIndexSpinner().setMinimum(-1);
			topBar.imageIndexSpinner().setMaximum(nImages-1);
			topBar.imageIndexSpinner().setPageIncrement(10);
			if(topBar.imageIndexSpinner().getSelection() != imageIndex) //don't change if same as current
				topBar.imageIndexSpinner().setSelection(imageIndex);
			//swtImageIndexSpinner.setValues(imageIndex, -2, nImages-1, 0, 1, 10);
			topBar.imageIndexSpinner().setEnabled(true);
		}
		
		setSelectedSourceIndex(imageIndex);
		
		//System.out.println("imageIndexChanged(), now nImages="+nImages+", index=" + imageIndex);
		
	}
	
	@Override
	public void setSelectedSourceIndex(int index) {
		//if(connectedSource != null)
			//System.out.println("setSelectedSourceIndex("+connectedSource.toShortString()+", i="+getSelectedSourceIndex());
		if(index == selectedSourceIndex)
			return;
		
		super.setSelectedSourceIndex(index);

		MainTopBar topBar = shellOps.topBar();
		 //if(swtImageIndexSpinner.getSelection() != getSelectedSourceIndex()){
        //	swtImageIndexSpinner.setSelection(selectedSourceIndex);
        //}
		int nImages = (connectedSource != null) ? connectedSource.getNumImages() : 0;
		if(topBar.imageIndexSpinner().getMinimum() != -2)
			topBar.imageIndexSpinner().setMinimum(-2);
		if(topBar.imageIndexSpinner().getMaximum() != nImages-1)
			topBar.imageIndexSpinner().setMaximum(nImages-1);		
		if(topBar.imageIndexSpinner().getSelection() != selectedSourceIndex)
			topBar.imageIndexSpinner().setSelection(selectedSourceIndex);
		//swtImageIndexSpinner.setValues(selectedSourceIndex, -2, nImages-1, 0, 1, 10);
		
		//also sync the image index in all the other sinks connected to this source
		if(connectedSource != null)
			for(ImgSink sink : connectedSource.getConnectedSinks())
				sink.setSelectedSourceIndex(selectedSourceIndex);
	}

	@Override
	protected void selectedImageChanged() {
		
		createSWTImage();					
			
		//System.out.println("ImageWindow " + hashCode() + " moved to image " + ((image == null) ? null : image.hashCode()));
		
		updateInfoPanel();
	}
	

	public static final int scaleToSel(double scale){
		return (int)(FastMath.log(scale) / logScaleConst) + 250;
	}
	
	public static final double selToScale(int sel){
		return FastMath.pow(scaleConst, sel - 250);
	};
	
	public double[] getScale(){ return new double[]{ scaleX, scaleY }; }	
	
	public double getScaleX(){ return scaleX; }
	public double getScaleY(){ return scaleY; }
	
	
	@Override
	public void setSource(ImgSource source){
		super.setSource(source);
		
		shellOps.imgSinkOps().setSource(source);
		
		
		
		if(swtImgScrollComp != null && !swtImgScrollComp.isDisposed()) {
					
			shellOps.infoPanel().repopulatePipeTree();

			checkScale();
		}
		
	}
	
	public Point getScrollOrigin() { return swtImgScrollComp.getOrigin();	}

	public void bringToFront() { shellOps.forceActive();	}

	@Override
	public String toString() {
		return "ImageWindow[src=" + (connectedSource == null ? "null" : connectedSource.toShortString()) + "]";
	}

	public Image getSWTImage() { return swtCurImage; }

	@Override
	public boolean isIdle() { return true; } // window never does any processing


	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }

	public int getColorMap() { return cMapType; }
	
	public void setGotoChanged(boolean gotoChanged){ 
		this.gotoChanged = gotoChanged;
		shellOps.topBar().gotoChangedCheckbox().setSelection(gotoChanged);
		shellOps.infoPanel().updateGUI(); 
	}
	public boolean getGotoChanged(){ return gotoChanged; }

	public boolean getBreakAspect() { return breakAspect;	}

	public void setOrigin(int x0, int y0) {
		swtImgScrollComp.setOrigin(x0, y0);
	}

	public ShellOps getShellOps() { return shellOps; }

	
}
