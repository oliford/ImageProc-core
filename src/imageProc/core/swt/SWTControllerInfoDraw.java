package imageProc.core.swt;

import org.eclipse.swt.graphics.GC;

import imageProc.core.ImagePipeController;

/** Controllers that want to draw on the GUI image, for user info */
public interface SWTControllerInfoDraw extends ImagePipeController {

	public void drawOnImage(GC gc, double[] scale, int imageWidth, int imageHeight, boolean asSource);
}
