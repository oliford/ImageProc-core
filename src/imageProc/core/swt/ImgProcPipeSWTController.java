package imageProc.core.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgProcessor;

/** Base SWT controller (GUI) for ImgProcPipe (things that process one set of images to make another set)
 * 
 * 
 * @author oliford
 *
 */
public abstract class ImgProcPipeSWTController extends ImageProcessorSWTController {

	private Button diskMappedCheckbox;
	
	/** Called by ImgProcessorSWTController to add things specific to pipes */
	@Override
	protected Composite addProcPipeControls(Composite parent) {
		Composite swtPipeControlsGroup = new Composite(parent, SWT.None);
		swtPipeControlsGroup.setLayout(new GridLayout(1, false));
		
		diskMappedCheckbox = new Button(swtPipeControlsGroup, SWT.CHECK);
		diskMappedCheckbox.setText("Disk Memory");
		diskMappedCheckbox.setToolTipText("Use memory mapped to a file in the path stored under minerva.cache.mappedMemoryPath in minerva-settings.");
		diskMappedCheckbox.setEnabled(true);
		diskMappedCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { diskMappedCheckboxEvent(event); } });
		diskMappedCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		return swtPipeControlsGroup;
	}
		
	@Override
	protected void doUpdate() {
		super.doUpdate();
		
		diskMappedCheckbox.setSelection(getPipe().getDiskMemoryLimit() == Long.MIN_VALUE);			
	}
	
	private void diskMappedCheckboxEvent(Event e){
		getPipe().setDiskMemoryLimit(diskMappedCheckbox.getSelection() ? Long.MIN_VALUE : Long.MAX_VALUE);		
		getPipe().calc();
	}
	
	@Override
	public abstract ImgProcPipe getPipe();
	
}
