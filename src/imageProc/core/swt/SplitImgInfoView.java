package imageProc.core.swt;

import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import algorithmrepository.Algorithms;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.proc.fft.FFTProcessor;

/** Container for most of the main window.
 * Made up of interacting parts:
 * ImagePanel
 * DispPropsPanel
 * Tabs:
 *    Info tab
 *    Source tab
 *    Sink tabs
 *  */
public class SplitImgInfoView implements ImageSinkOps {

	private ImagePanel imagePanel;
	private ShellOps shellOps;
	
	private MainTopBar topBar;
	private Composite comp;
	
	private SashForm swtSashForm;
	private Composite swtLHSComposite;
	private Composite swtRHSComposite;
	private Composite swtInfoTabComposite;
	
	private ImagePipeController sourceControl;
	private Composite sourceControlComposite;
	
	
	private CTabItem swtSourceTab;
	private CTabItem swtInfoTab;
		
	private CTabFolder swtTabFoler;	
	private InfoPanel infoPanel;	
	private DispPropsPanel dispPropsPanel;
	private TabFolderMenu tabMenu;
	
	private ImgSourceOrSinkImpl guiPipe;
	
	private ConcurrentHashMap<ImgSink, ImagePipeController> sinkControllers = new ConcurrentHashMap<ImgSink, ImagePipeController>();
	
	
	public SplitImgInfoView(ShellOps shellOps) {
		this.shellOps = shellOps;

		topBar = new MainTopBar(this);
        imagePanel = new ImagePanel(shellOps);
        infoPanel = new InfoPanel(shellOps);
        dispPropsPanel = new DispPropsPanel(shellOps);
        tabMenu = new TabFolderMenu(shellOps);
        
        guiPipe = imagePanel; //for now
	}

	public void build(Composite parent) {
		
		comp = new Composite(parent, SWT.NONE);
        comp.setLayout(new GridLayout(1, false));
        comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        comp.getDisplay().addFilter(SWT.KeyDown, new Listener() {  @Override public void handleEvent(Event event) { globalKeyEvent(event); } });
        
        topBar.build(comp);
        
        swtSashForm = new SashForm(comp, SWT.HORIZONTAL | SWT.BORDER_SOLID);
        swtSashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        swtSashForm.setLayout(new GridLayout(2, false));
        
        swtLHSComposite = new Composite(swtSashForm, SWT.BORDER);
        swtLHSComposite.setLayout(new GridLayout(1, false));
                
		imagePanel.build(swtLHSComposite);
		
		dispPropsPanel.build(swtLHSComposite, SWT.BORDER_SOLID);
        	       
        swtRHSComposite = new Composite(swtSashForm, SWT.BORDER);
        swtRHSComposite.setLayout(new GridLayout(7, false));        
        
        swtTabFoler = new CTabFolder(swtRHSComposite, SWT.BORDER);
        swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 7, 1));
        swtTabFoler.setUnselectedCloseVisible(true);
        swtTabFoler.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) {
        		//bit of a hack to deal with SWT 4.x things not laying out properly when they're initialised hidden
        		//we need to kick them one thier first (here any) activation
	        	CTabItem ti = swtTabFoler.getSelection();
	        	if(ti == null) return;
	        	Control c = ti.getControl();
	        	if(c instanceof Composite)
	        		((Composite)c).layout(true, true);        	
        	} });

        tabMenu.build();                

        swtInfoTab = new CTabItem(swtTabFoler, SWT.NONE);
        
        swtInfoTabComposite = new Composite(swtTabFoler, SWT.NONE);
        swtInfoTabComposite.setLayout(new GridLayout(1, false));
        
        infoPanel.build(swtInfoTabComposite, SWT.BORDER_SOLID);
        
        swtInfoTab.setControl(swtInfoTabComposite);
        swtInfoTab.setText("&0: Info");
        swtInfoTab.setToolTipText("General information on the source and general GUI settings.");
        swtTabFoler.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				if(swtTabFoler.getSelection() == swtInfoTab)
					infoPanel.updateLists(true);
				
			}
		});
        
    	swtSourceTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSourceTab.setText("&1: Source");		
		swtSourceTab.setToolTipText("Settings and control of the image source this window is displaying.");
		System.out.println("init");
        
	}
	
	protected void globalKeyEvent(Event event) {
		//if((event.stateMask | SWT.SHIFT) != 0) {
			switch(event.keyCode) {
				/*case 's' | 'S':
					guiPipe.broadcastEvent(imageProc.core.EventReciever.Event.);
					break;*/
				case SWT.ESC:
					guiPipe.broadcastEvent(imageProc.core.EventReciever.Event.GlobalAbort);
					break;
						
			}
		//}
	}

	public ImagePanel imagePanel() { return imagePanel; }
	public MainTopBar topBar() { return topBar; }
	public InfoPanel infoPanel() { return infoPanel; }
	public DispPropsPanel dispPropsPanel() { return dispPropsPanel; }
	public ShellOps shellOps() { return shellOps; }

	public void initControllers() {
		//try to load controllers for all sinks already connected to the source
		if(guiPipe.getConnectedSource() != null){
			for(ImgSink sink : guiPipe.getConnectedSource().getConnectedSinks()){
				try{
					addSinkController(sink);
				}catch(RuntimeException err){
					System.err.println("Error creating SWT controller for sink '" + sink + "':");
					err.printStackTrace();
				}
			}
		}
	}
	
	/** Add an SWT controller for the given sink to our window */ 
	public void addSinkController(ImgSink sink){
		
		//find next ID for keyboard accelerator
		int nextIdx = 1;
		boolean available = true;
		do {
			available = true;
			nextIdx++;
			for(CTabItem c : swtTabFoler.getItems()) {
				String parts[] = c.getText().split(":");
				if(parts[0].charAt(0) == '&')
					parts[0] = parts[0].substring(1);
				int idx = Algorithms.mustParseInt(parts[0]);
				if(idx == nextIdx) {
					available = false;
					break;
				}
			}
		}while(!available);
		
		//ImagePipeController sinkController = (ImagePipeController)sink.createSinkController(Composite.class, new Object[]{ swtSidePane, SWT.BORDER_SOLID });
		final ImagePipeController sinkController = (ImagePipeController)sink.createSinkController(Composite.class, new Object[]{ swtTabFoler, SWT.BORDER_SOLID });
    	if(sinkController != null){
    		final Composite sinkComposite = (Composite)sinkController.getInterfacingObject();
    		//sinkComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
    		CTabItem sinkTab = new CTabItem(swtTabFoler, SWT.NONE | SWT.CLOSE);
    		sinkTab.setText(((nextIdx < 10) ? "&" : "") + nextIdx + ": " 
    						  + sinkController.getPipe().toShortString());
    		sinkTab.setControl(sinkComposite);
    		sinkTab.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) {
    			sinkController.destroy();
    			if(!sinkComposite.isDisposed())
    				sinkComposite.dispose();
    			sinkControllers.remove((ImgSink)sinkController.getPipe());
    			
    		}});
    		sinkTab.setData(sink);
    		
    		sinkControllers.put(sink, sinkController);
    		
    		if(sinkController instanceof ImagePipeSWTController){
    			((ImagePipeSWTController)sinkController).setTabItem(sinkTab); //some plugins change the name on their tab
    		}
        }
    }

	public void fixedPos(int posX, int posY) {
		if(sourceControl instanceof ImagePipeControllerROISettable)
			((ImagePipeControllerROISettable)sourceControl).fixedPos(posX, posY);
		for(ImagePipeController sinkController : sinkControllers.values())			
			if(sinkController instanceof ImagePipeControllerROISettable)
				((ImagePipeControllerROISettable)sinkController).fixedPos(posX, posY);
	}

	public void movingPos(int posX, int posY) {
		if(sourceControl instanceof ImagePipeControllerROISettable)
			((ImagePipeControllerROISettable)sourceControl).movingPos(posX, posY);
	
		for(ImagePipeController sinkController : sinkControllers.values())			
			if(sinkController instanceof ImagePipeControllerROISettable)
				((ImagePipeControllerROISettable)sinkController).movingPos(posX, posY);
	}

	public void setRect(int x0, int y0, int w, int h) {
		if(sourceControl instanceof ImagePipeControllerROISettable)
			((ImagePipeControllerROISettable)sourceControl).setRect(x0, y0, w, h);
		
		for(ImagePipeController sinkController : sinkControllers.values())			
			if(sinkController instanceof ImagePipeControllerROISettable)
				((ImagePipeControllerROISettable)sinkController).setRect(x0, y0, w, h);
	}

	public void drawOnImage(GC gc, double[] scale, int width, int height) {
		for(ImagePipeController sinkController : sinkControllers.values())
        	if(sinkController instanceof SWTControllerInfoDraw){
        		try{
        			((SWTControllerInfoDraw)sinkController).drawOnImage(gc, scale, width, height, false);
        		}catch(RuntimeException err){
        			err.printStackTrace();
        		}
        	}
        	        
        if(sourceControl != null && sourceControl instanceof SWTControllerInfoDraw){
        	try{
        		((SWTControllerInfoDraw)sourceControl).drawOnImage(gc, scale, width, height, true);
        	}catch(RuntimeException err){
    			err.printStackTrace();
    		}	        	
        }
	}

	public void setSource(ImgSource source) {
		
		if(source != guiPipe.getConnectedSource()){			
			
			if(guiPipe.getConnectedSource() != null && sourceControl != null){
				ImagePipeController ctrl = sourceControl;
				sourceControl = null; //don't go round in circles
				ctrl.destroy(); //does the SWT dispose		
			}
			guiPipe.setSource(source);
		}
			
		if(guiPipe.getConnectedSource() != null){
			sourceControl = source.createSourceController(Composite.class, new Object[]{ swtTabFoler, SWT.BORDER_SOLID });
        	if(sourceControl != null){
        		sourceControlComposite = (Composite) sourceControl.getInterfacingObject();
        		sourceControlComposite.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false));
        		swtSourceTab.setControl(sourceControlComposite);
    			swtSourceTab.setText("&1: " + source.toShortString());
	        }
        	//swtSidePaneScrollComp.pack();
        	
        	//Special for FFTProcessor - if the colour scale had not been moved from default, set it to something reasonable
        	if(guiPipe.getConnectedSource() instanceof FFTProcessor && dispPropsPanel.swtColorLogScale.getSelection() == 50){
        		dispPropsPanel.swtColorLogScale.setSelection(6);
        	}
        }
		 
		guiPipe.notifySourceChanged();
		
		//also set the source of all sinks for which we have a controller in our window
		for(ImagePipeController sinkController : sinkControllers.values()){
			ImgSink sink = (ImgSink)sinkController.getPipe();			
			sink.setSource(guiPipe.getConnectedSource());
		}
		
	}


	public CTabFolder getTabFolder() { return swtTabFoler; }
	public CTabItem getSourceTab() { return swtSourceTab; }
	public CTabItem getInfoTab() { return swtInfoTab; }

	public ImgSourceOrSinkImpl guiPipe() { return guiPipe;	}
	
}
