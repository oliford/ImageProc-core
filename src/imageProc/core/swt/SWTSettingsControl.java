package imageProc.core.swt;

import org.eclipse.swt.widgets.Composite;

import imageProc.core.ConfigurableByID;

/** Small control panels for settings saving/loading
 * These control a ConfigurableByID processor
 * 
 * Implementors so far: GMDSSettingsControl and JsonFileSettingsControl
 * 
 * @author oliford
 *
 */
public interface SWTSettingsControl {
	
	public void buildControl(Composite parent, int style, ConfigurableByID proc);

	public Composite getComposite();
	
	public void doUpdate();
}
