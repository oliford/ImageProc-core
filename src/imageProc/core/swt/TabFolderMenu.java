package imageProc.core.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.TabFolder;

import imageProc.core.EventReciever;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;

/** Code for handling the tab folder context menu of ImageWindow */
public class TabFolderMenu {
	
	/** Things for context-menu on tab folder headers */
	private MenuItem tabMenuItemShowAsOther, tabMenuItemStart, tabMenuItemAbort, tabMenuItemRemove, tabMenuItemReload;
	private int lastTabClickX = 0, lastTabClickY = 0;
	private CTabItem tabMenuSelectedTab = null;
	
	private Menu sinkMenu;
	private ShellOps shellOps;
	
	public TabFolderMenu(ShellOps shellOps) {
		this.shellOps = shellOps;
	}
	
	public void build() {

		CTabFolder swtTabFolder = shellOps.imgSinkOps().getTabFolder();
		Menu sinkMenu = new Menu(swtTabFolder);
		swtTabFolder.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tabFolderMouseDownEvent(event); } });
		sinkMenu.addMenuListener(new MenuListener() {		
			@Override public void menuShown(MenuEvent e) { sinkMenu(e, true); }		
			@Override public void menuHidden(MenuEvent e) { sinkMenu(e, false); }
		}); 
        
		
        tabMenuItemShowAsOther = new MenuItem(sinkMenu, SWT.CASCADE);
        tabMenuItemShowAsOther.setText("Show window as source");
        tabMenuItemShowAsOther.setToolTipText("Shows/opens the window in which this source/sink is a sink/source. Only applicable for 'pipes' (things that are both a source and sink)");
        tabMenuItemShowAsOther.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { tabShowEvent(event); }});

        tabMenuItemStart = new MenuItem(sinkMenu, SWT.CASCADE);
        tabMenuItemStart.setText("Start");
        tabMenuItemStart.setToolTipText("Signal start to only this module.");
        tabMenuItemStart.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { tabStartEvent(event); }});
        
        tabMenuItemAbort = new MenuItem(sinkMenu, SWT.CASCADE);
        tabMenuItemAbort.setText("Abort");
        tabMenuItemAbort.setToolTipText("Signal abort to only this module.");
        tabMenuItemAbort.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { tabAbortEvent(event); }});
        
        tabMenuItemRemove = new MenuItem(sinkMenu, SWT.CASCADE);
        tabMenuItemRemove.setText("Remove");
        tabMenuItemRemove.setToolTipText("Remove the sink and it's panel.");
        tabMenuItemRemove.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { tabRemoveEvent(event); }});
        
        tabMenuItemReload = new MenuItem(sinkMenu, SWT.CASCADE);
        tabMenuItemReload.setText("Reload panel");
        tabMenuItemReload.setToolTipText("Remove and reload the panel for this sink in this window. The sink object itself is not reloaded.");
        tabMenuItemReload.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { tabReloadPanelEvent(event); }});
        
        
        swtTabFolder.setMenu(sinkMenu);
        
	}
	

	
	protected void tabFolderMouseDownEvent(Event event) {
		// store clock location for sinkMenu
		lastTabClickX = event.x;
		lastTabClickY = event.y;
	}

	protected void sinkMenu(MenuEvent event, boolean shown) {
		if(!shown)
			return;
		
		//work out which tab item was clicked and store it ready for the menu item listeners
		CTabItem[] t = shellOps.imgSinkOps().getTabFolder().getItems();
		tabMenuSelectedTab = null;
		for(CTabItem ti : t) {
			
			Rectangle r = ti.getBounds();
			
			if(r.contains(lastTabClickX, lastTabClickY)) {
				tabMenuSelectedTab = ti;				
			}
		}
		
		tabMenuItemShowAsOther.setEnabled(false);
		tabMenuItemShowAsOther.setText("Show other window ...");
		tabMenuItemStart.setEnabled(false);
		tabMenuItemAbort.setEnabled(false);
		tabMenuItemRemove.setEnabled(false);
		tabMenuItemReload.setEnabled(false);
		
		if(tabMenuSelectedTab == shellOps.getInfoTab()) {
			//leav all disabled
		}else if(tabMenuSelectedTab == shellOps.getSourceTab()) {
			tabMenuItemReload.setEnabled(true);

			if(shellOps.guiPipe().getConnectedSource() instanceof EventReciever) {
				tabMenuItemStart.setEnabled(true);
				tabMenuItemAbort.setEnabled(true);
			}
			
			if(shellOps.guiPipe().getConnectedSource() instanceof ImgSink) {
				tabMenuItemShowAsOther.setEnabled(true);
				tabMenuItemShowAsOther.setText("Show as sink");
				tabMenuItemShowAsOther.setToolTipText("Shows/creates the window where this pipe is a sink");
			}else {
				tabMenuItemShowAsOther.setText("Show ... (not a sink)");
			}
			
		}else if(tabMenuSelectedTab != null && tabMenuSelectedTab.getData() != null) {
			if(tabMenuSelectedTab.getData() instanceof ImgSink) {
				tabMenuItemRemove.setEnabled(true);
				tabMenuItemReload.setEnabled(true);
			}

			
			if(tabMenuSelectedTab.getData() instanceof EventReciever) {
				tabMenuItemStart.setEnabled(true);
				tabMenuItemAbort.setEnabled(true);
			}
			
			if(tabMenuSelectedTab.getData() instanceof ImgSource) {
				tabMenuItemShowAsOther.setEnabled(true);
				tabMenuItemShowAsOther.setText("Show as source");
				tabMenuItemShowAsOther.setToolTipText("Shows/creates the window where this pipe is a source");
			}
		}
	}
	

	protected void tabReloadPanelEvent(Event event) {
		if(tabMenuSelectedTab == null)
			return;
		
	}

	protected void tabRemoveEvent(Event event) {
		if(tabMenuSelectedTab != null && tabMenuSelectedTab.getData() != null && tabMenuSelectedTab.getData() instanceof ImgSourceOrSinkImpl)
			((ImgSourceOrSinkImpl)tabMenuSelectedTab.getData()).destroy();
	}

	protected void tabStartEvent(Event event) {
		if(tabMenuSelectedTab == null)
			return;
		
		if(tabMenuSelectedTab == shellOps.getSourceTab() && shellOps.guiPipe().getConnectedSource() != null && shellOps.guiPipe().getConnectedSource() instanceof EventReciever) {
			((EventReciever)shellOps.guiPipe().getConnectedSource()).event(EventReciever.Event.GlobalStart);
			
		}else if(tabMenuSelectedTab.getData() != null && tabMenuSelectedTab.getData() instanceof EventReciever) {
			((EventReciever)tabMenuSelectedTab.getData()).event(EventReciever.Event.GlobalStart);				
		}		
	}


	protected void tabAbortEvent(Event event) {
		if(tabMenuSelectedTab == null)
			return;
		
		if(tabMenuSelectedTab == shellOps.getSourceTab() && shellOps.guiPipe().getConnectedSource() != null && shellOps.guiPipe().getConnectedSource() instanceof EventReciever) {
			((EventReciever)shellOps.guiPipe().getConnectedSource()).event(EventReciever.Event.GlobalAbort);
			
		}else if(tabMenuSelectedTab.getData() != null && tabMenuSelectedTab.getData() instanceof EventReciever) {
			((EventReciever)tabMenuSelectedTab.getData()).event(EventReciever.Event.GlobalAbort);				
		}		
	}

	protected void tabShowEvent(Event event) {
		if(tabMenuSelectedTab == null)
			return;
		
		ImgSource windowOfSource = null;
		
		if(tabMenuSelectedTab == shellOps.getSourceTab()) {
			//they click on a source that is a pipe, look for a window attached to it's source 
			ImgSource src = shellOps.guiPipe().getConnectedSource();
			if(!(src instanceof ImgSink))
					return;
			
			src = ((ImgSink)src).getConnectedSource();
			
			windowOfSource = src;
		}
		
		//they clicked on a sink. look for a window with it as a source
		if(tabMenuSelectedTab.getData() != null && tabMenuSelectedTab.getData() instanceof ImgSourceOrSinkImpl) {
			ImgSourceOrSinkImpl pipe = (ImgSourceOrSinkImpl)tabMenuSelectedTab.getData();
			
			if(pipe instanceof ImgSource)
				windowOfSource = (ImgSource)pipe;
		}
		
		if(windowOfSource == null)
			return;
		
		//or clicked on a source, find the sink that is it's window (or all of them)
		boolean found = false;
		for(ImgSink sink : windowOfSource.getConnectedSinks()){
			if(sink instanceof ImagePanel){
				((ImagePanel)sink).bringToFront();
				found = true;
			}
		}
		if(!found){
			shellOps.newWindow(windowOfSource, 0);
		}
		
	}

}
