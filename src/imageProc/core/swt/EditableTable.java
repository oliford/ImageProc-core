package imageProc.core.swt;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import imageProc.proc.softwareBinning.SoftwareROIsConfig.SoftwareROI;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class EditableTable extends Table {

	public enum EditType {
		EditBox,
		ReadOnly,
		/** Just calls modify on click, no edit box*/
		Click, 
	}

	public interface TableModifyListener {
		/** also called (null, -1, null) when nothing changes to generally update the table */
		public void tableModified(TableItem item, int column, String text);
	}
	
	//public Table table;
	public TableEditor tableEditor;
	public TableItem tableItemEditing = null;
	public Text tableEditBox = null;
	
	private EditType[] editType;
	
	private TableModifyListener modifyListener = null;
	
	public EditableTable(Composite parent, int style) {
		super(parent, style);
		
		tableEditor = new TableEditor(this);
		tableEditor.horizontalAlignment = SWT.LEFT;
		tableEditor.grabHorizontal = true;
		this.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		
		// double click arbitrarily in table to repack everything
		this.addListener(SWT.MouseDoubleClick, new Listener() {
			@Override
			public void handleEvent(Event event) {
				modifyListener.tableModified(null, -1, null); //generally 'update' things
				for (int i = 0; i < getColumnCount(); i++)
					getColumn(i).pack();
			}
		});
		
	}
	
	@Override
	protected void checkSubclass() {
		//we're ... fine... I think
		//super.checkSubclass();
	}

	public void setEditTypeAll(EditType type) {
		editType = new EditType[this.getColumnCount()];
		for(int i=0; i < editType.length; i++) {
			editType[i] = type;
		}
	}
	
	public void setEditType(int column, EditType type) {
		if(editType == null) {
			editType = new EditType[this.getColumnCount()];
			
		}else if(editType.length != this.getColumnCount()) {
			editType = Arrays.copyOf(editType, this.getColumnCount());
			
		}
		editType[column] = type;
	}
	
	public EditType getEditType(int column) {
		return editType == null 
				? EditType.EditBox 
				: (editType[column] == null ? EditType.ReadOnly : editType[column]);
	}
	
	/**
	 * Copied from 'Snippet123' Copyright (c) 2000, 2004 IBM Corporation and
	 * others. [ org/eclipse/swt/snippets/Snippet124.java ]
	 */
	private void tableMouseDownEvent(Event event) {
		
		Rectangle clientArea = this.getClientArea();
		Point pt = new Point(clientArea.x + event.x, event.y);
		int index = this.getTopIndex();
		while (index < this.getItemCount()) {
			boolean visible = false;
			final TableItem item = this.getItem(index);
			for (int i = 0; i < this.getColumnCount(); i++) {
				Rectangle rect = item.getBounds(i);
				if (rect.contains(pt)) {
					if(i == 0) //first column never editable, it's always the handle
						return;
					switch(getEditType(i)){
						case ReadOnly: return;
						case Click:
							if(modifyListener != null)
								modifyListener.tableModified(item, i, item.getText(i));
							
							return;
						case EditBox:
							doEdit(clientArea, item, i);
					}
				}
				if (!visible && rect.intersects(clientArea)) {
					visible = true;
				}
			}
			if (!visible)
				return;
			index++;
		}
	}

	private void doEdit(Rectangle clientArea, TableItem item, int column) {

		if (tableEditBox != null) {
			tableEditBox.dispose(); // oops, one left over
		}
		tableEditBox = new Text(this.getParent(), SWT.NONE);
		tableItemEditing = item;
		Listener textListener = new Listener() {
			public void handleEvent(final Event e) {
				switch (e.type) {
				case SWT.FocusOut:
					if(modifyListener != null)
						modifyListener.tableModified(item, column, tableEditBox.getText());
					tableEditBox.dispose();
					tableEditBox = null;
					tableItemEditing = null;
					break;
				case SWT.Traverse:
					switch (e.detail) {
					case SWT.TRAVERSE_RETURN:
						if(modifyListener != null)
							modifyListener.tableModified(item, column, tableEditBox.getText());									
						// FALL THROUGH
					case SWT.TRAVERSE_ESCAPE:
						tableEditBox.dispose();
						tableEditBox = null;
						tableItemEditing = null;
						e.doit = false;
					}
					break;
				}
			}
		};
		tableEditBox.addListener(SWT.FocusOut, textListener);
		tableEditBox.addListener(SWT.Traverse, textListener);
		tableEditor.setEditor(tableEditBox, item, column);
		tableEditBox.setText(item.getText(column));
		tableEditBox.selectAll();
		tableEditBox.setFocus();
		// hacks for SWT4.4 (under linux GTK at least)
		// Textbox won't display if it's a child of the table
		// so we make it a child of the table's parent, but now need
		// to adjust the location
		{
			tableEditBox.moveAbove(this);
			final Point p0 = tableEditBox.getLocation();
			Point p1 = this.getLocation();
			p0.x += p1.x - clientArea.x;
			p0.y += p1.y;// + editBox.getSize().y;
			// p0.x = pt.x + p1.x;
			// p0.y = pt.y + p1.y;
			tableEditBox.setLocation(p0);
			tableEditBox.addListener(SWT.Move, new Listener() {
				@Override
				public void handleEvent(Event event) {
					tableEditBox.setLocation(p0); // TableEditor keeps
												// moving it to
												// relative to the
												// table, so move it
												// back
				}
			});
		}
		return;
	}

	public void setTableModifyListener(TableModifyListener modifyListener) {
		this.modifyListener = modifyListener;
	}

	public boolean isEditing() {
		return tableItemEditing != null;
	}

}
