package imageProc.core.swt;

import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Image;

import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;

/** Things that components have to ask the containing shell to do.
 
 * ... and access across components */
public interface ShellOps {
	
	public void newWindow(ImgSource imgSrc, int imageIndex);

	public void newWindow(ImgSource connectedSource, int selectedSourceIndex, double scaleX, double scaleY, int x, int y);
	
	public void forceActive();

	public void updateTitleAndIcon();

	public InfoPanel infoPanel();

	public DispPropsPanel dispPropsPanel();

	public MainTopBar topBar();

	public ImagePanel imagePanel();

	public ImgSourceOrSinkImpl guiPipe();

	public CTabItem getInfoTab();

	public CTabItem getSourceTab();
	
	public ImageSinkOps imgSinkOps();
	
}
