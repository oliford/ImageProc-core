package imageProc.core.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgProcessor;
import imageProc.core.ImgSourceOrSinkImpl;

/** Base SWT controller (GUI) for ImgProcessor (things that process one set of images.
 * 
 * Modules which produce an output set of images of processing the input set should extend ImgProcPipeSWTController
 * 
 * @author oliford
 *
 */
public abstract class ImageProcessorSWTController implements ImagePipeController{
	protected Group swtGroup;
	private Composite swtProcessorCommonGroup;
	
	protected Label statusLabel;
	protected Button singleImageCheckbox;
	
	protected Button autoUpdateCheckbox;
	protected Button forceUpdateButton;
	protected Spinner nThreadsSpinner;


	protected void addCommonPipeControls(Group parent) {		
		int guiColumns = ((GridLayout)parent.getLayout()).numColumns;
		
		swtProcessorCommonGroup = new Composite(parent, SWT.NONE);
		swtProcessorCommonGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, guiColumns, 1));
		swtProcessorCommonGroup.setLayout(new GridLayout(6, false));
		
		Label lStat = new Label(swtProcessorCommonGroup, SWT.NONE); lStat.setText("Status:");
		statusLabel = new Label(swtProcessorCommonGroup, SWT.NONE);
		statusLabel.setText("init");
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		singleImageCheckbox = new Button(swtProcessorCommonGroup, SWT.CHECK);
		singleImageCheckbox.setText("Single Image");
		singleImageCheckbox.setToolTipText("Processor output only a single image derived from the selected input image, rather than processing the whole image set.");
		singleImageCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { singleImageEvent(event); } });
		singleImageCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		autoUpdateCheckbox = new Button(swtProcessorCommonGroup, SWT.CHECK);
		autoUpdateCheckbox.setText("Auto");
		autoUpdateCheckbox.setToolTipText("Trigger this processor to begin processing whenever a source image or the processor settings are changed");
		autoUpdateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { autoUpdateEvent(event); } });
		autoUpdateCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		ImgProcessor pipe = getPipe();
		int nThreads = (pipe instanceof ImgProcPipeMultithread) ? ((ImgProcPipeMultithread)pipe).getNumThreads() : -1;
				
		Label lNT = new Label(swtProcessorCommonGroup, SWT.NONE); lNT.setText("Threads:");
		nThreadsSpinner = new Spinner(swtProcessorCommonGroup, SWT.NONE);		
		nThreadsSpinner.setValues(nThreads, 1, 100, 0, 1, 10);
		nThreadsSpinner.setToolTipText("Number of threads of multithreaded processing. <= 1 for single thread. Multithreading will process the images in a random order.");
		nThreadsSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { nThreadsSpinnerEvent(event); } });
		nThreadsSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		nThreadsSpinner.setEnabled(nThreads > 0);
				
		forceUpdateButton = new Button(swtProcessorCommonGroup, SWT.PUSH);
		forceUpdateButton.setText("Force Update");
		forceUpdateButton.setToolTipText("Trigger the processor to start processing now.");
		forceUpdateButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceUpdateEvent(event); } });
		forceUpdateButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Composite pipeControlsGroup = addProcPipeControls(swtProcessorCommonGroup);
		pipeControlsGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
	}
	
	/** For ImageProcPipeSWTController to add some specific things */
	protected Composite addProcPipeControls(Composite parent) {
		return new Composite(parent, SWT.None);
	}

	private void autoUpdateEvent(Event e){
		getPipe().setAutoUpdate(autoUpdateCheckbox.getSelection());
	}
	
	private void singleImageEvent(Event e){
		getPipe().setCalcSelectedImageOnly(singleImageCheckbox.getSelection());
		getPipe().calc();
	}
	
	private void nThreadsSpinnerEvent(Event e){
		ImgProcessor pipe = getPipe();
		if(!(pipe instanceof ImgProcPipeMultithread))
			return;
		((ImgProcPipeMultithread)pipe).setNumThreads(nThreadsSpinner.getSelection());
	}

	private void forceUpdateEvent(Event e){
		getPipe().forceCalc();
	}

	@Override
	public abstract ImgProcessor getPipe();

	@Override
	public final void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate(); }
		});
	}
	
	protected void doUpdate() {
		autoUpdateCheckbox.setSelection(getPipe().getAutoUpdate());
		singleImageCheckbox.setSelection(getPipe().isCalcSelectedImageOnly());
		ImageProcUtil.setColorByStatus(getPipe().getStatus(), statusLabel);
		statusLabel.setText(getPipe().getStatus());
		statusLabel.setToolTipText(getPipe().getStatus());

		ImgProcessor pipe = getPipe();
		if(pipe instanceof ImgProcPipeMultithread){
			nThreadsSpinner.setSelection(((ImgProcPipeMultithread)pipe).getNumThreads());
		}
	}
}
