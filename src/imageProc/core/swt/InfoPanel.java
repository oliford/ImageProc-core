package imageProc.core.swt;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import otherSupport.bufferControl.DirectBufferControl;

public class InfoPanel {

	
	Group swtInfoGroup;
	Label swtImageInfoLabel;
	Combo swtSourceCombo;
	Combo swtNewSinkCombo;
	
	Combo preferredTimebaseCombo;
	Spinner offsetMSSpinner;

	Scale alphaBySigma;
	Spinner swtColorMinSpinner;
	Spinner swtColorMaxSpinner;
	Button relativeMinMax;
	Combo swtColorMapCombo;
	
	Group swtImgCfgComposite;	
	Group swtTimeCfgComposite;
	Button swtCopyImageButton;
	Button swtCopySourceButton;
	Button swtCopyWindowButton;
	Button swtAcceptNewButton;
	Button swtGotoChangedCheckbox;
	Button swtSaveImageButton;
	
	Group swtGlobalGroup;
	Button swtMemoryUpdate;
	Button swtForceGC;
	Label swtMemoryUsageLabel; 
	
	Tree pipeTree;
	
	private ShellOps shellOps;
	
	public InfoPanel(ShellOps shellOps) {
		this.shellOps = shellOps;
	}
	
	public void build(Composite parent, int style) {
		
		swtInfoGroup = new Group(parent, SWT.BORDER_SOLID);
        swtInfoGroup.setLayout(new GridLayout(2, false));
        swtInfoGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
        
        swtInfoGroup.setText("Image Information");
        
        new Label(swtInfoGroup, 0).setText("Source:");
        
        swtSourceCombo = new Combo(swtInfoGroup, SWT.DROP_DOWN | SWT.READ_ONLY);        
        swtSourceCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
        swtSourceCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { changeSourceComboEvent(event); } });
                
        new Label(swtInfoGroup, 0).setText("Image:");
        swtImageInfoLabel = new Label(swtInfoGroup, 0);
        swtImageInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        
        new Label(swtInfoGroup, 0).setText("Add Sink:");
        swtNewSinkCombo = new Combo(swtInfoGroup, SWT.DROP_DOWN | SWT.READ_ONLY);        
        swtNewSinkCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        swtNewSinkCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { newSinkComboEvent(event); } });
        
        swtTimeCfgComposite = new Group(parent, SWT.BORDER_SOLID);
        swtTimeCfgComposite.setLayout(new GridLayout(5, false));
        swtTimeCfgComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));        
        swtTimeCfgComposite.setText("Timebase");

        new Label(swtTimeCfgComposite, SWT.NONE).setText("Preferred:");
        preferredTimebaseCombo = new Combo(swtTimeCfgComposite, SWT.DROP_DOWN);        
        preferredTimebaseCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
        preferredTimebaseCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { timebaseChangeEvent(event); } });
        
        new Label(swtTimeCfgComposite, SWT.NONE).setText("Offset [ms]");
        offsetMSSpinner = new Spinner(swtTimeCfgComposite, 0);
        offsetMSSpinner.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 10000, 100000);
        offsetMSSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        offsetMSSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { timebaseChangeEvent(event); } });
     
        
		swtImgCfgComposite = new Group(parent, SWT.BORDER_SOLID);		
        swtImgCfgComposite.setLayout(new GridLayout(5, false));
        swtImgCfgComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));        
        swtImgCfgComposite.setText("View Properties");


        new Label(swtImgCfgComposite, 0).setText("Color Map:");
        swtColorMapCombo = new Combo(swtImgCfgComposite, SWT.DROP_DOWN | SWT.READ_ONLY);        
        swtColorMapCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        swtColorMapCombo.setItems(new String[] { 
        		"jet (Blue-Green-Red)", 
        		"Grayscale", 
        		"Blue-White-Red (Diverging)",
        		"Inferno",
        		"Viridis",
        		"Plasma",
        		"nipy_spectral",
        		"Pink-White-Green (Diverging)",
        		"Purple-White-Green (Diverging)",
        		"Brown-White-Green (Diverging)",
        		"Purple-White-Orange (Diverging)",   
        		"RedYellow-White-Blue (Diverging)",        		
        		});
        swtColorMapCombo.select(shellOps.imagePanel().getColorMap());
        swtColorMapCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { colourMapEvent(event); } });

        Label lASS = new Label(swtImgCfgComposite, 0); lASS.setText("Alpha:");
        alphaBySigma = new Scale(swtImgCfgComposite, 0);
        alphaBySigma.setMinimum(0);
        alphaBySigma.setMaximum(100);
        alphaBySigma.setIncrement(1);
        alphaBySigma.setSelection(0);
        alphaBySigma.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
        alphaBySigma.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { colorScaleEvent(event, false); } });
     
        Label lCMS = new Label(swtImgCfgComposite, 0);
        lCMS.setText("Min:");
        swtColorMinSpinner = new Spinner(swtImgCfgComposite, 0);
        swtColorMinSpinner.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 10000, 100000);
        swtColorMinSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        swtColorMinSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { colorScaleEvent(event, false); } });
     
        Label lCXS = new Label(swtImgCfgComposite, 0);
        lCXS.setText("Max:");
        swtColorMaxSpinner = new Spinner(swtImgCfgComposite, 0);
        swtColorMaxSpinner.setValues(1000000, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 10000, 100000);
        swtColorMaxSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        swtColorMaxSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { colorScaleEvent(event, false); } });
     
        relativeMinMax = new Button(swtImgCfgComposite, SWT.CHECK);
        relativeMinMax.setText("Relative");
        relativeMinMax.setToolTipText("Min/max color are relative to min/val (0-1000)");
        relativeMinMax.setSelection(true);
        relativeMinMax.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        relativeMinMax.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { colorScaleEvent(event, false); } });

        Label lCP = new Label(swtImgCfgComposite, 0);
        lCP.setText("Copy:");
        lCP.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
        
        swtCopySourceButton = new Button(swtImgCfgComposite, 0);
        swtCopySourceButton.setText("Source");
        swtCopySourceButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { copySourceButtonEvent(event);	}});
        swtCopySourceButton.setToolTipText("Make a new copy of the image source, at the same image index (Which may or may not be the same image instance).");
        swtCopySourceButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1 ,1));
       
        swtCopyWindowButton = new Button(swtImgCfgComposite, 0);
        swtCopyWindowButton.setText("Window");
        swtCopyWindowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { copyWindowButtonEvent(event);	}});
        swtCopyWindowButton.setToolTipText("Make a copy of this window, viewing the same source");
        swtCopyWindowButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2 ,1));
        
        swtSaveImageButton = new Button(swtImgCfgComposite, 0);
        swtSaveImageButton.setText("Save Image");
        swtSaveImageButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveImageButtonEvent(event);	}});
        swtSaveImageButton.setToolTipText("Saves image to /tmp/imageProcImage.png");
        swtSaveImageButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1 ,1));
       
        Label lMT = new Label(swtImgCfgComposite, 0); lMT.setText("Switch to:");
        swtAcceptNewButton = new Button(swtImgCfgComposite, SWT.CHECK); 
        swtAcceptNewButton.setText("New images");
        swtAcceptNewButton.setToolTipText("Switches to images that are added to the source (somewhat obsolete).");
        swtAcceptNewButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        
        swtGotoChangedCheckbox = new Button(swtImgCfgComposite, SWT.CHECK); 
        swtGotoChangedCheckbox.setText("Changed Images ('live' view mode)");
        swtGotoChangedCheckbox.setToolTipText("Switches to images that last changed.");
        swtGotoChangedCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
        swtGotoChangedCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { gotoChangedCheckboxEvent(event);	}});
        
        
        swtGlobalGroup = new Group(parent, SWT.BORDER_SOLID);
        swtGlobalGroup.setLayout(new GridLayout(4, false));
        swtGlobalGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        swtGlobalGroup.setText("Global Information");
       
        new Label(swtGlobalGroup, 0).setText("Memory Usage:");
        swtMemoryUsageLabel = new Label(swtGlobalGroup, 0);
        swtMemoryUsageLabel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        swtMemoryUsageLabel.setText("? / ?\n? / ?\n?");
        
        swtMemoryUpdate = new Button(swtGlobalGroup, SWT.PUSH);
        swtMemoryUpdate.setText("Update");
        swtMemoryUpdate.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { updateButtonEvent(event);	}});
        
        swtForceGC = new Button(swtGlobalGroup, SWT.PUSH);
        swtForceGC.setText("Force GC");
        swtForceGC.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceGCButtonEvent(event);	}});
        
        pipeTree = new Tree(swtGlobalGroup, SWT.NONE);
        pipeTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
        pipeTree.addMouseListener(new MouseListener() {			
				@Override public void mouseUp(MouseEvent event) { }			
				@Override public void mouseDown(MouseEvent event) { treeMouseDownEvent(event); }			
				@Override public void mouseDoubleClick(MouseEvent event) { treeDoubleClickEvent(event);  }
			});
        
        Menu popupMenu = new Menu(pipeTree);
        
        MenuItem attachWinItem = new MenuItem(popupMenu, SWT.CASCADE);
        attachWinItem.setText("Attach Window");
        attachWinItem.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { menuAttachWinItemEvent(event); }});
         
        MenuItem refreshItem = new MenuItem(popupMenu, SWT.NONE);
        refreshItem.setText("Refresh");
        refreshItem.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { menuRefreshItemEvent(event); }});
                
        MenuItem destroyItem = new MenuItem(popupMenu, SWT.NONE);
        destroyItem.setText("Destroy");
        destroyItem.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { menuDeleteItemEvent(event); }});
         
        MenuItem moveUpItem = new MenuItem(popupMenu, SWT.NONE);
        moveUpItem.setText("Move up");
        moveUpItem.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { menuMoveItemEvent(event, true); }});
        
        MenuItem moveDownItem = new MenuItem(popupMenu, SWT.NONE);
        moveDownItem.setText("Move down");
        moveDownItem.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { menuMoveItemEvent(event, false); }});
        
        pipeTree.setMenu(popupMenu);
        //pipeTree.pack();

        repopulatePipeTree();
        
	}

	protected void timebaseChangeEvent(Event event) {		
		ImagePanel imgWin = shellOps.imagePanel();
		imgWin.getConnectedSource().setSeriesMetaData("/timebase/preferred", preferredTimebaseCombo.getText(), false);
		imgWin.getConnectedSource().setSeriesMetaData("/timebase/offsetMS", offsetMSSpinner.getSelection() / 1000.0, false);
		updateGUI();
	}

	protected void gotoChangedCheckboxEvent(Event event) {
		shellOps.imagePanel().setGotoChanged(swtGotoChangedCheckbox.getSelection());
	}
	
	public void updateGUI(){
		swtGotoChangedCheckbox.setSelection(shellOps.imagePanel().getGotoChanged());
		
		if(!preferredTimebaseCombo.isFocusControl()) {
			String current = preferredTimebaseCombo.getText();
			ArrayList<String> timebases = ImageProcUtil.getPossibleTimebases(shellOps.imagePanel().getConnectedSource());
			preferredTimebaseCombo.setItems(timebases.toArray(new String[timebases.size()]));
			preferredTimebaseCombo.setText(current);
		}
	}

	private void menuAttachWinItemEvent(Event event) {
		TreeItem item = pipeTree.getSelection()[0];
		Object o = item.getData();
		if(o instanceof ImgSource){
			shellOps.newWindow((ImgSource)o, 0);			
		}
	}
	
	private void menuDeleteItemEvent(Event event) {
		TreeItem item = pipeTree.getSelection()[0];
		Object o = item.getData();
		if(o instanceof ImgSourceOrSinkImpl){
			((ImgSourceOrSinkImpl)o).destroy();
		}
		repopulatePipeTree();
	}

	private void menuRefreshItemEvent(Event event) {
		repopulatePipeTree();
	}
	
	private void menuMoveItemEvent(Event event, boolean moveUp){
		TreeItem item = pipeTree.getSelection()[0];
		
		ImgSource src = shellOps.imagePanel().getConnectedSource();
		if(src == null)
			return;
		
		ImgSink selectedSink = (ImgSink)item.getData();
		
		List<ImgSink> sinkList = src.getConnectedSinks();
		
		if(!sinkList.contains(selectedSink)){
			System.err.println("WARNING: Can't move sink '"+selectedSink+"', because it's not in the sink list of '"+src+"'");
			return;
		}
		
		int idx = sinkList.indexOf(selectedSink);
		
		sinkList.remove(selectedSink);
		
		if(moveUp)
			idx--;
		else 
			idx++;
		
		sinkList.add(idx, selectedSink);
		
		repopulatePipeTree();
	}
	
	private void forceGCButtonEvent(Event event){
        
		System.gc();
		updateButtonEvent(null);
	}
	
	private void updateButtonEvent(Event event){
		DecimalFormat fmt = new DecimalFormat("##.#");
		
		String cachePath = SettingsManager.defaultGlobal().getPathProperty("minerva.cache.path", "/tmp/minerva");
		String buffPath = SettingsManager.defaultGlobal().getPathProperty("minerva.cache.mappedMemoryPath", cachePath + "/mappedMemoryBuffers");
		File f = new File(buffPath);
		long mappedDiskFree = f.getUsableSpace();
		
		Runtime rt = Runtime.getRuntime();
		long usedMem = rt.totalMemory() - rt.freeMemory();
		
		swtMemoryUsageLabel.setText(fmt.format(DirectBufferControl.getUsedMemory()/1e6) + " / " + fmt.format(DirectBufferControl.getMaxMemory()/1e6) + " MB direct memory.\n"
						+ fmt.format(usedMem/1e6) + " / " + fmt.format(rt.totalMemory()/1e6) + " / " + fmt.format(rt.maxMemory()/1e6) + " MB java heap.\n"
						+ fmt.format(mappedDiskFree/1e6) + " MB disk free for mapped memory");
		
		
		repopulatePipeTree();
	}
	
	private void treeMouseDownEvent(MouseEvent event) {
		
	}
		
	private void treeDoubleClickEvent(MouseEvent event) {
		if(event.button > 1){
			repopulatePipeTree();
			return;
		}
		
		TreeItem item = pipeTree.getSelection()[0];
		
		Object o = item.getData();
		if(o instanceof ImagePanel){ //if clicked on window itself
			((ImagePanel)o).bringToFront();
		}else if(o instanceof ImgSource){
			//or clicked on a source, find the sink that is it's window (or all of them)
			boolean found = false;
			for(ImgSink sink : ((ImgSource)o).getConnectedSinks()){
				if(sink instanceof ImagePanel){
					((ImagePanel)sink).bringToFront();
					found = true;
				}
			}
			if(!found){
				shellOps.newWindow((ImgSource)o, 0);
			}
		}
	}
	
	private void changeSourceComboEvent(Event event){
		String srcShortName = swtSourceCombo.getItem(swtSourceCombo.getSelectionIndex());
		
		LinkedList<ImgSource> srcList = ImageProcUtil.getSourceInstances();
        for(ImgSource src : srcList){
        	if(src != null && srcShortName.equals(src.toShortString())){
        		shellOps.imagePanel().setSource(src);	
        	}
        }
	}

	private void copySourceButtonEvent(Event event){
		ImagePanel imgWin = shellOps.imagePanel();
		if(imgWin.getConnectedSource() == null) return;

		Point p0 = imgWin.getScrollOrigin();
		ImgSource newSource = imgWin.getConnectedSource().clone();
		shellOps.newWindow(newSource, imgWin.getSelectedSourceIndex(), imgWin.getScaleX(), imgWin.getScaleY(), p0.x, p0.y);		
	}

	private void copyWindowButtonEvent(Event event){
		ImagePanel imgWin = shellOps.imagePanel();
		Point p0 = imgWin.getScrollOrigin();
		shellOps.newWindow(shellOps.imagePanel().getConnectedSource(), imgWin.getSelectedSourceIndex(), imgWin.getScaleX(), imgWin.getScaleY(), p0.x, p0.y);
	}
	
	private void saveImageButtonEvent(Event event){
		Image swtImage = shellOps.imagePanel().getSWTImage();
		if(swtImage == null){
			System.err.println("No image to save");
			return;
		}
		org.eclipse.swt.graphics.ImageLoader loader = new org.eclipse.swt.graphics.ImageLoader();
		loader.data = new ImageData[] { swtImage.getImageData() };
		loader.save("/tmp/imageProcImage.png", SWT.IMAGE_PNG);	       
	}

	private void colorScaleEvent(Event event, boolean centreOnMouse){
		shellOps.imagePanel().colorScaleEvent(event, centreOnMouse);		
	}
	
	private void colourMapEvent(Event event) {
		shellOps.imagePanel().colorMapEvent(event);
	}
	
	private void newSinkComboEvent(Event event){
		ImagePanel imgWin = shellOps.imagePanel();
		String sinkTypeName = swtNewSinkCombo.getItem(swtNewSinkCombo.getSelectionIndex());
		
		if(sinkTypeName != null){
			List<Class> sinkTypes = ImageProcUtil.getSinkClasses();
			for(Class sinkType : sinkTypes){
	        	if(sinkTypeName.equals(sinkType.getSimpleName())){
	        		
					try {
						ImgSink sink = (ImgSink)sinkType.newInstance();
						sink.setSource(imgWin.getConnectedSource());
						sink.setSelectedSourceIndex(imgWin.getSelectedSourceIndex());
						shellOps.imgSinkOps().addSinkController(sink);
						
						//if the sink is also a source (i.e. a full pipe), create a new image window with
						// it selected
						if(sink instanceof ImgSource)
							shellOps.newWindow((ImgSource)sink, imgWin.getSelectedSourceIndex());

					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					break;
	        	}
	        }
		}
        
		shellOps.imagePanel().updateInfoPanel(); //refresh the list (and hence clear the selection) 
		repopulatePipeTree();
	}

	void repopulatePipeTree() {
		pipeTree.setRedraw(false);
		pipeTree.removeAll();
		
		ImgSource src = shellOps.imagePanel().getConnectedSource();
		if(src == null)
			return;
			
		//find root source
		while(src instanceof ImgSink && ((ImgSink)src).getConnectedSource() != null)
			src = ((ImgSink)src).getConnectedSource();
		
		TreeItem rootSourceItem = new TreeItem(pipeTree, SWT.NONE);
		rootSourceItem.setText(src.toString());
		rootSourceItem.setData(src);
		
		populateTree(rootSourceItem, src);
		
		expandTree(rootSourceItem);
		pipeTree.setRedraw(true);
		pipeTree.redraw();
	}
	
	private void populateTree(TreeItem srcItem, ImgSource src){
		int i=0;
		for(ImgSink sink : src.getConnectedSinks()){
			
			TreeItem nextItem = new TreeItem(srcItem, SWT.NONE);
			nextItem.setText(i + ": " + sink.toString());
			nextItem.setData(sink);
			if(sink == shellOps.imagePanel()){
				//nextItem.setBackground(swtInfoGroup.getDisplay().getSystemColor(SWT.COLOR_GRAY));
				nextItem.setForeground(swtInfoGroup.getDisplay().getSystemColor(SWT.COLOR_BLUE));
			}else if(sink instanceof ImagePanel){
				nextItem.setForeground(swtInfoGroup.getDisplay().getSystemColor(SWT.COLOR_DARK_BLUE));				
			}			
			if(sink instanceof ImgSource){ //also a source, so need to recurse
				nextItem.setForeground(swtInfoGroup.getDisplay().getSystemColor(SWT.COLOR_DARK_GREEN));	
				populateTree(nextItem, (ImgSource)sink);
			}
			i++;
		}
	}
	
	private void expandTree(TreeItem item0){
		item0.setExpanded(true);
		for(TreeItem item : item0.getItems())
			expandTree(item);		
	}

	public void setImageInfoText(String string) {
		if(swtImageInfoLabel != null)
			swtImageInfoLabel.setText(string);
	}

	public void doUpdate() {
		if(swtInfoGroup == null || swtInfoGroup.isDisposed())
			return;
		
		shellOps.updateTitleAndIcon();
		
		Img image = shellOps.guiPipe().getSelectedImageFromConnectedSource();
		ImgSource connectedSource = shellOps.guiPipe().getConnectedSource();
		int selectedSourceIndex = shellOps.guiPipe().getSelectedSourceIndex();
		
		if(image == null){
			shellOps.infoPanel().setImageInfoText("NULL");
			shellOps.topBar().setTimeText("t = xxxx, max = xxx (No source/image)");
			
		}else{
			
			shellOps.infoPanel().setImageInfoText(image.toString());
			shellOps.infoPanel().swtAcceptNewButton.setEnabled(connectedSource != null); //only works with an image source
	        
	        //if we have some metaData called time
	        double t = Double.NaN;
	        String timeSrc = "No '*/time' metadata";
	        
	        if(connectedSource != null){
	        	Object o = null;
	        	
	        	String preferred = (String)connectedSource.getSeriesMetaData("/timebase/preferred");	        	
	        	if(preferred != null) 
	        		o = connectedSource.getImageMetaData(preferred, selectedSourceIndex);
	        		
	        	if(o == null || !(o instanceof Number)) {
	        		preferred = connectedSource.getCompleteSeriesMetaDataMap().findEndsWith("/time", true);	        	
		        	if(preferred != null) 
		        		o = connectedSource.getImageMetaData(preferred, selectedSourceIndex);
	        	}
	        	
	        	if(o != null && o instanceof Number) {
	        		timeSrc = preferred;
        			t = ((Number)o).doubleValue();
        			if(t > 1_000_000_000L) { 
        				//looks like a nano or ms timestamp, take relative to first for now
        			
        				long tNano = ((Number)o).longValue();
        				o = connectedSource.getImageMetaData(preferred, 0);
        				long tNano0 = ((Number)o).longValue();
        				
        				if(t > 100_000_000_000L)
        					t = ((tNano - tNano0) / 1_000_000L) / 1000.0;
        				else{
        					t = ((tNano - tNano0)) / 1000.0;
        				}
        			}
        			
        			o = connectedSource.getSeriesMetaData("/timebase/offsetMS");
        			if(o != null && o instanceof Number)
        				t -= ((Number)o).doubleValue();
        			
        		
	        	}
	        }
	        shellOps.topBar().setTimeText("t = " + t + " (" + timeSrc + "), max = " + image.getMax());
	        
		}
		
		updateLists(false);
	}
	
	private long lastListUpdate = 0;
	private long listUpdatePeriod = 5000;
	
	public void updateLists(boolean force) {
		//this is particularly heavy, so only do it periodically. Really it should be linked to doing things with those boxes
		if(!force && System.currentTimeMillis() < (lastListUpdate + listUpdatePeriod))
			return;
		
		lastListUpdate = System.currentTimeMillis();
		
		ImgSource connectedSource = shellOps.guiPipe().getConnectedSource();
		
        // Update list of sources
        LinkedList<ImgSource> srcList = ImageProcUtil.getSourceInstances();
        srcList.sort(new Comparator<ImgSource>() {
			@Override
			public int compare(ImgSource o1, ImgSource o2) {
				return o1.toShortString().compareTo(o2.toShortString());
			}
		});
        
        shellOps.infoPanel().swtSourceCombo.setItems(new String[0]);        
        for(ImgSource src : srcList){
        	shellOps.infoPanel().swtSourceCombo.add(src.toShortString());
    		if(src == connectedSource){
    			shellOps.infoPanel().swtSourceCombo.select(shellOps.infoPanel().swtSourceCombo.getItemCount() - 1);
    		}
    	}
        
        // Update list of sink types        
        List<Class> sinkTypes = ImageProcUtil.getSinkClasses();
        
        sinkTypes.sort(new Comparator<Class>() {
			@Override
			public int compare(Class o1, Class o2) {
				return o1.getSimpleName().compareTo(o2.getSimpleName());
			}
		});
        
        shellOps.infoPanel().swtNewSinkCombo.setItems(new String[0]);
        for(Class sinkType : sinkTypes){
        	shellOps.infoPanel().swtNewSinkCombo.add(sinkType.getSimpleName());
        }
	}
	
	public String getShortID(){
		Img image = shellOps.guiPipe().getSelectedImageFromConnectedSource();
		ImgSource src = shellOps.guiPipe().getConnectedSource();
		return 
				((src == null) ? "NoSRC" : src.toShortString()) 
						+ "-" +
				((image == null) ? "NoIMG" : image.toShortString());
	}
	
}
