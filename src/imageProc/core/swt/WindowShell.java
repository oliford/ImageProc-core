package imageProc.core.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import algorithmrepository.Algorithms;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import otherSupport.RandomManager;
import otherSupport.SettingsManager;

/** Outer shell of the main window.
 * 
 * Most of the code is in SplitImgInfoView 
 * 
 * @author oliford
 *
 */
public class WindowShell implements ShellOps {
	private Shell swtShell;
	
	private SplitImgInfoView splitView;
	
	private static final int iconColors[] = new int[]{
			SWT.COLOR_BLUE,
			SWT.COLOR_RED,
			SWT.COLOR_BLACK,
			SWT.COLOR_MAGENTA,
			SWT.COLOR_GRAY,
			SWT.COLOR_DARK_RED,
			SWT.COLOR_DARK_GREEN,
			SWT.COLOR_DARK_BLUE,
			SWT.COLOR_DARK_YELLOW,
	};
	
	protected int iconColor = -1;

	protected String profileName = null;
	
	/** two chars for the icon */
	protected String profileChars = null;
	
	public WindowShell(Display swtDisplay, ImgSource source, int imageIndex) {
		
		splitView = new SplitImgInfoView(this);

		createWindow(swtDisplay);

		splitView.imagePanel().setSource(source);
		splitView.imagePanel().setSelectedSourceIndex(imageIndex);
		splitView.initControllers();
	}
	
	public WindowShell(Display swtDisplay, ImgSource source, int imageIndex, double scaleX, double scaleY, int x0, int y0) {
		splitView = new SplitImgInfoView(this);
		
		createWindow(swtDisplay);

		splitView.imagePanel().setSource(source);
		splitView.imagePanel().setSelectedSourceIndex(imageIndex);
		splitView.initControllers();
		splitView.imagePanel().rescale(scaleX, scaleY, 0, 0, (scaleX != scaleY));
		splitView.imagePanel().setOrigin(x0, y0);
	}
		
	private void createWindow(Display display){

		swtShell = new Shell(display, SWT.RESIZE | SWT.DIALOG_TRIM | SWT.MIN | SWT.MAX);
		swtShell.setText("ImageProc [init]");
		
        swtShell.setLayout(new GridLayout(1, false));
       
        splitView.build(swtShell);        
      
       // swtSidePane.setSize(swtSidePane.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        swtShell.pack();        
        swtShell.open();
        swtShell.setSize(
        		Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.gui.windowWidth", "1200")),
        		Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.gui.windowHeight", "800")));
        
        swtShell.setMaximized(Boolean.parseBoolean(SettingsManager.defaultGlobal().getProperty("imageProc.gui.maximized", "false")));
                
	}

	@Override
	public void forceActive() { 
		if(swtShell != null && !swtShell.isDisposed())
			swtShell.forceActive();
	}

	@Override
	public void newWindow(ImgSource imgSrc, int imageIndex) {
		new WindowShell(swtShell.getDisplay(), imgSrc, imageIndex);		
	}

	@Override
	public void newWindow(ImgSource connectedSource, int selectedSourceIndex, double scaleX, double scaleY, int x, int y) {
		new WindowShell(swtShell.getDisplay(), connectedSource, selectedSourceIndex, scaleX, scaleY, x, y);
	}

	public void rescale(double newScaleX, double newScaleY, int centreX, int centreY, boolean breakAspect) {
		splitView.imagePanel().rescale(newScaleY, newScaleY, centreX, centreY, breakAspect);
	}

	@Override
	public InfoPanel infoPanel() { return splitView.infoPanel(); }
	@Override
	public DispPropsPanel dispPropsPanel() { return splitView.dispPropsPanel();	}

	@Override
	public MainTopBar topBar() { return splitView.topBar(); }

	@Override
	public ImagePanel imagePanel() { return splitView.imagePanel(); }

	@Override
	public ImgSourceOrSinkImpl guiPipe() { return splitView.guiPipe(); }

	@Override
	public CTabItem getInfoTab() { return splitView.getInfoTab(); }

	@Override
	public CTabItem getSourceTab() { return splitView.getSourceTab(); }

	@Override
	public ImageSinkOps imgSinkOps() { return splitView; }

	@Override
	public void updateTitleAndIcon() {
		if(swtShell.isDisposed())
			return;
		
		Img image = guiPipe().getSelectedImageFromConnectedSource();
		ImgSource connectedSource = guiPipe().getConnectedSource();
		int selectedSourceIndex = guiPipe().getSelectedSourceIndex();
		
		//go back and find the ID of the root source
		ImgSource rootSource = connectedSource;		
		while(rootSource instanceof ImgSink && ((ImgSink)rootSource).getConnectedSource() != null)
			rootSource = ((ImgSink)rootSource).getConnectedSource();
		
		if(iconColor < 0) {
			//make a random colour by default
			iconColor = iconColors[(int)RandomManager.instance().nextUniform(0, iconColors.length)];
		}
		
		//if the root source already has a window (that's not this one), copy that one's info
		if(rootSource != null) {
			for(ImgSink sink : rootSource.getConnectedSinks()){
				if(sink instanceof ImagePanel && sink != imagePanel()) {
					ShellOps theirOps = ((ImagePanel)sink).getShellOps();
					if(theirOps != this && (theirOps instanceof WindowShell)) {
						this.iconColor = ((WindowShell)theirOps).iconColor;
						this.profileName = ((WindowShell)theirOps).profileName;
						this.profileChars = ((WindowShell)theirOps).profileChars;
					}
				}
			}
		}
				
		
		String appName = "ImageProc";
		if(profileName != null)
			appName = profileName;
		
		if(image == null){
			swtShell.setText(appName + " " + infoPanel().getShortID());
			
		}else{
			
			
			String pulseStr = "???";
			if(connectedSource != null){
				Object o = ImageProcUtil.singleObjectMeta(connectedSource, "aug/pulse");;
				int augShot = (o instanceof Integer) ? (Integer)o : -1;

				String w7xProgID = (String)ImageProcUtil.singleObjectMeta(connectedSource, "w7x/programID");				
				pulseStr = "{ gmds/" + GMDSUtil.getMetaDatabaseID(connectedSource) +
						", aug/" + augShot + ", w7x/" + w7xProgID + "}";			
			}
			
			swtShell.setText(appName + " " + infoPanel().getShortID() + pulseStr);
	     
		}
		
		//set the window icon to the first letter of the source class (for Alt-Tab help)

	
		Image icon = new Image(swtShell.getDisplay(), 32, 32);
       
		GC gc = new GC(icon);
		
		String str1 =  null;
		if(profileChars != null) {
			str1 = profileChars.substring(0, Math.min(profileChars.length(), 3));
			
		}else if(profileName != null) {
			if(profileName.matches("[A-Z][A-Z][A-Z]-.*")) {
				str1 = profileName.substring(4, 6);
			}else {
				str1 = profileName.substring(0, 2);
			}
			
		}else {
			str1 = "ImP";
		}
		
		String str2 = (connectedSource == null) ? "--" : connectedSource.getClass().getSimpleName();
		if(str2.length() > 3)
			str2 = str2.substring(0, 3);
	    
	    Font font = new Font(swtShell.getDisplay(), "Arial", 12, SWT.BOLD); 
		gc.setForeground(swtShell.getDisplay().getSystemColor(iconColor)); 
		gc.setFont(font); 
		gc.drawText(str1, 0, -2);
		
		font = new Font(swtShell.getDisplay(), "Arial", 10, SWT.ITALIC); 
		gc.setForeground(swtShell.getDisplay().getSystemColor(iconColor)); 
		gc.setFont(font);
		gc.drawText(str2, 2, 16); 
		font.dispose();
			
		swtShell.setImage(icon);
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName; 
		updateTitleAndIcon();
	}

	/** two chars for the icon */
	public void setProfileChars(String profileChars) {
		this.profileChars = profileChars; 
		updateTitleAndIcon();
	}
}
