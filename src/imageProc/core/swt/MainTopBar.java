package imageProc.core.swt;

import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.EventReciever;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;

public class MainTopBar {
	private Spinner swtImageIndexSpinner;
	private Button swtGotoChangedCheckbox;
	private Spinner swtFieldSpinner;
	private Label swtTimeText;
	private Button commandStartButton;	
	private Button commandAbortButton;
	private Button refreshButton;
	private Button wriggleButton;
	
	private Composite swtTopBar;
	private SplitImgInfoView splitView;
	
	public MainTopBar(SplitImgInfoView splitView) {
		this.splitView = splitView;
	}
	
	public void build(Composite parent) {
		swtTopBar = new Composite(parent, SWT.NONE);
        GridLayout gl = new GridLayout(10, false);
        gl.marginLeft = 0; gl.marginRight = 0; gl.marginHeight = 0; gl.marginWidth = 0;
        gl.marginTop = 0; gl.marginBottom = 0;
        swtTopBar.setLayout(gl);
        swtTopBar.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
        Label lII = new Label(swtTopBar, 0); lII.setText("&Index:");
        
        swtImageIndexSpinner = new Spinner(swtTopBar, 0);
        swtImageIndexSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
       	swtImageIndexSpinner.setValues(-2, -2, -1, 0, 1, 10);
        swtImageIndexSpinner.setEnabled(true);
        swtImageIndexSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { splitView.imagePanel().imageIndexChanged(); } });
        swtImageIndexSpinner.setToolTipText("Image index in series, usually time series");
               
        swtGotoChangedCheckbox = new Button(swtTopBar, SWT.CHECK); 
        swtGotoChangedCheckbox.setText("&Live");
        swtGotoChangedCheckbox.setToolTipText("Switches to images that last changed, giving effective 'live' view.");
        swtGotoChangedCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, false, false, 1, 1));
        swtGotoChangedCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { splitView.imagePanel().setGotoChanged(swtGotoChangedCheckbox.getSelection());	}});
      
        
        swtTimeText = new Label(swtTopBar, SWT.NONE);
        swtTimeText.setText("t = ??.??????? (????????????), max = ?????????");
        swtTimeText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        swtTimeText.setToolTipText("Time of selected image from timebase metadata selected in info panel.");
        
        Label lF = new Label(swtTopBar, 0); lF.setText("&Field:");
        swtFieldSpinner = new Spinner(swtTopBar, 0);
        swtFieldSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        swtFieldSpinner.setValues(0, 0, 100, 0, 1, 1);
        swtFieldSpinner.setEnabled(true);
        swtFieldSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { splitView.imagePanel().imageIndexChanged(); } });
        swtFieldSpinner.setToolTipText("Image field to display. Often field 1 is uncertainty. From FFT {0=absolute, 1=real, 2=imaginary}.");
               	
        commandStartButton = new Button(swtTopBar, SWT.PUSH);
        commandStartButton.setText("&Start All");
        commandStartButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        commandStartButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { commandStartButtonEvent(event); } });
        commandStartButton.setToolTipText("Send all connected modules in the source/sink tree a general software start trigger");
        
        commandAbortButton = new Button(swtTopBar, SWT.PUSH);
        commandAbortButton.setText("&Abort All");
        commandAbortButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        commandAbortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { commandAbortButtonEvent(event); } });
        commandAbortButton.setToolTipText("Stop all aquisition, processing and control. Sends all connected modules in the source/sink tree a general software abort trigger");
        //commandAbortButton
        
        Menu abortMenu = new Menu(commandAbortButton);
        
        MenuItem interruptAllItem = new MenuItem(abortMenu, SWT.CASCADE);
        interruptAllItem.setText("Interrupt all threads");
        interruptAllItem.setToolTipText("Interrupt all threads in the JVM. This can help get things unstuck, but can also cause background monitoring threads to get lost.");
        interruptAllItem.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortMenuInterruptThreadsEvent(event); }});

        MenuItem abortModuleItem = new MenuItem(abortMenu, SWT.CASCADE);
        abortModuleItem.setText("Abort module");
        abortModuleItem.setToolTipText("Signal abort to only the source module in this window.");
        abortModuleItem.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortModuleEvent(event); }});
        
        commandAbortButton.setMenu(abortMenu);

        refreshButton = new Button(swtTopBar, SWT.PUSH);
        refreshButton.setText("%");
        refreshButton.setLayoutData(new GridData(SWT.END, SWT.TOP, false, false, 1, 1));
        refreshButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refresh(); } });
        refreshButton.setToolTipText("Refresh all GUI elements - Issue a controller update to all controllers of all sources and sinks");

        wriggleButton = new Button(swtTopBar, SWT.PUSH);
        wriggleButton.setText("~");
        wriggleButton.setLayoutData(new GridData(SWT.END, SWT.TOP, false, false, 1, 1));
        wriggleButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { wriggle(); } });
        wriggleButton.setToolTipText("Wriggle the window to sort out GUI bugs");
       
	}

	protected void refresh() {
		ImageProcUtil.refreshAll(splitView.imagePanel().getConnectedSource());
	}

	/**Adjusts and readjusts the main sash form weights (position) to force all the GUI to sort itself
	 * out. */
	public void wriggle() {
		ImageProcUtil.wriggle(swtTopBar);
		
		splitView.imagePanel().updateInfoPanel();
	}

	private void commandStartButtonEvent(Event event){
		ImgSource src = splitView.imagePanel().getConnectedSource();
		while(src instanceof ImgSink)
			src = ((ImgSink)src).getConnectedSource();		
		src.broadcastEvent(EventReciever.Event.GlobalStart);		
	}

	private void commandAbortButtonEvent(Event event){
		ImgSource src = splitView.imagePanel().getConnectedSource();
		while(src instanceof ImgSink)
			src = ((ImgSink)src).getConnectedSource();		
		src.broadcastEvent(EventReciever.Event.GlobalAbort);
	}
	
	protected void abortModuleEvent(Event event) {
		if(splitView.imagePanel().getConnectedSource() instanceof EventReciever) {
			((EventReciever)splitView.imagePanel().getConnectedSource()).event(EventReciever.Event.GlobalAbort);
		}
	}

	protected void abortMenuInterruptThreadsEvent(Event event) {
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
		for(Thread t : threadSet) {
			t.interrupt();
		}
	}

	
	private Composite getComposite() { return swtTopBar; }

	public void setTimeText(String string) {
		swtTimeText.setText(string);
	}

	public Spinner imageIndexSpinner() { return swtImageIndexSpinner; }

	public Spinner fieldSpinner() { return swtFieldSpinner; }

	public Button gotoChangedCheckbox() { return swtGotoChangedCheckbox; }
	
}
