package imageProc.core;

import org.eclipse.swt.custom.CTabItem;

/** For use when SWT Pipe controllers need functions specific to SWT.
 * Not all SWT Pipe controllers will implement this. Only when they need it.
 * @author oliford
 *
 */
public interface ImagePipeSWTController {

	/** For SWT GUI controllers, some sink controllers change their names after construction and need access to their own
	 * tab. This is a bit of a hack but I see no other way of doing it  */
	public void setTabItem(CTabItem sinkTab);
}
