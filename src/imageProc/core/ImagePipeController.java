package imageProc.core;

import org.eclipse.swt.custom.CTabItem;

/** Anything which interrogates and/or modifies an image pipe
 * 
 * This is basically either a GUI element, or a CLI module or something
 * 
 * @author oliford
 *
 */
public interface ImagePipeController extends GeneralController {

	/** Return the controlled pipe */
	public ImgSourceOrSinkImpl getPipe();

	@Override
	public default Object getControlledObject() {		// TODO Auto-generated method stub
		return getPipe();
	}
}
