package imageProc.metaview;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import otherSupport.RandomManager;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.gmds.GMDSSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.graph.JFreeChartGraph;
import imageProc.graph.Series;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class MetaDataManagerSWTController implements ImagePipeController {
	public static final String colNames[] = { "ID", "Persistent?", "Time Series", "Datatype", "Level" };
	
	public static final int COL_ID = 0;
	public static final int COL_PERSISTENT = 1;
	public static final int COL_TIME_SERIES = 2;
	public static final int COL_DATATYPE = 3;
	public static final int COL_LEVEL = 4;
	
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.######");

	private MetaDataManager proc;

	protected Group swtGroup;
	private SashForm swtSashForm;
	private Composite swtTopComp;
	private Composite swtInfoComp;

	private Label statusLabel;
	private Combo whenToApplyCombo;
	private Button overwriteCheckbox;
	private Button addNowButton;

	private Button showPersistentMetaRadio;  
	private Button showSourceMetaRadio;  
	
	private Table table;
	
	private Text nameTextbox;
	private Combo datatypeTextbox;
	private Button isPersistentCheckbox;	
	private Button isTimeSeriesCheckbox;
	private Button refreshButton;
	private Text dataTextbox;
	
	private Button modifyButton;
	private Button addButton;

	private SWTSettingsControl settingsCtrl;

	private static String[] typeNames = { 
		"String", "Integer", "Short", "Long", "Double", "Float",
		"[Ljava.lang.String; (string array)", "[I (integer array)", "[S (short array)", "[J (long array)", "[D (double array)", "[F (float array)",		
	};
	
	public MetaDataManagerSWTController(Composite parent, int style, MetaDataManager proc) {
		this.proc = proc;

		swtGroup = new Group(parent, style);
		swtGroup.setText("MetaDataManager Control (" + proc.toShortString() + ")");
		swtGroup.setLayout(new FillLayout());
		swtGroup.addListener(SWT.Dispose, new Listener() {
			@Override
			public void handleEvent(Event event) {
				destroy();
			}
		});

		swtSashForm = new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		swtSashForm.setLayout(new FillLayout());

		swtTopComp = new Composite(swtSashForm, SWT.BORDER);
		swtTopComp.setLayout(new GridLayout(6, false));

		Label lStat = new Label(swtTopComp, SWT.NONE);
		lStat.setText("Status:");
		statusLabel = new Label(swtTopComp, SWT.NONE);
		statusLabel.setText("init");
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 7, 1));

		
		Label lWA = new Label(swtTopComp, SWT.NONE); lWA.setText("Add persistent:");
		whenToApplyCombo = new Combo(swtTopComp, SWT.READ_ONLY);
		whenToApplyCombo.setItems(new String[] { "Never", "On change" });
		for(imageProc.core.EventReciever.Event event : imageProc.core.EventReciever.Event.values())
			whenToApplyCombo.add(event.toString());
		whenToApplyCombo.setToolTipText("When to automatically add the persistent metadata");
		whenToApplyCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
		whenToApplyCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		whenToApplyCombo.setText(imageProc.core.EventReciever.Event.Process.toString());
		
		addNowButton = new Button(swtTopComp, SWT.PUSH);
		addNowButton.setText("Add persistent now");
		addNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceUpdateEvent(event);	}});
		addNowButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		
		Label lMT = new Label(swtTopComp, SWT.NONE); lMT.setText("Show:");
		showPersistentMetaRadio = new Button(swtTopComp, SWT.RADIO);
		showPersistentMetaRadio.setText("Persistent");
		showPersistentMetaRadio.setToolTipText("Show only metadata which is persistent. i.e. entries that are added by this module.");
		showPersistentMetaRadio.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { generalControllerUpdate(); } });
		showPersistentMetaRadio.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		showPersistentMetaRadio.setSelection(false);
		
		showSourceMetaRadio = new Button(swtTopComp, SWT.RADIO);
		showSourceMetaRadio.setText("All");
		showSourceMetaRadio.setToolTipText("Show all metadata. This includes metadata add by this or other modules.");
		showSourceMetaRadio.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { generalControllerUpdate(); } });
		showSourceMetaRadio.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		showSourceMetaRadio.setSelection(true);;
		
		Label lB = new Label(swtTopComp, SWT.NONE); lB.setText("");		
		overwriteCheckbox = new Button(swtTopComp, SWT.CHECK);
		overwriteCheckbox.setText("Overwrite");
		overwriteCheckbox.setToolTipText("Add all persistent data entires, even if they alreeady exist. i.e. overwrite existing metadata with persistent items.");
		overwriteCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
		overwriteCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		overwriteCheckbox.setSelection(false);
		
		refreshButton = new Button(swtTopComp, SWT.PUSH);
		refreshButton.setText("Refresh");
		refreshButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refreshButtonEvent(event);	}});
		refreshButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		table = new Table(swtTopComp, SWT.BORDER | SWT.MULTI);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn cols[] = new TableColumn[colNames.length];
		for (int i = 0; i < colNames.length; i++) {
			cols[i] = new TableColumn(table, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]);
		}
		
		dataToGUI();

		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		table.addListener(SWT.MouseDoubleClick, new Listener() { 
			@Override
			public void handleEvent(Event event) {
				for (int i = 0; i < colNames.length; i++)
					cols[i].pack();
			}
		});

		table.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableItemSelected(); } });
		table.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { tableItemSelected(); } });
				
		swtInfoComp = new Composite(swtSashForm, SWT.BORDER);
		swtInfoComp.setLayout(new GridLayout(6, false));
		

		Label lN = new Label(swtInfoComp, SWT.NONE); lN.setText("ID:");
		nameTextbox = new Text(swtInfoComp, SWT.NONE);
		nameTextbox.setText("");
		///nameTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ???(event);	}});
		nameTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		Label lDT = new Label(swtInfoComp, SWT.NONE); lDT.setText("Datatype:");
		datatypeTextbox = new Combo(swtInfoComp, SWT.NONE);
		datatypeTextbox.setItems(typeNames);
		///nameTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ???(event);	}});
		datatypeTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		
		Label lBLK = new Label(swtInfoComp, SWT.NONE); lBLK.setText("");
		
		isPersistentCheckbox = new Button(swtInfoComp, SWT.CHECK);
		isPersistentCheckbox.setText("Persistent");
		isPersistentCheckbox.setToolTipText("True for meta-data created here, Otherwise it may be created somewhere else and will be overwritten or lost when the source updates.");
		//isPersistent.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ???(event);	}});
		isPersistentCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		isTimeSeriesCheckbox = new Button(swtInfoComp, SWT.CHECK);
		isTimeSeriesCheckbox.setText("Time series");
		isTimeSeriesCheckbox.setToolTipText("If true, the metadata entry is an array that is treated as a time series. It should be the same length as the number of images.");
		//isTimeSeries.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ???(event);	}});
		isTimeSeriesCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));

		Label lD = new Label(swtInfoComp, SWT.NONE); lD.setText("Data:");
		lD.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		dataTextbox = new Text(swtInfoComp, SWT.MULTI);
		dataTextbox.setText("");
		//dataTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ???(event);	}});
		dataTextbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		
		modifyButton = new Button(swtInfoComp, SWT.PUSH);
		modifyButton.setText("Update/modify");
		modifyButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { modifyButtonEvent(event);	}});
		modifyButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));

		addButton = new Button(swtInfoComp, SWT.PUSH);
		addButton.setText("Add");
		addButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { addButtonEvent(event);	}});
		addButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
	
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtInfoComp, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));

		swtInfoComp.pack();
		swtGroup.pack();
		doUpdate();
	}

	protected void refreshButtonEvent(Event event) {
		dataToGUI();
	}


	private Object makeObject(){
		String typeName = datatypeTextbox.getText();
	
		Class dataType;
		try {
			dataType = Class.forName(typeName);
		} catch (ClassNotFoundException e) {
			try{
				dataType = Class.forName("java.lang." + typeName);
			}catch(ClassNotFoundException e2){
				try{ //try anytihng before a space
					dataType = Class.forName(typeName.split(" ")[0]);
				}catch(Exception e3){
					statusLabel.setText("Unknown type class '" + typeName + "'");
					throw new RuntimeException(e);
				}
			}
		}
	
		try {			
			Gson gson = new Gson();		
			Object data = gson.fromJson(dataTextbox.getText(), dataType);
			
			return data;
			
		}catch(Exception err){
			statusLabel.setText("Unable to decipher data: " + err);			
			throw err;
		}
	}
	
	protected void modifyButtonEvent(Event event) {
		String id = nameTextbox.getText();
		boolean tobePersistent = isPersistentCheckbox.getSelection();
		boolean isTimeSeries = isTimeSeriesCheckbox.getSelection();
		
		Object data = makeObject();
		
		//look for it in normal metadata first
		boolean wasPersistent = false;
		
		MetaData metadata = proc.getCompleteSeriesMetaDataMap().getMetaDataEntry(id);
		MetaData pMetadata = proc.getPersistentMetadata().getMetaDataEntry(id);
				
		if(pMetadata != null)
			metadata = pMetadata;
		
		if(tobePersistent){
			proc.getPersistentMetadata().put(id, data, isTimeSeries);
		}else{
			//remove it from persistent
			proc.getPersistentMetadata().remove(id); 
		}
			
		//always update in actual metadata
		proc.setSeriesMetaData(id, data, isTimeSeries);
	
		statusLabel.setText("Modified '"+id+"'.");
	}

	
	protected void addButtonEvent(Event event) {
		String id = nameTextbox.getText();
		boolean isPersistent = isPersistentCheckbox.getSelection();
		boolean isTimeSeries = isTimeSeriesCheckbox.getSelection();
		
		Object data = makeObject();
		
		if(isPersistent){
			proc.getPersistentMetadata().put(id, data, isTimeSeries);
			
		}else{
			
			proc.setSeriesMetaData(id, data, isTimeSeries);
		}
		
		
	}

	protected void settingsChangingEvent(Event event) {
		MetaDataManagerConfig config = proc.getConfig();
		 
		config.overwrite = overwriteCheckbox.getSelection();
		
		String selectedString = whenToApplyCombo.getText();
		if(selectedString.equals("Never")) {
			config.addOnChange = false;
			config.addOnEvent = null;
			return;
			
		}else if(selectedString.equals("On change")) {
			config.addOnChange = true;
			config.addOnEvent = null;
			return;
			
		}else {
			config.addOnChange = false;
			config.addOnEvent = imageProc.core.EventReciever.Event.valueOf(selectedString);
		}
	}

	private void forceUpdateEvent(Event e) {
		proc.calc();
	}


	@Override
	public final void generalControllerUpdate() {
		if (swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}

	public final String slowUpdateSyncObject = new String("SlowUpdateSyncObject");

	protected void doUpdate() {

		settingsCtrl.doUpdate();
		
		dataToGUI();
		
		statusLabel.setText(proc.getStatus());

	}


	private final String formatTableValue(double val) {
		return Double.isNaN(val) ? "NaN" : tableValueFormat.format(val);
	}

	private void dataToGUI() {
		
		MetaDataMap activeMap = proc.getCompleteSeriesMetaDataMap();		
		MetaDataMap fullMap;
		synchronized (activeMap) { fullMap = activeMap.clone(); } //clone to avoid collisions/locking
		
		TableItem[] select = table.getSelection();
		String selectedID = (select == null || select.length < 1) ? null : select[0].getText(COL_ID);
		
		//don't reset the table when the user is in there, as it is annoying
		if(table.isFocusControl())
			return; 
		
		table.removeAll();
		
		boolean showPersistent = showPersistentMetaRadio.getSelection();
		
		Set<Entry<String, MetaData>> entrySet = fullMap.entrySet();
		ArrayList<Entry<String, MetaData>> entryList = new ArrayList<>(entrySet);
				
		Collections.sort(entryList, new Comparator<Entry<String, MetaData>>() {
			@Override
			public int compare(Entry<String, MetaData> o1, Entry<String, MetaData> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}			
		});
		
		for(Entry<String, MetaData> entry : entryList){
			String id = entry.getKey();
			MetaData metadata = entry.getValue();
			
			MetaData persistentMetaEntry = proc.getPersistentMetadata().getMetaDataEntry(id);
			boolean isPersistent = persistentMetaEntry != null;
			
			if(showPersistent && !isPersistent)
				continue;
			
			TableItem item = new TableItem(table, SWT.NONE);
			
			item.setText(COL_ID, id);
			item.setText(COL_PERSISTENT, isPersistent ? "Y" : "");
			item.setText(COL_TIME_SERIES, metadata.isTimeSeries ? "Y" : "");
			item.setText(COL_DATATYPE, (metadata == null || metadata.object == null) ? "(null)" : metadata.object.getClass().getName());
			item.setText(COL_LEVEL, "No Impl");
			
			if(selectedID != null && selectedID.equals(id))
				table.setSelection(item);
		}
		
	}

	private void tableItemSelected() {
		TableItem items[] = table.getSelection();
		if(items.length < 1)
			return;
			
		TableItem item = items[0];
		String id = item.getText(COL_ID);
		try{
			
			MetaData metaDataPers = proc.getPersistentMetadata().getMetaDataEntry(id);
			MetaData metaData = proc.getCompleteSeriesMetaDataMap().getMetaDataEntry(id);
			
			boolean isPersistent;
			if(metaData == null || proc.getConfig().overwrite) {
				metaData = metaDataPers;
				isPersistent = true;				
			}else {
				isPersistent = false;
			}
			
			if(metaData == null){				
				throw new RuntimeException("Selected entry doesn't exist");
			}
			
			
			nameTextbox.setText(id);
			isPersistentCheckbox.setSelection(isPersistent);
			isTimeSeriesCheckbox.setSelection(metaData.isTimeSeries);
			
			if(metaData.object == null){
				datatypeTextbox.setText("(null)");
				dataTextbox.setText("(null)");
			}else{
				datatypeTextbox.setText( metaData.object.getClass().getName());
				
				Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create();
				String json = gson.toJson(metaData.object);
				
				dataTextbox.setText(json);
			}
			statusLabel.setText("OK");
			
		}catch(Exception err){
			datatypeTextbox.setText("(ERROR)");
			String errMsg = "Unable to get JSON for '"+id+"': " + err;
			statusLabel.setText(errMsg);
			dataTextbox.setText("Unable to get JSON for '"+id+"': " + err);
		}
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
		// proc.destroy(); //and actively kill it, for good measure
	}

	@Override
	public Object getInterfacingObject() {
		return swtGroup;
	}

	@Override
	public MetaDataManager getPipe() {
		return proc;
	}
}
