package imageProc.metaview;

import imageProc.core.MetaDataMap;
import imageProc.core.EventReciever.Event;

public class MetaDataManagerConfig {
	/** Metadata to be added/overwritten by this module */
	public MetaDataMap persistentMetadata = new MetaDataMap();;
	
	public Event addOnEvent = Event.Process;

	protected boolean addOnChange = false;	
	
	public boolean overwrite = false;

	
}
