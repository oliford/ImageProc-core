package imageProc.metaview;

import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.core.EventReciever.Event;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;

import org.eclipse.swt.widgets.Composite;

import otherSupport.SettingsManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;


/** Adds user specified metadata to the source.
 * 
 * @author oliford
 */
public class MetaDataManager extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, EventReciever {

	protected boolean abortCalc = false;

	private String status;
	
	private boolean lastCalcComplete = false;

	private String lastLoadedConfig;
	
	private MetaDataManagerConfig config = new MetaDataManagerConfig();

	public MetaDataManager() {
		status = "init";
	}
	
	public MetaDataManager(ImgSource source, int selectedIndex) {
		this();
		setSource(source);
		setSelectedSourceIndex(selectedIndex);
	}
	
	
	public boolean getAutoUpdate() { return this.config.addOnChange; }

	@Override
	public void imageChangedRangeComplete(int idx) { 
		if(config.addOnChange){
			calc();
		}
 	}
	
	@Override
	protected void selectedImageChanged() {
		super.selectedImageChanged();
		if(config.addOnChange)
			calc();
		
		//maybe GUI needs to know anyway
		updateAllControllers();			
	}

	@Override	
	public void setAutoUpdate(boolean autoCalc) {
		if(!this.config.addOnChange && autoCalc){
			this.config.addOnChange = true;
			updateAllControllers();
			calc();			
		}else{
			this.config.addOnChange = autoCalc;
			updateAllControllers();
		}
	}

	@Override
	public void calc() {
		abortCalc = false;
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalc(); } });
	}
		
	/** Guaranteed to be called only once at a time */
	protected void doCalc(){
		try{
			lastCalcComplete = false;
			if(connectedSource == null)
				return;
			
			int nF = connectedSource.getNumImages();
			
			synchronized (config.persistentMetadata) {
				
				ArrayList<String> persistentAdded = new ArrayList<String>();
				
				//getConnectedSource().addMetaDataMap(persistentMetadata);
				MetaDataMap metaMap = getConnectedSource().getCompleteSeriesMetaDataMap();
				for(Entry<String, MetaData> entry : config.persistentMetadata.entrySet()) {
					String name = entry.getKey();
					if(config.overwrite || metaMap.get(name) == null) {
						MetaData metaData = entry.getValue();
						connectedSource.setSeriesMetaData(name, metaData.object, metaData.isTimeSeries);
						persistentAdded.add(name);
					}
				}
				
				connectedSource.setSeriesMetaData("/MetaDataManager/persistentAdded", persistentAdded.toArray(new String[0]), false);
							
				specialCases();
			}
			
			updateAllControllers();
			lastCalcComplete = true;
			setStatus("Done");
			
		}catch(RuntimeException err){
			err.printStackTrace();
			setStatus("ERROR: " + err);
		}
	}
	
	
	protected void specialCases() {
		
		//check that the AOI matches the saved image size
		Object aoiWidth = config.persistentMetadata.get("/AndorCam/config/AOIWidth");
		Object aoiHeight = config.persistentMetadata.get("/AndorCam/config/AOIHeight");
		Img img = connectedSource.getImage(0);
		if(aoiWidth != null && ((Number)aoiWidth).intValue() != img.getWidth())
			throw new RuntimeException("Metadata width mismatch /AndorCam/config/AOIWidth=" + ((Number)aoiWidth).intValue() + ", image width = " + img.getWidth());
		if(aoiHeight != null && ((Number)aoiHeight).intValue() != img.getHeight())
			throw new RuntimeException("Metadata width mismatch /AndorCam/config/AOIHeight=" + ((Number)aoiHeight).intValue() + ", image height = " + img.getHeight());
		
		
		
		//ILS_Red missing metadata requires ExposureTime and FrameRate from the settings table
		// in the pilot autoconfiguration
		Object applyILSRedAutoexposure = config.persistentMetadata.get("/MetaDataManager/specialCases/ILS_Red_Binning/enable");
		Object expTimeMetadataSrc = connectedSource.getSeriesMetaData("/AndorCam/config/ExposureTime");
		if(applyILSRedAutoexposure != null && ((Boolean)applyILSRedAutoexposure) &&
				(expTimeMetadataSrc == null || !(expTimeMetadataSrc instanceof Double) || Double.isNaN(((Double)expTimeMetadataSrc)) )) {
						
			//first determine the average frame time					
			double t[] = (double[])Mat.fixWeirdContainers(connectedSource.getSeriesMetaData("/w7x/archive/time"), true);
			double dt = 0;
			int n=0;
			for(int i=0; i < (t.length-1); i++) {
				if(t[i] >= 0 && t[i+1] >= 0 && t[i+1] > t[i]) {
					dt += t[i+1] - t[i];
					n++;
				}
			}
			dt /= n;
			
			double dtSigma = 0;
			n=0;
			for(int i=0; i < (t.length-1); i++) {
				if(t[i] >= 0 && t[i+1] >= 0 && t[i+1] > t[i]) {
					dtSigma += Math.pow((t[i+1] - t[i]) - dt, 2);
					n++;
				}
			}
			dtSigma /= n;
			
			if(dtSigma > (0.01 * dt)) {
				throw new RuntimeException("Greater than 1% variation in dt, not sure about the frame time");
			}
			
			String jsonStr = OneLiners.fileToText("/data/jsonSettings/W7XPilot/ILS_Red-Default");

			JsonObject jObj = new JsonStreamParser(jsonStr).next().getAsJsonObject();					
			JsonArray jArr = jObj.getAsJsonObject("autoconf").getAsJsonArray("tableLookup");
			
			JsonArray expTimeArr = jArr.get(1).getAsJsonArray();
			JsonArray frameTimeArr = jArr.get(2).getAsJsonArray();
			
			double expTimes[] = new double[expTimeArr.size()];
			double frameTimes[] = new double[expTimeArr.size()];
			
			
			int j=0;
			for(int i=1; i < expTimes.length; i++){						
				expTimes[j] = expTimeArr.get(i).getAsDouble();
				frameTimes[j] = frameTimeArr.get(i).getAsDouble();
			
				if(j == 0 || frameTimes[j] > frameTimes[j-1])
					j++;
			}
			
			expTimes = Arrays.copyOf(expTimes, j);
			frameTimes = Arrays.copyOf(frameTimes, j);
			
			Interpolation1D interp = new Interpolation1D(frameTimes, expTimes, InterpolationMode.LINEAR, ExtrapolationMode.LINEAR, 0);
			
			double expTimeSecs = interp.eval(dt * 1000) / 1000;
			double frameTimeSecs = dt;
			connectedSource.setSeriesMetaData("/AndorCam/config/ExposureTime", expTimeSecs, false);
			connectedSource.setSeriesMetaData("/AndorCam/config/FrameRate", 1.0 / frameTimeSecs, false);
			connectedSource.setSeriesMetaData("/MetaDataManager/specialCases/ILS_Red_Binning/dt", dt, false);
			connectedSource.setSeriesMetaData("/MetaDataManager/specialCases/ILS_Red_Binning/dtSigma", dtSigma, false);
			connectedSource.setSeriesMetaData("/MetaDataManager/specialCases/ILS_Red_Binning/ExposureTime", expTimeSecs, false);
			connectedSource.setSeriesMetaData("/MetaDataManager/specialCases/ILS_Red_Binning/FrameRate", 1.0 / frameTimeSecs, false);
			
		}
	}

	@Override
	public boolean wasLastCalcComplete(){ return lastCalcComplete; }

	@Override
	public void event(Event event) {
		if(event == Event.GlobalAbort)
			abortCalc();
		if(event == config.addOnEvent)
			calc();
	}
	
	@Override
	public void abortCalc(){ 
		abortCalc = true;
	}

	@Override
	public MetaDataManager clone() { return new MetaDataManager(connectedSource, getSelectedSourceIndex());	}


	@Override
	public boolean isIdle() { 
		return !ImageProcUtil.isUpdateInProgress(this);
	}

	
	public void mapModified() {		
		updateAllControllers();
		if(this.config.addOnChange)
			calc();
	}
	
	public MetaDataMap getPersistentMetadata() { return config.persistentMetadata; }
	
	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			MetaDataManagerSWTController controller = new MetaDataManagerSWTController((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public String getStatus() { return status; }

	private void setStatus(String status) {
		this.status = status;
		updateAllControllers();
	}

	@Override
	public void loadConfig(String id) {
		if(id.startsWith("jsonfile:")){
			String fileName = id.substring(9);
			String jsonString = OneLiners.fileToText(fileName);
			
			Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
			
			config = gson.fromJson(jsonString, MetaDataManagerConfig.class);			
			
			//if its empty, assume it didn't work (not strictly true)
			if(config == null || config.persistentMetadata == null || config.persistentMetadata.entrySet().isEmpty()) {
				//see if it's in the old format
				logr.warning("Unable to load settings " + id + ". Trying old format.");
			
				config = new MetaDataManagerConfig();
				config.persistentMetadata = gson.fromJson(jsonString, MetaDataMap.class);
				if(config.persistentMetadata != null && !config.persistentMetadata.entrySet().isEmpty()) {
					logr.warning("Settings " + id + " is in old format (no config class, only the metadata map)");
				}
			}

			updateAllControllers();
		}else {
			throw new RuntimeException("Unknown config type " + id);
		}
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}

	@Override
	public void saveConfig(String id) {
		if(id.startsWith("jsonfile:")){
			String fileName = id.substring(9);
					
			Gson gson = new GsonBuilder()
					.serializeSpecialFloatingPointValues()
					.serializeNulls()
					.setPrettyPrinting()
					.create(); 
			
			String json = gson.toJson(config);
		
			OneLiners.textToFile(fileName, json);
			lastLoadedConfig = id;
		}
	}

	public MetaDataManagerConfig getConfig() { return this.config;	}	
}
