package imageProc.control.arduinoComm;

public class ArduinoCommConfig {
	
	public int watchdogTimeoutMS = 3600000;
	
	public SequenceConfig sequence = null;
	
	public MotorConfig motor[];
	
	/** Send configuration to arduino on config load */
	public boolean setConfigOnLoad = true;

	/** Serial port to connect to */
	public String portName = "/dev/ttyACM0";
	
	/** BAUD rate to connect serial port with */
	public int baudRate = 115200;
	
	/** Minimum time between successive commands being sent */ 
	public long minCommandTime = 0;

	public boolean enableDAQ = false;

	/** Configuration of saving of state to somewhere */ 
	public SaveStateConfig saveState = new SaveStateConfig();
	
}
