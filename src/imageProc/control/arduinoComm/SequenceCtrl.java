package imageProc.control.arduinoComm;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import imageProc.core.ImgSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import otherSupport.bufferControl.DirectBufferControl;

/** Sets up sequences in the Arduino and manages the triggering of the fast advance line
 * and the camera via the NI DAC.
 * 
 * Sometimes, the Arduino may be driven by a different source (e.g. the Camera's exporsure output)
 * in which case the sequencing by the DAC here isn't needed. 
 * 
 * @author oliford
 */
public class SequenceCtrl {
	public static final byte FAST_CMD_ADVANCE = (byte)0xF0;
	public static final byte FAST_CMD_ABORT = (byte)0xF1;
	
	private ArduinoCommHanger proc;
	
	private long lastSequenceEndTime = -1;
	
	/** Is the arduino in sequence */
	private boolean remoteInSequence; 
	
	private boolean softTriggerArmed = false;
	
	public SequenceCtrl(ArduinoCommHanger proc) {
		this.proc = proc;
	}
	
	public void armSoftTrigger(boolean enable, SequenceConfig config){
		this.softTriggerArmed = enable;
		if(enable)
			proc.config().sequence = config;
	}
	
	public void softTriggerStart(){
		if(softTriggerArmed){		
			initSequence(null);
		}
	}
	
	public void softTriggerAbort(){
		abort();
	}
	
	public void initSequence(SequenceConfig newConfig) {
		
		SequenceConfig config = proc.config().sequence;
		if(newConfig != null){
			proc.config().sequence = newConfig;
			config = newConfig;
		}
		
		if(config == null){
			System.err.println("initSequence(): No config to start");
		}
		
		calcSequence(config); //some of this makes sense so do this anyway, even if the DAC isn't doing the sequencing

		//make sure watchdog timeout is longer than sequence (though we can't account for trigger delay)
		int watchdogTimeMS = proc.getWatchdogTimeout();
		if(watchdogTimeMS < (int)(config.totalTimeUS * 1.1 / 1000)){
			proc.setWatchdogTimeout((int)(config.totalTimeUS * 1.1 / 1000));
		}
		
		//do one last ping first, and ask for the motor positions and oven temp 		
		proc.serialComm().send("\\PING");
		//proc.requestOvenTemp();
		proc.getMotorCtrl().motorRequestPositions();
		try {
			Thread.sleep(300); //hackery hackery
		} catch (InterruptedException e) { }
		
		config.initMotorPositions = proc.getMotorCtrl().getMotorPoitions();
		//config.initOvenTemp = proc.getInHallCtrl().getOvenTemp();
		
		if(config.lineTrigger == SequenceConfig.ADVANCE_TIMED){
			proc.serialComm().send("SEQ-ADVTIM" + config.selfAdvanceUS);
		}
		proc.serialComm().send("SEQ-ADVDAQ" + (config.daqOnAdvance ? "1" : "0"));
		proc.serialComm().send("SEQ-SETREC" + config.recordMotorID);
		proc.serialComm().send("SEQ-SETSTP" + config.recordStep);
		
		proc.serialComm().send("SEQ-INIT"
				+ config.nFrames + "," 
				+ config.flcMode + "," 
				+ config.nStepsPerFrame + "," 
				+ config.lineTrigger + ","
				+ config.motorID);
		//this sometimes doesn't respond
		
		//save the sequence config
		ImgSource src = proc.getConnectedSource();
		if(src != null){
			src.addNonTimeSeriesMetaDataMap(config.toMap("arduinoSeq/config"));
		}
			
		try {
			Thread.sleep(100); //hackery hackery
		} catch (InterruptedException e) { }
		
	}
	
	public void calcSequence(SequenceConfig c){
		
		c.exposuresPerFrame = (c.flcMode == SequenceConfig.FLC_INTERLACE) ? 2 : 1;
		
		c.exposureCLKs = c.exposureTimeUS*1000 / c.clockPeriodNS;
		c.postExposure1CLKs = c.postExposureTimeUS*1000 / c.clockPeriodNS;
		c.postExposure2CLKs = c.postExposureTimeUS*1000 / c.clockPeriodNS;
		
		c.expPulseCLKs = Math.max(2, SequenceConfig.EXPOSURE_PULSE_LENGTH_NS / c.clockPeriodNS);
		c.advPulseCLKs = Math.max(2, SequenceConfig.FAST_ADVANCE_PULSE_LENGTH_NS / c.clockPeriodNS);
		
		//For the second or only exposure, the stepping time which starts as soon
		//as the exposure is finished, probably takes longer than the given post exposure time		
		if(c.nStepsPerFrame > 0){
			c.steppingTimeCLKs = (c.stepPeriodUS*1000 / c.clockPeriodNS) * (c.nStepsPerFrame + 2); //a little bit more for stabilisation
			if(c.steppingTimeCLKs > c.postExposure2CLKs){
				c.postExposure2CLKs = c.steppingTimeCLKs;
			}
		}
		
		c.frameTimeCLKs = ((c.exposuresPerFrame == 2) ? (c.exposureCLKs + c.postExposure1CLKs) : 0)
							+ (c.exposureCLKs + c.postExposure2CLKs);
		
		c.startDelayCLKs = c.startDelayUS*1000/c.clockPeriodNS;
		
		c.fullLenCLKs = c.startDelayCLKs + c.frameTimeCLKs * c.nFrames;
		
		if(c.motorID < 0){
			c.startPos = 0;
			c.endPos = 0;
		}else{
			c.startPos = proc.getMotorCtrl().getMotorPos(c.motorID);
			c.endPos = c.startPos + c.nFrames * c.nStepsPerFrame;
		}
				
		//now refill times		
		c.frameTimeUS = c.frameTimeCLKs * (c.clockPeriodNS/1000);
		c.totalTimeUS = c.fullLenCLKs * (c.clockPeriodNS/1000);
		
		//advance time, if doing self advance will be frameTime+postExposureTime
		c.selfAdvanceUS = c.frameTimeUS;
	}
	
	public void abort() {
		fastAbortSequence();
	}
	
	public void fastAbortSequence() {
		//serialComm.send("SEQ-STOP"); //slow way
		if(proc.serialComm().isConnected())
			proc.serialComm().sendByte(FAST_CMD_ABORT);
	}

	public void requestSequenceRecord() {
		proc.serialComm().send("SEQ-RECD");
	}
	
	public void fastAdvanceSequence() {
		//serialComm.send("SEQ-ADVN");
		proc.serialComm().sendByte(FAST_CMD_ADVANCE);
	}

	public String getStatusString() {
		
		return "Arduino: " + (remoteInSequence ? "Active" : "Inactive");
	}
	
	public void setArduinoInSeq(boolean active){ this.remoteInSequence = active; }
	
	public boolean isRemoteActive(){ return remoteInSequence; }

	public void recievedRecordDump(byte[] lastBinaryDump) {
		if(proc.getConnectedSource() == null)
			return;
		
		SequenceConfig config = proc.config().sequence;
		
		proc.getConnectedSource().setSeriesMetaData("/arduinoSeq/record/bindump", lastBinaryDump.clone(), false);
		
		ByteBuffer bBuf = ByteBuffer.wrap(lastBinaryDump);
		bBuf.order(ByteOrder.LITTLE_ENDIAN);
		
		int motorID = bBuf.get(0);
		int recordStep = bBuf.get(1);
		int nFrames = bBuf.getShort(2);
		
		System.out.println("Processing record of motorID " + motorID + " every " + recordStep + 
							" frames of total " + nFrames + " frames. Config.nFrames was " + config.nFrames);
		
		int nPos = nFrames / recordStep;
		int posRaw[] = new int[nPos];		
		for(int i=0; i < nPos; i++){
			posRaw[i] = bBuf.getInt(4 + i*4);
		}
				
		proc.getConnectedSource().setSeriesMetaData("/arduinoSeq/record/motorPosRaw", posRaw, false);
		
		int pos[] = new int[nFrames];
		for(int j=0; j < nFrames; j++){
			double i = j / recordStep;
			int i0 = (int)i;
			int i1 = (int)i + 1;
			
			if(i1 >= nPos){
				i0 = nPos - 2;
				i1 = nPos - 1;
			}
			
			double f = ((double)j - i0*recordStep) / recordStep;
			
			
			pos[j] = (int)((1.0-f)*posRaw[i0] + f*posRaw[i1]);
		
		}
		
		
		proc.getConnectedSource().setSeriesMetaData("/arduinoSeq/record/motorPos", pos, false);
		
		
		/*
		int lastFrame = bBuf.getShort(0);
		int nFrames = bBuf.getShort(2);
		int flcMode = bBuf.getShort(4);
		int nExps = (flcMode == 2) ? 2*nFrames : nFrames;
		
		int time[] = new int[nExps];
		int posX[] = new int[nExps];
		int posY[] = new int[nExps];
		
		for(int i=0; i < nExps; i++){
			time[i] = bBuf.getInt(6+i*12);
			posX[i] = bBuf.getInt(6+i*12+4);
			posY[i] = bBuf.getInt(6+i*12+8);
		}
		
		proc.getConnectedSource().setSeriesMetaData("/arduinoSeq/record/time", time);
		proc.getConnectedSource().setSeriesMetaData("/arduinoSeq/record/posX", posX);
		proc.getConnectedSource().setSeriesMetaData("/arduinoSeq/record/posY", posY);
		 */		
	}

	public void recievedLine(String line) {
		
		if(line.startsWith("SEQ:START")){
			setArduinoInSeq(true);			
		}else if(line.startsWith("SEQ:STOP")){
			setArduinoInSeq(false);
		}else if(line.startsWith("SEQ:RECDUMP")){
			recievedRecordDump(proc.serialComm().getLastBinaryDump());
		}
	}
	
	
}
