package imageProc.control.arduinoComm;

import algorithmrepository.Algorithms;
import imageProc.core.ImgSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SimpleIOCtrl {

	private ArduinoCommHanger proc;
	
	public SimpleIOCtrl(ArduinoCommHanger proc) {
		this.proc = proc;
		proc.state().simpleIO = new SimpleIOState();
	}
	
	private int nDigital = 8, nAnalog = 8;


	public void requestAnalogIn(int pinNum){
		proc.serialComm.send("SIO-GETA" + pinNum);
	}

	public void requestDigitalIn(int pinNum){
		proc.serialComm.send("SIO-GETD" + pinNum);
	}
	
	public void setAnalogOut(int pinNum, int value){
		proc.serialComm.send("SIO-SETA" + pinNum + "," + value);		
	}
	
	public void setDigitalOut(int pinNum, boolean high){
		proc.serialComm.send("SIO-SETD" + pinNum + "," + (high ? 1 : 0));		
	}
	
	public void receivedLine(String line){
		ImgSource src = proc.getConnectedSource();
		
		if(line.startsWith("SIO:")){
			String command = line.substring(4);
		
			if(command.startsWith("GET")){
				boolean analog = (command.charAt(3) == 'A');
				String parts[] = command.substring(4).split(",");
				int pinNum = Algorithms.mustParseInt(parts[0].trim());
				int val = Algorithms.mustParseInt(parts[1].trim());
				
				if(analog){
					state().analogValues[pinNum] = val;
				}else{
					state().digitalValues[pinNum] = val;
				}

				proc.updateAllControllers();
			}
		}
	}

	public int[] getDigitalValues() { return state().digitalValues; }
	public int[] getAnalogValues() { return state().analogValues; }

	public SimpleIOState state() { return proc.state().simpleIO; }
}
