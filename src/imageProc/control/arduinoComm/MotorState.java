package imageProc.control.arduinoComm;

/** Motor state (currenyl 2017 back-end) */
public class MotorState {
	
	/** last reported position */
	public int pos;
	
	/** current position the motor should be heading to if enabled */
	public int target;
	
	/** Name of the last target set, if set from the preset table */
	public String targetPresetName;

	/** Last report of motor enable state */
	public boolean enabled;

}
