package imageProc.control.arduinoComm;

import java.util.HashMap;

public class MotorConfig {
	/** Name of thing connected to motor */
	public String name;
	
	/** Home switch active low */
	public boolean switchPolarity = false;
	
	/** Motor direction reversed */
	public boolean dir = false;
	
	/** Motor step period in [us] */
	public int period = 800;
	
	/** Motor current in mA (if vREF is connected to PWM) */
	public int currentMilliAmp = 800;
	
	/** Timeout to disable motors to stop overheating [ms] */
	public int enableTimeoutMS = 30000;
	
	/** Home switch debounce delay, in steps. The zero position is taken from the first
	edge after which the switch is active for at least this many steps */ 
	public int homeSwitchMinSteps = 0;
	
	/** List of presets, by name */
	public HashMap<String, Double> presets = new HashMap<String, Double>();

	/** If not null, each change of position, homing etc is appended to this log file. Settings changes are not logged. */
	public String logFileName = null; 
	
	public MotorConfig() {
		presets.put("Ten", 10.0);
		presets.put("One Hundred", 100.0);
		presets.put("One Thousand", 1000.0);
	}
}
