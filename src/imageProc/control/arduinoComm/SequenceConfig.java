package imageProc.control.arduinoComm;

import java.util.HashMap;
import java.util.Map;

/** Terminology is a bit confusing here.
 * 
 * Under the sequencer, a 'frame' is one set of exposures at single motor position.
 * It's usually 1 exposure per 'frame', but 2 if the FLC interlace is on.
 * 
 * @author oliford
 */
public class SequenceConfig {
	
	public static final int EXPOSURE_PULSE_LENGTH_NS = 100000; //100µs	
	public static final int FAST_ADVANCE_PULSE_LENGTH_NS = 100000; //100µs
	
	/** Channel of Camera exposure start trigger */
	public static final int CH_CAM_TRIG = 0;
	public static final int CAM_TRIG_OFF = 0;
	public static final int CAM_TRIG_ON = (1 << CH_CAM_TRIG);
	
	/** Channel of fast advance line to Arduino */
	public static final int CH_FAST_ADV = 1;
	public static final int FAST_ADV_OFF = (1 << CH_FAST_ADV);
	public static final int FAST_ADV_ON = 0;
	
	/** PFI Channel of external sequence start trigger */
	public static final int PFI_START_TRIG = 0;

	/** FLC drive line low for entire scan */
	public static final int FLC_LOW = 0;

	/** FLC drive line high for entire scan */
	public static final int FLC_HIGH = 1;

	/** One exposure low and one exposure high in each frame, before stepping */
	public static final int FLC_INTERLACE = 2;
	
	public static final int ADVANCE_NONE = 0;
	public static final int ADVANCE_LINE = 1;
	public static final int ADVANCE_EXPOSE_RISING = 2;
	public static final int ADVANCE_EXPOSE_FALLING = 3;
	public static final int  ADVANCE_TIMED = 4;


	public long startDelayUS;	
	/** Number of sequence frames (which is a set of exposures at one motor position */
	public int nFrames;	
	public int flcMode;	
	public int nStepsPerFrame;	
	public long stepPeriodUS;	
	public long clockPeriodNS;	
	public int lineTrigger;
	public long exposureTimeUS;	
	public long postExposureTimeUS;	
	public boolean abortTrigAtEnd;	
	public long restartPeriodMS;
	public long selfAdvanceUS; //for ADVANCE_TIMED
	public int motorID;
	public boolean daqOnAdvance;
	/** record position of this motor at frame advance */
	public int recordMotorID; 
	/** record position only every n'th frame */
	public int recordStep; 
		
	//stuff filled in by calcSequence
	public long startPos;
	public long endPos;
	public int exposuresPerFrame;	
	public long exposureCLKs;
	public long postExposure1CLKs;
	public long postExposure2CLKs;
	public long steppingTimeCLKs;
	public long expPulseCLKs;
	public long advPulseCLKs;
	public long frameTimeCLKs;
	public long startDelayCLKs;
	public long fullLenCLKs;
	public long frameTimeUS;
	public long totalTimeUS;
	

	//stuff we save at init time
	public int initMotorPositions[];
	//public double initOvenTemp;

	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(prefix + "/startDelayUS", startDelayUS);
		map.put(prefix + "/nFrames", nFrames);
		map.put(prefix + "/flcMode", flcMode);
		map.put(prefix + "/exposureTimeUS", exposureTimeUS);
		map.put(prefix + "/postExposureTimeUS", postExposureTimeUS);
		map.put(prefix + "/nStepsPerFrame", nStepsPerFrame);
		map.put(prefix + "/clockPeriodNS", clockPeriodNS);
		map.put(prefix + "/stepPeriodUS", stepPeriodUS);
		map.put(prefix + "/lineTrigger", lineTrigger);
		map.put(prefix + "/motorID", motorID);
		map.put(prefix + "/selfAdvanceUS", selfAdvanceUS);
		map.put(prefix + "/daqOnAdvance", daqOnAdvance);
				
		map.put(prefix + "/startPos", startPos);
		map.put(prefix + "/endPos", endPos);
		map.put(prefix + "/exposuresPerFrame", exposuresPerFrame);	
		map.put(prefix + "/exposureCLKs", exposureCLKs);
		map.put(prefix + "/postExposure1CLKs", postExposure1CLKs);
		map.put(prefix + "/postExposure2CLKs", postExposure2CLKs);
		map.put(prefix + "/steppingTimeCLKs", steppingTimeCLKs);
		map.put(prefix + "/expPulseCLKs", expPulseCLKs);
		map.put(prefix + "/advPulseCLKs", advPulseCLKs);
		map.put(prefix + "/frameTimeCLKs", frameTimeCLKs);
		map.put(prefix + "/startDelayCLKs", startDelayCLKs);
		map.put(prefix + "/fullLenCLKs", fullLenCLKs);
		map.put(prefix + "/frameTimeUS", frameTimeUS);
		map.put(prefix + "/totalTimeUS", totalTimeUS);
		
		map.put(prefix + "initMotorPositions", initMotorPositions);
		//map.put(prefix + "initOvenTemp", initOvenTemp);
		
		return map;
	}
/*	
	@Override
	protected  clone() throws CloneNotSupportedException {

		
		startDelayUS startDelayUS;
			
		nFrames;
		
		flcMode;
		
		nStepsPerFrame;
		
		stepPeriodUS;
		
		clockPeriodNS;
		
		lineTrigger = CONFIG;

		startTriggerPFI;
		
		exposureTimeUS;
		
		postExposureTimeUS;
		
		abortTrigAtEnd;
		
		restartPeriodMS;
	}
		*/
}
