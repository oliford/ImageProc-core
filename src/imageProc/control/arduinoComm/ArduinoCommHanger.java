package imageProc.control.arduinoComm;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.Algorithms;
import imageProc.control.arduinoComm.LoggedComm.LogUser;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Communications with the Arduino in-hall controller code IMSEArduinoCtrl */

public class ArduinoCommHanger extends ImgSourceOrSinkImpl implements ImgSink, EventReciever,
										ConfigurableByID, LoggedComm.LogUser {

	protected ArduinoCommSWTControl swtController;
	protected LoggedComm serialComm;
	protected DataAqCtrl dataAqCtrl;
	protected SimpleIOCtrl simpleIOCtrl;
	private MotorControl motorCtrl;
	private SequenceCtrl seqCtrl;
	
	/** To be replaced/overridden by inheriting classes (or not, if they wish) */ 
	private ArduinoCommConfig configPlain = new ArduinoCommConfig();	
	private ArduinoCommState statePlain = new ArduinoCommState();	
	public ArduinoCommConfig config() { return configPlain; }
	public ArduinoCommState state() { return statePlain; }
	

	/** Message from last attempt to save the state */
	public String lastStateSaveStatus;
			
	public ArduinoCommHanger() {
		this(false); //preset
	}
	
	public ArduinoCommHanger(boolean presetIsOutsideHall) {
		
		String id = "General";		
		config().portName = SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm."+id+".portName", "/dev/ttyACM0");
		config().baudRate = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.arduinoComm."+id+".baudRate", "115200"));
		
		serialComm = new LoggedComm(this);
		dataAqCtrl = new DataAqCtrl(this);
		simpleIOCtrl = new SimpleIOCtrl(this);
		motorCtrl = new MotorControl(this);
		seqCtrl = new SequenceCtrl(this);
	}

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new ArduinoCommSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}
	
	@Override
	/** Expose as public for SequenceCtrl */
	public void updateAllControllers() {
		super.updateAllControllers();
	}
		
	public void receivedLine(String line){
		
		if(line.startsWith("CTL:TYPE")){
			setupController(line.substring(9));
		}else if(line.startsWith("DAQ:DATA")){
			dataAqCtrl.recievedData(serialComm.getLastBinaryDump());
		}else if(line.startsWith("SIO:")){
			simpleIOCtrl.receivedLine(line);
		}else if(line.matches("MT[0-9]:.*")){
			motorCtrl.receivedLine(line);			
		}else if(line.startsWith("SEQ")){
			seqCtrl.recievedLine(line);			
		}
			
		updateAllControllers();
	}

	protected void setupController(String type){		
		setWatchdogTimeout(config().watchdogTimeoutMS);
		motorCtrl.setupController();		
	}
	
	@Override
	public void connected(){
		seqCtrl.fastAbortSequence(); //stop any active sequence first
		seqCtrl.setArduinoInSeq(false);
		
		serialComm.send("CTL-TYPE"); //ask what type of controller it is
		
	}
	
	@Override
	public void logChanged() { //call through from LoggedSerialComm
		updateAllControllers();
	}
	
	public LoggedComm serialComm(){ return serialComm; }
	
	@Override
	public void destroy() {
		serialComm.destroy();
		super.destroy();
	}

	public MotorControl getMotorCtrl() { return motorCtrl;	}	
	
	public ArduinoCommSWTControl getSWTController(){ return swtController; }
		
	public long getKeepAliveTimeMS(){ return keepAliveRunner.getPeriodMS(); }
	public void setKeepAliveTimeMS(long keepAliveTimeMS){ keepAliveRunner.setPeriodMS(keepAliveTimeMS);	};

	public DataAqCtrl dataAqCtrl(){ return dataAqCtrl; }
	public SimpleIOCtrl simpleIOCtrl(){ return simpleIOCtrl; }
	public SequenceCtrl seqCtrl(){ return seqCtrl; }
	
	private class KeepAliveRunner implements Runnable {
		private Thread thread;
		private boolean death = false;
		private long periodMS;
		
		public KeepAliveRunner() {
			setPeriodMS(60000); //60s default period
		}
		
		@Override
		public void run() {
			while(!death){
				if(serialComm != null && serialComm.isConnected()){
					if(inhibitKeepAlive()){
						System.out.println("Keep-alive inhibited because sequence is active.");
					}else{
						serialComm.send("PING");
					}
				}
				try {
					Thread.sleep(periodMS);
				} catch (InterruptedException e) { }
			}
		}
		
		public void setPeriodMS(long periodMS) {
			this.periodMS = periodMS;
			if(periodMS <= 0){
				death = true;
				if(thread != null && thread.isAlive()){ 
					thread.interrupt();
				}
			}else{
				death = false;
				if(thread == null || !thread.isAlive()){
					thread = new Thread(this);
					thread.start();
				}else{
					thread.interrupt();
				}
			}		
			
		}
		public long getPeriodMS() { return periodMS; }
	}
	
	private KeepAliveRunner keepAliveRunner = new KeepAliveRunner();
	private String lastLoadedConfig;

	
	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalStart:
			seqCtrl.softTriggerStart();
			break;
			
		case GlobalAbort:
			seqCtrl.softTriggerAbort();
			
			if(serialComm.isConnected())
				dataAqCtrl.abort();

			break;
		default:
			break;
		}		

		//on any event, update the metadata
		updateMetadata();
	}
	
	private void updateMetadata() {
		for(int i=0;i < MotorControl.nMotors; i++) {			
			connectedSource.setSeriesMetaData("ArduinoComm/motor/"+i+"/lastRecievedPos", motorCtrl.getMotorPos(i), false);
			connectedSource.setSeriesMetaData("ArduinoComm/motor/"+i+"/lastTarget", motorCtrl.getState(i).target, false);
		}
		
	}
	
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config());
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
		
		if(config().setConfigOnLoad) {
			//(re)send the controller type request so that
			//the config gets set when the response comes
			serialComm.send("CTL-TYPE");	
		}
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		configPlain = gson.fromJson(jsonString, ArduinoCommConfig.class);
		
		updateAllControllers();	
	}

	
 	protected boolean inhibitKeepAlive() {
		return seqCtrl.isRemoteActive();
	}


 	public int getWatchdogTimeout() { return config().watchdogTimeoutMS; }
 	public void setWatchdogTimeout(int watchdogTimeoutMS) { 
 		config().watchdogTimeoutMS = Math.max(5000, watchdogTimeoutMS);
 		serialComm.send("CTL-WDOG" + config().watchdogTimeoutMS);
	} 	
 	
 	public boolean isIdle(){ return true; }

	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }

	public String getTypeName() { return "General"; }
	
	public boolean isRemoteActive() { return false;	} //general has no remote activity ... ahem.... DAQ!

	public boolean hasMotors() { return true; }

	public String getLastStateSaveStatus() { return lastStateSaveStatus != null ? lastStateSaveStatus : ""; }
	
	public void saveStateNow() {
		throw new RuntimeException("No general implementation of state saving!");
	}
		
}
