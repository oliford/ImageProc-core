package imageProc.control.arduinoComm;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.Arrays;

import algorithmrepository.Mat;
import binaryMatrixFile.BinaryMatrixFile;
import binaryMatrixFile.DataConvertPureJava;
import edu.emory.mathcs.jtransforms.fft.FloatFFT_1D;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import net.jafama.FastMath;

public class DataAqCtrl {

	private ArduinoCommHanger proc;
	
	private short lastData[][];
		
	public DataAqCtrl(ArduinoCommHanger proc) {
		this.proc = proc;
	}
	
	private String SpinningPolSyncObj = new String("SpinningPolSyncObj");
	private boolean abortSpinningPolProc = false;
	
	public void recievedData(byte[] lastBinaryDump) {
			
		System.out.println("Processing DAQ data length " + lastBinaryDump.length);
		ByteBuffer bBuf = ByteBuffer.wrap(lastBinaryDump);
		bBuf.order(ByteOrder.LITTLE_ENDIAN);
		
		ShortBuffer sBuf = bBuf.asShortBuffer();
		int setNum = sBuf.get();
		int nSamples = sBuf.get();
		int nChannels = sBuf.get();
		int nIntegrate = sBuf.get();
		
		short setData[][] = new short[nChannels][nSamples];
		for(int iS=0; iS < nSamples; iS++){
			for(int iC=0; iC < nChannels; iC++){
				setData[iC][iS] = sBuf.get();
			}
		}
		
		ImgSource imgSrc = proc.getConnectedSource();
		if(imgSrc != null){
			imgSrc.setSeriesMetaData("dataAq/nIntegrate", nIntegrate, false);
			imgSrc.setSeriesMetaData("dataAq/lastSetNum", setNum, false);		
		}
		
		short allData[][][] = (short[][][])imgSrc.getSeriesMetaData("dataAq/raw");
		if(allData == null){
			allData = new short[setNum+1][][];
		}else if(allData.length <= setNum){
			allData = Arrays.copyOf(allData, setNum+1);
		}
		allData[setNum] = setData;
		imgSrc.setSeriesMetaData("dataAq/raw", allData, false);
		
		lastData = setData;
		proc.updateAllControllers();
		
		abortSpinningPolProc = false;
		ImageProcUtil.ensureFinalUpdate(SpinningPolSyncObj, new Runnable() {
			public void run() {
				processAllSpinningPol();
			}
		});
		
	}

	/** Process all available spinning polariser data */
	private void processAllSpinningPol(){
		ImgSource imgSrc = proc.getConnectedSource();
		if(imgSrc == null)
			return;
		
		short allData[][][] = (short[][][])imgSrc.getSeriesMetaData("dataAq/raw");
		if(allData == null)
			return;
		
		double measAng[] = (double[])imgSrc.getSeriesMetaData("dataAq/measAng");
		double ampCh0[] = (double[])imgSrc.getSeriesMetaData("dataAq/ampCh0");
		double ampCh1[] = (double[])imgSrc.getSeriesMetaData("dataAq/ampCh1");
		double freq[] = (double[])imgSrc.getSeriesMetaData("dataAq/freq");
		int nSets = allData.length;
		int alreadyCalced = 0;
		if(measAng == null){
			measAng = new double[nSets];
			ampCh0 = new double[nSets];
			ampCh1 = new double[nSets];
			freq = new double[nSets];
		}else if(measAng.length < nSets){
			alreadyCalced = measAng.length;
			measAng = Arrays.copyOf(measAng, nSets);
			ampCh0 = Arrays.copyOf(ampCh0, nSets);
			ampCh1 = Arrays.copyOf(ampCh1, nSets);
			freq = Arrays.copyOf(freq, nSets);
		}
		
		for(int iSet=alreadyCalced; iSet < nSets; iSet++){
			if(abortSpinningPolProc)
				break;
			
			if(allData[iSet] == null)
				continue;
			short dataCh0[] = allData[iSet][0];
			short dataCh1[] = allData[iSet][1];
			if(dataCh0 == null || dataCh1 == null)
				continue;
			
			double ret[] = procSpinningPolAngle(iSet, dataCh0, dataCh1);
			measAng[iSet] = ret[0] * 180/Math.PI;
			ampCh0[iSet] = ret[1];
			ampCh1[iSet] = ret[2];
			freq[iSet] = ret[3];
			
		}
		imgSrc.setSeriesMetaData("dataAq/measAng", measAng, false);
		imgSrc.setSeriesMetaData("dataAq/ampCh0", ampCh0, false);
		imgSrc.setSeriesMetaData("dataAq/ampCh1", ampCh1, false);
		imgSrc.setSeriesMetaData("dataAq/freq", freq, false);
	}

	private double[] procSpinningPolAngle(int setNum, short[] dataCh0, short[] dataCh1) {
		
		boolean debug = false;
		int nSamples = dataCh0.length;
		
		float dch0[] = new float[2*nSamples];
		float dch1[] = new float[2*nSamples];
		for(int iS=0; iS < nSamples; iS++){
			dch0[iS] = dataCh0[iS];
			dch1[iS] = dataCh1[iS];
		}
		
		//NumericalRec
		
		FloatFFT_1D fft = new FloatFFT_1D(nSamples);
		
		if(debug){
			Mat.mustWriteBinary("/tmp/d0.bin", (double[])Mat.convert1DArray(dch0, new double[dch0.length]));
			Mat.mustWriteBinary("/tmp/d1.bin", (double[])Mat.convert1DArray(dch1, new double[dch1.length]));
		}
		//fft both
		fft.realForwardFull(dch1);
		fft.realForwardFull(dch0);
		
		if(debug){
			Mat.mustWriteBinary("/tmp/df0.bin", (double[])Mat.convert1DArray(dch0, new double[dch0.length]));
			Mat.mustWriteBinary("/tmp/df1.bin", (double[])Mat.convert1DArray(dch1, new double[dch1.length]));
		}
		
		int maxPos = -1;
		float maxValSq = Float.NEGATIVE_INFINITY;
		for(int i=10; i < nSamples/2; i++){
			float absch0 = dch0[i*2]*dch0[i*2] + dch0[i*2+1]*dch0[i*2+1];
			float absch1 = dch1[i*2]*dch1[i*2] + dch1[i*2+1]*dch1[i*2+1];
			float absValSq = 0.3f*absch0 + 0.7f*absch1;
			if(absValSq > maxValSq){
				maxValSq = absValSq;
				maxPos = i;
			}
		}
		
		//gaussian window properties
		float gf0 = maxPos;
		float gfw = 0.30f * maxPos;
		float gfs = gfw / 2.35f;

		float fMax = 3*gf0;

		//gaussian window both FTs only on the +ve frequency side
		for(int i=0; i < nSamples/2; i++){
			double g = FastMath.exp( -FastMath.pow2((i - gf0)/gfs) / 2);
			
			dch0[i*2] *= g;
			dch0[i*2+1] *= g;
			dch0[(nSamples/2 + i)*2] *= 0; //0 all negative frequencies
			dch0[(nSamples/2 + i)*2+1] *= 0;
			
			dch1[i*2] *= g;
			dch1[i*2+1] *= g;
			dch1[(nSamples/2 + i)*2] *= 0; //0 all negative frequencies
			dch1[(nSamples/2 + i)*2+1] *= 0;
		}

		if(debug){
			Mat.mustWriteBinary("/tmp/df0b.bin", (double[])Mat.convert1DArray(dch0, new double[dch0.length]));
			Mat.mustWriteBinary("/tmp/df1b.bin", (double[])Mat.convert1DArray(dch1, new double[dch0.length]));
		}
		
		//and back into real space
		fft.complexInverse(dch0, false);
		fft.complexInverse(dch1, false);
		
		if(debug){
			Mat.mustWriteBinary("/tmp/d0b.bin", (double[])Mat.convert1DArray(dch0, new double[dch0.length]));
			Mat.mustWriteBinary("/tmp/d1b.bin", (double[])Mat.convert1DArray(dch1, new double[dch0.length]));
		}		
		
		//we now have mg~exp(+i(wt + phi_m)) and dg~exp(+i(wt + phi_d)); so we can get the difference:
		double phaseDiff[] = new double[nSamples];
		double amp0Sig[] = new double[nSamples];
		double amp1Sig[] = new double[nSamples];
		for(int i=0; i < nSamples; i++){
			float d0Re = dch0[2*i], d0Im = dch0[2*i+1];
			float d1Re = dch1[2*i], d1Im = dch1[2*i+1];
			
			 // phase difference comes from complex: dch1 / dch0 = exp(+i(phi_d - phi_m));
			//res = d1 / d0
			double c2d2 = d0Re*d0Re + d0Im*d0Im;				
			double resRe = (d1Re*d0Re + d1Im*d0Im) / c2d2;
			double resIm = (d1Im*d0Re - d1Re*d0Im) / c2d2;
			
			//some attempt at jump corrections
			
			phaseDiff[i] = FastMath.atan2(resIm, resRe);
			amp0Sig[i] = FastMath.sqrt(d0Re*d0Re + d0Im*d0Im);
			amp1Sig[i] = FastMath.sqrt(d1Re*d1Re + d1Im*d1Im);
		}

		//Mat.mustWriteBinary("/tmp/phasediff.bin", phaseDiff);
		double thres = 130*Math.PI/180; 
		for(int i=1; i < nSamples; i++){
			if(phaseDiff[i-1] > thres && phaseDiff[i] < -thres){
				for(int j=i; j < nSamples; j++){
					if(phaseDiff[j] + 2*Math.PI > 2*Math.PI)
						break;
					phaseDiff[j] += 2*Math.PI;
				}
				
			}else if(phaseDiff[i-1] < -thres && phaseDiff[i] > thres){
				for(int j=i; j < nSamples; j++){
					if(phaseDiff[j] - 2*Math.PI < -2*Math.PI)
						break;
					phaseDiff[j] -= 2*Math.PI; 
				}
			}
		}
		
		//Mat.mustWriteBinary("/tmp/phasediff-uw.bin", phaseDiff);
		
		double measAng = 0, amp0 = 0, amp1 = 0;
		int i0 = (int)(0.2*nSamples);
		int i1 = (int)(0.8*nSamples);
		int n = i1 - i0;
		for(int i=i0; i < i1; i++){
			measAng += phaseDiff[i];
			amp0 += amp0Sig[i];
			amp1 += amp1Sig[i];
		}
		
		return new double[]{ measAng / n / 2, amp0, amp1, fMax }; // measAng = phaseDiff / 2
			
	}

	public void init() {
		proc.serialComm().send("DAQ-INIT");
		
	}

	public void setContinuous(boolean continuous) {
		proc.serialComm().send("DAQ-CONT" + (continuous ? "1" : "0"));
	}
	
	public void start(boolean continuous) {
		ImgSource imgSrc = proc.getConnectedSource();
		if(imgSrc != null){
			imgSrc.setSeriesMetaData("dataAq/raw", null, false);
			imgSrc.setSeriesMetaData("dataAq/measAng", null, false);
			imgSrc.setSeriesMetaData("dataAq/ampCh0", null, false);
			imgSrc.setSeriesMetaData("dataAq/ampCh1", null, false);
		}
		setContinuous(continuous);
		proc.serialComm().send("DAQ-START0");
	}
	
	public void abort() {
		proc.serialComm().send("DAQ-ABORT");
	}
	
	public void close() {
		proc.serialComm().send("DAQ-CLOSE");
	}
	
	public static void main(String[] args) {
		double data[][] = Mat.mustLoadBinary("/tmp/pd.bin", true);
		double a[] = new double[1000];
		for(int j=0; j < 1000; j++){
			short d0[] = new short[1024];
			short d1[] = new short[1024];
			for(int i=0; i < 1024; i++){
				d0[i] = (short)(data[0][j*1024+i] * 1024 / 8);
				d1[i] = (short)(data[1][j*1024+i] * 1024 / 8);
			}

			a[j] = (new DataAqCtrl(null)).procSpinningPolAngle(0, d0, d1)[0];
			System.out.println(j + "\t" + a[j]);
		}
		Mat.mustWriteBinary("/tmp/a.bin", a);
	}

	public short[][] getLastData() {
		return lastData;
	}

	public double[] getMeasAng() {
		ImgSource imgSrc = proc.getConnectedSource();
		return (double[])imgSrc.getSeriesMetaData("dataAq/measAng");		
	}

}
