package imageProc.control.arduinoComm;

public class ArduinoCommState {
	
	/** Motor positions, targets and current state */ 
	public MotorState[] motor;
	
	/** Simple I/O pin states */
	public SimpleIOState simpleIO;
}
