package imageProc.control.arduinoComm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import algorithmrepository.Algorithms;
import imageProc.core.ImgSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class MotorControl {
	public static final int nMotors = 3;
	
	private ArduinoCommHanger proc;
	
		
	public MotorControl(ArduinoCommHanger proc) {
		this.proc = proc;
		if(proc.config().motor == null) {
			proc.config().motor = new MotorConfig[nMotors];
			proc.state().motor = new MotorState[nMotors];
			for(int i=0; i < nMotors; i++) {
				proc.config().motor[i] = new MotorConfig();
				proc.state().motor[i] = new MotorState();
			}
		}
	}
		
	public void receivedLine(String line){
		MotorConfig[] motorCfg = proc.config().motor;
		MotorState[] state = proc.state().motor;
		
		ImgSource src = proc.getConnectedSource();
		
		if(line.matches("MT[0-9]:.*")){
			
			int motorID = line.charAt(2) - '0';
			String command = line.substring(4);
		
			if(command.startsWith("HOME")){
				state[motorID].pos = 0;
				
				//some version of controller doesn't deal with signed integers.
				//When home is -ve, help the user by displaying it signed
				String parts[] = command.split("was ");
				if(parts.length > 1) {
					long l = Algorithms.mustParseLong(parts[1]);
					if(l > Integer.MAX_VALUE)
						proc.serialComm().getLog().append("  (Home signed = "+(int)l+")  \n");
				}
				posLog(motorID, "HOMED", 0);
			
			}else if(command.startsWith("POS")){
				state[motorID].pos = Algorithms.mustParseInt(line.substring(7).trim());
				if(proc.config().saveState.autosave)
					proc.saveStateNow();
				
				proc.setSeriesMetaData("ArduinoComm/motor/"+motorID+"/lastRecievedPos", state[motorID].pos, false);
				posLog(motorID, "POS", state[motorID].pos);
			
			}else if(command.startsWith("ENBL")){
				state[motorID].enabled = (Algorithms.mustParseInt(line.substring(8).trim()) != 0);
				if(proc.config().saveState.autosave)
					proc.saveStateNow();
				
				posLog(motorID, "ENABLED", state[motorID].enabled ? 1 : 0);
				
			}else if(command.startsWith("SWT")){
				motorCfg[motorID].switchPolarity = (Algorithms.mustParseInt(line.substring(7).trim()) != 0);
				
			}else if(command.startsWith("RDIR")){
				motorCfg[motorID].dir = (Algorithms.mustParseInt(line.substring(8).trim()) != 0);
				
			}
			
		}
	}

	public void setupController() {
		
		for(int i = 0; i < nMotors; i++){
			MotorConfig cfg = proc.config().motor[i];
			MotorState state = proc.state().motor[i];
			
			state.enabled = false; //off by default to stop start-up crash of controller
			motorSetEnable(i, state.enabled);
			motorSetSwitchPolarity(i, cfg.switchPolarity);
			motorSetDirection(i, cfg.dir);
			motorSetCurrent(i, cfg.currentMilliAmp);
			motorSetStepPeriod(i, cfg.period);
			motorSetHomeMinSteps(i, cfg.homeSwitchMinSteps);
			motorRequestPos(i);
			proc.serialComm().send("MT"+i+"-TOUT" + cfg.enableTimeoutMS);
			
			posLog(i, "INIT", 0);
		}
	}
	
	public void motorSetCurrent(int motorID, int milliAmps) {
		proc.config().motor[motorID].currentMilliAmp = milliAmps;
		proc.serialComm().send("MT"+motorID+"-CURR" + milliAmps);
	}
	
	public int getMotorCurrentMilliAmp(int motorID){
		return proc.config().motor[motorID].currentMilliAmp;
	}
	
	public void motorSetStepPeriod(int motorID, int microSecs) {
		proc.config().motor[motorID].period = microSecs;
		proc.serialComm().send("MT"+motorID+"-SDLY" + microSecs);
	}
	
	public int motorGetStepPeriod(int motorID){
		return proc.config().motor[motorID].period;
	}
	
	public int motorGetHomeMinSteps(int motorID) {
		return proc.config().motor[motorID].homeSwitchMinSteps;
	}
	
	public void motorSetHomeMinSteps(int motorID, int minSteps) {
		proc.config().motor[motorID].homeSwitchMinSteps = minSteps;
		proc.serialComm().send("MT"+motorID+"-HSTP" + minSteps);
	}
	
	public void motorHome(int motorID) {
		MotorState state = proc.state().motor[motorID];
		state.target = Integer.MIN_VALUE;
		if(proc.config().saveState.autosave)
			proc.saveStateNow();
		
		proc.serialComm().send("MT"+motorID+"-HOME");
		proc.getConnectedSource().setSeriesMetaData("ArduinoComm/motor/"+motorID+"/lastTarget", Integer.MIN_VALUE, false);
		posLog(motorID, "GOHOME", 0);
	}
	
	public void motorSetEnable(int motorID, boolean enable) {
		proc.serialComm().send("MT"+motorID+"-ENBL" + (enable ? "1" : "0"));		
	}

	public void motorRequestPositions() {
		for(int motorID = 0; motorID < nMotors; motorID++)
			proc.serialComm().send("MT"+motorID+"-GPOS");
	}
	
	public void motorRequestPos(int motorID) {
		proc.serialComm().send("MT"+motorID+"-GPOS");
	}
	
	public void motorGotoPos(int motorID, int pos) {
		MotorState state = proc.state().motor[motorID];
		state.target = pos;
		if(proc.config().saveState.autosave)
			proc.saveStateNow();
		
		proc.serialComm().send("MT"+motorID+"-GOTO" + pos);
		proc.getConnectedSource().setSeriesMetaData("ArduinoComm/motor/"+motorID+"/lastTarget", pos, false);
		posLog(motorID, "GOTO", pos);
	}
	
	private void posLog(int motorID, String eventName, int pos) {
		String logFile = proc.config().motor[motorID].logFileName;
		if(logFile != null) {			
			try {
				FileWriter fw = new FileWriter(logFile, true);
				BufferedWriter bw = new BufferedWriter(fw);
			    bw.write(System.currentTimeMillis() + "\t" + eventName + "\t" + pos);
			    bw.newLine();
			    bw.close();
			} catch (IOException err) {
				Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Exception writing to motor log file " + logFile, err);
			}		    
		}		
	}

	public void motorSetSwitchPolarity(int motorID, boolean polarity) {		
		proc.serialComm().send("MT"+motorID+"-SSWT" + (polarity ? "1" : "0"));
	}

	public void motorSetDirection(int motorID, boolean reverseDir) {		
		proc.serialComm().send("MT"+motorID+"-RDIR" + (reverseDir ? "1" : "0"));
	}


	public int[] getMotorPoitions(){
		int pos[] = new int[nMotors];
		for(int i=0; i < nMotors; i++)
			pos[i] = proc.state().motor[i].pos;
		return pos;
	}
	
	public int getMotorPos(int motorID){ 
		return (motorID >= 0 && motorID < nMotors) 
				? proc.state().motor[motorID].pos
				: -1;
	}
	
	public boolean motorIsEnabled(int motorID) {
		return proc.state().motor[motorID].enabled;
	}
	
	public boolean getMotorSwitchPolarity(int motorID) {
		return proc.config().motor[motorID].switchPolarity;
	}
	
	public boolean getMotorDirection(int motorID) {
		return proc.config().motor[motorID].dir;
	}
	
	/** The period in microsecs of the motor used in the sequencer */
	public int getMotorPeriodUS(int motorID) {;
		return (motorID >= 0 && motorID < proc.config().motor.length) 
					? proc.config().motor[motorID].period
					: -1;
	}

	public MotorState getState(int motorID) { return proc.state().motor[motorID]; }

	public void motorGotoPreset(int motorID, String presetName) {
		double val = proc.config().motor[motorID].presets.get(presetName);
		proc.state().motor[motorID].targetPresetName = presetName;
		//proc.saveStateNow() will be done by motorGotoPos()		
		motorGotoPos(motorID, (int)val);
	}
	
}
