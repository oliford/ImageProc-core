package imageProc.control.arduinoComm;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.regex.Pattern;

import com.fazecast.jSerialComm.SerialPort;

import algorithmrepository.Algorithms;
import imageProc.core.ImageProcUtil;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class LoggedComm implements Runnable {
	
	public interface LogUser {

		/**Something specific for the Arduino */
		public void receivedLine(String lineStr);
		/** Called when the log has changed */
		public void logChanged();
		/** Called when the port has been connected*/
		public void connected();
		
	}
	private LogUser proc;
	
	private SerialPort serialPort = null;
	private Socket socket = null; 
	private InputStream inStream;
	private OutputStream outStream;
	private StringBuffer serialLog = new StringBuffer(1048576);
	
	private boolean death = false;
	private Thread thread;
	private byte[] lastBinaryDump;
	private int binaryDumpPos = -1;

	/** Send CR before LF */
	private boolean carriageReturn = true;
	
	private long lastLineTime = 0;
	private long minLineTime = 0;
	
	private final static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM HH:mm:ss");

	public LoggedComm(LogUser proc) {
		this.proc = proc;
	}
	
	@Override
	public void run() {
		StringBuffer lineBuild = new StringBuffer(128);
		
		try {
			while(!death){
				int byteRead;
				while(inStream.available() == 0){
					try{
						Thread.sleep(10);
					} catch (InterruptedException e) {
						if(death)break;
					}
				}
				if(death)break;
				byteRead = inStream.read();
					
				/*if(byteRead >= 0 && binaryDumpPos >= 0){
					lastBinaryDump[binaryDumpPos] = (byte)byteRead;
					binaryDumpPos++;
					if(binaryDumpPos >= lastBinaryDump.length){
						binaryDumpPos = -1;
						System.out.println("Serial: Binary dump received");
					}
					continue;
				}*/
					
				
				if(byteRead == 0x0A){
					serialLog.append(timestampFormat.format(new Date()));
					serialLog.append(" < ");
					serialLog.append(lineBuild);
					serialLog.append('\n');
					String lineStr = lineBuild.toString();
					if(lineStr.startsWith("BINDUMP")){
						int size = Algorithms.mustParseInt(lineStr.substring(7).trim());			
						lastBinaryDump = new byte[size];
						binaryDumpPos  = 0;
						if(serialPort != null)
							serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 5000, 0);
						
						while(binaryDumpPos < size){							
							int nRead = inStream.read(lastBinaryDump, binaryDumpPos, size-binaryDumpPos);
							if(nRead <= 0){
								System.out.println("Serial: ERROR: Binary dump read timed out");
								break;
							}
							binaryDumpPos += nRead;
						}				
						
						System.out.println("Serial: Received "+binaryDumpPos+" of binary dump len " + size);
					}else{
						proc.receivedLine(lineStr);
					}
					binaryDumpPos = 0;
					lineBuild.setLength(0);
				}else if(byteRead == 0x0D){
					// what?
				}else if(byteRead >= 0x20 && byteRead < 0x80){				
					lineBuild.append((char)byteRead);
				}else{
					lineBuild.append("{0x"+Integer.toHexString(byteRead)+"}");
				}
			}
			proc.logChanged();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	


	public void connectTCP(String address, int port) {
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doConnectTCP(address, port); } });
		
	}
	
	public void connectSerial(String portName, int baudRate){
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doConnectSerial(portName, baudRate ); } });
	}
	
	public void disconnect(){
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doDisconnect(); } });
	}
	

	private void doConnectSerial(String portName, int baudRate){
		doDisconnect();
		if(thread != null && thread.isAlive()){
			waitForThreadDeath();
		}
		
		try{
			serialLog.append("----- Connecting to port "+portName+" BAUD " + baudRate + " ----- \n");
			
			if(portName.startsWith("find:")) {
				Pattern p = Pattern.compile(portName.substring(5));
				
				// Enumeration
				SerialPort[] ports = SerialPort.getCommPorts();
				for(SerialPort port2 : ports) {
					System.out.println(port2.toString() + ": " + port2.getSystemPortPath());
					if(p.matcher(port2.getSystemPortPath()).matches()) {
						serialLog.append("Resolved to port " + port2.getSystemPortPath() + " (" + port2.toString() + ")\n");
						portName = port2.getSystemPortPath();
					}
				}
			}
			
			serialPort = SerialPort.getCommPort(portName);
			//port = ports[1];
			serialPort.setBaudRate(baudRate);
			serialPort.setComPortParameters(baudRate, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
			serialPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
			serialPort.openPort();
			
			inStream = serialPort.getInputStream();
			outStream = serialPort.getOutputStream();
			binaryDumpPos = -1;
			
			death = false;
			thread = new Thread(this);
			thread.start();
			

		}catch(Exception e) {
			e.printStackTrace();
			serialLog.append("| ERROR: " + e.getMessage() + '\n'); 
			serialPort = null;
		}

		try{
			proc.connected();
		}catch(Exception e) {
			e.printStackTrace();
			serialLog.append("| ERROR: " + e.getMessage() + '\n'); 
		}
		
		proc.logChanged();
		
	}
	
	public void doConnectTCP(String address, int port) {
		doDisconnect();
		if(thread != null && thread.isAlive()){
			waitForThreadDeath();
		}
		
		try{
			socket = new Socket(address, port);
			inStream = socket.getInputStream();
			outStream = socket.getOutputStream();
			
			death = false;
			thread = new Thread(this);
			thread.start();
			
		}catch(Exception e) {
			e.printStackTrace();
			serialLog.append("| ERROR: " + e.getMessage() + '\n'); 
			serialPort = null;
		}
		
		try{
			proc.connected();
		}catch(Exception e) {
			e.printStackTrace();
			serialLog.append("| ERROR: " + e.getMessage() + '\n'); 
		}
	}
	
	public StringBuffer getLog(){ return serialLog; }
	
	
	private void doDisconnect(){
		death = true;
		if(thread != null)
			thread.interrupt();
		
		if(serialPort != null){
			serialPort.closePort();
			serialPort = null;
			serialLog.append("----- Serial port closed -----\n");
			
		}else if(socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			serialLog.append("----- TCP connection closed -----\n");
		}
		proc.logChanged();
	}
	
	private void waitForThreadDeath(){
		serialLog.append("Waiting for thread to die");
		death = true;
		thread.interrupt();
		while(thread != null && thread.isAlive()){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) { }
		}
		serialLog.append("Thread died");		
		thread = null;
		proc.logChanged();
	}
	
	public void send(String sendData){
		send(sendData, true);
	}
	
	public void send(String sendData, boolean lineFeed){
		
		try {
			if(serialPort == null && socket == null){
				throw new RuntimeException("Not connected");
			}
			
			//outStream.write('\r'); //clear any previous rubbish
			//outStream.write('\n'); //clear any previous rubbish
			outStream.write(sendData.getBytes());
			if(lineFeed) {
				
				//delay if we are sending commands too fast
				long tSinceLast = (System.currentTimeMillis() - lastLineTime);
				if(lastLineTime > 0 && tSinceLast < minLineTime) {
					Thread.sleep(minLineTime - tSinceLast);
				}
				lastLineTime = System.currentTimeMillis();
				
				if(carriageReturn)
					outStream.write('\r');
				outStream.write('\n');
			}
			
			outStream.flush();
			
			serialLog.append(timestampFormat.format(new Date()));
			serialLog.append(" > ");
			serialLog.append(sendData);
			serialLog.append('\n');
			
		} catch (Exception e) {
			serialLog.append(">X ERROR: Could not send data '"+sendData+"', because: " + e.getMessage() + '\n');
			e.printStackTrace();
		}
		proc.logChanged();
	}
	

	public void sendByte(byte singleByte) {
		String hexStr = "0x" + Integer.toHexString(((int)singleByte) & 0x000000FF).toUpperCase();
		try {
			if(serialPort == null && socket == null){
				throw new RuntimeException("Not connected");
			}
			
			outStream.write(singleByte);
			
			serialLog.append(timestampFormat.format(new Date()));
			serialLog.append(" > [0x");
			serialLog.append(hexStr);
			serialLog.append("]\n");
			
		} catch (Exception e) {
			serialLog.append(">X ERROR: Could not send ["+hexStr+"], because: " + e.getMessage() + '\n');
			e.printStackTrace();
		}
		proc.logChanged();
	}

	public void destroy() {
		disconnect();
	}

	public String getStatus() {
		return "Port = " + (serialPort != null ? (serialPort.getDescriptivePortName() + " " + serialPort.getSystemPortPath()) : socket);
	}
	
	public boolean isConnected(){ return serialPort != null || socket != null; }

	public byte[] getLastBinaryDump() { return lastBinaryDump; }

	public void clear() {
		serialLog = new StringBuffer(1048576);
		proc.logChanged();
	}
	
	public void setCarriageReturn(boolean carriageReturn) {
		this.carriageReturn = carriageReturn;
	}
	
	public void setMinLineTime(long minLineTime) { this.minLineTime = minLineTime;	}
}
