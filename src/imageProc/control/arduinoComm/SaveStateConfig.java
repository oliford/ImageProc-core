package imageProc.control.arduinoComm;

public class SaveStateConfig {
	/** Path to save state to when it changes, (be it a file path or some kind of database path)  */
	public String path = "";
	
	/** Enable autosave on change */
	public boolean autosave = false;

}
