package imageProc.control.arduinoComm.swt;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;

import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.core.ImageProcUtil;
import imageProc.graph.JFreeChartGraph;
import imageProc.graph.Series;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class DataAqSWTGroup {
	private ArduinoCommHanger proc;
	
	private Group swtGroup;
	private Button initButton;
	private Button startButton;
	private Button continuousCheckbox;
	private Button abortButton;
	private Button closeButton;
	
	private JFreeChartGraph graph, graph2;
	
	public DataAqSWTGroup(Composite parent, ArduinoCommHanger proc) {
		this.proc = proc;
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("DataAq");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        
        graph = new JFreeChartGraph(swtGroup, SWT.NONE);
        graph.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		
        graph2 = new JFreeChartGraph(swtGroup, SWT.NONE);
        graph2.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		
    	initButton = new Button(swtGroup, SWT.PUSH);
    	initButton.setText("Init");
    	initButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
    	initButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { initButtonEvent(event); } });

    	continuousCheckbox = new Button(swtGroup, SWT.CHECK);
    	continuousCheckbox.setText("Continuous");
    	continuousCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
    	
    	startButton = new Button(swtGroup, SWT.PUSH);
    	startButton.setText("Start");
    	startButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
    	startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startButtonEvent(event); } });
		
    	abortButton = new Button(swtGroup, SWT.PUSH);
		abortButton.setText("Abort");
		abortButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		abortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortButtonEvent(event); } });
		
		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close");
		closeButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeButtonEvent(event); } });
		
		ImageProcUtil.addRevealWriggler(swtGroup);
	}
	
	private void initButtonEvent(Event e){
		proc.dataAqCtrl().init();
	}

	private void startButtonEvent(Event e){
		proc.dataAqCtrl().start(continuousCheckbox.getSelection());
	}

	private void abortButtonEvent(Event e){
		proc.dataAqCtrl().abort();
	}

	private void closeButtonEvent(Event e){
		proc.dataAqCtrl().close();
	}

	public void doUpdate() {
		short s[][] = proc.dataAqCtrl().getLastData();
		if(s==null)
			return;
		ArrayList<Series> seriesList = new ArrayList<Series>(s.length);
		
		for(int iC=0; iC < s.length; iC++){
			double x[] = new double[s[iC].length];
			double d[] = new double[s[iC].length];
			for(int iS=0; iS < s[iC].length; iS++){
				x[iS] = iS;
				d[iS] = s[iC][iS];
			}
			seriesList.add(new Series("ch" + iC, x, d));
		}
		
		graph.setData(seriesList);
		
		double d2[] = proc.dataAqCtrl().getMeasAng();
		if(d2 != null && d2.length >= 2){
			ArrayList<Series> seriesList2 = new ArrayList<Series>(s.length);
			double x2[] = Mat.linspace(0, d2.length-1, d2.length);
			seriesList2.add(new Series("measAngh", x2, d2));
			graph2.setData(seriesList2);
		}
		
		
	}

	public Group getSWTGroup(){ return swtGroup; }
}
