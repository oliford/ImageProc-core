package imageProc.control.arduinoComm.swt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.jcraft.jsch.ConfigRepository.Config;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.MotorConfig;
import imageProc.control.arduinoComm.MotorControl;
import imageProc.control.arduinoComm.swt.MotorPresetsTable.MotorPresetsHandler;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.EditableTable;
import imageProc.core.swt.EditableTable.EditType;
import imageProc.core.swt.EditableTable.TableModifyListener;
import imageProc.proc.softwareBinning.SoftwareROIsConfig;
import imageProc.proc.softwareBinning.SoftwareROIsConfig.SoftwareROI;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class MotorSWTGroup implements MotorPresetsHandler {
	
	private ArduinoCommHanger proc;
	private MotorControl ctrl;
	
	private Group swtGroup;
	private Button homeButton;
	private Text motorLogFileTextbox;
	private Spinner motorCurrentSpinner;
	private Spinner stepPeriodSpinner;
	private Spinner minStepsSpinner;
	private Button setCfgButton;
	private Button motorEnableCheckbox;
	private Button switchPolarityCheckbox;
	private Button motorDirCheckbox;
	private Spinner positionSpinner;
	private Button gotoPosButton;
	private Button getPosButton;
	
	private MotorPresetsTable presetsTable;
	
	private boolean dataChangeInhibit = false;
	
	private int motorID;
	
	
	public MotorSWTGroup(Composite parent, ArduinoCommHanger proc, int motorID) {
		this.proc = proc;
		this.ctrl = proc.getMotorCtrl();
		this.motorID = motorID;
		
		MotorConfig config = proc.config().motor[motorID];
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Motor "+motorID+" ("+config.name+")");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(4, false));

		motorEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		motorEnableCheckbox.setText("Enable");
		motorEnableCheckbox.setToolTipText("Enable the motor. Often shared between different motor/axes. Automatically turned off after some timeout");
		motorEnableCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		motorEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { motorEnableButtonEvent(event); } });
	
		switchPolarityCheckbox = new Button(swtGroup, SWT.CHECK);
		switchPolarityCheckbox.setText("Rev. Switch");
		switchPolarityCheckbox.setToolTipText("Sets whether the home switch is active high or active low");
		switchPolarityCheckbox.setSelection(ctrl.getMotorSwitchPolarity(motorID));
		switchPolarityCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		switchPolarityCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { switchPolarityEvent(event); } });
		
		motorDirCheckbox = new Button(swtGroup, SWT.CHECK);
		motorDirCheckbox.setText("Rev. Motor");
		motorDirCheckbox.setToolTipText("Sets the direction of +ve position");
		motorDirCheckbox.setSelection(ctrl.getMotorSwitchPolarity(motorID));
		motorDirCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		motorDirCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { motorDirEvent(event); } });

		Label lLF = new Label(swtGroup, SWT.NONE); lLF.setText("Log file:");
		motorLogFileTextbox = new Text(swtGroup, SWT.NONE);
		motorLogFileTextbox.setText("");
		motorLogFileTextbox.setToolTipText("Simple text log file for motor position, homing etc. Settings changes are not logged.");
		motorLogFileTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		motorLogFileTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lMC = new Label(swtGroup, SWT.NONE); lMC.setText("Current/mA:");
		motorCurrentSpinner = new Spinner(swtGroup, SWT.NONE);
		motorCurrentSpinner.setValues(ctrl.getMotorCurrentMilliAmp(motorID), 0, 5000, 0, 100, 1000);
		motorCurrentSpinner.setToolTipText("Motor max current. Only works if driver has VRef connected to a PWM. (i.e. not for the Arduino CNC shield)");
		motorCurrentSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		setCfgButton = new Button(swtGroup, SWT.PUSH);
		setCfgButton.setText("Set");
		setCfgButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 3));
		setCfgButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setMotorCfgButtonEvent(event); } });
        
		Label lSP = new Label(swtGroup, SWT.NONE); lSP.setText("Step Period [us]:");
		stepPeriodSpinner = new Spinner(swtGroup, SWT.NONE);
		stepPeriodSpinner.setValues(ctrl.getMotorPeriodUS(motorID), 0, 5000000, 0, 100, 1000);
		stepPeriodSpinner.setToolTipText("Delay period between steps in us");
		stepPeriodSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		Label lMS = new Label(swtGroup, SWT.NONE); lMS.setText("Home min steps:");
		minStepsSpinner = new Spinner(swtGroup, SWT.NONE);
		minStepsSpinner.setValues(ctrl.motorGetHomeMinSteps(motorID), 0, 1000, 0, 1, 10);
		minStepsSpinner.setToolTipText("Minimum number of steps that home switch needs to be active before home had been reached (swtich debouncing or noise mitigation)");
		minStepsSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Position:");
		positionSpinner = new Spinner(swtGroup, SWT.NONE);
		positionSpinner.setValues(500, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 100, 1000);
		positionSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		gotoPosButton = new Button(swtGroup, SWT.PUSH);
		gotoPosButton.setText("Goto");
		gotoPosButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		gotoPosButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { motorGotoButtonEvent(event); } });

		homeButton = new Button(swtGroup, SWT.PUSH);
		homeButton.setText("Home");
		homeButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		homeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { motorHomeButtonEvent(event); } });

		getPosButton = new Button(swtGroup, SWT.PUSH);
		getPosButton.setText("Get");
		getPosButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		getPosButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { motorGetPosButtonButtonEvent(event); } });
				
		presetsTable = new MotorPresetsTable(swtGroup, this);
		
		ImageProcUtil.addRevealWriggler(swtGroup);
	}

	protected void settingsChangedEvent(Event event) {
		if(dataChangeInhibit)
			return;
		
		dataChangeInhibit = true;		
		try {		
			MotorConfig config = proc.config().motor[motorID];
			
			config.logFileName = motorLogFileTextbox.getText().length() == 0 ? null : motorLogFileTextbox.getText();
			
		}finally{
			dataChangeInhibit = false;
		}		
	}

	private void setMotorCfgButtonEvent(Event event) {
		ctrl.motorSetCurrent(motorID, motorCurrentSpinner.getSelection()); // in mA 
		ctrl.motorSetStepPeriod(motorID, stepPeriodSpinner.getSelection()); //already in us
		ctrl.motorSetHomeMinSteps(motorID, minStepsSpinner.getSelection());
		ctrl.motorSetDirection(motorID, motorDirCheckbox.getSelection());
		ctrl.motorSetSwitchPolarity(motorID, switchPolarityCheckbox.getSelection());
	}

	private void motorHomeButtonEvent(Event event) {
		ctrl.motorHome(motorID);
	}
	
	private void motorEnableButtonEvent(Event event){
		if(dataChangeInhibit)return;
		
		ctrl.motorSetEnable(motorID, motorEnableCheckbox.getSelection());
		dataChangeInhibit = true;
		motorEnableCheckbox.setSelection(ctrl.motorIsEnabled(motorID));
		dataChangeInhibit = false;
	}

	private void switchPolarityEvent(Event event){
		if(dataChangeInhibit)return;
		
		ctrl.motorSetSwitchPolarity(motorID, switchPolarityCheckbox.getSelection());
		dataChangeInhibit = true;
		switchPolarityCheckbox.setSelection(ctrl.getMotorSwitchPolarity(motorID));
		dataChangeInhibit = false;
	}
	
	private void motorDirEvent(Event event){
		if(dataChangeInhibit)return;
		
		ctrl.motorSetDirection(motorID, motorDirCheckbox.getSelection());
		dataChangeInhibit = true;
		motorDirCheckbox.setSelection(ctrl.getMotorDirection(motorID));
		dataChangeInhibit = false;
	}
	
	private void motorPresetButtonEvent(Event event, int pos){
		if(pos == 0)
			ctrl.motorHome(motorID);
		else
			ctrl.motorGotoPos(motorID, pos);
	}

	private void motorGotoButtonEvent(Event event) {
		ctrl.motorGotoPos(motorID, positionSpinner.getSelection());
	}
	
	private void motorGetPosButtonButtonEvent(Event event) {
		ctrl.motorRequestPos(motorID);
	}
	
	public void doUpdate() {
		if(dataChangeInhibit)
			return;
		
		dataChangeInhibit = true;		
		try {
			MotorConfig config = proc.config().motor[motorID];
			
			motorEnableCheckbox.setSelection(ctrl.motorIsEnabled(motorID));
			if(!motorLogFileTextbox.isFocusControl())
				motorLogFileTextbox.setText(config.logFileName == null ? "" : config.logFileName);
			
			switchPolarityCheckbox.setSelection(config.switchPolarity);
			motorDirCheckbox.setSelection(config.dir);
			if(!stepPeriodSpinner.isFocusControl())
				stepPeriodSpinner.setSelection(config.period);
			if(!minStepsSpinner.isFocusControl())
				minStepsSpinner.setSelection(config.homeSwitchMinSteps); 
			if(!motorCurrentSpinner.isFocusControl())
				motorCurrentSpinner.setSelection(config.currentMilliAmp);  
			
		}finally{
			dataChangeInhibit = false;
		}
		presetsTable.presetsToGUI();
	}

	public int getStepPeriodUS() { return stepPeriodSpinner.getSelection(); }
	
	public Group getSWTGroup(){ return swtGroup; }

	@Override
	public HashMap<String, Double> getPresets() {
		MotorConfig config = proc.config().motor[motorID];
		if(config.presets == null) 
			config.presets = new HashMap<>();
		return config.presets;
	}

	@Override
	public void motorGotoPos(double presetVal) {
		proc.getMotorCtrl().motorGotoPos(motorID, (int)presetVal);
	}

	@Override
	public void motorGotoPreset(String presetName) {
		proc.getMotorCtrl().motorGotoPreset(motorID, presetName);		
	}

	@Override
	public void updateAllControllers() {
		proc.updateAllControllers();
	}
}
