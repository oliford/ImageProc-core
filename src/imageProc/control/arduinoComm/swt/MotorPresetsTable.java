package imageProc.control.arduinoComm.swt;

import java.util.HashMap;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import imageProc.control.arduinoComm.MotorConfig;
import imageProc.core.swt.EditableTable;
import imageProc.core.swt.EditableTable.EditType;
import imageProc.core.swt.EditableTable.TableModifyListener;

public class MotorPresetsTable {
	
	public interface MotorPresetsHandler {
		public HashMap<String, Double> getPresets();

		public void motorGotoPos(double presetVal);
		//proc.getMotorCtrl().motorGotoPos(motorID, presetVal);

		public void updateAllControllers();

		public void motorGotoPreset(String presetName);
	}
	
	MotorPresetsHandler handler;

	private EditableTable presetsTable;
	
	public MotorPresetsTable(Group swtGroup, MotorPresetsHandler handler) {
		this.handler = handler;
		
		Label lPS = new Label(swtGroup, SWT.NONE); lPS.setText("Presets:");
		presetsTable = new EditableTable(swtGroup, SWT.BORDER);				
		//featuresTable.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, true, 5, 0));
		presetsTable.setHeaderVisible(true);
		presetsTable.setLinesVisible(true);	
		
		final String[] colNames = { "-", "Name", "Value", "Goto" };
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(presetsTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]);
		}
		
		presetsToGUI();
		
		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		presetsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		presetsTable.setTableModifyListener(new TableModifyListener() { @Override 
			public void tableModified(TableItem item, int column, String text) { tableColumnModified(item, column, text); }
		});
		
		presetsTable.setEditTypeAll(EditType.EditBox);
		presetsTable.setEditType(3, EditType.Click);
	}
	
	private void tableColumnModified(TableItem item, int column, String text) {

		HashMap<String, Double> presets = handler.getPresets(); 
		
		
		if(item == null || column < 0) { //just a general update
			handler.updateAllControllers();
			return;
		}
		
		synchronized (presets) {
			if (item.isDisposed())
				return;

			String presetName = item.getText(1);
			Double presetVal = (presetName.length() > 0) ? presets.get(presetName) : null;
			
			if(presetVal == null && column != 1){
				//somethings broken. Queue an update
				handler.updateAllControllers();
				return;
			}
			
			switch (column) {
				case 1: //name
	
					if (presetVal == null && (presetName.length() <= 0 || presetName.equals("<new>"))) {
						presets.put(text, 0.0);
						
					} else if (text.length() <= 0) { // deleting point
						presets.remove(presetName);
						item.dispose();
						return;	
					} else { // just changing name
						presets.remove(presetName);
						presets.put(text, presetVal);
					}
	
					item.setText(column, text);
	
					break;
				case 2:
					//change value
					presets.put(presetName, Double.parseDouble(text));
					item.setText(column, text);
					break;
					
				case 3:
					//goto
					handler.motorGotoPreset(presetName);
										
					break;
					
			}

			TableItem lastItem = presetsTable.getItem(presetsTable.getItemCount() - 1);
			if (!lastItem.getText(1).equals("<new>")) {
				TableItem blankItem = new TableItem(presetsTable, SWT.NONE);
				blankItem.setText(0, "o");
				blankItem.setText(1, "<new>");
			}

		}
	}
	
	public void presetsToGUI(){
		if(presetsTable.isFocusControl() || presetsTable.isEditing())
			return;
		
		HashMap<String, Double> presets = handler.getPresets(); 
		
		presetsTable.removeAll();
		for(Entry<String,Double> entry : presets.entrySet()) {
			if(entry.getKey().length() <= 0)
				continue;
			TableItem item = new TableItem(presetsTable, SWT.NONE);
			item.setText(0, "o");
			item.setText(1, entry.getKey());
			Number v = entry.getValue();
			item.setText(2, Double.toString((Double)v));
			item.setText(3, "O");
		}
		/* the proper code for stable refresh:
		
		ArrayList<SoftwareROI> roiList;
		synchronized (config) {
			roiList = config.getROIListCopy();
		}

		int selected[] = table.getSelectionIndices();

		// use existing table entries, so it doesnt scroll around
		for (int i = 0; i < roiList.size(); i++) {
			SoftwareROI roi = roiList.get(i);

			if (roi.name.length() <= 0)
				continue;

			TableItem item;
			if (i < table.getItemCount())
				item = table.getItem(i);
			else
				item = new TableItem(table, SWT.NONE);

			item.setText(0, "o");
			item.setText(1, roi.name);
			item.setText(2, roi.enabled ? "Y" : "");

			item.setText(3, Integer.toString(roi.x));
			item.setText(4, Integer.toString(roi.width));
			item.setText(5, Integer.toString(roi.x_binning));

			item.setText(6, Integer.toString(roi.y));
			item.setText(7, Integer.toString(roi.height));
			item.setText(8, Integer.toString(roi.y_binning));
			
			item.setText(9, Double.toString(roi.tilt));
			
			item.setText(10, (roi.deadColumns == null) ? "" : roi.deadColumns.toString());

		}

		while (table.getItemCount() > roiList.size()) {
			table.remove(roiList.size());
		}
		 */
		
		// and finally a blank item for creating new point
		TableItem blankItem = new TableItem(presetsTable, SWT.NONE);
		blankItem.setText(0, "o");
		blankItem.setText(1, "<new>");
		

	}
}
