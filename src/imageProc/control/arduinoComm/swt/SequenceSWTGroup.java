package imageProc.control.arduinoComm.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.SequenceConfig;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.sources.capture.andorCam.AndorCamConfig;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.sensicam.SensicamConfig;
import imageProc.sources.capture.sensicam.SensicamSource;

public class SequenceSWTGroup {
	private ArduinoCommHanger proc;
	
	private Group swtGroup;

	private Label statusLabel;
	private Label timingLabel;
	private Spinner numSetsSpinner;
	private Spinner stepsPerSetSpinner;
	private Spinner motorIDSpinner;	
	private Combo flcStateCombo;
	private Spinner startDelaySpinner;
	private Spinner clockPeriodSpinner;
	private Spinner exposureTimeSpinner;
	private Spinner postExpTimeSpinner;	
	private Combo advanceTriggerCombo;
	private Button softTriggeringCheckbox;	
	private Spinner restartTimeSpinner;
	private Button startButton;	
	private Button syncCamButton;	
	private Button abortButton;	
	private Button downloadButton;	
	private Button advanceButton;
	private Button daqOnAdvanceCheckbox;
	private Spinner recordMotorIDSpinner;
	private Spinner recordStepSpinner;
		
	public SequenceSWTGroup(Composite parent, ArduinoCommHanger proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Sequence");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(4, false));
		
		Label lSL = new Label(swtGroup, SWT.NONE); lSL.setText("Status:");
		statusLabel = new Label(swtGroup, SWT.NONE);
		statusLabel.setText("init");
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));

		Label lTL = new Label(swtGroup, SWT.NONE); lTL.setText("Timing:");
		timingLabel = new Label(swtGroup, SWT.NONE);
		timingLabel.setText("init\n\n");
		timingLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));

		Label lNF = new Label(swtGroup, SWT.NONE); lNF.setText("Num Sets:");
		numSetsSpinner = new Spinner(swtGroup, SWT.NONE);
		numSetsSpinner.setValues(480, 0, Integer.MAX_VALUE, 0, 100, 1000);
		numSetsSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		numSetsSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });
		
		Label lNS = new Label(swtGroup, SWT.NONE); lNS.setText("Num Steps/Set:");
		stepsPerSetSpinner = new Spinner(swtGroup, SWT.NONE);
		stepsPerSetSpinner.setValues(Integer.MIN_VALUE, -1, Integer.MAX_VALUE, 0, 1, 10);
		stepsPerSetSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		stepsPerSetSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });
		
		Label lMI = new Label(swtGroup, SWT.NONE); lMI.setText("Motor:");
		motorIDSpinner = new Spinner(swtGroup, SWT.NONE);
		motorIDSpinner.setValues(-1, -1, 1, 0, 1, 1);
		motorIDSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		motorIDSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });
		
		Label lFS = new Label(swtGroup, SWT.NONE); lFS.setText("FLC:");
		flcStateCombo = new Combo(swtGroup, SWT.MULTI | SWT.DROP_DOWN | SWT.READ_ONLY);
		flcStateCombo.setItems(new String[]{ "Off", "On", "Interlaced" });
		flcStateCombo.select(0);
		flcStateCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		flcStateCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });
		
		Label lLT = new Label(swtGroup, SWT.NONE); lLT.setText("Advance:");
		advanceTriggerCombo = new Combo(swtGroup, SWT.MULTI | SWT.DROP_DOWN | SWT.READ_ONLY);
		advanceTriggerCombo.setItems(new String[]{ "None", "Fast Advance", "Exposure Start", "Exposure End", "Self Timed" });
		advanceTriggerCombo.select(3);
		advanceTriggerCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		advanceTriggerCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });
				
		Label lSD = new Label(swtGroup, SWT.NONE); lSD.setText("Start Delay (DAC) / ms:");
		startDelaySpinner = new Spinner(swtGroup, SWT.NONE);
		startDelaySpinner.setValues(0, 0, Integer.MAX_VALUE, 0, 1, 100);
		startDelaySpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		startDelaySpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });

		Label lCP = new Label(swtGroup, SWT.NONE); lCP.setText("Clock period / ns:");
		clockPeriodSpinner = new Spinner(swtGroup, SWT.NONE);
		clockPeriodSpinner.setValues(20000, 0, Integer.MAX_VALUE, 0, 100, 1000);
		clockPeriodSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		clockPeriodSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });

		Label lET = new Label(swtGroup, SWT.NONE); lET.setText("Exposure Time / µs:");
		exposureTimeSpinner = new Spinner(swtGroup, SWT.NONE);
		exposureTimeSpinner.setValues(3000, 0, Integer.MAX_VALUE, 0, 1000, 10000);
		exposureTimeSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		exposureTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });

		Label lPT = new Label(swtGroup, SWT.NONE); lPT.setText("Cam Min Post Exp / µs:");
		postExpTimeSpinner = new Spinner(swtGroup, SWT.NONE);
		postExpTimeSpinner.setValues(16000, 0, Integer.MAX_VALUE, 0, 1000, 10000);
		postExpTimeSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		postExpTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });
		
		syncCamButton = new Button(swtGroup, SWT.PUSH);
		syncCamButton.setText("Get camera timing");
		syncCamButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		syncCamButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { syncCamButtonEvent(event); } });
		
		softTriggeringCheckbox = new Button(swtGroup, SWT.CHECK);
		softTriggeringCheckbox.setText("S/W Start and S/W abort at end");
		softTriggeringCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		softTriggeringCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { softTriggeringEvent(event); } });
				
		Label lRT = new Label(swtGroup, SWT.NONE); lRT.setText("Restart time / ms:");
		restartTimeSpinner = new Spinner(swtGroup, SWT.NONE);
		restartTimeSpinner.setValues(0, 0, Integer.MAX_VALUE, 0, 1000, 10000);
		restartTimeSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		startButton = new Button(swtGroup, SWT.PUSH);
		startButton.setText("Start");
		startButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startButtonEvent(event); } });
		
		advanceButton = new Button(swtGroup, SWT.PUSH);
		advanceButton.setText("Manual Advance");
		advanceButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		advanceButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { advanceButtonEvent(event); } });

		downloadButton = new Button(swtGroup, SWT.PUSH);
		downloadButton.setText("Download Record");
		downloadButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		downloadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { downloadButtonEvent(event); } });
		
		abortButton = new Button(swtGroup, SWT.PUSH);
		abortButton.setText("Abort");
		abortButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		abortButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortButtonEvent(event); } });

		daqOnAdvanceCheckbox = new Button(swtGroup, SWT.CHECK);
		daqOnAdvanceCheckbox.setText("Arduino DAQ on advance");
		daqOnAdvanceCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		daqOnAdvanceCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sequenceTimingEvent(event); } });
				 
		Label lRM = new Label(swtGroup, SWT.NONE); lRM.setText("Record motor: ");
		recordMotorIDSpinner = new Spinner(swtGroup, SWT.NONE);
		recordMotorIDSpinner.setValues(-1, 0, Integer.MAX_VALUE, 0, 1, 1);
		recordMotorIDSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lRS2 = new Label(swtGroup, SWT.NONE); lRS2.setText("Every n'th stepL");
		recordStepSpinner = new Spinner(swtGroup, SWT.NONE);
		recordStepSpinner.setValues(-1, 0, Integer.MAX_VALUE, 0, 1, 1);
		recordStepSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
	}

	public void startButtonEvent(Event event){		
		proc.seqCtrl().initSequence(guiToConfig());
	}
	
	public void syncCamButtonEvent(Event event){				
		//find the root image source
		ImgSource rootSrc = proc.getConnectedSource();
		while(rootSrc instanceof ImgSink && ((ImgSink)rootSrc).getConnectedSource() != null)
			rootSrc = ((ImgSink)rootSrc).getConnectedSource();
		
		if(rootSrc instanceof SensicamSource){			
			SensicamSource cam = (SensicamSource)rootSrc;
			
			SensicamConfig camCfg = cam.getConfig();
			exposureTimeSpinner.setSelection(camCfg.exposureMS * 1000);
			postExpTimeSpinner.setSelection((int)(cam.getLastPostExpTimeMS() * 1000.0));
			
		}else if(rootSrc instanceof AndorCamSource){
			AndorCamSource cam = (AndorCamSource)rootSrc;
			
			AndorCamConfig camCfg = cam.getConfig();
			exposureTimeSpinner.setSelection((int)(camCfg.exposureTimeMS() * 1000.0));
			postExpTimeSpinner.setSelection((int)((camCfg.frameTimeMS() - camCfg.exposureTimeMS()) * 1000.0));
		}
		
		sequenceTimingEvent(null);
	}
	
	public void softTriggeringEvent(Event event){
		//if turning soft triggering on, set the config 		
		if(softTriggeringCheckbox.getSelection())
			proc.seqCtrl().armSoftTrigger(true, guiToConfig());
		else
			proc.seqCtrl().armSoftTrigger(false, null);
	}
	
	private SequenceConfig guiToConfig() {
		SequenceConfig config = new SequenceConfig();
		
		config.nFrames = numSetsSpinner.getSelection();
		config.flcMode = flcStateCombo.getSelectionIndex();
		config.nStepsPerFrame = stepsPerSetSpinner.getSelection();
		config.lineTrigger = advanceTriggerCombo.getSelectionIndex();
		config.clockPeriodNS = clockPeriodSpinner.getSelection();
		config.exposureTimeUS = exposureTimeSpinner.getSelection();
		config.postExposureTimeUS = postExpTimeSpinner.getSelection();
		
		//take step config from motor section 
		config.motorID = motorIDSpinner.getSelection();
		config.stepPeriodUS = proc.getMotorCtrl().getMotorPeriodUS(config.motorID); 
		config.startDelayUS = startDelaySpinner.getSelection()*1000;
		
		config.abortTrigAtEnd = softTriggeringCheckbox.getSelection();		
		config.restartPeriodMS = restartTimeSpinner.getSelection();
		
		config.daqOnAdvance = daqOnAdvanceCheckbox.getSelection();
		config.recordMotorID = recordMotorIDSpinner.getSelection();
		config.recordStep = recordStepSpinner.getSelection();
		
		proc.seqCtrl().calcSequence(config);
		
		return config;
	}

	public void abortButtonEvent(Event event){
		proc.seqCtrl().fastAbortSequence();
	}

	public void downloadButtonEvent(Event event){
		proc.seqCtrl().requestSequenceRecord();
	}

	public void advanceButtonEvent(Event event){
		proc.seqCtrl().fastAdvanceSequence();
	}
	
	public void sequenceTimingEvent(Event event){
		SequenceConfig config = guiToConfig();
		double tStepsUS = (config.steppingTimeCLKs * config.clockPeriodNS/1e3);
		timingLabel.setText("Steps " + config.startPos + " - " + config.endPos + ", nExp = " + (config.exposuresPerFrame * config.nFrames) + 
				"\ntSteps = " + tStepsUS/1e3 +
				"ms, tSet: (" + ((config.exposuresPerFrame > 1) ? (config.exposureTimeUS + (config.postExposure1CLKs*config.clockPeriodNS/1000.0))/1000.0 : 0) +
				" + " + (config.exposureTimeUS + (config.postExposure2CLKs*config.clockPeriodNS/1000.0))/1000.0
				+") =" + (config.frameTimeUS/1000.0) + "ms,\n tAll = " + (config.totalTimeUS/1.0e6) + "s.");
		
		postExpTimeSpinner.setForeground( swtGroup.getDisplay().getSystemColor((tStepsUS > config.postExposureTimeUS) ? SWT.COLOR_RED : SWT.COLOR_BLUE));
	}
	
	public void doUpdate() {
		statusLabel.setText(proc.seqCtrl().getStatusString());		
	}

	public Group getSWTGroup(){ return swtGroup; }
}
