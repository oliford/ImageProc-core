package imageProc.control.arduinoComm.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.Algorithms;
import imageProc.control.arduinoComm.ArduinoCommConfig;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.core.ImageProcUtil;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SerialSWTGroup {
	public static final long KEEPALIVE_TIME_MS = 60000;
	private ArduinoCommHanger proc;

	private Group swtGroup;
	private Label statusLabel;
	private Combo portDeviceTextbox;
	private Combo baudRateTextbox;
	private Button connectButton;
	private Button disconnectButton;	
	private Text sendTextbox;
	private Button sendButton;
	private Button pingCheckbox;
	private Button clearButton;
	private Spinner watchdogTimeoutSpinner;
	private Label helpLabel;
	private Text lineDelayTextbox;
	
	
	public SerialSWTGroup(Composite parent, ArduinoCommHanger proc) {
		this.proc = proc;
		
		ArduinoCommConfig config = proc.config();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Comms");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        swtGroup.setLayout(new GridLayout(4, false));
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
	    statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
       
        Label lPN = new Label(swtGroup, SWT.NONE); lPN.setText("Device:");
	    portDeviceTextbox = new Combo(swtGroup, SWT.MULTI);
	    portDeviceTextbox.setItems(new String[]{ config.portName == null ? "" : config.portName, "/dev/ttyS0", "/dev/ttyACM0" });
	    portDeviceTextbox.select(0);
	    portDeviceTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
	    portDeviceTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    portDeviceTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

	    connectButton = new Button(swtGroup, SWT.PUSH);
	    connectButton.setText("Connect");
	    connectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    connectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { connectButtonEvent(event); } });

	    Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Baud:");
	    baudRateTextbox = new Combo(swtGroup, SWT.MULTI);
	    baudRateTextbox.setItems(new String[]{ Integer.toString(config.baudRate),  "115200", "9600" });
	    baudRateTextbox.select((config.baudRate == 9600) ? 1 : 0);
	    baudRateTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
	    baudRateTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    baudRateTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    	    
	    disconnectButton = new Button(swtGroup, SWT.PUSH);
	    disconnectButton.setText("Disconnect");
	    disconnectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    disconnectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { disconnectButtonEvent(event); } });
	    

	    Label lD = new Label(swtGroup, SWT.NONE); lD.setText("Delay:");
	    lineDelayTextbox = new Text(swtGroup, SWT.NONE);
	    lineDelayTextbox.setText("0");
	    lineDelayTextbox.setToolTipText("Minimum time between successive lines/commands sent");
	    lineDelayTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 3, 1));
	    lineDelayTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    

	    Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Send:");
	        
	    sendTextbox = new Text(swtGroup, SWT.NONE );
	    sendTextbox.setText("");
	    sendTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
	    
	    sendButton = new Button(swtGroup, SWT.PUSH);
	    sendButton.setText("Send");
	    sendButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    sendButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sendButtonEvent(event); } });

	    Label lWD = new Label(swtGroup, SWT.NONE); lWD.setText("Timeout/min: ");
	    watchdogTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
	    watchdogTimeoutSpinner.setValues(60, -1, Integer.MAX_VALUE, 0, 1, 10);
	    watchdogTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
	    watchdogTimeoutSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { watchdogSpinnerEvent(event); } });

	    pingCheckbox = new Button(swtGroup, SWT.CHECK);
	    pingCheckbox.setText("Ping");
	    pingCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    pingCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pingCheckboxEvent(event); } });

	    helpLabel = new Label(swtGroup, SWT.NONE);
	    helpLabel.setText("Commands not in GUI:\n"
	    		+ "\tSEQ-SCAN{num rows},{steps per row},{motor ID of Y}\t- 2D scan using both motors.\n"
	    		+ "\tMTx-TOUT{timeout millisecs}\t-Motor timeout");
	    helpLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		   
	    clearButton = new Button(swtGroup, SWT.PUSH);
	    clearButton.setText("Clear");
	    clearButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    clearButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { clearButtonEvent(event); } });

	    ImageProcUtil.addRevealWriggler(swtGroup);
}
	

	protected void settingsChangedEvent(Event event) {
		ArduinoCommConfig config = proc.config();
		config.portName = portDeviceTextbox.getText();
		config.baudRate = Algorithms.mustParseInt(baudRateTextbox.getText());
		config.minCommandTime = (long)Algorithms.mustParseLong(lineDelayTextbox.getText());
		proc.serialComm().setMinLineTime(config.minCommandTime);
	}


	private void connectButtonEvent(Event event) {
		ArduinoCommConfig config = proc.config();
		config.portName = portDeviceTextbox.getText();
		config.baudRate = Algorithms.mustParseInt(baudRateTextbox.getText());
		config.minCommandTime = (long)Algorithms.mustParseLong(lineDelayTextbox.getText());
		proc.serialComm().connectSerial(config.portName, config.baudRate);
		proc.serialComm().setMinLineTime(config.minCommandTime);
	}


	private void disconnectButtonEvent(Event event) {
		proc.serialComm().disconnect();
	}
	
	private void sendButtonEvent(Event event) {
		proc.serialComm().send(sendTextbox.getText());
		sendTextbox.setText("");
	}
	
	private void clearButtonEvent(Event event) {
		proc.serialComm().clear();
		sendTextbox.setText("");
	}
	
	private void pingCheckboxEvent(Event event) {		
		proc.setKeepAliveTimeMS(pingCheckbox.getSelection() ? KEEPALIVE_TIME_MS : -1);		
	}
	
	private void watchdogSpinnerEvent(Event event) {		
		proc.setWatchdogTimeout(watchdogTimeoutSpinner.getSelection() * 60000);		
	}
	

	public void doUpdate() {
		statusLabel.setText(proc.serialComm().getStatus() 
				+ (proc.isRemoteActive() ? ", ** REMOTE ACTIVE **" : ", Remote not active"));
		
		ArduinoCommConfig config = proc.config();
		
		portDeviceTextbox.setText(config.portName == null ? "" : config.portName);
		baudRateTextbox.setText(Integer.toString(config.baudRate));
		lineDelayTextbox.setText(Long.toString(config.minCommandTime));
	}

	public Group getSWTGroup(){ return swtGroup; }
}
