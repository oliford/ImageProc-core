package imageProc.control.arduinoComm.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.MotorControl;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;

public class ArduinoCommSWTControl implements ImagePipeController, ImagePipeSWTController {
	protected ArduinoCommHanger proc;
	
	protected Group swtGroup;
	protected CTabItem thisTabItem;
	
	private CTabItem[] swtMotorTabs;	
	private MotorSWTGroup[] motorGroups;
	
	private CTabItem swtSequenceTab;
	private SequenceSWTGroup sequenceGroup;
	
	protected SashForm swtSashForm;
		
	protected CTabFolder swtTabFoler;	
	protected CTabItem swtSerialTab;

	protected CTabItem swtDataAqTab;
	protected CTabItem swtSimpleIOTab;
		
	protected Text serialLogTextbox;
	
	protected SerialSWTGroup serialGroup;
	protected DataAqSWTGroup dataAqGroup;
	protected SimpleIOSWTGroup simpleIOGroup;

	private CTabItem swtSaveStateTab;	
	private SaveStateSWTGroup saveStateGroup;
	
	private SWTSettingsControl settingsCtrl;
	
	public ArduinoCommSWTControl(ArduinoCommHanger proc, Composite parent, int style) {
			this.proc = proc;
			
			swtGroup = new Group(parent, style);
			swtGroup.setText("Arduino Comm");
			swtGroup.setLayout(new FillLayout());
			
			swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
			
	        serialLogTextbox = new Text(swtSashForm, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
		    serialLogTextbox.setText(proc.serialComm().getLog().toString());
		    Composite swtBottomComp = new Composite(swtSashForm, SWT.NONE);
		    swtBottomComp.setLayout(new GridLayout(1, false));
			swtTabFoler = new CTabFolder(swtBottomComp, SWT.BORDER);
			swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	        
				        
			serialGroup = new SerialSWTGroup(swtTabFoler, proc);
			swtSerialTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtSerialTab.setControl(serialGroup.getSWTGroup());
			swtSerialTab.setText("Comms");
	        
			dataAqGroup = null;//new DataAqSWTGroup(swtTabFoler, proc);
			simpleIOGroup = new SimpleIOSWTGroup(swtTabFoler, proc);
			sequenceGroup = new SequenceSWTGroup(swtTabFoler, proc);
			
			motorGroups = new MotorSWTGroup[MotorControl.nMotors];
			for(int i=0; i < MotorControl.nMotors; i++) {
				motorGroups[i] = new MotorSWTGroup(swtTabFoler, proc, i);
				motorGroups[i].getSWTGroup().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));				
			}
			
			setupTabs();
			
			saveStateGroup = new SaveStateSWTGroup(swtTabFoler, proc);
			swtSaveStateTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtSaveStateTab.setControl(saveStateGroup.getSWTGroup());
			swtSaveStateTab.setText("Save State");
			
			settingsCtrl = new JSONFileSettingsControl();
			settingsCtrl.buildControl(swtBottomComp, SWT.BORDER, proc);
			settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			
			generalControllerUpdate();			
	}
	
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	protected void doUpdate(){
		if(swtGroup.isDisposed())
			return;
		
		String str = proc.serialComm().getLog().toString();
		int startPos = serialLogTextbox.getCharCount();
		if(str.length() < startPos){
			serialLogTextbox.setText(str);
		}else{
			String appendString = str.substring(startPos);
			serialLogTextbox.append(appendString);
		}
		
		settingsCtrl.doUpdate();
		
		Object folder = swtGroup.getParent();
		String type = proc.getTypeName();
		swtGroup.setText("Arduino("+type+")");
		thisTabItem.setText("Arduino("+type+")");

		setupTabs();
		
		serialGroup.doUpdate();
		if(dataAqGroup != null)
			dataAqGroup.doUpdate();	
		simpleIOGroup.doUpdate();
		
		if(motorGroups != null) {
			for(int i=0; i < motorGroups.length; i++) {
				motorGroups[i].doUpdate();
				swtMotorTabs[i].setText("M"+i+" (" + proc.config().motor[i].name + ")");
			}
		}
				
		sequenceGroup.doUpdate();	
		
		saveStateGroup.doUpdate();
	}
	
	protected  void setupTabs(){
		
		if(swtDataAqTab == null && proc.config().enableDAQ){
			if(dataAqGroup == null)
				dataAqGroup = new DataAqSWTGroup(swtTabFoler, proc);
			
			swtDataAqTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtDataAqTab.setControl(dataAqGroup.getSWTGroup());
			swtDataAqTab.setText("DAQ");
		}

		if(swtSimpleIOTab == null){
			swtSimpleIOTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtSimpleIOTab.setControl(simpleIOGroup.getSWTGroup());
			swtSimpleIOTab.setText("SimpleIO");
		}
		
		if(swtMotorTabs == null && proc.hasMotors()){
			swtMotorTabs = new CTabItem[MotorControl.nMotors];
			for(int i=0; i < MotorControl.nMotors; i++) {
				swtMotorTabs[i] = new CTabItem(swtTabFoler, SWT.NONE);
				swtMotorTabs[i].setControl(motorGroups[i].getSWTGroup());
				swtMotorTabs[i].setText("M"+i+" (" + proc.config().motor[i].name + ")");
			}
		}else if(swtMotorTabs != null && !proc.hasMotors()){
			for(int i=0; i < swtMotorTabs.length; i++) {
				if(swtMotorTabs[i] != null)
					swtMotorTabs[i].dispose();
			}
			swtMotorTabs = null;
		}
		
		if(swtSequenceTab == null && proc.hasMotors()){
			swtSequenceTab = new CTabItem(swtTabFoler, SWT.NONE);
			swtSequenceTab.setControl(sequenceGroup.getSWTGroup());
			swtSequenceTab.setText("Sequence");
		}else if(swtSequenceTab != null && !proc.hasMotors()){
			swtSequenceTab.dispose();
			swtSequenceTab = null;
		}
	}

	@Override
	public void destroy() {
		proc.destroy();
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc;	}

	@Override
	public Object getInterfacingObject() { return swtGroup; }

	//public MotorSWTGroup getMotorSWTControl() { return motorGroup; }

	@Override
	public void setTabItem(CTabItem tabItem){
		this.thisTabItem = tabItem;
	}	
	
}
