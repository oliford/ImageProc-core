package imageProc.control.arduinoComm.swt;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.core.ImageProcUtil;

public class SimpleIOSWTGroup {
	private ArduinoCommHanger proc;
	
	private Group swtGroup;
	
	private Spinner digitalPinSpinner[];
	private Button digitalPinIsOutput[];
	private Button digitalPinState[];
	
	private Spinner analogPinSpinner[];
	private Button analogPinIsOutput[];
	private Spinner analogPinValue[];
	
	private Button readAllButton;
	
	private boolean inhibitListeners = false;
	
	private int nDigital = 8, nAnalog = 8;
	
	public SimpleIOSWTGroup(Composite parent, ArduinoCommHanger proc) {
		this.proc = proc;
				
		swtGroup = new Group(parent, SWT.NONE);
		swtGroup.setText("Simple I/O");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(2, false));


		readAllButton = new Button(swtGroup, SWT.PUSH);
		readAllButton.setText("Read All");
		readAllButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1));
		readAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { readAll(); } });
			

		Group swtDigitalGroup = new Group(swtGroup, SWT.BORDER);
		swtDigitalGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 1, 1));
		swtDigitalGroup.setLayout(new GridLayout(4, false));
		
		digitalPinSpinner = new Spinner[nDigital];
		digitalPinIsOutput = new Button[nDigital];
		digitalPinState = new Button[nDigital];
		for(int i=0; i < nDigital; i++){
			final int idx = i;
			
			Label lD = new Label(swtDigitalGroup, SWT.NONE); lD.setText("Digital ");
			digitalPinSpinner[i] = new Spinner(swtDigitalGroup, SWT.NONE);
			digitalPinSpinner[i].setValues(-1, -1, 255, 0, 1, 10);
			digitalPinSpinner[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			digitalPinSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { checkEnables(); } });
			
			digitalPinIsOutput[i] = new Button(swtDigitalGroup, SWT.CHECK);
			digitalPinIsOutput[i].setText("Output");
			digitalPinIsOutput[i].setSelection(false);;
			digitalPinIsOutput[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			digitalPinIsOutput[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { checkEnables(); } });
			
			digitalPinState[i] = new Button(swtDigitalGroup, SWT.CHECK);
			digitalPinState[i].setText("Active");
			digitalPinState[i].setSelection(false);;
			digitalPinState[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			digitalPinState[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setDigitalState(idx); } });
			
		}

		Group swtAnalogGroup = new Group(swtGroup, SWT.BORDER);
		swtAnalogGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 1, 1));
		swtAnalogGroup.setLayout(new GridLayout(4, false));

		analogPinSpinner = new Spinner[nDigital];
		analogPinIsOutput = new Button[nDigital];
		analogPinValue = new Spinner[nDigital];
		for(int i=0; i < nDigital; i++){
			final int idx = i;
			
			Label lD = new Label(swtAnalogGroup, SWT.NONE); lD.setText("Analog ");
			analogPinSpinner[i] = new Spinner(swtAnalogGroup, SWT.NONE);
			analogPinSpinner[i].setValues(-1, -1, 255, 0, 1, 10);
			analogPinSpinner[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			analogPinSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { checkEnables(); } });
			
			analogPinIsOutput[i] = new Button(swtAnalogGroup, SWT.CHECK);
			analogPinIsOutput[i].setText("Output");
			analogPinIsOutput[i].setSelection(false);;
			analogPinIsOutput[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			analogPinIsOutput[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { checkEnables(); } });
			
			analogPinValue[i] = new Spinner(swtAnalogGroup, SWT.NONE);
			analogPinValue[i].setValues(0, 0, 255, 0, 1, 10);
			analogPinValue[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			analogPinValue[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setAnalogValue(idx); } });
			
		}
		
		ImageProcUtil.addRevealWriggler(swtGroup);
		doUpdate();
	}
	

	private void setAnalogValue(int i){
		if(analogPinIsOutput[i].getSelection())
			proc.simpleIOCtrl().setAnalogOut(analogPinSpinner[i].getSelection(), analogPinValue[i].getSelection());
	}
			
	private void setDigitalState(int i){
		if(digitalPinIsOutput[i].getSelection())
			proc.simpleIOCtrl().setDigitalOut(digitalPinSpinner[i].getSelection(), digitalPinState[i].getSelection());
	}
	
	
	public void doUpdate() {
		inhibitListeners = true;
		
		int digitalVals[] = proc.simpleIOCtrl().getDigitalValues();		
		for(int i=0; i < digitalPinSpinner.length; i++){
			if(!digitalPinIsOutput[i].getSelection()){
				int pinNum = digitalPinSpinner[i].getSelection();
				if(pinNum >= 0 && pinNum < digitalVals.length)
					digitalPinState[i].setSelection(digitalVals[pinNum] != 0);
				
			}
		}
		
		int analogVals[] = proc.simpleIOCtrl().getAnalogValues();		
		for(int i=0; i < analogPinSpinner.length; i++){
			if(!analogPinIsOutput[i].getSelection()){
				int pinNum = analogPinSpinner[i].getSelection();
				if(pinNum >= 0 && pinNum < analogVals.length)
					analogPinValue[i].setSelection(analogVals[pinNum]);
				
			}		
		}
		
		checkEnables();
		inhibitListeners = false;		
	}
	
	private void readAll(){
		for(int i=0; i < digitalPinSpinner.length; i++){
			if(!digitalPinIsOutput[i].getSelection()){
				int pinNum = digitalPinSpinner[i].getSelection();
				if(pinNum >= 0)
					proc.simpleIOCtrl().requestDigitalIn(pinNum);
			}
		}
		
		for(int i=0; i < analogPinSpinner.length; i++){
			if(!analogPinIsOutput[i].getSelection()){
				int pinNum = analogPinSpinner[i].getSelection();
				if(pinNum >= 0)
					proc.simpleIOCtrl().requestAnalogIn(pinNum);
			}
		}
	}
	
	private void checkEnables() {
		for(int i=0; i < digitalPinSpinner.length; i++)
			digitalPinState[i].setEnabled(digitalPinIsOutput[i].getSelection());
		
		for(int i=0; i < analogPinSpinner.length; i++)
			analogPinValue[i].setEnabled(analogPinIsOutput[i].getSelection());
	}
	
	public Group getSWTGroup(){ return swtGroup; }
}
