package imageProc.control.standa.ximc.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import imageProc.control.standa.ximc.XIMCConfig;
import imageProc.control.standa.ximc.XIMCControl;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import ru.ximc.libximc.JXimc;

/** GUI control for example serial thing control module 
 * {@snippet :
 * 
 * }
 * */ 
public class XIMCSWTControl implements ImagePipeController, ImagePipeSWTController  {

	private XIMCControl proc;	
	
	private Group swtGroup;
	private CTabItem thisTabItem;
	private Text motorStatusTextbox;	
	
	private CTabFolder swtTabFoler;	
	
	private CTabItem swtExampleTab;	
	private ExampleGroupSWTControl exampleGroup;
	
	private SWTSettingsControl settingsCtrl;
	
	public XIMCSWTControl(XIMCControl proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("XIMC Control");
		swtGroup.setLayout(new FillLayout());
		
		SashForm swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		
		motorStatusTextbox = new Text(swtSashForm, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
        motorStatusTextbox.setText("...");
	    
	    Composite swtBottomComp = new Composite(swtSashForm, SWT.NONE);
	    swtBottomComp.setLayout(new GridLayout(1, false));
		swtTabFoler = new CTabFolder(swtBottomComp, SWT.BORDER);
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        	        
		exampleGroup = new ExampleGroupSWTControl(swtTabFoler, proc);
		swtExampleTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtExampleTab.setControl(exampleGroup.getSWTGroup());
		swtExampleTab.setText("Example group");

		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtBottomComp, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		generalControllerUpdate();	
	}

	/* ---- Some things we need to define for ImageProc ----- */
	@Override
	public void destroy() { proc.destroy(); }
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab;}
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	private void doUpdate(){
		updateStatus();
		settingsCtrl.doUpdate();
		exampleGroup.doUpdate();
	}
	
	private void updateStatus() {
		XIMCConfig config = proc.getConfig();
		
		StringBuffer sb = new StringBuffer("state_t:\n");
		if(config.state == null) {
			sb.append("\tnull\n");
		}else {
			sb.append("\tCurSpeed=" + config.state.CurSpeed +
						"\n\tCurPosition=" + config.state.CurPosition +
						"\n\tUpwr=" + config.state.Upwr +
						"\n\tIpwr=" + config.state.Ipwr +
						"\n\tFlags=" + config.state.Flags + "\n");
		}
				
		sb.append("\nengine_settings:\n");
		if(config.engine_settings == null) {
			sb.append("null\n");
		}else {
			sb.append("\tNomVoltage=" + config.engine_settings.NomVoltage +
					"\n\tNomCurrent=" + config.engine_settings.NomCurrent +
					"\n\tNomSpeed=" + config.engine_settings.NomSpeed +
					"\n");
			
		}
		
		motorStatusTextbox.setText(sb.toString());
		
	}
	
	
	
}
