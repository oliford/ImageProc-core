package imageProc.control.standa.ximc.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.standa.ximc.XIMCConfig;
import imageProc.control.standa.ximc.XIMCControl;


public class ExampleGroupSWTControl {
	
	private XIMCControl proc;
	
	private Group swtGroup;
	private Spinner positionSpinner;
	private Text nameTextBox;
	private Button probeDevicesCheckbox;
	private Button probeNetworkCheckbox;
	private Combo deviceListCombo;
	private Button openButton;
	private Button closeButton;
	
	private Button gotoPositionButton;
	private Button doThingNowButton;
	private Button doThingOnTriggerCheckbox;

	public ExampleGroupSWTControl(Composite parent, XIMCControl proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Example Group");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(4, false));
		

		Label lP = new Label(swtGroup, SWT.None); lP.setText("Probe:");

		probeDevicesCheckbox = new Button(swtGroup, SWT.CHECK);
		probeDevicesCheckbox.setText("Local devices");
		probeDevicesCheckbox.setToolTipText("Probe local devices for device list");
		probeDevicesCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		probeDevicesCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		probeNetworkCheckbox = new Button(swtGroup, SWT.CHECK);
		probeNetworkCheckbox.setText("Network devices");
		probeNetworkCheckbox.setToolTipText("Probe network devices for device list");
		probeNetworkCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		probeNetworkCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });


		Label lD = new Label(swtGroup, SWT.NONE); lD.setText("Devices:");
		deviceListCombo = new Combo(swtGroup, SWT.NONE);
		deviceListCombo.setItems(new String[0]);
		deviceListCombo.setToolTipText("List of Standa XIMC devices");
		deviceListCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		deviceListCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		openButton = new Button(swtGroup, SWT.PUSH);
		openButton.setText("Open");
		openButton.setToolTipText("Open the selected device");
		openButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		openButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { openButtonEvent(event); } });


		closeButton = new Button(swtGroup, SWT.PUSH);
		closeButton.setText("Close");
		closeButton.setToolTipText("Close the currently open device");
		closeButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		closeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { closeButtonEvent(event); } });

		// ------row 1------
		
		Label lTB = new Label(swtGroup, SWT.NONE); lTB.setText("Name:");
		nameTextBox = new Text(swtGroup, SWT.NONE);
		nameTextBox.setText("Do thing");
		nameTextBox.setToolTipText("Making the thing do a thing right now");
		nameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		nameTextBox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		// -----row 2-------
		
		Label lPs = new Label(swtGroup, SWT.NONE); lPs.setText("Position:");
		
		positionSpinner = new Spinner(swtGroup, SWT.NONE);
		positionSpinner.setValues(500, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 100, 1000);
		positionSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		gotoPositionButton = new Button(swtGroup, SWT.PUSH);
		gotoPositionButton.setText("Set");
		gotoPositionButton.setToolTipText("Set the position into the thing");
		gotoPositionButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		gotoPositionButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { gotoPositionButtonEvent(event); } });

		// ------row 3------
		
		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Speed:");
		
		positionSpinner = new Spinner(swtGroup, SWT.NONE);
		positionSpinner.setValues(500, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 100, 1000);
		positionSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		

		// ------row 4------
		
		
	}

	protected void openButtonEvent(Event event) {
		XIMCConfig config = proc.getConfig();
		config.deviceName = deviceListCombo.getText();
		proc.openDevice();
	}
	
	protected void closeButtonEvent(Event event) {
		proc.closeDevice();
	}

	/** Called by GUI components to set the config to changes of the GUI */
	protected void settingsChangedEvent(Event event) {
		XIMCConfig config = proc.getConfig();
		config.probeDevices = probeDevicesCheckbox.getSelection();
		config.probeNetwork = probeNetworkCheckbox.getSelection();
	}

	protected void gotoPositionButtonEvent(Event event) {
		XIMCConfig config = proc.getConfig();
		config.target = positionSpinner.getSelection();
		
		proc.gotoTarget();
		
	}
	
	/** Called by ImageProc to set the GUI to changes of the config */
	public void doUpdate() {
		XIMCConfig config = proc.getConfig();
		
		boolean isOpen = proc.isOpen();
		deviceListCombo.setEnabled(!isOpen);
		openButton.setEnabled(!isOpen);
		closeButton.setEnabled(!isOpen);
		
		probeDevicesCheckbox.setSelection(config.probeDevices);
		probeNetworkCheckbox.setSelection(config.probeNetwork);
		
		deviceListCombo.setItems(proc.getDeviceList());
		deviceListCombo.setText(config.deviceName);
		
	}

	public Group getSWTGroup(){ return swtGroup; }
}

