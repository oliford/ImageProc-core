package imageProc.control.standa.ximc;

import java.util.logging.Logger;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.standa.ximc.swt.XIMCSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.EventReciever.Event;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import ru.ximc.libximc.JXimc;
import ru.ximc.libximc.JXimc.engine_settings_t;
import ru.ximc.libximc.JXimc.status_t;

/** Example module for controlling a thing by Serial */
public class XIMCControl extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, EventReciever {
	public static final int PROBE_FLAGS_ENUM_PROBE = 1;
	public static final int PROBE_FLAGS_ENUM_NETWORK = 4;
	
	/** Current configuration */
	private XIMCConfig config = new XIMCConfig();
	
	/** Instance of the GUI controller connected to this */
	private XIMCSWTControl swtController;

	private String lastLoadedConfig;

	private String[] deviceList;
	
	private int deviceHandle = JXimc.DEVICE_UNDEFINED;
	
	private Logger logr = Logger.getLogger(this.getClass().getName());
	
	public XIMCControl() {	
		String jniName = System.getProperty("os.name").toLowerCase().startsWith("windows") ? "libjximc" : "jximc";
		System.loadLibrary(jniName);
		config.libximcVersion = JXimc.ximc_version();
		System.out.println("libximc version " + config.libximcVersion);
	}
	
	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalStart:			
			break;
		default:
			break;
		}		
	}

	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, XIMCConfig.class);
		
		updateAllControllers();	
	}

	public XIMCConfig getConfig() { return config; }
	
	
	/* --- Some things from ImageProc structure --- */
	@Override
	public boolean isIdle() { return true;	}
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new XIMCSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}

	public String[] getDeviceList() { return deviceList; }

	public void openDevice() {
		if(deviceHandle != JXimc.DEVICE_UNDEFINED)
			throw new RuntimeException("Device is already open");

		deviceHandle = JXimc.open_device(config.deviceName);
		if (deviceHandle == JXimc.DEVICE_UNDEFINED) {
			logr.severe("Cannot open a device");
			return;
		}
		logr.fine("Device " + config.deviceName + " opened as " + deviceHandle);
		
		config.engine_settings = JXimc.get_engine_settings(deviceHandle);
		config.state = JXimc.get_status(deviceHandle);
		
		//JXimc.command_read_settings(PROBE_FLAGS_ENUM_NETWORK);
		
		updateAllControllers();
	}
	
	public void closeDevice() {
		if(deviceHandle == JXimc.DEVICE_UNDEFINED)
			throw new RuntimeException("Device not open");

		logr.fine("Closing device " + deviceHandle);
		JXimc.close_device(deviceHandle);
		deviceHandle = JXimc.DEVICE_UNDEFINED;
		
		updateAllControllers();
	}
	
	
	public boolean isOpen() { return deviceHandle != JXimc.DEVICE_UNDEFINED; }
	
	
	
	public void updateDeviceList() {

		//  Set bindy (network) keyfile. Must be called before any call to "enumerate_devices" or "open_device" if you
		//  wish to use network-attached controllers. Accepts both absolute and relative paths, relative paths are resolved
		//  relative to the process working directory. If you do not need network devices then "set_bindy_key" is optional.
		JXimc.set_bindy_key( "keyfile.sqlite" );
		
		int probeFlags = (config.probeDevices ? PROBE_FLAGS_ENUM_PROBE : 0) 
						+ (config.probeNetwork ? PROBE_FLAGS_ENUM_NETWORK : 0);		

		long deviceEnumeration = JXimc.enumerate_devices(probeFlags, config.enumHints);
		if (deviceEnumeration == 0)
			throw new RuntimeException("Error enumerating devices");
		
		int namesCount = JXimc.get_device_count(deviceEnumeration);
		deviceList = new String[namesCount];
		for (int i = 0; i < namesCount; i++) {
			deviceList[i] = JXimc.get_device_name(deviceEnumeration, i);
		}

		JXimc.free_enumerate_devices(deviceEnumeration);
		
		updateAllControllers();
	}

	public void gotoTarget(int pos) {
		setTarget(pos);
		gotoTarget();
	}
	
	private void setTarget(int pos) {
		// TODO Auto-generated method stub
		
	}

	public void gotoTarget() {
		if(deviceHandle == JXimc.DEVICE_UNDEFINED)
			throw new RuntimeException("Device not open");
	}


	
}
