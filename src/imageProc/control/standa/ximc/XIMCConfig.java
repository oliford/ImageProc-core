package imageProc.control.standa.ximc;

import ru.ximc.libximc.JXimc;

/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everything should be public
 */
public class XIMCConfig {

	/** Serial port to connect to */
	public String deviceName;

	public String libximcVersion;
	
	public String enumHints = "addr=192.168.0.1,172.16.2.3";

	public boolean probeDevices = true;
	
	public boolean probeNetwork = true;
	
	public JXimc.engine_settings_t engine_settings;
	
	public JXimc.status_t state;

	public int target;
}
