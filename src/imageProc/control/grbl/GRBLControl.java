package imageProc.control.grbl;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.grbl.swt.GRBLSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.EventReciever.Event;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Example module for controlling a thing by Serial */
public class GRBLControl extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, 
												EventReciever, LoggedComm.LogUser {

	/** Current configuration */
	private GRBLConfig config = new GRBLConfig(3);
	
	/** Class for communicating with serial port (stolen from Arduino controller) */
	protected LoggedComm serialComm;
	
	/** Instance of the GUI controller connected to this */
	private GRBLSWTControl swtController;
	
	private Timer statusTimer = null;
	
	private String grblStatus = "";

	private boolean alarmActive = false;

	private String lastLoadedConfig;
	
	public GRBLControl() {
		config.portName = SettingsManager.defaultGlobal().getProperty("imageProc.thing.portName", "/dev/ttyS0");
		config.baudRate = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.thing.baudRate", "115200"));
		serialComm = new LoggedComm(this);
		serialComm.setCarriageReturn(false);
	}
	
	protected void requestStatus() {
		if(serialComm.isConnected()) {
			serialComm.send("?", false);
		}
	}
	
	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalStart:			
			break;
		case GlobalAbort:
			reset();
			break;
		default:
			break;
		}		
	}

	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, GRBLConfig.class);
		
		updateAllControllers();	
	}

	public LoggedComm serialComm() { return serialComm; }

	public GRBLConfig getConfig() { return config; }
	
		
	public void getSettings(){
		serialComm.send("$$");
	}

	@Override
	/** Called when a line of text was recieved from the serial port */
	public void receivedLine(String lineStr) {
		if(lineStr.startsWith("$")) {
			//setttings readback
			/*11/08 13:58:42 < $0=10 (step pulse, usec)
				11/08 13:58:42 < $1=25 (step idle delay, msec)
				11/08 13:58:42 < $2=0 (step port invert mask:00000000)
				11/08 13:58:42 < $3=1 (dir port invert mask:00000001)
				11/08 13:58:42 < $4=0 (step enable invert, bool)
				11/08 13:58:42 < $5=0 (limit pins invert, bool)
				11/08 13:58:42 < $6=0 (probe pin invert, bool)
				11/08 13:58:42 < $10=3 (status report mask:00000011)
				11/08 13:58:42 < $11=0.010 (junction deviation, mm)
				11/08 13:58:42 < $12=0.002 (arc tolerance, mm)
				11/08 13:58:42 < $13=0 (report inches, bool)
				11/08 13:58:42 < $20=0 (soft limits, bool)
				11/08 13:58:42 < $21=0 (hard limits, bool)
				11/08 13:58:42 < $22=1 (homing cycle, bool)
				11/08 13:58:42 < $23=1 (homing dir invert mask:00000001)
				11/08 13:58:42 < $24=25.000 (homing feed, mm/min)
				11/08 13:58:42 < $25=200.000 (homing seek, mm/min)
				11/08 13:58:42 < $26=250 (homing debounce, msec)
				11/08 13:58:42 < $27=1.000 (homing pull-off, mm)
				11/08 13:58:42 < $130=200.000 (x max travel, mm)
				11/08 13:58:42 < $131=200.000 (y max travel, mm)
				11/08 13:58:42 < $132=200.000 (z max travel, mm)*/
			
			Pattern p = Pattern.compile("\\$([0-9]*)=([0-9.]*).*");
			Matcher m = p.matcher(lineStr);
			if(!m.matches())
				throw new RuntimeException("Undeciphered line: "+lineStr);
			int settingCode = Integer.parseInt(m.group(1));
			double val = Double.parseDouble(m.group(2));
			
			switch(settingCode) {
			case 3:
				for(int i=0; i < config.axes.length; i++)
					config.axes[i].reverse = (((int)val) & (1 << i)) != 0;
				break;
			case 23:
				for(int i=0; i < config.axes.length; i++)
					config.axes[i].reverseHome = (((int)val) & (1 << i)) != 0;
				break;
			case 24:
				config.homeFeedRate = (int)val;
				break;
			case 25:
				config.homeSeekRate = (int)val;
				break;
			case 27:
				config.homePullOff = (int)val;
				break;
			case 28:
				config.noiseWindow = (int)val;
				break;
			case 100,101,102: //steps/mm
				config.axes[settingCode-100].resolution = val;
				break;
			case 110,111,112: //rate mm/min
				config.axes[settingCode-110].speed = val;
				break;
			case 120,121,122: //accel mm/min
				config.axes[settingCode-120].accel = val;
				break;
			
			}
		}else if(lineStr.startsWith("<")) {
			alarmActive = lineStr.contains("Alarm");
			
			lineStr.substring(1, lineStr.length() - 2);
			Pattern p = Pattern.compile("<(.*),MPos:([-0-9.]*),([-0-9.]*),([-0-9.]*),.*>");
			Matcher m = p.matcher(lineStr);
			if(!m.matches())
				throw new RuntimeException("Undeciphered line: "+lineStr);
			grblStatus = m.group(1);
			config.axes[0].pos = Double.parseDouble(m.group(2));
			config.axes[1].pos = Double.parseDouble(m.group(3));
			config.axes[2].pos = Double.parseDouble(m.group(4));
		}else if(lineStr.startsWith("Grbl")) {
			getSettings();
		}
		updateAllControllers();
	}

	@Override
	/** Called when anything is written to the serial log. 
	 * Really only to tell the GUI to update all the boxes and things */
	public void logChanged() {
		updateAllControllers(); //force update of GUI components
	}

	@Override
	/** Called when serial port is connected.
	 * Add initial configuration here */
	public void connected() {			
		setUpdatePeriod(config.updateEnable, config.statusUpdatePeriodMS);
	}

	public void home(int axisID) {
		serialComm.send("$H" + config.axes[axisID].id);
		//connectedSource.setSeriesMetaData("ThingControl/PositionWhenThingDone", config.positionOfA, false);
	}

	public void gotoTarget(int axisID) {
		serialComm.send("G1 " + config.axes[axisID].id + " " + config.axes[axisID].target + " F" + config.axes[axisID].speed);
	}

	public void gotoPos(int axisID, double pos) {
		serialComm.send("G1 " + config.axes[axisID].id + " " + pos + " F" + config.axes[axisID].speed);
	}

	public void holdCycle() {
		serialComm.send("!", false);		
	}
	
	public void continueCycle() {
		serialComm.send("~", false);		
	}
	
	public void reset() {
		serialComm.send("" + (char)0x18, false);		
	}
	
	public void clearAlarm() {
		serialComm.send("$x");		
	}
	
	public void setUpdatePeriod(boolean enable, int periodMS) {
		if(statusTimer != null) {
			if(enable == config.updateEnable && periodMS == config.statusUpdatePeriodMS)
				return;
		
			statusTimer.cancel();
			statusTimer = null;			
		}
		config.updateEnable = enable;
		config.statusUpdatePeriodMS = periodMS;
		
		if(enable) {			
			statusTimer = new Timer();
			statusTimer.schedule(new TimerTask() {
		            @Override
		            public void run() {
		                requestStatus();
		            }
        		}, 0, config.statusUpdatePeriodMS);
		}
		
	}

	public void setParameters() {
		int reverseMask = 0, reverseHomeMask = 0;
		for(int i=0; i < config.axes.length; i++) {
			int bit = 1 << i;
			if(config.axes[i].reverse)
				reverseMask |= bit;
			if(config.axes[i].reverseHome)
				reverseHomeMask |= bit;			
		}
		
		serialComm.send("$3=" + reverseMask);
		serialComm.send("$23=" + reverseHomeMask);
		serialComm.send("$24=" + config.homeFeedRate);
		serialComm.send("$25=" + config.homeSeekRate);
		serialComm.send("$27=" + config.homePullOff);
		serialComm.send("$28=" + config.noiseWindow);
		
		for(int i=0; i < config.axes.length; i++) {
			serialComm.send("$" + (100+i) + "=" + config.axes[i].resolution);
			serialComm.send("$" + (110+i) + "=" + config.axes[i].speed);
			serialComm.send("$" + (120+i) + "=" + config.axes[i].accel);
		}
	}

	/* --- Some things from ImageProc structure --- */
	@Override
	public boolean isIdle() { return true;	}
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
		
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new GRBLSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}

	public String getGRBLStatus() { return grblStatus;	}

	@Override
	/** Expose as public for SequenceCtrl */
	public void updateAllControllers() {
		super.updateAllControllers();
	}

	public boolean hasAlarm() { return alarmActive; }

	public void gotoPreset(int axisID, String presetName) {
		double val = config.axes[axisID].presets.get(presetName);
		//proc.state().motor[axisID].targetPresetName = presetName;
		gotoPos(axisID, val);
		
	}
}
