package imageProc.control.grbl;

import java.util.HashMap;

/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everything should be public
 */
public class GRBLConfig {

	/** Serial port to connect to */
	public String portName;
	
	/** BAUD rate to connect serial port with */
	public int baudRate;
	
	/** Period to request status updates */
	public int statusUpdatePeriodMS = 1000;
	public boolean updateEnable = true;
	
	/** cycles */
	public int noiseWindow;

	/** mm */
	public int homePullOff;

	/** mm/s */
	public int homeFeedRate;
	
	/** mm/s */
	public int homeSeekRate;
	
	public class Axis {
		public static String[] defaultIDs = { "X", "Y", "Z", "A", "B" };
	
		/** Axis ID in GRBL, usually defaultIDs */
		public String id;
		
		/** Descriptive name of device attached to axis */
		public String name; 
		
		/** Last know position of each axes when saved [mm]*/
		public double pos;
		
		/** Set position of each axes to go to [mm] */
		public double target;

		/** Resolution [steps/mm] */
		public double resolution;
		
		/** Max speed of each axis [mm/min] */
		public double speed;
		
		/** Max acceleration of each axis [mm/s^2] */
		public double accel;
		
		public boolean reverse;
		
		public boolean reverseHome;

		public HashMap<String, Double> presets;
	}
	
	public Axis[] axes;
	
	public GRBLConfig() { };
	
	public GRBLConfig(int nAxes) {
		axes = new Axis[nAxes];
		for(int i=0; i < nAxes; i++) {
			axes[i] = new Axis();
			axes[i].id = Axis.defaultIDs[i];
			axes[i].name = axes[i].id;
		}
	
	}
}
