package imageProc.control.grbl.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.arduinoComm.ArduinoCommConfig;
import imageProc.control.grbl.GRBLConfig;
import imageProc.control.grbl.GRBLControl;
import imageProc.core.ImageProcUtil;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SerialSWTGroup {
	public static final long KEEPALIVE_TIME_MS = 60000;
	private GRBLControl proc;

	private Group swtGroup;
	private Label statusLabel;
	private Combo portDeviceTextbox;
	private Combo baudRateTextbox;
	private Button connectButton;
	private Button disconnectButton;	
	private Text sendTextbox;
	private Button sendButton;
	private Button clearButton;
	private Button updateCheckbox;
	private Button applyButton;
	private Text updatePeriodTextbox;
	
	private Text homePullOffTextbox;
	private Text homeFeedRateTextbox;
	private Text homeSeekRateTextbox;
	private Text noiseWindowTextbox;
		
	public SerialSWTGroup(Composite parent, GRBLControl proc) {
		this.proc = proc;
		
		GRBLConfig config = proc.getConfig();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Comms");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        swtGroup.setLayout(new GridLayout(4, false));
		ImageProcUtil.addRevealWriggler(swtGroup);
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
	    statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
       
        Label lPN = new Label(swtGroup, SWT.NONE); lPN.setText("Device:");
	    portDeviceTextbox = new Combo(swtGroup, SWT.MULTI);
	    portDeviceTextbox.setItems(new String[]{ config.portName, "/dev/ttyS0", "/dev/ttyACM0" });
	    portDeviceTextbox.select(0);
	    portDeviceTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
	   
	    connectButton = new Button(swtGroup, SWT.PUSH);
	    connectButton.setText("Connect");
	    connectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    connectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { connectButtonEvent(event); } });

	    Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Baud:");
	    baudRateTextbox = new Combo(swtGroup, SWT.MULTI);
	    baudRateTextbox.setItems(new String[]{ Integer.toString(config.baudRate),  "115200", "9600" });
	    baudRateTextbox.select((config.baudRate == 9600) ? 1 : 0);
	    baudRateTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
	   
	    disconnectButton = new Button(swtGroup, SWT.PUSH);
	    disconnectButton.setText("Disconnect");
	    disconnectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    disconnectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { disconnectButtonEvent(event); } });

	    Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Send:");
	        
	    sendTextbox = new Text(swtGroup, SWT.NONE );
	    sendTextbox.setText("");
	    sendTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
	    
	    sendButton = new Button(swtGroup, SWT.PUSH);
	    sendButton.setText("Send");
	    sendButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    sendButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sendButtonEvent(event); } });

	    clearButton = new Button(swtGroup, SWT.PUSH);
	    clearButton.setText("Clear");
	    clearButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    clearButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { clearButtonEvent(event); } });

	    Label lUP = new Label(swtGroup, SWT.NONE); lUP.setText("Status period:");
	    
	    
	    updateCheckbox = new Button(swtGroup, SWT.CHECK);
	    updateCheckbox.setText("Enable");
	    updateCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    updateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { updateChangedEvent(event); } });
	     
	    updatePeriodTextbox = new Text(swtGroup, SWT.NONE );
	    updatePeriodTextbox.setText("?");
	    updatePeriodTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
	    updatePeriodTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { updateChangedEvent(event); } });
	    
	    Label lHP = new Label(swtGroup, SWT.NONE); lHP.setText("Home pull off:");
	    homePullOffTextbox = new Text(swtGroup, SWT.NONE);
	    homePullOffTextbox.setText("?");
	    homePullOffTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 3, 1));
	    homePullOffTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) {settingsChangedEvent(event); } });
	    
	    Label lHF = new Label(swtGroup, SWT.NONE); lHF.setText("Home feed rate [mm/s]:");
	    homeFeedRateTextbox = new Text(swtGroup, SWT.NONE);
	    homeFeedRateTextbox.setText("?");
	    homeFeedRateTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 3, 1));
	    homeFeedRateTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) {settingsChangedEvent(event); } });
	    
	    Label lHS = new Label(swtGroup, SWT.NONE); lHS.setText("Home seek rate [mm/s]:");
	    homeSeekRateTextbox = new Text(swtGroup, SWT.NONE);
	    homeSeekRateTextbox.setText("?");
	    homeSeekRateTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 3, 1));
	    homeSeekRateTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) {settingsChangedEvent(event); } });
	    	    
	    Label lNW = new Label(swtGroup, SWT.NONE); lNW.setText("Noise window:");
	    noiseWindowTextbox = new Text(swtGroup, SWT.NONE);
	    noiseWindowTextbox.setText("?");
	    noiseWindowTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 3, 1));
	    noiseWindowTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) {settingsChangedEvent(event); } });
	    
	    applyButton = new Button(swtGroup, SWT.PUSH);
		applyButton.setText("Apply");
		applyButton.setToolTipText("Set all feed, speed, accel etc. parameters into firmware");
		applyButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		applyButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.setParameters(); } });

	   
}
	

	protected void updateChangedEvent(Event event) {
		proc.setUpdatePeriod(updateCheckbox.getSelection(),
				Algorithms.mustParseInt(updatePeriodTextbox.getText()));
	}

	protected void settingsChangedEvent(Event event) {
		GRBLConfig config = proc.getConfig();
		config.homePullOff = Algorithms.mustParseInt(homePullOffTextbox.getText());
		config.homeFeedRate = Algorithms.mustParseInt(homeFeedRateTextbox.getText());
		config.homeSeekRate = Algorithms.mustParseInt(homeSeekRateTextbox.getText());
		config.noiseWindow = Algorithms.mustParseInt(noiseWindowTextbox.getText());
	}


	private void connectButtonEvent(Event event) {
		GRBLConfig config = proc.getConfig();
		config.portName = portDeviceTextbox.getText();
		config.baudRate = Algorithms.mustParseInt(baudRateTextbox.getText());
		proc.serialComm().connectSerial(config.portName, config.baudRate);
	}


	private void disconnectButtonEvent(Event event) {
		proc.serialComm().disconnect();
	}
	
	private void sendButtonEvent(Event event) {
		proc.serialComm().send(sendTextbox.getText());
		sendTextbox.setText("");
	}
	
	private void clearButtonEvent(Event event) {
		proc.serialComm().clear();
		sendTextbox.setText("");
	}
	
	
	public void doUpdate() {
		statusLabel.setText( proc.serialComm().getStatus() );
		
		GRBLConfig config = proc.getConfig();
		
		portDeviceTextbox.setText(config.portName);
		baudRateTextbox.setText(Integer.toString(config.baudRate));
		updateCheckbox.setSelection(config.updateEnable);
		updatePeriodTextbox.setText(Integer.toString(config.statusUpdatePeriodMS));
		homePullOffTextbox.setText(Integer.toString(config.homePullOff));
		homeFeedRateTextbox.setText(Integer.toString(config.homeFeedRate));
		homeSeekRateTextbox.setText(Integer.toString(config.homeSeekRate));
		noiseWindowTextbox.setText(Integer.toString(config.noiseWindow));
		
	}

	public Group getSWTGroup(){ return swtGroup; }
}
