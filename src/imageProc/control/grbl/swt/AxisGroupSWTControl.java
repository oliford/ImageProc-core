package imageProc.control.grbl.swt;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.arduinoComm.swt.MotorPresetsTable;
import imageProc.control.arduinoComm.swt.MotorPresetsTable.MotorPresetsHandler;
import imageProc.control.grbl.GRBLConfig;
import imageProc.control.grbl.GRBLControl;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.EditableTable;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class AxisGroupSWTControl implements MotorPresetsHandler {
	
	private GRBLControl proc;
	private int axisID;
	
	private Group swtGroup;
	private Text nameTextBox;	
	private Button homeButton;
	private Text posTextBox;
	private Spinner targetTextBox;
	private Button gotoButton;
	private Spinner resolutionTextBox;
	private Spinner speedTextBox;
	private Spinner accelTextBox;
	private Button reverseCheckbox;
	private Button reverseHomeCheckbox;
	private Button hardLimitCheckbox;	
	
	private Button applyButton;
	
	private MotorPresetsTable presetsTable;
	
	private boolean updateInhibit = false;

	public AxisGroupSWTControl(Composite parent, GRBLControl proc, int axisID) {
		this.proc = proc;
		this.axisID = axisID;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Axis Group");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(4, false));
		ImageProcUtil.addRevealWriggler(swtGroup);
				
		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Name:");
		nameTextBox = new Text(swtGroup, SWT.NONE);
		nameTextBox.setText("Axis " + axisID);
		nameTextBox.setToolTipText("Text name of what the motor controls");
		nameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		nameTextBox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		homeButton = new Button(swtGroup, SWT.PUSH);
		homeButton.setText("Home");
		homeButton.setToolTipText("Find the home switch position");
		homeButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		homeButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.home(axisID); } });

		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Pos [mm]:");
		posTextBox = new Text(swtGroup, SWT.READ_ONLY);
		posTextBox.setText("?");
		posTextBox.setToolTipText("Current position of motor");
		posTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		
		Label lG = new Label(swtGroup, SWT.NONE); lG.setText("Target [mm]:");
		targetTextBox = new Spinner(swtGroup, SWT.NONE);
		targetTextBox.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 1000, 10000);
		targetTextBox.setToolTipText("Position to goto");
		targetTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		targetTextBox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		gotoButton = new Button(swtGroup, SWT.PUSH);
		gotoButton.setText("Go");
		gotoButton.setToolTipText("Go to the given position now");
		gotoButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		gotoButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.gotoTarget(axisID); } });

		
		Label lR = new Label(swtGroup, SWT.NONE); lR.setText("Resolution [steps/mm]:");
		resolutionTextBox = new Spinner(swtGroup, SWT.NONE);
		resolutionTextBox.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 1000, 10000);
		resolutionTextBox.setToolTipText("Max speed");
		resolutionTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		resolutionTextBox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
				
		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Speed [mm/s]:");
		speedTextBox = new Spinner(swtGroup, SWT.NONE);
		speedTextBox.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 1000, 10000);
		speedTextBox.setToolTipText("Max speed");
		speedTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		speedTextBox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lA = new Label(swtGroup, SWT.NONE); lA.setText("Accel [mm/s²]:");
		accelTextBox = new Spinner(swtGroup, SWT.NONE);
		accelTextBox.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 1000, 10000);
		accelTextBox.setToolTipText("Max acceleration");
		accelTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		accelTextBox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		reverseCheckbox = new Button(swtGroup, SWT.CHECK);
		reverseCheckbox.setText("Reverse");
		reverseCheckbox.setToolTipText("Reverse the motor direction");
		reverseCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		reverseCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		reverseHomeCheckbox = new Button(swtGroup, SWT.CHECK);
		reverseHomeCheckbox.setText("Reverse Home");
		reverseHomeCheckbox.setToolTipText("Reverse the direction to search for home");
		reverseHomeCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		reverseHomeCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		applyButton = new Button(swtGroup, SWT.PUSH);
		applyButton.setText("Apply");
		applyButton.setToolTipText("Set speed, accel etc. parameters into firmware");
		applyButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		applyButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.setParameters(); } });

		presetsTable = new MotorPresetsTable(swtGroup, this);
	}

	/** Called by GUI components to set the config to changes of the GUI */
	protected void settingsChangedEvent(Event event) {
		if(updateInhibit)
			return;
		
		updateInhibit = true;
		GRBLConfig config = proc.getConfig();

		config.axes[axisID].name = nameTextBox.getText();
		config.axes[axisID].pos = Algorithms.mustParseDouble(posTextBox.getText());
		config.axes[axisID].target = (double)targetTextBox.getSelection()/1000;
		config.axes[axisID].resolution = (double)resolutionTextBox.getSelection()/1000;
		config.axes[axisID].speed = (double)speedTextBox.getSelection()/1000;
		config.axes[axisID].accel = (double)accelTextBox.getSelection()/1000;
		config.axes[axisID].reverse = reverseCheckbox.getSelection();
		config.axes[axisID].reverseHome = reverseHomeCheckbox.getSelection();
		updateInhibit = false;
	}


	
	/** Called by ImageProc to set the GUI to changes of the config */
	public void doUpdate() {
		if(updateInhibit)
			return;
		
		updateInhibit = true;
		GRBLConfig config = proc.getConfig();
		
		if(!nameTextBox.isFocusControl())
			nameTextBox.setText(config.axes[axisID].name == null ? "(null)" : config.axes[axisID].name);
		if(!posTextBox.isFocusControl())
			posTextBox.setText(Double.toString(config.axes[axisID].pos));
		if(!targetTextBox.isFocusControl())
			targetTextBox.setSelection((int)(config.axes[axisID].target*1000));
		if(!resolutionTextBox.isFocusControl())
			resolutionTextBox.setSelection((int)(config.axes[axisID].resolution)*1000);
		if(!speedTextBox.isFocusControl())
			speedTextBox.setSelection((int)(config.axes[axisID].speed)*1000);
		if(!accelTextBox.isFocusControl())
			accelTextBox.setSelection((int)(config.axes[axisID].accel)*1000);
		reverseCheckbox.setSelection(config.axes[axisID].reverse);
		reverseHomeCheckbox.setSelection(config.axes[axisID].reverseHome);

		presetsTable.presetsToGUI();
		
		updateInhibit = false;
	}

	public Group getSWTGroup(){ return swtGroup; }

	@Override
	public HashMap<String, Double> getPresets() {
		GRBLConfig config = proc.getConfig();
		if(config.axes[axisID].presets == null) 
			config.axes[axisID].presets = new HashMap<>();
		return config.axes[axisID].presets;
	}

	@Override
	public void motorGotoPos(double presetVal) {
		proc.gotoPos(axisID, presetVal);
	}

	@Override
	public void updateAllControllers() {
		proc.updateAllControllers();
	}

	@Override
	public void motorGotoPreset(String presetName) {
		proc.gotoPreset(axisID, presetName);
	}
}

