package imageProc.control.grbl.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.control.grbl.GRBLControl;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;

/** GUI control for example serial GRBL control module 
 * {@snippet :
 * 
 * }
 * */ 
public class GRBLSWTControl implements ImagePipeController, ImagePipeSWTController  {

	private GRBLControl proc;	
	
	private Group swtGroup;
	private CTabItem thisTabItem;	
	private Text serialLogTextbox;	
	private Label statusLabel;
	private Button holdButton;
	private Button continueButton;
	private Button clearAlarmButton;
	private Button resetButton;
	
	private CTabFolder swtTabFoler;	
	
	private CTabItem swtSerialTab;
	private SerialSWTGroup serialGroup;
	
	private CTabItem[] swtAxisTab;	
	private AxisGroupSWTControl[] axisGroup;
	
	private SWTSettingsControl settingsCtrl;
	
	public GRBLSWTControl(GRBLControl proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Arduino Comm");
		swtGroup.setLayout(new FillLayout());
		
		SashForm swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		
        serialLogTextbox = new Text(swtSashForm, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
	    serialLogTextbox.setText(proc.serialComm().getLog().toString());
	    
	    Composite swtBottomComp = new Composite(swtSashForm, SWT.NONE);
	    swtBottomComp.setLayout(new GridLayout(5, false));
	    
	    statusLabel = new Label(swtBottomComp, SWT.None);
	    statusLabel.setText("Status: init");
	    statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
	    
	    holdButton = new Button(swtBottomComp, SWT.PUSH);
	    holdButton.setText("Hold");
	    holdButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    holdButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.holdCycle(); } });
	    
	    continueButton = new Button(swtBottomComp, SWT.PUSH);
	    continueButton.setText("Continue");
	    continueButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    continueButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.continueCycle(); } });
	    
	    clearAlarmButton = new Button(swtBottomComp, SWT.PUSH);
	    clearAlarmButton.setText("clear alarm");
	    clearAlarmButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    clearAlarmButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.clearAlarm(); } });

	    resetButton = new Button(swtBottomComp, SWT.PUSH);
	    resetButton.setText("Reset");
	    resetButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    resetButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.reset(); } });

	    
		swtTabFoler = new CTabFolder(swtBottomComp, SWT.BORDER);
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
        	        
		serialGroup = new SerialSWTGroup(swtTabFoler, proc);
		swtSerialTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSerialTab.setControl(serialGroup.getSWTGroup());
		swtSerialTab.setText("Comms");

		axisGroup = new AxisGroupSWTControl[3];
		swtAxisTab = new CTabItem[axisGroup.length];
		for(int i=0; i < axisGroup.length; i++) {
			axisGroup[i] = new AxisGroupSWTControl(swtTabFoler, proc, i);
			swtAxisTab[i] = new CTabItem(swtTabFoler, SWT.NONE);
			swtAxisTab[i].setControl(axisGroup[i].getSWTGroup());
			swtAxisTab[i].setText("Axis " + i);
		}

		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtBottomComp, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		generalControllerUpdate();
	}

	@Override
	public void destroy() { proc.destroy(); }
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab;}
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	private void doUpdate(){
		settingsCtrl.doUpdate();
		
		statusLabel.setText("Status: Serial: " + proc.serialComm().getStatus() + ", GRBL: " + proc.getGRBLStatus());
		Color col = swtGroup.getDisplay().getSystemColor(proc.hasAlarm() ? SWT.COLOR_RED : SWT.COLOR_BLACK);
		statusLabel.setForeground(col);
		clearAlarmButton.setForeground(col);
		
		String str = proc.serialComm().getLog().toString();
		int startPos = serialLogTextbox.getCharCount();
		if(str.length() < startPos){
			serialLogTextbox.setText(str);
		}else{
			String appendString = str.substring(startPos);
			serialLogTextbox.append(appendString);
		}
		
		serialGroup.doUpdate();
		for(int i=0; i < axisGroup.length; i++)
			axisGroup[i].doUpdate();
	}
	
}
