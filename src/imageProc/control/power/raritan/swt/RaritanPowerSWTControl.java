package imageProc.control.power.raritan.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.power.raritan.RaritanPowerConfig;
import imageProc.control.power.raritan.RaritanPowerControl;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;

/** GUI control for example serial RaritanPower control module 
 * {@snippet :
 * 
 * }
 * */ 
public class RaritanPowerSWTControl implements ImagePipeController, ImagePipeSWTController  {

	private RaritanPowerControl proc;	
	
	private Group swtGroup;
	private CTabItem thisTabItem;
	private Text commLogTextbox;	
	
	private SWTSettingsControl settingsCtrl;

	private Text addressTextbox;
	private Spinner portSpinner;
	private Button connectOnLoadCheckbox;
	private Button connectButton;
	private Button disconnectButton;

	private Button getStateButton;
	private Group outletsComp; 
	private Label[] outletNumberLabel;
	private Text[] outletNamesTextbox;
	private Button[] outletStateCheckbox;

	private Text sendTextbox;
	private Button sendButton;
	
	public RaritanPowerSWTControl(RaritanPowerControl proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Raritan Power Control");
		swtGroup.setLayout(new FillLayout());
		
		SashForm swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		
        commLogTextbox = new Text(swtSashForm, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
	    commLogTextbox.setText(proc.serialComm().getLog().toString());
	    
	    Composite swtBottomComp = new Composite(swtSashForm, SWT.NONE);
	    swtBottomComp.setLayout(new GridLayout(5, false));
		
	    swtSashForm.setWeights(new int[] { 30, 70 });
		
	    Label lPN = new Label(swtBottomComp, SWT.NONE); lPN.setText("Address:");
	    addressTextbox = new Text(swtBottomComp, SWT.NONE);
	    addressTextbox.setText("?");
	    addressTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
	    addressTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    
	    portSpinner = new Spinner(swtBottomComp, SWT.NONE);
	    portSpinner.setValues(9623, 0, Integer.MAX_VALUE, 0, 1, 10);	    
	    portSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
	    portSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    
	    connectButton = new Button(swtBottomComp, SWT.PUSH);
	    connectButton.setText("Connect");
	    connectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    connectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.connect(); } });

	    disconnectButton = new Button(swtBottomComp, SWT.PUSH);
	    disconnectButton.setText("Disconnect");
	    disconnectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    disconnectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.disconnect(); } });
	    
	    Label lB = new Label(swtBottomComp, SWT.NONE); lB.setText("");        
		  
	    connectOnLoadCheckbox = new Button(swtBottomComp, SWT.CHECK);
	    connectOnLoadCheckbox.setText("Connect on config load");
	    connectOnLoadCheckbox.setToolTipText("Automatically connects when config is loaded. If the config is loaded by an ImageProc profile, this effectively connects on start-up");
	    connectOnLoadCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
	    connectOnLoadCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		    
	    Label lT = new Label(swtBottomComp, SWT.NONE); lT.setText("Send:");        
	    sendTextbox = new Text(swtBottomComp, SWT.NONE );
	    sendTextbox.setText("");
	    sendTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
	    
	    sendButton = new Button(swtBottomComp, SWT.PUSH);
	    sendButton.setText("Send");
	    sendButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    sendButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sendButtonEvent(event); } });
	    
	    outletsComp = new Group(swtBottomComp, SWT.BORDER);
	    outletsComp.setText("Outlets");
	    outletsComp.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 5, 1));
	    outletsComp.setLayout(new GridLayout(3, false));
	    
	    getStateButton = new Button(outletsComp, SWT.PUSH);
	    getStateButton.setText("Update");
	    getStateButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
	    getStateButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.updateOutletStates(); } });
	    
	    outletNumberLabel = new Label[0];
	    outletNamesTextbox = new Text[0];
	    outletStateCheckbox = new Button[0];
	    
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtBottomComp, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		ImageProcUtil.addRevealWriggler(swtGroup);
		generalControllerUpdate();	
	}

	

	protected void sendButtonEvent(Event event) {
		proc.send(sendTextbox.getText());
	}

	@Override
	public void destroy() { proc.destroy(); }
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab;}
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}

	protected void settingsChangedEvent(Event event) {
		RaritanPowerConfig config = proc.getConfig();
		config.address = addressTextbox.getText();
		config.port = portSpinner.getSelection();
		config.connectOnConfigLoad = connectOnLoadCheckbox.getSelection();
		
	}
	
	private void doUpdate(){
		settingsCtrl.doUpdate();
		

		String str = proc.serialComm().getLog().toString();
		int startPos = commLogTextbox.getCharCount();
		if(str.length() < startPos){
			commLogTextbox.setText(str);
		}else{
			String appendString = str.substring(startPos);
			commLogTextbox.append(appendString);
		}
		
		RaritanPowerConfig config = proc.getConfig();
		
		
		addressTextbox.setText(config.address);
		portSpinner.setSelection(config.port);
		connectOnLoadCheckbox.setSelection(config.connectOnConfigLoad);
		
		if(outletNamesTextbox.length != config.nOutlets || outletStateCheckbox.length != config.nOutlets) {
			for(int i=0; i < outletNamesTextbox.length; i++) {
				outletNumberLabel[i].dispose();
				outletNamesTextbox[i].dispose();
				outletStateCheckbox[i].dispose();
		    }
			outletNumberLabel = new Label[config.nOutlets];
			outletNamesTextbox = new Text[config.nOutlets];
			outletStateCheckbox = new Button[config.nOutlets];
		
			for(int i=0; i < config.nOutlets; i++) {
				outletNumberLabel[i] = new Label(outletsComp, SWT.NONE);
				outletNumberLabel[i].setText("Outlet #" + i + ":");			
				
				outletStateCheckbox[i] = new Button(outletsComp, SWT.CHECK);
				outletStateCheckbox[i].setText("On");
				final int j = i;
				outletStateCheckbox[i].addListener(SWT.Selection, new Listener() {
						@Override
						public void handleEvent(Event event) {
							proc.setOutlet(j, outletStateCheckbox[j].getSelection());
							
						}
					});
				
				outletNamesTextbox[i] = new Text(outletsComp, SWT.READ_ONLY);
				outletNamesTextbox[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		    }
			ImageProcUtil.wriggle(swtGroup); //resize gui
		}
		boolean state[] = proc.getOutletState();
		for(int i=0; i < config.nOutlets; i++) {
			outletNumberLabel[i].setText("Outlet #" + i + ":");
			if(config.outletNames == null || config.outletNames.length <= i || config.outletNames[i] == null) {
				outletNamesTextbox[i].setText("???");
				continue;
			}
			outletNamesTextbox[i].setText(config.outletNames[i]);
			outletStateCheckbox[i].setSelection(state[i]);
	    }
	}

	
}
