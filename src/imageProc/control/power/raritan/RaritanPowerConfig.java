package imageProc.control.power.raritan;

/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everyRaritanPower should be public
 */
public class RaritanPowerConfig {

	/** TCP address to connect to */
	public String address = "pdu-e3-qsk-1";
	
	/** TCP port to connect to */
	public int port = 9623;
	
	public boolean connectOnConfigLoad = false;
	
	public String username = "Enter username in config JSON";
	
	public String password = "Enter password in config JSON!";
	
	/** Names of each outlet (fetched from device) */
	public String[] outletNames = new String[0];
	
	public int nOutlets = 8;
	
	/** Send break control code 0x03 (^C) before each command (but not username/password) ... ugh */
	public boolean sendBreakBeforeCommand = false;
	
}
