package imageProc.control.power.raritan;

import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.power.raritan.swt.RaritanPowerSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.PowerControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Example module for controlling a RaritanPower by Serial */
public class RaritanPowerControl extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, PowerControl, LoggedComm.LogUser {

	/** Current configuration */
	private RaritanPowerConfig config = new RaritanPowerConfig();
	
	/** Class for communicating with TCP port (stolen from Arduino controller) */
	protected LoggedComm comm;
	
	/** Instance of the GUI controller connected to this */
	private RaritanPowerSWTControl swtController;

	/** State of outlets (fetched from device) */
	private boolean[] outletState = new boolean[0];
	
	private String lastLoadedConfig;
	
	public RaritanPowerControl() {
		comm = new LoggedComm(this);
	}
	
	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
		
		if(config.connectOnConfigLoad && !comm.isConnected())
			comm.connectTCP(config.address, config.port);
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, RaritanPowerConfig.class);
		
		updateAllControllers();	
	}

	public LoggedComm serialComm() { return comm; }

	public RaritanPowerConfig getConfig() { return config; }
	
	/** We are currently being given the outlet info for this outlet */
	private int currentOutletInfo = -1;
	@Override
	/** Called when a line of text was received from the serial port */
	public void receivedLine(String lineStr) {
		
		if(lineStr.contains("Login ")) {
			comm.send(config.username + "\n" + config.password + "\n");
		}else if(lineStr.contains("Welcome")) {
			Timer t = new Timer("initShowOutlets");
			t.schedule(new TimerTask() { @Override public void run() { 
				updateOutletStates();
				}}, 1000);
			
			
		}else if(lineStr.startsWith("Outlet ")) {
			
			// Outlet #: # ('Name')
			String[] parts = lineStr.substring(7).split(":");
			currentOutletInfo = Integer.parseInt(parts[0]) - 1;
			
			if(config.nOutlets <= currentOutletInfo 
					|| config.outletNames.length < config.nOutlets
					|| outletState.length < config.nOutlets) {
				config.nOutlets = currentOutletInfo + 1;
				config.outletNames = Arrays.copyOf(config.outletNames, config.nOutlets);
				outletState = Arrays.copyOf(outletState, config.nOutlets);
			}
			
			parts = parts[1].split("'");
			config.outletNames[currentOutletInfo] = parts.length > 1 ? parts[1] : parts[0];
			
		}else if(lineStr.startsWith("Power state:")) {
			//The power state comes on the next line
			
			outletState[currentOutletInfo] = lineStr.substring(12).trim().equals("On");
		}
		
		logChanged();
	}

	@Override
	/** Called when anyRaritanPower is written to the serial log. 
	 * Really only to tell the GUI to update all the boxes and RaritanPowers */
	public void logChanged() {
		updateAllControllers(); //force update of GUI components
	}

	@Override
	/** Called when serial port is connected.
	 * Add initial configuration here */
	public void connected() {
		updateAllControllers();
	}

	public void connect() {
		comm.connectTCP(config.address, config.port);
	}

	public void disconnect() {
		comm.disconnect();
	}
	
	public void setOutlet(int outlet, boolean on) {
		if(config.sendBreakBeforeCommand)
			comm.send("\03");
		comm.send("power outlets " + (outlet+1) + " " + (on ? "on" : "off") + " /y");
		if(config.sendBreakBeforeCommand)
			comm.send("\03");
		comm.send("show outlets " + (outlet+1));
		updateAllControllers();
	}
	
	/* --- Some RaritanPowers from ImageProc structure --- */
	@Override
	public boolean isIdle() { return true;	}
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new RaritanPowerSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}

	public void send(String text) {
		if(config.sendBreakBeforeCommand)
			comm.send("\03");
		comm.send(text);
	}

	@Override
	public boolean switchOn(String id) {
		int i = fromID(id);
		boolean wasOn = outletState[i];
		setOutlet(i, true);
		return !wasOn;
	}

	@Override
	public boolean switchOff(String id) {
		int i = fromID(id);
		boolean wasOn = outletState[i];
		setOutlet(i, false);
		return wasOn;
	}

	@Override
	public boolean isOn(String id) {
		return outletState[fromID(id)];
	}
	
	private int fromID(String id) {		
		//try as name first
		for(int i=0; i < config.nOutlets; i++) {
			if(id.equalsIgnoreCase(config.outletNames[i]))
				return i;
		}
		
		try {
			int i = Integer.parseInt(id);
			if(i >= 0 && i < config.nOutlets)
				return i;
		}catch(NumberFormatException err) { }
		
		throw new IllegalArgumentException("Outlet ID '" + id + "' does not match any outlet name and is not a number in range (0 - nOutlets)");

	}

	public void updateOutletStates() {
		comm.send("show outlets");
	}

	@Override
	public String toShortString() {
		return "Power";
	}

	public boolean[] getOutletState() { return outletState; }
}
