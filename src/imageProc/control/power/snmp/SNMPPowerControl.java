package imageProc.control.power.snmp;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Arrays;
import java.util.logging.Level;

import org.eclipse.swt.widgets.Composite;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.re2j.Pattern;

import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.power.snmp.swt.SNMPPowerSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.PowerControl;
import imageProc.core.EventReciever.Event;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Example module for controlling a thing by Serial */
public class SNMPPowerControl extends ImgSourceOrSinkImpl implements ImgSink, PowerControl, ConfigurableByID, EventReciever {

	/** Current configuration */
	private SNMPPowerConfig config = new SNMPPowerConfig();
	
	/** Message from last attempt to save the state */
	protected String lastStateSaveStatus;
		
	/** Instance of the GUI controller connected to this */
	private SNMPPowerSWTControl swtController;

	private String lastLoadedConfig;
	
	/** Return from each of config.infoOID */
	private String infos[];
	
	/** Current status of all outlets */
	private boolean outletState[];
	
	
	public SNMPPowerControl() {
	}
	
	@Override
	public void event(Event event) {
		switch(event) {
		default:
			break;
		}		
	}
	
	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, SNMPPowerConfig.class);
		
		updateAllControllers();	
	}


	public SNMPPowerConfig config() { return config; }
		
	
	/* --- Some things from ImageProc structure --- */
	@Override
	public boolean isIdle() { return true;	}
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new SNMPPowerSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}
	

	public void query() {

		if(config.outletNames == null || config.outletNames.length < config.numOutlets) {
			config.outletNames = new String[config.numOutlets];
			for(int i=0; i < config.numOutlets; i++) {
				config.outletNames[i] = "?" + i;
			}
		}
		
		try {
			TransportMapping transport = new DefaultUdpTransportMapping();
			Snmp snmp = new Snmp(transport);
			transport.listen();
			
			Address targetAddress = GenericAddress.parse("udp:" + config.host + "/161");
			CommunityTarget target = new CommunityTarget();
			target.setCommunity(new OctetString("public"));
			target.setAddress(targetAddress);
			target.setRetries(config.numRetries);
			target.setTimeout(config.timeout);
			target.setVersion(SnmpConstants.version2c);
			
			
			PDU pdu = new PDU();			
			pdu.setType(PDU.GET);
			
			infos = new String[config.infoOIDs.length];
			for(String[] set : config.infoOIDs) {
				pdu.add(new VariableBinding(new OID(set[1])));
			}
			
			for(int i=0; i < config.numOutlets; i++) {
				pdu.add(new VariableBinding(new OID(oidString(config.outletNameGroupOID, i))));
				pdu.add(new VariableBinding(new OID(oidString(config.outletGetStateGroupOID, i))));
			}			
			
			ResponseEvent event = snmp.send(pdu, target, null);
			
			for(int i=0; i < config.infoOIDs.length; i++) {
				Variable var = event.getResponse().get(i).getVariable();
				infos[i] = var.toString();
			}
			
			outletState = new boolean[config.numOutlets];
			for(int i=0; i < config.numOutlets; i++) {

				Variable var = event.getResponse().getVariable(new OID(oidString(config.outletNameGroupOID, i)));
				if(var.isException()) {
					config.outletNames[i] = "ERROR";
				}else {
					config.outletNames[i] = var.toString();
				}
				
				var = event.getResponse().getVariable(new OID(oidString(config.outletGetStateGroupOID, i)));
				if(var.isException()) {
					config.outletNames[i] = "ERROR";
					outletState[i] = false;
				}else {
					int syn = var.getSyntax();
					if(syn == 2) { // int32
						outletState[i] = (var.toInt() != 0);
						
					}else if(syn == 4) { //String
						outletState[i] = Integer.parseInt(var.toString()) != 0;
						
					} else {
						throw new RuntimeException("Unsupported SNMP response syntax " + var.getSyntax() + " (" + var.getSyntaxString() + ")");
					}
				}
				
			}
			
		}catch(IOException err) {
			logr.log(Level.WARNING, "Error during SNMP get", err);
			infos[0] = "SNMP ERROR " + err.getMessage();
			
		}
			
		updateAllControllers();
	}

	/**
	 * 
	 * @param templateStr
	 * @param i 0-based index
	 * @return
	 */
	private String oidString(String templateStr, int i) {
		int idx = config.firstOIDIndex + i;
		
		if(templateStr.contains("%i"))
			return templateStr.replaceAll("%i", Integer.toString(idx));
		else 
			return templateStr + "." + Integer.toString(idx);
		
	}

	public SNMPPowerConfig getConfig() { return config; }
	
	public String[] getInfos() { return infos == null ? new String[0] : infos; }

	public boolean[] getOutletState() { return outletState; }

	public void setOutlet(int idx, boolean selection) {
		try {
			TransportMapping transport = new DefaultUdpTransportMapping();
			Snmp snmp = new Snmp(transport);
			transport.listen();
			
			Address targetAddress = GenericAddress.parse("udp:" + config.host + "/161");
			CommunityTarget target = new CommunityTarget();
			//target.setCommunity(new OctetString("public"));
			target.setCommunity(new OctetString("private"));
			target.setAddress(targetAddress);
			target.setRetries(config.numRetries);
			target.setTimeout(config.timeout);
			target.setVersion(SnmpConstants.version2c);
						
			PDU pdu = new PDU();			
			
			String oidStr = selection ? config.outletSetOnStateGroupOID : config.outletSetOffStateGroupOID;
			if(config.outletSetIsGet) {
				pdu.setType(PDU.GET);
				pdu.add(new VariableBinding(new OID(oidString(oidStr, idx))));
			} else { 
				pdu.setType(PDU.SET);
				Variable var = new org.snmp4j.smi.Integer32(selection ? 1 : 0);
				pdu.add(new VariableBinding(new OID(oidString(oidStr, idx)), var));
			}

			ResponseEvent event = snmp.send(pdu, target, null);
			
			logr.info("SNMP Set returned: " + event.toString());
			//logr.info("SNMP Set returned: " + event.getResponse().get(0).getVariable().toString());
			

		}catch(IOException err) {
			logr.log(Level.WARNING, "Error during SNMP get", err);
			infos[0] = "SNMP ERROR " + err.getMessage();
		}

		updateAllControllers();
				
		//queue a query after configured delay
		if(config.queryDelayAfterSet > 0) {
			Timer t = new Timer("SNMPPowerQuery");
			t.schedule(new TimerTask() { @Override public void run() { query(); }}, config.queryDelayAfterSet);
			
		}else {
			query();
		}
	}

	@Override
	public boolean switchOn(String id) {
		return setState(id, true);
	}

	@Override
	public boolean switchOff(String id) {
		return setState(id, false);
	}
	
	public boolean setState(String id, boolean state) {
		checkArrays();
		int idx = fromID(id);
		
		boolean anySwitched = false;
		for(int i=0; i < config.numOutlets; i++) {
			//specific ID, or those that our config says we should control if id is null
			if(i == idx || (idx < 0 && config.controlOutlets[i])) {
				anySwitched |= (outletState[i] != state); 
				setOutlet(i, state);
			}
		}
		
		return anySwitched;
	}

	@Override
	public boolean isOn(String id) {
		checkArrays();
		int idx = fromID(id);
		
		boolean allOn = true;
		for(int i=0; i < config.numOutlets; i++) {
			//specific ID, or those that our config says we should control if id is null
			if(i == idx || (idx < 0 && config.controlOutlets[i])) {
				allOn &= outletState[i];
			}
		}
		
		return allOn;
	}
	
	private int fromID(String id) {	
		if(id == null || id.length() == 0)
			return -1;
		
		//try as name first
		for(int i=0; i < config.numOutlets; i++) {
			if(id.equalsIgnoreCase(config.outletNames[i]))
				return i;
		}
		
		try {
			int i = Integer.parseInt(id);
			if(i >= 0 && i < config.numOutlets)
				return i;
		}catch(NumberFormatException err) { }
		
		throw new IllegalArgumentException("Outlet ID '" + id + "' does not match any outlet name and is not a number in range (0 - nOutlets)");

	}
	
	/** Check that all config and state arrays match config.numOutlets */
	public void checkArrays() {
		if(config.numOutlets <= 0) {
			logr.warning("Invalid number of outlets, setting to 1");
			config.numOutlets = 1;
		}
		
		if(config.outletNames == null)
			config.outletNames = new String[config.numOutlets];
		if(config.controlOutlets == null)
			config.controlOutlets = new boolean[config.numOutlets];
		if(outletState == null)
			outletState = new boolean[config.numOutlets];

		if(config.outletNames.length <= config.numOutlets) {
			config.outletNames = Arrays.copyOf(config.outletNames, config.numOutlets);
		}if(config.controlOutlets.length <= config.numOutlets)
			config.controlOutlets = Arrays.copyOf(config.controlOutlets, config.numOutlets);
		if(outletState.length <= config.numOutlets)
			outletState = Arrays.copyOf(outletState, config.numOutlets);
		
		for(int i=0; i < config.numOutlets; i++)
			if(config.outletNames[i] == null)
				config.outletNames[i] = "UNKNOWN " + i;
	}
}
