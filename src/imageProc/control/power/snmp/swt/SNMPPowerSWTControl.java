package imageProc.control.power.snmp.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.power.snmp.SNMPPowerConfig;
import imageProc.control.power.snmp.SNMPPowerControl;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;

/** GUI control for example serial thing control module 
 * {@snippet :
 * 
 * }
 * */ 
public class SNMPPowerSWTControl implements ImagePipeController, ImagePipeSWTController  {

	private SNMPPowerControl proc;	
	
	private Group swtGroup;
	private CTabItem thisTabItem;
	
	private SWTSettingsControl settingsCtrl;
	
	private Text hostTextbox;
	private Label infoLabel;
	
	private Button queryButton;
	
	private Group outletsComp; 
	
	private Label[] outletNumberLabel;
	private Button[] outletStateCheckbox;
	private Button[] outletControlCheckbox;
	private Text[] outletNamesTextbox;
	
	
	public SNMPPowerSWTControl(SNMPPowerControl proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("SNMP Power");
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lHN = new Label(swtGroup, SWT.NONE); lHN.setText("Host:");
		hostTextbox = new Text(swtGroup, SWT.NONE);
	    hostTextbox.setText("?");
	    hostTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
	    hostTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    	    
	    Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
		queryButton = new Button(swtGroup, SWT.PUSH);
	    queryButton.setText("Query");
	    queryButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
	    queryButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.query(); } });
	    
	    Label lI = new Label(swtGroup, SWT.NONE); lI.setText("Info");
		infoLabel = new Label(swtGroup, SWT.NONE); 
		infoLabel.setText("?\n?\n?\n?\n?\n");
		infoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
	    	    
	    outletsComp = new Group(swtGroup, SWT.BORDER);
	    outletsComp.setText("Outlets");
	    outletsComp.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 5, 1));
	    outletsComp.setLayout(new GridLayout(4, false));
	    
	    outletNumberLabel = new Label[0];
	    outletStateCheckbox = new Button[0];
	    outletControlCheckbox = new Button[0];
	    outletNamesTextbox = new Text[0];
	    
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 5, 1));
		
		generalControllerUpdate();	
	}

	

	/* ---- Some things we need to define for ImageProc ----- */
	@Override
	public void destroy() { proc.destroy(); }
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab;}
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	protected void settingsChangedEvent(Event event) {
		SNMPPowerConfig config = proc.getConfig();
		
		config.host = hostTextbox.getText();
		
	}
	
	private void doUpdate(){
		SNMPPowerConfig config = proc.getConfig();
		proc.checkArrays();
		
		hostTextbox.setText(config.host);
		String[] infos = proc.getInfos();

		StringBuffer infoText = new StringBuffer(512);
		for(int i=0; i < Math.min(infos.length, config.infoOIDs.length); i++) {
			infoText.append(i + ": " + config.infoOIDs[i][0] + " = " + infos[i] + "\n");			
		}		
		infoLabel.setText(infoText.toString());

		if(outletNamesTextbox.length != config.numOutlets || outletStateCheckbox.length != config.numOutlets) {
			for(int i=0; i < outletNamesTextbox.length; i++) {
				outletNumberLabel[i].dispose();
				outletNamesTextbox[i].dispose();
				outletStateCheckbox[i].dispose();
				outletControlCheckbox[i].dispose();
		    }
			outletNumberLabel = new Label[config.numOutlets];
			outletNamesTextbox = new Text[config.numOutlets];
			outletStateCheckbox = new Button[config.numOutlets];
			outletControlCheckbox = new Button[config.numOutlets];
		
			for(int i=0; i < config.numOutlets; i++) {
				outletNumberLabel[i] = new Label(outletsComp, SWT.NONE);
				outletNumberLabel[i].setText("#" + i + ":");			

				final int j = i;
				outletStateCheckbox[i] = new Button(outletsComp, SWT.CHECK);
				outletStateCheckbox[i].setText("On");
				outletStateCheckbox[i].addListener(SWT.Selection, new Listener() {
						@Override
						public void handleEvent(Event event) {
							boolean toState = outletStateCheckbox[j].getSelection();
							
							//set the status back to what it was last queried as
							//and wait for query() to set it
							outletStateCheckbox[j].setSelection(proc.getOutletState()[j]);
							
							proc.setOutlet(j, toState);
							
						}
					});

				outletControlCheckbox[i] = new Button(outletsComp, SWT.CHECK);
				outletControlCheckbox[i].setText("Control");
				outletControlCheckbox[i].addListener(SWT.Selection, new Listener() {
						@Override
						public void handleEvent(Event event) {
							SNMPPowerConfig config = proc.getConfig();
							config.controlOutlets[j] = outletControlCheckbox[j].getSelection();
							
						}
					});
				
				outletNamesTextbox[i] = new Text(outletsComp, SWT.READ_ONLY);
				outletNamesTextbox[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		    }
			ImageProcUtil.wriggle(swtGroup); //resize gui
		}
		
		boolean[] state = proc.getOutletState();
				
		for(int i=0; i < config.numOutlets; i++) {
			outletNumberLabel[i].setText("Outlet #" + i + ":");
			outletNamesTextbox[i].setText(config.outletNames[i]);
			outletStateCheckbox[i].setSelection(state[i]);
			outletControlCheckbox[i].setSelection(config.controlOutlets[i]);
	    }
	}
	
}
