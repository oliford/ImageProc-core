package imageProc.control.power.snmp;

import imageProc.control.arduinoComm.SaveStateConfig;

/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everything should be public
 */
public class SNMPPowerConfig {

	/** Host to connect to */
	//public String host = "pdu-e3-qsk-1.ipp-hgw.mpg.de";
	public String host = "10.37.44.90";
	
	/** SNMP Context string (e.g. 'public') */
	public String community = "public";
	
	/** List of OIDs to get and display for general info */
	public String[][] infoOIDs = {
			{ "sysDesc", ".1.3.6.1.2.1.1.1.0"}, // sysDesc 
			{ "sysName", ".1.3.6.1.2.1.1.5.0"}  // SysName
	};
	
	/** Index into each OID group of the first power outlet control */
	public int firstOIDIndex = 1;
	
	/** OID of power control group (get)
	 * If it contains "%i", then this will be replaced with the index
	 * otherwise the index is added to the end with a "." */
	public String outletGetStateGroupOID = ".1.3.6.1.4.1.13742.6.4.1.2.1.2.1";
	
	/** OID of power control group (set) 
	 * If it contains "%i", then this will be replaced with the index
	 * otherwise the index is added to the end with a "." */
	public String outletSetOnStateGroupOID = ".1.3.6.1.4.1.13742.6.4.1.2.1.2.1";
	
	public String outletSetOffStateGroupOID = ".1.3.6.1.4.1.13742.6.4.1.2.1.2.1";
	
	/** Outlet sets are actually gets to different OIDs */
	public boolean outletSetIsGet = false;
	
	 /** OID to get name of power switch
 	  *  If it contains "%i", then this will be replaced with the index
	 * otherwise the index is added to the end with a "." */
	public String outletNameGroupOID = ".1.3.6.1.4.1.13742.6.3.5.3.1.3.1";
	
	/** Number of outlets */
	public int numOutlets = 10;
	
	/** Which outlets to turn on/off for this system */
	public boolean[] controlOutlets;

	public String[] outletNames;

	/** SNMP request timeout in ms */
	public long timeout = 1500;

	/** SNMP retries */
	public int numRetries = 2;

	/** How long after a set to query to see if it really changed [ms] (<=0 for immediately) */ 
	public long queryDelayAfterSet = -1;
	
}
