package imageProc.control.thing.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.arduinoComm.ArduinoCommConfig;
import imageProc.control.thing.ThingConfig;
import imageProc.control.thing.ThingControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SerialSWTGroup {
	public static final long KEEPALIVE_TIME_MS = 60000;
	private ThingControl proc;

	private Group swtGroup;
	private Label statusLabel;
	private Combo portDeviceTextbox;
	private Combo baudRateTextbox;
	private Button connectButton;
	private Button disconnectButton;	
	private Text sendTextbox;
	private Button sendButton;
	private Button clearButton;
	
	
	public SerialSWTGroup(Composite parent, ThingControl proc) {
		this.proc = proc;
		
		ThingConfig config = proc.config();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Comms");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        swtGroup.setLayout(new GridLayout(4, false));
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
	    statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
       
        Label lPN = new Label(swtGroup, SWT.NONE); lPN.setText("Device:");
	    portDeviceTextbox = new Combo(swtGroup, SWT.MULTI);
	    portDeviceTextbox.setItems(new String[]{ config.portName, "/dev/ttyS0", "/dev/ttyACM0" });
	    portDeviceTextbox.select(0);
	    portDeviceTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
	   
	    connectButton = new Button(swtGroup, SWT.PUSH);
	    connectButton.setText("Connect");
	    connectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    connectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { connectButtonEvent(event); } });

	    Label lB = new Label(swtGroup, SWT.NONE); lB.setText("Baud:");
	    baudRateTextbox = new Combo(swtGroup, SWT.MULTI);
	    baudRateTextbox.setItems(new String[]{ Integer.toString(config.baudRate),  "115200", "9600" });
	    baudRateTextbox.select((config.baudRate == 9600) ? 1 : 0);
	    baudRateTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
	   
	    disconnectButton = new Button(swtGroup, SWT.PUSH);
	    disconnectButton.setText("Disconnect");
	    disconnectButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    disconnectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { disconnectButtonEvent(event); } });

	    Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Send:");
	        
	    sendTextbox = new Text(swtGroup, SWT.NONE );
	    sendTextbox.setText("");
	    sendTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
	    
	    sendButton = new Button(swtGroup, SWT.PUSH);
	    sendButton.setText("Send");
	    sendButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    sendButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sendButtonEvent(event); } });

	    clearButton = new Button(swtGroup, SWT.PUSH);
	    clearButton.setText("Clear");
	    clearButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    clearButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { clearButtonEvent(event); } });
}
	

	private void connectButtonEvent(Event event) {
		ThingConfig config = proc.config();
		config.portName = portDeviceTextbox.getText();
		config.baudRate = Algorithms.mustParseInt(baudRateTextbox.getText());
		proc.serialComm().connectSerial(config.portName, config.baudRate);
	}


	private void disconnectButtonEvent(Event event) {
		proc.serialComm().disconnect();
	}
	
	private void sendButtonEvent(Event event) {
		proc.serialComm().send(sendTextbox.getText());
		sendTextbox.setText("");
	}
	
	private void clearButtonEvent(Event event) {
		proc.serialComm().clear();
		sendTextbox.setText("");
	}
	
	
	public void doUpdate() {
		statusLabel.setText(proc.serialComm().getStatus());
		
		ThingConfig config = proc.config();
		
		portDeviceTextbox.setText(config.portName);
		baudRateTextbox.setText(Integer.toString(config.baudRate));
		
	}

	public Group getSWTGroup(){ return swtGroup; }
}
