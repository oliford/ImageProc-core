package imageProc.control.thing.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.control.thing.ThingControl;


public class SaveStateSWTGroup {
	private ThingControl proc;
	
	private Group swtGroup;
	private Label statusLabel;
	private Text streamPathTextbox;
	private Button saveNowButton;
	private Button enableOnChangeCheckbox;
	
	public SaveStateSWTGroup(Composite parent, ThingControl proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Save State");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
	    statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
       
        Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Parlog path:");        
        streamPathTextbox = new Text(swtGroup, SWT.NONE );
        streamPathTextbox.setText("");
        streamPathTextbox.setToolTipText("Parlog path to save state to");
        streamPathTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
        streamPathTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
        streamPathTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
	    
	    Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");        
	    
	    saveNowButton = new Button(swtGroup, SWT.PUSH);
	    saveNowButton.setText("Save now");
	    saveNowButton.setToolTipText("Immediately save the current state to the parlog at the current timestamp");
	    saveNowButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
	    saveNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.saveStateNow(); } });
	    
	    enableOnChangeCheckbox = new Button(swtGroup, SWT.PUSH);
        enableOnChangeCheckbox.setText("Save on change");
        enableOnChangeCheckbox.setToolTipText("Save state to parlog everytime it changes");
        enableOnChangeCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
        enableOnChangeCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

        doUpdate();
	}

	protected void settingsChangedEvent(Event event) {
		proc.config().saveState.path = streamPathTextbox.getText();
		proc.config().saveState.autosave = enableOnChangeCheckbox.getSelection();
	}
	
	protected void doUpdate() {
		statusLabel.setText(proc.getLastStateSaveStatus());
		streamPathTextbox.setText(proc.config().saveState.path);
		enableOnChangeCheckbox.setSelection(proc.config().saveState.autosave);
	}

	public Control getSWTGroup() { return swtGroup; }
}
