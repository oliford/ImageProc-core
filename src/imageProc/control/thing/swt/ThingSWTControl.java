package imageProc.control.thing.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import imageProc.control.thing.ThingControl;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;

/** GUI control for example serial thing control module 
 * {@snippet :
 * 
 * }
 * */ 
public class ThingSWTControl implements ImagePipeController, ImagePipeSWTController  {

	private ThingControl proc;	
	
	private Group swtGroup;
	private CTabItem thisTabItem;
	private Text serialLogTextbox;	
	
	private CTabFolder swtTabFoler;	
	
	private CTabItem swtSerialTab;
	private SerialSWTGroup serialGroup;
	
	private CTabItem swtExampleTab;	
	private ExampleGroupSWTControl exampleGroup;
	
	private CTabItem swtSaveStateTab;
	private SaveStateSWTGroup saveStateGroup;
	
	private SWTSettingsControl settingsCtrl;
	
	public ThingSWTControl(ThingControl proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Arduino Comm");
		swtGroup.setLayout(new FillLayout());
		
		SashForm swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		
        serialLogTextbox = new Text(swtSashForm, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
	    serialLogTextbox.setText(proc.serialComm().getLog().toString());
	    
	    Composite swtBottomComp = new Composite(swtSashForm, SWT.NONE);
	    swtBottomComp.setLayout(new GridLayout(1, false));
		swtTabFoler = new CTabFolder(swtBottomComp, SWT.BORDER);
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        	        
		serialGroup = new SerialSWTGroup(swtTabFoler, proc);
		swtSerialTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSerialTab.setControl(serialGroup.getSWTGroup());
		swtSerialTab.setText("Comms");

		exampleGroup = new ExampleGroupSWTControl(swtTabFoler, proc);
		swtExampleTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtExampleTab.setControl(exampleGroup.getSWTGroup());
		swtExampleTab.setText("Example group");
		
		saveStateGroup = new SaveStateSWTGroup(swtTabFoler, proc);
		swtSaveStateTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSaveStateTab.setControl(saveStateGroup.getSWTGroup());
		swtSaveStateTab.setText("Save State");

		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtBottomComp, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		generalControllerUpdate();	
	}

	/* ---- Some things we need to define for ImageProc ----- */
	@Override
	public void destroy() { proc.destroy(); }
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab;}
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	private void doUpdate(){
		
		String str = proc.serialComm().getLog().toString();
		int startPos = serialLogTextbox.getCharCount();
		if(str.length() < startPos){
			serialLogTextbox.setText(str);
		}else{
			String appendString = str.substring(startPos);
			serialLogTextbox.append(appendString);
		}
		
		settingsCtrl.doUpdate();
		serialGroup.doUpdate();
		exampleGroup.doUpdate();
	}
	
}
