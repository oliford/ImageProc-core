package imageProc.control.thing.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.thing.ThingConfig;
import imageProc.control.thing.ThingControl;

public class ExampleGroupSWTControl {
	
	private ThingControl proc;
	
	private Group swtGroup;
	private Spinner positionSpinner;
	private Text nameTextBox;
	private Combo listOfStuffCombo;
	private Button setPositionButton;
	private Button doThingNowButton;
	private Button doThingOnTriggerCheckbox;

	public ExampleGroupSWTControl(Composite parent, ThingControl proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Example Group");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(4, false));
		
		// ------row 1------
		
		Label lTB = new Label(swtGroup, SWT.NONE); lTB.setText("Name:");
		nameTextBox = new Text(swtGroup, SWT.NONE);
		nameTextBox.setText("Do thing");
		nameTextBox.setToolTipText("Making the thing do a thing right now");
		nameTextBox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		nameTextBox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		// -----row 2-------
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Position of A:");
		positionSpinner = new Spinner(swtGroup, SWT.NONE);
		positionSpinner.setValues(500, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 100, 1000);
		positionSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		setPositionButton = new Button(swtGroup, SWT.PUSH);
		setPositionButton.setText("Set");
		setPositionButton.setToolTipText("Set the position into the thing");
		setPositionButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		setPositionButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setPositionButtonEvent(event); } });

		// ------row 3------

		doThingNowButton = new Button(swtGroup, SWT.PUSH);
		doThingNowButton.setText("Do thing");
		doThingNowButton.setToolTipText("Making the thing do a thing right now");
		doThingNowButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		doThingNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { doThingButtonEvent(event); } });

		doThingOnTriggerCheckbox = new Button(swtGroup, SWT.CHECK);
		doThingOnTriggerCheckbox.setText("Thing on software trigger");
		doThingOnTriggerCheckbox.setToolTipText("Making the thing do a thing when software start trigger is issued");
		doThingOnTriggerCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		doThingOnTriggerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		// ------row 4------
		
		Label lC = new Label(swtGroup, SWT.NONE); lC.setText("List of stuff:");
		listOfStuffCombo = new Combo(swtGroup, SWT.NONE);
		listOfStuffCombo.setItems(new String[]{ "toilet", "wallpaper", "chair", "ferry" });
		listOfStuffCombo.setToolTipText("List of random things");
		listOfStuffCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		listOfStuffCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

	}

	/** Called by GUI components to set the config to changes of the GUI */
	protected void settingsChangedEvent(Event event) {
		ThingConfig config = proc.config();
		config.selectedStuff = listOfStuffCombo.getText();
		config.nameOfAThing = nameTextBox.getText();
		config.doThingOnSoftwareStart = doThingOnTriggerCheckbox.getSelection();
	}

	protected void doThingButtonEvent(Event event) {
		proc.doThing();		
	}

	protected void setPositionButtonEvent(Event event) {
		ThingConfig config = proc.config();
		config.targetPositionOfA = positionSpinner.getSelection();
		
		proc.sendPosition();
	}
	
	/** Called by ImageProc to set the GUI to changes of the config */
	public void doUpdate() {
		ThingConfig config = proc.config();
		listOfStuffCombo.setText(config.selectedStuff);
		nameTextBox.setText(config.nameOfAThing);
		positionSpinner.setSelection(config.targetPositionOfA);
	}

	public Group getSWTGroup(){ return swtGroup; }
}

