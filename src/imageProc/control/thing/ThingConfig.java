package imageProc.control.thing;

import imageProc.control.arduinoComm.SaveStateConfig;

/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everything should be public
 */
public class ThingConfig {

	/** Serial port to connect to */
	public String portName;
	
	/** BAUD rate to connect serial port with */
	public int baudRate;
	
	/** Position of the thing A to set when pressing the button 'Set A' */
	public int targetPositionOfA;
	
	/** Send command to do a thing when the software start trigger is given 
	 * e.g. by the W7XPilot */
	public boolean doThingOnSoftwareStart;
	
	public String nameOfAThing = "defaultName";
	
	public String selectedStuff;

	/** Configuration of saving of state to somewhere */ 
	public SaveStateConfig saveState = new SaveStateConfig();
}
