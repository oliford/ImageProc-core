package imageProc.control.thing;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.thing.swt.ThingSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.EventReciever.Event;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Example module for controlling a thing by Serial */
public class ThingControl extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, EventReciever, LoggedComm.LogUser {

	/** Current configuration */
	private ThingConfig config = new ThingConfig();
	
	/** Curretn state */
	private ThingState state = new ThingState();

	/** Message from last attempt to save the state */
	protected String lastStateSaveStatus;
	
	/** Class for communicating with serial port (stolen from Arduino controller) */
	protected LoggedComm serialComm;
	
	/** Instance of the GUI controller connected to this */
	private ThingSWTControl swtController;

	private String lastLoadedConfig;
	
	public ThingControl() {
		config.portName = SettingsManager.defaultGlobal().getProperty("imageProc.thing.portName", "/dev/ttyS0");
		config.baudRate = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.thing.baudRate", "115200"));
		serialComm = new LoggedComm(this);
	}
	
	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalStart:
			if(config.doThingOnSoftwareStart)
				doThing();			
			break;
		default:
			break;
		}		
	}
	
	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, ThingConfig.class);
		
		updateAllControllers();	
	}

	public LoggedComm serialComm() { return serialComm; }

	public ThingConfig config() { return config; }
	
	public ThingState state() { return state; }
	
	public void doThing() {
		serialComm.send("DO_THING");
		connectedSource.setSeriesMetaData("ThingControl/PositionWhenThingDone", config.targetPositionOfA, false);
	}
		
	public void sendPosition(){
		serialComm.send("SET_POSITION " +config.nameOfAThing + " to " + config.targetPositionOfA);
	}

	@Override
	/** Called when a line of text was recieved from the serial port */
	public void receivedLine(String lineStr) {
		//pretend the thing sends us a position
		if(lineStr.startsWith("POSITION")) {
			state.positionOfA = Algorithms.mustParseInt(lineStr.substring(8));
		}
	}

	@Override
	/** Called when anything is written to the serial log. 
	 * Really only to tell the GUI to update all the boxes and things */
	public void logChanged() {
		updateAllControllers(); //force update of GUI components
	}

	@Override
	/** Called when serial port is connected.
	 * Add initial configuration here */
	public void connected() {
		sendPosition();		
	}

	
	/* --- Some things from ImageProc structure --- */
	@Override
	public boolean isIdle() { return true;	}
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new ThingSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}
	
	public String getLastStateSaveStatus() { return lastStateSaveStatus; }

	public void saveStateNow() {
		throw new RuntimeException("No general implementation for saveState");
	}
}
