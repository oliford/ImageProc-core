package imageProc.database.gmds;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;

public class GMDSPipeSWTControl implements ImagePipeController {
	protected GMDSPipe pipe;
	
	protected Group swtGroup;
	protected Combo swtExperimentCombo;
	protected Combo swtPathCombo;
	protected Spinner swtPulseSpinner;
	protected Button swtLoadSaveButton;
	protected Button swtSyncPulseButton;
	protected Button swtFindNextEmptyButton;
	protected Button swtEnableMemCacheCheckbox;
	protected Button saveInvalidCheckbox;
	protected Spinner maxImagesSpinner;
	protected Label swtInfoLabel;

	protected Text notesTextBox;
	
	//bit obsolete. Autoload is pointless and autosave is done by the Pilots now
	//protected Button swtAutoCheckbox; 
	
	
	protected boolean dataChangeInhibit = false;

	/** Default constructor only for use by derived classes that call init directly */
	protected GMDSPipeSWTControl() { }
	
	public GMDSPipeSWTControl(Composite parent, Integer style, GMDSPipe pipe) {
		init(parent, style, pipe);
	}

	protected void init(Composite parent, Integer style, GMDSPipe pipe) {
		swtGroup = new Group(parent, style);
		this.pipe = pipe;
		
		dataChangeInhibit = true;
		
		swtGroup.setLayout(new GridLayout(5, false));
		swtGroup.setText("MDS+ CIS " + ((pipe instanceof ImgSource) ? "(Source)" : "(Sink)"));
				
		Label lS = new Label(swtGroup, 0); lS.setText("Status:");
		swtInfoLabel = new Label(swtGroup, SWT.NONE);
		swtInfoLabel.setText("Initialising");
		swtInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lE = new Label(swtGroup, 0); lE.setText("Experiment:");
		swtExperimentCombo = new Combo(swtGroup, SWT.None);
		swtExperimentCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { dataChangingEvent(); } });
		swtExperimentCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lP = new Label(swtGroup, 0); lP.setText("Path:");
		swtPathCombo = new Combo(swtGroup, SWT.None);
		swtPathCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { dataChangingEvent(); } });
		swtPathCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		swtPathCombo.add("-- (Re)load list --");
		
		Label lPN = new Label(swtGroup, 0); lPN.setText("Pulse:");
		swtPulseSpinner = new Spinner(swtGroup, 0);
		swtPulseSpinner.setMinimum(0);
		swtPulseSpinner.setMaximum(Integer.MAX_VALUE);
		swtPulseSpinner.setSelection(-1);
		swtPulseSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtPulseSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { dataChangingEvent(); } });

		swtFindNextEmptyButton = new Button(swtGroup, SWT.PUSH);
		swtFindNextEmptyButton.setText("Next Empty");
		swtFindNextEmptyButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtFindNextEmptyButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { findNextEmptyEvent(event); } });

		swtSyncPulseButton = new Button(swtGroup, SWT.PUSH);
		swtSyncPulseButton.setText("Set To Metadata");
		swtSyncPulseButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtSyncPulseButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pulseToMetadataEvent(event); } });

		swtEnableMemCacheCheckbox = new Button(swtGroup, SWT.CHECK);
		swtEnableMemCacheCheckbox.setText("Enable Mem Cache");		
		swtEnableMemCacheCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { enableCacheButtonEvent(event); } });
		
		
		swtLoadSaveButton = new Button(swtGroup, SWT.PUSH);
		
		//swtAutoCheckbox = new Button(swtGroup, SWT.CHECK);
		//swtAutoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		//swtAutoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { autoCheckboxChangeEvent(event); } });
		
		
		if(pipe instanceof ImgSource){
			swtLoadSaveButton.setText("Load");
			swtLoadSaveButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadButtonEvent(event); } });
			
			//swtAutoCheckbox.setText("Auto Load");
			//swtAutoCheckbox.setToolTipText("Load images immediately when pulse number is changed");
		
		}else{
			swtLoadSaveButton.setText("Set/Save");
			swtLoadSaveButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
			
			//swtAutoCheckbox.setText("Auto Save");
			//swtAutoCheckbox.setToolTipText("Save images to current pulse when image set or image content changes");
				
			saveInvalidCheckbox = new Button(swtGroup, SWT.CHECK);
			saveInvalidCheckbox.setText("Save Invalid");
			saveInvalidCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveSettingsChangeEvent(event); } });
			saveInvalidCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
			
		}
		Label lSM = new Label(swtGroup, SWT.NONE); lSM.setText("Max images:");
		maxImagesSpinner = new Spinner(swtGroup, SWT.NONE);
		maxImagesSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 10, 100);
		maxImagesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { maxImagesSpinnerEvent(event); } });
		maxImagesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		

		
		
		addExtensionsSWT1();
				
		notesTextBox = new Text(swtGroup, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER );
		GridData notesGD = new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1);
		notesGD.heightHint = 500;
		notesTextBox.setLayoutData(notesGD);
		//notesTextBox.setText("\n\n\n\n");		
		notesTextBox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { notesChangingEvent(); } });
		
		
		addExtensionsSWT2();
		
		swtExperimentCombo.setText(pipe.getExperiment());
		swtPathCombo.setText(pipe.getPath());
		swtPulseSpinner.setSelection(pipe.getPulse());
		
		//swtExperimentCombo.setText(pipe.getDefaultExperiment());
		//swtPathCombo.setText(pipe.getDefaultPath());
		//swtPulseSpinner.setSelection(pipe.getDefaultPulse());
		
		addExtensionsSWT3();

		repopulateExpCombo();
		dataChangeInhibit = false;
		generalControllerUpdate();
	}
	
	protected void addExtensionsSWT1() { }
	protected void addExtensionsSWT2() { }
	protected void addExtensionsSWT3() { }

	

	public void notesChangingEvent(){
		String notesGUI = notesTextBox.getText();
		String notesMDS = pipe.getNotes(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
		if(!notesGUI.equals(notesMDS))
			pipe.setNotes(swtExperimentCombo.getText(), 
					swtPulseSpinner.getSelection(),
					notesGUI);
	}
		
	public void dataChangingEvent(){
		if(dataChangeInhibit)
			return;
		
		dataChangeInhibit = true;
		
		setTextColour();
		
		String notesGUI = notesTextBox.getText();
		String notesMDS = pipe.getNotes(swtExperimentCombo.getText(), swtPulseSpinner.getSelection());
		if(!notesGUI.equals(notesMDS))
			notesTextBox.setText(notesMDS);
		
		
		/*if((pipe instanceof GMDSSource) &&
				swtAutoCheckbox.getSelection() &&
				!(swtExperimentCombo.getText().equals(pipe.getExperiment()) && 
						swtPathCombo.getText().equals(pipe.getPath()) && 
						swtPulseSpinner.getSelection() == pipe.getPulse())
				){
			
				((GMDSSource)pipe).loadTarget(
						swtExperimentCombo.getText(), 
						swtPulseSpinner.getSelection(),
						swtPathCombo.getText());
		}*/
		
		repopulateExpCombo();
		
		if("-- (Re)load list --".equals(swtPathCombo.getText())){
			repopulatePathCombo();
		}
		
		dataChangeInhibit = false;
	}
	
	private void repopulatePathCombo() {		
		//String current = swtPathCombo.getText(); //never change the current
		
		swtPathCombo.setItems(pipe.getAvailableImageSets(swtExperimentCombo.getText(), swtPulseSpinner.getSelection()));
		swtPathCombo.add("-- (Re)load list --");
		
		swtPathCombo.select(0);
		//swtPathCombo.setText(current);
	}
	
	private void repopulateExpCombo() {		
		String current = swtExperimentCombo.getText(); //never change the current
		
		swtExperimentCombo.setItems(GMDSPipe.getAvailableExperiments());
		swtExperimentCombo.setText(current);
	}
	
	
	public void setTextColour(){
		Color textCol;
		if(swtExperimentCombo.getText().equals(pipe.getExperiment()) && 
				swtPathCombo.getText().equals(pipe.getPath()) && 
				swtPulseSpinner.getSelection() == pipe.getPulse()){
			
			if(pipe instanceof GMDSSource){
				textCol = swtGroup.getDisplay().getSystemColor(((GMDSSource)pipe).isLoading() ? SWT.COLOR_RED : SWT.COLOR_BLACK);
				//swtLoadSaveButton.setEnabled(false);
				
			}else{
				textCol = swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLACK);
			}
			
		}else{
			textCol = swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLUE);
			//swtLoadSaveButton.setEnabled(true);
		}
		
		swtExperimentCombo.setForeground(textCol);
		swtPathCombo.setForeground(textCol);
		swtPulseSpinner.setForeground(textCol);
		
	}
	
	public void autoCheckboxChangeEvent(Event event){
		if(pipe instanceof GMDSSink){
			//boolean autoSave = swtAutoCheckbox.getSelection();
			//((GMDSSink)pipe).setAutoSave(autoSave);
			((GMDSSink)pipe).setData(
					swtExperimentCombo.getText(), 
					swtPulseSpinner.getSelection(),
					swtPathCombo.getText());
			
		}
	}
		
	public void loadButtonEvent(Event event){
		((GMDSSource)pipe).loadTarget(
				swtExperimentCombo.getText(), 
				swtPulseSpinner.getSelection(),
				swtPathCombo.getText());
		
		//colour change will be triggered by the controller update
	}
	
	private void findNextEmptyEvent(Event event){
		int pulse = pipe.findNextEmpty(swtExperimentCombo.getText(), 
				swtPulseSpinner.getSelection(),
				swtPathCombo.getText());
		swtPulseSpinner.setSelection(pulse);
	}
	
	private void pulseToMetadataEvent(Event event) {
		pipe.setToMetadata();
		swtPulseSpinner.setSelection(pipe.getPulse());
	}

	public void enableCacheButtonEvent(Event event){
		pipe.setCacheEnable(swtEnableMemCacheCheckbox.getSelection());		
	}
	
	public void maxImagesSpinnerEvent(Event event){
		pipe.setMaxImages(maxImagesSpinner.getSelection());		
	}
	
	
	public void saveSettingsChangeEvent(Event event){
		((GMDSSink)pipe).setSaveInvalid(saveInvalidCheckbox.getSelection());
	}

	public void saveButtonEvent(Event event){
		((GMDSSink)pipe).setData(
				swtExperimentCombo.getText(), 
				swtPulseSpinner.getSelection(),
				swtPathCombo.getText());
		
		((GMDSSink)pipe).save();
	}
	
	public GMDSPipe getSource() { return pipe; };

	@Override
	public Object getInterfacingObject() { return swtGroup; }

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}

	protected void doUpdate(){
		
		
		String statStr = (pipe.isLoadingOrSaving() ? "Active, " : "Idle, ") +
							pipe.getExperiment() + "/" + pipe.getPulse() + ": " +
		                     pipe.nImagesDone() + " / " + pipe.nImagesTotal();
						
		swtInfoLabel.setText(statStr);
					
		/*if(pipe instanceof GMDSSink){
			boolean autoSave = ((GMDSSink)pipe).getAutoSave();
			swtAutoCheckbox.setSelection(autoSave);
		}*/
	}
	
	@Override
	public void destroy() {
		swtGroup.dispose();		
		pipe.controllerDestroyed(this);
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return pipe; }
}
