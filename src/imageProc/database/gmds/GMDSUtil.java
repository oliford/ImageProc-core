package imageProc.database.gmds;

import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import mds.GMDSFetcher;
import otherSupport.SettingsManager;

public abstract class GMDSUtil {

	public static GMDSFetcher globalGMDS() {
		String serverID = SettingsManager.defaultGlobal().getProperty("imageProc.cis.gmdsServerID", "pccis");
		GMDSFetcher gmds = GMDSFetcher.defaultInstance(serverID);
		gmds.setUseMemoryCache(false); //not for this app, we want to keep memory allocs fast
		return gmds;
	}

	public static String getMetaExp(ImgSource imgSrc){
		String experiment = null;
		if(imgSrc != null){
			Object obj = imgSrc.getSeriesMetaData("/gmds/experiment");
			if(obj != null){
				experiment = (obj instanceof String[]) ? ((String[])obj)[0] : (String)obj;
			}
		}
		if(experiment == null){
			return SettingsManager.defaultGlobal().getProperty("imageProc.cis.experiment", "DEFAULT");			
		}
		return experiment;
	}

	/** Gets database ID from metadata of a source */
	public static int getMetaDatabaseID(ImgSource imgSrc){
		return ImageProcUtil.getMetaInteger(imgSrc, "/gmds/pulse");
	}

	/** Gets AUG pulse from metadata of a source */
	public static int getMetaAUGShot(ImgSource imgSrc){
		return ImageProcUtil.getMetaInteger(imgSrc, "/aug/pulse");
	}
	
}
