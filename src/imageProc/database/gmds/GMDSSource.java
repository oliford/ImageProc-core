package imageProc.database.gmds;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import binaryMatrixFile.DataConvertPureJava;
import descriptors.SignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSource;
import mds.GMDSFetcher;
import mds.MdsPlusException;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.Signal;
import signals.gmds.GMDSSignal;

/** Loads images from MDS+ source stored as in J.Howard's LabView software */
public class GMDSSource extends GMDSPipe implements ImgSource {

	// public ByteBufferImage images[];

	BulkImageAllocation<ByteBufferImage> imageAlloc = new BulkImageAllocation<ByteBufferImage>(
			this);

	private int nImagesLoaded;
	
	public GMDSSource(SettingsManager settingsMgr) { super(settingsMgr); }

	public GMDSSource() { super();	}

	public GMDSSource(GMDSFetcher gmds, String experiment, String path,
			int pulse) {
		super(gmds);

		loadTarget(experiment, pulse, path);
	}

	public void load() {
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() {
			@Override
			public void run() {
				doLoad();
			}
		});
	}

	public void doLoad() {
		nImagesLoaded = 0;

		int pulse = this.pulse;
		String experiment = this.experiment;
		String path = this.path;
		abortCalc = false;
		isLoadingOrSaving = true;
		
		// take the controllers that we're loading
		updateAllControllers();

		// and hang the experiment, pulse etc on the image series so stuff knows
		// where to write direct data
		setSeriesMetaData("/gmds/experiment", experiment, false);
		setSeriesMetaData("/gmds/pulse", pulse, false);
		setSeriesMetaData("/notes", getNotes(experiment, pulse), false);

		// first see if it's all in one sequence
		GMDSSignalDesc imgDesc = new GMDSSignalDesc(pulse, experiment, path);// + "/IMAGES");
		try {
			System.out.println("GMDSSource: Attempt read of image sequence from signal '" + imgDesc.toString() + "'.");
			Signal imgSig = gmds.getSig(imgDesc);
			readCombinedSequence(imgSig);

			System.out.println("GMDSSource: Load complete from combined sequence.");
			
		} catch (MdsPlusException err) {
			int nImages = 0;

			// otherwise try separated images
			try {
				GMDSSignalDesc countDesc = new GMDSSignalDesc(pulse, experiment, path + "/IMAGE_COUNT");
				System.out.println("GMDSSource: No combined sequence, attempting read from signal '"
								+ imgDesc.toString() + "'.");
				Signal countSig = gmds.getSig(countDesc);
				nImages = ((int[]) countSig.getData())[0];

			} catch (MdsPlusException e) {
				nImages = -1;
			}

			try {

				// make some pre-emptive attempt to see how many images there
				// really are
				String sigs[] = gmds.dumpTree(experiment, pulse, "", true);
				int maxImgNo = 0;
				for (String sigName : sigs) {
					if (sigName != null && sigName.matches("[/]*" + path + "[/]*/IMAGE_[0-9]*.*")) {
						int imgNo = Algorithms.mustParseInt(sigName .replaceFirst("[/]*" + path + "[/]*IMAGE_([0-9]*).*", "$1"));
						if (imgNo > maxImgNo)
							maxImgNo = imgNo;
					}
				}

				if (maxImgNo > 0 && maxImgNo != nImages - 1) {
					System.err.println("GMDSSource: WARNING: Last image found had number '"
									+ maxImgNo
									+ "' but _COUNT reported "
									+ nImages + ", using max.");
					nImages = maxImgNo + 1;
				}

				if (maxImages >= 0 && nImages > maxImages)
					nImages = maxImages;

				updateAllControllers();
				
				loadMetaData(nImages);
				setSeriesMetaData("gmds/cacheFileCreateNanotime", new long[nImages], true);

				readSeparateImages(nImages);

				System.out.println("MDS+ load done");

			} catch (MdsPlusException err2) {
				System.err
						.println("Unable to read either combined image sequence or image[0] from MDS+");
				nImagesLoaded = 0;
				notifyImageSetChanged();
				updateAllControllers();
			}
		}

		setSeriesMetaData("/gmds/experiment", experiment, false);
		setSeriesMetaData("/gmds/pulse", pulse, false);

		isLoadingOrSaving = false;
		updateAllControllers();
	}
	
	@Override
	public int nImagesDone() { return nImagesLoaded; }
	
	@Override
	public int nImagesTotal() { return getNumImages(); };

	/**
	 * Images saved J Howard's LabView program are saved in a single long signal
	 * and are always short[][][]
	 */
	private void readCombinedSequence(Signal imgSig) {
		int[][][] src = (int[][][]) imgSig.getData();
		short[][][] imageData = new short[src.length][src[0].length][src[0][0].length];
		//short imageData[][][] = (short[][][]) imgSig.getData();
		Mat.convertMultiDimArray(src, imageData);
		int nImages = imageData.length;
		int height = imageData[0].length;
		int width = imageData[0][0].length;
		int bitDepth = 12; // err, where is this stored?
		ByteOrder byteOrder = ByteOrder.BIG_ENDIAN; // ... why??

		if (maxImages >= 0 && nImages > maxImages)
			nImages = maxImages;

		nImagesLoaded = 0;
		updateAllControllers();

		ByteBufferImage templateImage = new ByteBufferImage(null, -1, width,
				height, bitDepth, false);

		if (imageAlloc.reallocate(templateImage, nImages)) {
			notifyImageSetChanged();
		} else {
			imageAlloc.invalidateAll();
		}

		ByteBufferImage images[] = imageAlloc.getImages();

		for (int i = 0; i < nImages; i++) {
			WriteLock writeLock = images[i].writeLock();
			try {
				writeLock.lockInterruptibly();
			} catch (InterruptedException e) {
				images[i].invalidate();
				continue;
			}
			try{ 
				ByteBuffer buf = images[i].getWritableBuffer(writeLock);
				buf.order(byteOrder);
	
				ShortBuffer iBuff = buf.asShortBuffer();
				for (int iY = 0; iY < height; iY++) {
					iBuff.put(imageData[i][iY]);
				}
	
				System.out.println("GMDSSource: Img[" + i + " / " + nImages + "] read complete.");
	
				nImagesLoaded = i + 1;
				updateAllControllers();
	
				if (abortCalc)
					return;
			}finally{
				writeLock.unlock();
			}
			images[i].imageChanged(false);
		}

	}

	/**
	 * Images saved by GMDSSink have a _COUNT signal, and then _### signals for
	 * the images. They can in principal be any type.
	 * 
	 * @param nImages
	 */
	private void readSeparateImages(int nImages) {
		ByteOrder byteOrder = ByteOrder.LITTLE_ENDIAN;

		int nFields = -1, width = -1, height = -1;

		ByteBufferImage images[] = null;

		for (int i = 0; i < nImages; i++) {

			SignalDesc imgDesc = new GMDSSignalDesc(pulse, experiment, path + "/IMAGE_" + i);
			Signal imgSig;
			try {
				imgSig = gmds.getSig(imgDesc);
				
				//create a metadata entry with timestamps from image files
				try {
					String cacheFile = gmds.getCacheRoot() + "/" + imgDesc.pathInCache();
					FileTime ft = (FileTime) Files.getAttribute(Path.of(cacheFile), "creationTime");
					long nanotimeCreate = ft.toMillis() * 1_000_000L;
					if(i > 0) {					
						Object last = getImageMetaData("gmds/cacheFileCreateNanotime", i-1);
						if(last != null && last instanceof Long && nanotimeCreate <= ((Long)last)) {
							nanotimeCreate = ((Long)last) + 1;
							System.err.println("WARNING: Nudged gmds/cacheFileCreateNanotime["+i+"] because <= ["+(i-1)+"]");
						}
					}
					setImageMetaData("gmds/cacheFileCreateNanotime", i, nanotimeCreate);
					
				}catch(IOException err){
					System.err.println("WARNING: Unable to get creation time of GMDS cache file: " + err.getMessage());
				}
				
			} catch (MdsPlusException err) {
				System.err.println("GMDSSource: Error reading image " + i + " from signal '" + imgDesc + "'.");
				if (images != null && images.length > i && images[i] != null)
					images[i].invalidate();
				continue;
			}

			int rank = imgSig.getRank();
			if(rank < 2 || rank > 3)
				throw new RuntimeException("Invalid rank " + rank + " of image in signal " + imgDesc);
						
			Object imageData = imgSig.getData();
			Class dataType;
			Object nextLevel = imageData;
			
			int nF = 1;			
			if(rank == 3){
				nF = Array.getLength(nextLevel);
				nextLevel = Array.get(nextLevel, 0);
			}
			
			int h = Array.getLength(nextLevel);
			nextLevel = Array.get(nextLevel, 0);
			int w = Array.getLength(nextLevel);
			dataType = nextLevel.getClass().getComponentType();
			
			if (width > 0) {
				if (nFields != nF || height != h || width != w) {
					System.err.println("Image in signal " + imgDesc + " is " + w + "x" + h + " x " + nF + " but first was " + width + "x" + height + "x" + nFields);
					images[i].invalidate();
					continue;
				}

			} else {
				// now we have width/height info, do bulk allocation
				nFields = nF;
				height = h;
				width = w;

				int bitDepth = 0;
				
				if (dataType == short.class) {
					bitDepth = 16;
				} else if (dataType == int.class) {
					bitDepth = 32;
				} else if (dataType == double.class) {
					bitDepth = -64;
				} else{
					throw new RuntimeException("Unknown data type");
				}

				ByteBufferImage templateImage = new ByteBufferImage(null, -1,
						width, height, nFields, bitDepth, false);
				templateImage.setByteOrder(byteOrder);
				if (imageAlloc.reallocate(templateImage, nImages)) {
					notifyImageSetChanged();
				} else {
					imageAlloc.invalidateAll();
				}

				images = imageAlloc.getImages();

			}

			WriteLock writeLock = images[i].writeLock();
			try {
				writeLock.lockInterruptibly();
			} catch (InterruptedException e) {
				images[i].invalidate();
				continue;
			}

			try {
				ByteBuffer buf = images[i].getWritableBuffer(writeLock);
			
			
				if (dataType == short.class) {
					ShortBuffer iBuff = buf.asShortBuffer();
					for(int iF=0; iF < nFields; iF++){
						Object fieldData = (rank == 3) ? Array.get(imageData, iF) : imageData;				
						for (int iY = 0; iY < height; iY++) {
							iBuff.put(((short[][]) fieldData)[iY]);
						}
					}		
				} else if (dataType == int.class) {
					IntBuffer iBuff = buf.asIntBuffer();
					for(int iF=0; iF < nFields; iF++){
						Object fieldData = (rank == 3) ? Array.get(imageData, iF) : imageData;
							for (int iY = 0; iY < height; iY++) {
								iBuff.put(((int[][]) fieldData)[iY]);
							}
					}

				} else if (dataType == double.class) {
					DoubleBuffer iBuff = buf.asDoubleBuffer();
					for(int iF=0; iF < nFields; iF++){
						Object fieldData = (rank == 3) ? Array.get(imageData, iF) : imageData;
						for (int iY = 0; iY < height; iY++) {
							iBuff.put(((double[][]) fieldData)[iY]);
						}
					}
				} 
			
			} finally {
				writeLock.unlock();
			}

			images[i].imageChanged(false);
			
			System.out.println("GMDSSource: Img[" + i + " / " + images.length
					+ "] read complete.");

			nImagesLoaded = i + 1;
			updateAllControllers();

			if (abortCalc)
				return;
		}

		// still no size info??
		if (images == null) {
			System.err
					.println("GMDSSource: Never found any size info to allocate for "
							+ nImages + " images.");
			imageAlloc.destroy();
		}

		updateAllControllers();

	}

	private void loadMetaData(int nImages) {
		seriesMetaData.clear();

		setSeriesMetaData("/gmds/experiment", experiment, false);
		setSeriesMetaData("/gmds/pulse", pulse, false);
		setSeriesMetaData("/notes", getNotes(experiment, pulse), false);

		loadMetaData(nImages, true); // the old sequence data (pre Nov2013)
		loadMetaData(nImages, false);

		// hackery
		String notes = getNotes(experiment, pulse);
		Pattern p = Pattern.compile("AUGPULSE=([0-9]*)",
				Pattern.CASE_INSENSITIVE);

		Matcher matcher = p.matcher(notes);
		if (matcher.find()) {
			String pulseStr = matcher.group(1);
			try {
				int pulse = Integer.parseInt(pulseStr);
				seriesMetaData.put("/aug/pulse", pulse, false);
			} catch (NumberFormatException e) {
			}
		}

	}

	private void loadMetaData(int nImages, boolean seqData) {
		// look for the new one first
		String rootPath = path + (seqData ? "/seqData" : "/seriesData");
		String sigsPaths[] = gmds.dumpTree(experiment, pulse, rootPath, true);
		LinkedList<String> sigPathList = new LinkedList<String>();
		for (String signalPath : sigsPaths)
			sigPathList.add(signalPath);
		
		//move 'time' thing to the top, so we do it first to check the time series length
		if(sigPathList.removeFirstOccurrence("/time"))
			sigPathList.add(0, "/time");	

		int timeLength = nImages;

		for (String signalPath : sigPathList) {
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment,
					rootPath + "/" + signalPath);

			try {
				GMDSSignal sig = (GMDSSignal) gmds.getSig(sigDesc);

				Object data = sig.getData();
				
				if (seqData) {
					// we put the old sequence data in under seqXXX incase it
					// clashes with a series (I don't think it ever did)
					seriesMetaData.put("/seq" + signalPath, data, false);
				} else {					
					boolean isTimeSeries = false;
					if(data.getClass().isArray()){ 
						int len = Array.getLength(data);
						if(len == timeLength || len == nImages 
								|| len == maxImages){ //<-- hack to force recognise timebases
							isTimeSeries = true;
						}else if(signalPath.endsWith("nanoTimeA") || signalPath.equals("/time")){							
							System.err.println("WARNING: MetaData Time like thing '"+signalPath+"' was length "+len+" but there are " + nImages +" images. Will now match wither for time-series metadata");
							isTimeSeries = true;
							timeLength = len;
						}
						
								
					}
					seriesMetaData.put(signalPath, data, isTimeSeries);
				}

			} catch (RuntimeException err) {
				System.err.println("Couldn't read MetaData signal '" + sigDesc
						+ "': " + err.getMessage());
			}
		}
	}

	public void loadTarget(String experiment, int pulse, String path) {

		nImagesLoaded = 0;
		this.experiment = experiment;
		this.pulse = pulse;
		this.path = path;
		this.abortCalc = true; // cancel any existing run

		load();

	}

	@Override
	public int getNumImages() {
		ByteBufferImage images[] = imageAlloc.getImages();
		return (images == null) ? 0 : images.length;
	}

	@Override
	public Img getImage(int imgIdx) {
		ByteBufferImage images[] = imageAlloc.getImages();
		return (images != null && imgIdx >= 0 && imgIdx < images.length) ? images[imgIdx]
				: null;
	}

	@Override
	public Img[] getImageSet() {
		Img imageSet[] = new Img[nImagesLoaded];
		for (int i = 0; i < imageSet.length; i++)
			imageSet[i] = getImage(i);
		return imageSet;
	}

	@Override
	public GMDSSource clone() {
		return new GMDSSource(gmds, experiment, path, pulse);
	}

	public boolean isLoading() {
		return !isIdle();
	}

	@Override
	public void destroy() {
		super.destroy();
		imageAlloc.destroy();
	}

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "GMDSLoad"; 
	}
}
