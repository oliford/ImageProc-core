package imageProc.database.gmds;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ByteBufferImage;
import imageProc.core.DatabaseSink;
import imageProc.core.DoubleFlatArrayImage;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import mds.GMDSFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.gmds.GMDSSignal;

public class GMDSSink extends GMDSPipe implements ImgSink, DatabaseSink {
	
	private boolean autoSave = false; 
	
	private int lastWritten = -1;

	private int imagesInChangeID[] = null;
	private boolean saveInvalid = false;
	
	public boolean lastSaveCompleted = false;
	
	public GMDSSink() { super(); }

	public GMDSSink(GMDSFetcher gmds) { super(gmds); }
	
	public GMDSSink(SettingsManager settingsMgr) { super(settingsMgr); }

	
 	@Override
	public void imageChangedRangeComplete(int idx) { 
		if(autoSave)
			save();
	}

	@Override
	public void notifySourceChanged() {
		if(autoSave)
			save();
	}	
	
	/** Save the current image set */
	public void save(){
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doSequenceSaveCheck(); } }); 
	}
	
	/** The actual save operation, run by the workers */
	private void doSequenceSaveCheck() {
		abortCalc = false;
		
		if(path.matches("/*RAW/*") && !(connectedSource instanceof AcquisitionDevice)){
			throw new RuntimeException("SAFETY: Refusing to write 'RAW' data because source isn't an AcquisitionDevice");
		}
		
		if(connectedSource == null) 
			return;
		int nImages = connectedSource.getNumImages();
		
		isLoadingOrSaving = true;
		lastSaveCompleted = false;
		
		//sometimes this only saves 1 image, no idea why
		//to stop us losing data, keep retrying the image count until we get more than 10 images
		for(int i=0; i < 10; i++){
			System.out.println("GMDSSink: 1 image fault workaround: i="+i+", nImages="+nImages+".");
			if(nImages > 10)
				break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) { }
		}
			
		System.out.println("GMDSSink: nImages="+nImages+", maxImages=" + maxImages);
		if(maxImages >= 0 && nImages > maxImages)
			nImages = maxImages;
		
		//make sure everyone knows where it has been written
		connectedSource.setSeriesMetaData("gmds/experiment", new String(experiment), false);
		connectedSource.setSeriesMetaData("gmds/pulse", new Integer(pulse), false);
		
		doSaveMetaData(); //do this now, to get the info out to disk quickly
		
		GMDSSignalDesc imgDesc = new GMDSSignalDesc(pulse, experiment, path + "/IMAGE_COUNT");
		GMDSSignal countSig = new GMDSSignal(imgDesc, new int[]{ nImages });
		gmds.writeToCache(countSig);	
				        
		if(imagesInChangeID == null)
			imagesInChangeID = new int[nImages];
		else if(imagesInChangeID.length != nImages){
			imagesInChangeID = Arrays.copyOf(imagesInChangeID, nImages);
		}

		int maxImgNo = -1;
		for(int i=0; i < nImages; i++){
			Img imageIn = connectedSource.getImage(i);
			if(imageIn == null || (!saveInvalid && !imageIn.isRangeValid()))
				continue;
			
			maxImgNo = i;
			
//			if(imageIn.getChangeID() != imagesInChangeID[i]){
			
				System.out.println("CISSink: Img["+i+"] has changed or been modified, writing to MDS+.");
				
				lastWritten = i;
				updateAllControllers();
				
				writeImage(i, imageIn);
				
				if(abortCalc){
					break;
				}
								
				updateAllControllers();

	//		}else{
		//		System.out.println("CISSink: Img["+i+"] has not changed.");
			//}
		}
		
		//rewrite count signal with the max valid images we actually found
		System.out.println("CISSink: Valid images up to #" + maxImgNo + " written.");
		imgDesc = new GMDSSignalDesc(pulse, experiment, path + "/IMAGE_COUNT");
		countSig = new GMDSSignal(imgDesc, new int[]{ maxImgNo + 1 });
		gmds.writeToCache(countSig);	
		
		
		doSaveMetaData(); //do this again, to make sure we get the final state of the metadata
		
		//did it complete OK?
		lastSaveCompleted = (lastWritten == (nImages-1));
		
		isLoadingOrSaving = false;
		updateAllControllers();
	}
	
	@Override
	public int nImagesTotal() { return connectedSource.getNumImages(); }
	
	@Override
	public int nImagesDone() { return lastWritten; }
	
	private void doSaveMetaData() {
		
		MetaDataMap seriesDataMap = connectedSource.getCompleteSeriesMetaDataMap();
		for(Entry<String, MetaDataMap.MetaData> entry: seriesDataMap.entrySet()){
			String key = entry.getKey();
			try{
				//we store the meta data alongside the images, in the same path
				//to avoid metadata collisions, paths should be different				
				writeMetaData(path + "/seriesData/" + key, entry.getValue().object);
			}catch(RuntimeException err){
				System.err.println("MDS+ error saving series meta data '"+key+"':");
				err.printStackTrace();
			}
		}
	}
	
	private void writeMetaData(String name, Object data) {
		
		//deal with rank 0 entities
		if(data instanceof Byte) 
			data = new Byte[]{ (Byte)data };
		else if(data instanceof Integer) 
			data = new Integer[]{ (Integer)data };
		else if(data instanceof Short) 
			data = new Short[]{ (Short)data };		
		else if(data instanceof Long) 
			data = new Long[]{ (Long)data };		
		else if(data instanceof Float) 
			data = new Float[]{ (Float)data };		
		else if(data instanceof Double) 
			data = new Double[]{ (Double)data };
		else if(data instanceof Boolean) 
			data = new int[]{ ((Boolean)data) ? -1 : 0 }; //MDS can't deal with booleans
		else if(data instanceof String) 
			data = new String[]{ (String)data };
		else if(data instanceof boolean[]){
			//MDS can't deal with booleans
			boolean boolArr[] = (boolean[])data;
			int intArr[] = new int[boolArr.length];
			for(int i=0; i < intArr.length; i++)
				intArr[i] = boolArr[i] ? -1 : 0;
			data = intArr;
		}
		
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, name);
		
		GMDSSignal sig = new GMDSSignal(sigDesc, data);

		gmds.writeToCache(sig);
	}

	private void writeImage(int index, Img img){
		GMDSSignalDesc imgDesc = new GMDSSignalDesc(pulse, experiment, path + "/IMAGE_" + index);
		
		GMDSSignal imgSig = null;
		
		if(img == null){
			imgSig = new GMDSSignal(imgDesc, new short[0][0]); 
		}else{
			
			ReadLock readLock = img.readLock();
			try{
				readLock.lockInterruptibly();
			}catch(InterruptedException e){
				System.err.println("CISSink: writeImage("+index+") interrupted");
		        return;
			}
			
			try{
				if(img instanceof DoubleFlatArrayImage){
					imgSig = new GMDSSignal(imgDesc, 
									Mat.unflatten(
											((DoubleFlatArrayImage) img).getFlatArray(), 
											img.getHeight(), 
											img.getWidth()));
					
				}else if(img instanceof ByteBufferImage && ((ByteBufferImage)img).getBytesPerPixel() == 2 && img.getNFields() == 1){					
				
					short imageData[][] = new short[img.getHeight()][img.getWidth()];
					ByteBuffer bBuff = ((ByteBufferImage)img).getReadOnlyBuffer();
					if(bBuff.position() > 0){
						//System.err.println("CISSink WARNING: ByteBufferImage at non-zero position: " + bBuff.position());
						//it's really the short buffer that we'll care about
						bBuff.position(0);							
					}
					bBuff.order(ByteOrder.LITTLE_ENDIAN);
					ShortBuffer sBuff = bBuff.asShortBuffer();						
						
					for(int iY=0; iY < img.getHeight(); iY++) {
						sBuff.get(imageData[iY]);
					}
					imgSig = new GMDSSignal(imgDesc, imageData);
					
				}else{		
					double imageData[][][] = new double[img.getNFields()][img.getHeight()][img.getWidth()];
					for(int iF=0; iF < img.getNFields(); iF++){
						for(int iY=0; iY < img.getHeight(); iY++) {
							for(int iX=0; iX < img.getWidth(); iX++) {
									imageData[iF][iY][iX] = img.getPixelValue(readLock, iF, iX, iY);
							}
						}
					}
					if(img.getNFields() == 1)
						imgSig = new GMDSSignal(imgDesc, imageData[0]);
					else
						imgSig = new GMDSSignal(imgDesc, imageData);
				}
			}finally{
				readLock.unlock();
			}
			
		}
		
		if(imgSig != null){			
			gmds.writeToCache(imgSig);
		}
		
	}
		
	public void setData(String experiment, int pulse, String path){
		this.experiment = experiment;
		this.pulse = pulse;
		this.path = path;
		updateAllControllers();
	}
	
	public boolean getAutoSave(){ return this.autoSave; }
	
	public void setAutoSave(boolean enable){
		this.autoSave = enable; 
		updateAllControllers();
	}
		
	public void setSaveInvalid(boolean saveInvalid) {
		this.saveInvalid = saveInvalid;		
	}
	
	@Override
	/** Save triggered by a software controller of some kind.
	 * So we advance, write a note and save */
	public void triggerSave(int id) {
		pulse = id;
		
		String notes = getNotes(experiment, pulse);
		if("[EMPTY]".equalsIgnoreCase(notes)){
			String notesStr = 
					"Software triggered save at "
					+ DateFormat.getDateTimeInstance(
							DateFormat.SHORT, 
							DateFormat.SHORT).format(new Date());
			//hack for W7x program ID
			String w7xProgram = (String) getSeriesMetaData("w7x/programID");
			if(w7xProgram != null){
				notesStr += "\nW7X ProgramID = " + w7xProgram;
			}
			setNotes(experiment, pulse, notesStr);
		}
		
			
		extensionsPrepMetadataForSave();
		
		updateAllControllers();
		
		save();
	}
	
	@Override
	/** Save triggered by a software controller of some kind.
	 * Save under first free ID */
	public void triggerSave(){
		// look for next completely empty ID, starting from the 'current' (presumably last written) one
		int id = findNextEmpty(experiment, pulse, notesPath);
		triggerSave(id);
	}

	protected void extensionsPrepMetadataForSave() { }

	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	} //we don't really want to save based on the calc signal
	@Override
	public boolean wasLastCalcComplete(){ return lastSaveCompleted; }
	@Override
	public void abortCalc(){ }
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "GMDSSave [" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}
}
