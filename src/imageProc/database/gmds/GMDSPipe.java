package imageProc.database.gmds;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.StringEscapeUtils;
import org.eclipse.swt.widgets.Composite;

import descriptors.aug.AUGSignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.ImageProcUtil;
import imageProc.core.EventReciever;
import imageProc.core.EventReciever.Event;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import mds.AugShotfileFetcher;
import mds.GMDSFetcher;
import mds.MdsPlusException;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.Signal;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

public abstract class GMDSPipe extends ImgSourceOrSinkImpl implements EventReciever  {
	public final static String notesPath = "notes";
	
	protected GMDSFetcher gmds;
	
	protected int pulse;
	protected String experiment;
	protected String path;
	protected int maxImages = -1;
	
	protected HashMap<Integer, String> journalRemarks = new HashMap<Integer, String>(); 

	protected boolean abortCalc = false;
	protected boolean isLoadingOrSaving = false;

	private SettingsManager settingsMgr;
	
	public GMDSPipe() { this(null, null);	}

	public GMDSPipe(GMDSFetcher gmds) { this(null, gmds); }	
	
	public GMDSPipe(SettingsManager settingsMgr) { this(settingsMgr, null); }

	public GMDSPipe(SettingsManager settingsMgr, GMDSFetcher gmds) {
		this.settingsMgr = (settingsMgr != null) ? settingsMgr : SettingsManager.defaultGlobal();
		this.gmds = (gmds != null) ? gmds : GMDSUtil.globalGMDS(); 
		
		this.experiment = this.settingsMgr.getProperty("imageProc.gmds.experiment", "DEFAULT");
		this.pulse = Integer.parseInt(this.settingsMgr.getProperty("imageProc.gmds.pulse", "0"));
		this.path = this.settingsMgr.getProperty("imageProc.gmds.path", "/RAW");
		
		this.gmds.setUseMemoryCache(false); //we generally don't want this on
	}
	
	public String getExperiment(){ return experiment;	}
	public int getPulse(){ return pulse; }
	public String getPath(){ return path; }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			controller = new GMDSPipeSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);			
		}
		return controller;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + hashCode() + "]: " + experiment + "/" + pulse + "/" + path +"]";
	}
	
	public boolean isLoadingOrSaving(){ return isLoadingOrSaving; }
	public abstract int nImagesDone();
	public abstract int nImagesTotal();
	
	public String getNotes(String experiment, int pulse){
		GMDSSignalDesc notesDesc = new GMDSSignalDesc(pulse, experiment, notesPath);
		try{
			Signal notesSig = gmds.getSig(notesDesc);
			String str = ((String[])notesSig.getData())[0];
			return StringEscapeUtils.unescapeJava(str);
			
		}catch(MdsPlusException err){
			//hack for HDF5-->NetCDF convert
			String fileName = gmds.getCacheRoot() + "/gmds/" + experiment + "/" + pulse + "/notes.txt";
			if( (new File(fileName)).canRead()){
				System.err.println("*** Loaded hacked notes from " + fileName + "***");
				String text = OneLiners.fileToText(fileName);	
				setNotes(experiment, pulse, text);
				return text;
			}
			
			System.err.println("Error reading notes from '"+notesDesc+"': " + err.getMessage());
			return "[EMPTY]";
		}
	}
	
	public void setNotes(String experiment, int pulse, String notes){
		notes = StringEscapeUtils.escapeJava(notes);
		
		GMDSSignalDesc notesDesc = new GMDSSignalDesc(pulse, experiment, notesPath);
		try{
			GMDSSignal notesSig = new GMDSSignal(notesDesc, new String[]{ notes });			
			gmds.writeToCache(notesSig);
			
		}catch(MdsPlusException err){
			System.err.println("Error writing notes to '"+notesDesc+"': " + err.getMessage());
			
		}
	}

	public final static String clearCacheUpdateObj = "clearCacheUpdateObj"; 
	public void setCacheEnable(boolean enable) {
		gmds.setUseMemoryCache(enable);
		if(!enable){
			gmds.clearMemoryCache();
			System.gc(); //once here
			ImageProcUtil.ensureFinalUpdate(clearCacheUpdateObj, new Runnable() {
				@Override
				public void run() {
					System.gc(); //once again
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) { }
					System.gc(); //god dammit, do it already!
				}
			});
		}
	}
	
	public String[] getAvailableImageSets(String experiment, int pulse){
	
		String paths[] = gmds.dumpCacheTree(experiment, pulse, "/");
		
		//look for anything like and image set (a signal that ends with _COUNT),
		ArrayList<String> imageSets = new ArrayList<String>();
		
		for(String path : paths){
			if(path.endsWith("/IMAGE_COUNT")){
				imageSets.add(path.replaceAll("/IMAGE_COUNT", "").replaceAll("^[/]*", "/"));
			}
		}
		
		return imageSets.toArray(new String[0]);
		
	}
	
	public static String[] getAvailableExperiments(){
		ArrayList<String> exps = new ArrayList<String>();
		
		File dir = new File(GMDSUtil.globalGMDS().getCacheRoot() + "/gmds/");

		String[] children = dir.list();
		if (children == null) {
			System.err.println("No directory '" + dir.getAbsolutePath() + "' for exps list.");
			return new String[]{ "Default" };
		} 

		for (String fileName : children) {
			File file = new File(GMDSUtil.globalGMDS().getCacheRoot() + "/gmds/" + fileName);
			if(file.isDirectory()){
				exps.add(file.getName());	
			}
		}
		
		return exps.toArray(new String[0]);		
	}
	
	public boolean isIdle(){ 
		return !ImageProcUtil.isUpdateInProgress(this);
	}

	public void setMaxImages(int maxImages) {
		this.maxImages = maxImages;
	}

	
	/** Find the next completely empty (no notes or image0) pulse after the given one in the database */
	public int findNextEmpty(String exp, int pulse0, String path) {
		int nextPulse = pulse0;
		File dir;
		do{
			GMDSSignalDesc wildcardDesc = new GMDSSignalDesc(nextPulse, exp, "");
			String rootPath = wildcardDesc.path();
			
			dir = new File(gmds.getCacheRoot() + "/" + rootPath);
			nextPulse++;
		}while(dir.isDirectory());
		nextPulse--;
		
		//this.experiment = exp;
		//this.pulse = pulse;
		//this.path = path;
		//updateAllControllers();
		return nextPulse;
	}
	
	@Override
	public void event(Event event) {
		if(event == Event.GlobalAbort)
			abortCalc = true;
	}

	public void setToMetadata() {
		try{
			String metaExp = GMDSUtil.getMetaExp(connectedSource);
			int metaPulse = GMDSUtil.getMetaDatabaseID(connectedSource);
			
			experiment = metaExp;
			pulse = metaPulse;
			journalRemarks = null;
			
		}catch(RuntimeException err){
			System.err.println("Couldn't get gmds exp/pulse from metadata" + err);
		}
	}

	private String journalFetchSyncObj = new String("journalFetchSyncObj");
	public String getJournalRemarks(int augShot, boolean force) {
		if(augShot < 0){
			return "[ No shot number ]";
		}
		if(journalRemarks == null){
			// ???
			journalRemarks = new HashMap<Integer, String>();
		}
		
		String remarks = journalRemarks.get(augShot);
		
		if(remarks != null && !force){
			return remarks; //already got it
			
		}else{
			journalRemarks.put(augShot, "[ Fetching... ]");
			
			final int fAugShot = augShot;
			
			ImageProcUtil.ensureFinalUpdate(journalFetchSyncObj, new Runnable() {
				@Override
				public void run() {
					String remarks;
					try{
						AUGSignalDesc remarksDesc = new AUGSignalDesc(fAugShot, "JOU", "REMARKS", "REMARKS", "", 0, false);
						AUGSignal sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(remarksDesc); //this definitely exists
						
						Object o = sig.getData();
						if(o instanceof String[])
							o=((String[])o)[0];
						remarks = (String)o;
						
					}catch(RuntimeException err){
						System.err.println("GMDSPipe: Error getting journal remarks: " + err);
						remarks = "ERROR: " + err;
					}
					
					remarks = remarks.trim(); //remarks is length 400 because Fortran hasn't died yet
					if(!remarks.endsWith("\n"))
						remarks += "\n";
					
					try{
						for(int i=5; i <= 8*1; i++){
							AUGSignalDesc desc;
							AUGSignal sig;
							
							desc = new AUGSignalDesc(fAugShot, "JOU", "NBI", "NBI"+i+"L", "", 0, false);
							sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(desc);
							double power = (sig.getData() instanceof float[]) ? ((float[])sig.getData())[0] : (Float)sig.getData();
							
							if(Double.isNaN(power) || power <= 0)
								continue;
							
							desc = new AUGSignalDesc(fAugShot, "JOU", "NBI", "NBI"+i+"SP", "", 0, false);
							sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(desc);
							double voltage = (sig.getData() instanceof float[]) ? ((float[])sig.getData())[0] : (Float)sig.getData();
							
							desc = new AUGSignalDesc(fAugShot, "JOU", "NBI", "NBI"+i+"B", "", 0, false);
							sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(desc);
							double start = (sig.getData() instanceof float[]) ? ((float[])sig.getData())[0] : (Float)sig.getData();
							
							desc = new AUGSignalDesc(fAugShot, "JOU", "NBI", "NBI"+i+"E", "", 0, false);
							sig = (AUGSignal)AugShotfileFetcher.defaultInstance().getSig(desc);
							double end = (sig.getData() instanceof float[]) ? ((float[])sig.getData())[0] : (Float)sig.getData();
							
							DecimalFormat fmt = new DecimalFormat("##.##");
							remarks += "NBI"+i+": P=" + fmt.format(power/1e6) + 
									" MW, V="+fmt.format(voltage/1e3)+
									"kV, " + fmt.format(start) + " < t < " + fmt.format(end) +"\n";
						}
						
					}catch(RuntimeException err){
						System.err.println("GMDSPipe: Error getting journal remarks: " + err);
						remarks += "ERROR: " + err;
					}
					
					journalRemarks.put(fAugShot, remarks);
					System.out.println("|"+remarks+"|");
					updateAllControllers();
				}
			});
			
			return "[ Loading... ]";
		}
	}

	public GMDSFetcher getFetcher() { return gmds; }
	
}
