package imageProc.database.gmds;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ConfigurableByID;
import imageProc.core.swt.SWTSettingsControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Insert for saving/load settings to/from GMDS database */
public class GMDSSettingsControl implements SWTSettingsControl {	
	private Group swtGroup;
	
	private Combo loadExpCombo; 
	private Spinner loadPulseSpiner;
	private Button loadButton;
	private Button saveButton;

	private ConfigurableByID proc;
	
	@Override
	public void buildControl(Composite parent, int style, ConfigurableByID proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setLayout(new GridLayout(5, false));
		swtGroup.setText("GMDS Save/Load settings");
		
		Label lOP = new Label(swtGroup, SWT.NONE); lOP.setText("Load:");		
		loadExpCombo = new Combo(swtGroup, SWT.NONE);
		loadExpCombo.setItems(GMDSPipe.getAvailableExperiments());
		loadExpCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		loadPulseSpiner = new Spinner(swtGroup, SWT.NONE);
		loadPulseSpiner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		loadPulseSpiner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		loadButton = new Button(swtGroup, SWT.PUSH);
		loadButton.setText("Load");
		loadButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		loadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadAltPulseButtonEvent(event); } });
		
		saveButton = new Button(swtGroup, SWT.PUSH);
		saveButton.setText("Save (current pulse)");
		saveButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		saveButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
	}
	
	private void loadAltPulseButtonEvent(Event e) {
		proc.loadConfig("gmds/" + loadExpCombo.getText() + "/" + loadPulseSpiner.getSelection());		
	}
	
	private void saveButtonEvent(Event e) {
		proc.saveConfig("gmds//-1");
	}

	@Override
	public Composite getComposite() { return swtGroup;	}
	
	@Override
	public void doUpdate() { 
		String config = proc.getLastLoadedConfig();
		if(config == null)
			return;
		String parts[] = config.strip().split("/");
		if(parts[0].equals("gmds")) {
			loadExpCombo.setText(parts.length >= 2 ? parts[1] : "");
			loadPulseSpiner.setSelection(parts.length >= 3 ? Algorithms.mustParseInt(parts[1]) : 0);
		}
	}
}
