package imageProc.database.json;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import descriptors.SignalDesc;
import imageProc.core.ConfigurableByID;
import imageProc.core.swt.SWTSettingsControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Insert for saving/load settings to/from a json file on disk
 * Temporary remedy until I figure out how to put it in ArchiveDB
 */
public class JSONFileSettingsControl implements SWTSettingsControl {	
	private Group swtGroup;
	
	private Combo settingsFileCombo; 
	private Text filterTextbox;
	private Button loadButton;
	private Button saveButton;
	
	private ConfigurableByID proc;
	
	private String settingsPath;
	
	@Override
	public void buildControl(Composite parent, int style, ConfigurableByID proc) {
		this.proc = proc;
		this.settingsPath = SettingsManager.defaultGlobal().getPathProperty("imageProc.jsonSettings.path",
												SignalDesc.getDefaultCachePath() + "/jsonSettings");
		
		settingsPath += "/" + proc.getClass().getSimpleName();
		
		swtGroup = new Group(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));
		swtGroup.setText("JSON File Save/Load settings");
		
		Label lOP = new Label(swtGroup, SWT.NONE); lOP.setText("Load:");		
		settingsFileCombo = new Combo(swtGroup, SWT.NONE);
		settingsFileCombo.setItems();
		settingsFileCombo.setToolTipText("JSON file to save/load module settings to. Files are in path '" + this.settingsPath +"'");
		settingsFileCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		settingsFileCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refreshSettingsList(); } });
		
		Label lF = new Label(swtGroup, SWT.NONE); lF.setText("Filter:");		
		filterTextbox = new Text(swtGroup, SWT.NONE);
		filterTextbox.setText(".*              ");
		filterTextbox.setToolTipText("Filter list of existing JSON files");
		filterTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		filterTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { refreshSettingsList(); } });
				
		loadButton = new Button(swtGroup, SWT.PUSH);
		loadButton.setText("Load");
		loadButton.setToolTipText("Load the selected JSON settings for this module.");
		loadButton.setLayoutData(new GridData(SWT.END, SWT.BEGINNING, false, false, 1, 1));
		loadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadAltPulseButtonEvent(event); } });
		
		saveButton = new Button(swtGroup, SWT.PUSH);
		saveButton.setText("Save");
		saveButton.setToolTipText("Save the settings for this module to the specified JSON file.");
		saveButton.setLayoutData(new GridData(SWT.END, SWT.BEGINNING, false, false, 1, 1));
		saveButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
		
		refreshSettingsList();
		doUpdate();
	}
	
	private void refreshSettingsList(){
		String selected = settingsFileCombo.getText();
		File settingsDir = new File(settingsPath);
		if(!settingsDir.exists())
			settingsDir.mkdirs();
		
		File files[] = settingsDir.listFiles();
		
		ArrayList<String> names = new ArrayList<String>();
		for(File file : files){
			
			String name = file.getName();
			names.add(name);
		
		}
		Collections.sort(names);
		
		settingsFileCombo.setItems(new String[0]);
		int selIdx = -1;
		int idx = 0;
		Pattern p = Pattern.compile(filterTextbox.getText().stripTrailing());
		for(String name : names){
			if(!p.matcher(name).matches())
				continue;
				
			if(selected.equals(name))
				selIdx = idx;
			
			settingsFileCombo.add(name);
			idx++;
		}
		
		if(selIdx >= 0)
			settingsFileCombo.select(selIdx);
		else
			settingsFileCombo.setText(selected);
	}
	
	private void loadAltPulseButtonEvent(Event e) {
		if(settingsFileCombo.getText().length() > 0){
			String str = settingsFileCombo.getText();
			if(str.startsWith("w7x/")){
				proc.loadConfig("parlog:" + str);
			}else{
				proc.loadConfig("jsonfile:" + settingsPath + "/" + str);
			}
		}
		refreshSettingsList();
	}
	
	private void saveButtonEvent(Event e) {
		if(settingsFileCombo.getText().length() > 0)
			proc.saveConfig("jsonfile:" + settingsPath + "/" + settingsFileCombo.getText());
		refreshSettingsList(); //may have created a file
	}
	
	@Override
	public Composite getComposite() { return swtGroup;	}
	
	@Override
	public void doUpdate() { 
		String config = proc.getLastLoadedConfig();
		if(config == null || settingsFileCombo.isFocusControl())
			return;
		config = config.strip();
		if(config.startsWith("jsonfile:")) {
			config = config.substring(9).replaceAll("//*", "/");
			String setPath = settingsPath.replaceAll("//*", "/");
			if(config.startsWith(setPath + "/"))
				config = config.substring(setPath.length());
			settingsFileCombo.setText(config);		
		}
	}
	
}
