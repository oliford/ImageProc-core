package imageProc.app;

import org.eclipse.swt.widgets.Display;

import imageProc.core.ImageProcUtil;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.WindowShell;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.graph.GraphUtilProcessor;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.imgFit.ImageFitProcessor;
import imageProc.sources.sim.platesGen.PlatesGenSource;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The IMSE GUI App to tie all the SWT modules together
 * 
 * This version has no hardware interface modules loaded (so doesn't require any JNI)
 * 
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:+UseParNewGC -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 *  
 */
public class ImageProcApp_NoDrvs {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
			
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
		
		Display swtDisplay = new Display();
		
		///ConcurrencyUtils.setThreadPool(IMSEProc.getThreadPool()); //bad idea :(
		   
		//Read data from PCCIS MDS+ DB (HGW Lab)
		
		//Sources need to be instantiated
		GMDSSource mdsSrc = new GMDSSource();		
		//SensicamSource camSrc = new SensicamSource();
		PlatesGenSource platesSrc = new PlatesGenSource();
		
		//Sinks get instantiated as required, but we need to register the classes
		ImageProcUtil.addImgPipeClass(GraphUtilProcessor.class);
		ImageProcUtil.addImgPipeClass(GMDSSink.class);
		ImageProcUtil.addImgPipeClass(FFTProcessor.class);
		ImageProcUtil.addImgPipeClass(DSHDemod.class);
		ImageProcUtil.addImgPipeClass(ImageFitProcessor.class);	
				
		WindowShell imgWin = new WindowShell(swtDisplay, null, 0);

		ImageProcUtil.msgLoop(swtDisplay, true);
	}
}