package imageProc.app;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.swt.widgets.Display;

import algorithmrepository.Algorithms;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.grbl.GRBLControl;
import imageProc.control.power.raritan.RaritanPowerControl;
import imageProc.control.standa.ximc.XIMCControl;
import imageProc.control.thing.ThingControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.WindowShell;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.graph.GraphUtilProcessor;
import imageProc.metaview.MetaDataManager;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.imgFit.ImageFitProcessor;
import imageProc.proc.softwareBinning.SoftwareROIsProcessor;
import imageProc.proc.transform.TransformProcessor;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.andorV2.AndorV2Source;
import imageProc.sources.capture.as5216.AvantesAS5216Source;
import imageProc.sources.capture.avaspec.AvaspecSource;
import imageProc.sources.capture.example.ExampleCapture;
import imageProc.sources.capture.example.ExampleSource;
import imageProc.sources.capture.flir.FLIRCamSource;
import imageProc.sources.capture.oceanOptics.OmniDriverSource;
import imageProc.sources.capture.pco.PCOCamSource;
import imageProc.sources.capture.picam.PicamSource;
import imageProc.sources.capture.pylon.PylonSource;
import imageProc.sources.capture.sensicam.SensicamSource;
import imageProc.sources.sim.dshGen.DSHGenSource;
import imageProc.sources.sim.noiseGen.NoiseGenSource;
import imageProc.sources.sim.platesGen.PlatesGenSource;
import oneLiners.OneLiners;
import otherSupport.SettingsManager;

/** The ImageProc GUI App to tie all the SWT modules together
 *  Based on a settings-file style profile definition that has to be passed as a parameter
 *
 * All class names must be fully qualified
 * 
 * imageProc.profile.host=hostname	# This profile is intended for the given host
 * imageProc.profile.assertHost=true/false # Fail if not running on the correct host
 *
 * imageProc.profile.source.class={Class}
 * imageProc.profile.source.config={ID} - call loadConfig() for source with this parameter}
 * 
 * imageProc.profile.sinkXX.class={Class} - sink class name
 * imageProc.profile.sinkXX.init=true/false - Instantiate this class? If not just available
 * imageProc.profile.sinkXX.config={config} - Configure the sink, if inited
 * 
 * W7X specifics:
 * 
 *  w7x.archive.dastbase={'W7X_TEST','W7X_ARCHIVE'}
	w7x.archive.path={path} where to save

	imageProc.w7xPilot.systemID
	imageProc.w7xPilot.logPath
	imageProc.w7xPilot.statusComm.defaultPort
	imageProc.w7xPilot.inShot.acquireStartTimeMS
	
	imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault
	imageProc.w7xPilot.inShot.fault.resetPower
	imageProc.w7xPilot.inShot.fault.cameraOffTime
	imageProc.w7xPilot.inShot.acquireTimeout
	imageProc.w7xPilot.inShot.saveTimeout
	imageProc.w7xPilot.inShot.processChainDelay
	imageProc.w7xPilot.inShot.acquireStartTimeout
	imageProc.w7xPilot.ips.host
	imageProc.w7xPilot.ips.username
	imageProc.w7xPilot.ips.password
	imageProc.w7xPilot.ips.faultResetOutlets
	imageProc.w7xPilot.powerCtrlClass
	imageProc.w7xPilot.cameraWarmUpTime
	
	 
 * 
 * 
 * 
 * ----- Memory notes -------
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:+UseParNewGC -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 * 
 * Environment variables need to be set for the following modules:
 * 
 * Bitflow CameraLink framegrabber for Andor Zyla camera: 
 *    BITFLOW_INSTALL_DIRS /opt/andor/bitflow
 *    LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib
 * 
 * RXTX native library for all serial/serial over USB: 
 *    LD_LIBRARY_PATH /usr/lib64/rxtx
 *    
 * DDWW native libraries for ASDEX Upgrade shotfile access:
 *    LD_LIBRARY_PATH /afs/ipp/aug/ads/@sys/lib64
 * 
 * Ocean Optics OmniDriver:
 * 		LD_LIBRARY_PATH /opt/omniDriver/OOI_HOME
 *  
 */
public class ImageProc_Profile {
	private Logger logr;
	
	public static void main(String[] args) {
		(new ImageProc_Profile()).run(args);
	}	
	
	protected void run(String[] args) {
		
		SettingsManager global = new SettingsManager("minerva", false);
				
		Display swtDisplay = new Display();
		
		ImageProcUtil.setPreferredSettings(JSONFileSettingsControl.class);
		
		if(args.length < 1){
			throw new IllegalArgumentException("No profile files or 'by-host' specificed.");
		}
		
		String pName = args[0].replaceAll("/", "_");
		logr = ImageProcUtil.setupLog("ImageProc_Profile-" + pName, this.getClass().getName());
		
		
		String profiles[];
		if(args[0].startsWith("by-host")) {
			//start all profiles belonging to this host in this one VM
			String[] parts = args[0].split(":");
			int matchIdx = -1;
			if(parts.length > 1) {
				matchIdx = Integer.parseInt(parts[1]);				
			}else {				
			}
			profiles = getProfilesByHost(matchIdx);
		}else {
			profiles = args;
		}
		
		try{
					
			for(String profile : profiles) {			
				logr.info("Starting profile: " + profile);
				if(new File(profiles[0]).canRead()) {
					startProfile(profile, global, swtDisplay);
					
				}else {
					//not a file, try searching for systemID
					String profileFile = getProfileByID(profile);
					if(profileFile != null) {
						startProfile(profileFile, global, swtDisplay);
					}else {
						logr.severe(profile + " is not a profile file and no file in profiles/* has that as imageProc.w7xPilot.systemID");					
					}
					
				}
				
			}
			
			//ImageProcUtil.suppressAWTLoggingNoise();
			ImageProcUtil.msgLoop(swtDisplay, true);
			
		}finally {
			ImageProcUtil.shutdown();
			
			swtDisplay.dispose();	
	
			System.out.println("GUI thread exited. Terminating JVM to kill all remaining threads.");
			System.exit(0); //really exit, even if threads are running
		}
		
	}
	
	/** Look for profiles in profiles/* file that are supposed to run on this host.
	 * 
	 * @param matchIdx If >=0, return only the given index into those found
	 * @return
	 */
	private String[] getProfilesByHost(int matchIdx) {
		String hostname = OneLiners.getHostname();
		hostname = hostname.replaceAll("\\..*","");
		
		Logger logr = Logger.getLogger(this.getClass().getName());
		logr.info("Searching for " + (matchIdx < 0 ? "all profiles" : "profile #"+matchIdx)+" for host " + hostname);
				
		ArrayList<String> profiles = new ArrayList<>();
		try (Stream<Path> walk = Files.walk(Paths.get("./profiles"))) {
			List<String> allProfiles = walk.filter(Files::isRegularFile)
				.map(x -> x.toString()).collect(Collectors.toList());
			
			for(String profile : allProfiles) {
				SettingsManager s = new SettingsManager(profile, false);
				if(s.propertyDefined("imageProc.profile.host")) {
					String targetHost = s.getProperty("imageProc.profile.host");
					if(hostname.equals(targetHost)) 
					profiles.add(profile);
				}
			}
			
			if(matchIdx >= 0) {
				if(matchIdx >= profiles.size())
					throw new IllegalArgumentException("by-host:"+matchIdx+" specified, but we only found " + profiles.size() + " matching profiles");
				return new String[] { profiles.get(matchIdx) };
			}else {
				if(profiles.size() == 0)
					throw new RuntimeException("by-host specified, but no profiles specify this host '"+hostname+"'");
				return profiles.toArray(new String[0]);
			}
			
			
		}catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/** Look for a profile in profiles/* files that have this systemID
	 * 
	 * @param matchIdx If >=0, return only the given index into those found
	 * @return
	 */
	private String getProfileByID(String acqID) {
		
		Logger logr = Logger.getLogger(this.getClass().getName());
		logr.info("Searching for profile with ID " + acqID);
		
		try (Stream<Path> walk = Files.walk(Paths.get("./profiles"))) {
			List<String> allProfiles = walk.filter(Files::isRegularFile)
				.map(x -> x.toString()).collect(Collectors.toList());
			
			for(String profile : allProfiles) {
				SettingsManager s = new SettingsManager(profile, false);
				if(s.propertyDefined("imageProc.w7xPilot.systemID")) {
					String targetID = s.getProperty("imageProc.w7xPilot.systemID");
					if(acqID.equals(targetID)) 
						return profile;
				}
			}
			
		}catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		return null;
	}

	private void startProfile(String fileName, SettingsManager global, Display swtDisplay){
		try{
			SettingsManager profile = new SettingsManager(fileName, true, global); //overlay on the minerva-settings
			
			if(profile.propertyDefined("imageProc.profile.assertHost") 
					&& Algorithms.mustParseBoolean(profile.getProperty("imageProc.profile.assertHost"))){
				
				String assertHost = profile.getProperty("imageProc.profile.host");
				
				
				System.out.print("ImageProcProfile: Getting localhost hostname... ");
				String hostname = OneLiners.getHostname();
				hostname = hostname.replaceAll("\\..*","");
				System.out.println(" OK, Localhost = '"+hostname+"'.");
				if(assertHost.equals(hostname)){
					System.out.println("Local hostname matches profile '"+assertHost+"'");
				}else{				
					throw new RuntimeException("Local hostname '"+hostname+"' doesn't match the one in the profile file '"+assertHost+"'");
				}
			}
			
			//I'm sure this isn't right, but just trying to get it to keep them in memory right now 
			for(ImgSource src : addStandardSources())
				if(src instanceof ImgSourceOrSinkImpl)
					ImageProcUtil.addImgPipeInstance((ImgSourceOrSinkImpl)src, false);
			
			addStandardSinks();
			
			String className = profile.getProperty("imageProc.profile.source.class");
			
			ImgSource source;
			try {
				Class<ImgSource> cls = (Class<ImgSource>)Class.forName(className);
				source = cls.newInstance();
				
			} catch (Exception err) {
				throw new RuntimeException(err);
			}		
			
			if(profile.propertyDefined("imageProc.profile.source.config")){
				String sourceConfig = profile.getProperty("imageProc.profile.source.config");		
				if(!(source instanceof ConfigurableByID))
					throw new RuntimeException("Source is not configurable, but imageProc.profile.source.config was specified");
					
				((ConfigurableByID)source).loadConfig(sourceConfig);
			}
					
			Set<String> keys = profile.getKeys();
			
			String basePath = "imageProc.profile";
			scanSinks(profile, basePath, source);
	
			WindowShell imgWin1 = new WindowShell(swtDisplay, source, 0);
			
			imgWin1.setProfileName(profile.getProperty("imageProc.w7xPilot.systemID"));
			imgWin1.setProfileChars(profile.getProperty("imageProc.window.iconChars"));
			
			sourceSpecial(source, profile);			
			
		}catch(Exception err){
			logr.log(Level.SEVERE, "Error starting profile", err);
		}		
	}
	
	protected void sourceSpecial(ImgSource source, SettingsManager profile) {
	
	}
	
	private void scanSinks(SettingsManager profile, String basePath, ImgSource source) {
		String className = "?";
		for(int sinkID=0; sinkID < 100; sinkID++){
			try{
				className = profile.getProperty(basePath + ".sink"+sinkID+".class", "NONE");
				if(className.equals("NONE"))
					continue;
								
				Class<ImgSink> sinkCls = (Class<ImgSink>)Class.forName(className);
					
				ImageProcUtil.addImgPipeClass(sinkCls);
									
				boolean init = Algorithms.mustParseBoolean(profile.getProperty(basePath + ".sink"+sinkID+".init", "false"));
				if(init){
					ImgSink sink;
					try{
						Constructor<ImgSink> cons = sinkCls.getConstructor(SettingsManager.class);
						sink = cons.newInstance(profile);
							
					}catch(NoSuchMethodException err){
						//we don't really use the profile for this anymore, modules now have their own jsonSettings files						
						//System.err.println("WARNING: No constructor for '"+sinkCls+"' which takes a SettingsManager. Sink will probably not follow the profile.");
						
						Constructor<ImgSink> cons = sinkCls.getConstructor(); 
						sink = cons.newInstance();
					}
						
					sink.setSource(source);
						
					if(sink instanceof ConfigurableByID && profile.propertyDefined(basePath + ".sink"+sinkID+".config")){
						String configID = profile.getProperty(basePath + ".sink"+sinkID+".config");
						((ConfigurableByID)sink).loadConfig(configID);
					}
					
					sinkSpecial(sink, profile, basePath, sinkID);					
						
					if(sink instanceof ImgSource) //is a pipe, may have hierarchy
						scanSinks(profile, basePath + ".sink"+sinkID, (ImgSource)sink);
				}
			}catch(Exception err){
				throw new RuntimeException("Unable to instantiate or configure class " + className + " for sink entry " + basePath + ".sink" + sinkID, err);
								
			}
		}
	}
	
	protected void sinkSpecial(ImgSink sink, SettingsManager profile, String basePath, int sinkID) {
		
	}
	
	public List<ImgSource> addStandardSources() {
		ArrayList<ImgSource> srcs = new ArrayList<>();
		
		//Database sources 
		srcs.add(new GMDSSource());
		//JotterSource jotter = new JotterSource();
		
		// Capture sources
		/*SensicamSource camSrc = new SensicamSource();
		AndorCamSource andorSrc = new AndorCamSource();
		AndorV2Source andorV2 = new AndorV2Source();
		PicamSource picamSrc = new PicamSource();
		PCOCamSource pcoSrc = new PCOCamSource();
		FLIRCamSource flirSrc = new FLIRCamSource();
		PylonSource pylonSrc = new PylonSource();
		AvaspecSource avaspecSrc = new AvaspecSource();
		OmniDriverSource omniDrvSrc = new OmniDriverSource();
		AndorV2Source andor2Src2 = new AndorV2Source();
		AvantesAS5216Source as5216 = new AvantesAS5216Source();*/		
		//andor2Src2.m
		
		//Simulation sources
		//DSHGenSource dshSrc = new DSHGenSource();		
		srcs.add(new NoiseGenSource(344,260));
		srcs.add(new ExampleSource());
		//PlatesGenSource platesSrc = new PlatesGenSource();
				
		return srcs;
	}
	
	public void addStandardSinks() {
		//Sinks get instantiated as required, but we need to register the classes
		ImageProcUtil.addImgPipeClass(GraphUtilProcessor.class);
		//IMSEProc.addImgPipeClass(JotterSink.class);
		ImageProcUtil.addImgPipeClass(GMDSSink.class);
		ImageProcUtil.addImgPipeClass(FFTProcessor.class);
		ImageProcUtil.addImgPipeClass(DSHDemod.class);
		//IMSEProc.addImgPipeClass(StepperHanger.class); // The old stepper driver, now done via DACTiming, or ArduinoCommHanger
		ImageProcUtil.addImgPipeClass(ImageFitProcessor.class);		
		ImageProcUtil.addImgPipeClass(ArduinoCommHanger.class);
		ImageProcUtil.addImgPipeClass(GRBLControl.class);
		ImageProcUtil.addImgPipeClass(XIMCControl.class);
		ImageProcUtil.addImgPipeClass(MetaDataManager.class);
		ImageProcUtil.addImgPipeClass(SoftwareROIsProcessor.class);
		ImageProcUtil.addImgPipeClass(TransformProcessor.class);
		ImageProcUtil.addImgPipeClass(RaritanPowerControl.class);
		ImageProcUtil.addImgPipeClass(ThingControl.class);
	}
	
}