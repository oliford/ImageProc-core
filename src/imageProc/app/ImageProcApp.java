package imageProc.app;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Display;

import descriptors.gmds.GMDSSignalDesc;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.thing.ThingConfig;
import imageProc.control.thing.ThingControl;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.WindowShell;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.graph.GraphUtilProcessor;
import imageProc.metaview.MetaDataManager;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.imgFit.ImageFitProcessor;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.andorV2.AndorV2Source;
import imageProc.sources.capture.as5216.AvantesAS5216Source;
import imageProc.sources.capture.avaspec.AvaspecSource;
import imageProc.sources.capture.dc1394.DC1394Source;
import imageProc.sources.capture.ebus.EBusSource;
import imageProc.sources.capture.example.ExampleSource;
import imageProc.sources.capture.flir.FLIRCamSource;
import imageProc.sources.capture.flirSciCam.FLIRSciCamSource;
import imageProc.sources.capture.oceanDirect.OceanDirectSource;
import imageProc.sources.capture.oceanOptics.OmniDriverSource;
import imageProc.sources.capture.pco.PCOCamSource;
import imageProc.sources.capture.pcosdk.PCO_SDKSource;
import imageProc.sources.capture.picam.PicamSource;
import imageProc.sources.capture.pylon.PylonSource;
import imageProc.sources.capture.sensicam.SensicamSource;
import imageProc.sources.capture.v4l.V4LSource;
import imageProc.sources.capture.xclib.XCLIBSource;
import imageProc.sources.sim.dshGen.DSHGenSource;
import imageProc.sources.sim.noiseGen.NoiseGenSource;
import imageProc.sources.sim.platesGen.PlatesGenSource;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The IMSE GUI App to tie all the SWT modules together
 * 
 * This version starts with no source selected and no sinks attached. * 
 * 
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:+UseParNewGC -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 * 
 * For java 9, to allow freeing of ByteBuffers, add:
 *  --add-opens java.base=ALL-UNNAMED
 * 
 * Environment variables need to be set for the following modules:
 * 
 * Bitflow CameraLink framegrabber for Andor Zyla camera: 
 *    BITFLOW_INSTALL_DIRS /opt/andor/bitflow
 *    LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib
 * 
 * RXTX native library for all serial/serial over USB: 
 *    LD_LIBRARY_PATH /usr/lib64/rxtx
 *    
 * DDWW native libraries for ASDEX Upgrade shotfile access:
 *    LD_LIBRARY_PATH /afs/ipp/aug/ads/@sys/lib64
 * 
 *  
 */
public class ImageProcApp {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
				
		Display swtDisplay = new Display();
		
		///ConcurrencyUtils.setThreadPool(IMSEProc.getThreadPool()); //bad idea :(
		   
		//Read data from PCCIS MDS+ DB (HGW Lab)
		
		//Sources need to be instantiated
		addStandardSources();
		
		//Sinks get instantiated as required, but we need to register the classes
		ImageProcUtil.addImgPipeClass(GraphUtilProcessor.class);
		//IMSEProc.addImgPipeClass(JotterSink.class);
		ImageProcUtil.addImgPipeClass(GMDSSink.class);
		ImageProcUtil.addImgPipeClass(FFTProcessor.class);
		ImageProcUtil.addImgPipeClass(DSHDemod.class);
		//IMSEProc.addImgPipeClass(StepperHanger.class); // The old stepper driver, now done via DACTiming, or ArduinoCommHanger
		ImageProcUtil.addImgPipeClass(ImageFitProcessor.class);
		ImageProcUtil.addImgPipeClass(ArduinoCommHanger.class);
		ImageProcUtil.addImgPipeClass(MetaDataManager.class);
		
		ImageProcUtil.setPreferredSettings(JSONFileSettingsControl.class);
		
		//GraphUtilProcessor gu = new GraphUtilProcessor();
		
		//GMDSSignalDesc sigDesc = new GMDSSignalDesc(0, IMSEProc.getMetaExp(noiseSrc), "/signalLists/blah");
		//IMSEProc.globalGMDS().getSig(sigDesc);
		
		WindowShell imgWin1 = new WindowShell(swtDisplay, null, 0);
		
		ImageProcUtil.msgLoop(swtDisplay, true);	
		
	}

	public static List<ImgSource> addStandardSources() {
		ArrayList<ImgSource> srcs = new ArrayList<>();
		
		//Database sources 
		srcs.add(new GMDSSource());
		//JotterSource jotter = new JotterSource();
		
		// Capture sources
		srcs.add(new SensicamSource());
		srcs.add(new AndorCamSource());
		srcs.add(new AndorV2Source());
		srcs.add(new PicamSource());
		srcs.add(new PCOCamSource());
		srcs.add(new PCO_SDKSource());
		srcs.add(new FLIRCamSource());
		srcs.add(new PylonSource());
		srcs.add(new AvaspecSource());
		srcs.add(new OmniDriverSource());
		srcs.add(new AvantesAS5216Source());
		srcs.add(new EBusSource());
		srcs.add(new V4LSource());
		srcs.add(new DC1394Source());
		srcs.add(new XCLIBSource());
		srcs.add(new FLIRSciCamSource());
		srcs.add(new OceanDirectSource());
		srcs.add(new ExampleSource());
		//andor2Src2.m
		
		//Simulation sources
		srcs.add(new DSHGenSource());		
		srcs.add(new NoiseGenSource(344,260));
		srcs.add(new PlatesGenSource());
				
		return srcs;		
	}
}