package imageProc.app;

import java.io.File;
import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import imageProc.core.ImageProcUtil;

public class LoggerTest {

	public static void main(String[] args) {
		

		ImageProcUtil.setupLog("ImageProcW7X", LoggerTest.class.getName());
		
		//Logger.getLogger("imageProc").setLevel(Level.ALL);
		
		Logger.getLogger("sun.awt.bullshit.splurg.fuckoff").log(Level.FINEST, "arrrrrrg");
		Logger.getLogger("imageProc.thing.stuff").log(Level.INFO, "by the way...");
		Logger.getLogger("imageProc.thing.stuff").log(Level.FINEST, "weeeee");
		
		
		System.exit(0);
		
		System.out.println(System.getProperties().get("java.util.logging.config.file"));		
		System.out.println(System.getProperties().get("java.util.logging.config.class"));
		
		LogManager lm = LogManager.getLogManager();
		try {
			lm.readConfiguration();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Logger rootLogger = LogManager.getLogManager().getLogger("");
		//rootLogger.setLevel(Level.FINEST);
		for (Handler h : rootLogger.getHandlers()) {
		    h.setLevel(Level.FINEST);
		}
		
		Logger logrFood = Logger.getLogger("food");
		logrFood.setLevel(Level.FINEST);
		logrFood.log(Level.FINEST, "Finest food");
		
		Logger logrSpag = Logger.getLogger("food.pasta.spaghetti");
		
		logrSpag.log(Level.FINEST, "Finest spaghetti");
		
		System.out.println(logrSpag.getLevel());
		
		
		
	}
}
