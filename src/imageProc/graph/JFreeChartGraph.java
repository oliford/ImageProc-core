package imageProc.graph;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolTip;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.Range;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.experimental.chart.swt.ChartComposite;

import otherSupport.ScientificNumberFormat;

public class JFreeChartGraph {
	private static final Color[] defaultColors = new Color[]{ 
			Color.BLACK, 
			Color.BLUE, 
			Color.RED, 
			Color.MAGENTA, 
			Color.GREEN, 
			Color.DARK_GRAY, 
			Color.ORANGE, 
			Color.PINK, 
			Color.WHITE,
			Color.CYAN };
	 
	private ChartComposite frame;
	private XYIntervalSeriesCollection dataset;
	
	/** Stops the range shrinking */
	private boolean rangeHold = false; 
	private double rangeHoldMin, rangeHoldMax; 
	private NumberAxis xAxis;
	private NumberAxis valueAxis;
	private XYPlot plot;

	private ToolTip infoBox;
	
	private XYErrorRenderer pointRenderer;
	private XYLineAndShapeRenderer lineRenderer;

	private double lastX = Double.NaN, lastY = Double.NaN;
	
	private long setDataTimeoutMS = 3000;
	
    public JFreeChartGraph(Composite parent, int style){
    	    
    	XYIntervalSeries series = new XYIntervalSeries("Series 1");
        
        double value = 100.0;
        for (int i = 0; i < 200; i++) {
            series.add(i+100, i+90, i+110, value, 0.9*value, 1.1*value);    
            value = value * (1 + (Math.random() - 0.495) / 10.0);
        }
        
        dataset = new XYIntervalSeriesCollection();
        dataset.addSeries(series);

        init(parent, style, dataset);
    }
     
    private void init(Composite parent, int style, XYDataset dataset){
            
        
        StandardChartTheme currentTheme = new StandardChartTheme("JFree");
        
        ScientificNumberFormat format = new ScientificNumberFormat();
        
        xAxis = new NumberAxis(null);
        xAxis.setLowerMargin(0.00);  // reduce the default margins
        xAxis.setUpperMargin(0.00);
        xAxis.setAutoRangeIncludesZero(false);  // override default
        xAxis.setNumberFormatOverride(format);
        
        valueAxis = new NumberAxis(null);
        valueAxis.setLowerMargin(0.10);  // reduce the default margins
        valueAxis.setUpperMargin(0.10);
        valueAxis.setAutoRangeIncludesZero(false);  // override default
        valueAxis.setNumberFormatOverride(format);
        
        plot = new XYPlot(dataset, xAxis, valueAxis, null);

        lineRenderer = new XYLineAndShapeRenderer(true, false);
        //lineRenderer.setBaseToolTipGenerator(toolTipGenerator); We do this ourselves, it works better
        pointRenderer = new XYErrorRenderer();


        for(int i=0; i < defaultColors.length; i++){
        	lineRenderer.setSeriesPaint(i, defaultColors[i]);
        	pointRenderer.setSeriesPaint(i, defaultColors[i]);
        }
        
        plot.setRenderer(lineRenderer); //default, but gets set per-series according to point/line type

        JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        currentTheme.apply(chart);
        
        chart.setBackgroundPaint(Color.white);
        chart.setBorderVisible(true);
        chart.setBorderPaint(Color.BLACK);
        
        
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        
        plot.setAxisOffset(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
       
        
        frame = new ChartComposite(parent, SWT.NONE, chart, 
        		300,300,
        		10, 10, 1000, 1000, true, true, true, true, true, true);
        frame.setDisplayToolTips(true);
        frame.setHorizontalAxisTrace(true);
        frame.setVerticalAxisTrace(true);
        frame.addListener(SWT.Resize, new Listener() { @Override public void handleEvent(Event event) { resizeEvent(event); } });
        frame.addChartMouseListener(new ChartMouseListener() {			
			@Override public void chartMouseMoved(ChartMouseEvent event) { chartMouseMovedEvent(event); }			
			@Override public void chartMouseClicked(ChartMouseEvent event) { }
		});
        
        chart.setPadding(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        plot.setInsets(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        xAxis.setTickLabelInsets(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        valueAxis.setTickLabelInsets(new RectangleInsets(0.0, 4.0, 0.0, 0.0));
        
        Font f = xAxis.getTickLabelFont().deriveFont(10.0f);
        xAxis.setTickLabelFont(f);
        valueAxis.setTickLabelFont(f);
                
    }
    
	public void resizeEvent(Event event){
    	//System.out.println("Graph frame resize: " + event.width + " x " + event.height );
    	frame.layout();
    }
    
    public ChartComposite getComposite() { return frame; }
    
    //public void setData(double x[], double data[]){
    //	setData(x, null, new double[][]{ data });
    //}
    
    public void setData(List<Series> seriesList){ 
    	Logger logr = Logger.getLogger(this.getClass().getName());
    	
    	synchronized (dataset) {
    		dataset.removeAllSeries();
	    	   	
	    	//XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)((XYPlot)frame.getChart().getPlot()).getRenderer();
	
	      //some protection for silly data lengths and useful display for single values
	    	if(seriesList == null || seriesList.size() <= 0)
	    		return;
	    	
			NumberAxis valueAxis = (NumberAxis) ((XYPlot)frame.getChart().getPlot()).getRangeAxis();
	    	boolean isLogValueAxis = (valueAxis instanceof LogarithmicAxis);
			
			int nextColor = 0;
seriesLoop:
	    	for(int j=0; j < seriesList.size(); j++){
	    		long t0 = System.currentTimeMillis();
				
	    		Series series = seriesList.get(j);
	    		if(series == null)
	    			continue;

		    	//if a single value, make a series from [0,v] to [1,v]
		    	if(series.data.length == 1 && !series.points){
		    		if(series.x[0] == 0)
		    			series.x = new double[]{ 0, 1 };
		    		else
		    			series.x = new double[]{ series.x[0] - 0.1*series.x[0],
		    									series.x[0] + 0.1*series.x[0] };
		    		series.data = new double[]{ series.data[0], series.data[0] };
		    	}		    	
	    		
	        	if(series.data == null)
	    			continue; //skip empty
	        	if(series.x == null)
	        		throw new RuntimeException("No x data in series '" + series.name() + "'");
	        	if(series.data.length != series.x.length){
	        		logr.fine("Skipping series '" + series.name() + "' with data length " + 
	        							series.data.length + " but x length " + series.x.length);
	        		continue;
	        	}
	        	
	        	double newMin = Double.POSITIVE_INFINITY;
		    	double newMax = Double.NEGATIVE_INFINITY;
		    	
	        	String id = (series.name() == null) ? ("Series " + j) : (series.name() + "_" + j);
		    	
	        	XYIntervalSeries xySeries = new XYIntervalSeries(id);
		        boolean anyValid = false;
		        for (int i = 0; i < series.data.length; i++) {
		    		if(Double.isInfinite(series.data[i])) //graph can't cope with +/-inf
		    			series.data[i] = Double.NaN;
					if(isLogValueAxis && series.data[i] <= 0)
						series.data[i] = Double.NaN;
		    		
		    		if(series.dataLow != null){
		    			if(Double.isInfinite(series.dataLow[i])) //graph can't cope with +/-inf
			    			series.dataLow[i] = Double.NaN;
		    			if(Double.isInfinite(series.dataHigh[i])) //graph can't cope with +/-inf
			    			series.dataHigh[i] = Double.NaN;
			    		
		    			xySeries.add(series.x[i], series.x[i], series.x[i], series.data[i], series.dataLow[i], series.dataHigh[i]);
		    		}else{
		    			xySeries.add(series.x[i], series.x[i], series.x[i], series.data[i], series.data[i], series.data[i]);
		    		}
		        	
		        	anyValid |= (Double.isFinite(series.x[i]) && Double.isFinite(series.data[i]));
		        		
		        	if((System.currentTimeMillis() - t0) > setDataTimeoutMS){
		        		logr.fine("Skipping series '" + series.name() + " because it took too long to process (>"+(setDataTimeoutMS/1000)+"s)");
		        		continue seriesLoop;
		        	}

		        	if(series.data[i] < newMin) newMin = series.data[i];
		        	if(series.data[i] > newMax) newMax = series.data[i];
		    	}
		        
		    	if(!anyValid){		    		
		    		logr.fine("Skipping series '" + series.name() + " because it is entirely NaN");
	        		continue seriesLoop;		        	
		    	}
		        
		        //point size if points
		        double pointSize = (series.color != null && series.color.length > 3 && series.points) ? series.color[3] : 4;
	        	
		        Shape s = new Ellipse2D.Double(-pointSize/2, -pointSize/2, pointSize, pointSize); 
		        pointRenderer.setSeriesShape(j, s);	        		

		        plot.setRenderer(j, series.points ? pointRenderer : lineRenderer);
	        	
		        Color awtColor;
 		        if(series.color != null){
		        	awtColor = new Color(series.color[0], series.color[1], series.color[2]);		        		
		        }else{
		        	awtColor = defaultColors[nextColor];
		        	nextColor++;
		        	if(nextColor >= defaultColors.length)
		        		nextColor = 0;
		        }
		        	
		        lineRenderer.setSeriesPaint(j, awtColor);
	        	pointRenderer.setSeriesPaint(j, awtColor);
	        	//pointRenderer.
	        		
		        
		    	dataset.addSeries(xySeries);
		    	
		    	if(rangeHold){
		    		if(newMin < rangeHoldMin) rangeHoldMin = newMin;
		    		if(newMax > rangeHoldMax) rangeHoldMax = newMax;
		    	}
		    	
	    	}

	    	if(rangeHold){
		    	valueAxis.setRange(rangeHoldMin, rangeHoldMax);        	
	    	}
    	}
    }
    
    public void setAutoRangeIncludesZero(boolean autoRangeIncludesZero){
    	//NumberAxis valueAxis = (NumberAxis) ((XYPlot)frame.getChart().getPlot()).getRangeAxis();
    	valueAxis.setAutoRangeIncludesZero(autoRangeIncludesZero);
    }
    
    public void setRangeHold(boolean rangeHold){
    	NumberAxis valueAxis = (NumberAxis) ((XYPlot)frame.getChart().getPlot()).getRangeAxis();
    	Range range = valueAxis.getRange();
    	this.rangeHold = rangeHold;
    	if(rangeHold){
    		rangeHoldMin = range.getLowerBound();
    		rangeHoldMax = range.getUpperBound();
    		valueAxis.setAutoRange(false);
    	}else{
    		valueAxis.setAutoRange(true);
    	}
    }
    
    public void setRangeX(int x0, int x1){
    	
    }
    
    public void setDataset(XYDataset dataset) {
		plot.setDataset(dataset);
	}

	public void setColours(Color[] colors) {		
		AbstractXYItemRenderer renderer = (AbstractXYItemRenderer)plot.getRenderer();
		for(int i=0; i < colors.length; i++)
			renderer.setSeriesPaint(i, colors[i]);		
	}

	public void chartMouseMovedEvent(ChartMouseEvent event) {

        if(infoBox == null){
        	infoBox = new ToolTip(frame.getShell(), SWT.ICON_INFORMATION);
        }
        
		Rectangle dataArea = this.frame.getScreenDataArea();
        JFreeChart chart = event.getChart();
        XYPlot plot = (XYPlot) chart.getPlot();
        ValueAxis xAxis = plot.getDomainAxis();
        ValueAxis yAxis = plot.getRangeAxis();
        int xPx = event.getTrigger().getX();
        int yPx = event.getTrigger().getY();
        
        Rectangle2D r = new Rectangle2D.Float(dataArea.x, dataArea.y, dataArea.width, dataArea.height);
        
        if(xPx < dataArea.x || xPx >= (dataArea.x + dataArea.width) 
        		|| yPx < dataArea.y || yPx >= (dataArea.y + dataArea.height)){
        	infoBox.setText("Out of range");            
            infoBox.setVisible(false);
        	return;
        }

        lastX  = xAxis.java2DToValue(xPx, r, RectangleEdge.BOTTOM);
        lastY = yAxis.java2DToValue(yPx, r, RectangleEdge.LEFT);
        
        StringBuilder strB = new StringBuilder();
        //strB.append("Cursor: x=" + xPx + ", y=" + yPx + ", r= " + r.toString()+"\n");
        strB.append(String.format("Cursor: x=%05g, y=%05g\n", lastX, lastY));
        strB.append("Nearest data:\n");
        
        for(int i=0; i < dataset.getSeriesCount(); i++){
        	XYIntervalSeries series = dataset.getSeries(i);
        	Object key = series.getKey();
        	String name = (key != null && key instanceof String) ? ((String)key) : ("Series_" + i);
        	int j = getNearestXValue(series, lastX);
        	double xVal = series.getX(j).doubleValue();
        	double yVal = series.getYValue(j);        	
        	Paint paint = lineRenderer.getSeriesPaint(i);
        	String colName = "?";
        	if(paint instanceof Color){
        		Color c = (Color)paint;
        		colName = ((Color)paint).toString();
        		
        		Field fields[] = Color.class.getFields();
        		for(Field field : fields){
        			if(field.getType() == Color.class){
        				try {
							Object o = field.get(null);
							Color c2 = (Color)o;
							if(c.getRed() == c2.getRed() && 
			        					c.getGreen() == c2.getGreen() &&
			        					c.getBlue() == c2.getBlue()){
		        					colName = field.getName();
							}

						} catch (IllegalArgumentException
								| IllegalAccessException e) {
							e.printStackTrace();
						}
        			}
        		}
        		
        		
        	}
        	strB.append(String.format(" %s (%s): x=%05g, y=%05g\n",name,colName,xVal,yVal));
        }
        
        infoBox.setText(strB.toString());
        infoBox.setAutoHide(true);
        infoBox.setVisible(true);
	}
	
	/** Finds the index into xs[] nearest x (doubles)
	 * @params xs Array of values
	 * @param x Single value 
	 */	
	public final static int getNearestXValue(XYIntervalSeries series, double x){	
		int k,klo,khi;
		klo = 0;		
		khi = series.getItemCount() - 1;
		
		if(x <= series.getX(klo).doubleValue())return klo;
		if(x >= series.getX(khi).doubleValue())return khi;
		
		while( khi - klo > 1){
			k = (khi + klo) / 2;
			if( x < series.getX(k).doubleValue())
				khi = k;
			else
				klo = k;			
		}		
		if( (x - series.getX(klo).doubleValue()) < (series.getX(khi).doubleValue() - series.getX(klo).doubleValue())/2)
			return klo;		
		else
			return khi;
	}

	public void setLog10Y(boolean logAxis) {
		XYPlot xyPlot =  ((XYPlot)frame.getChart().getPlot());
		NumberAxis valueAxis = (NumberAxis)xyPlot.getRangeAxis();
		if(logAxis){
			if(valueAxis instanceof LogarithmicAxis)
				return;
			
			valueAxis = new LogarithmicAxis(null);
			((LogarithmicAxis)valueAxis).setAutoTickUnitSelection(true);
			
		}else{
			if(!(valueAxis instanceof LogarithmicAxis))
				return;
			
			valueAxis = new NumberAxis();
		}
		
		ScientificNumberFormat format = new ScientificNumberFormat();
        //format.
        
        NumberAxis xAxis = new NumberAxis(); //(NumberAxis) xyPlot.getDomainAxis();
        xAxis.setLowerMargin(0.00);  // reduce the default margins
        xAxis.setUpperMargin(0.00);
        xAxis.setAutoRangeIncludesZero(false);  // override default
        xAxis.setNumberFormatOverride(format);
        xyPlot.setDomainAxis(xAxis);        
        
        valueAxis.setLowerMargin(0.10);  // reduce the default margins
        valueAxis.setUpperMargin(0.10);
        valueAxis.setAutoRangeIncludesZero(false);  // override default
        valueAxis.setNumberFormatOverride(format);
        xyPlot.setRangeAxis(valueAxis);
	}
	
	public double[] getLastMousePos(){ return new double[]{ lastX, lastY }; }
}
