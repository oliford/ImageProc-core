package imageProc.graph;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.LinearInterpolation1D;
import algorithmrepository.Mat;
import binaryMatrixFile.AsciiMatrixFile;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.graph.shapeFit.FuncFitter;
import net.jafama.FastMath;
import otherSupport.SettingsManager;

/** The image processor part of the GraphUtil (as distinct from the SWT GUI part)
 *  It handles the basic arithmetic and anything that's non GUI specific
 *  All the state should be held here since mulitple controllers might 
 */
public class GraphUtilProcessor extends ImgSourceOrSinkImpl implements ImgSink {
	public static final String scanDirNames[] = new String[]{ "X", "Y", "T" };
	public static final String timebaseNames[] = new String[]{ "Image Num", "MetaSeries" };
	
	private int imagesInChangeID[] = new int[0];
	private int selectedImageChangeID = -1;	
	private boolean selChanged = true;
	private boolean subX0 = false;
	private boolean subY0 = false;
	private boolean includeNaNInAvg = false;
	private boolean idle = true;
	
	private boolean signalAbort = false;
	
	/** Display profile vs x at given y */
	public static final int SCAN_DIR_X = 0;
	/** Display profile vs y at given x */
	public static final int SCAN_DIR_Y = 1;
	/** Display profile vs x averaged over ROI */
	public static final int SCAN_DIR_T = 2;
	
	protected int scanType = SCAN_DIR_X;
	
	public static final int TIMEBASE_IMAGE_NUM = 0;
	public static final int TIMEBASE_META_SERIES = 1;
	
	private int timebaseType = TIMEBASE_IMAGE_NUM; 
	private String timebaseMetaSeries = null;
			
	private boolean roiAvg = false;
	
	private boolean autoCalc = true;

	private int roiX0 = -1, roiY0 = -1, roiW, roiH;
	private int posX = -1, posY = -1;
	private double val = Double.NaN;
	
	/** The primary series - x/y/t scan of image */
	private Series primarySeries = null;	
	/** List of all series from the image (other fields, sigma etc) */
	private List<Series> imageSeriesList;
	/** List of all series from the image and other modules */
	private List<Series> seriesList;
	
	private double roiSum;
	private double roiMin, roiMax;
	
	private FuncFitter funcFitter = new FuncFitter(this);
	private boolean calcSigma = true;
	
	private MetaDataPlotter metaPlotter = new MetaDataPlotter(this);
					
	public GraphUtilProcessor() { 
		setSource(null);
	}
	
	@Override
	protected void selectedImageChanged() {
		if(autoCalc)
			calc();		
	}
	
	public void setRect(int x0, int y0, int w, int h) {		
		this.roiX0 = x0; this.roiY0 = y0;
		this.roiW = w; this.roiH = h;
		selChanged = true;
		//System.out.println("GrapthUtil rect (" + x0 + ", " + y0 + " " + w +" x " + h + ")");
		calc();
	}

	public void setPoint(int x, int y) {
		this.posX = x; this.posY = y;
		selChanged = true;
		//System.out.println("GrapthUtil pos (" + x + ", " + y + "), val = ");// + ((image == null) ? "null" : image.getPixelValue(x, y)));
		calc();
	}
	
	@Override
	public void imageChangedRangeComplete(int idx) {
		calc();
	}
		
	@Override
	public void calc(){
		signalAbort = false;
		idle = false;
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalc();  } });
	}
	
	/** external direct use of doScan() */
	public void doCalcNow(){
		doCalc();
	}
	
	/** external direct use of doScan() */
	public void doCalcNowROI(int x0, int y0, int w, int h){
		this.roiX0 = x0; this.roiY0 = y0;
		this.roiW = w; this.roiH = h;
		this.roiAvg = true;
		selChanged = true;
		doCalc();
	}
	
	private void doCalc(){
		
		LinkedList<Series> newSeriesList = new LinkedList<Series>();
		
		calcImageSeriesList();
		
		if(imageSeriesList != null)
			newSeriesList.addAll(imageSeriesList);
		
		if(primarySeries != null){
			try{
				funcFitter.doFit(primarySeries);
				List<Series> fitSeriesList = funcFitter.getSeriesList();
				if(fitSeriesList != null)
					newSeriesList.addAll(fitSeriesList);
			}catch(RuntimeException err){
				err.printStackTrace();
			}
		}
		
		metaPlotter.addSeries(newSeriesList);
		
		addExtensionSeries(newSeriesList);
	
		seriesList = newSeriesList;
		updateAllControllers();
		idle = true;
		
	}
	

	private void calcImageSeriesList(){ 
		
		Img image = getSelectedImageFromConnectedSource();
		
		if(image == null || image.isDestroyed()){ //|| (scanType == SCAN_DIR_T && !image.isRangeValid())){ 
			primarySeries = null;
			imageSeriesList = null;
			updateAllControllers();
			return; 
		}
		
		ArrayList<Series> newImageSeriesList = new ArrayList<Series>();
		Series newPrimarySeries = null;
				
		try{ 
			ReadLock readLock = image.readLock();
			readLock.lockInterruptibly(); 
			
			try{
			
				int nImages = connectedSource.getNumImages();
				
				boolean anythingChanged = selChanged || funcFitter.needsUpdate() || extensionsNeedUpdate();				
				
				//see if we actually need to update
				if(scanType == SCAN_DIR_T){
					//just all images
					if(imagesInChangeID.length != nImages){
						imagesInChangeID = Arrays.copyOf(imagesInChangeID, nImages);
						anythingChanged = true;
					}
					for(int i=0; i < nImages; i++){
						Img imgT = connectedSource.getImage(i);
						int changeID = (imgT != null) ? imgT.getChangeID() : 0;
						if(imagesInChangeID[i] != changeID){
							anythingChanged = true;
							imagesInChangeID[i] = changeID;
						}
					}
				}else{
					//just check the selected one
					int changeID = (image != null) ? image.getChangeID() : 0;
					if(selectedImageChangeID != changeID){
						anythingChanged = true;
						selectedImageChangeID = changeID;
					}
				}
				
				if(imageSeriesList != null && !anythingChanged)
					return;			
				
				for(int iF=0; iF < image.getNFields(); iF++){ //multiple fields
					
					Series newImageSeries = calcFrameSeries(image, readLock, iF);
					if(signalAbort)
						return;
					
					if(iF == 1 && calcSigma){ //special handling to treat frame 1 as sigma of frame 0
						
						//for ROI binning, the error is reduced to the averaging
						double errorReduction = 1.0;
						if(roiAvg){
							switch (scanType) {
								case SCAN_DIR_X: errorReduction = 1.0 / FastMath.sqrt(roiH); break;
								case SCAN_DIR_Y: errorReduction = 1.0 / FastMath.sqrt(roiW); break;
								case SCAN_DIR_T: errorReduction = 1.0 / FastMath.sqrt(roiW * roiH); break;								
							}
						}
						
						double y[] = newPrimarySeries.data;
						double s[] = newImageSeries.data;
						double m[] = new double[y.length];
						double p[] = new double[y.length];						
						for(int j=0; j < y.length; j++){
							s[j] *= errorReduction;
							p[j] = y[j] + 2 * s[j];
							m[j] = y[j] - 2 * s[j];
						}
						
						//newImageSeriesList.add(new Series(newPrimarySeries.name + "-sigma", newImageSeries.x, s));
						newImageSeriesList.add(new Series(newPrimarySeries.name() + "-lower2Sigma", newImageSeries.x.clone(), m));
						newImageSeriesList.add(new Series(newPrimarySeries.name() + "-upper2Sigma", newImageSeries.x.clone(), p));
						
					}else
						newImageSeriesList.add(newImageSeries);
					
					
					if(iF == 0)
						newPrimarySeries = newImageSeries;
				}
				
			}catch(IndexOutOfBoundsException err){
				System.err.println("GraphUtil: Index out of bounds on image, ROI=("+roiX0+", "+roiY0+")-("
						+ (roiX0+roiW-1)+", "+(roiY0+roiH-1)+"), POS=("+posX+","+posY+")");
			}finally{
				readLock.unlock();
			}
			
		}catch(InterruptedException e){
			primarySeries = null;
			imageSeriesList = null;
			return; 
		}	
		
		//a meta series array with two entry can also be given, which is used to interpolate
		//the first (or just image number) into something else
		if(timebaseMetaSeries != null){
			double ret[][] = metaPlotter.scanFromMetaData(timebaseMetaSeries);
			if(signalAbort)
				return;
			if(ret != null && ret.length > 1 && ret[0].length > 1){
				Interpolation1D interp = new Interpolation1D(ret[0], ret[1], InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT_END,0);				
				for(Series s: newImageSeriesList)
					s.x = interp.eval(s.x);
			}
		}
		
		if(subX0){
			for(Series s: newImageSeriesList){
				for(int i=s.x.length-1; i >= 0; i--){
					s.x[i] -= s.x[0];
				}
			}
		}
		if(subY0){
			for(int i=newPrimarySeries.data.length-1; i >=0 ; i--){
				newPrimarySeries.data[i] -= newPrimarySeries.data[0];
			}
		}
		
		imageSeriesList = newImageSeriesList;
		primarySeries = newPrimarySeries;
		
		return;
	}

	private Series calcFrameSeries(Img image, ReadLock readLock, int iF){
		Series newImageSeries = new Series();
		
		int nImages = connectedSource.getNumImages();
		int w = image.getWidth(), h = image.getHeight();
		
		selChanged = false;
			
		if(!roiAvg){				
			switch(scanType){
				case SCAN_DIR_X:
					newImageSeries.name("scan X " + iF);
					newImageSeries.x = new double[w];
					newImageSeries.data = new double[w];
					for(int x=0; x < w; x++){
						newImageSeries.x[x] = x;
						newImageSeries.data[x] = image.getPixelValue(readLock, iF, x, posY);
					}
					break;
				case SCAN_DIR_Y:
					newImageSeries.name("scan Y" + iF);
					newImageSeries.x = new double[h];
					newImageSeries.data = new double[h];
					for(int y=0; y < h; y++){
						newImageSeries.x[y] = y;
						newImageSeries.data[y] = image.getPixelValue(readLock, iF, posX, y);
					}
					break;
				case SCAN_DIR_T:
					newImageSeries.name("scan T" + iF);
					newImageSeries.x = new double[nImages];
					newImageSeries.data = new double[nImages];
					for(int t=0; t < nImages; t++){
						if(signalAbort)
							return null;
						
						newImageSeries.x[t] = t;
						newImageSeries.data[t] = Double.NaN;
						Img imgT = connectedSource.getImage(t);
						if(imgT != null && imgT.isRangeValid() && posX < imgT.getWidth() && posY < imgT.getHeight()){
							try{
								//we can get into a deadlock here
								//because we hold the lock on the current
								//image and wait for locks on the others
								//We don't actually care that much though, so we have a short
								//timeout
								ReadLock readLockT = imgT.readLock();
								if(imgT == image || readLockT.tryLock(0, TimeUnit.SECONDS)){
									//readLockT.lockInterruptibly(); 
									try{
										newImageSeries.data[t] = imgT.getPixelValue(readLockT, iF, posX, posY);								
									}finally{
										if(imgT != image)
											readLockT.unlock(); 
									}
								}
							}catch(InterruptedException err){
								newImageSeries.data[t] = Double.NaN;								
							}
						}
					}
					break;
					
				default:
					newImageSeries.name("???");
					newImageSeries.x = null;
					newImageSeries.data = null;
			}
		}else{
			switch(scanType){
				case SCAN_DIR_X:
					newImageSeries.name("scan X" + iF + " ROI");
					newImageSeries.x = new double[roiW];
					newImageSeries.data = new double[roiW];
					roiSum = 0;
					roiMin = Double.POSITIVE_INFINITY;
					roiMax = Double.NEGATIVE_INFINITY;
					double W = 1.0;
					for(int iX=0; iX < roiW; iX++){		
						double sumW = 0.0;
						for(int y=roiY0; y < (roiY0 + roiH); y++){
							newImageSeries.x[iX] = roiX0 + iX;
							double val =  image.getPixelValue(readLock, iF, roiX0 + iX, y);
							if(iF == 0 && calcSigma && image.getNFields() > 1){
								W = image.getPixelValue(readLock, 1, roiX0 + iX, y);
								W = 1.0 / (W*W);
							}
							if(includeNaNInAvg || !Double.isNaN(val)){
								newImageSeries.data[iX] += val * W;
								sumW += W;
							}
							if(val < roiMin) roiMin = val;
							if(val < roiMax) roiMax = val;
						}
						roiSum += newImageSeries.data[iX]; 
						newImageSeries.data[iX] /= sumW;
					}						
					break;
				case SCAN_DIR_Y:
					newImageSeries.name("scan Y" + iF + " ROI");
					newImageSeries.x = new double[roiH];
					newImageSeries.data = new double[roiH];
					roiSum = 0;
					roiMin = Double.POSITIVE_INFINITY;
					roiMax = Double.NEGATIVE_INFINITY;
					W = 1.0;
					for(int iY=0; iY < roiH; iY++){	
						double sumW = 0.0;
						for(int x=roiX0; x < (roiX0 + roiW); x++){
							newImageSeries.x[iY] = roiY0 + iY;
							double val = image.getPixelValue(readLock, iF, x, roiY0 + iY);
							if(iF == 0 && calcSigma && image.getNFields() > 1){
								W = image.getPixelValue(readLock, 1, x, roiY0 + iY);
								W = 1.0 / (W*W);
							}
							if(includeNaNInAvg || !Double.isNaN(val)){
								newImageSeries.data[iY] += val * W;
								sumW += W;
							}
							if(val < roiMin) roiMin = val;
							if(val < roiMax) roiMax = val;
						}
						roiSum += newImageSeries.data[iY];
						newImageSeries.data[iY] /= sumW;
					}
					break;
				case SCAN_DIR_T:
					newImageSeries.name("scan T" + iF + " ROI");
					newImageSeries.x = new double[nImages];
					newImageSeries.data = new double[nImages];
					roiSum = 0;
					roiMin = Double.POSITIVE_INFINITY;
					roiMax = Double.NEGATIVE_INFINITY;
					for(int t=0; t < nImages; t++){
						if(signalAbort)
							return null;
						
						Img imgT = connectedSource.getImage(t);
						newImageSeries.x[t] = t;
						newImageSeries.data[t] = 0;
						
						if(imgT == null || !imgT.isRangeValid()){
							roiSum = Double.NaN;
							newImageSeries.data[t] = Double.NaN;
							continue;
						}		
						
						try{
							ReadLock readLockT = imgT.readLock();							 
							if(imgT == image || readLockT.tryLock(0, TimeUnit.SECONDS)){
								try{
									double sumW = 0.0;
									W = 1.0;
									for(int x=roiX0; x < (roiX0 + roiW); x++){
										for(int y=roiY0; y < (roiY0 + roiH); y++){
											
											if(x < imgT.getWidth() && y < imgT.getHeight()){
														
												double val = imgT.getPixelValue(readLockT, iF, x, y);
												if(iF == 0 && calcSigma && imgT.getNFields() > 1){
													W = imgT.getPixelValue(readLockT, 1, x, y);
													W = 1.0 / (W*W);
												}
												if(includeNaNInAvg || !Double.isNaN(val)){
													newImageSeries.data[t] += val * W;
													sumW += W;
												}
												if(val < roiMin) roiMin = val;
												if(val < roiMax) roiMax = val;
											}
										}
									}
									roiSum += newImageSeries.data[t];
									newImageSeries.data[t] /= sumW;
									
								}finally{ 
									if(imgT != image)
										readLockT.unlock(); 
								}
							}
						}catch(InterruptedException err){
							newImageSeries.data[t] = Double.NaN;
							roiSum = Double.NaN;
						}
					}
					break;
				default:
					newImageSeries.name("??? ROI");
					newImageSeries.x = null;
					newImageSeries.data = null;
					return newImageSeries; 
			}
		}
	
		//Calc series min/max
		newImageSeries.min = Double.POSITIVE_INFINITY;
		newImageSeries.max = Double.NEGATIVE_INFINITY;
		newImageSeries.sum = 0;
		for(int i=0;i < newImageSeries.data.length; i++){
			if(newImageSeries.data[i] > newImageSeries.max) newImageSeries.max = newImageSeries.data[i];
			if(newImageSeries.data[i] < newImageSeries.min) newImageSeries.min = newImageSeries.data[i];
			newImageSeries.sum += newImageSeries.data[i];			
		}
		
		if(posX >= 0 && posY >=0 && posX < image.getWidth() && posY < image.getHeight())
			this.val = image.getPixelValue(readLock, posX, posY);
		else
			this.val = Double.NaN;

		return newImageSeries; 
		
	}
	
	
	
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			GraphUtilSWTControl swtGraphControl = new GraphUtilSWTControl(this, (Composite)args[0], (Integer)args[1]);
			controllers.add(swtGraphControl);
			return swtGraphControl;
		}else
			return null;
	}

	public void setScanType(int scanType, boolean roiAvg, boolean includeNaNInAvg, 
						boolean subtractX0, boolean subtractY0, boolean calcSigma) { 
		this.scanType = scanType;
		this.roiAvg = roiAvg;
		this.subX0 = subtractX0;
		this.subY0 = subtractY0;
		this.includeNaNInAvg = includeNaNInAvg;
		this.calcSigma = calcSigma;
		selChanged = true;
		calc(); 
	}
	
	public int getScanDir(){ return scanType; }
	public int getTimebaseType(){ return timebaseType; }
	public boolean isScanROIAvg(){ return roiAvg; }
	public boolean getSubtractX0(){ return subX0; }
	public boolean getSubtractY0(){ return subY0; }
	public boolean getIncludeNaNInAvg(){ return includeNaNInAvg; }
	public boolean getCalcSigma() { return calcSigma; }
	
	public int getPosX(){ return posX; }
	public int getPosY(){ return posY; }
	public int getROIX0(){ return roiX0; }
	public int getROIY0(){ return roiY0; }
	public int getROIWidth(){ return roiW; }
	public int getROIHeight(){ return roiH; }
	
	public double getValue(){ return val; }
	public double getMin(){ return (primarySeries == null) ? Double.NaN : roiMin; }
	public double getMax(){ return (primarySeries == null) ? Double.NaN : roiMax; }
	public double getMean(){ return (primarySeries == null) ? Double.NaN : (roiSum / (roiW * roiH)); }
	public double getSum(){ return (primarySeries == null) ? Double.NaN : roiSum; }
	
	public List<Series> getSeriesList(){ return seriesList; }
	

	public void setTimebase(String metaSeriesName){
		if(this.timebaseMetaSeries == metaSeriesName)
			return;
		
		if(metaSeriesName != null && metaSeriesName.endsWith(" (time series)"))
			metaSeriesName = metaSeriesName.substring(0, metaSeriesName.length()-14);
		
		
		this.timebaseMetaSeries = metaSeriesName;
		selChanged = true;
		calc();
	}	
	
	
	
	public FuncFitter getFitter(){ return funcFitter; }
	
	@Override
	public void destroy() {
		super.destroy();
	}
	
	@Override
	public void updateAllControllers() { //make public for subProcs
		super.updateAllControllers();
	}
	
	@Override
	public boolean isIdle() { return idle;}

	@Override
	public boolean getAutoUpdate() { return this.autoCalc; }

	@Override
	public void setAutoUpdate(boolean enable) { this.autoCalc = enable; }

	@Override
	public boolean wasLastCalcComplete(){  return true;  } //we didn't really careif we don't finish
	
	@Override
	public void abortCalc(){
		if(!idle)
			signalAbort = true;		
	}
	
	public boolean isAbortSignalled(){ return signalAbort; }

	//Tie-ins for application specific extensions
	protected void addExtensionSeries(LinkedList<Series> newSeriesList) { }
	protected boolean extensionsNeedUpdate() { return false;	}

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "Graph[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}

	public MetaDataPlotter getMetaPlotter() { return metaPlotter; }

	public String getTimebaseMetaName() { return timebaseMetaSeries; }
	
	public void saveTraces() {

		int i=0;
		for(Series series : seriesList){			
			Mat.mustWriteAscii(System.getProperty("java.io.tmpdir") + "/graphData/"+ ((series.name() != null) ? series.name() : i) + ".txt",
					new double[][]{ series.x, series.data }, true);			
			i++;
		}
	}
	
	public void freezeTraces() {
				
		ArrayList<String> metaPlotList = metaPlotter.getMetadataToPlotList();
		for(Series series : imageSeriesList){
			
			String metaName = "/graph/freezeTrace/" + series.name();
			connectedSource.setSeriesMetaData(metaName, series.data, scanType == SCAN_DIR_T);
			if(!metaPlotList.contains(metaName))
				metaPlotList.add(metaName);			
		}
	}
}
