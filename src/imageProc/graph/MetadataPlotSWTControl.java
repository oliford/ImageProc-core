package imageProc.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import picamJNI.PICamROIs.PICamROI;

/** GUI panel for plotting of metadata */
public class MetadataPlotSWTControl {
	private Group metaGroup;
	
	private Table table; 
	private Button refreshButton;
	private Button clearButton;
	private Button[] standardSetButtons;
	
	private GraphUtilProcessor proc;
	
	private final static String colNames[] = { "En", "Name", "Colour" };
	
	public MetadataPlotSWTControl(Composite parent, int style, GraphUtilProcessor proc) {
		this.proc = proc;
			
		metaGroup = new Group(parent, style);
		metaGroup.setText("Metadata");
		metaGroup.setLayout(new GridLayout(6, false));
		ImageProcUtil.addRevealWriggler(metaGroup);		
		
		refreshButton = new Button(metaGroup, SWT.NONE);
		refreshButton.setText("Refresh");
		refreshButton.setToolTipText("Refill the table with the current metadata entries. This is not done automatically");
		refreshButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		refreshButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refreshButtonEvent(event); }});
		
		clearButton = new Button(metaGroup, SWT.NONE);
		clearButton.setText("Clear");
		clearButton.setToolTipText("Deselect all entries and remove them from the graph.");
		clearButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		clearButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { clearButtonEvent(event); }});
		
		//TODO: Generalise and get from W7X version, or store somewhere. Put the buttons in a group
		standardSetButtons = new Button[1];
		standardSetButtons[0] = new Button(metaGroup, SWT.NONE);
		standardSetButtons[0].setText("Time selection");
		standardSetButtons[0].setToolTipText("Show the time series metadata entries created by SeriesProc to show which frames are used for background and averaging.");
		standardSetButtons[0].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		standardSetButtons[0].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { standardSetButtonEvent(event, 0); }});
		
		
		table = new Table(metaGroup, SWT.BORDER | SWT.MULTI);		
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn cols[] = new TableColumn[colNames.length];
		for (int i = 0; i < colNames.length; i++) {
			cols[i] = new TableColumn(table, SWT.BORDER | SWT.V_SCROLL);
			cols[i].setText(colNames[i]);
		}

		updateTable();
		
		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		table.addListener(SWT.MouseDoubleClick, new Listener() { 
			@Override
			public void handleEvent(Event event) {
				for (int i = 0; i < colNames.length; i++)
					cols[i].pack();
			}
		});

		table.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		//table.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
	}
	
	
	protected void refreshButtonEvent(Event event) {
		updateTable();
	}
	
	protected void clearButtonEvent(Event event) {
		proc.getMetaPlotter().setMetadataList(new ArrayList<String>());
		updateTable();
	}	
	
	protected void standardSetButtonEvent(Event event, int setIdx) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("/SeriesProc/inputIsBackground");
		list.add("/SeriesProc/inputIsGoodData");
		proc.getMetaPlotter().setMetadataList(list);
		updateTable();
	}
	
	private void tableMouseDownEvent(Event event) {
		
		Rectangle clientArea = table.getClientArea();
		Point pt = new Point(clientArea.x + event.x, event.y);
		int index = table.getTopIndex();
		while (index < table.getItemCount()) {
			final TableItem item = table.getItem(index);
			for (int i = 0; i < table.getColumnCount(); i++) {
				Rectangle rect = item.getBounds(i);
				if (rect.contains(pt)) {
					final int column = i;
					if (column == 0) {
						// pointsTable.setSelection(index); //make sure its
						// selected
						if(item.getText(0).equals("Y")){
							item.setText(0, "");
						}else{
							item.setText(0, "Y");
						}
						ArrayList<String> selected = new ArrayList<String>();
						for(TableItem item2 : table.getItems()){
							if(item2.getText(0).equals("Y")){
								selected.add(item2.getText(1));
							}
						}
						proc.getMetaPlotter().setMetadataList(selected);
						return;						
					}
					return;
				}				
			}			
			index++;
		}
	}
	
	private void updateTable(){
		
		List<String> all = proc.getMetaPlotter().getAllMetadataNames();		
		Collections.sort(all);
		
		ArrayList<String> selected = proc.getMetaPlotter().getMetadataToPlotList();
		
		
		table.removeAll();
		for(String metaName : all){
				TableItem item = new TableItem(table, SWT.NONE);
				item.setText(1, metaName);
				
				item.setText(0, "");
				
				for(String s : selected){
					if(metaName.startsWith(s)){ //metaName often has '(time series)' on the end
						item.setText(0, "Y");
					}
				}
		}
		
		for (int i = 0; i < colNames.length; i++){
			table.getColumn(i).pack();
		}
	}
	
	public void update(){
		//updateTable();
	}

	public Control getSWTGroup() { return metaGroup; }
	
	
}
