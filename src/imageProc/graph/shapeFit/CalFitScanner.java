package imageProc.graph.shapeFit;

import descriptors.gmds.GMDSSignalDesc;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.swt.ImagePanel;
import imageProc.database.gmds.GMDSUtil;
import imageProc.graph.GraphUtilProcessor;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.StatusOutput;
import signals.gmds.GMDSSignal;

/** Code for performing the PolCal fit across a scan of the image and outputting the µ calibration */
public class CalFitScanner {
	
	private GraphUtilProcessor proc;
	
	// We need the CCD/AOI geometry information to make sense of what we have 
	private PhysicalROIGeometry ccdGeom;
	
	public CalFitScanner(GraphUtilProcessor proc) {
		this.proc = proc;
	}

	private String imageScanSyncObject = new String("imageScanSyncObject");
	/** Scan image and dump param maps */ 
	public void imageScan(int dx, int dy) {
		final int fdx = dx, fdy = dy;
		abortScan = false;
		ImageProcUtil.ensureFinalUpdate(imageScanSyncObject, new Runnable() { @Override public void run() { doImageScan(fdx, fdy); } });
	};
	
	public void abortScan() {
		abortScan = true;
	}
		
	private boolean abortScan;
	private void doImageScan(int nX, int nY) {
		String outPath = System.getProperty("java.io.tmpdir") + "/imageScanFit";
		
		
		//basic calc scan setup
		proc.setScanType(GraphUtilProcessor.SCAN_DIR_T, true, false, false, false, false);
		
		proc.getFitter().setAutoFit(false); //this interferes with us
		
		Img img = proc.getSelectedImageFromConnectedSource();
		int w = img.getWidth(), h = img.getHeight();
		double dx = (double)w / nX;
		double dy = (double)h / nY;
		
		double meanX[] = Mat.linspace(dx/2, w-(dx/2), nX);
		double meanY[] = Mat.linspace(dy/2, h-(dy/2), nY);
		
		int nP = proc.getFitter().getInitParams().length;
		int nT = proc.getConnectedSource().getNumImages();
		
		double params[][][] = new double[nP][nX][nY];
		double data[][][] = new double[nX][nY][nT];
		double fit[][][] = new double[nX][nY][nT];
		double t[] = new double[nT];
		
		System.out.println("FuncFitter.imageScan(): Writing param maps to " + outPath);
		
		StatusOutput stat = new StatusOutput("FuncFitter.imageScan", nX*nY);
yloop:	for(int y=0; y < nY; y++){
			for(int x=0; x < nX; x++){
				proc.getFitter().invalidate(); //invalidate fit
				int x0 = (int)FastMath.floor(x*dx);
				int y0 = (int)FastMath.floor(y*dy);
				proc.doCalcNowROI(x0, y0, (int)FastMath.floor(dx), (int)FastMath.floor(dy));
                
                //store the fit params
				double p[] = proc.getFitter().getFinalParams();
				for(int iP=0; iP < nP; iP++)
					params[iP][x][y] = p[iP];
				
				//store the trace
				try{
					data[x][y] = proc.getFitter().getData().clone();
					fit[x][y] = proc.getFitter().getFinalFit().clone();
					
				}catch(NullPointerException err){
					//If the user forces a re-fit, it can mess with the variables
					System.err.println("Missed some trace data - Don't try to force a  fit during the scan!");
				}
				
				stat.doStatus(y*nX+x);
				
				//force a redraw of the window. This is a very crude and processor heavy way to do this
				// as it recreates the whole image. Really there needs to be a public force repaint in ImageWindow 
				int idx = proc.getSelectedSourceIndex();
				for(ImgSink sink : proc.getConnectedSource().getConnectedSinks()){					
					if(sink instanceof ImagePanel){
						((ImagePanel)sink).imageChangedRangeComplete(idx);
					}
				}
								
				if(abortScan){
					System.out.println("FuncFitter.imageScan(): Scan aborted");
					break yloop;
				}
			}
		}
		stat.done();
		
		t = proc.getFitter().getX().clone();
		
		if(abortScan)
			return;
				
		saveResults(outPath, meanX, meanY, params, new double[][][]{{t}}, data, fit);

	}
	
	private void saveCCDPhysXY(double[] meanX, double[] meanY){
		//need to also save the physCCD coords so that the params can be applied later to any AOI
		ccdGeom = new PhysicalROIGeometry();
		//err, this should really get set up from proc's metadata tree
		String exp = GMDSUtil.getMetaExp(proc.getConnectedSource());
		int pulse = GMDSUtil.getMetaDatabaseID(proc.getConnectedSource());
		//ccdGeom.setFromCameraMetadata(IMSEProc.globalGMDS(), exp, pulse,  "RAW/seriesData");
		ccdGeom.setFromCameraMetadata(proc.getCompleteSeriesMetaDataMap().extractSingleArrayElements());
		
		double physX[] = new double[meanX.length];
		for(int i=0; i < meanX.length; i++)
			physX[i] = ccdGeom.getImgPhysX0() + meanX[i] * ccdGeom.getImgPhysW() / ccdGeom.getImgPixelW();
		
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, exp, "ANLYS/imageScanFit/finalP/ccdPhysX");
		GMDSSignal sig = new GMDSSignal(sigDesc, physX);
		GMDSUtil.globalGMDS().writeToCache(sig);
		
		double physY[] = new double[meanY.length];
		for(int i=0; i < meanY.length; i++)
			physY[i] = ccdGeom.getImgPhysY0() + meanY[i] * ccdGeom.getImgPhysH() / ccdGeom.getImgPixelH();
		
		sigDesc = new GMDSSignalDesc(pulse, exp, "ANLYS/imageScanFit/finalP/ccdPhysY");
		sig = new GMDSSignal(sigDesc, physY);
		GMDSUtil.globalGMDS().writeToCache(sig);
		
	}

	private void saveResults(String outPath, double meanX[], double meanY[], 
					double params[][][], double t[][][], double data[][][], double fit[][][]) {
		
		saveCCDPhysXY(meanX, meanY);
	   
		int pulse = GMDSUtil.getMetaDatabaseID(proc.getConnectedSource());
		String exp = GMDSUtil.getMetaExp(proc.getConnectedSource());
		
		try{	
    		
			//write the param 'images'
			for(int iP = 0; iP < params.length; iP++){
	    			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, exp, "ANLYS/imageScanFit/finalP/IMAGE_" + iP);
	    			GMDSSignal sig = new GMDSSignal(sigDesc, params[iP]);
	    			GMDSUtil.globalGMDS().writeToCache(sig);
	    	}
			
	       
	        //write the data and fit (not as images)
    		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, exp, "ANLYS/imageScanFit/scan/t");
			GMDSSignal sig = new GMDSSignal(sigDesc, t[0][0]);
			GMDSUtil.globalGMDS().writeToCache(sig);
			
			 //write the data and fit (not as images)
    		sigDesc = new GMDSSignalDesc(pulse, exp, "ANLYS/imageScanFit/scan/data");
			sig = new GMDSSignal(sigDesc, data);
			GMDSUtil.globalGMDS().writeToCache(sig);
			
			 //write the data and fit (not as images)
    		sigDesc = new GMDSSignalDesc(pulse, exp, "ANLYS/imageScanFit/scan/fit");
			sig = new GMDSSignal(sigDesc, fit);
			GMDSUtil.globalGMDS().writeToCache(sig);
			
			//write the image count for the param 'images'            
            sigDesc = new GMDSSignalDesc(pulse, exp, "ANLYS/imageScanFit/finalP/IMAGE_COUNT");
            sig = new GMDSSignal(sigDesc, new int[]{ params.length });
            GMDSUtil.globalGMDS().writeToCache(sig);   

			
		}catch(RuntimeException err){
			System.err.println("CalFitScanner: Error saving cal fit results to exp " + exp + 
								", pulse " + pulse + " because " + err);
		}
	}

}
