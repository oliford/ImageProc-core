package imageProc.graph.shapeFit;

import net.jafama.FastMath;
import seed.digeom.FunctionND;
import seed.digeom.IDomain;
import seed.digeom.RectangularDomain;

public class CostF extends FunctionND {
	
	private double x[];
	private double d[];
	private double sigma = 1;
	private int funcType;
	private double initP[], minP[], maxP[];
	private boolean paramEnable[];
	private int nSelected;
		
	public CostF(int funcType, double x[], double d[], boolean paramEnable[], 
					double initP[], double minP[], double maxP[]) {
		this.funcType = funcType;
		this.x = x;
		this.d = d;
		this.paramEnable = paramEnable;
		this.initP = initP;
		this.minP = minP;
		this.maxP = maxP;
		for(int i=0; i < initP.length; i++)
			if(paramEnable[i])
				nSelected++; 
	}
	
	public final double[] toSelectedParams(double allParams[]){
		double selectedParams[] = new double[nSelected];
		int j=0;
		for(int i=0; i < initP.length; i++)
			if(paramEnable[i])
				selectedParams[j++] = allParams[i];
		return selectedParams;
	}
	
	public final double[] toAllParams(double selectedParams[]){
		//get full param array from enable ones and inital ones for disabled
		double allParams[] = new double[initP.length];
		int j=0;
		for(int i=0; i < initP.length; i++){
			allParams[i] = paramEnable[i] ? selectedParams[j++] : initP[i];
		}
		return allParams;
	}
	
	@Override
	/** eval, from selected parameters only */
	public final double eval(double[] selectedParams) {
		return evalAllParams(toAllParams(selectedParams));		
	}
	
	/** eval, takes all parameters */
	public final double evalAllParams(double[] p) {
			
		double logP = 0;
		double sigmaAbove = 1.0;
		double sigmaBelow = 1.0;
		
		double v[] = func(p);
		
		for(int i=0; i < d.length; i++){
			if(x[i] != 0 && !Double.isNaN(x[i]) && !Double.isNaN(d[i])){				
				logP += 0.5 * FastMath.pow2((d[i] - v[i]) / ((d[i] > v[i]) ? sigmaAbove : sigmaBelow));
			}
		}
		
		return logP;
	}
		
	public final double[] diff(double p[]){
		double v[] = func(p);
		double ret[] = new double[d.length];
		for(int i=0; i < d.length; i++){
			ret[i] = d[i] - v[i];
		}
		return ret;
	}
		
	public final double[] func(double p[]){
		double f[] = new double[x.length];
		switch(funcType){
			case FuncFitter.FUNC_TRIANGLE:{
				double l = p[0], phs = p[1]*Math.PI/180, A = p[2], y0 = p[3];
				for(int i=0; i < x.length; i++){
					double xx = x[i] + phs*l/(2*Math.PI);
					f[i] = y0 + A*FastMath.signum(xx) * (4*FastMath.abs(((FastMath.abs(xx)/l + 0.75) % 1.0) - 0.5) - 1);
				}
				return f;
				
			}case FuncFitter.FUNC_SAWTOOTH:{
				double l = p[0], phs = p[1]*Math.PI/180, A = p[2], y0 = p[3];
				for(int i=0; i < x.length; i++){
					double xx = x[i] + phs*l/(2*Math.PI);
					if(xx > 0)
						f[i] = y0 + A * (xx/l % 1.0);
					else
						f[i] = y0 + A * (1.0 + (xx/l % 1.0));
				}
				return f;
				
			}case FuncFitter.FUNC_SINE:{
				double l = p[0], phs = p[1]*Math.PI/180, A = p[2], y0 = p[3];
				for(int i=0; i < x.length; i++)
					f[i] = y0 + A*FastMath.sin(2*Math.PI*x[i]/l + phs);

				return f;
			}case FuncFitter.FUNC_ABSSINE:{
				double l = p[0], phs = p[1]*Math.PI/180, A = p[2], y0 = p[3];
				for(int i=0; i < x.length; i++)
					f[i] = y0 + A*FastMath.abs(FastMath.sin(2*Math.PI*x[i]/l + phs));

				return f;
			}case FuncFitter.FUNC_QUADRATIC:{
				double x0 = p[0], a0 = p[1], a1 = p[2], a2 = p[3];
				for(int i=0; i < x.length; i++)
					f[i] = a0 + a1*(x[i] - x0) + a2*(x[i] - x0)*(x[i] - x0);

				return f;
			}case FuncFitter.FUNC_GAUSSIAN:{
				double A = p[0], x0 = p[1], fwhm = p[2], y0 = p[3];
				double sigma = fwhm / 2.35;
				for(int i=0; i < x.length; i++)
					f[i] = y0 + A * FastMath.exp(-FastMath.pow2((x[i] - x0) / sigma) /2);
				return f;
			}case FuncFitter.FUNC_GAUSSIAN_LINEAR:{
				double A = p[0], x0 = p[1], fwhm = p[2], y0 = p[3], m = p[4];
				double sigma = fwhm / 2.35;				
				for(int i=0; i < x.length; i++)
					f[i] = y0 + m*(x[i]-x0) 
							+ A * FastMath.exp(-FastMath.pow2((x[i] - x0) / sigma) /2);
				return f;
			}case FuncFitter.FUNC_SUPERGAUSSIAN_LINEAR:{
				double A = p[0], x0 = p[1], fwhm = p[2], y0 = p[3], m = p[4], pwr = p[5], slant = p[6];
				double sigma = fwhm / 2.35;				
				for(int i=0; i < x.length; i++)
					f[i] = y0 + m*(x[i]-x0) + 
							+ (1.0+(slant*(x[i]-x0))) *
									A * FastMath.exp(-FastMath.pow(FastMath.abs((x[i] - x0) / sigma), pwr) /2);
				return f;
			}case FuncFitter.FUNC_SINESHIFT:{
				double l = p[0], phs = p[1]*Math.PI/180, A = p[2], y0 = p[3], dAdx = p[4], dy0dx = p[5];
				for(int i=0; i < x.length; i++)
					f[i] = y0 + dy0dx * x[i] + (A +dAdx*x[i])*FastMath.sin(2*Math.PI*x[i]/l + phs);

				return f;
			}case FuncFitter.FUNC_POL_TRIANGLE:{
				 // Polariser Cal triangle for IMSE.
				 // This is the same as triangle but allowing for intrinsic contrast and some ellipticity.
				double l = p[0], phs = p[1]*Math.PI/180, mu = p[2], chi = p[3]*Math.PI/180;
				double tanSqchi =  	FastMath.pow2(FastMath.tan(chi));
				for(int i=0; i < x.length; i++){
					//the 'ideal' polarisation angle (0 - 360)
					double idealPolAng = (x[i] / l * 2 * Math.PI + phs) / 4;
					
					//instrument response ratio
					//II=-mu * (tan(2*polAng)**2 + tan(chi)**2 / sin(2*(polAng-45*pi/180))**2);
					double II4 = - mu*mu * (FastMath.pow2(FastMath.tan(2*idealPolAng))
										+ tanSqchi
												/ FastMath.pow2(FastMath.sin(2*(idealPolAng + 45*Math.PI/180)))
							);
					//can't find any way to trig avoid here as the start/end are actually angles
										
					double measPolAng = FastMath.atan(FastMath.sqrt(-II4)) / 2;
					
					//System.out.println(idealPolAng + "\t" + II4 + "\t" + measPolAng);
					f[i] = measPolAng * 180 / Math.PI;
				}
				return f;
			}default:
				throw new IllegalArgumentException("Unknown function");
		}
	}
	
	@Override
	public IDomain getDomain() { return new RectangularDomain(toSelectedParams(minP), toSelectedParams(maxP)); }
	
}

