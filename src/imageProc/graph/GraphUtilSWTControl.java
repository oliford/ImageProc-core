package imageProc.graph;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.jfree.experimental.chart.swt.ChartComposite;

import binaryMatrixFile.AsciiMatrixFile;
import binaryMatrixFile.BinaryMatrixFile;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.graph.shapeFit.FuncFitterSWTControl;
import net.jafama.FastMath;
import otherSupport.ScientificNumberFormat;

/** The Graph Util GUI component */
public class GraphUtilSWTControl implements ImagePipeController, ImagePipeControllerROISettable, SWTControllerInfoDraw {
	
	private ScientificNumberFormat fmt = new ScientificNumberFormat();
	
	private Group swtGroup;
	
	private SashForm swtSashForm;	
	
	private Combo typeSelectV;
	private Button roiAvgCheckbox;
	private Button subtractX0Checkbox;
	private Button subtractY0Checkbox;
	private Button log10YCheckbox;
	private Button ignoreNanCheckbox;
	private Button includeZeroCheckbox;
	private Button rangeHoldCheckbox;
	private Button includeSigmaCheckbox;
	
	private Combo timebaseMetaSeries;
	private Button refershMetaNamesButton;
	private Button saveTracesAsciiButton;
	private Button copyTraceToMetadataButton;
	
	protected CTabFolder swtTabFoler;	
	private CTabItem swtGeneralTab;
	private CTabItem swtFitterTab;
	private CTabItem swtMetadataTab;
	
	private Composite generalComp;
	private FuncFitterSWTControl fitterControl;
	private MetadataPlotSWTControl metadataControl;
		
	protected GraphUtilProcessor proc;
	private Label basicInfo;
		
	private JFreeChartGraph graph;
	
	public GraphUtilSWTControl(GraphUtilProcessor proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Graph Util");
		swtGroup.setLayout(new FillLayout());	
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		swtGroup.addListener(SWT.MouseDoubleClick, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		
		swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
        swtSashForm.setLayout(new FillLayout());
        
        
        swtTabFoler = new CTabFolder(swtSashForm, SWT.BORDER | SWT.TOP);		
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1)); //this doesn't work
		
        
        
        buildGeneralTab();
		
        swtGeneralTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtGeneralTab.setControl(generalComp);
        swtGeneralTab.setText("General");  
        swtGeneralTab.setToolTipText("General graph options and information.");
		
		
		
		fitterControl = new FuncFitterSWTControl(swtTabFoler, SWT.BORDER, proc);
		swtFitterTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtFitterTab.setControl(fitterControl.getSWTGroup());
		swtFitterTab.setText("Fitting");  
		swtFitterTab.setToolTipText("Fitting of basic profiles and wavefunctions to the graph.");
		
		metadataControl = new MetadataPlotSWTControl(swtTabFoler, SWT.BORDER, proc);
		swtMetadataTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtMetadataTab.setControl(metadataControl.getSWTGroup());
		swtMetadataTab.setText("Metadata");  
		swtMetadataTab.setToolTipText("Add metadata to the plot.");
		
		
		addSWTExtensions();
		
		swtTabFoler.setSelection(swtGeneralTab);
				
		graph = new JFreeChartGraph(swtSashForm, SWT.NONE);
		ChartComposite chartComp = graph.getComposite();
		chartComp.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) { graphKeyReleasedEvent(e); }
			
			@Override
			public void keyPressed(KeyEvent e) { graphKeyPressedEvent(e); }
		});
		
		generalControllerUpdate();
		
		
		swtSashForm.pack();
		graph.getComposite().pack();
		//swtTopComp.pack();
		swtGroup.pack();
		
	}
	
	protected void graphKeyPressedEvent(KeyEvent e) {
		if(e.keyCode == 'g' || e.keyCode == 'G') {
			imageWindowGotoPos();
		}		
	}

	/** Make the connected image window go to the select x/y/time */
	private void imageWindowGotoPos() {
		ImgSource src = proc.getConnectedSource();
		ImagePanel imgWin = null;
		for(ImgSink s : src.getConnectedSinks()) {
			if(s instanceof ImagePanel){
				imgWin = (ImagePanel)s;
				break;
			}
		}
		if(imgWin == null) {
			Logger.getLogger(this.getClass().getName()).warning("No ImageWindow found on source... where am I?");
			return;
		}
		
		double graphPos[] = graph.getLastMousePos();
		int imagePos[] = imgWin.getImagePos();
		
		int scanDir = proc.getScanDir();		
		if(scanDir == GraphUtilProcessor.SCAN_DIR_T) {
			imgWin.setSelectedSourceIndex((int)graphPos[0]);
			
		}else if(scanDir == GraphUtilProcessor.SCAN_DIR_X) {
			
			imgWin.setImagePos((int)graphPos[0], imagePos[1], false);
			
			
		}else if(scanDir == GraphUtilProcessor.SCAN_DIR_Y) {
			imgWin.setImagePos(imagePos[0], (int)graphPos[0], false);
			
		}
		
	}

	protected void graphKeyReleasedEvent(KeyEvent e) {
		
	}

	private void buildGeneralTab() {

        generalComp = new Composite(swtTabFoler, SWT.NONE);
        generalComp.setLayout(new GridLayout(5, false));
        
		Label lTV = new Label(generalComp, SWT.NONE); lTV.setText("Scan: ");		
		typeSelectV = new Combo(generalComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		typeSelectV.setItems(GraphUtilProcessor.scanDirNames);
		typeSelectV.select(GraphUtilProcessor.SCAN_DIR_X);
		typeSelectV.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		typeSelectV.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		typeSelectV.setToolTipText("Display graph of scan along X, Y or as time series T");

		Label lTT2 = new Label(generalComp, SWT.NONE); lTT2.setText(" vs:");
		timebaseMetaSeries = new Combo(generalComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		timebaseMetaSeries.setItems(new String[]{ "- None -" });
		timebaseMetaSeries.select(0);
		timebaseMetaSeries.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		timebaseMetaSeries.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { timebaseEvent(event); }});
		timebaseMetaSeries.setToolTipText("Use a given meta-data as x-axis of graph.");

		
		refershMetaNamesButton = new Button(generalComp, SWT.PUSH);
		refershMetaNamesButton.setText("Refresh");
		refershMetaNamesButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refreshMetadataButtonEvent(event); }});
		refershMetaNamesButton.setLayoutData(new GridData(SWT.END, SWT.BEGINNING, false, false, 1, 1));
		refershMetaNamesButton.setToolTipText("Refresh the list of metadata usable as graph x-axis base.");
		
		Group settingsGroupLHS = new Group(generalComp, SWT.BORDER);
		settingsGroupLHS.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		settingsGroupLHS.setLayout(new GridLayout(2, false));
	    
		basicInfo = new Label(settingsGroupLHS, SWT.NONE);
		basicInfo.setText("Init\n.\n.\n.\n.\n.\n.");
		basicInfo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		basicInfo.setToolTipText("Info: Position of cursor, ROI  etc");
		
		Group settingsGroupRHS = new Group(generalComp, SWT.BORDER);
		settingsGroupRHS.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		settingsGroupRHS.setLayout(new GridLayout(2, false));
	       
		copyTraceToMetadataButton = new Button(settingsGroupRHS, SWT.PUSH);
		copyTraceToMetadataButton.setText("Freeze as metadata");
		copyTraceToMetadataButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { freezeTracesButtonEvent(event); }});
		copyTraceToMetadataButton.setToolTipText("Copy current trace to metadata entry and plot it");
		
		saveTracesAsciiButton = new Button(settingsGroupRHS, SWT.PUSH);
		saveTracesAsciiButton.setText("Save Ascii");
		saveTracesAsciiButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveTracesButtonEvent(event); }});
		saveTracesAsciiButton.setToolTipText("Save current traces to TEMP/graphData in ASCII.");
		
		Label lSP = new Label(settingsGroupRHS, SWT.NONE); lSP.setText("");
		
		roiAvgCheckbox = new Button(settingsGroupRHS, SWT.CHECK);
		roiAvgCheckbox.setText("ROI Avg");
		roiAvgCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		roiAvgCheckbox.setToolTipText("Average image over given ROI.");
		
		includeZeroCheckbox = new Button(settingsGroupRHS, SWT.CHECK);
		includeZeroCheckbox.setText("Inc. y=0");
		includeZeroCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { graphSettingsChangeEvent(event); }});
		includeZeroCheckbox.setToolTipText("Scale graph to include y=0 line.");
		
		rangeHoldCheckbox = new Button(settingsGroupRHS, SWT.CHECK);
		rangeHoldCheckbox.setText("Hold Range");
		rangeHoldCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { graphSettingsChangeEvent(event); }});
		rangeHoldCheckbox.setToolTipText("Do not automatically decrease graph scale, only increase.");
		
		subtractX0Checkbox = new Button(settingsGroupRHS, SWT.CHECK);
		subtractX0Checkbox.setText("Subtact X0");
		subtractX0Checkbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		subtractX0Checkbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		subtractX0Checkbox.setToolTipText("Display graph relative to X0.");
		
		subtractY0Checkbox = new Button(settingsGroupRHS, SWT.CHECK);
		subtractY0Checkbox.setText("Subtact Y0");
		subtractY0Checkbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		subtractY0Checkbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		subtractY0Checkbox.setToolTipText("Display graph relative to X0.");
		
		ignoreNanCheckbox = new Button(settingsGroupRHS, SWT.CHECK);
		ignoreNanCheckbox.setText("Ignore NaNs");
		ignoreNanCheckbox.setSelection(true);
		ignoreNanCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		ignoreNanCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		ignoreNanCheckbox.setToolTipText("When ROI averaging, ignore NaNs");
		
		includeSigmaCheckbox = new Button(settingsGroupRHS, SWT.CHECK);
		includeSigmaCheckbox.setText("Calc uncertainty");
		includeSigmaCheckbox.setSelection(true);
		includeSigmaCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		includeSigmaCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		includeSigmaCheckbox.setToolTipText("Take field 1 of image as pixel uncertainty (sigma) and display +/- 2sigma on graph. "
											+ "Calculate unceratinty of ROI averaging assuming pixels and/or time series are uncorrelated");
		
		log10YCheckbox = new Button(settingsGroupRHS, SWT.CHECK);
		log10YCheckbox.setText("log10(Y)");
		log10YCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { typeChangeEvent(event); }});
		log10YCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		log10YCheckbox.setToolTipText("Show range axis (y) on logarithmic scale.");
		log10YCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { graphSettingsChangeEvent(event); }});
				
	}

	protected void refreshMetadataButtonEvent(Event event) {
		String current = timebaseMetaSeries.getText();
		timebaseMetaSeries.setItems(new String[]{ "- None -" });
		timebaseMetaSeries.select(0);
		fillNameSet(timebaseMetaSeries,  proc.getMetaPlotter().getAllMetadataNames(), current);
	}

	protected void addSWTExtensions(){
		
	}
	
	

	private void timebaseEvent(Event event){
		String seriesName =  (timebaseMetaSeries.getSelectionIndex() == 0) ? null : timebaseMetaSeries.getText();
		
		proc.setTimebase(seriesName);
	}
	
	public static void fillNameSet(Combo metaDataNames, List<String> nameList, String sel){
		if(nameList != null){
			//sort normally, except to put all the first level entries at the top
			Collections.sort(nameList, new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {					
					boolean o1IsFirstLevel = o1.matches("/*[^/]*"); // Any number of '/' at start, then no more '/' 
					boolean o2IsFirstLevel = o2.matches("/*[^/]*");
					if(o1IsFirstLevel && !o2IsFirstLevel) 
						return -1;
					if(!o1IsFirstLevel && o2IsFirstLevel)
						return 1;
					return o1.compareTo(o2);
				}
				
			});
			for(String name : nameList){
				metaDataNames.add(name);
				if(name.equals(sel))
					metaDataNames.select(metaDataNames.getItemCount()-1);
			}
		}		
	}
		
	private void typeChangeEvent(Event event){
		
		int selScanV = typeSelectV.getSelectionIndex();
			
		int scanDir = proc.getScanDir();
		boolean isAvg = proc.isScanROIAvg();		
		boolean subX0 = proc.getSubtractX0();
		boolean subY0 = proc.getSubtractY0();
		boolean ignoreNaNInAvg = !proc.getIncludeNaNInAvg();
		boolean calcSigma = proc.getCalcSigma();
		if(selScanV != scanDir || roiAvgCheckbox.getSelection() != isAvg 
				|| subtractX0Checkbox.getSelection() != subX0
				|| subtractY0Checkbox.getSelection() != subY0
				|| ignoreNanCheckbox.getSelection() != ignoreNaNInAvg
				|| includeSigmaCheckbox.getSelection() != calcSigma){
			proc.setScanType(selScanV, roiAvgCheckbox.getSelection(),
					!ignoreNanCheckbox.getSelection(),
					subtractX0Checkbox.getSelection(), 
					subtractY0Checkbox.getSelection(),
					includeSigmaCheckbox.getSelection());
		}
		
	}
	
	private void graphSettingsChangeEvent(Event event){
		graph.setLog10Y(log10YCheckbox.getSelection());
		graph.setAutoRangeIncludesZero(includeZeroCheckbox.getSelection());
		graph.setRangeHold(rangeHoldCheckbox.getSelection()); 
		
	}
		
	private void saveTracesButtonEvent(Event event) {
		proc.saveTraces();
	}
	
	private void freezeTracesButtonEvent(Event event) {
		proc.freezeTraces();
	}

	@Override
	public Composite getInterfacingObject() {
		return swtGroup;
	}	
	
	public void generalControllerUpdate() {	
		if(swtGroup.isDisposed()) return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable(){	
			@Override
			public void run() { doUpdate();  }
		});
	}
	
	private String specialCaseInfo(){
		if(proc.getConnectedSource() == null)
			return "No source!";
		
		Img img = proc.getSelectedImageFromConnectedSource();
		if(img == null)
			return "No img";
		
		String infoText = "";
		
		// AUG: Cell and beam coordinates from ReducedProcessor output
		try{
			double ccdY[] = (double[])proc.getConnectedSource().getSeriesMetaData("Reduced/ccdY");
			if(ccdY != null && ccdY.length == img.getHeight()){
				double viewPos[] = (double[])proc.getConnectedSource().getSeriesMetaData("Reduced/viewPos");
				
				//Reduced data. Isn't R/Z space, but we can calc it for a given pixel
				boolean isBox2 = viewPos[1] > 0; //+ve y means we're probably the permIMSE system
				
				int x = proc.getPosX();
				int y = proc.getPosY();
				
				DecimalFormat rzFmt = new DecimalFormat("#.###");
				
				String posStr = ""; 
				for(int iB=0; iB < 8; iB++){ //for each beam
					if((isBox2 && iB < 4) || (!isBox2 && iB >= 4))
						continue;
					
					double isectPos[][][] =  (double[][][])proc.getConnectedSource().getSeriesMetaData("Reduced/intersectionPos-Q"+(iB+1));
					if(isectPos != null){
						if(isectPos.length != img.getHeight() || isectPos[0].length != img.getWidth()){
							System.err.println("metadata 'Reduced/intersectionPos-Q"+(iB+1)+ "' mismatches image size");
						}else{
							double R = FastMath.sqrt(isectPos[y][x][0]*isectPos[y][x][0] + isectPos[y][x][1]*isectPos[y][x][1]);
							double Z = isectPos[y][x][2];
			
							posStr += "Q"+(iB+1) + ": R=" + rzFmt.format(R) +", Z=" + rzFmt.format(Z)
									+ (((iB % 2) == 1) ? "\n" : ",   ");
						}
					}
				}
				infoText += posStr + "\n";
			}
		}catch(RuntimeException err){ infoText += err.getMessage() + "\n"; }
		
		//R,Z coordinates from TransformProcessor
		try{
			double imageR[] = (double[])proc.getConnectedSource().getSeriesMetaData("/R");
			if(imageR != null){
				int x = proc.getPosX();
				if(x >= 0 && x < imageR.length)
					infoText += "trans posR = " + fmt.format(imageR[x]) + "\n";
			}
		}catch(RuntimeException err){ infoText += err.getMessage() + "\n"; }
		
		try{
			double imageZ[] = (double[])proc.getConnectedSource().getSeriesMetaData("/Z");
			if(imageZ != null){
				int y = proc.getPosY();
				if(y >= 0 && y < imageZ.length)
					infoText += "trans posZ = " + fmt.format(imageZ[y]) + "\n";
			}
		}catch(RuntimeException err){ infoText += err.getMessage() + "\n"; }
		
		//Wavelength at cursor for spectrometers
		try{
			Object wavelength = proc.getConnectedSource().getSeriesMetaData("/wavelength");
			if(wavelength == null)
				wavelength = proc.getConnectedSource().getSeriesMetaData("/SpecCal/wavelength");
			if(wavelength != null && wavelength.getClass().isArray()){
				if(Array.get(wavelength, 0).getClass().isArray()){ //2d, assume [y][x]
					int x = proc.getPosX();
					int y = proc.getPosY();
					Object row = Array.get(wavelength, y);
					Object val = Array.get(row, x);
					infoText += "wavelength(x,y) = " + fmt.format(val) + "\n";
					
				}else{
					int nL = Array.getLength(wavelength);				
					if(nL == img.getWidth()){
						int x = proc.getPosX();
						if(x >= 0 && x < nL)
							infoText += "wavelength(x) = " + fmt.format(Array.get(wavelength, x)) + "\n";	
					}
					if(nL == img.getHeight()){
						int y = proc.getPosY();
						if(y >= 0 && y < nL)
							infoText += "wavelength(y) = " + fmt.format(Array.get(wavelength, y)) + "\n";				
					}
				}
				
			}
		}catch(RuntimeException err){ infoText += err.getMessage() + "\n"; }
		
		//W7-X: nanoTime
		try{
			String nanoTimeName = proc.getConnectedSource().getCompleteSeriesMetaDataMap().findEndsWith("/nanoTime", true);
			Object o = proc.getConnectedSource().getCompleteSeriesMetaDataMap().get(nanoTimeName);
		
			int iF = proc.getSelectedSourceIndex();
			if(o != null && o.getClass().isArray() && Array.getLength(o) > iF){
				long nanoTime = (long)Array.get(o, iF);
				infoText += "nanoTime = " + nanoTime + "\n";
			}
		}catch(RuntimeException err){ infoText += err.getMessage() + "\n"; }
		
		//W7-X: LightPaths entries from spectrometers (i.e. which LOS is it)
		// ???
		
		try{
			String[] lightPaths = (String[]) proc.getConnectedSource().getSeriesMetaData("/SpecCal/linesOfSight/lightPaths");
			if(lightPaths != null){
				int y = proc.getPosY();
				infoText += "lightPath = " + lightPaths[y].replaceAll(" = ", "\n\t\t= ")
												.replaceAll("\\[[^\\]]*\\]", "")
												+ "\n";
			}
		}catch(RuntimeException err){ infoText += err.getMessage() + "\n"; }
		
		
		
		return infoText;
	}
	
	private void doUpdate(){
		if(swtGroup.isDisposed()) return;
		
		String infoText = "Pos: (" + fmt.format(proc.getPosX()) + ", " + fmt.format(proc.getPosY()) + ") = " + fmt.format(proc.getValue()) + "\n"
				+ "ROI: (" + fmt.format(proc.getROIX0()) + ", " + fmt.format(proc.getROIY0()) + ") "+proc.getROIWidth() + " x " + proc.getROIHeight() + "\n"
				+ "Mean: " + fmt.format(proc.getMean()) + "\n"
				+ "Sum: " + fmt.format(proc.getSum()) + "\n" 
				+ specialCaseInfo();
		
		int minLinesInInfoText = 7;		
		//make sure it's n lines long
		int nLinesInInfoText = 0;		
		for(int i=0; i < infoText.length(); i++){
			if(infoText.charAt(i) == '\n')
				nLinesInInfoText++;
		}
		for(int i=nLinesInInfoText; i < minLinesInInfoText; i++){
			infoText += "\n";
		}
		
		basicInfo.setText(infoText);
		
		graph.setData(proc.getSeriesList());
		
		int scanDir = proc.getScanDir();
		boolean isAvg = proc.isScanROIAvg();
		if(typeSelectV.getSelectionIndex() != scanDir || roiAvgCheckbox.getSelection() != isAvg){  
			typeSelectV.select(proc.getScanDir()); //this actually calls this back, so only do it when there's a change
			roiAvgCheckbox.setSelection(proc.isScanROIAvg());
		}
		
		fitterControl.update();
		metadataControl.update();
		updateExtensions();
	}
	
	protected void updateExtensions(){ }

	@Override
	public void movingPos(int x, int y) { proc.setPoint(x, y); }

	@Override
	public void fixedPos(int x, int y)  { proc.setPoint(x, y); }

	@Override
	public void setRect(int x0, int y0, int width, int height) { proc.setRect(x0, y0, width, height); }

	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth, int imageHeight, boolean asSource) {
		if(swtGroup.isDisposed())
			return;
		gc.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		if(proc.isScanROIAvg()){//typeSelectV.getSelectionIndex() >= GraphUtilProcessor.SCAN_DIR_X){
			gc.drawRectangle(
					(int)(proc.getROIX0() * scale[0]),
					(int)(proc.getROIY0() * scale[1]),
					(int)(proc.getROIWidth() * scale[0]),
					(int)(proc.getROIHeight() * scale[1]));
		}
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc;	}

	

}
