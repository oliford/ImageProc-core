package imageProc.graph;

import imageProc.core.Img;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.LinearInterpolation1D;

/** Plotting of metadata (and metadata support for timebases */
public class MetaDataPlotter {
	
	private ArrayList<String> metadataToPlotList = new ArrayList<String>();
	
	private GraphUtilProcessor proc;
	
	public MetaDataPlotter(GraphUtilProcessor proc) {
		this.proc = proc;
			
		String defaultSelectedList = SettingsManager.defaultGlobal().getProperty("imageProc.graph.metadata.defaultList","");
		for(String s : defaultSelectedList.split(",")){
			if(s.length() > 0 && !metadataToPlotList.contains(s))
				metadataToPlotList.add(s);
		}
	}
	
	public void addSeries(LinkedList<Series> newSeriesList) {
		
		String timebaseMetaSeries = proc.getTimebaseMetaName();
		
		for(String metaName : metadataToPlotList){
			
			double ret[][] = scanFromMetaData(metaName);
			if(proc.isAbortSignalled()){ return; }
			Series metaSeries = new Series();
			metaSeries.name("Metadata " + metaName);
			metaSeries.x = ret[0];
			metaSeries.data = ret[1];
			
			if(timebaseMetaSeries != null){
				ret = scanFromMetaData(timebaseMetaSeries);
				if(proc.isAbortSignalled()){ return; }
				if(ret != null && ret.length > 1 && ret[0].length > 1){
					Interpolation1D interp = new Interpolation1D(ret[0], ret[1], InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT_END, 0);
					metaSeries.x = interp.eval(metaSeries.x);
				}
			}
			
			newSeriesList.add(metaSeries);
		}	
	}
	
	public double[][] scanFromMetaData(String metaName) {
		
		Object seriesMetaData = proc.getSeriesMetaData(metaName);
		try{
			Img img = proc.getSelectedImageFromConnectedSource();
			int nX = img.getWidth();
			int nY = img.getHeight();
			int nT = proc.getConnectedSource().getNumImages();
			int posX = proc.getPosX(), posY = proc.getPosY(), posT = proc.getSelectedSourceIndex();
			
			seriesMetaData = Mat.fixWeirdContainers(seriesMetaData, true);
			
			if(seriesMetaData instanceof double[][][]){
				double arr[][][] = ((double[][][]) seriesMetaData);
				
				if(arr.length == nT && arr[0].length == nY && arr[0][0].length == nX){
					
					
					if(proc.getScanDir() == GraphUtilProcessor.SCAN_DIR_X){
						double ret[][] = new double[2][nX];
						for(int iX=0; iX < nX; iX++){
							if(proc.isAbortSignalled())
								return null;
							
							ret[0][iX] = iX;
							ret[1][iX] = arr[posT][posY][iX];
						}
						return ret;
					}else if(proc.getScanDir() == GraphUtilProcessor.SCAN_DIR_Y){						
						double ret[][] = new double[2][nY];
						for(int iY=0; iY < nY; iY++){
							if(proc.isAbortSignalled())
								return null;
							
							ret[0][iY] = iY;
							ret[1][iY] = arr[posT][iY][posX];
						}
						return ret;
					}else{
						double ret[][] = new double[2][nT];
						for(int iT=0; iT < nT; iT++){
							if(proc.isAbortSignalled())
								return null;
							
							ret[0][iT] = iT;
							ret[1][iT] = arr[iT][posY][posX];
						}
						return ret;
					}
				}
				
				System.err.println("Meta data '"+metaName+"' is 3d double array and different shape to images.");
				return null;
			}else if(seriesMetaData instanceof double[][] 
					&& ((double[][])seriesMetaData).length == nY
					&& ((double[][])seriesMetaData)[0].length == nX){
				
				//2D of image shape/size, so pick out the row/col
				double arr[][] = ((double[][]) seriesMetaData);
				
				if(proc.getScanDir() == GraphUtilProcessor.SCAN_DIR_X){
					double ret[][] = new double[2][nX];
					for(int iX=0; iX < nX; iX++){
						if(proc.isAbortSignalled())
							return null;
						
						ret[0][iX] = iX;
						ret[1][iX] = arr[posY][iX];
					}
					return ret;
				}else if(proc.getScanDir() == GraphUtilProcessor.SCAN_DIR_Y){						
					double ret[][] = new double[2][nY];
					for(int iY=0; iY < nY; iY++){
						if(proc.isAbortSignalled())
							return null;
						
						ret[0][iY] = iY;
						ret[1][iY] = arr[iY][posX];
					}
					return ret;
					
				}else{
					System.err.println("Meta data '"+metaName+"' is 2d double array with same shape as images and you're asking for it versus time??");
					return null;
				}
				
				
				
			}else if(seriesMetaData.getClass().isArray()){
				int n = Array.getLength(seriesMetaData);
				
				if(seriesMetaData instanceof double[]){ //special fast case - no conversion
					double ret[][] = new double[][]{ new double[n], (double[])seriesMetaData };
					for(int i=0; i < n; i++){
						if(proc.isAbortSignalled())
							return null;
						
						ret[0][i] = i;
					}
					return ret;
				}else{
					double ret[][] = new double[2][n];
					for(int i=0; i < n; i++){
						if(proc.isAbortSignalled())
							return null;
						
						fillMetaDataEntry(ret, i, Array.get(seriesMetaData, i));
					}
					return ret;
				}
			}else if(seriesMetaData instanceof Collection<?>){
				Collection<?> oColl = (Collection<?>)seriesMetaData;
				int n = oColl.size();
				double ret[][] = new double[2][n];
				int i=0;
				for(Object o : oColl){
					if(proc.isAbortSignalled())
						return null;
					
					fillMetaDataEntry(ret, i++, o);
				}
				return ret;
			}else{
				System.err.println("Meta data '"+metaName+"' not an array or list.");
				return new double[2][0];
			}
			
		}catch(RuntimeException err){
			System.err.println("Probably conversion error for meta data '"+metaName+"': " + err.getMessage());
			return new double[2][0];
		}
	}


	public void setMetadataList(List<String> metaNames){
		metadataToPlotList.clear();
		for(String metaDataName : metaNames){
			
			if(metaDataName != null && metaDataName.endsWith(" (time series)"))
				metaDataName = metaDataName.substring(0, metaDataName.length()-14);
				
			metadataToPlotList.add(metaDataName);
			
		}
		proc.calc();
	}	
	
	private void fillMetaDataEntry(double d[][], int i, Object o){
		if(o == null) {
			d[0][i] = i;
			d[1][i] = Double.NaN;
		}else if(o instanceof Number){
			d[0][i] = i;
			d[1][i] = ((Number)o).doubleValue();
			
		}else if(o instanceof List<?>){
			List<?> oList = (List<?>)o;
			if(oList.size() > 1){
				d[0][i] = ((Number)oList.get(0)).doubleValue();
				d[1][i] = ((Number)oList.get(1)).doubleValue();
			}else{
				d[0][i] = i;
				d[1][i] = ((Number)oList.get(0)).doubleValue();
			}
		}else if(o.getClass().isArray()){
			
			if(Array.getLength(o) > 1){				
				d[0][i] = Array.getDouble(o, 0);
				d[1][i] = Array.getDouble(o, 1);
			}else{
				d[0][i] = i;
				d[1][i] = Array.getDouble(o, 0);				
			}
		}
				
	}
	
	public List<String> getAllMetadataNames(){		
		MetaDataMap map = proc.getCompleteSeriesMetaDataMap();
		ArrayList<String> names = new ArrayList<String>();
		for(Entry<String, MetaData> entry : map.entrySet()){
			String name = entry.getKey();
			if(entry.getValue().isTimeSeries)
				name += " (time series)";
			names.add(name);
		}
		
		return names;
		
	}

	public ArrayList<String> getMetadataToPlotList() { return this.metadataToPlotList; }
}
