package imageProc.graph;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SimpleGraph extends Composite {
	
	private double data[][];
	private double xVals[][];
	private double dataFullRange[][];
	private double xValFullRange[][];
	private double dataDispRange[][];
	private double xValDispRange[][];

	public SimpleGraph(Group parent, int style) {
		super(parent, style);
		
		addPaintListener(new PaintListener() { @Override public void paintControl(PaintEvent e) { paint(e); } });
	}
	
	public void setData(double data[]){ setData(new double[][]{ data }); }
	
	public void setData(double data[][]){ 
		this.data = data; 
		calcDataRanges();
	}

	public void setXVals(double xVals[]){ setXVals(new double[][]{ xVals }); }
	
	public void setXVals(double xVals[][]){ 
		this.xVals = xVals;
		calcXRanges();
	}
	
	private void calcXRanges(){
		xValFullRange = new double[xVals.length][];
		for(int i=0; i < xVals.length; i++){
			xValFullRange[i] = Mat.getRange(xVals[i]);
		}
	}
	
	private void calcDataRanges(){		
		dataFullRange = new double[data.length][];
		for(int i=0; i < data.length; i++){
			dataFullRange[i] = Mat.getRange(data[i]);
		}	
	}
	
	public void resetDisplayRanges(){
		xValDispRange = new double[xValDispRange.length][];
		for(int i=0; i < xVals.length; i++){
			xValDispRange[i] = xValFullRange[i].clone();
		}
		
		dataDispRange = new double[data.length][];
		for(int i=0; i < data.length; i++){
			dataFullRange[i] = Mat.getRange(data[i]);
		}	
	}
	
	private void paint(PaintEvent event){
		if(data == null) return;
		if(dataDispRange == null || xValDispRange == null)
			resetDisplayRanges();
		
		for(int iS=0; iS < data.length; iS++){
			Rectangle ca = getClientArea();
			event.gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
			event.gc.fillRectangle(ca);
			
			if(data[iS] == null || data[iS].length == 0)
				continue;
			
			
			//find min/max in range
			
			int yLast = -1;
			
			for(int i=0; i < ca.width; i++){ //for each pixel on image
				/*double 
				
				int iX = (int)((double)i * data[iS].length / ca.width + 0.5);
				
				if(iX >= scan.length)
					iX = scan.length - 1;
				if(iX < 0)iX = 0;
				
				int y = (int)((scan[iX] - min) * ca.height / (max - min) + 0.5);
			
				if(i > 0)
					event.gc.drawLine(i-1, yLast, i, y);
				yLast = y;
				*/
			}
		}
		
	}
	

}
