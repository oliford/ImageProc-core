package imageProc.graph;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.Range;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.experimental.chart.swt.ChartComposite;

import otherSupport.ScientificNumberFormat;

/** trying to add error bars, but it's really slow for some reason */
public class JFreeChartGraphWithErrorBars {
	
	private ChartComposite frame;
	//private XYSeriesCollection dataset;
	private XYIntervalSeriesCollection dataset;
	
	/** Stops the range shrinking */
	private boolean rangeHold = false; 
	private double rangeHoldMin, rangeHoldMax; 
	private NumberAxis xAxis;
	private NumberAxis valueAxis;
	private XYPlot plot;
	
	private XYErrorRenderer pointRenderer;
	private XYLineAndShapeRenderer lineRenderer;
	
    public JFreeChartGraphWithErrorBars(Composite parent, int style){
    	    
    	XYIntervalSeries series = new XYIntervalSeries("Series 1");
        
        double value = 100.0;
        for (int i = 0; i < 200; i++) {
            series.add(i+100, i+100, i+100, value, value*0.9, value*1.1);    
            value = value * (1 + (Math.random() - 0.495) / 10.0);
        }
        
        dataset = new XYIntervalSeriesCollection();
        dataset.addSeries(series);

        initB(parent, style, dataset);
    }
     
    public JFreeChartGraphWithErrorBars(Composite parent, int style, XYDataset dataset){
    	init(parent, style, dataset);
    }
    
    private void init(Composite parent, int style, XYDataset dataset){

        StandardChartTheme currentTheme = new StandardChartTheme("JFree");
        
        ScientificNumberFormat format = new ScientificNumberFormat();
        //format.
        
        xAxis = new NumberAxis(null);
        xAxis.setLowerMargin(0.00);  // reduce the default margins
        xAxis.setUpperMargin(0.00);
        xAxis.setAutoRangeIncludesZero(false);  // override default
        xAxis.setNumberFormatOverride(format);
        
        valueAxis = new NumberAxis(null);
        valueAxis.setLowerMargin(0.10);  // reduce the default margins
        valueAxis.setUpperMargin(0.10);
        valueAxis.setAutoRangeIncludesZero(false);  // override default
        valueAxis.setNumberFormatOverride(format);
        
        plot = new XYPlot(dataset, xAxis, valueAxis, null);

        XYToolTipGenerator toolTipGenerator = StandardXYToolTipGenerator.getTimeSeriesInstance();

        //XYURLGenerator urlGenerator = new StandardXYURLGenerator();

        lineRenderer = new XYLineAndShapeRenderer(true, false);
        lineRenderer.setDefaultToolTipGenerator(toolTipGenerator);
        //renderer.setURLGenerator(urlGenerator);
        
    	//XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)((XYPlot)frame.getChart().getPlot()).getRenderer();
		pointRenderer = new XYErrorRenderer();

        
        JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        currentTheme.apply(chart);
        
        chart.setBackgroundPaint(Color.white);
        chart.setBorderVisible(true);
        chart.setBorderPaint(Color.BLACK);
                
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        //plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setAxisOffset(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        //plot.getRangeAxis().setFixedDimension(15.0);
        //XYItemRenderer renderer = plot.getRenderer();
        lineRenderer.setSeriesPaint(0, Color.black);
        
        
       
        frame = new ChartComposite(parent, SWT.NONE, chart, 
        		300,300,
        		10, 10, 1000, 1000, true, true, true, true, true, true);
        frame.setDisplayToolTips(true);
        frame.setHorizontalAxisTrace(true);
        frame.setVerticalAxisTrace(true);
        frame.addListener(SWT.Resize, new Listener() { @Override public void handleEvent(Event event) { resizeEvent(event); } });
        
        chart.setPadding(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        plot.setInsets(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        xAxis.setTickLabelInsets(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        valueAxis.setTickLabelInsets(new RectangleInsets(0.0, 4.0, 0.0, 0.0));
        
        Font f = xAxis.getTickLabelFont().deriveFont(10.0f);
        xAxis.setTickLabelFont(f);
        valueAxis.setTickLabelFont(f);
                
    }
    

    private void initB(Composite parent, int style, XYDataset dataset){
            
        
        StandardChartTheme currentTheme = new StandardChartTheme("JFree");
        
        ScientificNumberFormat format = new ScientificNumberFormat();
        
        xAxis = new NumberAxis(null);
        xAxis.setLowerMargin(0.00);  // reduce the default margins
        xAxis.setUpperMargin(0.00);
        xAxis.setAutoRangeIncludesZero(false);  // override default
        xAxis.setNumberFormatOverride(format);
        
        valueAxis = new NumberAxis(null);
        valueAxis.setLowerMargin(0.10);  // reduce the default margins
        valueAxis.setUpperMargin(0.10);
        valueAxis.setAutoRangeIncludesZero(false);  // override default
        valueAxis.setNumberFormatOverride(format);
        
        plot = new XYPlot(dataset, xAxis, valueAxis, null);

        XYToolTipGenerator toolTipGenerator = StandardXYToolTipGenerator.getTimeSeriesInstance();

        //XYURLGenerator urlGenerator = new StandardXYURLGenerator();


        lineRenderer = new XYLineAndShapeRenderer(true, false);
        lineRenderer.setDefaultToolTipGenerator(toolTipGenerator);
        //renderer.setURLGenerator(urlGenerator);
        
    	//XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)((XYPlot)frame.getChart().getPlot()).getRenderer();
		pointRenderer = new XYErrorRenderer();

        JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        currentTheme.apply(chart);
        
        chart.setBackgroundPaint(Color.white);
        chart.setBorderVisible(true);
        chart.setBorderPaint(Color.BLACK);
        
        
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        
        plot.setAxisOffset(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
       
        lineRenderer.setSeriesPaint(0, Color.black);
        pointRenderer.setSeriesPaint(0, Color.black);
        
        frame = new ChartComposite(parent, SWT.NONE, chart, 
        		300,300,
        		10, 10, 1000, 1000, true, true, true, true, true, true);
        frame.setDisplayToolTips(true);
        frame.setHorizontalAxisTrace(true);
        frame.setVerticalAxisTrace(true);
        frame.addListener(SWT.Resize, new Listener() { @Override public void handleEvent(Event event) { resizeEvent(event); } });
        
        chart.setPadding(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        plot.setInsets(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        xAxis.setTickLabelInsets(new RectangleInsets(0.0, 0.0, 0.0, 0.0));
        valueAxis.setTickLabelInsets(new RectangleInsets(0.0, 4.0, 0.0, 0.0));
        
        Font f = xAxis.getTickLabelFont().deriveFont(10.0f);
        xAxis.setTickLabelFont(f);
        valueAxis.setTickLabelFont(f);
                
    }
    
    public void resizeEvent(Event event){
    	//System.out.println("Graph frame resize: " + event.width + " x " + event.height );
    	frame.layout();
    }
    
    public ChartComposite getComposite() { return frame; }
    
    //public void setData(double x[], double data[]){
    //	setData(x, null, new double[][]{ data });
    //}
    
    public void setData(List<Series> seriesList){    	
    	synchronized (dataset) {
    		dataset.removeAllSeries();
	
	      //some protection for silly data lengths and useful display for single values
	    	if(seriesList == null || seriesList.size() <= 0)
	    		return;
	    	
	    	for(int j=0; j < seriesList.size(); j++){
	    		long t0 = System.currentTimeMillis();
				
	    		Series series = seriesList.get(j);
	    		if(series == null)
	    			continue;

		    	//if a single value, make a series from [0,v] to [1,v]
		    	if(series.data.length == 1){
		    		series.x = new double[]{ 0, 1 };		    		
		    		series.data = new double[]{ series.data[0], series.data[0] };
		    	}		    	
	    		
	        	if(series.data == null)
	    			continue; //skip empty
	        	if(series.x == null)
	        		throw new RuntimeException("No x data in series '" + series.name() + "'");
	        	if(series.data.length != series.x.length){
	        		System.err.println("JFreeChartGraph: Skipping series '" + series.name() + "' with data length " + 
	        							series.data.length + " but x length " + series.x.length);
	        		continue;
	        	}
	        		
	    		
	        	String id = (series.name() == null) ? ("Series " + j) : series.name();
		    	
		    	//XYSeries xySeries = new XYSeries(id);
		    	XYIntervalSeries xySeries = new XYIntervalSeries(id);
		        
		    	double newMin = Double.POSITIVE_INFINITY;
		    	double newMax = Double.NEGATIVE_INFINITY;
		    	Color[] autoColors = new Color[]{ Color.BLACK, Color.BLUE, Color.RED, Color.MAGENTA, Color.GREEN, Color.DARK_GRAY, Color.ORANGE, Color.PINK, Color.WHITE };
		    	int nextColor = 0;
		    	for (int i = 0; i < series.data.length; i++) {
		    		if(series.dataLow != null){
		    			xySeries.add(series.x[i], series.x[i], series.x[i], series.data[i], series.dataLow[i], series.dataHigh[i]);
		    		}else{
		    			xySeries.add(series.x[i], series.x[i], series.x[i], series.data[i], series.data[i], series.data[i]);
		    		}
		        	if((t0 - System.currentTimeMillis()) > 3000){
		        		System.err.println("JFreeChartGraph: Skipping series '" + series.name() + " because it took too long to process (>3s)");
		        		continue;
		        	}
		        	
		        	plot.setRenderer(i, series.points ? pointRenderer : lineRenderer);
		        	
		        	
		        	Color awtColor;
		        	if(series.color != null){
		        		awtColor = new Color(series.color[0], series.color[1], series.color[2]);		        		
		        	}else{
		        		awtColor = autoColors[nextColor];
		        		nextColor++;
		        		if(nextColor >= autoColors.length)
		        			nextColor = 0;
		        	}
		        	
		        	lineRenderer.setSeriesPaint(i, awtColor);
	        		pointRenderer.setSeriesPaint(i, awtColor);
	        		
		            
		        	if(series.data[i] < newMin) newMin = series.data[i];
		        	if(series.data[i] > newMax) newMax = series.data[i];
		        }
		        
		    	dataset.addSeries(xySeries);
		    	
		    	if(rangeHold){
		    		if(newMin < rangeHoldMin) rangeHoldMin = newMin;
		    		if(newMax > rangeHoldMax) rangeHoldMax = newMax;
		    	}
	    	}
	    	
	    	if(rangeHold){
		    		NumberAxis valueAxis = (NumberAxis) ((XYPlot)frame.getChart().getPlot()).getRangeAxis();
		    		valueAxis.setRange(rangeHoldMin, rangeHoldMax);        	
	    	}
    	}
    }
    
    public void setAutoRangeIncludesZero(boolean autoRangeIncludesZero){
    	//NumberAxis valueAxis = (NumberAxis) ((XYPlot)frame.getChart().getPlot()).getRangeAxis();
    	valueAxis.setAutoRangeIncludesZero(autoRangeIncludesZero);
    }
    
    public void setRangeHold(boolean rangeHold){
    	NumberAxis valueAxis = (NumberAxis) ((XYPlot)frame.getChart().getPlot()).getRangeAxis();
    	Range range = valueAxis.getRange();
    	this.rangeHold = rangeHold;
    	if(rangeHold){
    		rangeHoldMin = range.getLowerBound();
    		rangeHoldMax = range.getUpperBound();
    		valueAxis.setAutoRange(false);
    	}else{
    		valueAxis.setAutoRange(true);
    	}
    }
    
    public void setRangeX(int x0, int x1){
    	
    }
    
    public void setDataset(XYDataset dataset) {
		plot.setDataset(dataset);
	}

	public void setColours(Color[] colors) {		
		AbstractXYItemRenderer renderer = (AbstractXYItemRenderer)plot.getRenderer();
		for(int i=0; i < colors.length; i++)
			renderer.setSeriesPaint(i, colors[i]);		
	}
}
