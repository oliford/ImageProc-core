package imageProc.graph;

/** Info for each series to be plotted */
public class Series {
	public Series(){ }
	
	public Series(String name, double[] x, double[] data) {
		this.name = name;
		this.x = x;
		this.data = data;
		this.dataLow = null;
		this.dataHigh = null;
		this.points = false;
		this.color = null;
	}
	
	public Series(String name, double[] x, double[] data, double dataLow[], double dataHigh[]) {
		this.name = name;
		this.x = x;
		this.data = data;
		this.dataLow = dataLow;
		this.dataHigh = dataHigh;
		this.points = true;
	}

	private String name;
	public String name(){ return name; } //because of ExtSignalsPlotter.ExtSignalPlotInfo and serialisation
	public void name(String name){ this.name = name; }
	
	public double x[];
	public double data[];
	public double dataLow[];
	public double dataHigh[];
	
	/** color, r,g,b (0-255), null for auto */
	public int[] color;
	
	public double min = Double.NaN;
	public double max = Double.NaN;
	public double sum = Double.NaN;
	
	/** If true, signal is arbitarily scaled to max of other signals */
	public boolean rescale = false;
	
	public boolean points;
	
}
