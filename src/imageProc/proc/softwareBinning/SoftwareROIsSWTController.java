package imageProc.proc.softwareBinning;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import imageProc.core.swt.EditableTable;
import imageProc.core.swt.EditableTable.EditType;
import imageProc.core.swt.EditableTable.TableModifyListener;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.proc.softwareBinning.SoftwareROIsConfig.SoftwareROI;
import imageProc.sources.capture.andorV2.AndorV2Config;
import imageProc.sources.capture.picam.PicamConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import andor2JNI.AndorV2ROIs.AndorV2ROI;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import picamJNI.PICamROIs.PICamROI;

/** The table directly relfects the config data in the processor's map
* Edits are made into that cfg immediately.
*  
* @author oliford
*/
public class SoftwareROIsSWTController extends ImgProcPipeSWTController implements SWTControllerInfoDraw, ImagePipeControllerROISettable {

	private final static String colNames[] = new String[]{ "-", "Name", "Enable", "X0", "Width", "X bin", "Y0", "Height", "Y bin","Tilt","Dead columns" };			
	
	private EditableTable table;
	
	private Button drawPositionsCheckbox;
	private Button setPositionsCheckbox;
	private Button transposeBinnedCheckbox;
	private Button averageBinsCheckbox;
	
	private Button copyAllButton;
	private Button pasteAllButton;
	
	private Spinner adjustXSpinner;	
	private Spinner adjustYSpinner;	
	
	private Button jogLeftButton;
	private Button jogRightButton;
	private Button jogUpButton;
	private Button jogDownButton;
	
	private SWTSettingsControl settingsCtrl;
	
	protected SoftwareROIsProcessor proc;
	
	public boolean asSink;
				
	public SoftwareROIsSWTController(Composite parent, int style, SoftwareROIsProcessor proc, boolean asSink) {
		this.proc = proc;
		this.asSink = asSink;
		
		
		swtGroup = new Group(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));

		addCommonPipeControls(swtGroup);

		Composite checkboxesGroup = new Composite(swtGroup, SWT.NONE);
		checkboxesGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		checkboxesGroup.setLayout(new GridLayout(1, false));
		
		drawPositionsCheckbox = new Button(checkboxesGroup, SWT.CHECK);
		drawPositionsCheckbox.setText("Draw ROIs - Displays RIOs (white boxes, selected ROIs in magenta)");
		drawPositionsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		setPositionsCheckbox = new Button(checkboxesGroup, SWT.CHECK);
		setPositionsCheckbox.setText("Set ROIs - Select entry and draw box on full-frame image to set");
		setPositionsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		transposeBinnedCheckbox = new Button(checkboxesGroup, SWT.CHECK);
		transposeBinnedCheckbox.setText("Transpose binned - Transpose binned data before stacking in output frame");
		transposeBinnedCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		transposeBinnedCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		averageBinsCheckbox = new Button(checkboxesGroup, SWT.CHECK);
		averageBinsCheckbox.setText("Average bins - Output pixel is average of binned source pixels (otherwise summed)");
		averageBinsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		averageBinsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Group swtJogGroup = new Group(swtGroup, SWT.BORDER);
		swtJogGroup.setLayoutData(new GridData(SWT.END, SWT.BEGINNING, false, false, 1, 1));
		swtJogGroup.setLayout(new GridLayout(3, false));
		
		Label lJA = new Label(swtJogGroup, SWT.NONE);
		lJA.setText("Jog:");
		lJA.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		jogUpButton = new Button(swtJogGroup, SWT.PUSH);
		jogUpButton.setText("^");
		jogUpButton.setToolTipText("Jog all or selected ROIs up 1 pixel");
		jogUpButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogUpButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, 0, -1); } });
		
		Label lJB = new Label(swtJogGroup, SWT.NONE);
		lJB.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		jogLeftButton = new Button(swtJogGroup, SWT.PUSH);
		jogLeftButton.setText("<");
		jogLeftButton.setToolTipText("Jog all or selected ROIs left 1 pixel");
		jogLeftButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogLeftButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, -1, 0); } });

		jogDownButton = new Button(swtJogGroup, SWT.PUSH);
		jogDownButton.setText("v");
		jogDownButton.setToolTipText("Jog all or selected ROIs down 1 pixel");
		jogDownButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogDownButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, 0, +1); } });

		jogRightButton = new Button(swtJogGroup, SWT.PUSH);
		jogRightButton.setText(">");
		jogRightButton.setToolTipText("Jog all or selected ROIs right 1 pixel");
		jogRightButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		jogRightButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { jogButtonEvent(event, +1, 0); } });

		Label lAX = new Label(swtGroup, SWT.NONE); lAX.setText("Adjust X:");
		adjustXSpinner = new Spinner(swtGroup, SWT.NONE);
		adjustXSpinner.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 1, 10);
		adjustXSpinner.setToolTipText("Shift all ROIs relative to config table this many sensor pixels in X");
		adjustXSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		adjustXSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lAY = new Label(swtGroup, SWT.NONE); lAY.setText("Adjust Y:");
		adjustYSpinner = new Spinner(swtGroup, SWT.NONE);
		adjustYSpinner.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 1, 10);
		adjustYSpinner.setToolTipText("Shift all ROIs relative to config table this many sensor pixels in Y");
		adjustYSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		adjustYSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lT = new Label(swtGroup, SWT.NONE); lT.setText("Table:");
		lT.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		
		copyAllButton = new Button(swtGroup, SWT.PUSH);
		copyAllButton.setText("Copy");
		copyAllButton.setToolTipText("Copy all entries in table to clipboard in a format that is recognised by LibreOffice/Excel");
		copyAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		copyAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { copyAllButtonEvent(event); } });

		pasteAllButton = new Button(swtGroup, SWT.PUSH);
		pasteAllButton.setText("Paste");
		pasteAllButton.setToolTipText("Paste all entries from clipboard into the table, e.g. after modification in LibreOffice/Excel");
		pasteAllButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		pasteAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pasteAllButtonEvent(event); } });

		table = new EditableTable(swtGroup, SWT.BORDER | SWT.MULTI);				
		//featuresTable.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, true, 5, 0));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);	
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(table, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]);
		}
		
		configToGUI();
		
		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		table.setTableModifyListener(new TableModifyListener() { @Override 
			public void tableModified(TableItem item, int column, String text) { tableColumnModified(item, column, text); }
		});
		table.setEditTypeAll(EditType.EditBox);
		table.setEditType(0, EditType.ReadOnly);
		table.setEditType(2, EditType.Click);
		table.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ImageProcUtil.redrawImagePanel(proc.getConnectedSource()); } });
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		swtGroup.pack();
	}

	protected void jogButtonEvent(Event event, int dx, int dy) {

		SoftwareROIsConfig config = proc.getConfig();
		TableItem[] selectedItems = table.getSelection();
		if(selectedItems != null && selectedItems.length >=1) {
			for (TableItem item : selectedItems) {
				
				SoftwareROI roi = config.getROI(item.getText(1));
				if(roi != null)
					config.jogROI(roi, dx, dy);
			}
		}else {
			for(SoftwareROI roi : config.getROIListCopy()) {
				config.jogROI(roi, dx, dy);				
			}
		}
		
		configToGUI();
	}

	protected void settingsChangedEvent(Event event) {
		SoftwareROIsConfig config = proc.getConfig();
		config.transposeOutput =  transposeBinnedCheckbox.getSelection();
		config.adjustX = adjustXSpinner.getSelection();
		config.adjustY = adjustYSpinner.getSelection();
		config.averageBins = averageBinsCheckbox.getSelection();
		proc.configChanged();
	}

	private void tableColumnModified(TableItem item, int column, String text) {
		
		if(text != null && text.startsWith("^^^")) {
			text = text.substring(3); 
			for(TableItem item2 : table.getItems())
				tableColumnModified(item2, column, text);
			return;
		}

		SoftwareROIsConfig config = proc.getConfig();
		
		if(item == null || column < 0) { //just a general update
			generalControllerUpdate();
			return;
		}
		
		synchronized (config) {
			if (item.isDisposed())
				return;

			String roiName = item.getText(1);

			SoftwareROI roi = (roiName.length() > 0) ? config.getROI(roiName) : null;
			if(roi == null && column != 1){
				//somethings broken. Queue an update
				generalControllerUpdate();
				return;
			}
			
			switch (column) {
				case 1: //name
	
					if (roi == null && (roiName.length() <= 0 || roiName.equals("<new>"))) { 
						roi = new SoftwareROI();
						roi.name = text;
						config.addROI(roi);
	
					} else if (text.length() <= 0) { // deleting point
						config.remove(roi);
	
					} else { // just changing name
						roi.name = text;
					}
	
					item.setText(column, text);
	
					break;
				case 2: roi.enabled = !Algorithms.mustParseBoolean(text); item.setText(2, roi.enabled ? "Y" : ""); break;					
	
				case 3: roi.x = Algorithms.mustParseInt(text); item.setText(3, Integer.toString(roi.x)); break;
				case 4: roi.width = Algorithms.mustParseInt(text); item.setText(4, Integer.toString(roi.width)); break;
				case 5: roi.x_binning = Algorithms.mustParseInt(text); item.setText(5, Integer.toString(roi.x_binning)); break;
				
				case 6: roi.y = Algorithms.mustParseInt(text); item.setText(6, Integer.toString(roi.y)); break;
				case 7: roi.height = Algorithms.mustParseInt(text); item.setText(7, Integer.toString(roi.height)); break;
				case 8: roi.y_binning = Algorithms.mustParseInt(text); item.setText(8, Integer.toString(roi.y_binning)); break;
				case 9: roi.tilt = Algorithms.mustParseDouble(text); item.setText(9, Double.toString(roi.tilt)); break;
				case 10:
					Gson gson = new Gson();
					int[] cols = gson.fromJson(text, int[].class);
					if(roi.deadColumns == null)
						roi.deadColumns = new ArrayList<>();
					roi.deadColumns.clear();
					if(cols != null){
						for(int c : cols){
							if(c >= roi.x && c < (roi.x + roi.width))
								roi.deadColumns.add(c);
						}
					}
					item.setText(10, roi.deadColumns.toString());
					break;
					
			}

			TableItem lastItem = table.getItem(table.getItemCount() - 1);
			if (!lastItem.getText(1).equals("<new>")) {
				TableItem blankItem = new TableItem(table, SWT.NONE);
				blankItem.setText(0, "o");
				blankItem.setText(1, "<new>");
			}

		}
	}

	
	
	void configToGUI(){
		SoftwareROIsConfig config = proc.getConfig();
		
		transposeBinnedCheckbox.setSelection(config.transposeOutput);
		adjustXSpinner.setSelection(config.adjustX);
		adjustYSpinner.setSelection(config.adjustY);
		averageBinsCheckbox.setSelection(config.averageBins);
		
		ArrayList<SoftwareROI> roiList;
		synchronized (config) {
			roiList = config.getROIListCopy();
		}

		int selected[] = table.getSelectionIndices();

		// use existing table entries, so it doesnt scroll around
		for (int i = 0; i < roiList.size(); i++) {
			SoftwareROI roi = roiList.get(i);

			if (roi.name.length() <= 0)
				continue;

			TableItem item;
			if (i < table.getItemCount())
				item = table.getItem(i);
			else
				item = new TableItem(table, SWT.NONE);

			item.setText(0, "o");
			item.setText(1, roi.name);
			item.setText(2, roi.enabled ? "Y" : "");

			item.setText(3, Integer.toString(roi.x));
			item.setText(4, Integer.toString(roi.width));
			item.setText(5, Integer.toString(roi.x_binning));

			item.setText(6, Integer.toString(roi.y));
			item.setText(7, Integer.toString(roi.height));
			item.setText(8, Integer.toString(roi.y_binning));
			
			item.setText(9, Double.toString(roi.tilt));
			
			item.setText(10, (roi.deadColumns == null) ? "" : roi.deadColumns.toString());

		}

		while (table.getItemCount() > roiList.size()) {
			table.remove(roiList.size());
		}

		// and finally a blank item for creating new point
		TableItem blankItem = new TableItem(table, SWT.NONE);
		blankItem.setText(0, "o");
		blankItem.setText(1, "<new>");


	}
		

	private void copyAllButtonEvent(Event event){
		
		StringBuilder strB = new StringBuilder();
		
		SoftwareROIsConfig config = proc.getConfig();
		
		//Set<String> keySet = config.specEntries.get(0).toMap().keySet();
		String[] keySet = new String[]{ 
				"name", "enabled", "x", "width", "x_binning", "y", "height", "y_binning", "tilt"
			};
		
		for(String fieldName : keySet){
			strB.append("\"" + fieldName + "\"\t");
		}
		strB.append("\n");
		
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.create(); 
		
		for(SoftwareROI roi : config.getROIListCopy()){
			JsonElement jsonElem = gson.toJsonTree(roi);
			JsonObject o = jsonElem.getAsJsonObject();
			
			for(String fieldName : keySet){
				Object o2 = o.get(fieldName);
				strB.append((o2 == null ? "null" : o2.toString()) + "\t");
			}
			strB.append("\n");
		}
		
		System.out.println(strB.toString());
		
		Clipboard cb = new Clipboard(swtGroup.getDisplay());
		TextTransfer tt = TextTransfer.getInstance();
		cb.setContents(new Object[]{ strB.toString() }, new Transfer[]{ tt });
		cb.dispose();
	}


	private void pasteAllButtonEvent(Event event){

		Clipboard cb = new Clipboard(swtGroup.getDisplay());

		try{
			String plainText = (String)cb.getContents(TextTransfer.getInstance());
			System.out.println("Pasted: " + plainText);
			String lines[] = plainText.split("\n");
			String fieldNames[] = lines[0].split("\t");
	
			Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.create(); 
		
			SoftwareROIsConfig config = proc.getConfig();
			config.clear();
			
			for(int i=1; i < lines.length; i++){
				
				String jsonStr = "{";
				String fields[] = lines[i].split("\t");
				if(fields.length != fieldNames.length)
					throw new RuntimeException("Row " +i+" of pasted text had " + fields.length +" columns but row 0 (names) had " + fieldNames.length);
				
				for(int j=0; j < fields.length; j++){
					if(j > 0)
						jsonStr += ","; 
					
					if(fieldNames[j].startsWith("\""))
						jsonStr += fieldNames[j] + ":";
					else 
						jsonStr += "\"" + fieldNames[j] + "\":";
							
					if(fields[j].equals("null")){
						jsonStr += "null";
					}else if(fields[j].startsWith("[") && fields[j].endsWith("]")){ 
						//looks like a JSON array (e.g. ignoreX0Ranges
						jsonStr += fields[j];
					}else{
						if(fields[j].startsWith("\""))
							jsonStr += fields[j];
						else 
							jsonStr += "\"" + fields[j] + "\"";
					}
				}
				jsonStr += "}";
				System.out.println(jsonStr);
	
				SoftwareROI thing = (SoftwareROI)gson.fromJson(jsonStr, SoftwareROI.class);
	
				config.addROI(thing);
				
			}
			
			cb.dispose();
			
			System.out.println("Pasted "+config.getROIListCopy().size()+" entries.");
			
		}catch(RuntimeException err){
			err.printStackTrace();
			System.err.println("Error parsing pasted text:" + err.getMessage());
		}
		
		
		proc.updateAllControllers();
	}
	
	public Control getSWTGroup() { return swtGroup; }

	public void drawOnImage(GC gc, double[] scale, int imageWidth, int imageHeight, boolean asSource) {
		if(!asSink || swtGroup.isDisposed() || !drawPositionsCheckbox.getSelection())
			return;
		
		SoftwareROIsConfig config = proc.getConfig();
		List<SoftwareROI> roiList = config.getROIListCopy();
		
		int i = 0;
		for (SoftwareROI roi : roiList) {

			TableItem[] selectedItems = table.getSelection();
			boolean isSelected = false;
			for (TableItem item : selectedItems) {
				if (item.getText(1).equals(roi.name))
					isSelected = true;
			}
			if (isSelected) {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_MAGENTA));
				gc.setLineWidth(3);
			} else {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
				gc.setLineWidth(1);
			}

			int boxCoords[];
			PhysicalROIGeometry ccdGeom = proc.getCCDGeom();
			if(ccdGeom != null){
				boxCoords = new int[]{
						ccdGeom.sensorToImageX(roi.x + config.adjustX), ccdGeom.sensorToImageY(roi.y + config.adjustY), 
						ccdGeom.sensorToImageX(roi.x + roi.width + config.adjustX), ccdGeom.sensorToImageY(roi.y + roi.height + config.adjustY)						
					};
			}else{
				boxCoords = new int[]{ roi.x + config.adjustX, roi.y + config.adjustY, roi.x + roi.width + config.adjustX, roi.y + roi.height + config.adjustY };	
			}

			if (boxCoords[0] >= 0 && boxCoords[1] >= 0 && boxCoords[2] <= imageWidth && boxCoords[3] <= imageHeight
					&& boxCoords[2] > boxCoords[0] && boxCoords[3] > boxCoords[1]) {

				
				/*gc.drawRectangle(
						(int) (boxCoords[0] * scale[0]), 
						(int) (boxCoords[1] * scale[1]),
						(int) ((boxCoords[2] - boxCoords[0]) * scale[0]),
						(int) ((boxCoords[3] - boxCoords[1]) * scale[1]));
				//*/
				
				int width = (int) ((boxCoords[2] - boxCoords[0]) * scale[0]);
				int height = (int) ((boxCoords[3] - boxCoords[1]) * scale[1]);
				int x1 = (int) (boxCoords[0] * scale[0]);
				int y1 = (int) (boxCoords[1] * scale[1]);
				int x2 = (int) (boxCoords[2] * scale[0]);
				int y2 = (int) (boxCoords[3] * scale[1]);
				int dropX =0, dropY = 0;
				if(config.transposeOutput)
					dropX =  (int)(roi.tilt * height);
				else
					dropY =  (int)(roi.tilt * width * scale[1] / scale[0]);
				
				
				gc.drawPolygon(
						new int[]{ 
							x1, y1,
							x1+dropX, y2,
							x2+dropX, y2 + dropY,
							x2, y1 + dropY,
							x1, y1
						});
						//*/
			}

			i++;
		}

	}

	public void setRect(int x0, int y0, int width, int height) {		
		if (!asSink || swtGroup.isDisposed() || !setPositionsCheckbox.getSelection())
			return;

		SoftwareROIsConfig config = proc.getConfig();
				
		TableItem[] selected = table.getSelection();
		if (selected.length == 1) {
			String roiName = selected[0].getText(1);

			SoftwareROI roi = config.getROI(roiName);
			
			if(roi.x_binning <= 0) roi.x_binning = 1;
			if(roi.y_binning <= 0) roi.y_binning = 1;
			
			PhysicalROIGeometry ccdGeom = proc.getCCDGeom();
			if(ccdGeom != null){
				roi.x = ccdGeom.imageToSensorX(x0);
				width = ccdGeom.imageToSensorX(x0 + width) - roi.x;
				roi.width = (int)(width / roi.x_binning) * roi.x_binning;
				
				roi.y = ccdGeom.imageToSensorY(y0);
				height = ccdGeom.imageToSensorY(y0 + height) - roi.y;
				roi.height = (int)(height / roi.y_binning) * roi.y_binning;
				
			}else{
				roi.x = x0;
				roi.width = (int)(width / roi.x_binning) * roi.x_binning;
				
				roi.y = y0;
				roi.height = (int)(height / roi.y_binning) * roi.y_binning;
					
			}
			roi.x -= config.adjustX;
			roi.y -= config.adjustY;
			
			proc.configChanged();
		}
	}
	
	protected void doUpdate(){
		super.doUpdate();
		
		settingsCtrl.doUpdate();
		
		if(!table.isEditing())
			configToGUI();
		
	}

	@Override
	public void movingPos(int x, int y) {	}

	@Override
	public void fixedPos(int x, int y) {
		if (!asSink || swtGroup.isDisposed() || !setPositionsCheckbox.getSelection())
			return;

		SoftwareROIsConfig config = proc.getConfig();
		PhysicalROIGeometry ccdGeom = proc.getCCDGeom();
		if(ccdGeom != null){
			x = ccdGeom.imageToSensorX(x) - config.adjustX;
		}
			
		//Toggle dead column
		for(SoftwareROI roi : config.getROIListCopy()){
			if(x >= roi.x && x < (roi.x + roi.width)){
				if(roi.deadColumns == null)
					roi.deadColumns = new ArrayList<>();
				
				//toggle
				if(!roi.deadColumns.remove((Object)x)){
					roi.deadColumns.add(x);
				}
				proc.updateAllControllers();
				return;
			}
		}
		System.out.println("Position " + x+ " not in an ROI!");
		
		
	}

	
	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public SoftwareROIsProcessor getPipe() { return proc;	}
	
}
