package imageProc.proc.softwareBinning;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;

public class SoftwareROIsConfig {
	
	/** If true, after binning each ROI is transposed before stacking on the output image */ 
	public boolean transposeOutput;
	
	/** Adjust x,y for all ROIs. e.g. to compensate small movements in optics of spectrometers */
	public int adjustX;
	public int adjustY;
	
	public boolean averageBins = false; //otherwise sum 
	

	public static class SoftwareROI {
		public String name;
		public boolean enabled;
		
    	public int x;
    	public int width;
    	public int x_binning;
    	public int y;
    	public int height;
    	public int y_binning;
    	
    	/** Tilt the ROI box: How much to add to y for every pixel in x 
    	 * (in the sensor coordinates) */
    	public double tilt = 0;		
    	
    	/** List of columns to ignore (x value relative to global x=0)*/
    	public ArrayList<Integer> deadColumns;
    	
    	@Override
    	public String toString() {
    		return "{"+name+","
    				+ (enabled?"Enabled,":"Disabled,")
    				+ x + "," + width + "," + x_binning + ","
    				+ y + "," + height + "," + y_binning + "}";
    	}
    	
    }
    
    private ArrayList<SoftwareROI> rois;
    
    public SoftwareROIsConfig(){
    	rois = new ArrayList<SoftwareROI>();
    }
        
    
    public void addROI(int x, int width, int x_binning, int y, int height, int y_binning){
    	SoftwareROI roi = new SoftwareROI();
    	roi.x = x;
    	roi.width = width;
    	roi.x_binning = x_binning;
    	roi.y = y;
    	roi.height = height;
    	roi.y_binning = y_binning;
    	rois.add(roi);
    }

	public void addROI(SoftwareROI roi) {
		synchronized (rois) {
			rois.add(roi);
    	}
	} 
	
	public SoftwareROI getROI(String name){
		for(SoftwareROI roi : rois)
			if(name.equals(roi.name))
				return roi;
		return null;
	}

	@Override
	public String toString() {
		StringBuffer strB = new StringBuffer();
		strB.append("ROIs[");
		synchronized (rois) {
			for(SoftwareROI roi : rois)
				strB.append(roi);
		}
		strB.append("]");
		return strB.toString();
	}

	public void remove(SoftwareROI roi) {
		rois.remove(roi);
	}
	
	public ArrayList<SoftwareROI> getROIListCopy() {
		synchronized (rois) {
			return (ArrayList<SoftwareROI>)rois.clone();
		}
	}

	public void addArraysToMap(HashMap<String, Object> map, String prefix, boolean enabledOnly) {
		synchronized (rois) {
				
			ArrayList<String> roisNames = new ArrayList<>();
			ArrayList<Integer> roisX = new ArrayList<>();
			ArrayList<Integer> roisWidth = new ArrayList<>();
			ArrayList<Integer> roisXBinning = new ArrayList<>();
			ArrayList<Integer> roisY = new ArrayList<>();
			ArrayList<Integer> roisHeight = new ArrayList<>();
			ArrayList<Integer> roisYBinning = new ArrayList<>();
			ArrayList<Double> roisTilt = new ArrayList<>();
			ArrayList<Integer> roisEnabled = new ArrayList<>();
			ArrayList<Integer[]> roisDeadcols = new ArrayList<>();
			
			int idx = 0;
			for(SoftwareROI roi : rois){
				if(enabledOnly && !roi.enabled)
					continue;
				
				roisNames.add(roi.name);
				roisX.add(roi.x);
				roisWidth.add(roi.width);
				roisXBinning.add(roi.x_binning);
				roisY.add(roi.y);
				roisHeight.add(roi.height);
				roisYBinning.add(roi.y_binning);
				roisTilt.add(roi.tilt);
				roisEnabled.add(roi.enabled ? 1 : 0);
				roisDeadcols.add((roi.deadColumns != null) ? roi.deadColumns.toArray(new Integer[0]) : new Integer[0]);
				
				idx++;
			}
			
			map.put(prefix + "/names", roisNames.toArray(new String[idx]));
			map.put(prefix + "/x", roisX.toArray(new Integer[idx]));
			map.put(prefix + "/width", roisWidth.toArray(new Integer[idx]));
			map.put(prefix + "/x_binning", roisXBinning.toArray(new Integer[idx]));
			map.put(prefix + "/y", roisY.toArray(new Integer[idx]));
			map.put(prefix + "/height", roisHeight.toArray(new Integer[idx]));
			map.put(prefix + "/y_binning", roisYBinning.toArray(new Integer[idx]));			
			map.put(prefix + "/tilt", roisTilt.toArray(new Double[idx]));			
			map.put(prefix + "/enabled", roisEnabled.toArray(new Integer[idx]));
			map.put(prefix + "/deadColumns", roisDeadcols.toArray(new Integer[idx][]));
			
			map.put(prefix + "/adjustX", adjustX);
			map.put(prefix + "/adjustY", adjustY);			
			map.put(prefix + "/transposeOutput", transposeOutput);
		}
	}


	public void clear() {
		synchronized (rois) {
			rois.clear();
		}
	}


	public void jogROI(SoftwareROI roi, int dx, int dy) {
		roi.x += dx;
		roi.y += dy;		
	}
}
