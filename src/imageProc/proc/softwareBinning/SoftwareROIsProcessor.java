package imageProc.proc.softwareBinning;

import java.lang.reflect.Array;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.Algorithms;
import binaryMatrixFile.AsciiMatrixFile;
import descriptors.aug.AUGSignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.softwareBinning.SoftwareROIsConfig.SoftwareROI;
import imageProc.sources.capture.andorV2.AndorV2Config;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

/** Software ROI processor (binning)
 * 
 * Mimics CCD style binning (e.g. for use with CMOS camera images)
 * 
 * @author oliford
 *
 */
public class SoftwareROIsProcessor extends ImgProcPipeMultithread implements ConfigurableByID {

	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
	
	protected SoftwareROIsConfig config = new SoftwareROIsConfig();
	private PhysicalROIGeometry ccdGeom = null;

	private String lastLoadedConfig;
	
	public SoftwareROIsProcessor() {
		this(null, -1);
	}
	
	public SoftwareROIsProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);
		autoCalc = false;
		setNumThreads(1);
		
	}
	
	/** Image allocation */
	@Override
	protected boolean checkOutputSet(int nImagesIn){
		int nImagesOut = nImagesIn;
		if(config == null)
			throw new RuntimeException("config is null. Still in construction??");
		
		try{
			ccdGeom = new PhysicalROIGeometry();
			ccdGeom.setFromCameraMetadata(getCompleteSeriesMetaDataMap().extractSingleArrayElements());
		}catch(Exception err){
			System.err.println("WARNING: Unable to collect CCDGeom information, will assume ROIs are in image space: " + err.getMessage());
			ccdGeom = null;
		}
		
		//find  how many active rows and the most pixels in a single row
		outWidth = 0;
		outHeight = 0;
	    for(SoftwareROI roi : config.getROIListCopy()){
			if(roi.enabled){
				//check it makes sense
				if((roi.width % roi.x_binning) != 0)
					throw new IllegalArgumentException("ROI '"+roi.name+"' has width not divisible by x binning");
				if((roi.height % roi.y_binning) != 0)
					throw new IllegalArgumentException("ROI '"+roi.name+"' has height not divisible by y binning");
				
				int roiBinnedWidth = roi.width / roi.x_binning;
				int roiBinnedHeight = roi.height / roi.y_binning;
				
				if(!config.transposeOutput){ //normal way up
					outHeight += roiBinnedHeight;
					if(roiBinnedWidth > outWidth)
						outWidth = roiBinnedWidth;
					
				}else{ //transpose the binned ROIs
					outHeight += roiBinnedWidth;
					if(roiBinnedHeight > outWidth)
						outWidth = roiBinnedHeight;
				}
			}
		}
	    
	    if(outWidth <= 0 || outHeight <= 0)
	    	throw new IllegalArgumentException("No enabled ROIs with finite size.");
	    
	    HashMap<String,Object> roisMetadata = new HashMap<String, Object>(); 
	    config.addArraysToMap(roisMetadata, "SoftwareROIs/Rois_arrays", true);
	    addNonTimeSeriesMetaDataMap(roisMetadata);
	    
	    int nFields = getConnectedSource().getImage(0).getNFields();
	    
	    //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, nFields, ByteBufferImage.DEPTH_FLOAT, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }          
        return false;        
	}
	
	@Override
	protected int[] sourceIndices(int outIdx){	
			return new int[]{ outIdx };	
	}
	
	@Override
	protected boolean doCalc(Img imageOutG, WriteLock writeLockOut, Img sourceSet[], boolean settingsHadChanged) throws InterruptedException {
				
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		
		//actual averaging sums (because NaNs arn't counted)
		double sumAmp[][] = new double[outHeight][outWidth];
		
		//zero output image
		for(int oY=0; oY < outHeight; oY++) {
			for(int oX=0; oX < outWidth; oX++) {
				sumAmp[oY][oX] = 0;				
				imageOut.setPixelValue(writeLockOut, oX, oY, Double.NaN);						
			}
		}
		
		try{			
			//for each input image in the input set
			Img imageIn = sourceSet[0];
			if(imageIn == null)
				return true;
			
			int imageWidth = imageIn.getWidth();
			int imageHeight = imageIn.getHeight();
			
			//hackery for ILS Green cut304 Global Shutter mode (which is pretty screwed)				
			boolean isColumnDead[] = new boolean[imageWidth];
			
						
			int nextOutputStartRow = 0;
						
			ReadLock readLockIn = imageIn.readLock();
			readLockIn.lockInterruptibly();
			try{
				
				for(SoftwareROI roi : config.getROIListCopy()){
					if(roi.enabled){
						//apply any dead columns
						if(roi.deadColumns != null){
							for(int x : roi.deadColumns){								
								x = (ccdGeom == null) ? x : ccdGeom.sensorToImageX(x);
								isColumnDead[x] = true;
							}
						}
						
						int roiBinnedWidth = roi.width / roi.x_binning;
						int roiBinnedHeight = roi.height / roi.y_binning;
						
						
						for(int iOY=0; iOY < roiBinnedHeight; iOY++){
							for(int iOX=0; iOX < roiBinnedWidth; iOX++){
						
								int nDead = 0;
								
								double pixelVal = 0;
								for(int iBY=0; iBY < roi.y_binning; iBY++){
									for(int iBX=0; iBX < roi.x_binning; iBX++){
										//full chip pixel
										double xSensor = roi.x + (iOX * roi.x_binning) + iBX + config.adjustX;
										double ySensor = roi.y + (iOY * roi.y_binning) + iBY + config.adjustY;
										
										if(config.transposeOutput)
											xSensor += (ySensor * roi.tilt);
										else
											ySensor += (xSensor * roi.tilt);
												
										//pixel in image
										double xImage = (ccdGeom == null) ? xSensor : ccdGeom.sensorToImageX(xSensor);
										double yImage = (ccdGeom == null) ? ySensor : ccdGeom.sensorToImageY(ySensor);
										
										if(xImage < 0 || xImage >= imageWidth
												|| yImage < 0 || yImage >= imageHeight){
											throw new RuntimeException("ROI " + roi.name + " out of image for pixel xImage="+xImage+", yImage="+yImage);
										}
										
										//the is columns dead things doesn't necessarily work with the antialiasing
										if(isColumnDead != null && isColumnDead[(int)xImage]){
											//if(xImage > 1000 && xImage < 1600)
											//	System.out.print(xImage + " ");
											nDead++;
											continue;
										}										
										
										if(xImage % 1.0 == 0.0 && yImage % 1.0 == 0.0) {
											pixelVal += imageIn.getPixelValue(readLockIn, (int)xImage, (int)yImage);
											
										}else {
											//linear interpolate
											int x0 = (int)xImage;
											int y0 = (int)yImage;											
											double dx = xImage - x0;
											double dy = yImage - y0;
											
											if((x0+1) >= imageWidth || (y0+1) >= imageHeight){
												throw new RuntimeException("ROI " + roi.name + " out of image for pixel xImage="+(x0+1)+", yImage="+(y0+1));
											}
											
											pixelVal += (1 - dx) * (1 - dy) * imageIn.getPixelValue(readLockIn, x0, y0)
													+ (1 - dx) * dy * imageIn.getPixelValue(readLockIn, x0, y0+1)
													+ dx * (1 - dy) * imageIn.getPixelValue(readLockIn, x0+1, y0)
													+ dx * dy * imageIn.getPixelValue(readLockIn, x0+1, y0+1);
										}
									}
								}
								
								//figure out where to put this pixel in the output image
								int oX,oY;
								if(!config.transposeOutput){ //normal
									oX = iOX;
									oY = nextOutputStartRow + iOY;
									
								}else{ //transpose the binned data before putting on the output
									oX = iOY;
									oY = nextOutputStartRow + iOX;
									
								}
							
								if(config.averageBins)
									pixelVal /= (roi.x_binning * roi.y_binning);
								
								//compensate for dead pixel loss
								pixelVal *= (roi.x_binning * roi.y_binning) / (double)(roi.x_binning * roi.y_binning - nDead);								
								
								imageOut.setPixelValue(writeLockOut, oX, oY,pixelVal);
							}
						}
						
						nextOutputStartRow += !config.transposeOutput ? roiBinnedHeight : roiBinnedWidth;
						System.out.println();
						
					}
				}
				
			}finally{ readLockIn.unlock(); }
			
		}catch(InterruptedException err){ }
			
		return true;
	}
	
	
	@Override
	public SoftwareROIsProcessor clone() { return new SoftwareROIsProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			SoftwareROIsSWTController controller = new SoftwareROIsSWTController((Composite)args[0], (Integer)args[1], this, true);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public SoftwareROIsConfig getConfig(){ return config; }
	
	public void configModified(){
		settingsChanged = true;
		updateAllControllers();
		if(autoCalc)
			calc();
	}

	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, SoftwareROIsConfig.class);
		if(config == null) {
			logr.log(Level.SEVERE, "Invalid or empty configuration");
			status = "ERROR: Invalid or empty configuration";
			config = new SoftwareROIsConfig();
		}
		
		updateAllControllers();	
	}

	
	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "SoftwareROIs[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}

	public void configChanged() {
		updateAllControllers();
	}
	
	public PhysicalROIGeometry getCCDGeom(){ return ccdGeom; }
	
	@Override
	public void updateAllControllers() {
		// TODO Auto-generated method stub
		super.updateAllControllers();
	}
}
