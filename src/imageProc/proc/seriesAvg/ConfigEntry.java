package imageProc.proc.seriesAvg;

import java.util.HashMap;

import otherSupport.RandomManager;

public class ConfigEntry {

	public final static int RAD_NONE = 0;
	public final static int RAD_SPATIAL_MEDIAN = 1;
	public final static int RAD_TEMPORAL_MEDIAN = 2;
	public final static int RAD_SPIKE = 3;
	
	public static final String radModes[] = new String[] {
		"None", "Spatial Median", "Temporal Median", "Spike Threshold"
	};

	public String name;
	public boolean enable;
	public boolean subtract;
	public int outIdx0;
	public int outIdx1;
	public int inIdx0;
	public int inIdx1;
	public int inIdxStep;
	/** Smooth frames in window by gaussian of this sigmam in indices.
	 *  (averaging, so intensity conserving)*/
	public double timeSmoothSigma;
	public boolean interlaced;
	public int radRemoval;
	public double spikeThreshold;
	public boolean markAsGoodData;
	
	/** Integrate all frames of this entry, after smoothing and background subtraction
	 * into a single output frame */
	public boolean integrate;	
	
	/** Subtract the first frame of the given output section
	 * from each frame of this section before further processing */
	public ConfigEntry subtractPre, subtractPost;
	
	public ConfigEntry(String name, boolean enable, int inIdx0, int inIdx1, int inIdxStep, double timeSmoothSigma, 
			boolean interlaced, int rad, int spikeThreshold) {
		this.name = name;
		this.enable = enable;
		this.inIdx0 = inIdx0;
		this.inIdx1 = inIdx1;
		this.inIdxStep = inIdxStep;
		this.timeSmoothSigma = timeSmoothSigma;
		this.interlaced = interlaced;
		this.radRemoval = rad;
		this.spikeThreshold = spikeThreshold;
		this.subtractPre = null;
		this.subtractPost = null;
		this.markAsGoodData = true;
		this.integrate = false;
	}

	public ConfigEntry(double[] d) { 
		this.name = "CFG_" + ((int)RandomManager.instance().nextUniform(0, Integer.MAX_VALUE)); 
		setFromArray(d);	
	}

	public ConfigEntry() {
		this.name = "CFG_" + ((int)RandomManager.instance().nextUniform(0, Integer.MAX_VALUE));
		this.inIdx0 = 0;
		this.inIdx1 = -1;
		this.inIdxStep = 1;
		this.radRemoval = ConfigEntry.RAD_NONE;
		this.spikeThreshold = -1;
		this.timeSmoothSigma = -1;
		this.interlaced = false;
		this.enable = true;
		this.markAsGoodData = true;
	}

	public double[] toArray() {
		return new double[]{
				inIdx0,
				inIdx1,
				timeSmoothSigma,
				interlaced ? 1.0 : 0.0,
				radRemoval,
				spikeThreshold,
				inIdxStep,
				enable ? -1 : 0
		};
	}
	
	/** Sets config from double array, stored by toArray()
	 * Stuff has been gradually added to toArray() since the old format
	 * so we have to keep checking the array size
	 * 
	 * @param d
	 */
	public void setFromArray(double d[]){
		inIdx0 = (int)d[0];
		inIdx1 = (int)d[1];
		timeSmoothSigma = d[2];
		interlaced = (d[3] != 0);
		if(d.length == 5){
			//old format
			if(d[4] > 0){
				radRemoval = RAD_SPIKE;
				spikeThreshold = d[4];
			}else if(d[4] == -1){
				radRemoval = RAD_SPATIAL_MEDIAN;
			}else if(d[4] == -2){
				radRemoval = RAD_TEMPORAL_MEDIAN;
			}else{
				radRemoval = RAD_NONE;
			}
			enable = true; //enable was based on name, which we don't have, so just set it true for now
		}else{
			//new format
			radRemoval = (int)d[4];
			spikeThreshold = d[5];
			inIdxStep = (d.length > 6) ? (int)d[6] : 1;
			enable = (d.length > 7) ? (d[7] != 0) : true;			
		}
	}
	
	public HashMap<String, Object> toMap() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("name", name);
		map.put("enable", enable);
		map.put("subtract", subtract);
		map.put("outIdx0", outIdx0);
		map.put("outIdx1", outIdx1);
		map.put("inIdx0", inIdx0);
		map.put("inIdx1", inIdx1);
		map.put("inIdxStep", inIdxStep);
		map.put("timeSmoothSigma", timeSmoothSigma);
		map.put("interlaced", interlaced);
		map.put("radRemoval", radRemoval);
		map.put("spikeThreshold", spikeThreshold);
		map.put("markAsGoodData", markAsGoodData);
		
		return map;

	}
}
