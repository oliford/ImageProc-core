package imageProc.proc.seriesAvg;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;

import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.proc.seriesAvg.SeriesProcessorConfig.SmearCorrectionMode;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.RandomManager;

/** The table directly relfects the config data in the processor's map
* Edits are made into that cfg immediately.
*  
* @author oliford
*/
public class SeriesSWTController extends ImgProcPipeSWTController {
	public static final DecimalFormat fmt = new DecimalFormat("#.###");
	
	public static final String colNames[] = new String[] {
			"-", "Name", "Enable", "isGood", "In0", "In1", "Step", "Sigma (indices)", "Interlace?", "Rad Removal", "Threshold", "Out0", "Out1", "nOut"
		};	
	public static final int COL_HANDLE = 0;
	public static final int COL_NAME = 1;
	public static final int COL_EN = 2;
	public static final int COL_ISGOOD = 3;
	public static final int COL_IN0 = 4;
	public static final int COL_IN1 = 5;
	public static final int COL_STEP = 6;
	public static final int COL_SIGMA = 7;
	public static final int COL_INTERLACE = 8;
	public static final int COL_RAD_REMOVAL = 9;
	public static final int COL_THRESHOLD = 10;
	public static final int COL_OUT0 = 11;
	public static final int COL_OUT1 = 12;
	public static final int COL_NOUT = 13;
	
	protected SeriesProcessor proc;

	private SWTSettingsControl settingsCtrl;
	
	private Text timebaseOffset;
	private Button createBackgroundFieldsCheckbox;
	private Text smearCorrectionCoefficientTextbox;
	private Text smearCorrectionInterpolationRowsTextbox;
	private Combo smearCorrectionModeCombo;
	
	private boolean isSinkController;
	
	private Table cfgTable;
	private TableEditor cfgTableEditor;
	private TableItem tableItemEditing = null;

	private NumberFormat timeOffsetFormat = new DecimalFormat("00.000000");
	
	public SeriesSWTController(Composite parent, int style, SeriesProcessor proc, boolean isSinkController) {
		this.isSinkController = isSinkController;
		this.proc = proc;
		swtGroup = new Group(parent, style);
		swtGroup.setText("Series selection, averaging etc (" + proc.toShortString() + ")");		
		swtGroup.setLayout(new GridLayout(6, false));
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		addCommonPipeControls(swtGroup);
		singleImageCheckbox.setEnabled(false); //doesn't make sense for SeriesProcessor
	
		addAutoConfigControls(swtGroup, SWT.NONE);
		
		SeriesProcessorConfig config = proc.getConfig();
		
		Label lTO = new Label(swtGroup, SWT.NONE); lTO.setText("Timebase offset: ");
		timebaseOffset = new Text(swtGroup, SWT.NONE);		
		timebaseOffset.setText(timeOffsetFormat.format(config.timebaseOffset));
		timebaseOffset.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 0));
		timebaseOffset.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { otherSettingsEvent(event); } });
		
		createBackgroundFieldsCheckbox = new Button(swtGroup, SWT.CHECK);		
		createBackgroundFieldsCheckbox.setText("Create background fields");
		createBackgroundFieldsCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 0));
		createBackgroundFieldsCheckbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { otherSettingsEvent(event); } });

		Label lSC = new Label(swtGroup, SWT.NONE); lSC.setText("Smear correction: ");
		smearCorrectionCoefficientTextbox = new Text(swtGroup, SWT.NONE);
		smearCorrectionCoefficientTextbox.setToolTipText("Correct for CCD smearing by simple subtraction of cumulative total of rows from each row.");
		smearCorrectionCoefficientTextbox.setText(timeOffsetFormat.format(config.smearCoeff));
		smearCorrectionCoefficientTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 0));
		smearCorrectionCoefficientTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { otherSettingsEvent(event); } });
		
		Label lSR = new Label(swtGroup, SWT.NONE); lSR.setText("Rows:");
		smearCorrectionInterpolationRowsTextbox = new Text(swtGroup, SWT.NONE);
		smearCorrectionInterpolationRowsTextbox.setToolTipText("List of 'blank' rows to be interpolated in interpolated rows.");
		smearCorrectionInterpolationRowsTextbox.setText("");
		smearCorrectionInterpolationRowsTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 0));
		smearCorrectionInterpolationRowsTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { otherSettingsEvent(event); } });
		
		smearCorrectionModeCombo = new Combo(swtGroup, SWT.DROP_DOWN | SWT.READ_ONLY);		
		smearCorrectionModeCombo.setItems(Mat.toStringArray(SmearCorrectionMode.values()));	
		smearCorrectionModeCombo.setToolTipText("Smear correction mode.");		
		smearCorrectionModeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 0));
		smearCorrectionModeCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { otherSettingsEvent(event); } });
	
				
		cfgTable = new Table(swtGroup, SWT.BORDER);				
		cfgTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		cfgTable.setHeaderVisible(true);
		cfgTable.setLinesVisible(true);
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(cfgTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]); 
		}

		//proc.loadConfigGMDS(null, -1);
		configToTable();
		
		for(int i=0; i < colNames.length; i++)
			cols[i].pack();		
		
		cfgTableEditor = new TableEditor(cfgTable);			
		cfgTableEditor.horizontalAlignment = SWT.LEFT;
		cfgTableEditor.grabHorizontal = true;
		cfgTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		cfgTable.addListener(SWT.MouseDoubleClick, new Listener() {			
			@Override
			public void handleEvent(Event event) {
				for(int i=0; i < colNames.length; i++)
					cols[i].pack();		
			}
		});
		
		settingsCtrl = ImageProcUtil.createPreferredSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		swtGroup.pack();
		doUpdate();
	}

	private void otherSettingsEvent(Event e){
		SeriesProcessorConfig config = proc.getConfig();
		config.timebaseOffset = Algorithms.mustParseDouble(timebaseOffset.getText());
		config.createBackgroundFields = createBackgroundFieldsCheckbox.getSelection();
		config.smearCoeff = Algorithms.mustParseDouble(smearCorrectionCoefficientTextbox.getText());
		config.smearMode = SmearCorrectionMode.valueOf(smearCorrectionModeCombo.getText());
		config.smearInterpolationRows = (new Gson()).fromJson(smearCorrectionInterpolationRowsTextbox.getText(), int[].class);
	}

	private void configToTable() {
		//get rid of any broken entries		
		for(Entry<String, ConfigEntry> entry : proc.getConfig().map.entrySet()){
			if(entry.getKey().length() <= 0){
				ConfigEntry cfg = entry.getValue();
				if(cfg.name == null || cfg.name.length() == 0){
					cfg.name = "UNKNOWN_" + (int)RandomManager.instance().nextUniform(0, 10000);
				}
				proc.getConfig().map.remove("");
				proc.getConfig().map.put(cfg.name, cfg);
			}
		}
		
		Set<Entry<String, ConfigEntry>> dataSet = proc.getConfig().map.entrySet();
		
		
		//convert map to list and sort
		LinkedList<Entry<String, ConfigEntry>> dataList = new LinkedList<Entry<String,ConfigEntry>>(dataSet);
		Collections.sort(dataList, new Comparator<Entry<String, ConfigEntry>>() {
			@Override
			public int compare(Entry<String, ConfigEntry> o1,
					Entry<String, ConfigEntry> o2) {				
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		
		//clear and repopulate table
		cfgTable.removeAll();
		for(Entry<String, ConfigEntry> entry : dataList){
			String pointName = entry.getKey();
			if(pointName.length() <= 0)
				continue;
			
			TableItem item = new TableItem(cfgTable, SWT.NONE);
			item.setText(COL_NAME, pointName);
			ConfigEntry cfg = entry.getValue();
			if(cfg == null)
				cfg = new ConfigEntry();
			item.setText(COL_HANDLE, "o");			
			item.setText(COL_EN, cfg.enable ? "Y" : "N");
			item.setText(COL_ISGOOD, cfg.markAsGoodData ? "Y" : "N");
			item.setText(COL_IN0, Integer.toString(cfg.inIdx0));
			item.setText(COL_IN1, Integer.toString(cfg.inIdx1));
			item.setText(COL_STEP, Integer.toString(cfg.inIdxStep));
			item.setText(COL_SIGMA, fmt.format(cfg.timeSmoothSigma));
			item.setText(COL_INTERLACE, Boolean.toString(cfg.interlaced));
			item.setText(COL_RAD_REMOVAL, (cfg.radRemoval >= 0 && cfg.radRemoval < ConfigEntry.radModes.length) ? ConfigEntry.radModes[cfg.radRemoval] : "?");
			item.setText(COL_THRESHOLD, fmt.format(cfg.spikeThreshold));
			item.setText(COL_OUT0, Integer.toString(cfg.outIdx0));
			item.setText(COL_OUT1, Integer.toString(cfg.outIdx1));
			item.setText(COL_NOUT, Integer.toString(cfg.outIdx1 - cfg.outIdx0));
			
		}
		
		//and finally a blank item for creating new point
		TableItem blankItem = new TableItem(cfgTable, SWT.NONE);
		blankItem.setText(0, "");
	}
		
	protected void doUpdate(){
		super.doUpdate();
		
		settingsCtrl.doUpdate();
		
		if(tableItemEditing == null)
			configToTable();
		
		SeriesProcessorConfig config = proc.getConfig();
		if(!timebaseOffset.isFocusControl())
			timebaseOffset.setText(timeOffsetFormat.format(config.timebaseOffset));
		
		createBackgroundFieldsCheckbox.setSelection(config.createBackgroundFields);
		if(!smearCorrectionCoefficientTextbox.isFocusControl())			
			smearCorrectionCoefficientTextbox.setText(timeOffsetFormat.format(config.smearCoeff));
		
		if(!smearCorrectionInterpolationRowsTextbox.isFocusControl())			
			smearCorrectionInterpolationRowsTextbox.setText((new Gson()).toJson(config.smearInterpolationRows));
		
		if(!smearCorrectionModeCombo.isFocusControl()) 
			smearCorrectionModeCombo.setText(config.smearMode.toString());
	}
	
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event){
		
		Rectangle clientArea = cfgTable.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = cfgTable.getTopIndex ();
		while (index < cfgTable.getItemCount ()) {
			boolean visible = false;
			final TableItem item = cfgTable.getItem (index);
			for (int i=0; i<cfgTable.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;
					if(column <= COL_HANDLE || column >= COL_OUT0)
						return;
					else if(column == COL_EN || column == COL_ISGOOD){
						tableColumnModified(item, column, null); //just toggle
						return;
					}
					
					final Text text = new Text(cfgTable.getParent(), SWT.BORDER);
					tableItemEditing = item;
					Listener textListener = new Listener () {
						public void handleEvent (final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, text.getText());								
								text.dispose ();
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, text.getText());
									//FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									text.dispose ();
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					text.addListener (SWT.FocusOut, textListener);
					text.addListener (SWT.Traverse, textListener);
					cfgTableEditor.setEditor (text, item, i);
					text.setText (item.getText (i));
					text.selectAll ();
					text.setFocus ();
					//hacks for SWT4.4 (under linux GTK at least)
					//Textbox won't display if it's a child of the table
					//so we make it a child of the table's parent, but now need to adjust the location
					{
						text.moveAbove(cfgTable);
						final Point p0 = text.getLocation();
						Point p1 = cfgTable.getLocation();
						p0.x += p1.x;
						p0.y += p1.y + text.getSize().y;
						text.setLocation(p0);	
						text.addListener (SWT.Move, new Listener() {						
							@Override
							public void handleEvent(Event event) {
								text.setLocation(p0); //TableEditor keeps moving it to relative to the table, so move it back
							}
						});
					}
					return;
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}
	
	private void tableColumnModified(TableItem item, int column, String text){
		HashMap<String, ConfigEntry> pointsMap = proc.getConfig().map;
		
		String configName = item.getText(COL_NAME);
		ConfigEntry cfg = (configName.length() > 0) ? pointsMap.get(configName) : null;
		if(cfg == null)			
			cfg = new ConfigEntry();			
						
		switch(column){
		case COL_NAME:
			//changed point name (or new point)
			if(configName.length() > 0){
				pointsMap.remove(configName);
			}
			
			cfg.name = text;
			configName = text;
			item.setText(column, text);
			break;

		case COL_IN0:
			cfg.inIdx0 = Algorithms.mustParseInt(text);
			item.setText(column, Integer.toString(cfg.inIdx0)); 
			break;

		case COL_EN:
			cfg.enable = !cfg.enable;
			item.setText(column, cfg.enable ? "Y" : "N");
			break;

		case COL_ISGOOD:
			cfg.markAsGoodData = !cfg.markAsGoodData;
			item.setText(column, cfg.markAsGoodData ? "Y" : "N");
			break;
			
		case COL_IN1:
			cfg.inIdx1 = Algorithms.mustParseInt(text);
			item.setText(column, Integer.toString(cfg.inIdx1)); 
			break;
		
		case COL_STEP:
			cfg.inIdxStep = Algorithms.mustParseInt(text);
			if(cfg.inIdxStep < 1)
				cfg.inIdxStep = 1;
			item.setText(column, Integer.toString(cfg.inIdxStep)); 
			break;
			
		case COL_SIGMA:
			cfg.timeSmoothSigma = Algorithms.mustParseDouble(text);
			item.setText(column, fmt.format(cfg.timeSmoothSigma));
			break;
			
		case COL_INTERLACE:
			cfg.interlaced = Algorithms.mustParseBoolean(text);
			item.setText(column, Boolean.toString(cfg.interlaced));
			break;
		case COL_RAD_REMOVAL:
			cfg.radRemoval = OneLiners.findBestMatchingString(ConfigEntry.radModes, text);
			item.setText(column, (cfg.radRemoval >= 0 && cfg.radRemoval < ConfigEntry.radModes.length) ? ConfigEntry.radModes[cfg.radRemoval] : "??");
			break;
		case COL_THRESHOLD:
			cfg.spikeThreshold = Algorithms.mustParseDouble(text);
			item.setText(column, fmt.format(cfg.timeSmoothSigma));
			break;
		case COL_OUT0:
			item.setText(column, Integer.toString(cfg.outIdx0));
			break;
		case COL_OUT1:
			item.setText(column, Integer.toString(cfg.outIdx1));
			break;
		case COL_NOUT:
			item.setText(column, Integer.toString(cfg.outIdx1 - cfg.outIdx0));
			break;
		}
		
		if(cfg.name.length() > 0)
			pointsMap.put(configName, cfg); //put it back
		
		TableItem lastItem = cfgTable.getItem(cfgTable.getItemCount()-1);
		if(lastItem.getText(0).length() > 0){
			TableItem blankItem = new TableItem(cfgTable, SWT.NONE);
			blankItem.setText(0, "");
		}
		
		//proc.saveConfig();
		proc.configModified();
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public SeriesProcessor getPipe() { return proc;	}
	
	/** For dervied classes to add controls for machine-specfic auto config */
	protected void addAutoConfigControls(Composite parent, int style) {
		
	}
}
