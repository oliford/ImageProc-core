package imageProc.proc.seriesAvg;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SeriesProcessorConfig {
			
	/** list of ConfigEntry entries describing each block of frames to process */
	public HashMap<String, ConfigEntry> map = new HashMap<String, ConfigEntry>();
			
	/** Subtract single image from all frames */
	public int subtractImageIndex; 
	
	/** Shift all time metadata by given amount from this source downwards.
	 * Write corrected time metadata in upstream source */
	public double timebaseOffset = 0;
	
	/** When subtracting anything, write background that was subtracted in new fieldfor each input field */
	public boolean createBackgroundFields = false;
	
	public enum SmearCorrectionMode {
		/** No smear correction */
		None,
		
		/** Smear starts collecting at bottom of image and is worst at the top */ 
		Upwards,
		
		/** Smear starts collecting at top of image and is worst at the bottom */
		Downwards,
		
		/** Smear collects from whole image and affects equally all rows*/
		Total,
		
		/** Smear is assesed by interpolating two 'blank' rows vertically */
		Interpolate,
		
	}
	
	/** Smear removal - coefficient of cumulative signal to remove from each line.
	 * 0 to disable */
	public double smearCoeff = 0; //0.003; //AUG1/2 downwards??
	
	/** Rows to be interpolated for the interpolation mode */
	public int[] smearInterpolationRows;

	//smear correction (should go in another processor)
	//double smearCoeff = 0.0045; //AUG1 upwards
	
	//double smearCoeff = 0.0; //CMOS - no smear
	
	public SmearCorrectionMode smearMode = SmearCorrectionMode.None;

	public HashMap<String, Object> toMap() {
		HashMap<String, Object> ret = new HashMap<String, Object>();
		
		//this is a bit too much to add to the metadata
		/*for(Entry<String, ConfigEntry> entry : map.entrySet()) {
			Map<String, Object> entryMap = entry.getValue().toMap();
			
			for(Entry<String, Object> subEntry : entryMap.entrySet()) {
				ret.put("map/" + entry.getKey() + "/" + subEntry.getKey(), subEntry.getValue());
			}
		}*/
		
		ret.put("subtractImageIndex", subtractImageIndex);
		ret.put("timebaseOffset", timebaseOffset);
		ret.put("createBackgroundFields", createBackgroundFields);
		ret.put("smearCoeff", smearCoeff);
		ret.put("smearInterpolationRows", smearInterpolationRows);
		
		return ret;
	}
}
