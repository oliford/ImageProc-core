package imageProc.proc.seriesAvg;

import java.lang.reflect.Array;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import algorithmrepository.Algorithms;
import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import binaryMatrixFile.AsciiMatrixFile;
import descriptors.aug.AUGSignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import fusionDefs.sensorInfo.SensorInfo;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.database.gmds.GMDSUtil;
import imageProc.proc.seriesAvg.SeriesProcessorConfig.SmearCorrectionMode;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

/** Image series processor.
 * 
 * Time series manipulation:
 * 		- Selection cuts (only processing specific image ranges)
 * 		- Radiation removal (Medial average, spike removal ... )
 * 		- Temporal Averaging
 * 
 * @author oliford
 *
 */
public class SeriesProcessor extends ImgProcPipeMultithread implements ConfigurableByID {

	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
	
	protected SeriesProcessorConfig config;
	
	/** input indices for each output image */
	private Integer[] inIdx;

	private String lastLoadedConfig;
	
	public SeriesProcessor() {
		this(null, -1);
	}
	
	public SeriesProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);
		config = new SeriesProcessorConfig();		
	}
	
	@Override
	protected int[] sourceIndices(int outIdx) {
		ConfigEntry cfg = findConfig(outIdx);
		if(cfg == null) //no config means no inputs
			return new int[0];
				
		//the central matching in index
		int inIdxCentre = cfg.inIdx0 + (outIdx - cfg.outIdx0) * cfg.inIdxStep;
		
		//invalid sigma means just the matching image
		double sigmaIdx = cfg.timeSmoothSigma;
 		if(sigmaIdx <= 0){
 			return new int[]{ inIdxCentre };
 		}
		
 		//get full range
		int idx0 = (int)cfg.inIdx0;
		int idx1 = (int)cfg.inIdx1;
		int nImagesIn = connectedSource.getNumImages();
		if(idx1 < 0) //-1 here means to the end
			idx1 = nImagesIn - 1;
		
		//truncate to within sigma range
		if(Double.isFinite(sigmaIdx) && sigmaIdx > 0){
			idx0 = Math.max(idx0, inIdxCentre - (int)(4*sigmaIdx));
			idx1 = Math.min(idx1, inIdxCentre + (int)(4*sigmaIdx));
		}
		
		//if interlaced, make sure loops runs over odd/even input images
		boolean interlaced = cfg.interlaced;
		int step = 1;
		if(interlaced){
			step = 2;
			int offset = (inIdxCentre % 2);
			idx0 = ((int)(idx0/2) * 2) + offset;
			idx1 = ((int)(idx1/2) * 2) + offset; 
		}
		
		//if rad removal, we definitely need the one before and one after
		// err... no , because we dont actually want to include them
		/*boolean radRemoval = cfg[CFG_SPIKE_THRES] > 0;
		if(radRemoval){
			if(idx0 > (outIdx - step) && (outIdx - step) >= 0) 
				idx0 = (outIdx - step);
			if(idx1 < (outIdx + step) && (outIdx + step) < nImagesIn) 
				idx1 = (outIdx + step);
		}*/
				
				
		int n = (idx1 - idx0) / step;
		int idxs[] = new int[n];
		for(int i=0; i < n; i ++){
			idxs[i] = idx0 + i*step;
		}
		return idxs;
	}

	@Override
	/** Image allocation */
	protected boolean checkOutputSet(int nImagesIn){
		
		outWidth = inWidth;
		outHeight = inHeight;
		nFieldsOut = nFieldsIn * (config.createBackgroundFields ? 2 : 1);
		
		ArrayList<Integer> inIndices = new ArrayList<Integer>(nImagesIn);
		
		Set<Entry<String, ConfigEntry>> dataSet = config.map.entrySet();
		
		//convert map to list and sort
		LinkedList<Entry<String, ConfigEntry>> dataList = new LinkedList<Entry<String,ConfigEntry>>(dataSet);
		Collections.sort(dataList, new Comparator<Entry<String, ConfigEntry>>() {
			@Override
			public int compare(Entry<String, ConfigEntry> o1,
					Entry<String, ConfigEntry> o2) {				
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		
		int nImagesOut = 0;
		//work out how many images we are going to need, and create the in-out mapping
		for(Entry<String, ConfigEntry> entry : dataList){
			if(entry != null && entry.getKey().length() > 0){
				ConfigEntry cfg = entry.getValue();
				if(!cfg.enable){
					cfg.outIdx0 = -1;
					cfg.outIdx1 = -1;
					continue;
				}
				if(cfg.inIdxStep < 1)
					cfg.inIdxStep = 1;
				
				int inIdx1 = (cfg.inIdx1 >= 0) ? cfg.inIdx1 : connectedSource.getNumImages()-1;
						
				cfg.outIdx0 = nImagesOut;
				
				//special case of averaging a block into one image
				if(Double.isInfinite(cfg.timeSmoothSigma) && cfg.inIdxStep >= (cfg.inIdx1 - cfg.inIdx0)){
					inIndices.add((cfg.inIdx0 + cfg.inIdx1) / 2);
					nImagesOut++;
					
				}else{
					for(int i=cfg.inIdx0; i < inIdx1; i+=cfg.inIdxStep){
						inIndices.add(i);
						nImagesOut++;
					}
				}
				
				cfg.outIdx1 = nImagesOut;
			}
		}
		
		//save for common usage (metadata)
		inIdx = inIndices.toArray(new Integer[inIndices.size()]);
		
		boolean isGoodData[] = new boolean[nImagesOut];
		for(Entry<String, ConfigEntry> entry : dataList){
			ConfigEntry cfg = entry.getValue();
			if(cfg.markAsGoodData){
				for(int i=cfg.outIdx0; i < cfg.outIdx1; i++)
					isGoodData[i] = true;
			}
		}
		setSeriesMetaData("isGoodData", isGoodData, true);
				
	    //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, nFieldsOut, ByteBufferImage.DEPTH_DOUBLE, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }    
        invalidateAllOutput();
        return false;
	}
	
	@Override
	protected void preCalc(boolean settingsHadChanged) {
		super.preCalc(settingsHadChanged);

	    config.subtractImageIndex = -1;
	    for(Entry<String, ConfigEntry> entry : config.map.entrySet()){
			if(entry != null && entry.getKey().equalsIgnoreCase("subtract")){
				ConfigEntry cfg = entry.getValue();
				config.subtractImageIndex = (int)cfg.inIdx0;				
			}
	    }
	    
	    //some config info that is important to know later on
	    try{
		    setSeriesMetaData("SeriesProc/config/smearMode", config.smearMode.toString(), false);
		    setSeriesMetaData("SeriesProc/config/smearCoeff", config.smearCoeff, false);
		    setSeriesMetaData("SeriesProc/config/timebaseOffset", config.timebaseOffset, false);
		    
			//process meta data now so that its there as the processing is occuring
			//(we'll do it again at the end, because... bugs :( )
		processMetaData();
	    }catch(RuntimeException err){
	    	err.printStackTrace();
	    }
	    
	    //forced background scalaing (e.g. for passive from another spectrometer)
	    String backgroundScaleFilename = (String)getSeriesMetaData("SeriesProc/backgroundScale/fileName");
	    if(backgroundScaleFilename != null) {
	    		//"/home/specgui/backgroundScale.txt";
	    	double d[][] = Mat.transpose(Mat.mustLoadAscii(backgroundScaleFilename));
	    	setSeriesMetaData("SeriesProc/backgroundScale/timetrace", d, false);
	    	setSeriesMetaData("SeriesProc/backgroundScale/factor", 0.04, false);
	    }else {
	    	setSeriesMetaData("SeriesProc/backgroundScale/timetrace", null, false);
	    	setSeriesMetaData("SeriesProc/backgroundScale/factor", 1.0, false);
	    }
	}
	
	@Override
	protected void postCalc(boolean settingsHadChanged) {
		super.postCalc(settingsHadChanged);
		
		//do the metadata again, because... bugs ;(
		processMetaData();
		
	}
		
	private ConfigEntry findConfig(int imgOutIdx){
		for(Entry<String, ConfigEntry> entry : config.map.entrySet()){
			if(entry != null && entry.getKey().length() > 0){
				ConfigEntry cfg = entry.getValue();					
				if(cfg.enable && imgOutIdx >= cfg.outIdx0 && (imgOutIdx < cfg.outIdx1 || cfg.outIdx1 < 0)){
					return cfg;
				}
			}
		}
		return null;
	}
	
	@Override
	protected boolean doCalc(Img imageOutG, WriteLock writeLockOut, Img[] sourceSet, boolean settingsHadChanged) throws InterruptedException {
		//System.out.println(" ** SeriesProc.doCalc(): outIdx=" + imageOutG.getSourceIndex() + ", src[0].max = " + sourceSet[0].getMax() + " ** ");
		
		if(config.map == null){
			try{
				loadConfig(null);
			}catch(RuntimeException err){
				err.printStackTrace();
				
				config.map.put("Background", new ConfigEntry("Background", true, 0, 0, 1, -1, false, ConfigEntry.RAD_NONE, -1));				
				config.map.put("All", new ConfigEntry("All", true, 0, -1, 1, -1, false, ConfigEntry.RAD_TEMPORAL_MEDIAN, -1));				
			}
		}
		
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		int imgIdxOut = imageOut.getSourceIndex();
		ConfigEntry cfg = findConfig(imgIdxOut);
		
 		if(cfg == null){
 			imageOut.setPixelValue(writeLockOut, 0, 0, Double.NaN);
 			imageOut.invalidate();		
			return false;
		}

 		ArrayList<Img> subtractImgs = new ArrayList<Img>();
 		ArrayList<Double> subtractWeights = new ArrayList<Double>();
 		
 		double backgroundScale[][] = (double[][])getSeriesMetaData("SeriesProc/backgroundScale/timetrace");
	    Double backgroundScaleFactor = (Double)getSeriesMetaData("SeriesProc/backgroundScale/factor");
	    Interpolation1D backgroundScaleInterp = null;
	    double timeFromT1[] = (double[])Mat.fixWeirdContainers(getSeriesMetaData("/inshot/timeFromT1"), true);
	    if(backgroundScale != null) {
	    	backgroundScaleInterp = new algorithmrepository.Interpolation1D(backgroundScale[0], backgroundScale[1], InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT_END);	    	
	    }
 		
 		//check for images to subtract now, so we can abort if they're not ready
 		boolean hasPre =  (cfg.subtractPre != null) && (cfg.subtractPre.name.length() > 0);
		boolean hasPost = (cfg.subtractPost != null) && (cfg.subtractPost.name.length() > 0);
				
		if(hasPre){
			Img imageOutBG = getImage(cfg.subtractPre.outIdx0);
			if(imageOutBG != null){
				if(!imageOutBG.isRangeValid()){
					dependantImagesPending = true;
					return false;
				}
				subtractImgs.add(imageOutBG);
				if(hasPost){					
					double fraction = ((double)imageOut.getSourceIndex() - cfg.subtractPre.outIdx0)
							     / ((double)cfg.subtractPost.outIdx0 - cfg.subtractPre.outIdx0);
					subtractWeights.add(1.0 - fraction); //weight of pre
					
				}else{
					subtractWeights.add(1.0);	
				}
			}
		}
		if(hasPost){
			Img imageOutBG = getImage(cfg.subtractPost.outIdx0);
			if(imageOutBG != null){
				if(!imageOutBG.isRangeValid()){
					dependantImagesPending = true;
					return false;
				}
				subtractImgs.add(imageOutBG);
				if(hasPre){
					double fraction = ((double)imageOut.getSourceIndex() - cfg.subtractPre.outIdx0)
						    		/ ((double)cfg.subtractPost.outIdx0 - cfg.subtractPre.outIdx0);
					subtractWeights.add(fraction); //weight of post
					
				}else{
					subtractWeights.add(1.0);
				}
			}
		}
		
		boolean needPMImages = (cfg.radRemoval == ConfigEntry.RAD_SPIKE || cfg.radRemoval == ConfigEntry.RAD_TEMPORAL_MEDIAN);
		
		//actual averaging sums (because NaNs arn't counted)
		double sumAmp[][] = new double[outHeight][outWidth];

		//zero output image
		for(int oF=0; oF < nFieldsOut; oF++) {
			for(int oY=0; oY < outHeight; oY++) {
				for(int oX=0; oX < outWidth; oX++) {
					sumAmp[oY][oX] = 0;
					imageOut.setPixelValue(writeLockOut, oF, oX, oY, 0);						
				}
			}
		}
		
		setImageMetaData("SeriesProc/isIntegrated", imgIdxOut, cfg.integrate);
		setImageMetaData("SeriesProc/numInputImagesUsed", imgIdxOut, sourceSet.length);
		setImageMetaData("SeriesProc/numSubtractedImages", imgIdxOut, subtractImgs.size());
		
		int inputIndecies[] = new int[sourceSet.length];
		for(int i=0; i < sourceSet.length; i++)
			inputIndecies[i] = sourceSet[i].getSourceIndex();			
		setImageMetaData("SeriesProc/inputImagesUsed", imgIdxOut, inputIndecies);		
					
		for(int iF=0; iF < nFieldsIn; iF++) { //do each field totally separately because of bg subtract
			
			try{			
				//for each input image in the input set
				for(Img imageIn : sourceSet){
					if(imageIn == null)
						continue;
					int imgIdxIn = imageIn.getSourceIndex();
					
					if(abortCalc){
						lastCalcFullyComplete = false;
						throw new RuntimeException("Aborted on " + imageOutG.getSourceIndex());
					}
					
					
					//unfortunately, these escape the change detection
					Img imageInP = needPMImages ? connectedSource.getImage(imgIdxIn + (cfg.interlaced ? 2 : 1)) : null;
					Img imageInM = needPMImages ? connectedSource.getImage(imgIdxIn - (cfg.interlaced ? 2 : 1)) : null;
					
					
					ReadLock readLockIn = imageIn.readLock();
					readLockIn.lockInterruptibly();
					try{
						ReadLock readLockP = null;
						if(imageInP != null){
							readLockP = imageInP.readLock();
							readLockP.lockInterruptibly();
						}
						try{
							ReadLock readLockM = null;
							if(imageInM != null){
								readLockM = imageInM.readLock();
								readLockM.lockInterruptibly();
							}
							try{
										
								// Gaussian amplitude of this image adding 
								double amp;
								if(cfg.timeSmoothSigma > 0 && cfg.timeSmoothSigma < Double.POSITIVE_INFINITY){
									int inIdxCentre = cfg.inIdx0 + (imgIdxOut - cfg.outIdx0) * cfg.inIdxStep;
									double arg = ((double)imgIdxIn - inIdxCentre) / cfg.timeSmoothSigma;
									amp = FastMath.exp( -FastMath.pow2(arg) / 2);
									
								}else{
									amp = 1;
								}
								
								for(int oY=0; oY < outHeight; oY++) {
									for(int oX=0; oX < outWidth; oX++) {
										double sum = imageOut.getPixelValue(writeLockOut, iF, oX, oY);
										double val;
										
										switch(cfg.radRemoval){
											case ConfigEntry.RAD_SPIKE:
												val = checkForSpike(imageIn, imageInP, imageInM, readLockIn, readLockP, readLockM, iF, oX, oY, cfg.spikeThreshold);
												break;
											case ConfigEntry.RAD_SPATIAL_MEDIAN:
												val = spatialMedian(imageIn, readLockIn, iF, oX, oY);
												break;
											case ConfigEntry.RAD_TEMPORAL_MEDIAN:
												val = temporalMedian(imageIn, imageInP, imageInM, readLockIn, readLockP, readLockM, iF,oX,oY, cfg.spikeThreshold);
												break;
											default:
												val = imageIn.getPixelValue(readLockIn, iF, oX, oY);
										}
										
										if(!Double.isNaN(val)) {
											sumAmp[oY][oX] += amp;
											imageOut.setPixelValue(writeLockOut, iF, oX, oY, sum + (val * amp));									
										}
									}
									
								}
								
							}finally{ if(readLockM != null) readLockM.unlock(); }
						}finally{ if(imageInP != null) readLockP.unlock(); }
					}finally{ readLockIn.unlock(); }
				}
			}catch(InterruptedException err){ }
			
			// divide out actual sums, (unless we actually want the integral)
			if(!cfg.integrate){
				for(int oY=0; oY < outHeight; oY++) {
					for(int oX=0; oX < outWidth; oX++) {
						double sum = imageOut.getPixelValue(writeLockOut, oX, oY);
						imageOut.setPixelValue(writeLockOut, oX, oY, sum / sumAmp[oY][oX]);
					}						
				}
			}
			

			//should we save background field? if so, clear it
			int oFBG;
			if(config.createBackgroundFields){
				oFBG = nFieldsIn + iF;
				for(int oY=0; oY < outHeight; oY++) {
					for(int oX=0; oX < outWidth; oX++) {
						imageOut.setPixelValue(writeLockOut, oFBG, oX, oY, 0);
					}						
				}
			}else{
				oFBG = -1;
			}
			
			//subtract a given input image. Multiply background first by sumAmp if we integrated this one
			if(config.subtractImageIndex >= 0 && config.subtractImageIndex < connectedSource.getNumImages()){
				Img imageBG = connectedSource.getImage(config.subtractImageIndex);
				if(imageBG != null){
					subtractBGImage(imageOut, writeLockOut, iF, oFBG, imageBG, 1.0, cfg.integrate ? sumAmp : null);
				}
			}
			
			//subtract weighted average of output images (collected earlier)
			//Multiply background first by sumAmp if we integrated this one
			for(int i=0; i < subtractImgs.size(); i++){			
				double weight = subtractWeights.get(i);
				if(backgroundScaleInterp != null)
					weight *= backgroundScaleInterp.eval(timeFromT1[imageOut.getSourceIndex()]);
				if(backgroundScaleFactor != null)
					weight *= backgroundScaleFactor;				
				subtractBGImage(imageOut, writeLockOut, iF, oFBG, subtractImgs.get(i), weight, cfg.integrate ? sumAmp : null);
			}
			
			smearCorrection(imageOut, writeLockOut, iF);
				
		}//end fields loop
		
		//System.out.println(" ** SeriesProc.doCalc(): done for outIdx=" + imageOutG.getSourceIndex() + " ** ");
		return true;
	}

	private void smearCorrection(ByteBufferImage imageOut, WriteLock writeLockOut, int iF) {
		if(config.smearMode == SmearCorrectionMode.Interpolate){
			//special row interpolation mode
			if(config.smearInterpolationRows.length != 2)
				throw new RuntimeException("Currently only interpolation of 2 rows supported");
			int yA = config.smearInterpolationRows[0];
			int yB = config.smearInterpolationRows[1];
			
			for(int oYi=0; oYi < outHeight; oYi++) {				
				int oY = oYi; 
				
				for(int oX=0; oX < outWidth; oX++) {
					double valA = imageOut.getPixelValue(writeLockOut, iF, oX, yA);
					double valB = imageOut.getPixelValue(writeLockOut, iF, oX, yB);
					
					double val = imageOut.getPixelValue(writeLockOut, iF, oX, oY);
					val -= valA + ((double)oY - yA) * (valB - valA) / ((double)yB - yA);
					imageOut.setPixelValue(writeLockOut, iF, oX, oY, val);
					
					if(oY == 6 && oX == 293)
						System.out.println("RAR: " + val);
				}
			}
			return;
		}
		
		if(config.smearMode != SmearCorrectionMode.None){
			double rowCollect[] = new double[outWidth];
						
			for(int oYi=0; oYi < outHeight; oYi++) {				
				int oY = (config.smearMode == SmearCorrectionMode.Upwards) ? (outHeight - oYi - 1) : oYi; 
				
				for(int oX=0; oX < outWidth; oX++) {
					double val = imageOut.getPixelValue(writeLockOut, iF, oX, oY);
					if(config.smearMode != SmearCorrectionMode.Total){
						val -= config.smearCoeff * rowCollect[oX];
						imageOut.setPixelValue(writeLockOut, iF, oX, oY, val);
					}
					
					rowCollect[oX] += val;
				}
			}
			
			if(config.smearMode == SmearCorrectionMode.Total){
				for(int oY=0; oY < outHeight; oY++) {
					for(int oX=0; oX < outWidth; oX++) {
						double val = imageOut.getPixelValue(writeLockOut, iF, oX, oY);
						val -= config.smearCoeff * rowCollect[oX];
						imageOut.setPixelValue(writeLockOut, iF, oX, oY, val);
					}
				}
			}
		}
		
		/* I thought there might be smearing in X, but no, it was just another line
		double xSmearCorrectAdd = -0.10;
		double xSmearCorrectMul = 0.90;
		
		if(xSmearCorrectAdd > 0){
			for(int oY=0; oY < outHeight; oY++) {
				double collect = 0;
				for(int oX=outWidth-1; oX >=0; oX--) {
					double val = imageOut.getPixelValue(writeLockOut, oX, oY);
					val -= collect;
					collect = collect * xSmearCorrectMul + xSmearCorrectAdd * val; 
					imageOut.setPixelValue(writeLockOut, oX, oY, val);
				}
			}
		}*/
	}
	
	
	
	/** @return true if done, false if required iamge not available 
	 * 
	 * @param imageOut
	 * @param writeLockOut
	 * @param imageBG	
	 * @param weight	
	 * @param iF input field number
	 * @param oFBackground Field to write background level to, or -1 to not
	 * @param bgMul 	Multiply background image by this before subtraction
	 * @throws InterruptedException
	 */
	private void subtractBGImage(ByteBufferImage imageOut, WriteLock writeLockOut, int iF, int oFBackground, Img imageBG, double weight, double bgMul[][]) throws InterruptedException {		
		ReadLock readLockBG = imageBG.readLock();
		readLockBG.lockInterruptibly();
		try{
			double maxVal = imageBG.getMaxPossibleValue();
			for(int oY=0; oY < outHeight; oY++) {
				for(int oX=0; oX < outWidth; oX++) {
					
					double val = imageOut.getPixelValue(writeLockOut, iF, oX, oY);
					double bgVal = weight * imageBG.getPixelValue(readLockBG, oX, oY);
					
					if(bgMul != null)
						bgVal *= bgMul[oY][oX];
					
					if(oFBackground >= 0){
						double bgSum = imageOut.getPixelValue(writeLockOut, oFBackground, oX, oY);
						imageOut.setPixelValue(writeLockOut, oFBackground, oX, oY, bgSum + bgVal);
					}
					
					imageOut.setPixelValue(writeLockOut, iF, oX, oY,  
							(val >= maxVal || bgVal >= maxVal) ? Double.NaN : 
												(val - bgVal));
				}
			}
		}finally{ readLockBG.unlock(); }
	}

	/** This is pretty awful. It messes up interference patterns a lot */
	private double spatialMedian(Img imageIn, ReadLock readLockIn, int iF, int oX, int oY) {
		//spatial median filter, rubbish
		
		if(oX <= 0 || oY <= 0 || oX >= (outWidth-1) || oY >= (outHeight-1))
			return imageIn.getPixelValue(readLockIn, iF, oX, oY);
		
		double a[] = new double[]{
				imageIn.getPixelValue(readLockIn, iF, oX-1, oY-1),
				imageIn.getPixelValue(readLockIn, iF, oX  , oY-1),
				imageIn.getPixelValue(readLockIn, iF, oX+1, oY-1),
				imageIn.getPixelValue(readLockIn, iF, oX-1, oY  ),
				imageIn.getPixelValue(readLockIn, iF, oX  , oY  ),
				imageIn.getPixelValue(readLockIn, iF, oX+1, oY  ),
				imageIn.getPixelValue(readLockIn, iF, oX-1, oY+1),
				imageIn.getPixelValue(readLockIn, iF, oX  , oY+1),
				imageIn.getPixelValue(readLockIn, iF, oX+1, oY+1),
		};
		Algorithms.quicksort(a, null);
		return a[4];
		
		/*if(oX <= 1 || oX >= (outWidth-2))
			return imageIn.getPixelValue(readLockIn, iF, oX, oY);
		
		double a[] = new double[]{				
				imageIn.getPixelValue(readLockIn, oX-2, oY  ),
				imageIn.getPixelValue(readLockIn, iF, iF, oX-1, oY  ),
				imageIn.getPixelValue(readLockIn, iF, oX  , oY  ),
				imageIn.getPixelValue(readLockIn, iF, oX+1, oY  ),
				imageIn.getPixelValue(readLockIn, iF, oX+2, oY  ),
		};
		Algorithms.quicksort(a, null);
		return a[2];*/
	}

	/** This is much better. It does average out the time series quite a lot
	 * but it's very effective at removing the short term radiation spikes.  */
	private double temporalMedian(Img imageIn, Img imageInP, Img imageInM, 
										ReadLock readLockIn, ReadLock readLockP, ReadLock readLockM, 
										int iF, int oX, int oY, double threshold) {
		
		if(imageInP == null || imageInM == null)
			return imageIn.getPixelValue(readLockIn, iF, oX, oY);
			
		//temporal median filter
		double a[] = new double[]{
				imageInM.getPixelValue(readLockM, iF, oX, oY),
				imageIn.getPixelValue(readLockIn, iF, oX, oY),
				imageInP.getPixelValue(readLockP, iF, oX, oY),										
		};
		
		if(threshold <= 0 || a[1] > ((a[0]+a[2])/2 + threshold) ){
			Algorithms.quicksort(a, null);
		}
		return a[1];
		
	}
	
	private final double checkForSpike(Img imageIn, Img imageInP, Img imageInM,
										ReadLock readLockIn, ReadLock readLockP, ReadLock readLockM, 
											int f, int x, int y, double spikeThreshold) {
		 
		double val = imageIn.getPixelValue(readLockIn, f, x, y);
		
		//can't do anything without neighboring images
		if(imageInP == null || imageInM == null || x <= 1 || y <= 1 || x >= imageIn.getWidth() - 2 || y >= imageIn.getHeight() - 2)
			return val;
		
		//to call it a spike, we need it to be above both neighboring time, and above at least 18 of the 24 surrounding pixels
		double valTP = imageInP.getPixelValue(readLockP, f, x, y);
		double valTM = imageInM.getPixelValue(readLockM, f, x, y);
		
		boolean spTP = val > valTP + spikeThreshold;
		boolean spTM = val > valTM + spikeThreshold;
		
		//double replace = (valTP + valTM)/2; //we will probably replace it with the time average of the before/after
		int spXY = 0;
		double all[] = new double[24];
		int j=0;
		for(int yy=-2; yy <= 2; yy++){
			for(int xx=-2; xx <= 2; xx++){
				if(xx == 0 && yy == 0)
						continue;
				all[j] = imageIn.getPixelValue(readLockIn, f, x+xx, y+yy);
				if(val > all[j] + spikeThreshold )
					spXY++;
				j++;
			}
		}
		
		if(spTP && spTM && spXY >= 18) {
			//median average of surrounding
			Algorithms.quicksort(all, null);
			return (all[11] + all[12])/2;
			//return (valTP + valTM) / 2;
		}
		
		return val;
	}
	
	@Override
	public SeriesProcessor clone() { return new SeriesProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			SeriesSWTController controller = new SeriesSWTController((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public SeriesProcessorConfig getConfig(){ return config; }
	
	public void configModified(){
		settingsChanged = true;
		updateAllControllers();
		if(autoCalc)
			calc();
	}

	@Override
	public void saveConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds")){
			saveConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
			lastLoadedConfig = id;
		}else if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){

		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.setPrettyPrinting()
				.create(); 

		JsonElement jobj = gson.toJsonTree(config);
		
		((JsonObject) jobj).add("nThreads", new JsonPrimitive(getNumThreads()));
		((JsonObject) jobj).add("autoCalc", new JsonPrimitive(autoCalc));
		
		OneLiners.textToFile(fileName, jobj.toString());
	}
	
	public void saveConfigGMDS(String experiment, int pulse){		
		if(connectedSource == null) return;
		
		String configNames[] = new String[config.map.size()];
		double configData[][] = new double[config.map.size()][];
		
		int i=0;
		for(Entry<String, ConfigEntry> entry : config.map.entrySet()){
			configNames[i] = entry.getKey();
			configData[i] = entry.getValue().toArray();
			i++;
		}
		
		if(experiment == null)
			experiment = GMDSUtil.getMetaExp(connectedSource);
		if(pulse < 0)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		try{
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "SeriesProc/configNames");
			GMDSSignal sig = new GMDSSignal(sigDesc, configNames);
			GMDSUtil.globalGMDS().writeToCache(sig);
	
			sigDesc = new GMDSSignalDesc(pulse, experiment, "SeriesProc/configData");
			sig = new GMDSSignal(sigDesc, configData);
			GMDSUtil.globalGMDS().writeToCache(sig);
			
			sigDesc = new GMDSSignalDesc(pulse, experiment, "SeriesProc/timebaseOffset");
			sig = new GMDSSignal(sigDesc, new double[]{ config.timebaseOffset });
			GMDSUtil.globalGMDS().writeToCache(sig);
			
		}catch(RuntimeException e){
			System.err.println("SeriesProcessor: Error saving points to exp " + experiment + 
								", pulse " + pulse + " because " + e.getMessage());
		}
	}
	
	@Override
	public void loadConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds")){
			loadConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
		}else if(id.startsWith("jsonfile:")){
			loadConfigJSON(id.substring(9));
		}else {
			throw new RuntimeException("Unknown config type " + id);
		}
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	

	public void loadConfigJSON(String fileName){
		String jsonString = OneLiners.fileToText(fileName);
		
		JsonObject jobj = (new JsonParser()).parse(jsonString).getAsJsonObject();
		setNumThreads(((JsonPrimitive) jobj.get("nThreads")).getAsInt());
		autoCalc = ((JsonPrimitive) jobj.get("autoCalc")).getAsBoolean();

		Gson gson = new Gson();
				
		config = gson.fromJson(jsonString, SeriesProcessorConfig.class);
		
		updateAllControllers();
	}
	
	public void loadConfigGMDS(String experiment, int overridePulse) {
		if(connectedSource == null) return;
		
		if(experiment == null)
			experiment = GMDSUtil.getMetaExp(connectedSource);
		int pulse ;
		try{
			pulse = (overridePulse >= 0) ? overridePulse : GMDSUtil.getMetaDatabaseID(connectedSource);
		}catch(RuntimeException e){
			pulse = 0;
		}
		
		if(experiment == null || experiment.length() <= 0){
			experiment = "AUG";
		}
		
		
		//try reading from this specific pulse first
		String configNames[] = null;
		double configData[][] = null;
		try{	
			GMDSSignal sig;
			GMDSSignalDesc sigDesc;
			
			sigDesc = new GMDSSignalDesc(pulse, experiment, "SeriesProc/configData");		
			sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(sigDesc);
			configData = (double[][])sig.get2DData();
			
			try{
				sigDesc = new GMDSSignalDesc(pulse, experiment, "SeriesProc/configNames");		
				sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(sigDesc);
				configNames = (String[])sig.get1DData();
				
			}catch(RuntimeException e){
				//hack for HDF5 string reading problem = regenerate now and save as netCDF from now on
				configNames = new String[configData.length];
				for(int i=0; i <configNames.length; i++){
					configNames[i] = "point_" + i;
				}				
			}

			try{
				sigDesc = new GMDSSignalDesc(pulse, experiment, "SeriesProc/timebaseOffset");		
				sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(sigDesc);				
				config.timebaseOffset = ((double[])sig.get1DData())[0];
				
			}catch(RuntimeException e){
				config.timebaseOffset = 0;
				System.err.println("SeriesProcessor: No timebase offset in exp " + experiment + ", pulse " + pulse);
			}
			
		}catch(RuntimeException e){
			System.err.println("SeriesProcessor: Couldn't load points for exp " + experiment +
									", pulse " + pulse + "because: "+ e.getMessage() + ".");
			config.map.clear();
			config.map.put("All", new ConfigEntry("All", true, 0, -1, 1, -1, false, ConfigEntry.RAD_TEMPORAL_MEDIAN, 1000));			
			updateAllControllers();
			return;
		}
			
		config.map.clear();
		for(int i=0; i < configNames.length; i++){
			config.map.put(configNames[i], new ConfigEntry(configData[i]));
		}
	
		
		updateAllControllers();
		if(autoCalc)
			calc();
	}
	
	/** Everything in the source's metadata that looks like it is a per frame array,
	 * recast it according to the new frame numbers  */
	protected void processMetaData() {
		//firstly, save the original frame
		if(connectedSource.getSeriesMetaData("origFrame") == null){
			this.setSeriesMetaData("origFrame", inIdx, true);
		}else{
			// If there is already and 'origFrame' from earlier up the source chain
			// then it will get reprocessed beloew
		}
		
		MetaDataMap metaIn = connectedSource.getCompleteSeriesMetaDataMap();
				
		for(Entry<String, MetaData> entry : metaIn.entrySet()){
			String name = entry.getKey();
			try{
				Object origArray = entry.getValue().object;
				boolean isTimeSeries = entry.getValue().isTimeSeries;
				
				if(!origArray.getClass().isArray())
					continue;
				 				
				//resequencing time and anything the right length
				if(!isTimeSeries && Array.getLength(origArray) != connectedSource.getNumImages()) 
					continue;
				
				//don't resequence things we wrote (or will write) back as corrected
				if(entry.getKey().contains("SeriesProc/timeCorrected"))
					continue;
					
				int n = inIdx.length;
				Object newArray = Array.newInstance(origArray.getClass().getComponentType(), n);
				if(Array.getLength(origArray) < n){
					n = Array.getLength(origArray);
					System.err.println("SeriesProcessor.processMetaData(): Have "+inIdx.length+" input images but meta data '"+name+"' has length " + n);
					continue;
				}
				
				
				//looks like a nanoTime, 
				//look like a time in seconds from a trigger etc				
				if(isTimeSeries && (entry.getKey().contains("nano") || entry.getKey().contains("time")) 
						&& Array.get(origArray,0).getClass() == Long.class){
					for(int i=0; i < n; i++){			
						long origTime = (Long)Array.get(origArray, inIdx[i]);
						Array.set(newArray, i, origTime + (long)(config.timebaseOffset*1e9));
						setImageMetaData(name + "_orig", i, origTime);
					}
					
					//also rewrite the full series with the shift back to the source as 'corrected'
					long[] correctedArray = new long[Array.getLength(origArray)];
					for(int i=0; i < Array.getLength(origArray); i++){
						correctedArray[i] = ((Long)Array.get(origArray, i)) 
												+ (((long)config.timebaseOffset)*1_000_000_000L);
					}
					connectedSource.setSeriesMetaData("SeriesProc/timeCorrected/" + name, correctedArray, true);
					
				}else if(isTimeSeries && (entry.getKey().contains("time") || entry.getKey().contains("Time"))
						&& FastMath.abs(((Number)Array.get(origArray, inIdx[0])).doubleValue()) < 1000.0){
					for(int i=0; i < n; i++)					
						Array.set(newArray, i, (Double)Array.get(origArray, inIdx[i]) + config.timebaseOffset);
					
					//also rewrite the  full series with the shift back to the source as 'corrected'
					double[] correctedArray = new double[Array.getLength(origArray)];
					for(int i=0; i < Array.getLength(origArray); i++){
						double origTime = ((Number)Array.get(origArray, i)).doubleValue();
						Array.set(correctedArray, i, origTime + config.timebaseOffset);
					}
					connectedSource.setSeriesMetaData("SeriesProc/timeCorrected/" + name, correctedArray, true);
					
				}else{
					//otherwise the metadata take the value of the principle input frame
					//for a fully summed region this will be the first
					//for a running window average it'll be the central one
					for(int i=0; i < n; i++)					
						Array.set(newArray, i, Array.get(origArray, inIdx[i]));				
				}
				
				System.out.println("Resequenced time series metadata '"+name+"' from "+Array.getLength(origArray)+" to "+n+" entries.");
				
				//set into OUR meta data map. Sources further up won't see it, sources further down will
				this.setSeriesMetaData(name, newArray, isTimeSeries);
				
			}catch(RuntimeException err){
				logr.log(Level.WARNING, "Error reporcessing metadata '"+name+"' on to new frames: " + err.getMessage(), err);
			}
		}
		

		SensorInfo camInfo = ImageProcUtil.getGenericCameraMetadata(connectedSource);
		
		try {
			//generate frame by frame exposure time where SeriesProc was summing frames
			Object o = this.getSeriesMetaData("/SeriesProc/numInputImagesUsed");
			if(o != null && camInfo != null){
				boolean isSum[] = (boolean[])Mat.fixWeirdContainers(this.getSeriesMetaData("/SeriesProc/isIntegrated"), true);
							
				int nImgs[] = (int[])Mat.fixWeirdContainers(o, true);
				double[] expTime = new double[nImgs.length];
				for(int i=0; i < nImgs.length; i++)
					expTime[i] = camInfo.exposureTime * (isSum[i] ? nImgs[i] : 1);
				
				this.setSeriesMetaData("/SeriesProc/exposureTime", expTime, true);
				
			}
		}catch(RuntimeException err){
			logr.log(Level.WARNING, "Error reprocessing exposureTime to new frames: " + err.getMessage(), err);
		}
	}
		
	public void configChanged() {
		if(autoCalc)
			calc();
	}
	
	
	public double getTimebaseOffset() { return config.timebaseOffset;	}

	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "SeriesProc[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}
}
