package imageProc.proc.specCal2D;


import java.util.List;

import algorithmrepository.Algorithms;
import binaryMatrixFile.BinaryMatrixFile;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import seed.digeom.FunctionND;
import seed.digeom.IDomain;
import seed.digeom.RectangularDomain;
import seed.optimization.HookeAndJeeves;

/** Transform between iamge XY (0-1) and some other coordinate AB
 */
public class SpecCalFitter extends FunctionND {
	/** Range of knot X,Y values in interp when fitting, these can get quite high/low in the extrapolated areas */
	private static final double minP = -10, maxP = +10;
	
	private SpecCalFitCubic specCal;
		
	/** sigma of fitting */
	private double sigma = 0.001;
	
	private int nWavelen, nSpatial;
			
	public SpecCalFitter(SpecCalFitCubic xform){
		this.specCal = xform;
		this.nWavelen = xform.getNKnotsWavelen();
		this.nSpatial = xform.getNKnotsSpatial();
		
	}	

	/**
	 * @param fitDataXY		target points in normalised image coords
	 * @param fitDataAB		target points in dest (beam) coords
	 * @param initTransform		3x3 linear transform matrix x,y --> A,B 
	 * @param doFit
	 */
	public void fit() {
		//first invert to linear transform
		
		specCal.initCubicToLinear();
		double p[] = getP();
		
		if(!Double.isNaN(p[0]))
			p = doFit(p);
	
		setP(p);	
		
	}
	
	public void dumpAll(){
		double minA = specCal.getMinWavelength();
		double maxA = specCal.getMaxWavelength();
		double minB = specCal.getMinSpatial();
		double maxB = specCal.getMaxSpatial();
		
		double mg[][][] = Mat.meshgrid(Mat.linspace(minA, maxA, 500), Mat.linspace(minB, maxB, 500));
		double aaL[] = Mat.flatten(mg[0]);
		double bbL[] = Mat.flatten(mg[1]);
		
		double linXY[][] = specCal.lsToXYLinear(aaL, bbL); 
		
		Mat.mustWriteBinary("/tmp/Xl.bin", Mat.unflatten(linXY[0],500,500), false);
		Mat.mustWriteBinary("/tmp/Yl.bin", Mat.unflatten(linXY[1],500,500), false);
		
		if(specCal.isCubicValid()){
			double xyL[][] = specCal.lsToXY(aaL, bbL);
			Mat.mustWriteBinary("/tmp/X.bin", Mat.unflatten(xyL[0],500,500), false);
			Mat.mustWriteBinary("/tmp/Y.bin", Mat.unflatten(xyL[1],500,500), false);
			Mat.mustWriteBinary("/tmp/Xk.bin", specCal.getKnotValsX(), false);
			Mat.mustWriteBinary("/tmp/Yk.bin", specCal.getKnotValsY(), false);
		}		
	}
	
	private double[] doFit(double initP[]){
		//do the fit
		//ConjugateGradientDirectionFR cg = new ConjugateGradientDirectionFR();
		//CoordinateDescentDirection cd = new CoordinateDescentDirection();
		//GoldenSection ls = new GoldenSection(new MaxIterCondition(500));
		//NewtonsMethod1D ls = new NewtonsMethod1D(new MaxIterCondition(500));
		
		//LineSearchOptimizer opt = new LineSearchOptimizer(null, cg, ls);
		
		//gs.setInitialBracketMethod(new BracketingByParameterSpace());		
		HookeAndJeeves opt = new HookeAndJeeves(this);
		
		opt.setObjectiveFunction(this);
		opt.init(initP);
		
		//System.out.println(opt.getCurrentValue());
		//double p2[] = opt.getCurrentPos().clone();
		//System.out.println(eval(p2));
		//p2[2*nPX+2] = 1000;				
		//System.out.println(eval(p2));
		
		int nIters = 50;
		for(int i=0; i < nIters; i++){
			opt.refine();
			
			double p[] = opt.getCurrentPos();
			double cost = opt.getCurrentValue();			
			System.out.println("i=" + i + "\tp22 = "+p[1*nWavelen+1]+"\tcost=" + cost); 
			//*/
		}

		double finalP[] = opt.getCurrentPos();	
		System.out.println("Cost: init = " + eval(initP) + "\tFinal = " + eval(finalP));
		finalP = opt.getCurrentPos();	
		System.out.println("Cost: init = " + eval(initP) + "\tFinal = " + eval(finalP));
		
		return finalP;
	}

	@Override
	public IDomain getDomain() { return new RectangularDomain(Mat.fillArray(minP, nWavelen*nSpatial*2), Mat.fillArray(maxP, nWavelen*nSpatial*2)); }
	
	private double[] getP(){
		double gPX[][] = specCal.getKnotValsX();
		double gPY[][] = specCal.getKnotValsY();
		double p[] = new double[nWavelen*nSpatial*2];
		
		for(int iB=0; iB < nSpatial; iB++){
			for(int iA=0; iA < nWavelen; iA++){
				p[2*(iB*nWavelen + iA)] = gPX[iA][iB];
				p[2*(iB*nWavelen + iA)+1] = gPY[iA][iB];
			}
		}
		
		return p;
	}
		
	private void setP(double p[]){
		double gPX[][] = specCal.getKnotValsX();
		double gPY[][] = specCal.getKnotValsY();
		
		for(int iB=0; iB < nSpatial; iB++){
			for(int iA=0; iA < nWavelen; iA++){
				gPX[iA][iB] = p[2*(iB*nWavelen + iA)];
				gPY[iA][iB] = p[2*(iB*nWavelen + iA)+1];
			}
		}
		
		specCal.setKnotValsX(gPX);
		specCal.setKnotValsY(gPY);
	}
	
	@Override
	public double eval(double[] x) {			
		setP(x);
		
		double logP = 0;
		double r2p = FastMath.sqrt(2*Math.PI);
		
		List<SpecCalPoint> points = specCal.getPoints();
		synchronized (points) {
			for(SpecCalPoint p : points){
				if(p.includeInFit()){
					double fitXY[] = specCal.lsToXY(p.wavelength, p.spatial);
					double logPPoint = 0.5 * FastMath.pow2((p.ccdX - fitXY[0])/sigma) // - (sigma * r2p);
									+ 0.5 * FastMath.pow2((p.ccdY - fitXY[1])/sigma);// - (sigma * r2p);
					
					if(Double.isNaN(logPPoint)){
						//throw new RuntimeException("Point '" + p.name + "' evaluated to NaN in fit");
						System.err.println("Point '" + p.name + "' evaluated to NaN in fit");
						logPPoint = 0;
					}
					
					logP += logPPoint;
				}
			}
		}
		
		return logP;
	}
}
