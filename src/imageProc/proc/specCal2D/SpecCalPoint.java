package imageProc.proc.specCal2D;


public class SpecCalPoint {
	public final static String modeNames[] = new String[]{ 
		"Ignore", 
		"Cubic fit", 
		"Linear+Cubic fit", 
		"No fit", 
	};

	public final static int MODE_IGNORE = 0;			//Ignore entirely. Don't display or calculate with
	public final static int MODE_FIT = 1;				// Use in the full (cubic) fit.
	public final static int MODE_FIT_AND_MATRIX = 2;	// Use for the initial linear fit and in full fit
	public final static int MODE_BACK_TRANSLATE = 3;	// Display back transaltion but don't use in fits
	
	private final static int SIG_MODE = 0; //coordinates of feature in image, physical coordinates on CCD (in m)
	private final static int SIG_CCD_X = 1; //coordinates of feature in image, physical coordinates on CCD (in m)
	private final static int SIG_CCD_Y = 2;
	private final static int SIG_GRATING_POS = 3;
	private final static int SIG_WAVELEN = 4;
	private final static int SIG_SPATIAL = 5;
			
	/** Identifying name - point UID */
	public String name;
	
	/** X position on CCD (m, with 0 at centre) */
	public double ccdX;
	/** Y position on CCD (m, with 0 at centre) */
	public double ccdY;

	/** Variable parameter of spectrometer, usually motor position setting for wavelenth scanning */
	public double gratingPosition;
	/** Real wavelength seen at this point . in nm!!*/
	public double wavelength;
	/** Wavelength perpendicular coordinate of this point. Normally some kind of spatial, perhaps simply channel number. */		
	public double spatial;
	
	/** ID of data image where this calibration point came from. */
	public String calSourceID;
		
	/** Enable status */
	public int mode = SpecCalPoint.MODE_IGNORE;
	
	public SpecCalPoint(String name) {
		this.name = name;
	}
	
	public SpecCalPoint(String name, double[] data, String sourceID) {
		this.name = name;
		this.mode = (int)data[SIG_MODE];
		this.ccdX = data[SIG_CCD_X];
		this.ccdY = data[SIG_CCD_Y];
		this.gratingPosition = data[SIG_GRATING_POS];
		this.wavelength = data[SIG_WAVELEN];
		this.spatial = data[SIG_SPATIAL];	
		this.calSourceID = sourceID;
	}
	
	public double[] toArray() {			
		return new double[]{
				mode,
				ccdX, ccdY,
				gratingPosition,
				wavelength,
				spatial
		};
	}

	public void setField(int col, double val) {
		switch(col){
			case SIG_MODE: this.mode = (int)val; break;
			case SIG_CCD_X: this.ccdX = val; break;
			case SIG_CCD_Y: this.ccdY = val; break;
			case SIG_GRATING_POS: this.gratingPosition = val; break;
			case SIG_WAVELEN: this.wavelength = val; break;
			case SIG_SPATIAL: this.spatial = val; break;		
			default:
				throw new IllegalArgumentException();
		}
		
	}

	public boolean includeInFit() {
		return mode == MODE_FIT || mode == MODE_FIT_AND_MATRIX;
	}
}
