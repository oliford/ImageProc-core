package imageProc.proc.specCal2D;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import otherSupport.RandomManager;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.database.gmds.GMDSPipe;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SpecCalSWTController extends ImgProcPipeSWTController implements SWTControllerInfoDraw, ImagePipeControllerROISettable {

	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.###");
	
	public static final String colNames[] = new String[] { "-", "ID", "Type", "ccdX", "ccdY",  "Spec Setting", "Wavelength", "Spatial", "Cal Source" };

	private SpecCalProcessor proc;
	private Button setPositionsCheckbox;
	
	private Button doFitCheckbox;
	private Button cubicFitCheckbox;
	private Spinner cubicNumXSpinner;
	private Spinner cubicNumYSpinner;	
		
	private Button channelsAreRowsCheckbox;

	private Combo loadExpCombo; 
	private Spinner loadPulseSpiner;
	private Button loadPointsButton;
	private Button savePointsButton;
	
	private Text minWavelengthTextbox;
	private Text maxWavelengthTextbox;
	private Text minChannelTextbox;
	private Text maxChannelTextbox;
	
	private Table pointsTable;
	private TableEditor pointsTableEditor;
	private TableItem tableItemEditing = null;
	
	private Button setFromFitButton;
	
	public SpecCalSWTController(Composite parent, int style, SpecCalProcessor proc, boolean asSink) {
		this.proc = proc;
		swtGroup = new Group(parent, style);
		
		swtGroup.setText("SpecCal Control (" + proc.toShortString() + ")");		
		swtGroup.setLayout(new GridLayout(6, false));
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		addCommonPipeControls(swtGroup);
		
		doFitCheckbox = new Button(swtGroup, SWT.CHECK);
		doFitCheckbox.setText("Refit");
		doFitCheckbox.setSelection(proc.getDoFit());
		doFitCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		doFitCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		channelsAreRowsCheckbox = new Button(swtGroup, SWT.CHECK);
		channelsAreRowsCheckbox.setText("Channels fixed to rows");
		channelsAreRowsCheckbox.setSelection(proc.getSpecCalFit().channelsAreRows);
		channelsAreRowsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		channelsAreRowsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		setPositionsCheckbox = new Button(swtGroup, SWT.CHECK);
		setPositionsCheckbox.setText("Set points");
		setPositionsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		cubicFitCheckbox = new Button(swtGroup, SWT.CHECK);
		cubicFitCheckbox.setText("Cubic");
		cubicFitCheckbox.setSelection(false);
		cubicFitCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		cubicFitCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		setFromFitButton = new Button(swtGroup, SWT.PUSH);
		setFromFitButton.setText("From Fit");
		setFromFitButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { fromFitButtonEvent(event); } });
		setFromFitButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
	
		Label lNX = new Label(swtGroup, SWT.NONE); lNX.setText("nKnots X:");
		cubicNumXSpinner = new Spinner(swtGroup, SWT.NONE);
		cubicNumXSpinner.setValues(3, 2, 100, 0, 1, 4);
		cubicNumXSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		cubicNumXSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNY = new Label(swtGroup, SWT.NONE); lNY.setText("nKnots Y:");
		cubicNumYSpinner = new Spinner(swtGroup, SWT.NONE);
		cubicNumYSpinner.setValues(3, 2, 100, 0, 1, 4);
		cubicNumYSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		cubicNumYSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lNL = new Label(swtGroup, SWT.NONE); lNL.setText("Wavelength: min:");
		minWavelengthTextbox = new Text(swtGroup, SWT.NONE);
		minWavelengthTextbox.setText(formatTableValue(proc.getSpecCalFit().minWavelen));
		minWavelengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		minWavelengthTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lXL = new Label(swtGroup, SWT.NONE); lXL.setText("max:");
		maxWavelengthTextbox = new Text(swtGroup, SWT.NONE);
		maxWavelengthTextbox.setText(formatTableValue(proc.getSpecCalFit().maxWavelen));
		maxWavelengthTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		maxWavelengthTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lNC = new Label(swtGroup, SWT.NONE); lNC.setText("Channel: min:");
		minChannelTextbox = new Text(swtGroup, SWT.NONE);
		minChannelTextbox.setText(formatTableValue(proc.getSpecCalFit().minWavelen));
		minChannelTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		minChannelTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lXC = new Label(swtGroup, SWT.NONE); lXC.setText("max:");
		maxChannelTextbox = new Text(swtGroup, SWT.NONE);
		maxChannelTextbox.setText(formatTableValue(proc.getSpecCalFit().maxWavelen));
		maxChannelTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		maxChannelTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		pointsTable = new Table(swtGroup, SWT.BORDER | SWT.MULTI);				
		pointsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		pointsTable.setHeaderVisible(true);
		pointsTable.setLinesVisible(true);
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(pointsTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]); 
		}

		pointsToGUI();

		for(int i=0; i < colNames.length; i++)
			cols[i].pack();
		
		pointsTable.addListener(SWT.MouseDoubleClick, new Listener() {		//and the user can do that by double clicking 
			@Override
			public void handleEvent(Event event) {
				for(int i=0; i < colNames.length; i++)
					cols[i].pack();		
			}
		});
		
		pointsTableEditor = new TableEditor(pointsTable);
		pointsTableEditor.horizontalAlignment = SWT.LEFT;
		pointsTableEditor.grabHorizontal = true;
		pointsTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		pointsTable.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceImageRedraw(); } });
		
		Label lOP = new Label(swtGroup, SWT.NONE); lOP.setText("Load:");		
		loadExpCombo = new Combo(swtGroup, SWT.NONE);
		loadExpCombo.setItems(GMDSPipe.getAvailableExperiments());
		loadExpCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		loadPulseSpiner = new Spinner(swtGroup, SWT.NONE);
		loadPulseSpiner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		loadPulseSpiner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		loadPointsButton = new Button(swtGroup, SWT.PUSH);
		loadPointsButton.setText("Load");
		loadPointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadAltPulseButtonEvent(event); } });
		
		savePointsButton = new Button(swtGroup, SWT.PUSH);
		savePointsButton.setText("Save (current pulse)");
		savePointsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
		
		swtGroup.pack();
		doUpdate();
	}
	
	private void settingsChangedEvent(Event e){
		proc.setDoFit(doFitCheckbox.getSelection());		
		proc.setCubicInterp(cubicFitCheckbox.getSelection(), 
								cubicNumXSpinner.getSelection(), 
								cubicNumYSpinner.getSelection());
		SpecCalFit specCal = proc.getSpecCalFit();
		specCal.channelsAreRows = channelsAreRowsCheckbox.getSelection();
		specCal.minWavelen = Algorithms.mustParseDouble(minWavelengthTextbox.getText());
		specCal.maxWavelen = Algorithms.mustParseDouble(maxChannelTextbox.getText());
		specCal.minSpatial = Algorithms.mustParseDouble(minChannelTextbox.getText());
		specCal.maxSpatial = Algorithms.mustParseDouble(maxChannelTextbox.getText());
		
	}
	
	private void loadAltPulseButtonEvent(Event e) {
		proc.loadConfig(loadExpCombo.getText(), loadPulseSpiner.getSelection());
		pointsToGUI();
	}
	
	private void fromFitButtonEvent(Event e) {
		TableItem[] selected = pointsTable.getSelection();
		if(selected.length > 0){		
			String pointName = selected[0].getText(1);
			
			SpecCalFit specCal = proc.getSpecCalFit();
			SpecCalPoint p = specCal.getPointByName(pointName);
					
			proc.setFromFit(p);
		}
		pointsToGUI();
	}
	
	private void saveButtonEvent(Event e) {
		proc.saveConfig("gmds//-1");
		pointsToGUI();
	}
	
	protected void doUpdate(){
		super.doUpdate();
		
		boolean doFit = proc.getDoFit();
		if(doFitCheckbox.getSelection() != doFit)
			doFitCheckbox.setSelection(doFit);
		
		boolean chansAreRows = proc.getSpecCalFit().channelsAreRows;
		if(channelsAreRowsCheckbox.getSelection() != chansAreRows)
			channelsAreRowsCheckbox.setSelection(chansAreRows);
	
		int nX = proc.getCubicNX(), nY = proc.getCubicNY();
		if(nX <= 0 && nY <= 0){
			if(cubicFitCheckbox.getSelection())
				cubicFitCheckbox.setSelection(false);
			
		}else{
			if(!cubicFitCheckbox.getSelection())
				cubicFitCheckbox.setSelection(true);
			
			if(cubicNumXSpinner.getSelection() != nX)
				cubicNumXSpinner.setSelection(nX);
			
			if(cubicNumYSpinner.getSelection() != nY)
				cubicNumYSpinner.setSelection(nY);
		}
				
		
		if(tableItemEditing == null)
			pointsToGUI();
		
	}

	@Override
	public void movingPos(int x, int y) {	}

	@Override
	public void fixedPos(int x, int y) {	
		if(swtGroup.isDisposed() || !setPositionsCheckbox.getSelection())
			return;
		
		Img img = proc.getSelectedImageFromConnectedSource();
		if(img == null)
			return;
		int imageWidth = img.getWidth();
		int imageHeight = img.getHeight();
				
		TableItem[] selected = pointsTable.getSelection();
		if(selected.length == 1){
			String pointName = selected[0].getText(1);
			
			SpecCalFit specCal = proc.getSpecCalFit();
			PhysicalROIGeometry ccdGeom = proc.getCCDGeometry();
			SpecCalPoint p = specCal.getPointByName(pointName);
			p.ccdX = ccdGeom.getImgPhysX0() + x * (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0()) / imageWidth;
			p.ccdY = ccdGeom.getImgPhysY0() + y * (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0()) / imageHeight;
			
			//we have to edit the item because the update won't actually take place immediately
			selected[0].setText(2, tableValueFormat.format(x));
			selected[0].setText(3, tableValueFormat.format(y));
			
			proc.pointsMapModified();
		}
		
	}

	@Override
	public void setRect(int x0, int y0, int width, int height) {
		
	}
	
	private final String formatTableValue(double val){
		return Double.isNaN(val) ? "NaN" : tableValueFormat.format(val);
	}
	
	private void pointsToGUI() {
				
		ArrayList<SpecCalPoint> points = proc.getSpecCalFit().getPoints();
		synchronized (points) {
			points = new ArrayList<SpecCalPoint>(points); //clone to release lock but avoid concurrent modification
		}
			
		//convert map to list and sort
		Collections.sort(points, new Comparator<SpecCalPoint>() {
			@Override
			public int compare(SpecCalPoint p1, SpecCalPoint p2) {				
				return p1.name.compareTo(p2.name);
			}
		});
		
		//clear and repopulate table
		TableItem selected[] = pointsTable.getSelection();
		String selectedPoint = (selected.length >= 1) ? selected[0].getText(1) : null; 
		pointsTable.removeAll();
		for(SpecCalPoint p : points){
			
			if(p.name.length() <= 0)
				continue;
			TableItem item = new TableItem(pointsTable, SWT.NONE);
			item.setText(0, "o");
			item.setText(1, p.name);
			item.setText(2, SpecCalPoint.modeNames[p.mode]);
			item.setText(3, formatTableValue(p.ccdX * 1e3));
			item.setText(4, formatTableValue(p.ccdY * 1e3));
			

			item.setText(5, formatTableValue(p.gratingPosition));
			item.setText(6, formatTableValue(p.wavelength));
			item.setText(7, formatTableValue(p.spatial));
			item.setText(8, (p.calSourceID == null) ? "(null)" : p.calSourceID);
						
		}
		
		//and finally a blank item for creating new point
		TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
		blankItem.setText(0, "o");
		blankItem.setText(1, "<new>");
		
		if(selectedPoint != null){
			for(int i=0; i < pointsTable.getItemCount(); i++){
				if(selectedPoint.equals(pointsTable.getItem(i).getText(1))){
					pointsTable.select(i);
					break;
				}
			}
		}
	}
		
	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth, int imageHeight, boolean asSource) {
		
		ArrayList<SpecCalPoint> points = proc.getSpecCalFit().getPoints();
		synchronized (points) {
			points = new ArrayList<SpecCalPoint>(points); //clone to release lock but avoid concurrent modification
		}
		
		int r = Math.min(imageWidth, imageHeight) / 100;
		
		HashMap<SpecCalPoint, double[]> remapped = proc.getBackConvertedXY();
		PhysicalROIGeometry ccdGeom = proc.getCCDGeometry();
		
		int i=0;
		for(SpecCalPoint p : points){
			
			if(p.mode == SpecCalPoint.MODE_IGNORE)
				continue;
				
			TableItem[] selectedItems = pointsTable.getSelection();
			boolean isSelected = false;
			for(TableItem item : selectedItems){
				if(item.getText(1).equals(p.name))
					isSelected = true;
			}
			if(isSelected){
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_MAGENTA));
				gc.setLineWidth(3);
			}else{
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
				gc.setLineWidth(1);
			}
			
			double imgX, imgY;
		
			if(ccdGeom == null)
				return;
			
			imgX = (p.ccdX - ccdGeom.getImgPhysX0()) / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
			imgY = (p.ccdY - ccdGeom.getImgPhysY0()) / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
						
			if(imgX >= 0 && imgY >= 0 && 
					imgX < 1.0 && imgY < 1.0){
					
				gc.drawOval(
						(int)((imgX*imageWidth - r) * scale[0]), 
						(int)((imgY*imageHeight - r) * scale[1]), 
						(int)(r*2 * scale[0]), 
						(int)(r*2 * scale[1]));
			}
			
			
			
			if(remapped != null){
				double remappedPos[] = remapped.get(p);
				if(!asSource && remappedPos != null) {
					double remappedX = (remappedPos[0] - ccdGeom.getImgPhysX0()) / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
					double remappedY = (remappedPos[1] - ccdGeom.getImgPhysY0()) / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
					gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_RED));			
					gc.drawOval(
							(int)((remappedX*imageWidth - r) * scale[0]), 
							(int)((remappedY*imageHeight - r) * scale[1]), 
							(int)(r*2 * scale[0]), 
							(int)(r*2 * scale[1]));
					
					if(isSelected){
						gc.drawLine((int)(imgX*imageWidth * scale[0]),
								(int)(imgY*imageHeight * scale[1]),
								(int)(remappedX*imageWidth * scale[0]),
								(int)(remappedY*imageHeight * scale[1]));
					}
				}
			}
			
			i++;
		}
			
	}
	
	private void forceImageRedraw() {
		ImgSource src = proc.getConnectedSource();
		if(src == null)
			return;
		//force update of GUI windows connected to the image source
		for(ImgSink sink : src.getConnectedSinks()){
			if(sink instanceof ImagePanel){
				((ImagePanel)sink).redraw();
			}
		}
	}
	
	private Text editBox = null;
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event){
		Rectangle clientArea = pointsTable.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = pointsTable.getTopIndex ();
		while (index < pointsTable.getItemCount ()) {
			boolean visible = false;
			final TableItem item = pointsTable.getItem (index);
			for (int i=0; i<pointsTable.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;
					if(column == 0){
						//pointsTable.setSelection(index); //make sure its selected
						forceImageRedraw();
						return;
					}
					if(editBox != null){
						editBox.dispose(); //oops, one left over
					}
					editBox = new Text(pointsTable.getParent(), SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener () {
						public void handleEvent (final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, editBox.getText());								
								editBox.dispose();
								editBox = null;
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, editBox.getText());
									//FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									editBox.dispose ();
									editBox = null;
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					editBox.addListener (SWT.FocusOut, textListener);
					editBox.addListener (SWT.Traverse, textListener);
					pointsTableEditor.setEditor (editBox, item, i);
					editBox.setText (item.getText (i));
					editBox.selectAll ();
					editBox.setFocus ();
					//hacks for SWT4.4 (under linux GTK at least)
					//Textbox won't display if it's a child of the table
					//so we make it a child of the table's parent, but now need to adjust the location
					{
						editBox.moveAbove(pointsTable);
						final Point p0 = editBox.getLocation();
						Point p1 = pointsTable.getLocation();
						p0.x += p1.x;
						p0.y += p1.y + editBox.getSize().y;
						editBox.setLocation(p0);	
						editBox.addListener (SWT.Move, new Listener() {						
							@Override
							public void handleEvent(Event event) {
								editBox.setLocation(p0); //TableEditor keeps moving it to relative to the table, so move it back
							}
						});
					}
					return;
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}
	
	private void tableColumnModified(TableItem item, int column, String text){
		
		ArrayList<SpecCalPoint> points = proc.getSpecCalFit().getPoints();
		synchronized (points) {
			
			String pointName = item.getText(1);
			
			SpecCalPoint point = (pointName.length() > 0) 
									? proc.getSpecCalFit().getPointByName(pointName)
									: null;
	
			if(column == 1){ //name
				
				if(point == null && (pointName.length() <= 0 || pointName.equals("<new>"))){ //creating new point
					point = new SpecCalPoint(text);
					points.add(point);
					
				}else if(text.length() <= 0){ //deleting point
					points.remove(point);
					
				}else{ //just changing name
					point.name = text;
				}
				
				item.setText(column, text);
			}else if(column == 2){ //for point type, we accept any part of the string, or the value 
				if(pointName.length() > 0 && !pointName.equals("<new>") && point != null){ //can only edit values for existing points
	
					int bestMatchIdx = OneLiners.findBestMatchingString(SpecCalPoint.modeNames, text);
					
					if(bestMatchIdx > 0){ 
						point.mode = bestMatchIdx;
					}else{//otherwise, fall back to interpreting as a number
						point.mode = Algorithms.mustParseInt(text);
						
					}
					
					item.setText(column, SpecCalPoint.modeNames[point.mode]);
					
				}
			}else if(column == 8){
				point.calSourceID = text;
				item.setText(column, text);
				
			}else{
				if(pointName.length() > 0 && point != null){ //can only edit values for existing points
					double val = Algorithms.mustParseDouble(text);
					
					if(column >= 3 && column <= 4){
						val /= 1000;
					}
										
					point.setField(column-2, val);
					
					item.setText(column,  Double.isNaN(val) ? "NaN" :tableValueFormat.format(val));
				}
			}
	
			TableItem lastItem = pointsTable.getItem(pointsTable.getItemCount()-1);
			if(!lastItem.getText(1).equals("<new>")){
				TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
				blankItem.setText(0, "o");
				blankItem.setText(1, "<new>");
			}
			
			//proc.saveMapPoints();
			proc.pointsMapModified();
		}
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
		//proc.destroy(); //and actively kill it, for good measure
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public SpecCalProcessor getPipe() { return proc;	}
}
