package imageProc.proc.specCal2D;

import java.util.ArrayList;
import java.util.Map;

import descriptors.gmds.GMDSSignalDesc;
import fusionDefs.transform.PhysicalROIGeometry;
import oneLiners.OneLiners;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.Interpolation2D;
import algorithmrepository.InterpolationMode;
import net.jafama.FastMath;
import seed.digeom.Function;
import seed.digeom.FunctionND;
import seed.digeom.IDomain;
import seed.digeom.RectangularDomain;
import seed.optimization.HookeAndJeeves;
import signals.gmds.GMDSSignal;
import mds.GMDSFetcher;

/** Linear and cubic version of SpecCalFit */
public class SpecCalFitCubic extends SpecCalFit {

	/** Axes of the cubic interpolators */
	private double pWavelen[], pSpatial[];
	
	/** Interpolators for the cubic fit. These give the values X and Y on a grid in LS */
	private Interpolation2D interpX, interpY;

	/** Matrix of linear version */
	private double linXYtoLS[][], linLStoXY[][];

	
	public SpecCalFitCubic() {
		super();
	}
	
	public SpecCalFitCubic(GMDSFetcher gmds, String experiment, int pulse) {
		super(gmds, experiment, pulse);
		
		loadLinear(gmds, experiment, pulse);
		loadKnots(gmds, experiment, pulse);
	}
	
	public SpecCalFitCubic(Map<String, Object> metaData) {
		super(metaData);
		
		loadLinear(metaData);
		loadKnots(metaData);
	}
	
	public void saveToSignal(GMDSFetcher gmds, String experiment, int pulse){
		super.saveToSignal(gmds, experiment, pulse);
		if(isLinearValid()) saveLinear(gmds, experiment, pulse);
		if(isCubicValid()) saveKnots(gmds, experiment, pulse);
	}
	
	public void loadLinear(GMDSFetcher gmds, String experiment, int pulse){
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/linearXYtoLL");		
		GMDSSignal sig = (GMDSSignal)gmds.getSig(sigDesc);
		linXYtoLS = (double[][])sig.getData();

		linLStoXY = Mat.inv3x3(linXYtoLS);		
	}

	public void loadKnots(GMDSFetcher gmds, String experiment, int pulse){
		pWavelen = (double[])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsWavelength"))).getData();
		pSpatial = (double[])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsSpatial"))).getData();
		
		double kX[][] = (double[][])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsX"))).getData();
		double kY[][] = (double[][])((GMDSSignal)gmds.getSig(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsY"))).getData();
		
		interpX = new Interpolation2D(pWavelen, pSpatial, kX, InterpolationMode.CUBIC);
		interpY = new Interpolation2D(pWavelen, pSpatial, kY, InterpolationMode.CUBIC);
	}
	
	public void saveLinear(GMDSFetcher gmds, String experiment, int pulse){
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "SpecCal/linearXYtoLS"), linXYtoLS));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "SpecCal/linearLStoXY"), linLStoXY));				
	}

	public void saveKnots(GMDSFetcher gmds, String experiment, int pulse){
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsWavelength"), pWavelen));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsSpatial"), pSpatial));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsX"), interpX.getF()));
		gmds.writeToCache(new GMDSSignal(new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsY"), interpY.getF()));				
	}
	
	public void loadLinear(Map<String, Object> metaData){
		linXYtoLS = (double[][])metaData.get("/SpecCal/linearXYtoLL");
		linLStoXY = Mat.inv3x3(linXYtoLS);		
	}
	
	public void loadKnots(Map<String, Object> metaData){
		pWavelen = (double[])metaData.get("/SpecCal/cubicKnotsWavelength");
		pSpatial = (double[])metaData.get("/SpecCal/cubicKnotsSpatial");
		
		double kX[][] = (double[][])metaData.get("/SpecCal/cubicKnotsX");
		double kY[][] = (double[][])metaData.get("/SpecCal/cubicKnotsY");

		if(pWavelen == null || pSpatial == null || kX == null || kY == null){
			System.out.println("No cubic data loaded for SpecCalFitCubic");
			interpX = null;
			interpY = null;
			return;
		}
		
		interpX = new Interpolation2D(getKnotsWavelength(), getKnotsSpatial(), kX, InterpolationMode.CUBIC);
		interpY = new Interpolation2D(getKnotsWavelength(), getKnotsSpatial(), kY, InterpolationMode.CUBIC);
	}	

	@Override
	public double[] xyToLS(double X, double Y) {
		return xyToLS(X, Y, Double.NaN, Double.NaN);
	}
	
	/** Find XY of a given LL, this is numeric, quite slow and surprisingly bad */
	public double[] xyToLS(double X, double Y, double initA, double initB) {
		if(Double.isNaN(initA) || interpX == null){
			initA = linXYtoLS[0][0]*X + linXYtoLS[0][1]*Y + linXYtoLS[0][2];// + RandomManager.instance().nextUniform(-0.05, 0.05);
			initB = linXYtoLS[1][0]*X + linXYtoLS[1][1]*Y + linXYtoLS[1][2];// + RandomManager.instance().nextUniform(-0.05, 0.05);
		
			if(interpX == null){
				return new double[]{ initA, initB };
			}
		}
		
		if(Double.isNaN(interpX.eval(initA, initB)) ||
				Double.isNaN(interpY.eval(initA, initB)))
					return new double[]{ Double.NaN, Double.NaN };
		
		final double targX = X, targY = Y;
		Function costF = new FunctionND() {
			@Override
			public double eval(double ll[]) {
				double X = interpX.eval(ll[0], ll[1]);
				double Y = interpY.eval(ll[0], ll[1]);
				
				return (X - targX)*(X - targX) + (Y - targY)*(Y - targY); 
			}
			public IDomain getDomain() { 
				return new RectangularDomain(
					new double[]{ 0, 0 }, 
					new double[]{ 1, 1 }); 
			}			
		};
		HookeAndJeeves opt = new HookeAndJeeves(costF);
		/*ConjugateGradientDirectionFR cg = new ConjugateGradientDirectionFR();
		CoordinateDescentDirection cd = new CoordinateDescentDirection();
		//GoldenSection ls = new GoldenSection(new MaxIterCondition(500));
		NewtonsMethod1D ls = new NewtonsMethod1D(new MaxIterCondition(50));
				
		LineSearchOptimizer opt = new LineSearchOptimizer(null, cd, ls);//*/
		opt.setObjectiveFunction(costF);
		
		double maxDistSq = 0.0001 * 0.0001;
		
		opt.init(new double[]{ initA, initB });
		
		int n=0;
		while(opt.getCurrentValue() > maxDistSq && n < 1000){ // NaN value (out of grid) will fall out of the loop here too
			opt.refine();
			//double pos[] = opt.getCurrentPos();
			//System.out.println(pos[0] + "\t" + pos[1] + "\t" + opt.getCurrentValue());
			n++;
		}
		
		double pos[] = opt.getCurrentPos();
		if(Double.isNaN(interpX.eval(pos[0], pos[1])) ||
			Double.isNaN(interpY.eval(pos[0], pos[1])))
				return new double[]{ Double.NaN, Double.NaN };
				
		//System.out.println("XY-->LS in " + n + "steps");
		
		return pos;
	}
			
	public void initCubic(int nWavelen, int nSpatial) {
		if(pWavelen == null || nWavelen != pWavelen.length || nSpatial != pSpatial.length){			
			pWavelen = Mat.linspace(minWavelen, maxWavelen, nWavelen);
			pSpatial = Mat.linspace(minSpatial, maxSpatial, nSpatial);
			interpX = new Interpolation2D(pWavelen, pSpatial, new double[nWavelen][nSpatial], InterpolationMode.CUBIC);
			interpY = new Interpolation2D(pWavelen, pSpatial, new double[nWavelen][nSpatial], InterpolationMode.CUBIC);			
		}		
	}
	
	/** Use linear transform of the four corners in XY to calc range of LL */
	public void calcLLRanges(PhysicalROIGeometry ccdGeom){
		//we want the corners of the image, not of the 'full frame'
		
		//and for ranges, start with X matrix of the four corners of the image
		double CA[] = new double[]{
				linXYtoLS[0][0]*ccdGeom.getImgPhysX0() + linXYtoLS[0][1]*ccdGeom.getImgPhysY0() + linXYtoLS[0][2], //top left
				linXYtoLS[0][0]*ccdGeom.getImgPhysX1() + linXYtoLS[0][1]*ccdGeom.getImgPhysY0() + linXYtoLS[0][2], //top right
				linXYtoLS[0][0]*ccdGeom.getImgPhysX0() + linXYtoLS[0][1]*ccdGeom.getImgPhysY1() + linXYtoLS[0][2], //bot left
				linXYtoLS[0][0]*ccdGeom.getImgPhysX1() + linXYtoLS[0][1]*ccdGeom.getImgPhysY1() + linXYtoLS[0][2], //bot right
		};
		double CB[] = new double[]{
				linXYtoLS[1][0]*ccdGeom.getImgPhysX0() + linXYtoLS[1][1]*ccdGeom.getImgPhysY0() + linXYtoLS[1][2], //top left
				linXYtoLS[1][0]*ccdGeom.getImgPhysX1() + linXYtoLS[1][1]*ccdGeom.getImgPhysY0() + linXYtoLS[1][2], //top right
				linXYtoLS[1][0]*ccdGeom.getImgPhysX0() + linXYtoLS[1][1]*ccdGeom.getImgPhysY1() + linXYtoLS[1][2], //bot left
				linXYtoLS[1][0]*ccdGeom.getImgPhysX1() + linXYtoLS[1][1]*ccdGeom.getImgPhysY1() + linXYtoLS[1][2], //bot right
		};
		minWavelen = Math.min(Math.min(CA[0], CA[1]), Math.min(CA[2], CA[3]));
		maxWavelen = Math.max(Math.max(CA[0], CA[1]), Math.max(CA[2], CA[3]));
		minSpatial = Math.min(Math.min(CB[0], CB[1]), Math.min(CB[2], CB[3]));
		maxSpatial = Math.max(Math.max(CB[0], CB[1]), Math.max(CB[2], CB[3]));
	
		this.pWavelen = null;
		this.pSpatial = null;
		this.interpX = null;
		this.interpY = null;
	}
	
	/** Use the 3 MODE_FIT_AND_MATRIX points to calculate the approx linear transform between XY and LL and vice versa */
	public void calcLinear() {	
			double linPointsXY[][] = new double[3][3];
			double linPointsLL[][] = new double[3][3];
			
			int nLinearFit = 0;
			for(SpecCalPoint p : points){				
				if(p.mode == SpecCalPoint.MODE_FIT_AND_MATRIX){ //use for initial
					linPointsXY[0][nLinearFit] = p.ccdX;
					linPointsXY[1][nLinearFit] = p.ccdY;
					linPointsXY[2][nLinearFit] = 1;
					linPointsLL[0][nLinearFit] = p.wavelength;
					linPointsLL[1][nLinearFit] = p.spatial;
					linPointsLL[2][nLinearFit] = 1;
					nLinearFit++;
				}
				
				if(nLinearFit >= 3)
					break;
			}
			
			if(nLinearFit < 3){
				throw new IllegalArgumentException("Less than 3 points available for linear transform inversion");					
			}
			
			// calc initial linear trasnform
			double invX[][] = Mat.inv3x3(linPointsXY);
			linXYtoLS = Mat.mul3x3(linPointsLL, invX);
			linLStoXY = Mat.inv3x3(linXYtoLS);
			
	}

	public boolean isLinearValid() { return (linLStoXY != null); }
	public boolean isCubicValid() { return (interpX != null); }
	
	@Override
	public boolean isValid() { return isLinearValid(); }
	
	@Override
	public final double[] lsToXY(double lon, double lat){
		return isCubicValid() 
				? new double[]{ interpX.eval(lon, lat), interpY.eval(lon, lat) } 
				: lsToXYLinear(lon, lat);
	}
	
	public final double[][] lsToXY(double lon[], double lat[]){
		return isCubicValid() 
				? new double[][]{ interpX.eval(lon, lat), interpY.eval(lon, lat) } 
				: lsToXYLinear(lon, lat);
	}
	
	public double[] lsToXYLinear(double lon, double lat) {
		return new double[]{  
				linLStoXY[0][0] * lon + linLStoXY[0][1] * lat + linLStoXY[0][2],
				linLStoXY[1][0] * lon + linLStoXY[1][1] * lat + linLStoXY[1][2]						
			};
	}
	
	public double[][] lsToXYLinear(double lon[], double lat[]) {
		double xy[][] = new double[2][lon.length];
		for(int i=0; i < lon.length; i++){  
				xy[0][i] = linLStoXY[0][0] * lon[i] + linLStoXY[0][1] * lon[i] + linLStoXY[0][2];
				xy[1][i] = linLStoXY[1][0] * lat[i] + linLStoXY[1][1] * lat[i] + linLStoXY[1][2];						
		}
		return xy;
	}	

	public double[][] getXYtoLSMat() { return linXYtoLS; }
	public double[][] getLStoXYMat() { return linLStoXY; }

	public double[][] getKnotValsX() { return (interpX != null) ? interpX.getF() : null; }; 
	public double[][] getKnotValsY() { return (interpY != null) ? interpY.getF() : null; }
	
	public void setKnotValsX(double knotValsX[][]){ interpX.setF(knotValsX); }
	public void setKnotValsY(double knotValsY[][]){ interpY.setF(knotValsY); }
	
	public double[] getKnotsWavelength() { return pWavelen; }
	public double[] getKnotsSpatial() { return pSpatial; } 	

	public int getNKnotsWavelen(){ return pWavelen.length; }
	public int getNKnotsSpatial(){ return pSpatial.length; }
	
	public void initCubicToLinear() {
		
		double pX[][] = new double[pWavelen.length][pSpatial.length];
		double pY[][] = new double[pWavelen.length][pSpatial.length];
		
		for(int iB = 0; iB < pSpatial.length; iB++) {
			for(int iA = 0; iA < pWavelen.length; iA++) {				
				pX[iA][iB] = linLStoXY[0][0]*pWavelen[iA] + linLStoXY[0][1]*pSpatial[iB] + linLStoXY[0][2];
				pY[iA][iB] = linLStoXY[1][0]*pWavelen[iA] + linLStoXY[1][1]*pSpatial[iB] + linLStoXY[1][2];				
			}				
		}
		
		interpX.setF(pX);
		interpY.setF(pY);
		
	}

	public void invalidateCubic() { interpX = null; interpY = null; }

	
}
