package imageProc.proc.specCal2D;

import java.util.ArrayList;
import java.util.Map;

import descriptors.gmds.GMDSSignalDesc;
import mds.GMDSFetcher;
import signals.gmds.GMDSSignal;

/** Spectral calibration of a 1/2D spectrometer image.
 * 
 * Made up of points identified as particular spectral lines and channels.
 * For single line spectrometers, or where the CCD is alignhed and binned to give
 * one row per channel, the channel number is not fit or translated.
 * 
 * 
 * @author oliford
 *
 */
public abstract class SpecCalFit {
		
	/** Validity/fitting range of calibration */ 
	public double minWavelen, maxWavelen, minSpatial, maxSpatial;
	
	/** If true, channel is just the row number and not fit or translated */
	public boolean channelsAreRows;

	protected ArrayList<SpecCalPoint> points = new ArrayList<SpecCalPoint>();
	
	public SpecCalFit(){		
	}
	
	/** Construct from metaData signals */
	public SpecCalFit(GMDSFetcher gmds, String experiment, int pulse) {
		loadPoints(gmds, experiment, pulse);				
	}
		
	public ArrayList<SpecCalPoint> getPoints(){ return points; }	
	public SpecCalPoint getPointByName(String name){
		synchronized (points) {
			for(SpecCalPoint p : points){
				if(name.equals(p.name))
					return p;					
			}
		}
		return null;
	}
	
	public void saveToSignal(GMDSFetcher gmds, String experiment, int pulse){
		savePoints(gmds, experiment, pulse);
	}
		
	public void savePoints(GMDSFetcher gmds, String experiment, int pulse){
		String pointNames[] = new String[points.size()];
		double pointData[][] = new double[points.size()][];
		String pointSourceIDs[] = new String[points.size()];
		
		int i=0;
		synchronized (points) {
			for(SpecCalPoint p : points){
				pointNames[i] = p.name;
				pointData[i] = p.toArray();
				pointSourceIDs[i] = p.calSourceID;
				
				i++;
			}
		}
			
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/pointNames");
		GMDSSignal sig = new GMDSSignal(sigDesc, pointNames);
		gmds.writeToCache(sig);

		sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/pointData");
		sig = new GMDSSignal(sigDesc, pointData);
		gmds.writeToCache(sig);
		

		sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/pointSourceIDs");
		sig = new GMDSSignal(sigDesc, pointSourceIDs);
		gmds.writeToCache(sig);
	
	}
	
	/** Construct from metaData  */
	public SpecCalFit(Map<String, Object> metaData) {
		loadPoints(metaData);				
	}
	
	public void loadPoints(GMDSFetcher gmds, String experiment, int pulse){
		
		//try reading from this specific pulse first
		String pointNames[] = null;
		double pointData[][] = null;
		String pointSourceIDs[] = null;
		
		GMDSSignal sig;
		GMDSSignalDesc sigDesc;
			
		sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/pointData");		
		sig = (GMDSSignal)gmds.getSig(sigDesc);
		pointData = (double[][])sig.get2DData();
			
		sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/pointNames");		
		sig = (GMDSSignal)gmds.getSig(sigDesc);
		pointNames = (String[])sig.get1DData();
				
		sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/pointSourceIDs");		
		sig = (GMDSSignal)gmds.getSig(sigDesc);
		pointSourceIDs = (String[])sig.get1DData();
		
		loadPoints(pointNames, pointData, pointSourceIDs);
	}
		
	public void loadPoints(Map<String, Object> metaData){
		String pointNames[] = (String[]) metaData.get("/SpecCal/pointNames");
		double pointData[][] = (double[][]) metaData.get("/SpecCal/pointData");
		String pointSourceIDs[] = (String[]) metaData.get("/SpecCal/pointSourceIDs");
		
		loadPoints(pointNames, pointData, pointSourceIDs);
	}
		
	private void loadPoints(String pointNames[], double pointData[][], String pointSourceIDs[]){
		synchronized (points) {
			points.clear();
			
						
			for(int i=0; i < pointNames.length; i++){
				points.add(new SpecCalPoint(pointNames[i], pointData[i], pointSourceIDs[i]));
			}
		}
	}
	
		
	public abstract double[] lsToXY(double lat, double Lon);
	public abstract double[] xyToLS(double x, double y);
	public abstract boolean isValid();


	public final double getMinWavelength() { return minWavelen; }
	public final double getMaxWavelength() { return maxWavelen; }
	public final double getMinSpatial() { return minSpatial; }
	public final double getMaxSpatial() { return maxSpatial; }
	
}
