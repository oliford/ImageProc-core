package imageProc.proc.specCal2D;

import java.lang.reflect.Array;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.Interpolation2D;
import algorithmrepository.InterpolationMode;
import descriptors.gmds.GMDSSignalDesc;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import imageProc.graph.GraphUtilProcessor;
import imageProc.graph.shapeFit.FuncFitter;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.gmds.GMDSSignal;

/** SpecCal processor.
 * 
 * Deals with the fitting of a SpecCalFit to a images and generating the 
 * 1D/2D wavelength and channel number.
 * 
 *  See {@link SpecCalFit} for details.
 * 
 * @author oliford
 */
public class SpecCalProcessor extends ImgProcPipeMultithread implements ConfigurableByID {

	/** The fit - holds the data of the points to fit, and the fit result */
	private SpecCalFitCubic xform = new SpecCalFitCubic();
	
	/** Holds the transform between physical CCD coordinates and the image (via ROI settings and CCD properties etc) */
	private PhysicalROIGeometry ccdGeom;
	
	/** The fitter that does the work */
	private SpecCalFitter fitter;
			
	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
	
	/** Image L,S coordinates (corners are not necessarily the same as the transform corners) */
	private double L0, S0, L1, S1, dL, dS;
	
	private int cubicNX = 2, cubicNY = 2;
	
	/** Unused points, converted from R,Z back to image X,Y */
	private HashMap<SpecCalPoint, double[]> backConvertedXY;
		
	/** Actually fit the linear/cubic transform to the stored points. If false, it is just used as it was */
	boolean doFit = true;
	
	//Dimensions of the cubic interpolators outputImageCCD{XY}
	// This should be quite a lot, because the edges are NaN and anything in a grid cell
	// touching an edge will also be NaN (but really shouldn't) However doing it for every pixel 
	// is just silly
	private int outputImageCCDConversionGridSizeX = 102; 
	private int outputImageCCDConversionGridSizeY = 98;
	/** Physical XY coords on the CCD of a given pixel on the output image [beamIdx](ioX, ioY) */
	private Interpolation2D outputImageCCDX, outputImageCCDY;

	private boolean includeFullImage;

	private String lastLoadedConfig;
		
	public SpecCalProcessor() { 
		super(ByteBufferImage.class);
		//bulkAlloc.setMaxMemoryBufferAlloc(Long.MIN_VALUE);
	}
	
	public SpecCalProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);	
		//bulkAlloc.setMaxMemoryBufferAlloc(Long.MIN_VALUE);
	}
	
	@Override
	protected int[] sourceIndices(int outIdx) {		
		return new int[]{ outIdx }; //always 1:1
	}
	
	@Override
	protected void preCalc(boolean settingsHadChanged) {
		super.preCalc(settingsHadChanged);
		
		updateAllControllers();		
	}
		
	/** Image allocation */
	protected boolean checkOutputSet(int nImagesIn){
		int nImagesOut;
		
		if(connectedSource == null || nImagesIn <= 0){
			System.err.println("SpecCalProcessor: No source, or no images");
			nImagesOut = 0;
			if(bulkAlloc != null) //Should never be null, but sometimes is
				bulkAlloc.destroy();
			return true;
		}
		
		//need to do the transform now, so we know how big the output images have to be
		try{
			status = "Calculating transform";
			updateAllControllers();
			
			prepTransform();
			if(!xform.isValid())
				throw new RuntimeException("Transform invalid");
						
			status = "Calculating image coordinates";
			updateAllControllers();
			
			calcOutputImageAxes(includeFullImage);
			calcOutputPixelXY(); //can take ages, also calculates the 'A factors'
			if(abortCalc){
				nImagesOut = 0;
				bulkAlloc.destroy();				
				return true;
			}
			
			nImagesOut = nImagesIn;
		}catch(RuntimeException err){			
			nImagesOut = 0;
			bulkAlloc.destroy();
			imagesOut = new ByteBufferImage[0];
			RuntimeException err2 = new RuntimeException("Error in transform init/fit: " + err);
			err2.initCause(err);
			throw(err2);
		}		
		
		
		Img anImage = null;
		for(int i=0; i < connectedSource.getNumImages(); i++){
			anImage = connectedSource.getImage(i);
			if(anImage != null)
				break;
		}
		if(anImage == null)
			throw new RuntimeException("No input images");
		int nFields = anImage.getNFields();				
		
		if(outWidth == 0 || outHeight == 0)
			throw new RuntimeException("Invalid output image size " + outWidth + " x " + outHeight);
		
		 //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, nFields, ByteBufferImage.DEPTH_DOUBLE, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
	    status = "Allocating";
		updateAllControllers();
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }          
        return false;
	}
	
	@Override
	protected boolean doCalc(Img imageOutG, WriteLock writeLockOut, Img[] sourceSet, boolean settingsHadChanged) throws InterruptedException {
		Img imageIn = sourceSet[0];		
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		
		if(outputImageCCDX == null){
			throw new RuntimeException("No outputImage coordinates");
		}
		
		try{
			ReadLock readLockIn = imageIn.readLock();
			readLockIn.lockInterruptibly();
			try{
				for(int oY=0; oY < outHeight; oY++) {
					for(int oX=0; oX < outWidth; oX++) {						
						//get physical coords on CCD corresponding to this pixel on the transformed image
						double physX = outputImageCCDX.eval((double)oX/outWidth, (double)oY/outHeight);
						double physY = outputImageCCDY.eval((double)oX/outWidth, (double)oY/outHeight);
						
						//convert to input image coordinate
						double fX = (physX - ccdGeom.getImgPhysX0()) * inWidth / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
						double fY = (physY - ccdGeom.getImgPhysY0()) * inHeight / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
					
						if(fX < 0 || fX >= (inWidth-2) ||
							fY < 0 || fY >= (inHeight-2) ){
								//out of limits
							for(int iF=0;iF < imageIn.getNFields(); iF++)
								imageOut.setPixelValue(writeLockOut, iF, oX, oY, Double.NaN);
							continue;
						}
						
						int iiX = (int)fX;
						int iiY = (int)fY;
						
						fX -= iiX;
						fY -= iiY;
						
						if(fX < 0 || fY < 0 || fX >= 1 || fY >= 1){
							System.err.println("SpecCalProcessor.doCalc(): Interpolation out of range.");
						}
						
						for(int iF=0;iF < imageIn.getNFields(); iF++){
							double val = 	(1 - fX) * (1 - fY) * imageIn.getPixelValue(readLockIn, iF, iiX,   iiY) + 
										         fX  * (1 - fY) * imageIn.getPixelValue(readLockIn, iF, iiX+1, iiY) + 
											(1 - fX) *      fY  * imageIn.getPixelValue(readLockIn, iF, iiX,   iiY+1) + 
										         fX  *      fY  * imageIn.getPixelValue(readLockIn, iF, iiX+1, iiY+1) ;
							
							imageOut.setPixelValue(writeLockOut, iF, oX, oY, val);
						}
					}
				}
			}finally{
				readLockIn.unlock();
			}
			
		}catch(InterruptedException err){ }
		
		return true;
	}
	
	
	/** Create the linear and non-linear mapping of physical CCD position to wavelen / spatial
	 * (and save that info to the INPUT image metadata) */
	private void prepTransform() {
		
		//have to do these anyway. We still need to deal with ROIs etc even if we're not fitting
		calcCCDGeom();			//work out the corners of the image in physical CCD space, from the camera MetaData
				
		if(doFit){
			xform.calcLinear();			//Calculate the linear (imgX,imgY) <--> (lon,lat) tranform
			xform.calcLLRanges(ccdGeom);		//Calculate range of transform for the input image
		}
		
		//put these in source metadata list so that any saving of the transform also stores the transform used to generate it
		//this stuff is relevant for the input image, so save it to our source (output relevant stuff is saved here downwards)
		connectedSource.setSeriesMetaData("/SpecCal/linearXYtoLS", xform.getXYtoLSMat(), false);
		connectedSource.setSeriesMetaData("/SpecCal/linearLStoXY", xform.getLStoXYMat(), false);
				
		if(cubicNX >= 1){
			xform.initCubic(cubicNX, cubicNY);
			
			fitter = new SpecCalFitter(xform);
			fitter.fit();
			
			connectedSource.setSeriesMetaData("/SpecCal/cubicKnotsWavelength", xform.getKnotsWavelength(), false);
			connectedSource.setSeriesMetaData("/SpecCal/cubicKnotsSpatial", xform.getKnotsSpatial(), false);
			connectedSource.setSeriesMetaData("/SpecCal/cubicKnotsX", xform.getKnotValsX(), false);
			connectedSource.setSeriesMetaData("/SpecCal/cubicKnotsY", xform.getKnotValsY(), false);			
		}
		
		List<SpecCalPoint> points = xform.getPoints();
		synchronized (points) {
			backConvertedXY = new HashMap<SpecCalPoint, double[]>();
			for(SpecCalPoint p : points){
				if(p.mode != SpecCalPoint.MODE_IGNORE){		
					double backXY[] = xform.lsToXY(p.wavelength, p.spatial);
					double AB2[] = xform.xyToLS(backXY[0], backXY[1]);					
					double lin[] = xform.lsToXYLinear(p.wavelength, p.spatial);
					System.out.println(p.name + ": wavelen: " + p.wavelength + " --> x="+backXY[0]+" --> wavelen="+AB2[0] + ", linX = " + lin[0]);
					System.out.println("spatial: " + p.spatial + " --> y="+backXY[1]+" --> spatial="+AB2[1] + ", linY = " + lin[1]);
					backConvertedXY.put(p, backXY);
				}			
			}
		
			// we may have changed the R,Z data
			//saveMapPoints();
			
			String pointNames[] = new String[points.size()];
			double pointData[][] = new double[points.size()][];		
			int i=0;
			for(SpecCalPoint p : points){
				pointNames[i] = p.name;
				pointData[i] = p.toArray();
				i++;
			}		
			connectedSource.setSeriesMetaData("/SpecCal/pointNames", pointNames, false);
			connectedSource.setSeriesMetaData("/SpecCal/pointData", pointData, false);
			connectedSource.addNonTimeSeriesMetaDataMap(ccdGeom.toMap());	
		}	
	}
	
	/** Calculate the physical X,Y on the CCD of each output image pixel */	
	private void calcOutputPixelXY(){
		//ConversionGrid: 
		// these will be out grid to the outputImageCCD{X/Y} interpolators
		//  they are fractional position in the transformed image space (oX,oY)
		double cgX[] = Mat.linspace(0.0, 1.0, outputImageCCDConversionGridSizeX);
		double cgY[] = Mat.linspace(0.0, 1.0, outputImageCCDConversionGridSizeY);
		
		//linear output coords on conversion grid
		double cgA[] = new double[cgX.length];
 		double cgB[] = new double[cgY.length];
 		for(int iX=0; iX < cgX.length; iX++)
			cgA[iX] = L0 + cgX[iX]*outWidth * dL;
 		for(int iY=0; iY < cgY.length; iY++)
 			cgB[iY] = S1 - cgY[iY]*outHeight * dS;
			
 		//If output coords are just lat/lon of camera, this is easy
		
		double gridCCDX[][] = new double[outputImageCCDConversionGridSizeX][outputImageCCDConversionGridSizeY];
		double gridCCDY[][] = new double[outputImageCCDConversionGridSizeX][outputImageCCDConversionGridSizeY];
		
		for(int iY=0; iY < cgY.length; iY++){
			for(int iX=0; iX < cgX.length; iX++){
				double A = L0 + cgX[iX]*outWidth * dL;
				double B = S1 - cgY[iY]*outHeight * dS;
				
		        double fXY[] = xform.lsToXY(A, B);					
		        gridCCDX[iX][iY] = fXY[0];
		        gridCCDY[iX][iY] = fXY[1];
			}
		}
		outputImageCCDX = new Interpolation2D(cgX, cgY, gridCCDX, InterpolationMode.CUBIC);
		outputImageCCDY = new Interpolation2D(cgX, cgY, gridCCDY, InterpolationMode.CUBIC);
		
		setSeriesMetaData("SpecCal/convGridWavelength", cgA, false);
		setSeriesMetaData("SpecCal/convGridSpatial", cgB, false);		
	
	}
	
	
	/** Calculates the min/max of the output image space and then divides
	 * that up to make a 'reasonable' output image roughly the same
	 * resolution as the input one.
	 * AB here might be LL, or RZ if a beam is selected.
	 * @param full If true create an image out to the corners of the transformed original image (can be very large if tranform is very bendy)
	 * 				otherwise, uses min/max lat/lon or R,Z of points
	 */
	private void calcOutputImageAxes(boolean full){
		//def A,B of a pixel as A,B in top left corner of that pixel
		//double dAB = 0.001; //desired size of pixel
		//dA = dAB; //force isometric
		//dB = dAB;
		double pixelRatio = 1.5;
		
		double wavelen0,spatial0,wavelen1,spatial1;
		if(full){
			//use transform corners as a starting point
			//R1,Z1 is coord of top left corner of the top right pixel  
			wavelen0 = xform.getMinWavelength();
			wavelen1 = xform.getMaxWavelength(); 
			spatial0 = xform.getMinSpatial();
			spatial1 = xform.getMaxSpatial();
		}else{
			wavelen0 = Double.POSITIVE_INFINITY;
			wavelen1 = Double.NEGATIVE_INFINITY;
			spatial0 = Double.POSITIVE_INFINITY;
			spatial1 = Double.NEGATIVE_INFINITY;
			
			ArrayList<SpecCalPoint> points = xform.getPoints();
			synchronized (points) {
				for(SpecCalPoint point : points){
					if(!point.includeInFit())
						continue;
					if(point.wavelength < wavelen0) wavelen0 = point.wavelength;
					if(point.wavelength > wavelen1) wavelen1 = point.wavelength;
					if(point.spatial < spatial0) spatial0 = point.spatial;
					if(point.spatial > spatial1) spatial1 = point.spatial;
				}
			}
			
			double rangeLon = wavelen1 - wavelen0;
			wavelen0 -= rangeLon / 4;
			wavelen1 += rangeLon / 4;
			double rangeLat = spatial1 - spatial0;
			spatial0 -= rangeLat / 4;
			spatial1 += rangeLat / 4;
		} 
		
		L0 = wavelen0; L1 = wavelen1;
		S0 = spatial0; S1 = spatial1;
	
		//we want roughly pixelRatio pixels in output image for each pixel in input image
		//output range = (A1 - A0)
		//input range = inWidth
		dL = (L1 - L0) / (inWidth*pixelRatio);
		dS = (S1 - S0) / (inHeight*pixelRatio);
		dL = (dL + dS) / 2; //average and make isometric
		dS = dL;
		
		//image size ought to be 32-bit aligned, for some reason
		outWidth = 4*(int)(((L1 - L0) / dL + 1) / 4);
		outHeight = 4*(int)(((S1 - S0) / dS + 1) / 4);
		L1 = L0 + (outWidth-1) * dL;
		S1 = S0 + (outHeight-1) * dS;
		
		if(Double.isNaN(L0) || Double.isNaN(L1) || Double.isNaN(S0) || Double.isNaN(S1)){
			throw new RuntimeException("SpecCalFitProcessor:  ERROR: Output image space is Nan");
		}
		
		//make axes and write that to the metadata
		double A[] = new double[outWidth];
		for(int i=0; i < outWidth; i++){
			A[i] = L0 + i*dL;
		}
		double B[] = new double[outHeight];
		for(int i=0; i < outHeight; i++){
			B[i] = S1 - i*dS;
		}
		
		//all of these are only relevant for the output image
		setSeriesMetaData("/SpecCal/imageOutWavelength", A, false);
		setSeriesMetaData("/SpecCal/imageOutSpatial", B, false);	
	}
	

	@Override
	public void notifySourceChanged() {
		super.notifySourceChanged();
		if(autoCalc)
			calc();
	}

	@Override
	public void imageChangedRangeComplete(int idx) { 
 		if(autoCalc)
			calc();
 	}
	
	@Override
	public int getNumImages() {
		return (imagesOut != null) ? imagesOut.length : 0;
	}

	@Override
	public Img getImage(int imgIdx) {
		return (imgIdx >= 0 && imgIdx < imagesOut.length) ? imagesOut[imgIdx] : null;
	}
	
	@Override
	public Img[] getImageSet() { return imagesOut; }

	@Override
	public SpecCalProcessor clone() { return new SpecCalProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			SpecCalSWTController controller = new SpecCalSWTController((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public double getA0(){ return L0; }
	public double getA1(){ return L1; }
	public double getB0(){ return S0; }
	public double getB1(){ return S1; }
		
	public void setAutoUpdate(boolean autoCalc) {
		if(!this.autoCalc && autoCalc){
			this.autoCalc = true;
			updateAllControllers();
			calc();			
		}else{
			this.autoCalc = autoCalc;
			updateAllControllers();
		}
	}
	public boolean getAutoUpdate() { return this.autoCalc; }

	@Override
	public void setSource(ImgSource source) {
		super.setSource(source);
		if(autoCalc)
			calc();
	}
	
	@Override
	public void destroy() {
	    abortCalc = true;
		super.destroy();
	}
	
	public SpecCalFit getSpecCalFit(){ return xform; }
	
	public void pointsMapModified(){
		settingsChanged = true;
		updateAllControllers();
		if(autoCalc)
			calc();
	}

	@Override
	public void saveConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds")) {
			saveConfig(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfig(String experiment, int pulse){
		if(connectedSource == null) return;
		
		if(experiment ==null)
			experiment = GMDSUtil.getMetaExp(connectedSource);
		if(pulse < 0)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		saveTransformSettings(experiment, pulse);		
	}
	
	public void saveTransformSettings(String experiment, int pulse){
		
		try{
			xform.saveToSignal(GMDSUtil.globalGMDS(), experiment, pulse);
			
		}catch(RuntimeException e){
			System.err.println("SpecCal: Error saving points to exp " + experiment + 
								", pulse " + pulse + " because: " + e);
		}
		
		try{
			ccdGeom.saveToSignal(GMDSUtil.globalGMDS(), experiment, pulse);			
		}catch(RuntimeException e){
			System.err.println("SpecCal: Error saving CCD/ROI geometry to exp " + experiment + 
								", pulse " + pulse + " because: " + e);
		}
	}
	
	@Override
	public void loadConfig(String id){
		String parts[] = id.split("/");
		if(parts[0].equals("gmds"))
			loadConfig(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfig(String experiment, int overridePulse) {		
		if(experiment == null || experiment.length() <= 0){
			experiment = GMDSUtil.getMetaExp(connectedSource);
		}
		
		int pulse ;
		try{
			pulse = overridePulse >= 0 ? overridePulse : GMDSUtil.getMetaDatabaseID(connectedSource);
		}catch(RuntimeException e){
			pulse = 0;
		}
		
		//try reading from this specific pulse first
		try{			
			xform.loadPoints(GMDSUtil.globalGMDS(), experiment, pulse);
			
		}catch(RuntimeException err){
			System.err.println("SpecCal: Couldn't load points for exp " + experiment +
					", pulse " + pulse + " because: "+ err + ".");
		}
		
		try{
			// and load the cublic X/Y grid size, and 'full image' setting
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "SpecCal/cubicKnotsX");		
			GMDSSignal sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(sigDesc);
			double cubicKnotsX[][] = (double[][])sig.getDataAsType(double.class);
			cubicNX = cubicKnotsX.length;
			cubicNY = cubicKnotsX[0].length;
		}catch(RuntimeException e3){
			cubicNX = -1;
			cubicNY = -1;
		}
				
		//load the transform too, in case they want to use it without fitting again
		try{
			xform.loadLinear(GMDSUtil.globalGMDS(), experiment, pulse);
			xform.loadKnots(GMDSUtil.globalGMDS(), experiment, pulse);
			
		}catch(RuntimeException err){
			System.err.println("SpecCal: Couldn't load transform fit for exp " + experiment +
					", pulse " + pulse + " because: "+ err + ". Might still work by re-fitting.");
		}

		updateAllControllers();
		if(autoCalc)
			calc();
		
	}
	
	public HashMap<SpecCalPoint, double[]> getBackConvertedXY(){ return backConvertedXY; }

	public void invalidate() {
		settingsChanged = true;
	}

	public void setCubicInterp(boolean enable, int nKnotsX, int nKnotsY) {
		if(enable){
			cubicNX = nKnotsX;
			cubicNY = nKnotsY;
		}else{
			cubicNX = -1;
			cubicNY = -1;
		}
		settingsChanged = true;
		if(autoCalc)
			calc();		
	}

	public int getCubicNX() { return this.cubicNX; }
	public int getCubicNY() { return this.cubicNY; }
	
	
	/** Work out the corners of the image in physical CCD space, from the camera MetaData */
	private void calcCCDGeom(){
		
		if(ccdGeom == null)
			ccdGeom = new PhysicalROIGeometry();

		//build a simple map containing all the necessary metadata and give it to ccdGeom to work out the CCD and ROI geometry
		
		HashMap<String, Object> cameraMetaData = new HashMap<String, Object>();
		for(String metaName : PhysicalROIGeometry.cameraMetadataNames){
			Object o = connectedSource.getSeriesMetaData(metaName);
			if(o == null)
				o = connectedSource.getSeriesMetaData("/" + metaName);
			
			if(o != null && o.getClass().isArray()){
				o = Array.get(o, 0);
			}
			
			cameraMetaData.put(metaName, o);
		}
		
		ccdGeom.setFromCameraMetadata(cameraMetaData);
		ccdGeom.setImageSize(inWidth, inHeight);
	}
	

	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }

	public PhysicalROIGeometry getCCDGeometry() { return ccdGeom; }

	public void setIncludeFullImage(boolean fullImage) { 
		includeFullImage = fullImage; 
		settingsChanged = true;
		if(autoCalc)
			calc();
	}
	
	public boolean getIncludeFullImage(){
		return includeFullImage;
	}
	
	public boolean getDoFit(){ return doFit; }
	public void setDoFit(boolean enable){ this.doFit = enable; }

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "SpecCal[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}

	/** Set the given point's ccdX/Y according to the fitter of the first graph connected to the source that has a Gaussian fit */
	public void setFromFit(SpecCalPoint point) {
		ImgSource src = getConnectedSource();
		if(src == null) return;
		
		double fitPos = Double.NaN;
		boolean isY = false;
		
		for(ImgSink sink : src.getConnectedSinks()){
			if(sink instanceof GraphUtilProcessor){
				GraphUtilProcessor graph = (GraphUtilProcessor)sink;
				
				if(graph.getScanDir() == GraphUtilProcessor.SCAN_DIR_X){
					isY = false;
				}else if(graph.getScanDir() == GraphUtilProcessor.SCAN_DIR_Y){
					isY = true;
				}else{
					continue;
				}
				
				FuncFitter fitter = graph.getFitter();				
				if(fitter.getFuncType() == FuncFitter.FUNC_GAUSSIAN){
					fitPos = fitter.getFinalParams()[1];  //centre					
					break;
				}				
			}
		}
		
		if(Double.isNaN(fitPos))  //none found, or invalid
			return;
		
		//need to convert to CCD position
		//if(ccdGeom == null)
			calcCCDGeom();
		
		if(!isY){
			point.ccdX = ccdGeom.pixelToPhysX(fitPos);			
		}else{
			point.ccdY = ccdGeom.pixelToPhysY(fitPos);
		}
		
		pointsMapModified();
	}
}
