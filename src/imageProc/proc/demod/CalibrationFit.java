package imageProc.proc.demod;

import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.Interpolation2D;
import algorithmrepository.InterpolationMode;
import descriptors.gmds.GMDSSignalDesc;
import net.jafama.FastMath;
import mds.GMDSFetcher;
import signals.gmds.GMDSSignal;

/** Calibration sweep fit by CalFitScanner (under GraphProcessor)
 * can used by DSH to process the image. 
 * 
 * @author oliford
 */
public class CalibrationFit {
	public Interpolation2D mu; 
	public Interpolation2D offset;
	public Interpolation2D effEllip;
	
	public CalibrationFit() {
	}
	
	public void loadFromSignals(GMDSFetcher gmds, String path) {
		GMDSSignal sig = (GMDSSignal) gmds.getSig(new GMDSSignalDesc(path + "/ccdPhysX"));
		double x[] = (double[])sig.getDataAsType(double.class);
		
		sig = (GMDSSignal) gmds.getSig(new GMDSSignalDesc(path + "/ccdPhysY"));
		double y[] = (double[])sig.getDataAsType(double.class);

		sig = (GMDSSignal) gmds.getSig(new GMDSSignalDesc(path + "/IMAGE_1"));
		double offData[][] = (double[][])sig.getDataAsType(double.class);
		
		sig = (GMDSSignal) gmds.getSig(new GMDSSignalDesc(path + "/IMAGE_2"));
		double muData[][] = (double[][])sig.getDataAsType(double.class);

		sig = (GMDSSignal) gmds.getSig(new GMDSSignalDesc(path + "/IMAGE_3"));
		double ellipData[][] = (double[][])sig.getDataAsType(double.class);
		
		for(int i=0; i < muData.length; i++)
			for(int j=0; j < muData[i].length; j++)
				muData[i][j] = FastMath.abs(muData[i][j]);

		//offset = new CubicInterpolation2D(x, y, Mat.transpose(offData));
		//mu = new CubicInterpolation2D(x, y,  Mat.transpose(muData));
		offset = new Interpolation2D(x, y, offData, InterpolationMode.CUBIC);
		mu = new Interpolation2D(x, y,  muData, InterpolationMode.CUBIC);
		effEllip = new Interpolation2D(x, y, ellipData, InterpolationMode.CUBIC);
	}
}
