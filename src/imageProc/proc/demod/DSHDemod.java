package imageProc.proc.demod;

import java.io.File;
import java.nio.ByteOrder;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import algorithmrepository.exceptions.NotImplementedException;
import binaryMatrixFile.BinaryMatrixFile;
import descriptors.gmds.GMDSSignalDesc;
import edu.emory.mathcs.jtransforms.fft.DoubleFFT_2D;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ComplexImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSUtil;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.Signal;
import signals.gmds.GMDSSignal;

/**
 * The main demodulation code, takes ComplexImage FFT input images and produces
 * real images with a variety of output modes.
 * 
 * All the actual calculations are in DSHCalc, this is mostly just data prep,
 * image handling and switching between different calc modes.
 * 
 * Definitely NOT thread safe!
 * 
 * @author oliford
 */
public class DSHDemod extends ImgProcPipe implements ConfigurableByID {

	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);

	/** Components from incoming FFT */
	public static final int nComps = 5;
	public static final int COMPONENT_00 = 0;
	public static final int COMPONENT_p0 = 1;
	public static final int COMPONENT_pp = 2;
	public static final int COMPONENT_pm = 3;
	public static final int COMPONENT_0p = 4;
	public static final String componentNames[] = new String[] { "(0, 0) Black", "(+, 0) Green", "(+, +) Red",
			"(+, -) Blue", "(0, +) Yellow", };
	/** Component selection boxes from incoming FFT image, set by controller */
	private int selections[][] = null;

	public static final String outputModeNames[] = new String[] { "Amp (0, 0)", "Phase (0, 0)", "Amp (+, 0)",
			"Phase (+, 0)", "Contrast (+,0)/(0,0)", "Amp (+, +)", "Phase (+, +)", "Contrast (+,+)/(0,0)", "Amp (+, -)",
			"Phase (+, -)", "Contrast (+,-)/(0,0)", "Amp (0, +)", "Phase (0, +)", "Contrast (0,+)/(0,0)",
			"Pol 2|+,+| / |+,0|", "Pol 2|+,-| / |+,0|", "Pol 4(+,+)(+,-) / (+,0)��",
			"Pol 4(+,+)(+,-) / (+,0)��, [Roll Over 0 < a < 45deg]", "Pol 4|(+,+)||(+,-)| / |(+,0)|��",
			"Phase 4(+,+)(+,-) / (+,0)��", "Missbalance (+,+) / (+,-)", "Contrast (|(+,+)|+|(+,-)|+|(+,0)|) / |(0,0)|",
			"Zeeman Pol from [(+,-) + (+,+)] / (+,0)", "Zeeman Pol from [(+,-) - (+,+)] / (+,0)",
			"Savart Cmpt Diff (+,+) - (+,-)", "Phase Cmpt Diff (+,+) - (+,-)", };

	/** Output modes, must line up with outputModeNames[] */
	public static final int OUTPUT_MODE_AMP_00 = 0;
	public static final int OUTPUT_MODE_PHS_00 = 1;
	public static final int OUTPUT_MODE_AMP_p0 = 2;
	public static final int OUTPUT_MODE_PHS_p0 = 3;
	public static final int OUTPUT_MODE_CTR_p0 = 4;
	public static final int OUTPUT_MODE_AMP_pp = 5;
	public static final int OUTPUT_MODE_PHS_pp = 6;
	public static final int OUTPUT_MODE_CTR_pp = 7;
	public static final int OUTPUT_MODE_AMP_pm = 8;
	public static final int OUTPUT_MODE_PHS_pm = 9;
	public static final int OUTPUT_MODE_CTR_pm = 10;
	public static final int OUTPUT_MODE_AMP_0p = 11;
	public static final int OUTPUT_MODE_PHS_0p = 12;
	public static final int OUTPUT_MODE_CTR_0p = 13;
	public static final int OUTPUT_MODE_POL_pp_p0 = 14;
	public static final int OUTPUT_MODE_POL_pm_p0 = 15;
	public static final int OUTPUT_MODE_POL_px = 16;
	public static final int OUTPUT_MODE_POL_px_rpj = 17;
	public static final int OUTPUT_MODE_POL_px_abs = 18;
	public static final int OUTPUT_MODE_PHS_px = 19;
	public static final int OUTPUT_MODE_BAL_px = 20;
	public static final int OUTPUT_MODE_CNT_pp = 21;
	public static final int OUTPUT_MODE_ZEM_p = 22;
	public static final int OUTPUT_MODE_ZEM_m = 23;
	public static final int OUTPUT_MODE_AMP_pd = 24;
	public static final int OUTPUT_MODE_PHS_pd = 25;

	/** Primary Output mode, set by controller */
	private int outputMode = OUTPUT_MODE_POL_px;

	/** Input interlacing mode, and what to do with it */
	public static final int INTERLACE_MODE_OUTPUT_ALL = 0;
	public static final int INTERLACE_MODE_OUTPUT_EVEN = 1;
	public static final int INTERLACE_MODE_OUTPUT_ODD = 2;
	public static final int INTERLACE_MODE_PAIR_DIFF = 3;
	public static final int INTERLACE_MODE_PAIR_SUM = 4;
	public static final int INTERLACE_MODE_ELLIP_COMP_A = 5;
	public static final int INTERLACE_MODE_ELLIP_COMP_B = 6;

	public static final String interlaceModeOpts[] = new String[] { "Show all", "Show even", "Show odd", "Phase Diff",
			"Phase Avg", "Ellip Comp. A", // ellipticity compensation
											// polarisation
			"Ellip Comp. B", // experimental ellipticity compensation
								// polarisation
	};

	private int interlaceMode = INTERLACE_MODE_OUTPUT_ALL;

	private boolean unwrapPhases = false;
	private boolean calcSigma = true;

	/** Global intrinsic contrast compensation */
	private double adjustP0 = 1.0, adjustPP = 1.0, adjustPM = 1.0;

	/** Calibration image config */
	private String calibImgPath;
	private boolean calibPathValid = false; // set true when calibImgPath was
											// read successfully
	private int calibImgIdx = -1; // input index of calib image
	private double calibImgAng = 22.5 * Math.PI / 180;

	/** Calib state */
	public ByteBufferImage calibImage = null; // calib image (meaning depends on
												// mode)
	public CalibrationFit calibFit = null;

	private PhysicalROIGeometry ccdGeom;

	private double cmptCmlpxData0[][];

	/** Only for keeping track of selection saving */
	private int currentPulse = -1;

	public DSHDemod() {
		super(ByteBufferImage.class);
	}

	public DSHDemod(ImgSource source, int selectedIndex) {
		super(ComplexImage.class, source, selectedIndex);
	}

	@Override
	protected void preCalc(boolean settingsHadChanged) {
		super.preCalc(settingsHadChanged);

		String experiment = GMDSUtil.getMetaExp(connectedSource);
		int pulse = GMDSUtil.getMetaDatabaseID(connectedSource);

		// if there's no selections
		if (selections == null) {
			// first, if we have a pulse number, see if there's any in the DB
			try {
				loadConfigGMDS(experiment, pulse);
				currentPulse = pulse;
			} catch (Exception e) {
				System.err.println("DSHDemod: Error loaded selecions from DB: " + e.getMessage());
			}

			if (selections != null) {
				System.out.println("DSHDemod: Loaded selections from GMDS DB");
			} else {
				// try auto selections
				// if possible, do this from the our 'selected' image

				Img selImg = getSelectedImageFromConnectedSource();

				// too slow
				// selections =
				// ComponentsFinder.autoInitSelections((ComplexImage)selImg);
				updateAllControllers();
			}
		}

		if (pulse > 0 && currentPulse != pulse) {
			// if the pulse has changed and there are saved selection in the new
			// pulse, load those
			try {
				loadConfigGMDS(experiment, pulse);
				currentPulse = pulse;
			} catch (Exception e) {
				System.err.println("Pulse changed, but couldn't load selection from new pulse. Keeping old. Err: "
						+ e.getMessage());
			}
			currentPulse = pulse;
		}

		// we might need ccd geom info to apply any calibration
		try {
			ccdGeom = new PhysicalROIGeometry();
			// ccdGeom.fromMap(getCompleteSeriesMetaDataMap());
			ccdGeom.setFromCameraMetadata(getCompleteSeriesMetaDataMap().extractSingleArrayElements());
		} catch (RuntimeException err) {
			System.err.println("Couldn't load ccdGeom data");
			ccdGeom = null;
		}

		// we need to process the calibration image first
		calibFit = null;
		if (calibImgPath != null && calibImgPath.length() > 0) { // preferably
																	// from an
																	// external
																	// path
			loadExternalCalibImage();
			setSeriesMetaData("DSHDemod/appliedCalibration", calibImgPath, false);

		} else {
			Img calibSrcImg = connectedSource.getImage(calibImgIdx);
			if (calibSrcImg != null) { // or from an image in this set
				if (calibImage == null || calibImage.getWidth() != outWidth || calibImage.getHeight() != outHeight) {
					if (calibImage != null)
						calibImage.destroy();

					calibImage = new ByteBufferImage(this, -1, outWidth, outHeight, ByteBufferImage.DEPTH_DOUBLE,
							false);
				}
				attemptCalc(calibImage, new Img[] { calibSrcImg }, settingsHadChanged);
				setSeriesMetaData("DSHDemod/appliedCalibration", "calibImgIdx=" + calibImgIdx, false);
			} else { // otherwise, not
				calibImage = null;
			}
		}

		// if there's a 'time' sequence, apply the interlace selection
		if (interlaceMode != INTERLACE_MODE_OUTPUT_ALL) {
			double time[] = (double[]) connectedSource.getSeriesMetaData("/time");
			if (time != null) {
				double newTime[] = new double[time.length / 2];
				for (int i = 0; i < newTime.length; i++) {
					newTime[i] = (time[i * 2] + time[i * 2 + 1]) / 2;
				}
				setSeriesMetaData("/time", newTime, true);
			}
		}

		cmptCmlpxData0 = null;
	}

	/** Image allocation */
	@Override
	protected boolean checkOutputSet(int nImagesIn) {
		outWidth = inWidth;
		outHeight = inHeight;
		int nImagesOut = (interlaceMode == INTERLACE_MODE_OUTPUT_ALL) ? nImagesIn : (nImagesIn / 2);

		int nFields = calcSigma ? 2 : 1;
		// allocate or re-use
		ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, nFields,
				ByteBufferImage.DEPTH_DOUBLE, false);
		templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);

		if (bulkAlloc.reallocate(templateImage, nImagesOut)) {
			imagesOut = bulkAlloc.getImages();
			return true;
		}
		return false;
	}

	@Override
	protected int[] sourceIndices(int outIdx) {
		switch (interlaceMode) {
		case INTERLACE_MODE_OUTPUT_ALL:
			return new int[] { outIdx };
		case INTERLACE_MODE_OUTPUT_EVEN:
			return new int[] { 2 * outIdx };
		case INTERLACE_MODE_OUTPUT_ODD:
			return new int[] { 2 * outIdx + 1 };
		default:
			return new int[] { 2 * outIdx, 2 * outIdx + 1 };
		}
	}

	/** Reusable temporary image for ellipticity info */
	private ByteBufferImage ellipImage = null;

	private String lastLoadedConfig;

	@Override
	public boolean doCalc(Img imageOutGen, WriteLock wLock, Img sourceSet[], boolean settingsHadChanged)
			throws InterruptedException {
		ByteBufferImage imageOut = (ByteBufferImage) imageOutGen;

		DSHCalc calc = new DSHCalc();
		calc.imageOut = imageOut;
		calc.writeLock = wLock;
		calc.selections = selections;
		calc.calibImage = calibImage;
		calc.calibImgAng = calibImgAng;
		calc.ccdGeom = ccdGeom;
		calc.calibFit = calibFit;
		try {
			calc.noiseInfo = ImageProcUtil.getCameraSensitivityInfo(connectedSource);
		} catch (RuntimeException err) {
			if (calcSigma) {
				throw err;
			} else {
				System.err.println("Invalid camera noise info because: ");
				err.printStackTrace();
				calc.noiseInfo = null;

			}
		}

		// check all source images are ComplexImage
		for (int j = 0; j < sourceSet.length; j++) {
			if (!(sourceSet[j] instanceof ComplexImage))
				throw new IllegalArgumentException(
						"sourceSet[" + j + "] is " + sourceSet[j].toString() + " not a ComplexImage");
		}

		// Deal with the interlaced modes that use both input images first
		if (interlaceMode == INTERLACE_MODE_PAIR_DIFF || interlaceMode == INTERLACE_MODE_PAIR_SUM
				|| interlaceMode == INTERLACE_MODE_ELLIP_COMP_A || interlaceMode == INTERLACE_MODE_ELLIP_COMP_B) {

			double evenCmptData[][] = calcNeededComplexData((ComplexImage) sourceSet[0]);
			double oddCmptData[][] = calcNeededComplexData((ComplexImage) sourceSet[1]);

			// ellipticity compensation modes require normal mode calcs of both
			// images first
			if (interlaceMode == INTERLACE_MODE_ELLIP_COMP_B) {
				calc.cmplxData = null;
				calc.ellipCompensatedThetaB(evenCmptData, oddCmptData);

			} else if (interlaceMode == INTERLACE_MODE_ELLIP_COMP_A) {
				if (ellipImage == null || ellipImage.getWidth() != outWidth || ellipImage.getHeight() != outHeight) {
					if (calibImage != null)
						calibImage.destroy();

					ellipImage = new ByteBufferImage(this, -1, outWidth, outHeight, ByteBufferImage.DEPTH_DOUBLE,
							false);
				}

				WriteLock wLockEllip = ellipImage.writeLock();
				wLockEllip.lockInterruptibly();

				calc.imageOut = ellipImage;
				calc.writeLock = wLock;
				calc.cmplxData = evenCmptData;

				try {
					switch (outputMode) {
					case OUTPUT_MODE_POL_pp_p0:
						calc.polAngSingle(COMPONENT_pp, COMPONENT_p0, false);
						break;
					case OUTPUT_MODE_POL_pm_p0:
						calc.polAngSingle(COMPONENT_pm, COMPONENT_p0, false);
						break;
					case OUTPUT_MODE_POL_px:
						calc.balanced(false, false, false);
						break;
					case OUTPUT_MODE_POL_px_rpj:
						calc.balanced(false, false, true);
						break;
					}
				} finally {
					wLockEllip.unlock();
				}

				calc.imageOut = imageOut;
				calc.writeLock = wLock;
				calc.cmplxData = oddCmptData;

				switch (outputMode) {
				case OUTPUT_MODE_POL_pp_p0:
					calc.polAngSingle(COMPONENT_pp, COMPONENT_p0, false);
					break;
				case OUTPUT_MODE_POL_pm_p0:
					calc.polAngSingle(COMPONENT_pm, COMPONENT_p0, false);
					break;
				case OUTPUT_MODE_POL_px:
					calc.balanced(false, false, false);
					break;
				case OUTPUT_MODE_POL_px_rpj:
					calc.balanced(false, false, true);
					break;
				}

				calc.ellipCompensatedTheta(imageOut, wLock, ellipImage);

			} else { // Phase sum/diff images
				calc.cmplxData = null;
				switch (outputMode) {
				case OUTPUT_MODE_PHS_00:
					calc.phaseDiff(evenCmptData, oddCmptData, COMPONENT_00, interlaceMode, unwrapPhases);
					break;
				case OUTPUT_MODE_PHS_p0:
					calc.phaseDiff(evenCmptData, oddCmptData, COMPONENT_p0, interlaceMode, unwrapPhases);
					break;
				case OUTPUT_MODE_PHS_pp:
					calc.phaseDiff(evenCmptData, oddCmptData, COMPONENT_pp, interlaceMode, unwrapPhases);
					break;
				case OUTPUT_MODE_PHS_pm:
					calc.phaseDiff(evenCmptData, oddCmptData, COMPONENT_pm, interlaceMode, unwrapPhases);
					break;
				case OUTPUT_MODE_PHS_0p:
					calc.phaseDiff(evenCmptData, oddCmptData, COMPONENT_0p, interlaceMode, unwrapPhases);
					break;
				case OUTPUT_MODE_PHS_pd:
					calc.phaseCmptDiffDiff(evenCmptData, oddCmptData, interlaceMode, unwrapPhases);
					break;
				default:
					throw new IllegalArgumentException("Interlaced pair sum/diff only works for phase output modes");
				}
			}

		} else { // a normal 1-1 image to image mode (although we might still
					// have interlaced input)

			calc.cmplxData = calcNeededComplexData((ComplexImage) sourceSet[0]);

			// err... this is bit broken
			if ((outputMode == OUTPUT_MODE_CNT_pp || outputMode == OUTPUT_MODE_CTR_p0
					|| outputMode == OUTPUT_MODE_CTR_pp || outputMode == OUTPUT_MODE_CTR_pm
					|| outputMode == OUTPUT_MODE_CTR_0p) && calibImgIdx >= 0 && imageOut == imagesOut[calibImgIdx]) {
				cmptCmlpxData0 = calc.cmplxData;
			}

			switch (outputMode) {
			case OUTPUT_MODE_POL_pp_p0:
				calc.polAngSingle(COMPONENT_pp, COMPONENT_p0, true);
				break;
			case OUTPUT_MODE_POL_pm_p0:
				calc.polAngSingle(COMPONENT_pm, COMPONENT_p0, true);
				break;
			case OUTPUT_MODE_POL_px_abs:
				calc.balancedAbs(imageOut != calibImage);
				break; // don't finish if producing calib image
			case OUTPUT_MODE_POL_px:
				calc.balanced(false, imageOut != calibImage, false);
				break; // don't finish if producing calib image
			case OUTPUT_MODE_POL_px_rpj:
				calc.balanced(false, imageOut != calibImage, true);
				break; // don't finish if producing calib image
			case OUTPUT_MODE_PHS_px:
				calc.balanced(true, imageOut != calibImage, false);
				break; // don't finish if producing calib image
			case OUTPUT_MODE_BAL_px:
				calc.missbalance();
				break;
			case OUTPUT_MODE_AMP_pd:
				calc.ampCmptDiff(false);
				break;
			case OUTPUT_MODE_PHS_pd:
				calc.phaseCmptDiff(false);
				break;

			case OUTPUT_MODE_ZEM_p:
				calc.zeeman(false, +1, imageOut != calibImage);
				break; // don't finish if producing calib image
			case OUTPUT_MODE_ZEM_m:
				calc.zeeman(false, -1, imageOut != calibImage);
				break; // don't finish if producing calib image

			case OUTPUT_MODE_AMP_00:
				calc.amplitude(COMPONENT_00);
				break;
			case OUTPUT_MODE_PHS_00:
				calc.phase(COMPONENT_00, unwrapPhases);
				break;

			case OUTPUT_MODE_AMP_p0:
				calc.amplitude(COMPONENT_p0);
				break;
			case OUTPUT_MODE_PHS_p0:
				calc.phase(COMPONENT_p0, unwrapPhases);
				break;
			case OUTPUT_MODE_CTR_p0:
				calc.contrast(cmptCmlpxData0, COMPONENT_p0);
				break;

			case OUTPUT_MODE_AMP_pp:
				calc.amplitude(COMPONENT_pp);
				break;
			case OUTPUT_MODE_PHS_pp:
				calc.phase(COMPONENT_pp, unwrapPhases);
				break;
			case OUTPUT_MODE_CTR_pp:
				calc.contrast(cmptCmlpxData0, COMPONENT_pp);
				break;

			case OUTPUT_MODE_AMP_pm:
				calc.amplitude(COMPONENT_pm);
				break;
			case OUTPUT_MODE_PHS_pm:
				calc.phase(COMPONENT_pm, unwrapPhases);
				break;
			case OUTPUT_MODE_CTR_pm:
				calc.contrast(cmptCmlpxData0, COMPONENT_pm);
				break;

			case OUTPUT_MODE_AMP_0p:
				calc.amplitude(COMPONENT_0p);
				break;
			case OUTPUT_MODE_PHS_0p:
				calc.phase(COMPONENT_0p, unwrapPhases);
				break;
			case OUTPUT_MODE_CTR_0p:
				calc.contrast(cmptCmlpxData0, COMPONENT_0p);
				break;

			case OUTPUT_MODE_CNT_pp:
				calc.combinedContrast(cmptCmlpxData0, COMPONENT_pp);
				break;
			}

		}

		return true;
	}

	/**
	 * Work out which components we need for a given calculation mode, and
	 * calculate the inverse FFT'ed complex data
	 */
	private double[][] calcNeededComplexData(ComplexImage imgT) {

		boolean componentsNeeded[];
		switch (outputMode) { // (0,0), (+,0), (+,+), (+,-) , (0,+)
		case OUTPUT_MODE_AMP_00:
			componentsNeeded = new boolean[] { true, false, false, false, false };
			break;
		case OUTPUT_MODE_PHS_00:
			componentsNeeded = new boolean[] { true, false, false, false, false };
			break;
		case OUTPUT_MODE_AMP_p0:
			componentsNeeded = new boolean[] { false, true, false, false, false };
			break;
		case OUTPUT_MODE_PHS_p0:
			componentsNeeded = new boolean[] { false, true, false, false, false };
			break;
		case OUTPUT_MODE_CTR_p0:
			componentsNeeded = new boolean[] { true, true, false, false, false };
			break;
		case OUTPUT_MODE_AMP_pp:
			componentsNeeded = new boolean[] { false, false, true, false, false };
			break;
		case OUTPUT_MODE_PHS_pp:
			componentsNeeded = new boolean[] { false, false, true, false, false };
			break;
		case OUTPUT_MODE_CTR_pp:
			componentsNeeded = new boolean[] { true, false, true, false, false };
			break;
		case OUTPUT_MODE_AMP_pm:
			componentsNeeded = new boolean[] { false, false, false, true, false };
			break;
		case OUTPUT_MODE_PHS_pm:
			componentsNeeded = new boolean[] { false, false, false, true, false };
			break;
		case OUTPUT_MODE_CTR_pm:
			componentsNeeded = new boolean[] { true, false, false, true, false };
			break;
		case OUTPUT_MODE_AMP_0p:
			componentsNeeded = new boolean[] { false, false, false, false, true };
			break;
		case OUTPUT_MODE_PHS_0p:
			componentsNeeded = new boolean[] { false, false, false, false, true };
			break;
		case OUTPUT_MODE_CTR_0p:
			componentsNeeded = new boolean[] { true, false, false, false, true };
			break;
		case OUTPUT_MODE_POL_pp_p0:
			componentsNeeded = new boolean[] { false, true, true, false, false };
			break;
		case OUTPUT_MODE_POL_pm_p0:
			componentsNeeded = new boolean[] { false, true, false, true, false };
			break;
		case OUTPUT_MODE_POL_px:
			componentsNeeded = new boolean[] { false, true, true, true, false };
			break;
		case OUTPUT_MODE_POL_px_rpj:
			componentsNeeded = new boolean[] { false, true, true, true, false };
			break;
		case OUTPUT_MODE_POL_px_abs:
			componentsNeeded = new boolean[] { false, true, true, true, false };
			break;
		case OUTPUT_MODE_PHS_px:
			componentsNeeded = new boolean[] { false, true, true, true, false };
			break;
		case OUTPUT_MODE_BAL_px:
			componentsNeeded = new boolean[] { false, false, true, true, false };
			break;
		case OUTPUT_MODE_CNT_pp:
			componentsNeeded = new boolean[] { true, true, true, true, false };
			break;
		case OUTPUT_MODE_ZEM_p:
			componentsNeeded = new boolean[] { false, true, true, true, false };
			break;
		case OUTPUT_MODE_ZEM_m:
			componentsNeeded = new boolean[] { false, true, true, true, false };
			break;
		case OUTPUT_MODE_AMP_pd:
			componentsNeeded = new boolean[] { false, false, true, true, false };
			break;
		case OUTPUT_MODE_PHS_pd:
			componentsNeeded = new boolean[] { false, false, true, true, false };
			break;
		default:
			throw new NotImplementedException();
		}
		if (calcSigma) { // noise calculations always require the background
							// level
			componentsNeeded[0] = true;
		}

		try {
			ReadLock readLock = imgT.readLock();
			readLock.lockInterruptibly();
			try {
				double cmptCmlpxData[][] = new double[nComps][];

				if (componentsNeeded[COMPONENT_00])
					cmptCmlpxData[COMPONENT_00] = selectComponent(COMPONENT_00, imgT, readLock, 1.0);

				if (componentsNeeded[COMPONENT_p0])
					cmptCmlpxData[COMPONENT_p0] = selectComponent(COMPONENT_p0, imgT, readLock, adjustP0);

				if (componentsNeeded[COMPONENT_pp])
					cmptCmlpxData[COMPONENT_pp] = selectComponent(COMPONENT_pp, imgT, readLock, adjustPP);

				if (componentsNeeded[COMPONENT_pm])
					cmptCmlpxData[COMPONENT_pm] = selectComponent(COMPONENT_pm, imgT, readLock, adjustPM);

				if (componentsNeeded[COMPONENT_0p])
					cmptCmlpxData[COMPONENT_0p] = selectComponent(COMPONENT_0p, imgT, readLock, 1.0);

				return cmptCmlpxData;

			} finally {
				readLock.unlock();
			}
		} catch (InterruptedException err) {
			return null;
		}

	}

	/**
	 * Selects the given component from the FT and inverse FTs it, returning the
	 * complex data array
	 */
	private double[] selectComponent(int sel, ComplexImage imageIn, ReadLock readLock, double adjust) {
		if (imageIn == null || selections == null) {
			return null;
		}

		int sx0 = selections[sel][0], sy0 = selections[sel][1];
		int sw = selections[sel][2], sh = selections[sel][3];
		int sxC = selections[sel][4], syC = selections[sel][5];
		int sx1 = sx0 + sw - 1, sy1 = sy0 + sh - 1;
		int iw = imageIn.getWidth(), ih = imageIn.getHeight();

		if (sx1 >= iw || sy1 >= ih
		// sw <= 0 || sh <= 0 || sxC < sx0 || sxC > sx1 || syC < sy0 || syC >
		// sy1 ||
		) {
			System.err.println("DSHDemod.demod(" + sel + "): Invalid selection / centre freq.");
			return null;
		}

		// pr00,pi00,pr12,pi12...
		// and then rows go [+ve cols, -ve cols, +ve cols ....]
		// and then the block of rows pairs are [ +++++, ------] in y
		double cmplxData[] = new double[iw * ih * 2];

		DoubleFFT_2D fft = new DoubleFFT_2D(ih, iw);

		for (int iy = sy0; iy <= sy1; iy++) {
			for (int ix = sx0; ix <= sx1; ix++) {
				// we need the img coords (ix,iy) in frequency coords (x,y)
				int x = ix - (iw / 2);
				int y = iy - (ih / 2);
				if (x < 0)
					x += iw; // and remeber that the -ve x are on the RHS
				if (y < 0)
					y += ih; // and -ve y at the bottom

				cmplxData[2 * y * iw + x * 2] = adjust * imageIn.getReal(readLock, ix, iy);
				cmplxData[2 * y * iw + x * 2 + 1] = adjust * imageIn.getImag(readLock, ix, iy);
			}
		}

		fft.complexInverse(cmplxData, true);

		return cmplxData;

	}

	public void setWindow(int selection, int x0, int y0, int width, int height) {
		if (selections == null)
			selections = new int[nComps][6];
		selections[selection][0] = x0;
		selections[selection][1] = y0;
		selections[selection][2] = width;
		selections[selection][3] = height;
		selections[selection][4] = x0 + width / 2;
		selections[selection][5] = y0 + height / 2;
		settingsChanged = true;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	public void setCentralFreq(int selection, int x, int y) {
		if (selections == null)
			selections = new int[nComps][6];
		selections[selection][4] = x;
		selections[selection][5] = y;
		settingsChanged = true;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if (interfacingClass == Composite.class) {
			ImagePipeController controller = new DSHDemodSinkSWTController(this, (Composite) args[0], (Integer) args[1],
					asSink);
			controllers.add(controller);
			return controller;
		}

		return null;
	}

	@Override
	public DSHDemod clone() {
		return new DSHDemod(connectedSource, getSelectedSourceIndex());
	}

	public int[][] getSelections() {
		return selections;
	}

	public void setPhaseUnwrap(boolean unwrapPhases) {
		if (this.unwrapPhases == unwrapPhases)
			return;

		this.unwrapPhases = unwrapPhases;
		this.settingsChanged = true;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	public void setCalcSigma(boolean calcSigma) {
		if (this.calcSigma == calcSigma)
			return;

		this.calcSigma = calcSigma;
		this.settingsChanged = true;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	public void setOutputMode(int outputMode) {
		if (this.outputMode == outputMode)
			return;

		this.outputMode = outputMode;
		this.settingsChanged = true;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	public int getOutputMode() {
		return this.outputMode;
	}

	public void setComponentAdjust(double adjustP0, double adjustPP, double adjustPM) {
		if (this.adjustP0 == adjustP0 && this.adjustPP == adjustPP && this.adjustPM == adjustPM)
			return;

		this.adjustP0 = adjustP0;
		this.adjustPP = adjustPP;
		this.adjustPM = adjustPM;
		this.settingsChanged = true;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	public void setInterlaceMode(int interlaceMode) {
		this.interlaceMode = interlaceMode;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	public void setCalibImage(String calibImgPath, int calibImg, double calibImgAng) {
		if (((this.calibImgPath == null && calibImgPath == null)
				|| (this.calibImgPath != null && this.calibImgPath.equals(calibImgPath)))
				&& this.calibImgIdx == calibImg && this.calibImgAng == calibImgAng)
			return;
		this.calibImgPath = calibImgPath;
		this.calibImgAng = calibImgAng;
		this.calibImgIdx = calibImg;
		this.calibPathValid = false;
		this.settingsChanged = true;
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	@Override
	public void loadConfig(String id) {

		if (id.startsWith("gmds")) {
			String parts[] = id.split("/");
			loadConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
		} else if (id.startsWith("jsonfile:")) {
			loadConfigJSON(id.substring(9));
		}else {
			throw new RuntimeException("Unknown config type " + id);
		}
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);

		JsonObject jobj = (new JsonParser()).parse(jsonString).getAsJsonObject();

		autoCalc = ((JsonPrimitive) jobj.get("autoCalc")).getAsBoolean();

		outputMode = ((JsonPrimitive) jobj.get("outputMode")).getAsInt();
		interlaceMode = ((JsonPrimitive) jobj.get("interlaceMode")).getAsInt();
		unwrapPhases = ((JsonPrimitive) jobj.get("unwrapPhases")).getAsBoolean();
		calcSigma = ((JsonPrimitive) jobj.get("calcSigma")).getAsBoolean();
		adjustP0 = ((JsonPrimitive) jobj.get("adjustP0")).getAsInt();
		adjustPP = ((JsonPrimitive) jobj.get("adjustPP")).getAsInt();
		adjustPM = ((JsonPrimitive) jobj.get("adjustPM")).getAsInt();
		JsonElement jsonElem = jobj.get("calibImgPath");

		calibImgPath = jsonElem == null ? null : jsonElem.toString();
		//more json nonsense: any amount of '\'s spaces and some kine of 'null'
		if(calibImgPath.matches("[\\\\\"\\s]*[Nn][Uu][Ll][Ll][\\\\\"\\s]*")) 
			calibImgPath = null;
		calibImgIdx = ((JsonPrimitive) jobj.get("calibImgIdx")).getAsInt();
		calibImgAng = ((JsonPrimitive) jobj.get("calibImgAng")).getAsInt();

		JsonArray jsonArr = (JsonArray) jobj.get("selections");
		if (jsonArr != null) {
			selections = new int[jsonArr.size()][];
			for (int i = 0; i < selections.length; i++) {
				JsonArray jsonArr2 = (JsonArray) jsonArr.get(i);
				selections[i] = new int[jsonArr2.size()];
				for (int j = 0; j < selections[i].length; j++) {
					selections[i][j] = jsonArr2.get(j).getAsInt();

				}
			}
		} else {
			selections = null;
		}

		updateAllControllers();
	}

	public void loadConfigGMDS(String experiment, int pulse) {
		if (pulse == -1 && connectedSource != null) {
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		}

		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "DSHDemod/Selections");

		GMDSSignal sig = (GMDSSignal) GMDSUtil.globalGMDS().getSig(sigDesc);

		selections = (int[][]) sig.get2DData();

		updateAllControllers();
		if (autoCalc)
			calc();
	}

	@Override
	public void saveConfig(String id) {
		if (id.startsWith("gmds")) {
			String parts[] = id.split("/");
			saveConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
			lastLoadedConfig = id;
		} else if (id.startsWith("jsonfile:")) {
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}

	public void saveConfigJSON(String fileName) {

		Gson gson = new GsonBuilder()
							.serializeSpecialFloatingPointValues()
							.serializeNulls()
							.setPrettyPrinting()
							.create();

		JsonElement jsonSelections = gson.toJsonTree(selections);

		JsonObject jobj = new JsonObject();
		jobj.add("autoCalc", new JsonPrimitive(autoCalc));
		jobj.add("selections", jsonSelections);
		jobj.add("outputMode", new JsonPrimitive(outputMode));
		jobj.add("interlaceMode", new JsonPrimitive(interlaceMode));
		jobj.add("unwrapPhases", new JsonPrimitive(unwrapPhases));
		jobj.add("calcSigma", new JsonPrimitive(calcSigma));
		jobj.add("adjustP0", new JsonPrimitive(adjustP0));
		jobj.add("adjustPP", new JsonPrimitive(adjustPP));
		jobj.add("adjustPM", new JsonPrimitive(adjustPM));
		jobj.add("calibImgPath", calibImgPath == null ? null : new JsonPrimitive(calibImgPath));
		jobj.add("calibImgIdx", new JsonPrimitive(calibImgIdx));
		jobj.add("calibImgAng", new JsonPrimitive(calibImgAng));

		OneLiners.textToFile(fileName, jobj.toString());
	}

	public void saveConfigGMDS(String experiment, int pulse) {
		if (connectedSource == null || selections == null)
			return;

		if (experiment == null)
			experiment = GMDSUtil.getMetaExp(connectedSource);
		if (pulse < 0)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);

		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "DSHDemod/Selections");

		GMDSSignal sig = new GMDSSignal(sigDesc, selections.clone());

		GMDSUtil.globalGMDS().writeToCache(sig);
	}

	private String autoSelectSyncObject = "autoSync";

	public void findAutoSelections() {
		ImageProcUtil.ensureFinalUpdate(autoSelectSyncObject, new Runnable() {
			@Override
			public void run() {
				Img selImg = getSelectedImageFromConnectedSource();
				settingsChanged = true;
				selections = ComponentsFinder.autoInitSelections((ComplexImage) selImg);
				calc();
				updateAllControllers();
			}
		});
	}

	private void loadExternalCalibImage() {
		double calibData[][] = null;

		String path = calibImgPath;
		boolean calibIsMu = false;
		boolean calibIsPhaseOffset = false;
		calibFit = null;

		if (calibImgPath.endsWith("@calFit")) { // explicitly calFit result
			path = calibImgPath.replaceFirst("@calFit", "");
			try {
				calibFit = new CalibrationFit();
				calibFit.loadFromSignals(GMDSUtil.globalGMDS(), path);
				calibPathValid = true;
				setSeriesMetaData("DSHDemod/appliedCalibration", calibImgPath, false);
				return;

			} catch (RuntimeException err) {
				System.err.println("Couldn't load calibFit from '" + path + "' because: " + err);
				calibFit = null;
				setSeriesMetaData("DSHDemod/appliedCalibration", "NONE", false);
				return;
			}
		} else if (calibImgPath.endsWith("@offsetGrid")) {
			// given thing is offset grid, (e.g. generated by ReducedProcessor)
			throw new NotImplementedException();

		} else if (calibImgPath.endsWith("@mu")) { // given image is mu
			calibIsMu = true;
			path = calibImgPath.replaceFirst("@mu", "");
			calibImgAng = 22.5 * Math.PI / 180;
			setSeriesMetaData("DSHDemod/appliedCalibration", "calibImgAng=" + calibImgAng, false);

		} else if (calibImgPath.matches(".*@[0-9.]*")) { // give image is the
															// given angle
			this.calibImgAng = Algorithms.mustParseDouble(calibImgPath.replaceFirst(".*@([0-9.]*)", "$1")) * Math.PI
					/ 180;
			path = calibImgPath.replaceFirst("(.*)@[0-9.]*", "$1");

			setSeriesMetaData("DSHDemod/appliedCalibration", calibImgPath, false);

		} else if (calibImgPath.endsWith("@phaseOffset")) {
			// calib image is a phase offset calibration , not a contrast
			calibIsPhaseOffset = true;
			path = calibImgPath.replaceFirst("@phaseOffset", "");

		}

		// see if it's a file first
		if (new File(path).canRead()) {
			calibFit = null;

			// as a Signal file?
			try {
				Signal sig = new Signal(path);
				calibData = (double[][]) sig.getDataAsType(double.class);

			} catch (Exception e) {
			}

			if (calibData == null) {
				// binary matrix file?
				try {
					calibData = Mat.mustLoadBinary(path, false);
				} catch (Exception e) {
				}
			}

			if (calibData == null) {
				System.err.println("Calib image path '" + calibImgPath
						+ "' seems to be a file, but isn't a valid DataSignals or BinaryMatrix file");
				calibImage = null;
				setSeriesMetaData("DSHDemod/appliedCalibration", "NONE", false);

				return;
			}

			setSeriesMetaData("DSHDemod/appliedCalibration", "file://" + calibImgPath, false);

		} else {
			try {
				// GMDS signal path?
				GMDSSignalDesc desc = new GMDSSignalDesc(path);
				GMDSSignal sig = (GMDSSignal) GMDSUtil.globalGMDS().getSig(desc);
				Object o = sig.getDataAsType(double.class);
				if (o instanceof double[][][]) {
					System.err.println(
							"WARNING: Assuming first field of multi-field calibration image at '" + calibImgPath + "'");
					calibData = ((double[][][]) o)[0]; // probably first field
														// if multi-field
				} else {
					calibData = (double[][]) o;
				}

				setSeriesMetaData("DSHDemod/appliedCalibration", "gmds:", false);
			} catch (Exception e) {
				System.err.println("Calib image path '" + calibImgPath + "' not a file or a GMDS signal path.");
				setSeriesMetaData("DSHDemod/appliedCalibration", "NONE", false);
				return;
			}
		}

		calibImage = new ByteBufferImage(this, -1, calibData[0].length, calibData.length, ByteBufferImage.DEPTH_DOUBLE,
				false);
		WriteLock writeLockCalib = calibImage.writeLock();

		try {
			writeLockCalib.lockInterruptibly();
			try {

				// doCalc() uses calibImage as if it were part of the DSH
				// processing
				// i.e., if it was ����.tan��(2��), which is ���� at 22.5deg

				if (calibIsMu) {
					for (int iY = 0; iY < calibData.length; iY++) {
						for (int iX = 0; iX < calibData[0].length; iX++) {
							calibImage.setPixelValue(writeLockCalib, iX, iY, FastMath.pow2(calibData[iY][iX]));
						}
					}

				} else if (calibIsPhaseOffset) {
					for (int iY = 0; iY < calibData.length; iY++) {
						for (int iX = 0; iX < calibData[0].length; iX++) {
							calibImage.setPixelValue(writeLockCalib, iX, iY, calibData[iY][iX]);
						}
					}

				} else {
					for (int iY = 0; iY < calibData.length; iY++) {
						for (int iX = 0; iX < calibData[0].length; iX++) {
							calibImage.setPixelValue(writeLockCalib, iX, iY,
									FastMath.pow2(FastMath.tan(2 * calibData[iY][iX] * Math.PI / 180)));
						}
					}
				}

			} finally {
				writeLockCalib.unlock();
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		calibImage.imageChanged(false);
		calibPathValid = true;
	}

	public String getCalibPath() {
		return calibPathValid ? calibImgPath : null;
	}

	@Override
	public BulkImageAllocation getBulkAlloc() {
		return bulkAlloc;
	}

	public boolean getUnwrapPhases() {
		return unwrapPhases;
	}

	public boolean getCalcSigma() {
		return calcSigma;
	}

	public String getCalibImgPath() {
		return calibImgPath;
	}

	public double getCalibImgAngle() {
		return calibImgAng;
	}

	public int getCalibImgIndex() {
		return calibImgIdx;
	}

	public boolean isCalibPathValid() {
		return calibPathValid;
	}

	public int getInterlaceMode() {
		return interlaceMode;
	}

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "Demod[" + hhc.substring(hhc.length() - 2, hhc.length()) + "]";
	}
}
