package imageProc.proc.demod;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import algorithmrepository.PhaseUnwrap2D;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ByteBufferImage;
import imageProc.core.Img;
import net.jafama.FastMath;

/** The actual calculations for DSHDemod.
 * 
 * Some notes about noise calculation:
 * 
 *  We completely ignore the spatial averaging effect of the FT windows of each component.
 * They will greatly reduce the noise from what you calc here for a single pixel.
 * However, usually a greater spatial averaging is done later and provided that assumes each
 * pixel as independent errors, it will take up what is ignored here.
 *  
 */
public class DSHCalc {
	
	/** The output image to fill, and it's write lock that we should be holding already */
	ByteBufferImage imageOut;
	WriteLock writeLock;
	
	/** The input complex data fields from the necessary components */
	double cmplxData[][];
	
	/** noiseInfo, { elecsPerCount, readNoise, backgroundLevel } */
	double noiseInfo[];
	
	/** The selection boxes on the FT that cmplxData was generated from */ 
	int selections[][];
	
	/** The alrady processed calibration image */  
	Img calibImage;
	/** The polarisation angle that the calibration image represents. */
	double calibImgAng;
	
	CalibrationFit calibFit;
	PhysicalROIGeometry ccdGeom;
	
	/** The average contrast = [Σ[|(+,+)|,|(+,-)|,|(+,0)|] / (|(0,0)| - |(0,0)[0]|) */
	public void combinedContrast(double cmplxData0[][], int sel){
		if(cmplxData[sel] == null){
			imageOut.invalidate();
			return;
		}
				
		int ow = imageOut.getWidth(), oh = imageOut.getHeight();
		int iw = ow, ih = oh;
		for(int y=0; y < ih; y++){			
			for(int x=0; x < iw; x++){
				double realPP = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
				double imagPP = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
				double absPP = FastMath.sqrt(realPP*realPP + imagPP*imagPP);

				double realPM = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
				double imagPM = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
				double absPM = FastMath.sqrt(realPM*realPM + imagPM*imagPM);

				double realP0 = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2];
				double imagP0 = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2+1];
				double absP0 = FastMath.sqrt(realP0*realP0 + imagP0*imagP0);

				double real00 = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2];
				double imag00 = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2+1];
				double abs00 = FastMath.sqrt(real00*real00 + imag00*imag00);
				
				double abs000 = 0;
				if(cmplxData0 != null){
					double real000 = cmplxData0[DSHDemod.COMPONENT_00][y*iw*2 + x*2];
					double imag000 = cmplxData0[DSHDemod.COMPONENT_00][y*iw*2 + x*2+1];
					abs000 = FastMath.sqrt(real000*real000 + imag000*imag000);
				}else if(noiseInfo != null && !Double.isNaN(noiseInfo[2])){
					abs000 = noiseInfo[2];
				}
								 
				imageOut.setPixelValue(writeLock, x, y, (abs00 <= abs000) ? Double.NaN : ((absPP + absPM + absP0) / (abs00 - abs000))); 
			}
		}		
	}
	
	/** Amplitude */
	public void amplitude(int sel){
		if(cmplxData[sel] == null) {
			imageOut.invalidate();
			return;
		}
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double xxRe = cmplxData[sel][y*iw*2 + x*2];
				double xxIm = cmplxData[sel][y*iw*2 + x*2+1];
				
				imageOut.setPixelValue(writeLock, x, y, FastMath.sqrt(xxRe*xxRe + xxIm*xxIm));
			}
		}
				
		// Noise calculation
		if(imageOut.getNFields() > 1){
			for(int y=0; y < ih; y++){
				for(int x=0; x < iw; x++){
					double zzRe = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2];
					double zzIm = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2+1];
					double I00 = FastMath.sqrt(zzRe*zzRe + zzIm*zzIm) * noiseInfo[0]; // in electrons

					double xxRe = cmplxData[sel][y*iw*2 + x*2];
					double xxIm = cmplxData[sel][y*iw*2 + x*2+1];
					double Isig = FastMath.sqrt(xxRe*xxRe + xxIm*xxIm) * noiseInfo[0];
					
					//square errors, assuming sqrt(N)/N error
					double se = I00; 
					if(false && sel != DSHDemod.COMPONENT_00){
						se += Isig;
					}
					
					se += noiseInfo[1]*noiseInfo[1];
					
					imageOut.setPixelValue(writeLock, 1, x, y, FastMath.sqrt(se));
					
					
				}
			}
		}		
	}
	
	/** Amplitude */
	public void contrast(double cmplxData0[][], int sel){
		if(cmplxData[sel] == null) {
			imageOut.invalidate();
			return;
		}
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double xxRe = cmplxData[sel][y*iw*2 + x*2];
				double xxIm = cmplxData[sel][y*iw*2 + x*2+1];
				double xxAbs = FastMath.sqrt(xxRe*xxRe + xxIm*xxIm);

				double real00 = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2];
				double imag00 = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2+1];
				double abs00 = FastMath.sqrt(real00*real00 + imag00*imag00);
				
				double abs000 = 0;
				if(cmplxData0 != null){
					double real000 = cmplxData0[DSHDemod.COMPONENT_00][y*iw*2 + x*2];
					double imag000 = cmplxData0[DSHDemod.COMPONENT_00][y*iw*2 + x*2+1];
					abs000 = FastMath.sqrt(real000*real000 + imag000*imag000);
				}else if(noiseInfo != null && !Double.isNaN(noiseInfo[2])){
					abs000 = noiseInfo[2];
				}
				
				imageOut.setPixelValue(writeLock, x, y, (abs00 <= abs000) ? Double.NaN : (2 * xxAbs / (abs00 - abs000)));
			}
		}
				/*
		// Noise calculation
		if(imageOut.getNFields() > 1){
			for(int y=0; y < ih; y++){
				for(int x=0; x < iw; x++){
					double zzRe = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2];
					double zzIm = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2+1];
					double I00 = FastMath.sqrt(zzRe*zzRe + zzIm*zzIm) * noiseInfo[0]; // in electrons

					double xxRe = cmplxData[sel][y*iw*2 + x*2];
					double xxIm = cmplxData[sel][y*iw*2 + x*2+1];
					double Isig = FastMath.sqrt(xxRe*xxRe + xxIm*xxIm) * noiseInfo[0];
					
					//square errors, assuming sqrt(N)/N error
					double se = I00; 
					if(false && sel != DSHDemod.COMPONENT_00){
						se += Isig;
					}
					
					se += noiseInfo[1]*noiseInfo[1];
					
					imageOut.setPixelValue(writeLock, 1, x, y, FastMath.sqrt(se));
					
					
				}
			}
		}	
		*/	
	}
	
	/** Phase */
	public void phase(int sel, boolean unwrapPhases){
		if(cmplxData[sel] == null || selections == null){
			imageOut.invalidate();
			return;
		}
		
		//Calc frequency of selection centre mark (to subtract that oscillation) 
		int sx0 = selections[sel][0], sy0 = selections[sel][1];
		int sw = selections[sel][2], sh = selections[sel][3];
		int sxC = selections[sel][4], syC = selections[sel][5];
		int sx1 = sx0 + sw - 1, sy1 = sy0 + sh - 1;
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		int fx0 = sxC - (iw/2);
		int fy0 = syC - (ih/2);
		
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double phase = (double)x * fx0 / iw + (double)y * fy0 / ih;
				double realOsc = FastMath.cos(-2.0 * Math.PI * phase);
				double imagOsc = FastMath.sin(-2.0 * Math.PI * phase);
				double realImg = cmplxData[sel][y*iw*2 + x*2];
				double imagImg = cmplxData[sel][y*iw*2 + x*2+1];
				
				double realD = (realOsc * realImg) - (imagOsc * imagImg);
				double imagD = (realOsc * imagImg) + (imagOsc * realImg);
				
				imageOut.setPixelValue(writeLock, x, y, FastMath.atan2(imagD, realD));
			}
		}
		
		//phase unwrapping
		if(unwrapPhases){
			ByteBuffer phaseBBuff = imageOut.getWritableBuffer(writeLock);
			DoubleBuffer phaseBuff = phaseBBuff.asDoubleBuffer();
			
			double phaseData[] = new double[iw*ih];
			phaseBuff.rewind();
			phaseBuff.limit(phaseBuff.capacity());
			phaseBuff.get(phaseData);
			PhaseUnwrap2D.unwrap(phaseData, iw, ih, false, false);
			phaseBuff.clear();
			phaseBuff.put(phaseData);
		}
		
		//output images should be in degrees
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				imageOut.setPixelValue(writeLock, x, y, imageOut.getPixelValue(writeLock, x, y) * 180 / Math.PI);
			}
		}
		
		//subtract phase from calib image (if it's not this one)
		if(calibImage != null && calibImage != imageOut && 
				calibImage.getWidth() == iw && calibImage.getHeight() == ih){ 
			try{
				ReadLock readLockCalib = calibImage.readLock();
				readLockCalib.lockInterruptibly();
				try{
					
					for(int y=0; y < ih; y++){
						for(int x=0; x < iw; x++){
							double v =  imageOut.getPixelValue(writeLock, x, y) - calibImage.getPixelValue(readLockCalib, x, y);
							if(v >= 180) v -= 360;
							if(v < -180) v += 360;
							imageOut.setPixelValue(writeLock, x, y, v);
						}
					}
				}finally{ readLockCalib.unlock(); }
			}catch(InterruptedException err){ }
					
		}
	}
	
	/** Even/odd phase difference */
	public void phaseDiff(double evenCmplxData[][], double oddCmplxData[][], int sel, int interlaceMode, boolean unwrapPhases){
		if(evenCmplxData[sel] == null || oddCmplxData[sel] == null || selections == null){
			imageOut.invalidate();
			return;
		}
		
		int sxC = selections[sel][4], syC = selections[sel][5];
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		int fx0 = sxC - (iw/2);
		int fy0 = syC - (ih/2);
		
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				//double phase = (double)x * fx0 / iw + (double)y * fy0 / ih;
				
				double phase = (interlaceMode == DSHDemod.INTERLACE_MODE_PAIR_SUM) ? ((double)x * fx0 / iw + (double)y * fy0 / ih) : 0;
				double realOsc = FastMath.cos(-2.0 * Math.PI * phase);
				double imagOsc = FastMath.sin(-2.0 * Math.PI * phase);
				
				double realImgE = evenCmplxData[sel][y*iw*2 + x*2];
				double imagImgE = evenCmplxData[sel][y*iw*2 + x*2+1];
				
				double realDE = (realOsc * realImgE) - (imagOsc * imagImgE);
				double imagDE = (realOsc * imagImgE) + (imagOsc * realImgE);
				
				//double phaseE = FastMath.atan2(imagDE, realDE);
				
				double realImgO = oddCmplxData[sel][y*iw*2 + x*2];
				double imagImgO = oddCmplxData[sel][y*iw*2 + x*2+1];
				
				double realDO = (realOsc * realImgO) - (imagOsc * imagImgO);
				double imagDO = (realOsc * imagImgO) + (imagOsc * realImgO);
				
				//double phaseO = FastMath.atan2(imagDO, realDO);
				
				if(interlaceMode == DSHDemod.INTERLACE_MODE_PAIR_SUM){
					//imageOut.setPixelValue(x, y, ((phaseO + phaseE) % (Math.PI)));
					
					imageOut.setPixelValue(writeLock, x, y, FastMath.atan2((imagDE*realDO + realDE*imagDO), (realDE*realDO - imagDE*imagDO)));
					
					
				}else{ //INTERLACE_MODE_PAIR_DIFF
					
					imageOut.setPixelValue(writeLock, x, y, FastMath.atan2((imagDE*realDO - realDE*imagDO), (realDE*realDO + imagDE*imagDO)));
					
					//imageOut.setPixelValue(x, y, ((phaseO - phaseE) % (Math.PI)));
					
				}
				
			}
		}
		
		if(unwrapPhases){
			ByteBuffer phaseBBuff = imageOut.getWritableBuffer(writeLock);
			DoubleBuffer phaseBuff = phaseBBuff.asDoubleBuffer();
			
			double phaseData[] = new double[iw*ih*8];
			phaseBuff.rewind();
			phaseBuff.limit(phaseBuff.capacity());
			phaseBuff.get(phaseData);
			PhaseUnwrap2D.unwrap(phaseData, iw, ih, false, false);
			phaseBuff.clear();
			phaseBuff.put(phaseData);
		}
		
		
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				imageOut.setPixelValue(writeLock, x, y, imageOut.getPixelValue(writeLock, x, y) * 180 / Math.PI);
			}
		}
	}
	
	/** Polarisation angle from single component ratio |(+,±)| / |(+,0)| */
	public void polAngSingle(int selA, int selB, boolean finish) {
		if(cmplxData[selA] == null || cmplxData[selB] == null ){
			imageOut.invalidate();
			return;
		}
				
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double real, imag;
				
				real = cmplxData[selA][y*iw*2 + x*2];
				imag = cmplxData[selA][y*iw*2 + x*2+1];
				double ampA = FastMath.sqrt(real*real + imag*imag);

				real = cmplxData[selB][y*iw*2 + x*2];
				imag = cmplxData[selB][y*iw*2 + x*2+1];
				double ampB = FastMath.sqrt(real*real + imag*imag);
				
				if(finish){
					double ang = FastMath.atan2(2*ampA, ampB)/2 * 180 / Math.PI;				
					imageOut.setPixelValue(writeLock, x, y, ang);
				}else{
					imageOut.setPixelValue(writeLock, x, y, 2*ampA / ampB);
				}
			}
		}
	}
	
	/** Polarisation from (+,+)*(+,-)/(+,0)^2 which seems to mostly balance the Γ(x') spectrum integral.
	 * In theory, it also automatically removes the oscillations, so doesnt need the abs.
	 * @param reprojectInvalidValues  Throw values that seems to be below 0 or above 45degrees back over that end  
	 * */
	public void balanced(boolean phase, boolean finish, boolean reprojectInvalidValues) {
		 
		
		if(cmplxData[DSHDemod.COMPONENT_pp] == null ||cmplxData[DSHDemod.COMPONENT_pm] == null ||
				cmplxData[DSHDemod.COMPONENT_p0] == null){
			imageOut.invalidate();
			return;
		}
		
		double tanSq2CalAng = FastMath.pow2(FastMath.tan(2 * calibImgAng));
		
		ReadLock readLockCalib = null;
		try{
			if(calibImage != null){
				readLockCalib = calibImage.readLock();
				readLockCalib.lockInterruptibly();
			}
			try{
				
				int iw = imageOut.getWidth();
				int ih = imageOut.getHeight();
				for(int y=0; y < ih; y++){
					for(int x=0; x < iw; x++){
						if(Thread.interrupted())
							throw new InterruptedException();
						
						double ppRe = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
						double ppIm = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
						
						double pmRe = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
						double pmIm = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
						
						double p0Re = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2];
						double p0Im = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2+1];
						
						// nom = (+,+)*(+,-)
						double nomRe = ppRe * pmRe - ppIm * pmIm;
						double nomIm = ppRe * pmIm + ppIm * pmRe;
		
						// denom = (+,0)*(+,0)
						double denomRe = p0Re * p0Re - p0Im * p0Im;
						double denomIm = p0Re * p0Im + p0Im * p0Re;
						
						//res = nom / denom
						double c2d2 = denomRe*denomRe + denomIm*denomIm;				
						double resRe = (nomRe*denomRe + nomIm*denomIm) / c2d2;
						double resIm = (nomIm*denomRe - nomRe*denomIm) / c2d2;
							
						if(phase){		
							//imageOut.setPixelValue(writeLock, x, y, FastMath.atan2(resIm, resRe)*180/Math.PI);
							imageOut.setPixelValue(writeLock, x, y, (nomRe*nomRe + nomIm*nomIm));							
						}else{
								
							double tanSq2Theta = -4*resRe;
																		
							if(finish){
								double ang;
								
								if(calibFit != null){
									double physX = ccdGeom.pixelToPhysX(x);
									double physY = ccdGeom.pixelToPhysY(y);
									double mu = calibFit.mu.eval(physX, physY);
									double phs = (calibFit.offset.eval(physX, physY) - calibFit.offset.eval(0,0)) * Math.PI / 180;
									double chi = calibFit.effEllip.eval(physX, physY) * Math.PI / 180;
												
									// idealPolAng = (x[i] / l * 2 * Math.PI + phs) / 4;
									// II=-mu * (tan(2*idealPolAng)**2 + tan(chi)**2 / sin(2*(idealPolAng-45*pi/180))**2);
									
									//tanSq2Theta /= mu;
									boolean dbg = false;//(x==308 && y==374);
									if(dbg)
										System.out.println("rar");
									
									//double II4 = -tanSq2Theta;
																		
									double tanSqchi = FastMath.pow2(FastMath.tan(chi));
									
									double muSq = mu*mu;

									double nievePolAng = FastMath.atan(FastMath.sqrt(tanSq2Theta / muSq)) / 2;
									if(dbg) System.out.println(nievePolAng);
									//tanSqchi=0;
									//phs=0;
									double idealPolAng = nievePolAng;
									for(int i=0; i < 4; i++){
										idealPolAng = FastMath.atan(FastMath.sqrt(tanSq2Theta / muSq - tanSqchi / FastMath.pow2(FastMath.sin(2*(idealPolAng + 45*Math.PI/180))))) / 2;
										if(dbg) System.out.println(idealPolAng);
													
									}
									
									double tanSq2ThetaB = mu*mu * (FastMath.pow2(FastMath.tan(2*idealPolAng))
											+ tanSqchi / FastMath.pow2(FastMath.sin(2*(idealPolAng + 45*Math.PI/180)))
												);
									if(dbg) System.out.println(tanSq2Theta + " ?= " + tanSq2ThetaB);
									
									ang = idealPolAng- phs/4;
									
									
									 //store the effect at 22.5deg instead (so we can see how it looks transformed)
									/*idealPolAng = 22.5 * Math.PI / 180;
									tanSq2ThetaB = mu*mu * (FastMath.pow2(FastMath.tan(2*idealPolAng))
											+ tanSqchi / FastMath.pow2(FastMath.sin(2*(idealPolAng + 45*Math.PI/180))) 
											);
									nievePolAng = FastMath.atan(FastMath.sqrt(tanSq2ThetaB)) / 2;
									
									ang = nievePolAng - idealPolAng;
									//*/
									
									//ang = FastMath.atan(FastMath.sqrt(tanSq2Theta / muSq)) / 2;
								}else{
									if(calibImage != null){
									
										
										// calibImage = tan(2*mang)² = µ² tan(2*calibAngImg)²
										// µ² = calibImage / tan(2*calibAngImg)²
										
										// tanSq2Theta = tan(2*mang)² = µ² tan(2*cang)
										// cang = atan(tanSq2Theta / µ²)/2
										// cang = atan(tanSq2Theta * tan(2*calibAngImg)² / calibImage)/2
										
										//TODO: proper ROI stuff
										int cx = x * calibImage.getWidth() / iw;
										int cy = y * calibImage.getHeight()/ ih;
										tanSq2Theta *= tanSq2CalAng / calibImage.getPixelValue(readLockCalib, cx, cy);
									}
										
									/* Noise on the components can throw the effective value under 0 or over 45 degrees,
									 * which results in a negative value for tanSq2Theta. If we leave it as NaN, then
									 * spatial/temporal average later will only pick up values that arn't NaN, and bias
									 * the result towards 22.5degrees. To avoid this, we try to work out which end of the
									 * scale the pixel has fallen over and give the the 'effective' angle that is out of
									 * range, in the hope of removing the bias in the average.
									*/
									if(tanSq2Theta < 0 && reprojectInvalidValues){
										//the value has fallen over one end of the scale, but we have to work out which end.
										//We can guess it based on the component magnitudes
										double absPm2 = ppRe*ppRe + ppIm*ppIm;
										double absPp2 = pmRe*pmRe + pmIm*pmIm;
										double absP02 = p0Re*p0Re + p0Im*p0Im;
																						
										if(absPp2*absPm2 < absP02*absP02){
											ang = -FastMath.atan(FastMath.sqrt(-tanSq2Theta))/2;
										}else{
											ang = (Math.PI/4 - FastMath.atan(FastMath.sqrt(-tanSq2Theta))/2) + Math.PI/4;
										}
									}else{
										//just calculate as it is, and let it NaN if it's over the end
										ang = FastMath.atan(FastMath.sqrt(tanSq2Theta))/2;									
									}
								}								
								imageOut.setPixelValue(writeLock, x, y, ang * 180/Math.PI);
							}else{
								imageOut.setPixelValue(writeLock, x, y, tanSq2Theta);
							}							
						}

						if(imageOut.getNFields() > 1){ // Noise calculation
							//for the sigma, we just assume the phases cancel properly and
							// use |I++||I+-| / |I+0|²
							
							// The logic here is really weird.
							//
							// Firstly, we completely ignore the spatial averaging effect of the FT windows of each component.
							// They will greatly reduce the noise from what you calc here for a single pixel.
							// However, usually a greater spatial averaging is done later and provided that assumes each
							// pixel as independent errors, it will take up what is ignored here.
							//
							// Secondly, if the background I00 component it large, it will contribute most of the 
							// noise to all 3 components. When we calculate the ratio, we have to not include it as independent
							// noise on each. It's /a bit/ independant, since it comes from neigbouring pixels too. 
							// So I decided to simply divide the I00 up amongst the 4 in their sqrt(N) calculation.
							// Empirically, that seems to give a good final calculated error (versus simulated from PlatesGen)
							// across different I++/I+0 and different I+0/I00 ratios. It's not perfect, but mostly works.
							
							//if(imageOut.getSourceIndex() == 163 && x==160 && y==120)
								//System.out.println("rar");
							
							double zzRe = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2];
							double zzIm = cmplxData[DSHDemod.COMPONENT_00][y*iw*2 + x*2+1];
							
							double I00 = (FastMath.sqrt(zzRe*zzRe + zzIm*zzIm)) * noiseInfo[0];
							double Ipp = (FastMath.sqrt(ppRe*ppRe + ppIm*ppIm)) * noiseInfo[0]; //  noiseInfo[0] = electronsPerCount
							double Ipm = (FastMath.sqrt(pmRe*pmRe + pmIm*pmIm)) * noiseInfo[0];
							double Ip0 = (FastMath.sqrt(p0Re*p0Re + p0Im*p0Im)) * noiseInfo[0];
							
							//square fraction errors (sigma/I)² = I/I², assuming sqrt(N)/N error
							double sfePP = (I00/4 + Ipp) / (Ipp*Ipp) ; //  noiseInfo[1] = read noise in elecs
							double sfePM = (I00/4 + Ipm) / (Ipm*Ipm);
							double sfeP0 = (I00/2 + Ip0) / (Ip0*Ip0);
							
							double res = Ipp*Ipm / (Ip0*Ip0);
							double sfeRes = sfePP + sfePM + 4.0*sfeP0;
							
							double A = FastMath.sqrt(4 * res);
							double sfeA = 0.25 * sfeRes;
							double seA = sfeA * A*A;
														
							double ang = FastMath.atan(A)/2;	
							double seAng = seA / ((1.0 + A*A)*(1.0 + A*A)) / 4;
							
							//imageOut.setPixelValue(writeLock, 0, x, y, ang * 180/Math.PI);
							imageOut.setPixelValue(writeLock, 1, x, y, FastMath.sqrt(seAng) * 180/Math.PI);
							
							
							//if(eAng < 0.01)
								//System.out.println("rar");
							
							//imageOut.setPixelValue(writeLock, 1, x, y, eAng);
							
							//if(Double.isNaN(res) || Double.isInfinite(res))
							//	System.out.println("res=NaN");
							
						}
					}
				}
			}finally{ 
				if(readLockCalib != null)
					readLockCalib.unlock();
			}
		}catch(InterruptedException err){
			
		}
	}
	
	/** Polarisation from |(+,+)||(+,-)|/|(+,0)|².
	 * Same as polAngBalanced which balances the Γ(x') spectrum integral.
	 * This one does the absolute of each component first, so doesn't produce NaNs */
	public void balancedAbs(boolean finish) {
		if(cmplxData[DSHDemod.COMPONENT_pp] == null ||cmplxData[DSHDemod.COMPONENT_pm] == null ||
				cmplxData[DSHDemod.COMPONENT_p0] == null){
			imageOut.invalidate();
			return;
		}
		
		double tanSq2CalAng = FastMath.pow2(FastMath.tan(2 * calibImgAng));
		
		ReadLock readLockCalib = null;
		try{
			if(calibImage != null){
				readLockCalib = calibImage.readLock();
				readLockCalib.lockInterruptibly();
			}
			try{
				
				int iw = imageOut.getWidth();
				int ih = imageOut.getHeight();
				for(int y=0; y < ih; y++){
					for(int x=0; x < iw; x++){
						double ppRe = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
						double ppIm = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
						
						double pmRe = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
						double pmIm = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
						
						double p0Re = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2];
						double p0Im = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2+1];
		
						double IppSq = (ppRe*ppRe + ppIm*ppIm);
						double IpmSq = (pmRe*pmRe + pmIm*pmIm);
						double Ip0Sq = (p0Re*p0Re + p0Im*p0Im);
					
						double tanSq2Theta = 4*FastMath.sqrt(IppSq*IpmSq) / Ip0Sq;
											
						if(finish){
							if(calibImage != null){								
								// see balanced()
								//TODO: proper ROI stuff
								int cx = x * calibImage.getWidth() / iw;
								int cy = y * calibImage.getHeight()/ ih;
								tanSq2Theta *= tanSq2CalAng / calibImage.getPixelValue(readLockCalib, cx, cy);
							}
								
							double ang = FastMath.atan(FastMath.sqrt(tanSq2Theta))/2;
							
							imageOut.setPixelValue(writeLock, x, y, ang * 180/Math.PI);
						}else{
							imageOut.setPixelValue(writeLock, x, y, tanSq2Theta);
						}							
					}
				}
			}finally{ 
				if(readLockCalib != null)
					readLockCalib.unlock();
			}
		}catch(InterruptedException err){
			
		}
	}
	
	/** The misbalance ratio |(+,+)| / |(+,-)| */
	public void missbalance() {
		if(cmplxData[DSHDemod.COMPONENT_pp] == null ||cmplxData[DSHDemod.COMPONENT_pm] == null){
			imageOut.invalidate();
			return;
		}
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double ppRe = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
				double ppIm = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
				
				double pmRe = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
				double pmIm = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
								
				double ppAbs = FastMath.sqrt(ppRe*ppRe + ppIm*ppIm);
				double pmAbs = FastMath.sqrt(pmRe*pmRe + pmIm*pmIm);
				
				imageOut.setPixelValue(writeLock, x, y, ppAbs / pmAbs);
			}
		}
	}
	
	/** Phase of misbalance (+,+)/(+,-) */
	public void phaseCmptDiff(boolean unwrapPhases){
		if(cmplxData[DSHDemod.COMPONENT_pp] == null ||cmplxData[DSHDemod.COMPONENT_pm] == null){
			imageOut.invalidate();
			return;
		}
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		
		int fx0PP, fy0PP, fx0PM, fy0PM;
		//Calc frequency of selection centre mark (to subtract that oscillation)
		{
			int sx0 = selections[DSHDemod.COMPONENT_pp][0], sy0 = selections[DSHDemod.COMPONENT_pp][1];
			int sw = selections[DSHDemod.COMPONENT_pp][2], sh = selections[DSHDemod.COMPONENT_pp][3];
			int sxC = selections[DSHDemod.COMPONENT_pp][4], syC = selections[DSHDemod.COMPONENT_pp][5];
			int sx1 = sx0 + sw - 1, sy1 = sy0 + sh - 1;		
			fx0PP = sxC - (iw/2);
			fy0PP = syC - (ih/2);
		}{
			int sx0 = selections[DSHDemod.COMPONENT_pm][0], sy0 = selections[DSHDemod.COMPONENT_pm][1];
			int sw = selections[DSHDemod.COMPONENT_pm][2], sh = selections[DSHDemod.COMPONENT_pm][3];
			int sxC = selections[DSHDemod.COMPONENT_pm][4], syC = selections[DSHDemod.COMPONENT_pm][5];
			int sx1 = sx0 + sw - 1, sy1 = sy0 + sh - 1;		
			fx0PM = sxC - (iw/2);
			fy0PM = syC - (ih/2);
		}
		
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double phasePP, phasePM;
				{
					double phase = (double)x * fx0PP / iw + (double)y * fy0PP / ih;
					double realOsc = FastMath.cos(-2.0 * Math.PI * phase);
					double imagOsc = FastMath.sin(-2.0 * Math.PI * phase);
					double realImg = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
					double imagImg = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
					
					double realD = (realOsc * realImg) - (imagOsc * imagImg);
					double imagD = (realOsc * imagImg) + (imagOsc * realImg);
					
					phasePP = FastMath.atan2(imagD, realD);
				}{
					double phase = (double)x * fx0PM / iw + (double)y * fy0PM / ih;
					double realOsc = FastMath.cos(-2.0 * Math.PI * phase);
					double imagOsc = FastMath.sin(-2.0 * Math.PI * phase);
					double realImg = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
					double imagImg = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
					
					double realD = (realOsc * realImg) - (imagOsc * imagImg);
					double imagD = (realOsc * imagImg) + (imagOsc * realImg);
					
					phasePM = FastMath.atan2(imagD, realD);
				}
				
				imageOut.setPixelValue(writeLock, x, y, phasePP - phasePM);
			}
		}
		
		//phase unwrapping
		if(unwrapPhases){
			ByteBuffer phaseBBuff = imageOut.getWritableBuffer(writeLock);
			DoubleBuffer phaseBuff = phaseBBuff.asDoubleBuffer();
			
			double phaseData[] = new double[iw*ih];
			phaseBuff.rewind();
			phaseBuff.limit(phaseBuff.capacity());
			phaseBuff.get(phaseData);
			PhaseUnwrap2D.unwrap(phaseData, iw, ih, false, false);
			phaseBuff.clear();
			phaseBuff.put(phaseData);
		}
		
		//output images should be in degrees
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				imageOut.setPixelValue(writeLock, x, y, imageOut.getPixelValue(writeLock, x, y) * 180 / Math.PI);
			}
		}
		
		//subtract phase from calib image (if it's not this one)
		if(calibImage != null && calibImage != imageOut && 
				calibImage.getWidth() == iw && calibImage.getHeight() == ih){ 
			try{
				ReadLock readLockCalib = calibImage.readLock();
				readLockCalib.lockInterruptibly();
				try{
					
					for(int y=0; y < ih; y++){
						for(int x=0; x < iw; x++){
							double v =  imageOut.getPixelValue(writeLock, x, y) - calibImage.getPixelValue(readLockCalib, x, y);
							if(v >= 180) v -= 360;
							if(v < -180) v += 360;
							imageOut.setPixelValue(writeLock, x, y, v);
						}
					}
				}finally{ readLockCalib.unlock(); }
			}catch(InterruptedException err){ }
					
		}
	}
	
	
	/** Even/odd phase difference */
	public void phaseCmptDiffDiff(double evenCmplxData[][], double oddCmplxData[][], int interlaceMode, boolean unwrapPhases){
		if(evenCmplxData[DSHDemod.COMPONENT_pp] == null || oddCmplxData[DSHDemod.COMPONENT_pm] == null || selections == null){
			imageOut.invalidate();
			return;
		}
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		
		int fx0PP, fy0PP, fx0PM, fy0PM;
		{
			int sx0 = selections[DSHDemod.COMPONENT_pp][0], sy0 = selections[DSHDemod.COMPONENT_pp][1];
			int sw = selections[DSHDemod.COMPONENT_pp][2], sh = selections[DSHDemod.COMPONENT_pp][3];
			int sxC = selections[DSHDemod.COMPONENT_pp][4], syC = selections[DSHDemod.COMPONENT_pp][5];
			int sx1 = sx0 + sw - 1, sy1 = sy0 + sh - 1;		
			fx0PP = sxC - (iw/2);
			fy0PP = syC - (ih/2);
		}{
			int sx0 = selections[DSHDemod.COMPONENT_pm][0], sy0 = selections[DSHDemod.COMPONENT_pm][1];
			int sw = selections[DSHDemod.COMPONENT_pm][2], sh = selections[DSHDemod.COMPONENT_pm][3];
			int sxC = selections[DSHDemod.COMPONENT_pm][4], syC = selections[DSHDemod.COMPONENT_pm][5];
			int sx1 = sx0 + sw - 1, sy1 = sy0 + sh - 1;		
			fx0PM = sxC - (iw/2);
			fy0PM = syC - (ih/2);
		}
		
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double phasePP, phasePM;
				
				{
					double phase = (interlaceMode == DSHDemod.INTERLACE_MODE_PAIR_SUM) ? ((double)x * fx0PP / iw + (double)y * fy0PP / ih) : 0;
					double realOsc = FastMath.cos(-2.0 * Math.PI * phase);
					double imagOsc = FastMath.sin(-2.0 * Math.PI * phase);
					
					double realImgE = evenCmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
					double imagImgE = evenCmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
					
					double realDE = (realOsc * realImgE) - (imagOsc * imagImgE);
					double imagDE = (realOsc * imagImgE) + (imagOsc * realImgE);
					
					//double phaseE = FastMath.atan2(imagDE, realDE);
					
					double realImgO = oddCmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
					double imagImgO = oddCmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
					
					double realDO = (realOsc * realImgO) - (imagOsc * imagImgO);
					double imagDO = (realOsc * imagImgO) + (imagOsc * realImgO);
					
					if(interlaceMode == DSHDemod.INTERLACE_MODE_PAIR_SUM){					
						phasePP =  FastMath.atan2((imagDE*realDO + realDE*imagDO), (realDE*realDO - imagDE*imagDO));					
					}else{ //INTERLACE_MODE_PAIR_DIFF
						phasePP = FastMath.atan2((imagDE*realDO - realDE*imagDO), (realDE*realDO + imagDE*imagDO));
					}
				}{
					double phase = (interlaceMode == DSHDemod.INTERLACE_MODE_PAIR_SUM) ? ((double)x * fx0PM / iw + (double)y * fy0PM / ih) : 0;
					double realOsc = FastMath.cos(-2.0 * Math.PI * phase);
					double imagOsc = FastMath.sin(-2.0 * Math.PI * phase);
					
					double realImgE = evenCmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
					double imagImgE = evenCmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
					
					double realDE = (realOsc * realImgE) - (imagOsc * imagImgE);
					double imagDE = (realOsc * imagImgE) + (imagOsc * realImgE);
					
					//double phaseE = FastMath.atan2(imagDE, realDE);
					
					double realImgO = oddCmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
					double imagImgO = oddCmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
					
					double realDO = (realOsc * realImgO) - (imagOsc * imagImgO);
					double imagDO = (realOsc * imagImgO) + (imagOsc * realImgO);
					
					if(interlaceMode == DSHDemod.INTERLACE_MODE_PAIR_SUM){					
						phasePM =  FastMath.atan2((imagDE*realDO + realDE*imagDO), (realDE*realDO - imagDE*imagDO));					
					}else{ //INTERLACE_MODE_PAIR_DIFF
						phasePM = FastMath.atan2((imagDE*realDO - realDE*imagDO), (realDE*realDO + imagDE*imagDO));
					}
				}
					
				//double phaseO = FastMath.atan2(imagDO, realDO);
				
				
				imageOut.setPixelValue(writeLock, x, y, phasePP - phasePM);
			}
		}
		
		if(unwrapPhases){
			ByteBuffer phaseBBuff = imageOut.getWritableBuffer(writeLock);
			DoubleBuffer phaseBuff = phaseBBuff.asDoubleBuffer();
			
			double phaseData[] = new double[iw*ih*8];
			phaseBuff.rewind();
			phaseBuff.limit(phaseBuff.capacity());
			phaseBuff.get(phaseData);
			PhaseUnwrap2D.unwrap(phaseData, iw, ih, false, false);
			phaseBuff.clear();
			phaseBuff.put(phaseData);
		}
		
		
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				imageOut.setPixelValue(writeLock, x, y, imageOut.getPixelValue(writeLock, x, y) * 180 / Math.PI);
			}
		}
	}
	
	
	/** M.Zolchow's calculation for the Zeeman polariastion (in theory)*/
	public void zeeman(boolean phase, int signPM, boolean finish) {
		if(cmplxData[DSHDemod.COMPONENT_pp] == null ||cmplxData[DSHDemod.COMPONENT_pm] == null ||
				cmplxData[DSHDemod.COMPONENT_p0] == null){
			imageOut.invalidate();
			return;
		}
		
		double tanSq2CalAng = FastMath.pow2(FastMath.tan(2 * calibImgAng));
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double ppRe = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
				double ppIm = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
				
				double pmRe = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
				double pmIm = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
				
				double p0Re = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2];
				double p0Im = cmplxData[DSHDemod.COMPONENT_p0][y*iw*2 + x*2+1];
				

				double ppAbs = FastMath.sqrt(ppRe*ppRe + ppIm*ppIm);
				double pmAbs = FastMath.sqrt(pmRe*pmRe + pmIm*pmIm);
				double p0Abs = FastMath.sqrt(p0Re*p0Re + p0Im*p0Im);
				
				/*
				
				//nom = (+,-) - (+,+)
				double nomRe = pmRe - ppRe;
				double nomIm = pmIm - ppIm;

				// denom = (+,0)*(+,0)
				double denomRe = p0Re;
				double denomIm = p0Im;
				
				//res = nom / denom
				double c2d2 = denomRe*denomRe + denomIm*denomIm;				
				double resRe = (nomRe*denomRe + nomIm*denomIm) / c2d2;
				double resIm = (nomIm*denomRe - nomRe*denomIm) / c2d2;
				*/
				if(phase){
						throw new RuntimeException("err, think about this!!");
					
					///imageOut.setPixelValue(x, y, FastMath.atan2(resIm, resRe)*180/Math.PI);
				}else{
						
					double tan2Theta = p0Abs / (pmAbs + signPM * ppAbs);  
					
					if(x == imageOut.getWidth()/2 && y == imageOut.getHeight()/2){
						tan2Theta = 1 * tan2Theta;
					}							
									
					if(finish){
						//resRe *= 4;
						
						if(calibImage != null){
							throw new RuntimeException("err, think about this!!");
							// calibImage = tan(2*mang)² = µ² tan(2*calibAngImg)²
							// µ² = calibImage / tan(2*calibAngImg)²
							
							// tanSq2Theta = tan(2*mang)² = µ² tan(2*cang)
							// cang = atan(tanSq2Theta / µ²)/2
							// cang = atan(tanSq2Theta * tan(2*calibAngImg)² / calibImage)/2						
							//tanSq2Theta *= tanSq2CalAng / calibImage.getPixelValue(x, y);						
							
						}
						
						double ang = FastMath.abs(FastMath.atan(tan2Theta)/2);
						
						ang *= 180/Math.PI;
						
						//if(Double.isInfinite(ang) || Double.isNaN(ang))
						//	ang = -2;
						
						imageOut.setPixelValue(writeLock, x, y, ang);
					}else{
						throw new RuntimeException("err, think about this!!");
						//imageOut.setPixelValue(x, y, tan2Theta);
					}
				}
			}
		}
	}
	
	/** M.Zolchow's calculation for the difference of the two Savart components (+,+) - (+,-)*/
	public void ampCmptDiff(boolean phase) {
		if(cmplxData[DSHDemod.COMPONENT_pp] == null ||cmplxData[DSHDemod.COMPONENT_pm] == null){
			imageOut.invalidate();
			return;
		}
				
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double ppRe = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
				double ppIm = cmplxData[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
				
				double pmRe = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
				double pmIm = cmplxData[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
								
				double ppAbs = FastMath.sqrt(ppRe*ppRe + ppIm*ppIm);
				double pmAbs = FastMath.sqrt(pmRe*pmRe + pmIm*pmIm);				
			
				if(phase){
					throw new RuntimeException("err, think about this!!");					
				}else{						
					imageOut.setPixelValue(writeLock, x, y, (pmAbs - ppAbs));					
				}
			}
		}
	}
	

	
	
	/** Recover polarisation from (+,+)E*(+,-)E/(+,0)O^2
	 * E = even(FLC on), O = odd(FLC off)
	 * That should give theta - without contamination from Chi */
	public void ellipCompensatedThetaB(double cmplxDataEven[][], double cmplxDataOdd[][]) {
		if(cmplxDataEven[DSHDemod.COMPONENT_pp] == null ||cmplxDataEven[DSHDemod.COMPONENT_pm] == null ||
				cmplxDataEven[DSHDemod.COMPONENT_p0] == null || cmplxDataOdd[DSHDemod.COMPONENT_p0] == null ||
				cmplxDataOdd[DSHDemod.COMPONENT_pp] == null || cmplxDataOdd[DSHDemod.COMPONENT_pm] == null){
			imageOut.invalidate();
			return;
		}
		
		int iw = imageOut.getWidth();
		int ih = imageOut.getHeight();
		for(int y=0; y < ih; y++){
			for(int x=0; x < iw; x++){
				double ppEvenRe = cmplxDataEven[DSHDemod.COMPONENT_pp][y*iw*2 + x*2];
				double ppEvenIm = cmplxDataEven[DSHDemod.COMPONENT_pp][y*iw*2 + x*2+1];
				
				double pmEvenRe = cmplxDataEven[DSHDemod.COMPONENT_pm][y*iw*2 + x*2];
				double pmEvenIm = cmplxDataEven[DSHDemod.COMPONENT_pm][y*iw*2 + x*2+1];
				
				double p0OddRe = cmplxDataOdd[DSHDemod.COMPONENT_p0][y*iw*2 + x*2];
				double p0OddIm = cmplxDataOdd[DSHDemod.COMPONENT_p0][y*iw*2 + x*2+1];
				
				// nom = (+,+)e*(+,-)e
				double nomRe = ppEvenRe * pmEvenRe - ppEvenIm * pmEvenIm;
				double nomIm = ppEvenRe * pmEvenIm + ppEvenIm * pmEvenRe;

				// denom = (+,0)o*(+,0)o
				double denomRe = p0OddRe * p0OddRe - p0OddIm * p0OddIm;
				double denomIm = p0OddRe * p0OddIm + p0OddIm * p0OddRe;
				
				//res = nom / denom
				double c2d2 = denomRe*denomRe + denomIm*denomIm;				
				double resRe = (nomRe*denomRe + nomIm*denomIm) / c2d2;
				double resIm = (nomIm*denomRe - nomRe*denomIm) / c2d2;
				
				double ang = FastMath.atan(Math.sqrt(FastMath.abs(-resRe * 4 - 1)))/2;
				
				//if(Double.isInfinite(ang) || Double.isNaN(ang))
				//	ang = -2;
				ang *= 180/ Math.PI;
				
				imageOut.setPixelValue(writeLock, x, y, ang);			
			}
		}		
	}
	
	public void ellipCompensatedTheta(ByteBufferImage thetaImg, WriteLock writeLock, ByteBufferImage ellipImg) {
		//for the ADSH system, the raw thetaImg isnt't actually tan²(2Theta), but this:
		// X = -( cos²(2Theta) + tan²(Chi) ) / sin²(2Theta)
		
		//so we recover Theta from:
		// sin²(2Theta) = (tan²(chi) + 1) / (1 - X) 
		
		if(thetaImg == null || ellipImg == null){			
			return;
		}
		
		ReadLock thetaReadLock = thetaImg.readLock();
		ReadLock ellipReadLock = ellipImg.readLock();
		try{
			thetaReadLock.lockInterruptibly();
			try{
				ellipReadLock.lockInterruptibly();
				try{
					int iw = thetaImg.getWidth();
					int ih = thetaImg.getHeight();
					for(int y=0; y < ih; y++){
						for(int x=0; x < iw; x++){
							double X = thetaImg.getPixelValue(thetaReadLock, x, y);
							double tanSqChi = ellipImg.getPixelValue(ellipReadLock, x, y);
							tanSqChi = -1 / tanSqChi;
							double a = FastMath.atan(FastMath.sqrt(tanSqChi));
							//a = 30 *Math.PI/180;
							tanSqChi = FastMath.pow2(FastMath.tan(a));
							double sinSq2Theta = (tanSqChi + 1) / (1 - X);
							
							
							double ang;
							ang = FastMath.asin(FastMath.sqrt(sinSq2Theta))/2;
							//ang =  FastMath.atan(FastMath.sqrt(tanSqChi));
							//ang = FastMath.atan(1 / FastMath.sqrt(-X))/2;
							thetaImg.setPixelValue(writeLock, x, y, ang*180/Math.PI);
						}
					}
				}finally{ ellipReadLock.unlock(); }
			}finally{ thetaReadLock.unlock(); }
		}catch(InterruptedException err){ }
	}
}
