package imageProc.proc.fastSpec;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Configuration for simple fast spectroscopy line fitting */ 
public class FastSpecEntry {
	
	
	/** For converting names to numbers */
	public final static String periodicTable[] = {  "-",
		"H",																			   "He",
		"Li","Be",													   "B","C","N","O","F","Ne",
		"Na","Mg",													"Al","Si","P","S","Cl","Ar",
		"K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
		"Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe",
		"Cs","Ba",
			"La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu",
				      "Hf","Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn",
		"Fr","Ra",
			"Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr",
			   "Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Uut","Uuq","Uup","Uuh","Uus","Uuo"
	};
	
	public FastSpecEntry(String id) {
		this.id = id;
		this.enable = true;
	}

	public static final String toRoman(int n){
        String roman="";
        int repeat;
        int magnitude[]={1000,900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String symbol[]={"M","CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        repeat=n/1;
        for(int x=0; n>0; x++){
            repeat=n/magnitude[x];
            for(int i=1; i<=repeat; i++){
                roman=roman + symbol[x];
            }
            n=n%magnitude[x];
        }
        return roman;
    }
	
	/** TODO: solve properly!! */
	public static final int fromRoman(String roman){		
		for(int i=1; i < 1000; i++){
			if(roman.equalsIgnoreCase(toRoman(i)))
				return i;
		}
	
		throw new RuntimeException("Too much roman!");
	}
	
	public String id;

	/** Include in fit. ANDed with frameEnable[] for given frame */
	public boolean enable;
	
	/** Identity of device and channel that this is measuring */
	public String channelID;
	
	/** Full light path (fibres) */
	public String lightPath;
	
	/** Line of sight ID (end of light path) */
	public String losName; 	
	
	/** Average dispersion over defined window. (nm/px) */ 
	public double dispersion;
	
	public int atomicNumber;	
	public int ionisationState;
	public int upperState;
	public int lowerState;	

	public double atomicMass;
	
	public double nominalWavelength;
	
	/** If difference between max and min of data is less than this, don't attempt the fit */
	public double minViableDataRange = 0;
	
	/* fitting properties */
	/** ROI box in CCD space */
	public double x0, y0, x1, y1;
	
	public double ignoreX0Ranges[][];
	
	/** Fitting min,max,init,prior-sigma */
	public double x0Min = Double.NEGATIVE_INFINITY, x0Max = Double.POSITIVE_INFINITY;
	public double x0Init = Double.NaN, x0PriorMean = Double.NaN, x0PriorSigma = Double.NaN;
	
	public double sigmaMin = 0, sigmaMax = Double.POSITIVE_INFINITY;
	public double sigmaInit = Double.NaN, sigmaPriorMean = Double.NaN, sigmaPriorSigma = Double.NaN;
	/** Set sigma to match the sigma of another component, possibly scaled e.g. "Comp1*5.00"*/
	public String sigmaMatch = null;
	
	public double powerMin = 2, powerMax = 2;
	public double powerInit = 2, powerPriorMean = Double.NaN, powerPriorSigma = Double.NaN;
	
	public double amplitudeMin = 0, amplitudeMax = Double.POSITIVE_INFINITY;
	public double amplitudeInit = Double.NaN, amplitudePriorMean = Double.NaN, amplitudePriorSigma = Double.NaN;
	/** Set amplitude to match the amplitude of another component, possibly scaled, e.g. "Comp1*5.00" */
	public String amplitudeMatch = null;
	
	/** When amplitudeInit=Nan, init the amplitude to this fraction of 
	 * maximum data in window detected by the initial guess algorithm.
	 * This is particularly useful when fitting multiple Gaussians in the same range */
	public double amplitudeInitFraction = 1.0;
	
	public double y0Min = Double.NEGATIVE_INFINITY, y0Max = Double.POSITIVE_INFINITY;
	public double y0Init = Double.NaN, y0PriorMean = Double.NaN, y0PriorSigma = Double.NaN;
	
	public double slopeMin = Double.NEGATIVE_INFINITY, slopeMax = Double.POSITIVE_INFINITY;
	public double slopeInit = Double.NaN, slopePriorMean = Double.NaN, slopePriorSigma = Double.NaN;
	
	/** If non-NaN, clamps y0 to within given percentage of value deduced by init */
	public double clampY0ToBaselineInit = Double.NaN;

	public static class FitResults {
		/* ------ Results for all frames (or 0, if singleFrame) ----- */
		public double X0[], UncertaintyX0[];
		public double Sigma[], UncertaintySigma[];
		public double Amplitude[], UncertaintyAmpltiude[];
		public double Power[], UncertaintyPower[];
		
		/** Dynamic per-frame initialisation results  */
		public double frameAmplitudeInit[];
		public double frameAmplitudePriorMean[];
		public double frameSigmaInit[];
		public double frameSigmaPriorMean[];	
		public double framePowerInit[];
		public double framePowerPriorMean[];		
		
		/** Baseline level at X0 */
		public double Y0[], UncertaintyY0[];
		
		/** Baseline slope (dy0/dx) */
		public double Slope[];
		
		/** Chi2 of fit */
		public double RMSError[];
			
		public HashMap<String, double[]> inferredQuantities;

		/** Enable flags for each frame for each entry. ANDed with entry.enable. Assumed true if null */
		public boolean[] frameEnable;	
	}
	
	public FitResults fit = new FitResults();
	
	/** transition name X_YYY(B-A)
	 * X = Element ident
	 * Y = ionisation state+1
	 * B = upper level
	 * A = lower level
	 * e.g. C_VII(8-7) */
	public String getTransition(){
			return ((atomicNumber < 1 || atomicNumber > periodicTable.length) ? "??" : periodicTable[atomicNumber])
					+ "_" + toRoman(ionisationState+1) + "(" + upperState + "-" + lowerState + ")";
	}
		
	/** See getTransition() */
	public void setTransition(String code){
		Pattern pattern = Pattern.compile("(.*?)_([MXCVI]*)\\(([0-9]*)-([0-9]*)\\)");
		Matcher matcher = pattern.matcher(code);
		if (!matcher.find()){
			atomicMass = -1;
			ionisationState = -1;
			upperState = -1;
			lowerState = -1;
			return;
		}
			
		atomicNumber = OneLiners.findBestMatchingString(periodicTable, matcher.group(1));
		ionisationState = fromRoman(matcher.group(2))-1;
		upperState = Algorithms.mustParseInt(matcher.group(3));
		lowerState = Algorithms.mustParseInt(matcher.group(4));
	}
	
	public HashMap<String, Object> toMap() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("id", id); 
		map.put("channelID", channelID);
		map.put("atomicNumber",atomicNumber);
		map.put("ionisationState",ionisationState);
		map.put("upperState",upperState);
		map.put("lowerState",lowerState);
		map.put("atomicMass",atomicMass);
		map.put("nominalWavelength",nominalWavelength);
		map.put("x0",x0);
		map.put("y0",y0);
		map.put("x1",x1);
		map.put("y1",y1);
		/*map.put("fitX0",fitX0);
		map.put("fitSigma",fitSigma);
		map.put("fitAmplitude",fitAmplitude);
		map.put("fitY0",fitY0);
		map.put("fitSlope",fitSlope);
		map.put("inferredQuantities", inferredQuantities);*/
		return map;
	}
	
	public static final FastSpecEntry fromMap(HashMap<String, Object> map) {
		
		String channelID = (String) map.get("channelID");
		FastSpecEntry entry = new FastSpecEntry(channelID);
		
		entry.atomicNumber = (int) map.get("atomicNumber");
		entry.ionisationState = (int) map.get("ionisationState");
		entry.upperState = (int) map.get("upperState");
		entry.lowerState = (int) map.get("lowerState");
		entry.atomicMass = (int) map.get("atomicMass");
		entry.nominalWavelength = ((Number)map.get("nominalWavelength")).doubleValue();
		entry.x0 = (double) ((Number)map.get("x0")).doubleValue();
		entry.y0 = (double) ((Number)map.get("y0")).doubleValue();
		entry.x1 = (double) ((Number)map.get("x1")).doubleValue();
		entry.y1 = (double) ((Number)map.get("y1")).doubleValue();
		
		/*entry.fitX0 = (double[]) map.get("fitX0");
		entry.fitSigma = (double[]) map.get("fitSigma");
		entry.fitAmplitude = (double[]) map.get("fitAmplitude");
		entry.fitY0 = (double[]) map.get("fitY0");
		entry.fitSlope = (double[]) map.get("fitSlope");*/
		
		return entry;
	}
}

