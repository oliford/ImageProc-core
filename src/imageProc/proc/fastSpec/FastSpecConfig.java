package imageProc.proc.fastSpec;

import java.util.ArrayList;
import java.util.List;

import imageProc.proc.fastSpec.FastSpecEntry.FitResults;

public class FastSpecConfig {
	
	public FastSpecConfig() {
		specEntries = new ArrayList<FastSpecEntry>();
	}
	
	public ArrayList<FastSpecEntry> specEntries;

	/** Both cordinates are just pixels */
	public static final int COORDTYPE_PIXELS = 0;
	
	/** Coordinates are CCD physical (m from CCD centre) */
	public static final int COORDTYPE_CCDGEOM = 1;
	
	/** Wavelength coordinate in one direction from metadata of wavlength for each pixel. Other axis as pixels */
	public static final int COORDTYPE_WAVELENGTH_AXIS = 2;
	
	/** Wavelength coordinate from coefficients in both directions (in config). 'wavelenIsY' is ignored */
	//public static final int COORDTYPE_WAVELENGTH_COEFFS_2D = 3;
			
	public int coordType = COORDTYPE_PIXELS;
	
	/** which direction is wavelength? */
	public boolean wavelenIsY = false;
	
	/** Fit overlapped windows with at the same time (multi-gaussian) */
	public boolean multiGauss = false;
	
	/** Calculate errors on fit parameters */
	public boolean calcFitUncertainty = true;
	
	/** Calculate marginal uncertanties by inverting to covariance matrix. 
	 * Otherwise calcuation conditional uncertainties. */
	public boolean marginalUncertainties = true;
	
	/** If non-zero the inverse covariance matrix will
	 * be reconditioned with Mat.makeSPD() to force the given
	 * minimum eigenvalue */
	public double minInvCovarianceEigenvalue = 0.0;
	
	public FastSpecEntry getPointByName(String pointName) {
		for(FastSpecEntry entry : specEntries){
			if(pointName.equals(entry.id)) {
				if(entry.fit == null)
					entry.fit = new FitResults(); 
				return entry;
			}
		}
		return null;
	}

	public boolean saveResults = false;
	public boolean saveMetadata = false;

	/** Fit only frames maked as 'good' */
	public boolean processOnlyGoodFrames = false;
	
	/** Save results only for frames marked as 'good' */
	public boolean saveOnlyGoodFrames = false;
	
	/** Subtract (in quadrature) instrument function width from fitted width when calculating Ti */
	public boolean subtractInstrumentFunctionFromTi = true;	
	
	public boolean forceBackupInitGuess = false;
	
	public static enum Optimiser {
		HookeAndJeeves, GoldenSectionCoordinateDescent, GoldenSectionGradientDescent;
		
		public static String[] getStrings(){
			Optimiser[] vals = values();
			String ret[] = new String[vals.length];
			for(int i=0; i < vals.length; i++)
				ret[i] = vals[i].toString();
			return ret;
		}
	}
	
	public Optimiser optimser = Optimiser.HookeAndJeeves;
	public int numIterations = 200;
	
	/** Coefficients to arrive at wavelength in m from pixel (x,y) values, starting at 0,0 in the corner.
	 * 
		l=    1   x   x²  x³
		  -------------------
		1 | a00 a01 a02 a03
		y | a10 a11 a12 a13
		y²| a20 a21 a22 a23
		x and y are in pixel values from the corner, ignore (so with) AOI
		
		Default as example: 500nm in the corner, rising 1nm/pixel in x and 1pm/pixel in y skew
	 */
	public double wavelengthCoeffs[][] = new double[][]{{ 500e-9, 1e-9, 0}, { 1e-12, 0, 0 }};

	/** List of inferred quantities in the hashmaps in each specEntry */
	public List<String> inferredQuantities = new ArrayList<String>();

	/** Throw exception and stop if any cost function is NaN */
	public boolean errorOnNaNFits = false;

	/** Don't fit data points above this level */
	public double saturationLevel = Double.NaN;
	
	public static enum InitMode {
		/** Initialise fitting as specified in entry configuration */
		CONFIG,
		
		/** Initialise fitting to final values of last frame */
		LAST_FRAME,
		
		/** Don't reinitialise fitting, start from the value as it currently is */
		CONTINUE,;

	};
	
	/** How to set initial values for fitting parameters */ 
 	public InitMode initMode = InitMode.CONFIG;
	
 	public static enum AmpFitMode {
 		/** No special amplitude treatment. Fit all parameters in one go.*/
 		NONE,
 		
 		/** Only fit the amplitudes. Leave everything else as initial values */
 		AMP_ONLY,
 		
 		/** Fit amplitude first with fixed other parameters, then fit everything */
 		AMP_FIRST
 	}
 		
	/** Special treatment of amplitudes in fitting*/
 	public AmpFitMode amplitudeFit = AmpFitMode.NONE;

	/** Artifically increase errors by a gaussian at the given location by the given amount */
	public double expandErrorsX0 = Double.NaN;
	public double expandErrorsXSigma = Double.NaN;
	public double expandErrorsValue = Double.NaN;
	
	/** Forces use of Java LAPACK implementation. The fortran version is a bit faster but when it crashes, it locks the whole JVM.
	 * The Java version can be run multithreaded though, so may end up faster anyway. */
	public boolean forceNonNativeLAPACK = true;

	/** Fit only frames where max of whole image is above given threshold. */
	public double thresholdMaxImage = Double.NaN;				

}
