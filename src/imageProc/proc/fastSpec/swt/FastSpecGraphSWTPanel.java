package imageProc.proc.fastSpec.swt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import binaryMatrixFile.AsciiMatrixFile;
import binaryMatrixFile.BinaryMatrixFile;
import imageProc.core.ImageProcUtil;
import imageProc.graph.JFreeChartGraph;
import imageProc.graph.Series;
import imageProc.proc.fastSpec.FastSpecConfig;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessor;
import imageProc.proc.fastSpec.FastSpecProcessor.FrameData;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class FastSpecGraphSWTPanel {
	private static String[] basicPlotTypes = new String[] { "Single Frame Fit", "Intensity vs time", "Sigma vs time", "Power vs time" , "x0 vs time", "y0 vs time", "slope vs time", "RMS error vs time" };
	
	private final String seriesName[] = { "data", "error", "init", "baseline", "fit", "residual" };
	private final int seriesColor[][] = { { 0, 0, 0 }, {255, 0, 255}, { 0, 255, 255 }, { 255, 0, 0 }, { 0, 0, 255 }, { 0,255,0 }, {255,255,0} };

	public final static int PLOTTYPE_FRAME_FIT = 0;
	public final static int PLOTTYPE_TIME_INTENSITY = 1;
	public final static int PLOTTYPE_TIME_FWHM = 2;
	public final static int PLOTTYPE_TIME_POWER = 3;
	public final static int PLOTTYPE_TIME_X0 = 4;
	public final static int PLOTTYPE_TIME_Y0 = 5;
	public final static int PLOTTYPE_TIME_SLOPE = 6;
	public final static int PLOTTYPE_TIME_CHI2 = 7;
		
	private FastSpecProcessor proc;
	private FastSpecSWTController ctrl;
	
	private Composite swtGraphComp;
	private SashForm swtSashForm2;

	private Combo plotTypeCombo;
	private JFreeChartGraph graph;
	
	private Group axisTypeGroup;
	private Button axisTypeFrames;
	private Button axisTypeTime;
	private Button axisTypeProfile;
	
	private Text entryFilterTextbox;
	private Button saveGraphDataButton;
	private Button logCheckbox;
	private Button limitsCheckbox;
	
	/** Currently plotted data */
	private ArrayList<Series> seriesList;
	
	public FastSpecGraphSWTPanel(FastSpecProcessor proc, FastSpecSWTController ctrl, Composite graphTabComp) {
		this.proc = proc;
		this.ctrl = ctrl;
						
		swtSashForm2 = new SashForm(graphTabComp, SWT.VERTICAL | SWT.BORDER);
		swtSashForm2.setLayout(new FillLayout());

		swtGraphComp = new Group(swtSashForm2, SWT.BORDER);
		swtGraphComp.setLayout(new GridLayout(8, false));
		
		Label lPT = new Label(swtGraphComp, SWT.NONE);
		lPT.setText("Plot: ");
		plotTypeCombo = new Combo(swtGraphComp, SWT.DROP_DOWN);
		plotTypeCombo.setItems(new String[] { "Single Frame Fit", "Intensity vs time", "Sigma vs time", "x0 vs time", "y0 vs time" });
		plotTypeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		plotTypeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ctrl.generalControllerUpdate(); } });
		
		axisTypeGroup = new Group(swtGraphComp, SWT.BORDER);
		axisTypeGroup.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		axisTypeGroup.setLayout(new GridLayout(4, false));
		
		Label lAT = new Label(axisTypeGroup, SWT.NONE); lAT.setText("Axis: ");		
		axisTypeFrames = new Button(axisTypeGroup, SWT.RADIO); 
		axisTypeFrames.setText("Frames");
		axisTypeFrames.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		axisTypeFrames.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ctrl.generalControllerUpdate(); } });
		
		axisTypeTime = new Button(axisTypeGroup, SWT.RADIO); 
		axisTypeTime.setText("Time");
		axisTypeTime.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		axisTypeTime.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ctrl.generalControllerUpdate(); } });
		
		axisTypeProfile = new Button(axisTypeGroup, SWT.RADIO); 
		axisTypeProfile.setText("Profile");
		axisTypeProfile.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		axisTypeProfile.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ctrl.generalControllerUpdate(); } });
		
		Label lFT = new Label(swtGraphComp, SWT.NONE); lFT.setText("Filter: ");
		entryFilterTextbox = new Text(swtGraphComp, SWT.NONE); 
		entryFilterTextbox.setText("");
		entryFilterTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ctrl.generalControllerUpdate(); } });
		entryFilterTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		
		saveGraphDataButton = new Button(swtGraphComp, SWT.PUSH); 
		saveGraphDataButton.setText("Save");
		saveGraphDataButton.setToolTipText("Saves all graph traces to "+System.getProperty("java.io.tmpdir") + "/FastSpecProcessor/xxxx.txt");
		saveGraphDataButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		saveGraphDataButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { dumpGraphData(event, false); } });
		
		logCheckbox = new Button(swtGraphComp, SWT.CHECK); 
		logCheckbox.setText("logY");
		logCheckbox.setToolTipText("Display y-axis as log-plot");
		logCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		logCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ctrl.generalControllerUpdate(); } });
		
		limitsCheckbox = new Button(swtGraphComp, SWT.CHECK); 
		limitsCheckbox.setText("limits");
		limitsCheckbox.setToolTipText("Display limits and priors");
		limitsCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		limitsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ctrl.generalControllerUpdate(); } });
				
		graph = new JFreeChartGraph(swtGraphComp, SWT.NONE);
		graph.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 8, 1));

		graph.getComposite().addKeyListener(new KeyListener() {			
			@Override public void keyReleased(KeyEvent event) { keyEvent(event); }
			@Override public void keyPressed(KeyEvent e) { }
		});
	   
	}
	
	private void keyEvent(KeyEvent event){
		String plotType = plotTypeCombo.getText();
		TableItem[] selected = ctrl.getSelectedTableEntries();
		FastSpecConfig cfg = proc.getConfig();
		
		String entryID = selected[0].getText(1);
		FastSpecEntry specEntry = cfg.getPointByName(entryID);
		
		double p[] = graph.getLastMousePos();
		switch (event.keyCode) {
			case 'i': 
				if(plotType.startsWith("Intensity"))
					specEntry.amplitudeInit = p[1];
				else if(plotType.startsWith("Sigma"))
					specEntry.sigmaInit = p[1];
				else if(plotType.startsWith("x0"))
					specEntry.x0Init = p[1];
				else if(plotType.startsWith("y0"))
					specEntry.y0Init = p[1];
			case 'n': 
				if(plotType.startsWith("Intensity"))
					specEntry.amplitudeMin = p[1];
				else if(plotType.startsWith("Sigma"))
					specEntry.sigmaMin = p[1];
				else if(plotType.startsWith("x0"))
					specEntry.x0Min = p[1];
				else if(plotType.startsWith("y0"))
					specEntry.y0Min = p[1];
			case 'x': 
				if(plotType.startsWith("Intensity"))
					specEntry.amplitudeMax = p[1];
				else if(plotType.startsWith("Sigma"))
					specEntry.sigmaMax = p[1];
				else if(plotType.startsWith("x0"))
					specEntry.x0Max = p[1];
				else if(plotType.startsWith("y0"))
					specEntry.y0Max = p[1];
			default:
				break;
		}
	}

	protected void dumpGraphData(Event event, boolean binary) {
						
		for(int i=0; i < seriesList.size(); i++){
			Series series = seriesList.get(i);
			
			if(binary) {
				Mat.mustWriteBinary(System.getProperty("java.io.tmpdir") + "/FastSpecProcessor/"+ ((series.name() != null) ? series.name() : i) + ".bin",
						new double[][]{ series.x, series.data }, true);
			}else {
				Mat.mustWriteAscii(System.getProperty("java.io.tmpdir") + "/FastSpecProcessor/"+ ((series.name() != null) ? series.name() : i) + ".txt",
						new double[][]{ series.x, series.data }, true);
				
			}
		}
	}


	protected void doUpdate() {

		FastSpecConfig config = proc.getConfig();
		
		int selIndex = plotTypeCombo.getSelectionIndex();
		String selText = plotTypeCombo.getText();
		plotTypeCombo.setItems(basicPlotTypes);
		if(config.inferredQuantities != null){
			for(String name : config.inferredQuantities)
				plotTypeCombo.add(name);
		}
		if(selIndex >= 0)
			plotTypeCombo.select(selIndex);
		else
			plotTypeCombo.setText(selText);
		graph.setLog10Y(logCheckbox.getSelection());
	}

	void updateFitGraph() {
		
		TableItem[] selected = ctrl.getSelectedTableEntries();
		
		if (plotTypeCombo.getSelectionIndex() == PLOTTYPE_FRAME_FIT) {
			plotSingleFit(selected);
		}else if(axisTypeFrames.getSelection()){
			plotTimeTrace(selected, null);
		}else if(axisTypeTime.getSelection()){
			double x[] = null;
			
			x = (double[]) proc.getSeriesMetaData("/w7x/archive/timeFromT1");
			if(x == null){
				String name = proc.getCompleteSeriesMetaDataMap().findEndsWith("/time", true);        	
	        	if(name != null){
	        		x = new double[proc.getConnectedSource().getNumImages()];
	        		for(int i=0; i < x.length; i++){
		        		Object o = proc.getConnectedSource().getImageMetaData(name, i);
		        		if(o != null){
		        			try{
		        				x[i] = (double)o;
		        			}catch(ClassCastException err) { }
		        		}
	        		}
	        		for(int i=x.length-1; i >= 0; i--){
	        			x[i] -= x[0];
	        		}
	        	}
			}
			
			plotTimeTrace(selected, x);				
		}else{
			plotProfile();		
		}
	}


	private void plotProfile() {
		seriesList = new ArrayList<Series>();
		FastSpecConfig cfg = proc.getConfig();
		
		int nEntries = cfg.specEntries.size();
		double x[] = new double[nEntries];
		double y[] = new double[nEntries];
		double yUpper[] = new double[nEntries];
		double yLower[] = new double[nEntries];
		double yInit[] = new double[nEntries];
		double yMin[] = new double[nEntries];
		double yMax[] = new double[nEntries];
		
		/** Lower, Centre, Upper of prior */
		double yPrior[][] = new double[3][nEntries];
		
		TableItem[] selected = ctrl.getSelectedTableEntries();
		double xSelected[] = new double[nEntries];
		double ySelected[] = new double[nEntries];
				
		int frameIndex = proc.getSelectedSourceIndex();
		
		String filter = entryFilterTextbox.getText().trim();
		int n=0, nSelected=0;
		for(FastSpecEntry specEntry : cfg.specEntries){
			
			//if(filter.length() > 0 && !specEntry.id.contains(filter))
			if(filter.length() > 0 && !specEntry.id.matches(filter))
				continue; //ignore entries not matching filter
			
						
			double profileX[] = null;
			if(specEntry.fit != null && specEntry.fit.inferredQuantities != null){
				profileX = specEntry.fit.inferredQuantities.get("ProfileX");
			}
				
			if(profileX == null){
				Logger.getLogger(this.getClass().getName()).log(Level.FINE,
						"Entry '"+specEntry.id+"' is missing inferred quantity profileX");
				x[n] = specEntry.y0;
				
			}else{			
				x[n] = profileX[frameIndex];
			}
			
			TraceData d = getTraceData(specEntry);			
			
			if(!Double.isFinite(x[n]) || d == null || d.y == null || !Double.isFinite(d.y[frameIndex]))
				continue;
			
			y[n] = d.y[frameIndex];
			yLower[n] = d.y[frameIndex] - ((d.yErr == null) ? 0 : (2.35*FastMath.abs(d.yErr[frameIndex])/2));
			yUpper[n] = d.y[frameIndex] + ((d.yErr == null) ? 0 : (2.35*FastMath.abs(d.yErr[frameIndex])/2));
			yInit[n] = d.yInit != null ? d.yInit[frameIndex] : 0;
			yMin[n] = d.yMin != null ? d.yMin[frameIndex] : 0;
			yMax[n] = d.yMax != null ? d.yMax[frameIndex] : 0;
			yPrior[0][n] = d.yPriorMean != null ? (d.yPriorMean[frameIndex] - d.yPriorSigma[frameIndex] * 2.35 / 2) : 0;
			yPrior[1][n] = d.yPriorMean != null ? d.yPriorMean[frameIndex] : 0;
			yPrior[2][n] = d.yPriorMean != null ? (d.yPriorMean[frameIndex] + d.yPriorSigma[frameIndex] * 2.35 / 2) : 0;
			
			boolean isSelected = false;
			for (int i = 0; i < selected.length; i++) {
				String entryID = selected[i].getText(1);
				if(specEntry.id.equals(entryID)){
					isSelected = true;
					break;
				}
			}
			
			if(isSelected){
				xSelected[nSelected] = x[n];
				ySelected[nSelected] = y[n];
				nSelected++;
				
			}else{
				
			}
			n++;
		}
		
		x = Arrays.copyOf(x, n);
		y = Arrays.copyOf(y, n);
		yUpper = Arrays.copyOf(yUpper, n);
		yLower = Arrays.copyOf(yLower, n);
		yInit = Arrays.copyOf(yInit, n);
		yMin = Arrays.copyOf(yMin, n);
		yMax = Arrays.copyOf(yMax, n);
		yPrior[0] = Arrays.copyOf(yPrior[0], n);
		yPrior[1] = Arrays.copyOf(yPrior[1], n);
		yPrior[2] = Arrays.copyOf(yPrior[2], n);
	
		Series s;
		s = new Series("Profile", x, y, yLower, yUpper); seriesList.add(s); 
		
		if(nSelected == 1){
			xSelected = Arrays.copyOf(xSelected, nSelected);
			ySelected = Arrays.copyOf(ySelected, nSelected);
		}else{
			xSelected = new double[]{ xSelected[0], xSelected[0] };
			ySelected = new double[]{ ySelected[0], ySelected[0] };			
		}
		s = new Series("selected", xSelected, ySelected); s.points = true; s.color = new int[]{ 255,255,0, 10}; seriesList.add(s);
		//s = new Series("selected2", xSelected, ySelected); s.points = true; s.color = new int[]{ 255,255,0, 10}; seriesList.add(s); 
		
		if(limitsCheckbox.getSelection()) {
			
			s = new Series("min", x, yMin); seriesList.add(s); 
			s = new Series("max", x, yMax); seriesList.add(s); 
			s = new Series("init", x, yInit); seriesList.add(s); 
			
			s = new Series("prior -1sigma", x, yPrior[0]); seriesList.add(s); 
			s = new Series("prior mean", x, yPrior[1]); seriesList.add(s); 
			s = new Series("prior +1sigma", x, yPrior[2]); seriesList.add(s); 
		}
		
		graph.setData(seriesList);
	}

	private class TraceData{
		double y[], yErr[], yMin[], yMax[];
		double yInit[], yPriorMean[], yPriorSigma[];
	}
	
	private void plotTimeTrace(TableItem[] selected, double xAxis[]) {
		seriesList = new ArrayList<Series>();
		FastSpecConfig cfg = proc.getConfig();
		
		for (int i = 0; i < selected.length; i++) {
			String entryID = selected[i].getText(1);

			FastSpecEntry specEntry = cfg.getPointByName(entryID);
			
			TraceData d = getTraceData(specEntry);			
			if(d == null)
				return;

			double x[] = (xAxis != null) ? xAxis : Mat.linspace(0,d.y.length - 1, 1.0);
			
			//strip NaNs
			int n=0;
			double x2[] = new double[x.length];
			double y2[] = new double[x.length];
			double yLow[] = new double[x.length];
			double yHigh[] = new double[x.length];
			for(int j=0; j < x.length; j++){
				if(!Double.isNaN(x[j]) && !Double.isNaN(d.y[j])){
					x2[n] = x[j];
					y2[n] = d.y[j];
					if(d.yErr != null){
						yLow[n] = d.y[j] - 2.35*FastMath.abs(d.yErr[j])/2;
						yHigh[n] = d.y[j] + 2.35*FastMath.abs(d.yErr[j])/2;
					}
					n++;
				}
			}
			x2 = Arrays.copyOf(x2, n);
			y2 = Arrays.copyOf(y2, n);
			yLow = Arrays.copyOf(yLow, n);
			yHigh = Arrays.copyOf(yHigh, n);
			
			Series s = new Series(specEntry.id, x2, y2, yLow, yHigh);
			seriesList.add(s);

			if(d.yMin != null && !Double.isNaN(d.yMin[0]))
				seriesList.add(new Series("min", x, d.yMin));

			if(d.yMax != null && !Double.isNaN(d.yMax[0]))
				seriesList.add(new Series("max", x, d.yMax));

			if(d.yInit != null && !Double.isNaN(d.yInit[0]))
				seriesList.add(new Series("init", x, d.yInit));

			if(d.yPriorMean != null && d.yPriorSigma != null) {
				double p[] = new double[x.length];
				double m[] = new double[x.length];
				for(int j=0; j < x.length; j++) {
					m[j] = d.yPriorMean[j] - d.yPriorSigma[j]*2.35/2;
					p[j] = d.yPriorMean[j] + d.yPriorSigma[j]*2.35/2;
				}

				seriesList.add(new Series("prior0", x2, d.yPriorMean));
				seriesList.add(new Series("prior-HWHM", x2, m));
				seriesList.add(new Series("prior+HWHM", x2, p));
			}
		}

		graph.setData(seriesList);
	}

	private TraceData getTraceData(FastSpecEntry specEntry) {
		TraceData d = new TraceData();
		switch (plotTypeCombo.getSelectionIndex()) {
			case PLOTTYPE_TIME_INTENSITY:
				d.y = specEntry.fit.Amplitude;
				d.yErr = specEntry.fit.UncertaintyAmpltiude;
				d.yInit = specEntry.fit.frameAmplitudeInit != null ? specEntry.fit.frameAmplitudeInit : Mat.fillArray(specEntry.amplitudeInit, d.y.length);
				d.yMin = Mat.fillArray(specEntry.amplitudeMin, d.y.length); 
				d.yMax = Mat.fillArray(specEntry.amplitudeMax, d.y.length);
				d.yPriorMean = specEntry.fit.frameAmplitudePriorMean != null ? specEntry.fit.frameAmplitudePriorMean : Mat.fillArray(specEntry.amplitudePriorMean, d.y.length);
				d.yPriorSigma = Mat.fillArray(specEntry.amplitudePriorSigma, d.y.length);
				break;
			case PLOTTYPE_TIME_FWHM:
				d.y = specEntry.fit.Sigma;
				d.yErr = specEntry.fit.UncertaintySigma;
				d.yInit = specEntry.fit.frameSigmaInit != null ? specEntry.fit.frameSigmaInit : Mat.fillArray(specEntry.sigmaInit, d.y.length);
				d.yMin = Mat.fillArray(specEntry.sigmaMin, d.y.length); 
				d.yMax = Mat.fillArray(specEntry.sigmaMax, d.y.length);
				d.yPriorMean = specEntry.fit.frameSigmaPriorMean != null ? specEntry.fit.frameSigmaPriorMean : Mat.fillArray(specEntry.sigmaPriorMean, d.y.length);
				d.yPriorSigma = Mat.fillArray(specEntry.sigmaPriorSigma, d.y.length);
				break;
			case PLOTTYPE_TIME_POWER :
				d.y = specEntry.fit.Power;
				d.yErr = specEntry.fit.UncertaintyPower;
				d.yInit = specEntry.fit.framePowerInit != null ? specEntry.fit.framePowerInit : Mat.fillArray(specEntry.powerInit, d.y.length);
				d.yMin = Mat.fillArray(specEntry.powerMin, d.y.length); 
				d.yMax = Mat.fillArray(specEntry.powerMax, d.y.length);
				d.yPriorMean = specEntry.fit.framePowerPriorMean != null ? specEntry.fit.framePowerPriorMean : Mat.fillArray(specEntry.powerPriorMean, d.y.length);
				d.yPriorSigma = Mat.fillArray(specEntry.powerPriorSigma, d.y.length);
				break;
			case PLOTTYPE_TIME_X0:
				d.y = specEntry.fit.X0;
				d.yErr = specEntry.fit.UncertaintyX0;
				d.yInit = Mat.fillArray(specEntry.x0Init, d.y.length);
				d.yMin = Mat.fillArray(specEntry.x0Min, d.y.length); 
				d.yMax = Mat.fillArray(specEntry.x0Max, d.y.length);
				d.yPriorMean = Mat.fillArray(specEntry.x0PriorMean, d.y.length);
				d.yPriorSigma = Mat.fillArray(specEntry.x0PriorSigma, d.y.length);
				break;
			case PLOTTYPE_TIME_Y0:
				d.y = specEntry.fit.Y0;
				d.yErr = specEntry.fit.UncertaintyY0;
				d.yInit = Mat.fillArray(specEntry.y0Init, d.y.length);
				d.yMin = Mat.fillArray(specEntry.y0Min, d.y.length); 
				d.yMax = Mat.fillArray(specEntry.y0Max, d.y.length);
				d.yPriorMean = Mat.fillArray(specEntry.y0PriorMean, d.y.length);
				d.yPriorSigma = Mat.fillArray(specEntry.y0PriorSigma, d.y.length);
				break;
			case PLOTTYPE_TIME_SLOPE:
				d.y = specEntry.fit.Slope;
				d.yErr = null; //specEntry.fitUncertaintySlope;
				d.yInit = Mat.fillArray(specEntry.slopeInit, d.y.length);
				d.yMin = Mat.fillArray(specEntry.slopeMin, d.y.length); 
				d.yMax = Mat.fillArray(specEntry.slopeMax, d.y.length);
				d.yPriorMean = Mat.fillArray(specEntry.slopePriorMean, d.y.length);
				d.yPriorSigma = Mat.fillArray(specEntry.slopePriorSigma, d.y.length);
				break;
			case PLOTTYPE_TIME_CHI2:
				d.y = specEntry.fit.RMSError;
				d.yErr = null;						
				break;
			default:
				String name = plotTypeCombo.getText();
				d.y = (specEntry.fit.inferredQuantities == null) ? null : specEntry.fit.inferredQuantities.get(name);
				d.yErr = (specEntry.fit.inferredQuantities == null) ? null : specEntry.fit.inferredQuantities.get(name + "_uncertainty");
				break;					
		}
		
		return (d.y != null) ? d : null;
	}

	private void plotSingleFit(TableItem[] selected) {
		if (selected.length == 1) {
			String entryID = selected[0].getText(1);

			seriesList = new ArrayList<Series>();

			FrameData frameD = proc.getCurrentFrameData(entryID);
			
			if (frameD != null) {
				double d[][] = frameD.all();
				for (int i = 1; i < d.length; i++) {
					String name = (i <= seriesName.length) ? seriesName[i-1] : ("g_" + frameD.componentIDs[i - seriesName.length - 1]); 
					Series s = new Series(name, d[0], d[i]);
					
					s.color = (i <= seriesColor.length) ? seriesColor[i - 1] : new int[]{ 255, 255, 255 };
					seriesList.add(s);
				}
			}

			graph.setData(seriesList);
		} else {
			graph.setData(null);
		}
	}

	void updateFitResults() {
		StringBuilder infoStrB = new StringBuilder();
		infoStrB.append("Fit results (in mm):\n");
		
		TableItem[] selected = ctrl.getSelectedTableEntries();
		FastSpecConfig cfg = proc.getConfig();
		
		for (int i = 0; i < selected.length; i++) {
			
			String entryID = selected[i].getText(1);
			FastSpecEntry specEntry = cfg.getPointByName(entryID);
			if(specEntry == null)
				continue;
			
			infoStrB.append(specEntry.id + ":");
			
			if(specEntry.fit.X0 == null){
				infoStrB.append("No fit\n");
				continue; //no data
			}
			
			int iF = specEntry.fit.X0.length == 1 ? 0 : proc.getSelectedSourceIndex();

			
			infoStrB.append("x0=" + String.format("%6.5g", specEntry.fit.X0[iF]) + " intgrl="
					+ String.format("%6.5g", specEntry.fit.Amplitude[iF]) + ", sigma="
					+ String.format("%6.5g", specEntry.fit.Sigma[iF]) + "\n");

		}
		
		//swtFitInfoLabel.setText(infoStrB.toString());
		// swtGroup.pack();

	}

}
