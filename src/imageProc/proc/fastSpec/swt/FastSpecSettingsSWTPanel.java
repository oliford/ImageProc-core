package imageProc.proc.fastSpec.swt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import imageProc.core.ImageProcUtil;
import imageProc.proc.fastSpec.FastSpecConfig;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessor;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import imageProc.proc.fastSpec.FastSpecConfig.AmpFitMode;
import imageProc.proc.fastSpec.FastSpecConfig.InitMode;
import imageProc.proc.fastSpec.FastSpecConfig.Optimiser;

public class FastSpecSettingsSWTPanel {

	private FastSpecProcessor proc;
	private FastSpecSWTController ctrl;
	
	private Button multiGaussCheckbox;
	Button drawBoxesCheckbox;
	Button setBoxesCheckbox;
	private Button wavelengthIsYCheckbox;	
	private Combo coordTypeCombo;
	private Combo optimiserCombo;
	private Combo initModeCombo;
	private Spinner numIterationsSpinner;
	private Text thresholdMaxImageTextbox;
	private Button copyAllButton;
	private Button pasteAllButton;
	private Button backupInitGuess;
	private Combo amplitudeModeCombo;
	private Combo calcFitUncertaintyCombo;	
	private Button fitOnlyGoodFramesCheckbox;
	private Spinner nThreadsSpinner;
	
	private Composite settingsTabComp;

	//TODO: clampY0ToBaselineInit
	
	public FastSpecSettingsSWTPanel(FastSpecProcessor proc, FastSpecSWTController ctrl, Composite settingsTabComp) {
		this.proc = proc;
		this.ctrl = ctrl;
		this.settingsTabComp = settingsTabComp;
				
		drawBoxesCheckbox = new Button(settingsTabComp, SWT.CHECK);
		drawBoxesCheckbox.setText("Draw boxes");
		drawBoxesCheckbox.setSelection(true);
		drawBoxesCheckbox.setToolTipText("Draw boxes of the fit regions on the image. Selected in magenta");
		drawBoxesCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));

		setBoxesCheckbox = new Button(settingsTabComp, SWT.CHECK);
		setBoxesCheckbox.setText("Set boxes");
		setBoxesCheckbox.setToolTipText("Allow drag-selection of the selected entry on the image.");
		setBoxesCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));		
		
		multiGaussCheckbox = new Button(settingsTabComp, SWT.CHECK);
		multiGaussCheckbox.setText("Multi-Gauss");
		multiGaussCheckbox.setToolTipText("Fit overlapping windows in a single multi-Gaussian fit.");
		multiGaussCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		multiGaussCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lCT = new Label(settingsTabComp, SWT.NONE); lCT.setText("Coordiantes:");
		coordTypeCombo = new Combo(settingsTabComp, SWT.DROP_DOWN);
		coordTypeCombo.setItems(new String[] { "Pixels", "CCDGeom", "Wavelen Axis", "Wavelen Coeffs" });
		coordTypeCombo.setToolTipText("Sets x-axis used for fits and fit parameters.\nPixels = Simple pixel number, Wavelen Axis = Wavelength from metadata (e.g. on USB spectrometers)");
		coordTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		coordTypeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		wavelengthIsYCheckbox = new Button(settingsTabComp, SWT.CHECK);
		wavelengthIsYCheckbox.setText("Wavelenth vertical");
		wavelengthIsYCheckbox.setToolTipText("The fit axis (wavelength) goes up and down image, otherwise left-right)");
		wavelengthIsYCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		wavelengthIsYCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lO = new Label(settingsTabComp, SWT.NONE); lO.setText("Optimiser: ");
		optimiserCombo = new Combo(settingsTabComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		optimiserCombo.setToolTipText("The optimiser algorithm to use for the fitting.");
		optimiserCombo.setItems(Optimiser.getStrings());
		optimiserCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		optimiserCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		backupInitGuess = new Button(settingsTabComp, SWT.CHECK);
		backupInitGuess.setText("Alternative init guess");
		backupInitGuess.setToolTipText("Alternative initial guess algorithm that may or may not be better.");
		backupInitGuess.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		backupInitGuess.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		
		Label lIT = new Label(settingsTabComp, SWT.NONE); lIT.setText("Init: ");
		initModeCombo = new Combo(settingsTabComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		initModeCombo.setToolTipText("How to set initial values");		
		initModeCombo.setItems(Mat.toStringArray(InitMode.values()));		
		initModeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		initModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lAM = new Label(settingsTabComp, SWT.NONE); lAM.setText("Amp: ");
		amplitudeModeCombo = new Combo(settingsTabComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		amplitudeModeCombo.setToolTipText("Only fit amplitudes (first, or at all), use fixed intial values of all other parameters.");		
		amplitudeModeCombo.setItems(Mat.toStringArray(AmpFitMode.values()));
		amplitudeModeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		amplitudeModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNI = new Label(settingsTabComp, SWT.NONE); lNI.setText("Num. Iters.: ");
		numIterationsSpinner = new Spinner(settingsTabComp, SWT.NONE);
		numIterationsSpinner.setValues(100, 0, Integer.MAX_VALUE, 0, 1, 10);
		numIterationsSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		numIterationsSpinner.setToolTipText("Number of interations of the optimiser.");
		numIterationsSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNT = new Label(settingsTabComp, SWT.NONE); lNT.setText("Num threads:");
		nThreadsSpinner = new Spinner(settingsTabComp, SWT.NONE);
		nThreadsSpinner.setValues(4, 0, Integer.MAX_VALUE, 0, 1, 10);
		nThreadsSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
		nThreadsSpinner.setToolTipText("Number of threads doing frames in parallel.");
		nThreadsSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lTM = new Label(settingsTabComp, SWT.NONE); lTM.setText("Threshold max:");
		thresholdMaxImageTextbox = new Text(settingsTabComp, SWT.NONE);
		thresholdMaxImageTextbox.setText("NaN");
		thresholdMaxImageTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		thresholdMaxImageTextbox.setToolTipText("Don't fit frames where the image maximum value is less than this threshold. Set to NaN or blank to disable.");
		thresholdMaxImageTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		Label lT = new Label(settingsTabComp, SWT.NONE); lT.setText("Table:");
		copyAllButton = new Button(settingsTabComp, SWT.PUSH);
		copyAllButton.setText("Copy");
		copyAllButton.setToolTipText("Copy all entries in table to clipboard in a format that is recognised by LibreOffice/Excel");
		copyAllButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		copyAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { copyAllButtonEvent(event); } });

		pasteAllButton = new Button(settingsTabComp, SWT.PUSH);
		pasteAllButton.setText("Paste");
		pasteAllButton.setToolTipText("Paste all entries from clipboard into the table, e.g. after modification in LibreOffice/Excel");
		pasteAllButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		pasteAllButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pasteAllButtonEvent(event); } });

		Label lFGF = new Label(settingsTabComp, SWT.NONE); lFGF.setText("");
		fitOnlyGoodFramesCheckbox = new Button(settingsTabComp, SWT.CHECK);
		fitOnlyGoodFramesCheckbox.setText("Fit only 'good' frames");
		fitOnlyGoodFramesCheckbox.setToolTipText("Fits only frames marked as 'good' e.g. from SeriesProcessor");
		fitOnlyGoodFramesCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		fitOnlyGoodFramesCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lFU = new Label(settingsTabComp, SWT.NONE); lFU.setText("Uncertainty:");
		calcFitUncertaintyCombo = new Combo(settingsTabComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		calcFitUncertaintyCombo.setItems(new String[] { "None", "Conditional", "Marginal" });
		calcFitUncertaintyCombo.setToolTipText("Calculate uncertainty on fit parameters from statistical error on data. Conditional (from fit Hessian) or marginal (inverted)");
		calcFitUncertaintyCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
		calcFitUncertaintyCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		
	}


	protected void settingsChangedEvent(Event e) {
		FastSpecConfig config = proc.getConfig();
		config.multiGauss = multiGaussCheckbox.getSelection();
		config.wavelenIsY = wavelengthIsYCheckbox.getSelection();
		config.coordType = coordTypeCombo.getSelectionIndex();
		config.forceBackupInitGuess = backupInitGuess.getSelection();
		config.initMode = InitMode.valueOf(initModeCombo.getText()); 
		config.amplitudeFit = AmpFitMode.valueOf(amplitudeModeCombo.getText());
		config.optimser = Optimiser.valueOf(Optimiser.class, optimiserCombo.getText());
		config.numIterations = numIterationsSpinner.getSelection();
		config.processOnlyGoodFrames = fitOnlyGoodFramesCheckbox.getSelection();
		config.calcFitUncertainty = calcFitUncertaintyCombo.getSelectionIndex() > 0;
		config.marginalUncertainties = calcFitUncertaintyCombo.getSelectionIndex() == 2;
		try {
			config.thresholdMaxImage = Double.parseDouble(thresholdMaxImageTextbox.getText());
		}catch(NumberFormatException err){ 
			config.thresholdMaxImage = Double.NaN;
		}
		proc.setNThreads(nThreadsSpinner.getSelection());
	}
	
	protected void doUpdate() {

		FastSpecConfig config = proc.getConfig();
		multiGaussCheckbox.setSelection(config.multiGauss);
		wavelengthIsYCheckbox.setSelection(config.wavelenIsY);
		coordTypeCombo.select(config.coordType);
		optimiserCombo.setText(config.optimser.toString());
		initModeCombo.setText(config.initMode.toString());
		amplitudeModeCombo.setText(config.amplitudeFit.toString());		
		if(!numIterationsSpinner.isFocusControl())
			numIterationsSpinner.setSelection(config.numIterations);
		numIterationsSpinner.setValues(config.numIterations, 0, 10000, 0, 1, 10);
		backupInitGuess.setSelection(config.forceBackupInitGuess); 
		fitOnlyGoodFramesCheckbox.setSelection(config.processOnlyGoodFrames);
		calcFitUncertaintyCombo.select(!config.calcFitUncertainty ? 0 : (config.marginalUncertainties ? 2 : 1));		
		nThreadsSpinner.setSelection(proc.getNThreads());
		thresholdMaxImageTextbox.setText(Double.toString(config.thresholdMaxImage));
	}


	private void copyAllButtonEvent(Event event){
		
		StringBuilder strB = new StringBuilder();
		
		FastSpecConfig config = proc.getConfig();
		
		//Set<String> keySet = config.specEntries.get(0).toMap().keySet();
		String[] keySet = new String[]{ "id",  "enable",
				"channelID", "atomicNumber", "ionisationState", "upperState", "lowerState", "atomicMass", "nominalWavelength",
				"x0", "y0", "x1", "y1", "ignoreX0Ranges",
				"x0Min","x0Max","x0Init","x0PriorMean","x0PriorSigma",
				"sigmaMin","sigmaMax","sigmaInit","sigmaPriorMean","sigmaPriorSigma", "sigmaMatch",
				"powerMin","powerMax","powerInit","powerPriorMean","powerPriorSigma",
				"amplitudeMin","amplitudeMax","amplitudeInit","amplitudePriorMean","amplitudePriorSigma", "amplitudeInitFraction", "amplitudeMatch",
				"y0Min","y0Max","y0Init","y0PriorMean","y0PriorSigma","clampY0ToBaselineInit",
				"slopeMin","slopeMax","slopeInit","slopePriorMean","slopePriorSigma",
			};
		
		for(String fieldName : keySet){
			strB.append("\"" + fieldName + "\"\t");
		}
		strB.append("\n");
		
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.create(); 
		
		for(FastSpecEntry specEntry : config.specEntries){
			
			JsonElement jsonElem = gson.toJsonTree(specEntry);
			JsonObject o = jsonElem.getAsJsonObject();
			
			for(String fieldName : keySet){
				Object o2 = o.get(fieldName);
				strB.append((o2 == null ? "null" : o2.toString()) + "\t");
			}
			strB.append("\n");
		}
		
		System.out.println(strB.toString());
		
		Clipboard cb = new Clipboard(settingsTabComp.getDisplay());
		TextTransfer tt = TextTransfer.getInstance();
		cb.setContents(new Object[]{ strB.toString() }, new Transfer[]{ tt });
		cb.dispose();
	}


	private void pasteAllButtonEvent(Event event){

		Clipboard cb = new Clipboard(settingsTabComp.getDisplay());

		try{
			String plainText = (String)cb.getContents(TextTransfer.getInstance());
			System.out.println("Pasted: " + plainText);
			String lines[] = plainText.split("\n");
			String fieldNames[] = lines[0].split("\t");
	
			Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.create(); 
		
			ArrayList<FastSpecEntry> specEntries = new ArrayList<FastSpecEntry>();
			for(int i=1; i < lines.length; i++){
				
				String jsonStr = "{";
				String fields[] = lines[i].split("\t");
				if(fields.length != fieldNames.length)
					throw new RuntimeException("Row " +i+" of pasted text had " + fields.length +" columns but row 0 (names) had " + fieldNames.length);
				
				for(int j=0; j < fields.length; j++){
					if(j > 0)
						jsonStr += ","; 
					jsonStr += "\"" + fieldNames[j] + "\":";
							
					if(fields[j].equals("null")){
						jsonStr += "null";
					}else if(fields[j].startsWith("[") && fields[j].endsWith("]")){ 
						//looks like a JSON array (e.g. ignoreX0Ranges
						jsonStr += fields[j];
					}else{
						jsonStr += "\"" + fields[j] + "\"";
					}
				}
				jsonStr += "}";
				System.out.println(jsonStr);
	
				FastSpecEntry thing = (FastSpecEntry)gson.fromJson(jsonStr, FastSpecEntry.class);
	
				specEntries.add(thing);
				
			}
			FastSpecConfig config = proc.getConfig();
			config.specEntries = specEntries;
			cb.dispose();
			
			proc.setStatus("Pasted "+specEntries.size()+" entries.");
			
		}catch(RuntimeException err){
			err.printStackTrace();
			proc.setStatus("Error parsing pasted text:" + err.getMessage());
		}
		
		
		ctrl.generalControllerUpdate();
	}
}
