package imageProc.proc.fastSpec.swt;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import otherSupport.RandomManager;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.gmds.GMDSSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.graph.JFreeChartGraph;
import imageProc.graph.JFreeChartGraphWithErrorBars;
import imageProc.graph.Series;
import imageProc.proc.fastSpec.FastSpecConfig;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessor;
import imageProc.proc.fastSpec.FastSpecConfig.Optimiser;
import imageProc.proc.fastSpec.FastSpecProcessor.FrameData;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class FastSpecSWTController implements SWTControllerInfoDraw, ImagePipeControllerROISettable {
	
	
	public static enum TableModes {
		Line, Window, X0, Amplitude, Sigma, Power, Y0, Slope, All;

		public static String[] toStrings() {
			TableModes m[] = TableModes.values();
			String s[] = new String[m.length];
			for(int i=0; i < m.length; i++)
				s[i] = m[i].toString();
			return s;
		}
	};
	
	public static String[] colNames0 = { "o","id","enable","channelID", "losName" };
	
	public static HashMap<TableModes,String[]> colNames = new HashMap<TableModes,String[]>();
	static{
		colNames.put(TableModes.Line, new String[]{ "nominalWavelength", "atomicNumber", "atomicMass", "ionisationState", "upperState", "lowerState" });
		colNames.put(TableModes.Window, new String[]{ "x0", "x1", "y0", "y1", "ignoreX0Ranges", "minViableDataRange" });		
		colNames.put(TableModes.X0, new String[]{ "x0Min", "x0Max", "x0Init", "x0PriorMean", "x0PriorSigma" });
		colNames.put(TableModes.Amplitude, new String[]{ "amplitudeMin", "amplitudeMax", "amplitudeInit", "amplitudePriorMean", "amplitudePriorSigma", "amplitudeInitFraction", "amplitudeMatch" });
		colNames.put(TableModes.Sigma, new String[]{ "sigmaMin", "sigmaMax", "sigmaInit", "sigmaPriorMean", "sigmaPriorSigma", "sigmaMatch" });
		colNames.put(TableModes.Power, new String[]{ "powerMin", "powerMax", "powerInit", "powerPriorMean", "powerPriorSigma" });
		colNames.put(TableModes.Y0, new String[]{ "y0Min", "y0Max", "y0Init", "y0PriorMean", "y0PriorSigma" });
		colNames.put(TableModes.Slope, new String[]{ "slopeMin", "slopeMax", "slopeInit", "slopePriorMean", "slopePriorSigma" });
	}
		
	public TableModes tableMode;
	
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.#####");

	protected FastSpecProcessor proc;

	protected Group swtGroup;
	
	protected CTabFolder tabFolder;
	private CTabItem tabItemSettings;
	private CTabItem tabItemSaveSettings;	
	private CTabItem tabItemSave;
	private CTabItem tabItemGraphs;
	protected CTabItem specificTabItems[];
	
	private SashForm swtSashForm;
	
	private Composite swtTopComp;
	private Composite swtBottomComp;

	private Label statusLabel;
	private Button autoUpdateCheckbox;
	private Button autoBoundButton;
	private Button forceUpdateButton;
	private Button forceUpdateEntryButton;
	private Button forceUpdateFrameButton;
	private Button singleFrameCheckbox;
	
	private Combo tableModeCombo;
	
	protected SWTSettingsControl settingsCtrl;

	private Table pointsTable;
	private TableEditor pointsTableEditor;
	private TableItem tableItemEditing = null;
	private Text tableEditBox = null;

	private FastSpecGraphSWTPanel graphPanel;
	private FastSpecSettingsSWTPanel settingsPanel;
	private FastSpecSaveSWTPanel savePanel;
	protected FastSpecSpecificPanel specifcPanels[];

	private int sortByColumn = 1;

	
	public FastSpecSWTController(Composite parent, int style, FastSpecProcessor proc) {
		this.proc = proc;

		swtGroup = new Group(parent, style);
		swtGroup.setText("FastSpec Control (" + proc.toShortString() + ")");
		swtGroup.setLayout(new FillLayout());
		swtGroup.addListener(SWT.Dispose, new Listener() {
			@Override
			public void handleEvent(Event event) {
				destroy();
			}
		});

		swtSashForm = new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		swtSashForm.setLayout(new FillLayout());

		swtTopComp = new Composite(swtSashForm, SWT.NONE);
		swtTopComp.setLayout(new GridLayout(6, false));

		Label lStat = new Label(swtTopComp, SWT.NONE);
		lStat.setText("Status:");
		statusLabel = new Label(swtTopComp, SWT.NONE);
		statusLabel.setText("init");
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		
		Label lTM = new Label(swtTopComp, SWT.NONE); lTM.setText("Table:");
		tableModeCombo = new Combo(swtTopComp, SWT.DROP_DOWN);
		tableModeCombo.setItems(TableModes.toStrings());
		tableModeCombo.setToolTipText("Column set to show in table");
		tableModeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		tableModeCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setTableModeEvent(event); }	});
		
		pointsTable = new Table(swtTopComp, SWT.BORDER | SWT.MULTI);
		pointsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		pointsTable.setHeaderVisible(true);
		pointsTable.setLinesVisible(true);


		
		for (int i = 0; i < colNames0.length; i++) {
			TableColumn col = new TableColumn(pointsTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			
			col.setText(colNames0[i]);			
			final int j= i;
			col.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sortTableEvent(event, j); } });
			
		}
		
		setTableMode(TableModes.Line);
		
		pointsToGUI();

		for (TableColumn col : pointsTable.getColumns())
			col.pack();
		

		pointsTable.addListener(SWT.MouseDoubleClick, new Listener() {
			@Override
			public void handleEvent(Event event) {
				for (TableColumn col : pointsTable.getColumns())
					col.pack();
			}
		});

		pointsTableEditor = new TableEditor(pointsTable);
		pointsTableEditor.horizontalAlignment = SWT.LEFT;
		pointsTableEditor.grabHorizontal = true;
		pointsTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		pointsTable.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceImageRedraw(); generalControllerUpdate(); } });
		
		swtBottomComp = new Composite(swtSashForm, SWT.NONE);
		swtBottomComp.setLayout(new GridLayout(8, false));
		
		singleFrameCheckbox = new Button(swtBottomComp, SWT.CHECK);
		singleFrameCheckbox.setText("Single Frame");
		singleFrameCheckbox.setToolTipText("Only fit the current frame (faster)");
		singleFrameCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		singleFrameCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }	});
		
		autoUpdateCheckbox = new Button(swtBottomComp, SWT.CHECK);		
		autoUpdateCheckbox.setText("Auto");
		autoUpdateCheckbox.setToolTipText("Trigger refit on all settings and image source changes");
		autoUpdateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { autoUpdateEvent(event); }});
		autoUpdateCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		autoBoundButton = new Button(swtBottomComp, SWT.PUSH);
		autoBoundButton.setText("Auto-bound");
		autoBoundButton.setToolTipText("Set min/max of all properties to +/-5 sigma of the time series fits results for the selected entry.");
		autoBoundButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { autoBoundEvent(event, false, false); }});
		autoBoundButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		Label lU = new Label(swtBottomComp, SWT.NONE); lU.setText("Update:");		
		forceUpdateButton = new Button(swtBottomComp, SWT.PUSH);
		forceUpdateButton.setText("All");
		forceUpdateButton.setToolTipText("Update and perform the fit of all entries to all frames now.");
		forceUpdateButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceUpdateEvent(event, false, false); }});
		forceUpdateButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		forceUpdateEntryButton = new Button(swtBottomComp, SWT.PUSH);
		forceUpdateEntryButton.setText("Entry");
		forceUpdateEntryButton.setToolTipText("Perform fit on selected entry for all frames now.");
		forceUpdateEntryButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceUpdateEvent(event, true, false); }});
		forceUpdateEntryButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		forceUpdateFrameButton = new Button(swtBottomComp, SWT.PUSH);
		forceUpdateFrameButton.setText("Frame");
		forceUpdateFrameButton.setToolTipText("Perform fit on all entries of selected frame.");
		forceUpdateFrameButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceUpdateEvent(event, false, true); }});
		forceUpdateFrameButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		Button forceUpdateSingleButton;		
		forceUpdateSingleButton = new Button(swtBottomComp, SWT.PUSH);
		forceUpdateSingleButton.setText("Single");
		forceUpdateSingleButton.setToolTipText("Perform fit on selected frame of selected entry now.");
		forceUpdateSingleButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceUpdateEvent(event, true, true); }});
		forceUpdateSingleButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		tabFolder = new CTabFolder(swtBottomComp, SWT.BORDER | SWT.TOP);		
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 8, 1)); //this doesn't work
			
		Composite settingsTabComp = new Composite(tabFolder, SWT.NONE);
		settingsTabComp.setLayout(new GridLayout(6, false));
		tabItemSettings = new CTabItem(tabFolder, SWT.NONE);
		tabItemSettings.setControl(settingsTabComp);
		tabItemSettings.setText("Settings");  
		tabItemSettings.setToolTipText("Coordinate set up and fitting settings");
			
		Composite saveSettingsTabComp = new Composite(tabFolder, SWT.NONE);
		saveSettingsTabComp.setLayout(new GridLayout(6, false));
		tabItemSaveSettings = new CTabItem(tabFolder, SWT.NONE);
		tabItemSaveSettings.setControl(saveSettingsTabComp);
		tabItemSaveSettings.setText("Save/load Settings");  
		tabItemSaveSettings.setToolTipText("Save/load settings");
			
		addSettingsWidget(saveSettingsTabComp);
				
		Composite saveTabComp = new Composite(tabFolder, SWT.NONE);
		saveTabComp.setLayout(new GridLayout(6, false));
		tabItemSave = new CTabItem(tabFolder, SWT.NONE);
		tabItemSave.setControl(saveTabComp);
		tabItemSave.setText("Write results");  
		tabItemSave.setToolTipText("Settings concerning saving of fit results");

		Composite graphTabComp = new Composite(tabFolder, SWT.NONE);
		graphTabComp.setLayout(new FillLayout());
		tabItemGraphs = new CTabItem(tabFolder, SWT.NONE);
		tabItemGraphs.setControl(graphTabComp);
		tabItemGraphs.setText("Graph");  
		tabItemGraphs.setToolTipText("Graphs of output and fits");
		
		graphPanel = new FastSpecGraphSWTPanel(proc, this, graphTabComp);		
		settingsPanel = new FastSpecSettingsSWTPanel(proc, this, settingsTabComp);
		savePanel = new FastSpecSaveSWTPanel(proc, this, saveTabComp);
		addSpecificPanels();
		
		tabFolder.setSelection(tabItemGraphs);
		
		swtGroup.pack();
		doUpdate();
	}
	
	protected void autoBoundEvent(Event event, boolean b, boolean c) {
		FastSpecConfig config = proc.getConfig();
		TableItem[] selected = pointsTable.getSelection();
		
		proc.autoBound(selected[0].getText(1));
		
	}

	protected void setTableModeEvent(Event event) {
		setTableMode(TableModes.valueOf(tableModeCombo.getText()));
	}

	private void setTableMode(TableModes mode){
		this.tableMode = mode;
		
		
		TableColumn[] cols = pointsTable.getColumns();
		cols[4].setText("losName");
		colNames0 = new String[]{ "o","id","enable","channelID", "losName" };		
		for(int i=colNames0.length; i < cols.length; i++){
			cols[i].dispose();
		}
		
		String colNames1[] = colNames.get(mode);
		for (int i = 0; i < colNames1.length; i++) {
			TableColumn col = new TableColumn(pointsTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			
			col.setText(colNames1[i]);
			final int j= i;
			col.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sortTableEvent(event, j); } });
		}

		pointsToGUI();
		
		for (TableColumn col : pointsTable.getColumns()){
			col.pack();
		}

		
	}
	
	protected void sortTableEvent(Event event, int column) {
		//column doesn't work because the 'final' int is overwritten around the loop or something		
		column=-1;
		TableColumn[] cols = pointsTable.getColumns();
		for(int i=0; i < cols.length; i++){
			if(cols[i] == event.widget){
				column = i;
				break;
			}
				
		}
		
		if(column <= 0){
			column = 1;
		}		
		
		sortByColumn  = column;
		generalControllerUpdate();
	}
	
	protected void addSpecificPanels(){
		specificTabItems = new CTabItem[0];
		specifcPanels = new FastSpecSpecificPanel[0];
	}
	

	protected void addSettingsWidget(Composite parent){
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(parent, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 6, 1));
		
	}	
	
	protected void addSaveControls(Composite parent) {
		Label l = new Label(parent, SWT.NONE);
		l.setText("(No save controls)");
		l.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
	}

	private void autoUpdateEvent(Event e) {
		proc.setAutoUpdate(autoUpdateCheckbox.getSelection());
	}

	private void forceUpdateEvent(Event e, boolean selectedOnly, boolean forceFrameOnly) {		
		FastSpecConfig config = proc.getConfig();
		TableItem[] selected = pointsTable.getSelection();
		if(selectedOnly && selected.length == 1){
			proc.calc(proc.getConfig().getPointByName(selected[0].getText(1)), 
					forceFrameOnly | singleFrameCheckbox.getSelection());
		}else{
			proc.calc(null, 
					forceFrameOnly | singleFrameCheckbox.getSelection());
		}
			
	}
		
	protected void settingsChangedEvent(Event e) {
		FastSpecConfig config = proc.getConfig();
			
	}

	@Override
	public final void generalControllerUpdate() {
		if (swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() {
				doUpdate();
			}
		});
	}

	public final String slowUpdateSyncObject = new String("SlowUpdateSyncObject");
	protected void doUpdate() {

		settingsCtrl.doUpdate();
		
		autoUpdateCheckbox.setSelection(proc.getAutoUpdate());
		statusLabel.setText(proc.getStatus());

		settingsPanel.doUpdate();
		savePanel.doUpdate();
		graphPanel.doUpdate();
		for(FastSpecSpecificPanel panel : specifcPanels)
			panel.doUpdate();
		
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), slowUpdateSyncObject, new Runnable() {
			@Override
			public void run() {
				slowUpdate();
			}
		});		
	}

	// slower and needs to be done less often
	private void slowUpdate() {
		if (tableItemEditing == null)
			pointsToGUI();
		
		try{
			graphPanel.updateFitGraph();
			graphPanel.updateFitResults();
			
		}catch(RuntimeException err){
			err.printStackTrace();
		}
	}

	

	@Override
	public void movingPos(int x, int y) {
	}

	@Override
	public void fixedPos(int x, int y) {

	}

	@Override
	public void setRect(int x0, int y0, int width, int height) {
		if (swtGroup.isDisposed() || !settingsPanel.setBoxesCheckbox.getSelection())
			return;

		Img img = proc.getSelectedImageFromConnectedSource();
		if (img == null)
			return;
		int imageWidth = img.getWidth();
		int imageHeight = img.getHeight();

		TableItem[] selected = pointsTable.getSelection();
		if (selected.length == 1) {
			String pointName = selected[0].getText(1);

			FastSpecEntry p = proc.getConfig().getPointByName(pointName);
			proc.setBoxInPixels(p, x0, y0, x0 + width, y0 + height);

			// we have to edit the item because the update won't actually take
			// place immediately
			/*
			 * selected[0].setText(2, tableValueFormat.format(x));
			 * selected[0].setText(3, tableValueFormat.format(y));
			 */

			proc.mapModified();
		}
	}

	private final String formatTableValue(double val) {
		return Double.isNaN(val) ? "NaN" : tableValueFormat.format(val);
	}

	private void pointsToGUI() {
		if(tableItemEditing != null) //don't mess with the table while the user is editing
			return;
		
		FastSpecConfig config = proc.getConfig();
		ArrayList<FastSpecEntry> points;
		synchronized (config.specEntries) {
			// clone to release lock but avoid concurrent modification
			points = new ArrayList<FastSpecEntry>(config.specEntries); 
		}
		
		try{
			String colName = pointsTable.getColumn(sortByColumn).getText();
			 
			// convert map to list and sort
			Collections.sort(points, new Comparator<FastSpecEntry>() {
				
				@Override
				public int compare(FastSpecEntry p1, FastSpecEntry p2) {
					if(p1 == null)
						return (p2 == null) ? 0 : 1;
					if(p2 == null)
						return -1;
					
					Object o1, o2;
					
					Field f;
					try {
						f = FastSpecEntry.class.getDeclaredField(colName);
						o1 = f.get(p1);
						o2 = f.get(p2);
					} catch (NoSuchFieldException | SecurityException 
								| IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
						return 0;
					}
					if(o1 == null)
						return (o2 == null) ? 0 : 1;
					if(o2 == null)
						return -1;

					if(!(o1 instanceof Comparable))
						throw new RuntimeException("Sorting by non-comparable object:" + f + ", " + p1 + ", " + o1);
					if(!(o2 instanceof Comparable))
						throw new RuntimeException("Sorting by non-comparable object:" + f + ", " + p2 + ", " + o2);
					
					//return p1.id.compareTo(p2.id);
					return ((Comparable)o1).compareTo((Comparable)o2);
				}
			});
		}catch(RuntimeException err){
			err.printStackTrace();
		}

		// clear and repopulate table
		int selected[] = pointsTable.getSelectionIndices();

		// use existing table entries, so it doesnt scroll around
		for (int i = 0; i < points.size(); i++) {
			FastSpecEntry p = points.get(i);

			if (p.id.length() <= 0)
				continue;

			TableItem item;
			if (i < pointsTable.getItemCount())
				item = pointsTable.getItem(i);
			else
				item = new TableItem(pointsTable, SWT.NONE);

			item.setText(0, "o");
			
			for(int j=1; j < pointsTable.getColumnCount(); j++){
				String colName = pointsTable.getColumn(j).getText();
				if(colName.equals("enable")){
					item.setText(j, p.enable ? "Y" : "N");
					continue;
				}
				
				Field f;
				Object o;
				try {
					f = FastSpecEntry.class.getDeclaredField(colName);
					o = f.get(p);
				} catch (NoSuchFieldException | SecurityException 
							| IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
					continue;
				}
				
				if(o == null){
					item.setText(j, "(null)");
				}else if(o instanceof Integer){
					item.setText(j, ((Integer)o).toString());
				}else if(o instanceof Number){
					item.setText(j, formatTableValue(((Number)o).doubleValue()));
				}else if(o.getClass().isArray()){
					Gson gson = new Gson();
					item.setText(j, gson.toJson(o));
				}else{
					item.setText(j, o.toString());
				}
			}
		}

		while (pointsTable.getItemCount() > points.size()) {
			pointsTable.remove(points.size());
		}

		// and finally a blank item for creating new point
		TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
		blankItem.setText(0, "o");
		blankItem.setText(1, "<new>");

		// pointsTable.select(selected);
		/*
		 * for(int i=0; i < ) if(selectedPoint != null){ for(int i=0; i <
		 * pointsTable.getItemCount(); i++){
		 * if(selectedPoint.equals(pointsTable.getItem(i).getText(1))){
		 * pointsTable.select(i); break; } } }
		 */
		// pointsTable.setTopIndex(topIndex);

	}

	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth, int imageHeight, boolean asSource) {
		if (swtGroup.isDisposed() || !settingsPanel.drawBoxesCheckbox.getSelection())
			return;

		List<FastSpecEntry> points = proc.getConfig().specEntries;

		synchronized (points) {
			points = new ArrayList<FastSpecEntry>(points); // clone to release
															// lock but avoid
															// concurrent
															// modification
		}

		int i = 0;
		for (FastSpecEntry p : points) {

			TableItem[] selectedItems = pointsTable.getSelection();
			boolean isSelected = false;
			for (TableItem item : selectedItems) {
				if (item.getText(1).equals(p.id))
					isSelected = true;
			}
			if (isSelected) {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_MAGENTA));
				gc.setLineWidth(3);
			} else {
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
				gc.setLineWidth(1);
			}

			int boxCoords[] = proc.getBoxInPixels(p);

			if (boxCoords[0] >= 0 && boxCoords[1] >= 0 && boxCoords[2] <= imageWidth && boxCoords[3] <= imageHeight
					&& boxCoords[2] > boxCoords[0] && boxCoords[3] > boxCoords[1]) {

				gc.drawRectangle((int) (boxCoords[0] * scale[0]), (int) (boxCoords[1] * scale[1]),
						(int) ((boxCoords[2] - boxCoords[0]) * scale[0]),
						(int) ((boxCoords[3] - boxCoords[1]) * scale[1]));
			}

			i++;
		}

	}

	private void forceImageRedraw() {
		ImgSource src = proc.getConnectedSource();
		if (src == null)
			return;
		// force update of GUI windows connected to the image source
		for (ImgSink sink : src.getConnectedSinks()) {
			if (sink instanceof ImagePanel) {
				((ImagePanel) sink).redraw();
			}
		}
	}

	/**
	 * Copied from 'Snippet123' Copyright (c) 2000, 2004 IBM Corporation and
	 * others. [ org/eclipse/swt/snippets/Snippet124.java ]
	 */
	private void tableMouseDownEvent(Event event) {
		Rectangle clientArea = pointsTable.getClientArea();
		Point pt = new Point(clientArea.x + event.x, event.y);
		int index = pointsTable.getTopIndex();
		while (index < pointsTable.getItemCount()) {
			boolean visible = false;
			final TableItem item = pointsTable.getItem(index);
			for (int i = 0; i < pointsTable.getColumnCount(); i++) {
				Rectangle rect = item.getBounds(i);
				if (rect.contains(pt)) {
					final int column = i;
					String colName = pointsTable.getColumn(column).getText();
					if (column == 0) {
						// pointsTable.setSelection(index); //make sure its
						// selected
						generalControllerUpdate();
						forceImageRedraw();
						return;
					}else if(colName.equals("enable")){
						boolean en = item.getText(column).startsWith("Y");
						en = !en; //flip
						tableColumnModified(item, column, en ? "Y" : " ");
						return;
					}
					
					if (tableEditBox != null) {
						tableEditBox.dispose(); // oops, one left over
					}
					tableEditBox = new Text(pointsTable.getParent(), SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener() {
						public void handleEvent(final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, tableEditBox.getText());
								tableEditBox.dispose();
								tableEditBox = null;
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, tableEditBox.getText());
									// FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									tableEditBox.dispose();
									tableEditBox = null;
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					tableEditBox.addListener(SWT.FocusOut, textListener);
					tableEditBox.addListener(SWT.Traverse, textListener);
					pointsTableEditor.setEditor(tableEditBox, item, i);
					tableEditBox.setText(item.getText(i));
					tableEditBox.selectAll();
					tableEditBox.setFocus();
					// hacks for SWT4.4 (under linux GTK at least)
					// Textbox won't display if it's a child of the table
					// so we make it a child of the table's parent, but now need
					// to adjust the location
					{
						tableEditBox.moveAbove(pointsTable);
						final Point p0 = tableEditBox.getLocation();
						Point p1 = pointsTable.getLocation();
						p0.x += p1.x - clientArea.x;
						p0.y += p1.y;// + editBox.getSize().y;
						// p0.x = pt.x + p1.x;
						// p0.y = pt.y + p1.y;
						tableEditBox.setLocation(p0);
						tableEditBox.addListener(SWT.Move, new Listener() {
							@Override
							public void handleEvent(Event event) {
								tableEditBox.setLocation(p0); // TableEditor keeps
															// moving it to
															// relative to the
															// table, so move it
															// back
							}
						});
					}
					return;
				}
				if (!visible && rect.intersects(clientArea)) {
					visible = true;
				}
			}
			if (!visible)
				return;
			index++;
		}
	}

	private void tableColumnModified(TableItem item, int column, String text) {
		//command to set all columns
		if(text.startsWith("^^")){
			for(TableItem i : pointsTable.getItems()){
				if(i.getText(1).equals("<new>"))
					continue;
				tableColumnModified(i, column, text.substring(3));
			}
			return;
		}

		FastSpecConfig config = proc.getConfig();
		synchronized (config.specEntries) {
			if (item.isDisposed()){
				System.err.println("Item disposed!");
				return;
			}

			String pointName = item.getText(1);

			FastSpecEntry point = (pointName.length() > 0) ? config.getPointByName(pointName) : null;

			String colName = pointsTable.getColumn(column).getText();
			if(colName.equals("id")){
				if(text.equals("<new>"))
					return; //don't allow points called <new>
				
				if (point == null && (pointName.length() <= 0 || pointName.equals("<new>"))) { // creating
																								// new
																								// point
					point = new FastSpecEntry(text);
					config.specEntries.add(point);

				} else if (text.length() <= 0) { // deleting point
					config.specEntries.remove(point);

				} else { // just changing name
					point.id = text;
				}

				item.setText(column, text);

			}else if(colName.equals("enable")){
				point.enable = text.startsWith("Y");
				item.setText(column, point.enable ? "Y" : " ");
				
			}else{
				Field f;
				try {
					f = FastSpecEntry.class.getDeclaredField(colName);
									
					if(f.getType() == int.class){
						f.set(point, Algorithms.mustParseInt(text));
					}else if(f.getType() == double.class){
						f.set(point, Algorithms.mustParseDouble(text));
					}else if(f.getType() == String.class){
						if(text != null && (text.length() == 0 || text.equals("null") || text.equals("(null)")))
							text = null;
						f.set(point, text);
					}else if(f.getType().isArray()){
						Gson gson = new Gson();
						Object o = gson.fromJson(text, f.getType());
						f.set(point, o);
					}else{
						System.err.println("Unknown type for field " + f.getName());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
				

			TableItem lastItem = pointsTable.getItem(pointsTable.getItemCount() - 1);
			if (!lastItem.getText(1).equals("<new>")) {
				TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
				blankItem.setText(0, "o");
				blankItem.setText(1, "<new>");
			}

			// proc.saveMapPoints();
			proc.mapModified();
		}
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
		// proc.destroy(); //and actively kill it, for good measure
	}

	@Override
	public Object getInterfacingObject() {
		return swtGroup;
	}

	@Override
	public FastSpecProcessor getPipe() {
		return proc;
	}

	public TableItem[] getSelectedTableEntries() {
		return pointsTable.getSelection();
	}

	
}
