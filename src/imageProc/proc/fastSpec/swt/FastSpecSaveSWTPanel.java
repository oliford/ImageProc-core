package imageProc.proc.fastSpec.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import imageProc.proc.fastSpec.FastSpecConfig;
import imageProc.proc.fastSpec.FastSpecProcessor;

public class FastSpecSaveSWTPanel {

	private FastSpecProcessor proc;
	private FastSpecSWTController ctrl;
	private Composite saveTabComp;

	private Button resultsInMetadataCheckbox;
	private Button saveResultsCheckbox;
	
	private Button saveNowNoFit;
	
	public FastSpecSaveSWTPanel(FastSpecProcessor proc, FastSpecSWTController ctrl, Composite saveTabComp) {
		this.proc = proc;
		this.ctrl = ctrl;
		this.saveTabComp = saveTabComp;
		

		resultsInMetadataCheckbox = new Button(saveTabComp, SWT.CHECK);
		resultsInMetadataCheckbox.setText("Save in metadata");
		resultsInMetadataCheckbox.setToolTipText("Store the best fit parameters in the image metadata.");
		resultsInMetadataCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		resultsInMetadataCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		saveResultsCheckbox = new Button(saveTabComp, SWT.CHECK);
		saveResultsCheckbox.setText("Save on fit");
		saveResultsCheckbox.setToolTipText("Store the best fit parameters in the database after fitting.");
		saveResultsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		saveResultsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		saveNowNoFit = new Button(saveTabComp, SWT.PUSH);
		saveNowNoFit.setText("Save now");
		saveNowNoFit.setToolTipText("Store the current best fit parameters in the database now.");
		saveNowNoFit.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		saveNowNoFit.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveNowButtonEvent(event); } });
				
		ctrl.addSaveControls(saveTabComp);

			
	}
	
	private void saveNowButtonEvent(Event e){
		proc.saveCurrent();
	}
	
	public void doUpdate(){
		FastSpecConfig config = proc.getConfig();
		
		saveResultsCheckbox.setSelection(config.saveResults);
		
		resultsInMetadataCheckbox.setSelection(config.saveMetadata);
		
	}

	protected void settingsChangedEvent(Event e) {
		FastSpecConfig config = proc.getConfig();
		config.saveMetadata = resultsInMetadataCheckbox.getSelection();
		config.saveResults = saveResultsCheckbox.getSelection();			
	}

}
