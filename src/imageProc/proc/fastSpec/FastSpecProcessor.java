package imageProc.proc.fastSpec;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.Interpolation1D;
import binaryMatrixFile.DataConvertPureJava;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.EventReciever.Event;
import imageProc.proc.fastSpec.FastSpecConfig.AmpFitMode;
import imageProc.proc.fastSpec.FastSpecConfig.InitMode;
import imageProc.proc.fastSpec.FastSpecEntry.FitResults;
import imageProc.proc.fastSpec.swt.FastSpecSWTController;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import otherSupport.SettingsManager;
import seed.digeom.FunctionND;
import seed.digeom.IDomain;
import seed.digeom.RectangularDomain;
import seed.matrix.DenseMatrix;
import seed.matrix.Matrix;
import seed.optimization.BracketingByParameterSpace;
import seed.optimization.CoordinateDescentDirection;
import seed.optimization.GoldenSection;
import seed.optimization.GradientDescentDirection;
import seed.optimization.HookeAndJeeves;
import seed.optimization.IBracketingMethod;
import seed.optimization.IStoppingCondition;
import seed.optimization.LineOptimizer;
import seed.optimization.LineSearchOptimizer;
import seed.optimization.MaxIterCondition;
import seed.optimization.NewtonsMethod1D;
import seed.optimization.Optimizer;
import seed.optimization.SearchDirectionMethod;

public class FastSpecProcessor extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, EventReciever {

	protected FastSpecConfig config;

	// fields in data
	public class FrameData {
		public double[] x;
		public double[] data;
		public double[] error;
		public double[] init;
		public double[] base;
		public double[] fit;
		public double[] residual;
		public double[][] componentsFit;
		public String[] componentIDs;
		
		public double minData, maxData;
				
		public FrameData(int nPoints, int nComponents) {
			x = new double[nPoints];
			data = new double[nPoints];
			error = new double[nPoints];
			init = new double[nPoints];
			base = new double[nPoints];
			fit = new double[nPoints];
			residual = new double[nPoints];
			componentsFit = new double[nComponents][nPoints];
			componentIDs = new String[nComponents];
		}

		public double[][] all(){
			double all[][] = new double[componentsFit.length + 7][];
			all[0] = x;
			all[1] = data;
			all[2] = error;
			all[3] = init;
			all[4] = base;
			all[5] = fit;
			all[6] = residual;
			for(int i=0; i < componentsFit.length; i++)
				all[7+i] = componentsFit[i];
			return all;
		}
	}

	/**
	 * Holds the transform between physical CCD coordinates and the image (via
	 * ROI settings and CCD properties etc)
	 */
	private PhysicalROIGeometry ccdGeom;

	protected double wavelengthMetadata[][];
	protected Interpolation1D[] wavelengthToPixel;

	protected boolean autoCalc = false;

	protected boolean abortCalc = false;

	private String status;

	private boolean lastCalcComplete = false;

	private boolean forceSaveOnly = false;

	private SettingsManager settingsMgr;
	
	private FastSpecEntry fitSingleEntry = null;
	
	private int nThreads;
	
	public FastSpecProcessor(SettingsManager settingsMgr) {
		this.settingsMgr = (settingsMgr != null) ? settingsMgr : SettingsManager.defaultGlobal();
		
		try{
			String threadsSetting = SettingsManager.defaultGlobal().getProperty("imageProc.FastSpecProcessor.nThreads");		
			nThreads = Integer.parseInt(threadsSetting);
			
		}catch(RuntimeException err){
			ThreadPoolExecutor exec = (ThreadPoolExecutor)ImageProcUtil.getThreadPool();
			nThreads = exec.getCorePoolSize() - exec.getActiveCount(); //leave at least 1 free for GUI stuff
			nThreads = Math.min(nThreads, 10); //10 as upper limit
		}

		config = new FastSpecConfig();
		status = "init";
	}

	public FastSpecProcessor() {
		this(null);
	}

	public FastSpecProcessor(ImgSource source, int selectedIndex) {
		this();
		setSource(source);
		setSelectedSourceIndex(selectedIndex);
	}

	public boolean getAutoUpdate() {
		return this.autoCalc;
	}

	@Override
	public void imageChangedRangeComplete(int idx) {
		if (autoCalc) {
			calc();
		}
	}

	@Override
	protected void selectedImageChanged() {
		super.selectedImageChanged();
		
		// maybe GUI needs to know anyway
		updateAllControllers();
	}

	@Override
	public void setAutoUpdate(boolean autoCalc) {
		if (!this.autoCalc && autoCalc) {
			this.autoCalc = true;
			updateAllControllers();
			calc();
		} else {
			this.autoCalc = autoCalc;
			updateAllControllers();
		}
	}

	@Override
	public void calc(){ calc(null, false); }
		
	public void calc(FastSpecEntry entry, boolean singleFrame) {
		this.fitSingleEntry = entry;
		abortCalc = false;
		
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() {
			@Override
			public void run() {
				doCalc(singleFrame);
			}
		});
	}

	/** Guaranteed to be called only once at a time */
	protected void doCalc(boolean singleFrame) {
		try {
			lastCalcComplete = false;
			
			if (!forceSaveOnly) {

				setupCoords();
				
				int nF = connectedSource.getNumImages();

				if(fitSingleEntry == null && !singleFrame){
					for (FastSpecEntry specEntry : config.specEntries) {
						if(specEntry.fit == null)
							specEntry.fit = new FitResults();
						
						specEntry.fit.Amplitude = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.UncertaintyAmpltiude = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.X0 = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.UncertaintyX0 = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.Slope = Mat.fillArray(Double.NaN, nF);
						//specEntry.fit.UncertaintySlope = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.Y0 = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.UncertaintyY0 = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.Sigma = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.UncertaintySigma = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.Power = Mat.fillArray(Double.NaN, nF);					
						specEntry.fit.UncertaintyPower = Mat.fillArray(Double.NaN, nF);
						specEntry.fit.RMSError = Mat.fillArray(Double.NaN, nF);
	
						specEntry.fit.inferredQuantities = new HashMap<String, double[]>();
					}
					config.inferredQuantities = null;
				}
				
				if(config.inferredQuantities == null){
					config.inferredQuantities = new ArrayList<String>();
					inferredQuantities(CalcStage.Prepare, null, -1); // prepare inferredQuantities
				}
				

				boolean[] goodFrames = (boolean[])connectedSource.getSeriesMetaData("isGoodData");
				if(config.processOnlyGoodFrames || config.saveOnlyGoodFrames){					
					if(goodFrames == null)
						throw new RuntimeException("Config set to fit/save only good frame but no isGoodData metadata found.");
				}
				
				ConcurrentLinkedQueue<Integer> framesTodo = null;
				if(nThreads > 1 && !singleFrame)
					framesTodo = new ConcurrentLinkedQueue<Integer>();
				
				for (int iF = 0; iF < nF; iF++) {
					if (singleFrame && iF != getSelectedSourceIndex())
						continue;
					
					//skip  frame if its range isn;t valid (don't know why this changed down in ImgprocPipe or whereever)
					// should fix it there
					Img img = connectedSource.getImage(iF);
					if(img == null || !img.isRangeValid())
						continue;
					
					//skip frame is it's not good (and config says not to)
					if (config.processOnlyGoodFrames && goodFrames != null && !goodFrames[iF])
						continue;
					
					if (!Double.isNaN(config.thresholdMaxImage)) {
						//If under threshold or NaN (don't change the logic here!)
						if(!(connectedSource.getImage(iF).getMax() > config.thresholdMaxImage))
							continue;
					}
					
					if(framesTodo == null){
						setStatus("Fitting frame " + iF + " / " + nF);
						doFitFrame(iF);						
					}else{
						framesTodo.add(iF);
					}
					
					if (abortCalc){ //drop through
						setStatus("Aborted at frame " + iF + " / " + nF + " done");
						return; // Status should be set already	
					} 
					
				}
				
				 
				if(framesTodo != null){
					ThreadPoolExecutor exec = (ThreadPoolExecutor)ImageProcUtil.getThreadPool();
					if(nThreads >= (exec.getMaximumPoolSize() - 2)){
						//always need at least one thread left for updates
						//-2 because you have to count this one
						nThreads = exec.getMaximumPoolSize() - 2;
					}
					if(exec.getCorePoolSize() < (nThreads + 2)){
						exec.setCorePoolSize(nThreads + 2);
					}
					ArrayList<FrameFitTask> runners = new ArrayList<FrameFitTask>();
					
					for(int i=0; i < nThreads; i++){
						FrameFitTask runner = new FrameFitTask(framesTodo);
												
						runners.add(runner);
						exec.execute(runner);
					}
					
					boolean allDone = false;
					do{
						Integer iF = framesTodo.peek();
						
						if(abortCalc){
							setStatus("Aborted at frame " + ((iF==null) ? nF : iF) + " / " + nF + " done");
							return;
						}
						
						if(iF != null){
							setStatus("Next frame to fit: " + iF + " / " + nF + " done");
						}
						
						try {								
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
							
						
						allDone = true;
						for(FrameFitTask runner : runners){
							allDone &= runner.done;
						}

						updateAllControllers();
						
					}while(!framesTodo.isEmpty() || !allDone);
				}
			}

			inferredQuantities(CalcStage.PostProcess, null, -1); //post process inferred quantities
			
			//add ImageProc-core git commit ID and date to metadata			
			String gitCommit[] = ImageProcUtil.getGitCommitInfo("ImageProc-core");
			connectedSource.setSeriesMetaData("git/ImageProc-core/commit", gitCommit[0], false);
			connectedSource.setSeriesMetaData("git/ImageProc-core/date", gitCommit[1], false);
			
			if ((forceSaveOnly || config.saveResults) && fitSingleEntry == null && !singleFrame) {
				doSaveResults();
				//doSaveResults() will set the final status
			}else{				
				setStatus("Done");
			}
			lastCalcComplete = true;

		} catch (RuntimeException err) {
			err.printStackTrace();
			setStatus("ERROR: " + err);

		} finally {
			forceSaveOnly = false;
		}
	}
	
	public class FrameFitTask implements Runnable {
		private ConcurrentLinkedQueue<Integer> framesTodo;
		private ThreadPoolExecutor exec;
		private boolean done;
		
		public FrameFitTask(ConcurrentLinkedQueue<Integer> framesTodo) {
			this.framesTodo = framesTodo;
			this.done = false;
			exec = (ThreadPoolExecutor)ImageProcUtil.getThreadPool();			
		}
		
		@Override
		public void run() {
			
			//Logger.getLogger(this.getClass().getName()).finest("+ " + this.hashCode() + ".run()");
			if(abortCalc || framesTodo.isEmpty()){
				//Logger.getLogger(this.getClass().getName()).finest("A1 " + this.hashCode() + ".run()");
				done = true;
				return;
			}
		
			Integer iF = framesTodo.poll();
			if(iF != null){
				try{
					Logger.getLogger(this.getClass().getName()).fine("Start doFitFrame " + iF);
					doFitFrame(iF);
					Logger.getLogger(this.getClass().getName()).fine("Done doFitFrame " + iF);
				}catch(Exception err){
					Logger.getLogger(this.getClass().getName()).fine("Failed doFitFrame "+ iF + ":");
					err.printStackTrace();
				}
			}

			if(abortCalc || framesTodo.isEmpty()){
				//System.out.println("A2 " + this.hashCode() + ".run()");
				done = true;
				return;
			}
			
			//requeue another worker to maintain the number of workers, but return
			// so the working runs concurrently with other work in the pool task queue
			exec.execute(this);
			
			//System.out.println("- " + this.hashCode() + ".run()");
			
		}
	}
	
		
	private void doFitFrame(int iF) {
		
		ArrayList<FastSpecEntry> remainingEntires = new ArrayList<FastSpecEntry>();
		
		if(fitSingleEntry != null){
			remainingEntires.add(fitSingleEntry);						
		}else{
			for(FastSpecEntry entry : config.specEntries)
				if(entry.enable && (entry.fit.frameEnable == null || entry.fit.frameEnable[iF]))
					remainingEntires.add(entry);						
		}
		
		FastSpecEntry primaryEntry = null;
		int nF = connectedSource.getNumImages();
		while(remainingEntires.size() > 0){
			//try {
				if (abortCalc){
					return;
				}
				
				primaryEntry = remainingEntires.get(0);
				ArrayList<FastSpecEntry> entriesToFit;
				
				if(config.multiGauss){
					entriesToFit = getOverlappingEntries(primaryEntry, iF);
				}else{
					entriesToFit = new ArrayList<FastSpecEntry>();
					entriesToFit.add(primaryEntry);
				}
				remainingEntires.removeAll(entriesToFit);
				
				//so that the init result actually represents the fit,
				//we need to overwrite the *init values of matched parameters
				for(FastSpecEntry entry : entriesToFit){
					if(entry.amplitudeMatch != null && 
							entry.amplitudeMatch.matches("[\\(]*null[\\)]*"))
						entry.amplitudeMatch = null;
									
					if(entry.amplitudeMatch != null){
						String matchID = entry.amplitudeMatch;
						String parts[] = matchID.split("\\*");
						double scale = 1.0;
						if(parts.length > 1){
							matchID = parts[0];
							scale = Double.parseDouble(parts[1]);
						}
						if(entry.id.equals(matchID))
							throw new RuntimeException("Can't match amp of "+entry.id+" to itself.");
						
						FastSpecEntry matchedEntry = findEntry(matchID);
						
						entry.amplitudeInit = matchedEntry.amplitudeInit * scale;
					}
					
					
					if(entry.sigmaMatch != null){
						String matchID = entry.sigmaMatch;
						String parts[] = matchID.split("\\*");
						double scale = 1.0;
						if(parts.length > 1){
							matchID = parts[0];
							scale = Double.parseDouble(parts[1]);
						}
						if(entry.id.equals(matchID))
							throw new RuntimeException("Can't match amp of "+entry.id+" to itself.");
						
						FastSpecEntry matchedEntry = findEntry(matchID);
						
						entry.sigmaInit = matchedEntry.sigmaInit * scale;
					}
				}
				
				
				FrameData frameD = fillData(entriesToFit, iF);
				
				boolean anyViable = false;
				for(FastSpecEntry specEntry : entriesToFit) {
					if(specEntry.minViableDataRange <= 0 || (frameD.maxData - frameD.minData) > specEntry.minViableDataRange) {
						anyViable = true;
						break;
					}
				}
				if(!anyViable)
					continue;

				double baselineInfo[] = findBaseline(frameD); //find baseline and estimate point error from it
				applyIgnoreRanges(entriesToFit, frameD); //add ignored ranges			
				initFrameFit(entriesToFit, frameD, iF, baselineInfo);
				fillBaselineAndInit(entriesToFit, frameD, iF);				
				doFit(entriesToFit, iF, frameD, baselineInfo[0]);

				for(FastSpecEntry specEntry : entriesToFit){
					String metaName = "FastSpec/" + specEntry.id;
					if (config.saveMetadata) {
						connectedSource.setImageMetaData(metaName + "/fitX0", iF, specEntry.fit.X0[iF]);
						connectedSource.setImageMetaData(metaName + "/fitY0", iF, specEntry.fit.Y0[iF]);
						connectedSource.setImageMetaData(metaName + "/fitSlope", iF, specEntry.fit.Slope[iF]);
						connectedSource.setImageMetaData(metaName + "/fitSigma", iF, specEntry.fit.Sigma[iF]);
						connectedSource.setImageMetaData(metaName + "/fitAmplitude", iF,
								specEntry.fit.Amplitude[iF]);
					}
					
					inferredQuantities(CalcStage.SingleFrame, specEntry, iF);
				}
				
			//} catch (RuntimeException err) {
			//	throw new RuntimeException(
			//			"Exception preparing or fitting entry '" + primaryEntry.id + "' for frame " + iF, err);
			//}
		}
	}

	private ArrayList<FastSpecEntry> getOverlappingEntries(FastSpecEntry primaryEntry, int iF) {
		ArrayList<FastSpecEntry> overlappedEntries = new ArrayList<FastSpecEntry>();
		overlappedEntries.add(primaryEntry); //include the original (not necessarily included, if disabled!)
		
		FastSpecEntry fullWindowEntry = new FastSpecEntry("TEMP");
		fullWindowEntry.x0 = Double.min(primaryEntry.x0, primaryEntry.x1);
		fullWindowEntry.x1 = Double.max(primaryEntry.x0, primaryEntry.x1);
		fullWindowEntry.y0 = primaryEntry.y0;
		fullWindowEntry.y1 = primaryEntry.y1;
		
		
		int lastNFound = -1;
		for(int j=0; j < 100; j++){ //round 1: find min/max window,
			int nFound = 0;
			Iterator<FastSpecEntry> it2 = config.specEntries.iterator();
			while(it2.hasNext()){
				FastSpecEntry entry = it2.next();
				if(!entry.enable)
					continue;
				if(entry.fit.frameEnable != null && !entry.fit.frameEnable[iF])
					continue;
				if(entry != primaryEntry && entryBoxIntersects(fullWindowEntry, entry)){
					fullWindowEntry.x0 = Double.min(Double.min(fullWindowEntry.x0, entry.x0), entry.x1);
					fullWindowEntry.x1 = Double.max(Double.max(fullWindowEntry.x1, entry.x0), entry.x1);
					fullWindowEntry.y0 = Double.min(Double.min(fullWindowEntry.y0, entry.y0), entry.y1);
					fullWindowEntry.y1 = Double.max(Double.max(fullWindowEntry.y1, entry.y0), entry.y1);
					
					//System.out.println(entry.id + "("+entry.x0+"-" + entry.x1 +", " + entry.y0 + " - " + entry.y1 
					//		+ ") overlaps " + primaryEntry.id + 
					//		"("+primaryEntry.x0+"-" + primaryEntry.x1 +", " + primaryEntry.y0 + " - " + entry.y1+")");
					
					if(!overlappedEntries.contains(entry))
						overlappedEntries.add(entry);
					nFound++;
				}
			}
			//System.out.println("nFound = " + nFound + ", lastNFound " + lastNFound);
			if(nFound == lastNFound)
				return overlappedEntries;
			
			lastNFound = nFound;
		}

		
		return overlappedEntries;

	}

	private final boolean entryBoxIntersects(FastSpecEntry e1, FastSpecEntry e2) {
		double e1x0 = FastMath.min(e1.x0, e1.x1);
		double e1x1 = FastMath.max(e1.x0, e1.x1);
		double e1y0 = FastMath.min(e1.y0, e1.y1);
		double e1y1 = FastMath.max(e1.y0, e1.y1);
		double e2x0 = FastMath.min(e2.x0, e2.x1);
		double e2x1 = FastMath.max(e2.x0, e2.x1);
		double e2y0 = FastMath.min(e2.y0, e2.y1);
		double e2y1 = FastMath.max(e2.y0, e2.y1);
		return !((e1x0 >= e2x1) //e1 right of e2
				|| (e1x1 <= e2x0) // e1 left of e2 
				|| (e1y0 >= e2y1) // e1 below e2
				|| (e1y1 <= e2y0)); // e1 above e2
	}

	private void setupCoords() {
		if (config.coordType == FastSpecConfig.COORDTYPE_CCDGEOM) {
			ccdGeom = new PhysicalROIGeometry();
			ccdGeom.setFromCameraMetadata(connectedSource.getCompleteSeriesMetaDataMap().asJavaMap());
		} else {
			ccdGeom = null;
		}

		if (config.coordType == FastSpecConfig.COORDTYPE_WAVELENGTH_AXIS) {
			String name = connectedSource.getCompleteSeriesMetaDataMap().findEndsWith("wavelength", false);
			Object o = (name == null) ? null : connectedSource.getSeriesMetaData(name);
			Img img = getSelectedImageFromConnectedSource();
			if(img == null)
				img = connectedSource.getImage(0);
			if(img == null)
				throw new RuntimeException("Selection image and image[0] are invalid. Are there any valid images?");
			
			int nL = (img == null) ? -1 : (config.wavelenIsY ? img.getHeight() : img.getWidth());
			int nCh = (img == null) ? -1 : (config.wavelenIsY ? img.getWidth() : img.getHeight());
			if (o != null)
				o = Mat.fixWeirdContainers(o, true);

			if (o == null || !o.getClass().isArray()
					|| (nL > 0 && Array.getLength(o) != nL && Array.getLength(o) != nCh)) {
				throw new RuntimeException(
						"No metadata named '...wavelength', or not the correct size/shape/type of array.");
			}
			// wavelength metadata which is wavelength at each pixel

			o = Mat.fixWeirdContainers(o, true);
			if (o instanceof float[]) {
				o = Mat.convert1DArray(o, new double[((float[]) o).length]);
			}

			wavelengthMetadata = new double[nCh][];
			wavelengthToPixel = new Interpolation1D[nCh];

			boolean is2D = Array.get(o, 0).getClass().isArray();

			double pixel[] = null;
			double wavelen[] = null;
			Interpolation1D interp = null;

			for (int iCh = 0; iCh < nCh; iCh++) {

				if (is2D || iCh == 0) {
					pixel = new double[nL];
					wavelen = new double[nL];

					Object rowObj = is2D ? Array.get(o, iCh) : o;
					for (int iL = 0; iL < nL; iL++) {
						pixel[iL] = iL;
						wavelen[iL] = (double) (Number) Array.get(rowObj, iL);
					}
					interp = new Interpolation1D(wavelen, pixel);
				}

				wavelengthMetadata[iCh] = wavelen;
				wavelengthToPixel[iCh] = interp;
			}

		} 
	}

	public final void setBoxInPixels(FastSpecEntry specEntry, int x0, int y0, int x1, int y1) {
		switch (config.coordType) {
		case FastSpecConfig.COORDTYPE_PIXELS:
			specEntry.x0 = x0;
			specEntry.y0 = y0;
			specEntry.x1 = x1;
			specEntry.y1 = y1;
			return;

		case FastSpecConfig.COORDTYPE_CCDGEOM:
			specEntry.x0 = ccdGeom.pixelToPhysX(x0);
			specEntry.y0 = ccdGeom.pixelToPhysY(y0);
			specEntry.x1 = ccdGeom.pixelToPhysX(x1);
			specEntry.y1 = ccdGeom.pixelToPhysY(y1);
			return;

		case FastSpecConfig.COORDTYPE_WAVELENGTH_AXIS:
			if (config.wavelenIsY) {
				specEntry.x0 = x0;
				specEntry.x1 = x1;
				specEntry.y0 = wavelengthMetadata[x0][y0];
				specEntry.y1 = wavelengthMetadata[x0][y1];

			} else {
				specEntry.x0 = wavelengthMetadata[y0][x0];
				specEntry.x1 = wavelengthMetadata[y0][x1];
				specEntry.y0 = y0;
				specEntry.y1 = y1;
			}
			break;
		
		default:
			throw new IllegalArgumentException("Invalid coord type");
		}
	}

	public final int[] getBoxInPixels(FastSpecEntry specEntry) {
		switch (config.coordType) {
		case FastSpecConfig.COORDTYPE_PIXELS:
			return new int[] { (int) specEntry.x0, (int) specEntry.y0, (int) specEntry.x1, (int) specEntry.y1 };

		case FastSpecConfig.COORDTYPE_CCDGEOM:
			return new int[] { ccdGeom.physToPixelX(specEntry.x0), ccdGeom.physToPixelY(specEntry.y0),
					ccdGeom.physToPixelX(specEntry.x1), ccdGeom.physToPixelY(specEntry.y1), };
		case FastSpecConfig.COORDTYPE_WAVELENGTH_AXIS:
			if (wavelengthToPixel == null) {
				setupCoords();
				if (wavelengthToPixel == null)
					throw new RuntimeException("No wavelength interpolation");
			}

			if (config.wavelenIsY) {
				return new int[] { (int) specEntry.x0, (int) wavelengthToPixel[(int) specEntry.x0].eval(specEntry.y0),
						(int) specEntry.x1, (int) wavelengthToPixel[(int) specEntry.x0].eval(specEntry.y1) };
			} else {
				return new int[] { (int) wavelengthToPixel[(int) specEntry.y0].eval(specEntry.x0), (int) specEntry.y0,
						(int) wavelengthToPixel[(int) specEntry.y0].eval(specEntry.x1), (int) specEntry.y1 };
			}

		default:
			throw new IllegalArgumentException("Invalid coord type");
		}
	}

	public final int toPixelX(double x, int wavlenRow) {
		switch (config.coordType) {
		case FastSpecConfig.COORDTYPE_PIXELS:
			return (int) x;
		case FastSpecConfig.COORDTYPE_CCDGEOM:
			return ccdGeom.physToPixelX(x);
		case FastSpecConfig.COORDTYPE_WAVELENGTH_AXIS:
			if (config.wavelenIsY)
				return (int) x;
			else
				return (int) wavelengthToPixel[wavlenRow].eval(x);

		default:
			throw new IllegalArgumentException("Invalid coord type");
		}
	}

	public final int toPixelY(double y, int wavlenRow) {
		switch (config.coordType) {
		case FastSpecConfig.COORDTYPE_PIXELS:
			return (int) y;
		case FastSpecConfig.COORDTYPE_CCDGEOM:
			return ccdGeom.physToPixelY(y);
		case FastSpecConfig.COORDTYPE_WAVELENGTH_AXIS:
			if (config.wavelenIsY)
				return (int) wavelengthToPixel[wavlenRow].eval(y);
			else
				return (int) y;

		default:
			throw new IllegalArgumentException("Invalid coord type");
		}
	}

	private FrameData fillData(ArrayList<FastSpecEntry> entriesToFit, int iF) {

		//find widest min/max of all overlapping entries 
		double minX0 = Double.POSITIVE_INFINITY, maxX1 = Double.NEGATIVE_INFINITY;
		double minY0 = Double.POSITIVE_INFINITY, maxY1 = Double.NEGATIVE_INFINITY;
		for(FastSpecEntry entry : entriesToFit){
			//sometimes x0,x1 are the wrong way around (if the wavlength cal is decreasing)
			double x0 = Double.min(entry.x0, entry.x1);
			if(x0 < minX0) minX0 = x0;			
			
			double x1 = Double.max(entry.x0, entry.x1); 
			if(x1 > maxX1) maxX1 = x1;
			
			double y0 = Double.min(entry.y0, entry.y1); 
			if(y0 < minY0) minY0 = entry.y0;
			
			double y1 = Double.max(entry.y0, entry.y1); 
			if(y1 > maxY1) maxY1 = y1;
			
		}

		int p0, p1; // scan direction
		int a0, a1; // averaging direction

		if (config.wavelenIsY) {
			p0 = toPixelY(minY0, (int) minX0);
			p1 = toPixelY(maxY1, (int) minX0);
			a0 = toPixelX(minX0, -1);
			a1 = toPixelX(maxX1, -1);
		} else {
			p0 = toPixelX(minX0, (int) minY0);
			p1 = toPixelX(maxX1, (int) minY0);
			a0 = toPixelY(minY0, -1);
			a1 = toPixelY(maxY1, -1);
		}
		
		//which now might be the wrong way around again
		if(p0 > p1){ int i=p0; p0=p1; p1=i; }
		if(a0 > a1){ int i=a0; a0=p1; a1=i; }
		
		int nP = p1 - p0;
		int nA = a1 - a0;
		int meanA = (a0 + a1) / 2;

		FrameData frameD = new FrameData(nP, entriesToFit.size());

		Img image = connectedSource.getImage(iF);

		try {
			ReadLock readLock = image.readLock();
			readLock.lockInterruptibly();
			try {
				frameD.minData = Double.POSITIVE_INFINITY;
				frameD.maxData = Double.NEGATIVE_INFINITY;
				for (int iP = 0; iP < nP; iP++) {
					int p = p0 + iP;

					switch (config.coordType) {
					case FastSpecConfig.COORDTYPE_PIXELS:
						frameD.x[iP] = p;
						break;
					case FastSpecConfig.COORDTYPE_CCDGEOM:
						frameD.x[iP] = config.wavelenIsY ? ccdGeom.pixelToPhysY(p) : ccdGeom.pixelToPhysX(p);
						break;
					case FastSpecConfig.COORDTYPE_WAVELENGTH_AXIS:
						if (p < 0)
							p = 0;
						if (p >= wavelengthMetadata[a0].length)
							p = wavelengthMetadata[a0].length - 1;

						frameD.x[iP] = (p < 0 || p > wavelengthMetadata[a0].length) ? 0 : wavelengthMetadata[a0][p];
						break;
					}

					frameD.data[iP] = 0;
					if (config.wavelenIsY) {
						for (int a = a0; a < a1; a++)
							frameD.data[iP] += image.getPixelValue(readLock, a, p);
					} else {
						for (int a = a0; a < a1; a++)
							frameD.data[iP] += image.getPixelValue(readLock, p, a);
					}
					frameD.data[iP] /= nA;

					if(frameD.data[iP] < frameD.minData)
						frameD.minData = frameD.data[iP];
					if(frameD.data[iP] > frameD.maxData)
						frameD.maxData = frameD.data[iP];
				}
			} finally {
				readLock.unlock();
			}

		} catch (InterruptedException e) {
			throw new RuntimeException("FastSpecProcessor: Interrupted in data collection.");
		}

		return frameD;
	}

	private void doFit(ArrayList<FastSpecEntry> entriesToFit, int iF, FrameData frameD, double baselineX0) {

		CostF costF = new CostF(frameD.x, frameD.data, frameD.error, entriesToFit.size());
		costF.baselineX0 = baselineX0;
		
		double p0[], p1[];
		if(config.amplitudeFit != AmpFitMode.NONE){
			p0 = buildParamsArray(entriesToFit, costF, iF, frameD, true);
			p1 = fitLoop(costF, iF, frameD, p0);		
			postFit(entriesToFit, costF, iF, frameD, p1);
			
			updateAllControllers();
		}		
		
		if(config.amplitudeFit != AmpFitMode.AMP_ONLY){
			p0 = buildParamsArray(entriesToFit, costF, iF, frameD, false);
			p1 = fitLoop(costF, iF, frameD, p0);
			if(p1 == null)
				throw new RuntimeException("fit result is null");
			postFit(entriesToFit, costF, iF, frameD, p1);
		}
		
	}
	
	/** Setup parameter indexing and cost function.
	 * Doesn't modify entries
	 * amplitudeOnly - Include parameters for Gaussian amplitudes and also baseline offset and slope 
	 * */
	private double[] buildParamsArray(ArrayList<FastSpecEntry> entriesToFit, CostF costF, int iF, FrameData frameD, boolean amplitudeOnly){
		int nEntries = entriesToFit.size();
		FastSpecEntry entry0 = entriesToFit.get(0);

		int maxParams = 2 + nEntries * 4;
		String pNames[] = new String[maxParams];
		double p0[] = new double[maxParams];
		double domainMin[] = new double[maxParams];
		double domainMax[] = new double[maxParams];
		
		costF.g[0].x0Offset = 0;
		
		int nP=0; //next parameter
		
		int pY0 = nP++; 
		int pSlope = nP++;
		
		pNames[pY0] = "Y0";
		pNames[pSlope] = "slope";
		
		//common baseline params only from first entry  
		p0[pY0] = entry0.fit.Y0[iF];
		p0[pSlope] = entry0.fit.Slope[iF];		
		domainMin[pY0] = entry0.y0Min;
		domainMin[pSlope] = entry0.slopeMin;		
		domainMax[pY0] = entry0.y0Max;
		domainMax[pSlope] = entry0.slopeMax;
		
		//particular hack to indicate y0 min/max should be set near y0init, after its been determined
		if(Double.isFinite(entry0.clampY0ToBaselineInit) && entry0.clampY0ToBaselineInit != 0){
			double sign = FastMath.signum(p0[pY0]); //don't get min/max swapped
			domainMin[pY0] = p0[pY0] * (1.0 - sign*entry0.clampY0ToBaselineInit);
			domainMax[pY0] = p0[pY0] * (1.0 + sign*entry0.clampY0ToBaselineInit);
		}
	
		boolean[] goodFrames = (boolean[])connectedSource.getSeriesMetaData("isGoodData");
		
		//clear all params first, in case costF was already used before
		for(int i=0; i < nEntries; i++){
			costF.g[i].pAmp = -1;
			costF.g[i].pPower = -1;
			costF.g[i].pSlope = -1;
			costF.g[i].pX0 = -1;
			costF.g[i].pY0 = -1;
			costF.g[i].pSigma = -1;
		}
		
		//System.out.println("Creating parameters for the following entries:");
		//for(FastSpecEntry entry : entriesToFit){
		//	System.out.println("   " + entry.id);			
		//}
		
		for(int i=0; i < nEntries; i++){
			costF.g[i].pY0 = pY0;
			costF.g[i].pSlope = pSlope;
			
			FastSpecEntry entry = entriesToFit.get(i);
			// optimistion had better parameter ranging when Dx0/x0 is small, so
			// offset from init value
			costF.g[i].entryName = entry.id;
			costF.g[i].x0Offset = Double.isNaN(entry.x0Init) ? 0 : entry.x0Init;
	
			// prior to stay somewhere near the initial guesses
			costF.g[i].priorAmp0_mean = Double.isNaN(entry.amplitudePriorMean) ? 0 : entry.amplitudePriorMean;
			costF.g[i].priorAmp0_sigma = Double.isNaN(entry.amplitudePriorSigma) ? Double.POSITIVE_INFINITY : entry.amplitudePriorSigma;
			costF.g[i].priorSigma_mean = Double.isNaN(entry.sigmaPriorMean) ? 0 : entry.sigmaPriorMean;
			costF.g[i].priorSigma_sigma = Double.isNaN(entry.sigmaPriorSigma) ? Double.POSITIVE_INFINITY : entry.sigmaPriorSigma;
			costF.g[i].priorSlope_mean = Double.isNaN(entry.slopePriorMean) ? 0 : entry.slopePriorMean;
			costF.g[i].priorSlope_sigma = Double.isNaN(entry.slopePriorSigma) ? Double.POSITIVE_INFINITY : entry.slopePriorSigma;
			costF.g[i].priorX0_mean = Double.isNaN(entry.x0PriorMean) ? 0 : (entry.x0PriorMean - costF.g[i].x0Offset);
			costF.g[i].priorX0_sigma = Double.isNaN(entry.x0PriorSigma) ? Double.POSITIVE_INFINITY : entry.x0PriorSigma;
			costF.g[i].priorPower_mean = Double.isNaN(entry.powerPriorMean) ? 0 : entry.powerPriorMean;
			costF.g[i].priorPower_sigma = (!(entry.powerPriorSigma > 0)) ? Double.POSITIVE_INFINITY : entry.powerPriorSigma;
			
			//frame specific priors override the general ones
			if(entry.fit.frameAmplitudePriorMean != null)
				costF.g[i].priorAmp0_mean = entry.fit.frameAmplitudePriorMean[iF];
			if(entry.fit.frameSigmaPriorMean != null)
				costF.g[i].priorSigma_mean = entry.fit.frameSigmaPriorMean[iF];
			if(entry.fit.framePowerPriorMean != null)
				costF.g[i].priorPower_mean = entry.fit.framePowerPriorMean[iF];
			
			//System.out.println("Added g["+i+"] for " + entry.id);
				 
			if(costF.g[i].pAmp < 0){ //if not already set by matched components
				
				int iP = nP++;
				costF.g[i].pAmp = iP; 
				p0[iP] = entry.fit.Amplitude[iF];
				domainMin[iP] = entry.amplitudeMin;
				domainMax[iP] = entry.amplitudeMax;
				pNames[iP] = entry.id + "_Amp";
				//System.out.println("Add p["+iP+"] = " + pNames[iP]);
	
				//special hack to disallow active component when beam is off			
				if(goodFrames != null && !goodFrames[iF] && entry.id.contains("_ACX")){
					
					domainMin[iP] = 0.00;				
					domainMax[iP] = 0.02;
					p0[iP] = 0.01;				
					
				}
				
				if(entry.amplitudeMatch != null && 
						entry.amplitudeMatch.matches("[\\(]*null[\\)]*"))
					entry.amplitudeMatch = null;
								
				if(entry.amplitudeMatch != null){
					
					String matchID = entry.amplitudeMatch;
					String parts[] = matchID.split("\\*");
					double scale = 1.0;
					if(parts.length > 1){
						matchID = parts[0];
						scale = Double.parseDouble(parts[1]);
					}
					if(entry.id.equals(matchID))
						throw new RuntimeException("Can't match amp of "+entry.id+" to itself.");
					
					//link to amplitude of a matched component (e.g. pi+, pi- in multiplets)
					Logger.getLogger(this.getClass().getName()).fine("Looking for " + matchID + " to match " + entry.id + " amp");
					
					FastSpecEntry matchedEntry = findEntry(matchID);
					int matchedIdx = entriesToFit.indexOf(matchedEntry);
					
					if(matchedIdx < 0)
						throw new RuntimeException("Unable to find entry '"+matchID+"' for matched amplitude from entry '"+entry.id+"'");
					
					Logger.getLogger(this.getClass().getName()).fine("Found g["+matchedIdx+"]("+costF.g[matchedIdx].entryName+")"
					     + " to match " + matchID + " for " + entry.id);
									
					if(costF.g[matchedIdx].pAmp > 0){
						//already had one, just use that and drop the one we just made
						costF.g[i].pAmp = costF.g[matchedIdx].pAmp;
						costF.g[i].sAmp = scale; //and this entry's amplitude scales against it
						Logger.getLogger(this.getClass().getName()).fine("Processing entry " + i + " (" +entry.id+ "), g["+matchedIdx+"] already had pAmp at p["+costF.g[i].pAmp+"] ("
											+pNames[costF.g[i].pAmp]+"). Using that and dropping p["+nP+"]. Now have " + nP + " parameters");						
						nP--;
						if(costF.g[matchedIdx].pAmp == nP)
							throw new RuntimeException("Removing the matched fit parameter");
					}else{
						//doesnt have one, use the one we just made for that one too
						//System.out.println("g["+matchedIdx+"] didn't have pAmp. Using the now created p["+nP+"]");
						iP = costF.g[i].pAmp;
						costF.g[matchedIdx].pAmp = iP;
						costF.g[matchedIdx].sAmp = 1.0; //and that one has no scaling
						costF.g[i].sAmp = scale; //but this one scales against it
						
						//and we use that one's range and prior
						p0[iP] = matchedEntry.fit.Amplitude[iF];
						domainMin[iP] = matchedEntry.amplitudeMin;
						domainMax[iP] = matchedEntry.amplitudeMax;
						pNames[iP] = matchedEntry.id + "_Amp";
						
						Logger.getLogger(this.getClass().getName()).fine("Processing entry " + i + " (" +entry.id+ "), g["+matchedIdx+"] had no pAmp already, so using this one p["+iP+"] ("
								+pNames[iP]+") also for this ("+entry.id+"). Now have " + nP + " parameters");						
			
					}
					
					
				}
			}else{
				//System.out.println(entry.id + "_Amp was already set by matched component");
			}
			
			if(!amplitudeOnly){
				if(entry.x0Max == entry.x0Min){
					//x0 is fixed
					costF.g[i].pX0 = -1;
					costF.g[i].vX0 = entry.x0Min - costF.g[i].x0Offset;
				}else{
					int iP = nP++;
					costF.g[i].pX0 = iP;
					p0[iP] = entry.fit.X0[iF] - costF.g[i].x0Offset;
					domainMin[iP] = entry.x0Min - costF.g[i].x0Offset;
					domainMax[iP] = entry.x0Max - costF.g[i].x0Offset;
					pNames[iP] = entry.id + "_X0";
					//System.out.println("Add p["+iP+"] = " + pNames[iP]);
				}
				
				if(costF.g[i].pSigma < 0){ //if not already set by matched components
						
					if(entry.sigmaMax == entry.sigmaMin){
						//sigma is fixed
						costF.g[i].pSigma = -1;
						costF.g[i].vSigma = entry.sigmaMin;
						
					}else {
						int iP = nP++;
						costF.g[i].pSigma = iP;
						p0[iP] = entry.fit.Sigma[iF];
						domainMin[iP] = entry.sigmaMin;
						domainMax[iP] = entry.sigmaMax;
						pNames[iP] = entry.id + "_Sigma";
						//System.out.println("Add p["+iP+"] = " + pNames[iP]);
						if(entry.sigmaMatch != null && (entry.sigmaMatch.length() == 0 
												|| entry.sigmaMatch.equalsIgnoreCase("(null)")))
							entry.sigmaMatch = null; //should be null
						
						if(entry.sigmaMatch != null){
							
							String matchID = entry.sigmaMatch;
							String parts[] = matchID.split("\\*");
							double scale = 1.0;
							if(parts.length > 1){
								matchID = parts[0];
								scale = Double.parseDouble(parts[1]);
							}
							
							FastSpecEntry matchedEntry = findEntry(matchID);				 
							int matchedIdx = entriesToFit.indexOf(matchedEntry);
						
							if(matchedIdx < 0)
								throw new RuntimeException("Unable to find entry '"+matchID+"' for matched sigma from entry '"+entry.id+"'");
							
							//link to amplitude of a matched component (e.g. pi+, pi- in multiplets)
							
							Logger.getLogger(this.getClass().getName()).fine("Linking sigma to g["+matchedIdx+"]("+costF.g[matchedIdx].entryName+")"
								     + " to match " + matchID +" for " + entry.id + " sigma");
								
							if(costF.g[matchedIdx].pSigma > 0){
								//already had one, just use that and drop the one we just made
								costF.g[i].pSigma = costF.g[matchedIdx].pSigma;
								costF.g[i].sSigma = scale;
								Logger.getLogger(this.getClass().getName()).finest("Processing entry " + i + " (" +entry.id+ "), g["+matchedIdx+"] already had pSigma at p["+costF.g[i].pSigma+"] ("
										+pNames[costF.g[i].pSigma]+"). Using that and dropping p["+nP+"]. Now have " + nP + " parameters");						
								nP--;
							}else{
								//doesnt have one, use the one we made for that one too
								//System.out.println("g["+matchedIdx+"] didn't have pSigma. Using the now created p["+nP+"]");
								
								iP = costF.g[i].pSigma;
								costF.g[matchedIdx].pSigma = iP;
								costF.g[matchedIdx].sSigma = 1.0;
								costF.g[i].sSigma = scale;
								
								//and we use that one's range and prior
								p0[iP] = matchedEntry.fit.Sigma[iF];
								domainMin[iP] = matchedEntry.sigmaMin;
								domainMax[iP] = matchedEntry.sigmaMax;
								pNames[iP] = matchedEntry.id + "_Sigma";
								
								Logger.getLogger(this.getClass().getName()).finest("Processing entry " + i + " (" +entry.id+ "), g["+matchedIdx+"] had no pSigma already, so using this one p["+iP+"] ("
										+pNames[iP]+") also for this ("+entry.id+"). Now have " + nP + " parameters");						
					
							}
							
							
						}
					}
				}else{
					//already set by matched component
				}
				
				if(entry.powerMax == entry.powerMin){
					//power is fixed (probably fixed to a gaussian)
					if(entry.powerMax == 0)
						throw new IllegalArgumentException("Supergaussian power fixed at min=max=0. Probably wasn't loaded properly.");
					costF.g[i].pPower = -1;
					costF.g[i].vPower = entry.powerMin;
				}else{
					int iP = nP++;
					costF.g[i].pPower = iP;
					p0[iP] = entry.fit.Power[iF];
					domainMin[iP] = entry.powerMin;
					domainMax[iP] = entry.powerMax;
					pNames[iP] = entry.id + "_Power";
					//System.out.println("Add p["+iP+"] = " + pNames[iP]);
					
				}
				
				
			}else{
				costF.g[i].pX0 = -1;
				costF.g[i].vX0 = entry.fit.X0[iF] - costF.g[i].x0Offset;
				costF.g[i].pSigma = -1;
				costF.g[i].vSigma = entry.fit.Sigma[iF];
				costF.g[i].pPower = -1;
				costF.g[i].vPower = entry.fit.Power[iF];
			}			
		}
		
		if(nP < maxParams){
			p0 = Arrays.copyOf(p0, nP);
			domainMin = Arrays.copyOf(domainMin, nP);
			domainMax = Arrays.copyOf(domainMax, nP);
			pNames = Arrays.copyOf(pNames, nP);
		}
				
		for(int i=0; i< nP; i++){
			if(Double.isNaN(domainMin[i])) domainMin[i] = Double.NEGATIVE_INFINITY;
			if(Double.isNaN(domainMax[i])) domainMax[i] = Double.POSITIVE_INFINITY;			
		}
		
		costF.setDomain(new RectangularDomain(domainMin, domainMax));
				
		// force in limits
		double bounds[][] = costF.getDomain().getRectangularBounds();
		for (int j = 0; j < p0.length; j++) {
			if (p0[j] < bounds[j][0])
				p0[j] = bounds[j][0];
			if (p0[j] > bounds[j][1])
				p0[j] = bounds[j][1];
			
			if (bounds[j][0] >= bounds[j][1])
				throw new RuntimeException("Limits overlap! param["+j+"] ('"+pNames[j]+"'): " 
										+ bounds[j][0] +" - " + bounds[j][1]);
		}
		
		Logger logr = Logger.getLogger(this.getClass().getName());
		if(logr.isLoggable(Level.FINE)){
			logr.finest("Final parameters:");
			for(int iP=0; iP < pNames.length; iP++)
				logr.finest("   p["+iP+"] = " + pNames[iP]);
		}
		
		return p0;
	}

	/** Iterate the fitter */
	private double[] fitLoop(CostF costF, int iF, FrameData frameD, double p0[]){
	
		Optimizer opt = null;

		switch (config.optimser) {
			case HookeAndJeeves: {
				opt = new HookeAndJeeves(costF);
				break;
			}
			case GoldenSectionCoordinateDescent: {
				IStoppingCondition stopCond = new MaxIterCondition(100);
				BracketingByParameterSpace bracketMethod = new BracketingByParameterSpace();
				bracketMethod.range = 5000;
				LineOptimizer lineOptimiser = new GoldenSection(stopCond, bracketMethod);
				SearchDirectionMethod searchDirMethod = new CoordinateDescentDirection();
				opt = new LineSearchOptimizer(costF, searchDirMethod, lineOptimiser);
				break;
			}
			case GoldenSectionGradientDescent: {
				IStoppingCondition stopCond = new MaxIterCondition(200);
				IBracketingMethod bracketMethod = null;// new
														// BracketingByParameterSpace();
				// LineOptimizer lineOptimiser = new GoldenSection(stopCond,
				// bracketMethod);
				LineOptimizer lineOptimiser = new NewtonsMethod1D(stopCond);
				SearchDirectionMethod searchDirMethod = new GradientDescentDirection();
				opt = new LineSearchOptimizer(costF, searchDirMethod, lineOptimiser);
				break;
			}
		}

		opt.setObjectiveFunction(costF);
		opt.init(p0);

		for (int i = 0; i < config.numIterations; i++) {
			if (abortCalc){
				setStatus("Aborted at frame " + iF + " / " + connectedSource.getNumImages() + " done");
				return null;
			}

			opt.refine();

			double p[] = opt.getCurrentPos();
			
			/*for (int j = 0; j < p0.length; j++) {
				if (p[j] < bounds[j][0])
					p[j] = bounds[j][0];
				if (p[j] > bounds[j][1])
					p[j] = bounds[j][1];
			}
			opt.init(p);*/
			
			double cost = opt.getCurrentValue();
			if(Double.isNaN(cost)){
				String msg = "NaN cost function!";
				if(config.errorOnNaNFits)
					throw new RuntimeException(msg);
				else
					Logger.getLogger(this.getClass().getName()).warning(msg);
			}
			//System.out.println("i=" + i + "\tcost=" + cost);
			// */

			//fillFrameFit(costF, frameD, p);
			// updateAllControllers();
		}

		return opt.getCurrentPos(); //final params		
	}
	
	private void fillFrameFitFromParams(CostF costF, FrameData frameD, double p[]){
		for (int iX = 0; iX < frameD.x.length; iX++) {
			double x = frameD.x[iX];
			frameD.fit[iX] = costF.func(x, p);
			frameD.residual[iX] = (frameD.data[iX] - frameD.fit[iX]) / frameD.error[iX];
			
			for(int iG=0; iG < frameD.componentsFit.length; iG++){
				frameD.componentsFit[iG][iX] = costF.g[iG].get(x, p);
			}
		}
	}
	
	
	/** Calculate uncertainties and write everything back into specEntries */ 
	private void postFit(ArrayList<FastSpecEntry> entriesToFit, CostF costF, int iF, FrameData frameD, double p1[]){
		int nEntries = entriesToFit.size();
		
		if(config.calcFitUncertainty){			
			calcFitUncertainty(entriesToFit, costF, iF, frameD, p1);
			
		}else{
			for(int i=0; i < nEntries; i++){
				FastSpecEntry entry = entriesToFit.get(i);
				entry.fit.UncertaintyAmpltiude[iF] = Double.NaN;
				entry.fit.UncertaintySigma[iF] = Double.NaN;
				entry.fit.UncertaintyX0[iF] = Double.NaN;
				entry.fit.UncertaintyY0[iF] = Double.NaN;
				entry.fit.UncertaintyPower[iF] = Double.NaN;
			}
		}

		//copy results from parameter arrays back to entries
		for(int i=0; i < nEntries; i++){
			FastSpecEntry entry = entriesToFit.get(i);
			entry.fit.Y0[iF] = p1[costF.g[0].pY0]; //FIXME: This is the common Y0, not the Y0 at the X0 of this gaussian!!
			entry.fit.Slope[iF] = p1[costF.g[0].pSlope];
			
			entry.fit.X0[iF] = costF.g[i].x0Offset + ((costF.g[i].pX0 >= 0) ? p1[costF.g[i].pX0] : costF.g[i].vX0);
			entry.fit.Amplitude[iF] = (costF.g[i].pAmp >= 0) ? (p1[costF.g[i].pAmp] * costF.g[i].sAmp) : costF.g[i].vAmp;
			entry.fit.Sigma[iF] = (costF.g[i].pSigma >= 0) ? (p1[costF.g[i].pSigma] * costF.g[i].sSigma) : costF.g[i].vSigma;
			entry.fit.Power[iF] = (costF.g[i].pPower >= 0) ? p1[costF.g[i].pPower] : costF.g[i].vPower;
			
			//calc RMS residual of the fit
			//ideally we would limit this to the window of this entry
			entry.fit.RMSError[iF] = 0;
			for(int iX=0; iX < frameD.x.length; iX++){
				frameD.residual[iX] = (frameD.data[iX] - frameD.fit[iX]) / frameD.error[iX];
				entry.fit.RMSError[iF] += FastMath.pow2(frameD.residual[iX]);
			}
			entry.fit.RMSError[iF] = FastMath.sqrt(entry.fit.RMSError[iF] / frameD.x.length);
		}

	}
	
	private static String matrixSyncObject = new String("matrixSyncObject");
	private void calcFitUncertainty(ArrayList<FastSpecEntry> entriesToFit, CostF costF, int iF, FrameData frameD, double p1[]) {
		if(p1 == null)
			return;
		int nP = p1.length;
		int nEntries = entriesToFit.size();
		FastSpecEntry entry0 = entriesToFit.get(0);
		
		double hessian[][] = new double[nP][nP];
		double dP[] = new double[nP];
		double p[] = p1.clone();

		//temporarily disable the hard limits of logP, so that we can do differentials
		// near the edges            
        RectangularDomain domain = (RectangularDomain)costF.getDomain();
		costF.setDomain(null);
		double bounds[][] = domain.getRectangularBounds();
		
		for(int i=0; i < nP; i++){
			//find a sensible finite difference step using the best information available
			if(Double.isFinite(bounds[i][1])){
				if(Double.isFinite(bounds[i][0])){
					dP[i] = 1e-4 * (bounds[i][1] - bounds[i][0]);						
				}else {
					dP[i] = 1e-4 * (bounds[i][1] - p1[i]);
				}					
			}else if(Double.isFinite(bounds[i][0])){
				dP[i] = 1e-4 * (p1[i] - bounds[i][0]);
			}else{
				dP[i] = 1e-6 * p1[i];
			}
			
			
			for(int j=0; j <= i; j++){
				//calc d log(P) / dP_i, dP_j with finite central difference
				
				double logPpp, logPmm, logPpm, logPmp;
				
				p[i] = p1[i]; p[j] = p1[j]; p[i] += dP[i]; p[j] += dP[j]; logPpp = costF.eval(p);
				p[i] = p1[i]; p[j] = p1[j]; p[i] -= dP[i]; p[j] -= dP[j]; logPmm = costF.eval(p);
				p[i] = p1[i]; p[j] = p1[j]; p[i] += dP[i]; p[j] -= dP[j]; logPpm = costF.eval(p);
				p[i] = p1[i]; p[j] = p1[j]; p[i] -= dP[i]; p[j] += dP[j]; logPmp = costF.eval(p);					
				p[i] = p1[i]; p[j] = p1[j];
				
				hessian[i][j] = (logPpp + logPmm - logPpm - logPmp) / (2*dP[i]) / (2*dP[j]);					
				hessian[j][i] = hessian[i][j];
			}
				
		}
		
		//reenable domain
		costF.setDomain(domain);			
		config.minInvCovarianceEigenvalue = 1e3;
		double cov[][] = null;			
		if(config.marginalUncertainties){ //invert hessian --> marginal uncertainties
			DenseMatrix hessMat = new DenseMatrix(hessian);

			if(config.forceNonNativeLAPACK){
			
				//force non-native LAPACK so it doesn't crash and kill the JVM
				System.setProperty("com.github.fommil.netlib.BLAS", "com.github.fommil.netlib.F2jBLAS");
				System.setProperty("com.github.fommil.netlib.LAPACK", "com.github.fommil.netlib.F2jLAPACK");
				System.setProperty("com.github.fommil.netlib.ARPACK", "com.github.fommil.netlib.F2jARPACK");
				String aa = System.getProperty("com.github.fommil.netlib.BLAS");
				String cc = System.getProperty("com.github.fommil.netlib.LAPACK");
				
				cov = checkedMatrixInverse(hessMat, iF, entry0);
				
			}else{ 
				//use Native LAPACK (which is faser)
				//although we're not sure its thread safe. It seems to crash a lot.
				//so perhaps it ends up slower due to this					
				synchronized(matrixSyncObject) {												
					Logger.getLogger(this.getClass().getName()).finest("Sync in " + matrixSyncObject);						
					cov = checkedMatrixInverse(hessMat, iF, entry0);					
					Logger.getLogger(this.getClass().getName()).finest("Sync out " + matrixSyncObject);					
				}
			}
		}
		
		for(int i=0; i < nEntries; i++){
			FastSpecEntry entry = entriesToFit.get(i);
			
			if(cov != null){ //marginals if requested and the inversion worked
				entry.fit.UncertaintyY0[iF] = FastMath.sqrt(cov[costF.g[0].pY0][costF.g[0].pY0]);
				//entry.fitUncertaintySlope[iF] = FastMath.sqrt(cov[CostF.P_M][CostF.P_M]);
				
				
				entry.fit.UncertaintyX0[iF] = (costF.g[i].pX0 >= 0)
						? FastMath.sqrt(cov[costF.g[i].pX0][costF.g[i].pX0])
						: Double.NaN;
				entry.fit.UncertaintyAmpltiude[iF] = (costF.g[i].pAmp >= 0) 
						? FastMath.sqrt(cov[costF.g[i].pAmp][costF.g[i].pAmp]) 
						: Double.NaN;
				entry.fit.UncertaintySigma[iF] = (costF.g[i].pSigma >= 0) 
						? FastMath.sqrt(cov[costF.g[i].pSigma][costF.g[i].pSigma])
						: Double.NaN;		
				entry.fit.UncertaintyPower[iF] = (costF.g[i].pPower >= 0) 
						? FastMath.sqrt(cov[costF.g[i].pPower][costF.g[i].pPower])
						: Double.NaN;
						
			}else{ //conditionals (fall-back if marginals failed)
				entry.fit.UncertaintyY0[iF] = FastMath.sqrt(1/hessian[costF.g[0].pY0][costF.g[0].pY0]);
				//entry.fitUncertaintySlope[iF] = FastMath.sqrt(1/hessian[CostF.P_M][CostF.P_M]);
				entry.fit.UncertaintyX0[iF] = (costF.g[i].pX0 >= 0) 
						? FastMath.sqrt(1/hessian[costF.g[i].pX0][costF.g[i].pX0]) 
						: Double.NaN;
				entry.fit.UncertaintyAmpltiude[iF] = (costF.g[i].pAmp >= 0) 
							? FastMath.sqrt(1/hessian[costF.g[i].pAmp][costF.g[i].pAmp]) 
							: Double.NaN;
				entry.fit.UncertaintySigma[iF] = (costF.g[i].pSigma >= 0)
						? FastMath.sqrt(1/hessian[costF.g[i].pSigma][costF.g[i].pSigma])
						: Double.NaN;
				entry.fit.UncertaintyPower[iF] = (costF.g[i].pPower >= 0)
						? FastMath.sqrt(1/hessian[costF.g[i].pPower][costF.g[i].pPower])
						: Double.NaN;
			}
			//apply scalings
			if(costF.g[i].pAmp >= 0)
				entry.fit.UncertaintyAmpltiude[iF] *= costF.g[i].sAmp;
			if(costF.g[i].pSigma >= 0)
				entry.fit.UncertaintySigma[iF] *= costF.g[i].sSigma;
		}
	}

	private double[][] checkedMatrixInverse(DenseMatrix hessMat, int iF, FastSpecEntry entry0) {
		//a common point to get stuck is while all threads wait for a very slow LAPACK call
		//so catch abort flag on all waiting threads before going into another one
		if (abortCalc){
			throw new RuntimeException("Aborted in matrix inverse lock wait");						
		}
		
		double det = seed.matrix.Mat.det(hessMat);
		if(!Double.isFinite(det)){
			Logger.getLogger(this.getClass().getName()).fine("Non finite determinant in uncertainty matrix");
			return null;
		}
		
		if(config.minInvCovarianceEigenvalue > 0)
			hessMat = (DenseMatrix)seed.matrix.Mat.makeSPD(hessMat, config.minInvCovarianceEigenvalue); 
			
		Matrix covMat = seed.matrix.Mat.inv(hessMat);
		
		//covMat = (DenseMatrix)Mat.makeSPD(covMat, 1e-5); 
		if(Double.isFinite(seed.matrix.Mat.det(covMat)))
			return covMat.toArray();
			
		Logger.getLogger(this.getClass().getName()).warning("Failed to invert covariance matrix for frame "+iF+" of " + entry0.id + ". Using conditional uncertainty");
		return null;
	}
	
	public static class GaussComponent {
		
		/** indices into parameters array for each property value, or -1 to use vXXX */
		public int pAmp, pX0, pSigma, pY0, pSlope, pPower;
		
		/** fixed values instead of parameters */
		public double vAmp, vX0, vSigma, vY0, vSlope, vPower;
		
		/** fixed scaling of the parameter value (but not of fixed v value). 
		 * Use when using same parameter for multiple components */
		public double sAmp = 1.0, sSigma = 1.0;
		
		/** Offset so that p[X0] is near 0 */
		public double x0Offset;
				
		/** Prior for each gaussian */
		public double priorAmp0_mean;
		public double priorAmp0_sigma;
		public double priorSigma_mean;
		public double priorSigma_sigma;
		public double priorSlope_mean;
		public double priorSlope_sigma;
		public double priorX0_mean;
		public double priorX0_sigma;	
		public double priorPower_mean;
		public double priorPower_sigma;	 
		 
		public String entryName;
		
		public GaussComponent() {
			pAmp = -1;
			pX0 = -1;
			pSigma = -1;
			pY0 = -1;
			pSlope = -1;
			pPower = 2;

			priorAmp0_mean = 0.0;
			priorAmp0_sigma = 1000;
			priorSigma_mean = 0;
			priorSigma_sigma = 1;
			priorSlope_mean = 0;
			priorSlope_sigma = 1000;
			priorX0_mean = 0;
			priorX0_sigma = 1000;
			priorPower_mean = 0;
			priorPower_sigma = 1000;
			x0Offset = 0;
		}
		
		public final double get(double x, double p[]){
			if(pAmp >= 0) vAmp = p[pAmp] * sAmp;
			if(pSigma >= 0) vSigma = p[pSigma] * sSigma;
			if(pX0 >= 0) vX0 = p[pX0];
			if(pPower >= 0) vPower = p[pPower];
			
			return vAmp / (vSigma * FastMath.sqrt(2 * Math.PI))
					* FastMath.exp(-FastMath.pow(FastMath.abs((x - (vX0 + x0Offset)) / vSigma), vPower) / 2);
			//return (FastMath.abs(x - (vX0 + x0Offset)) < 0.1) ? (p[pAmp] * sAmp): 0;
		}
		
		public final double logPrior(double p[]){
			if(pAmp >= 0) vAmp = p[pAmp] * sAmp;
			if(pSigma >= 0) vSigma = p[pSigma] * sSigma;
			if(pX0 >= 0) vX0 = p[pX0];
			
			double logPrior = FastMath.pow2(vSigma - priorSigma_mean) / (2 * priorSigma_sigma * priorSigma_sigma);
			logPrior += FastMath.pow2(vAmp - priorAmp0_mean) / (2 * priorAmp0_sigma * priorAmp0_sigma);
			logPrior += FastMath.pow2(vX0 - priorX0_mean) / (2 * priorX0_sigma * priorX0_sigma);
			logPrior += FastMath.pow2(vPower - priorPower_mean) / (2 * priorPower_sigma * priorPower_sigma);
			//slope and y0 are done outside, as they are only required once
			return logPrior;
		}
	}

	public static class CostF extends FunctionND {
		
		/** Data, x-axis and error */
		public double x[], d[], e[];

		/** Gaussian components in fit */
		public GaussComponent g[];
				
		/** X0 of baseline */
		public double baselineX0;
		
		public int nGaussians;
		
		public CostF(double x[], double d[], double e[], int nGaussians) {
			this.nGaussians = nGaussians;
			this.x = x;
			this.d = d;
			this.e = e;
			setDomain(new RectangularDomain(
					new double[] { Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY,
							Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY },
					new double[] { Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY,
							Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY }));

			g = new GaussComponent[nGaussians];
			for(int i=0; i < nGaussians; i++){
				g[i] = new GaussComponent();
			}
		}

		@Override
		public final double eval(double[] p) {
			double logLHD = 0;
			IDomain domain = getDomain();
			if(domain != null){
				double bounds[][] = domain.getRectangularBounds();				
				for (int j = 0; j < p.length; j++) {
					if (p[j] < bounds[j][0] || p[j] > bounds[j][1])
						return Double.POSITIVE_INFINITY;
				}
			}

			for (int i = 0; i < x.length; i++) {
				logLHD += FastMath.pow2(d[i] - func(x[i], p)) / (2 * e[i] * e[i]);
			}

			
			double logPrior = 0;
			//hack - borrowed for priorPower
			logPrior += FastMath.pow2(p[g[0].pSlope] - g[0].priorSlope_mean) / (2 * g[0].priorSlope_sigma * g[0].priorSlope_sigma);
			int nPG=3;
			for(int i=0; i < nGaussians; i++){
				logPrior += g[i].logPrior(p);
			}
			
			//special hack: prior on Pi/Sigma ratios for BES
			/*double priorSigmaPiMean = 0.50;
			double priorSigmaPiSigma = 0.05;
			for(int i=0; i < nGaussians; i++){
				if(g[i].entryName.contains("_Sigma")){
					int pSigmaAmp = g[i].pAmp;
					String piSearch = g[i].entryName.replace("_Sigma", "_Pi+");
					for(int j=0; j < nGaussians; j++){
						if(piSearch.equals(g[j].entryName)){
							int pPiAmp = g[j].pAmp;
							double rSigmaPi = (p[pSigmaAmp]/p[pPiAmp]);
							
							logPrior += FastMath.pow2(rSigmaPi - priorSigmaPiMean) / (2 * priorSigmaPiSigma * priorSigmaPiSigma);	
							break;
						}
					}
				}
			}*/
			
					
			return logLHD + logPrior;
		}

		public double func(double x, double p[]) {
			double val = p[g[0].pY0] + (x - baselineX0) * p[g[0].pSlope]; // baseline
			
			for(int i=0; i < nGaussians; i++){
				val += g[i].get(x,p);
			}
			return val;
		}

	}
	
	/** Initialise the fit parameters (in the entires) for one frame of a set of entries 
	 * according to the config init mode */
	public void initFrameFit(ArrayList<FastSpecEntry> entriesToFit, FrameData frameD, int iF, double baselineInfo[]) {
		
		int nP = frameD.x.length;
		if (nP < 5)
			throw new RuntimeException("Less than 5 points for entry '" + entriesToFit.get(0).id + "'");
		
		
		for(FastSpecEntry specEntry : entriesToFit){
			if(config.initMode == InitMode.CONFIG 
					|| (config.initMode == InitMode.LAST_FRAME && iF == 0)){
				//setup initial frame fit params from entry configs
				doFitInitSingle(specEntry, frameD, iF, baselineInfo);
				
			}else if(config.initMode == InitMode.LAST_FRAME){
				//copy last result of last frame
				specEntry.fit.Amplitude[iF] = specEntry.fit.Amplitude[iF-1];
				specEntry.fit.X0[iF] = specEntry.fit.X0[iF-1];
				specEntry.fit.Y0[iF] = specEntry.fit.Y0[iF-1];
				specEntry.fit.Sigma[iF] = specEntry.fit.Sigma[iF-1];
				specEntry.fit.Slope[iF] = specEntry.fit.Slope[iF-1];
				
			}else if(config.initMode == InitMode.CONTINUE){
				//Nothing todo, just start from the last result
				
			}
		}			
	}
	
	public void fillBaselineAndInit(ArrayList<FastSpecEntry> entriesToFit, FrameData frameD, int iF){ 
		int nP = frameD.x.length;
		
		//fill in the baseline and init traces
		for (int iP = 0; iP < nP; iP++) {
			FastSpecEntry entry0 = entriesToFit.get(0);		
			frameD.base[iP] = entry0.fit.Y0[iF] + (frameD.x[iP] - entry0.fit.X0[iF]) * entry0.fit.Slope[iF]; // baseline
			frameD.init[iP] = frameD.base[iP];
			
			for(int i=0; i < entriesToFit.size(); i++){
				FastSpecEntry specEntry = entriesToFit.get(i);
			
				// init fit
				frameD.init[iP] += specEntry.fit.Amplitude[iF] / (specEntry.fit.Sigma[iF] * FastMath.sqrt(2 * Math.PI)) * FastMath
								.exp(-FastMath.pow2((frameD.x[iP] - specEntry.fit.X0[iF]) / specEntry.fit.Sigma[iF]) / 2);
				
				frameD.componentIDs[i] = specEntry.id;
			}
		}
		
	}
	
	
	/** @return double[]{ x0, y0, m, baselineNoiseSigma } */
	private double[] findBaseline(FrameData frameD) {
		double sumY = 0, sumXY = 0, sumX2Y = 0; // for stats calcs

		int nP = frameD.x.length;
		int nBaseLineAvg = FastMath.min(20, nP / 6); // fraction of length to use as baseline and
									// noise estimation
		nBaseLineAvg = Math.max(nBaseLineAvg, 2);

		// init: find baseline as linear slope from average of box at either end
		double x0 = 0, x1 = 0, y0 = 0, y1 = 0;
		for (int j = 0; j < nBaseLineAvg; j++) {
			x0 += frameD.x[j];
			x1 += frameD.x[nP - j - 1];
			y0 += frameD.data[j];
			y1 += frameD.data[nP - j - 1];
		}
		x0 /= nBaseLineAvg;
		y0 /= nBaseLineAvg;
		x1 /= nBaseLineAvg;
		y1 /= nBaseLineAvg;
		double m = (y1 - y0) / (x1 - x0);

		// create baseline trace
		for (int iP = 0; iP < nP; iP++) {
			frameD.base[iP] = y0 + (frameD.x[iP] - x0) * m;
		}

		// estimate baseline noise from baseline
		double sumY2 = 0 * 0;
		sumY = 0;
		for (int j = 0; j < nBaseLineAvg; j++) {

			double y = frameD.data[j] - frameD.base[j];
			sumY += 0;
			sumY2 += y * y;

			y = frameD.data[nP - j - 1] - frameD.base[nP - j - 1];
			sumY += 0;
			sumY2 += y * y;
		}
		double baselineNoiseSigma = FastMath
				.sqrt(sumY2 / (2 * nBaseLineAvg) - FastMath.pow2(sumY / (2 * nBaseLineAvg)));
		//baselineNoiseSigma = 100;
		frameD.error = Mat.fillArray(baselineNoiseSigma, nP);
		
		for(int i=0; i < frameD.data.length; i++){
			if(frameD.data[i] >= config.saturationLevel)
				frameD.error[i] = config.saturationLevel * 10;
		}
		
		//Apply artificial error correction
		if(!Double.isNaN(config.expandErrorsX0)){			 
			for(int i=0; i < frameD.data.length; i++){
				frameD.error[i] += config.expandErrorsValue * FastMath.exp(-FastMath.pow2(frameD.x[i] - 656.28) / (2 * config.expandErrorsXSigma * config.expandErrorsXSigma));
			}
		}
		
		return new double[]{ x0, y0, m, baselineNoiseSigma};
	}
	
	private void applyIgnoreRanges(ArrayList<FastSpecEntry> entriesToFit, FrameData frameD){
		int nP = frameD.x.length;
		for(FastSpecEntry specEntry : entriesToFit){
			if (specEntry.ignoreX0Ranges != null) {
				for (int i = 0; i < nP; i++) {
					for (int j = 0; j < specEntry.ignoreX0Ranges.length; j++) {
						if (frameD.x[i] >= specEntry.ignoreX0Ranges[j][0]
								&& frameD.x[i] < specEntry.ignoreX0Ranges[j][1]) {
							frameD.error[i] = Double.POSITIVE_INFINITY;
						}
	
					}
				}
			}
		}
	}

	public void doFitInitSingle(FastSpecEntry specEntry, FrameData frameD, int iF, double baselineInfo[]) {
		
		int nP = frameD.x.length;
		
		double x0 = baselineInfo[0];
		double y0 = baselineInfo[1];
		double m = baselineInfo[2];
		
		// init: gaussian max, centre of weight and width relative to linear
		// baseline
		double v[] = new double[nP];

		double sumY = 0;
		double sumXY = 0;
		double sumX2Y = 0;

		//find position and height of maximum in range
		double max = Double.NEGATIVE_INFINITY;
		int maxPos = 0;
		for (int iP = 0; iP < nP; iP++) {
			double baseline = frameD.base[iP];
			double dx = 0;
			// try{
			dx = FastMath.abs(
					(iP == nP - 1) ? (frameD.x[iP] - frameD.x[iP - 1]) : (frameD.x[iP + 1] - frameD.x[iP]));
			// }catch(ArrayIndexOutOfBoundsException err){
			// System.out.println("wtf??");
			// }
			double y = (frameD.data[iP] - baseline);
			sumY += y * dx;
			sumXY += frameD.x[iP] * y * dx;
			sumX2Y += FastMath.pow2(frameD.x[iP]) * y * dx;

			if (y > max) {
				max = y; // without dx
				maxPos = iP;
			}
			v[iP] = y;
		}

		specEntry.fit.Amplitude[iF] = sumY * specEntry.amplitudeInitFraction;
		specEntry.fit.X0[iF] = frameD.x[maxPos];// sumXY / sumY;
		specEntry.fit.Y0[iF] = y0 + (specEntry.fit.X0[iF] - x0) * m;
		specEntry.fit.Sigma[iF] = FastMath.sqrt(sumX2Y / sumY - FastMath.pow2(sumXY / sumY));
		specEntry.fit.Power[iF] = 2.0;
		specEntry.fit.Slope[iF] = m;

		if (!Double.isNaN(specEntry.x0Init))
			specEntry.fit.X0[iF] = specEntry.x0Init;
		if (!Double.isNaN(specEntry.y0Init))
			specEntry.fit.Y0[iF] = specEntry.y0Init;
		if (specEntry.fit.frameAmplitudeInit != null)
			specEntry.fit.Amplitude[iF] = specEntry.fit.frameAmplitudeInit[iF];
		else if (!Double.isNaN(specEntry.amplitudeInit))
			specEntry.fit.Amplitude[iF] = specEntry.amplitudeInit;		
		if (specEntry.fit.frameSigmaInit != null)
			specEntry.fit.Sigma[iF] = specEntry.fit.frameSigmaInit[iF];
		else if (!Double.isNaN(specEntry.sigmaInit))
			specEntry.fit.Sigma[iF] = specEntry.sigmaInit;
		if (!Double.isNaN(specEntry.slopeInit))
			specEntry.fit.Slope[iF] = specEntry.slopeInit;

		// if moment didn't work (or forceBackupInitGuess but not specifically set)
		// try looking for the 2/3 falloff from the peak
		// FWHM should be 1/2, but peak will generally be higher by noise,
		// making it the peak
		if (Double.isNaN(specEntry.fit.Sigma[iF]) ||
				(config.forceBackupInitGuess && Double.isNaN(specEntry.sigmaInit))) {
			for (int j = 0; j < nP; j++) {
				int iP0 = maxPos - j;
				if (iP0 < 0)
					break;
				double baseline0 = y0 + (frameD.x[iP0] - x0) * m; // line
				double yv0 = (frameD.data[iP0] - (baseline0));

				int iP1 = maxPos + j;
				if (iP1 >= nP)
					break;
				double baseline1 = y0 + (frameD.x[iP1] - x0) * m; // line
				double yv1 = (frameD.data[iP1] - (baseline1));

				double avgVal = (yv0 + yv1) / 2;
				if (avgVal < (0.3 * max)) {
					specEntry.fit.Sigma[iF] = FastMath.abs(frameD.x[iP1] - frameD.x[iP0]) / 2.35;
					break;
				}
			}
		}
		
		if (Double.isNaN(specEntry.fit.Sigma[iF])) { // fall back to peak and
													// fixed width
			Logger.getLogger(this.getClass().getName()).warning("Entry '" + specEntry.id + "' has bad width");
			specEntry.fit.X0[iF] = frameD.x[maxPos];
			specEntry.fit.Y0[iF] = y0 + (specEntry.fit.X0[iF] - x0) * m;
			specEntry.fit.Sigma[iF] = (frameD.x[nP - 1] - frameD.x[0]) / 10;
			specEntry.fit.Amplitude[iF] = max * specEntry.amplitudeInitFraction 
										* (specEntry.fit.Sigma[iF] * FastMath.sqrt(2 * Math.PI));
		}
	}
	
	private String frameDataSyncObj = new String("frameDataSyncObj");

	protected String lastLoadedConfig;	
	
	/** @return data of the current selected source frame only */
	public FrameData getCurrentFrameData(String id) {
		int iF = getSelectedSourceIndex();
		
		
		
		FastSpecEntry specEntry = config.getPointByName(id);

		int nF = connectedSource.getNumImages();
		if (specEntry.fit.Amplitude == null) {
			specEntry.fit.Amplitude = new double[nF];
			specEntry.fit.UncertaintyAmpltiude = new double[nF];
			specEntry.fit.X0 = new double[nF];
			specEntry.fit.UncertaintyX0 = new double[nF];
			specEntry.fit.Slope = new double[nF];
			//specEntry.fit.UncertaintySlope = new double[nF];
			specEntry.fit.Y0 = new double[nF];
			specEntry.fit.UncertaintyY0 = new double[nF];
			specEntry.fit.Sigma = new double[nF];
			specEntry.fit.UncertaintySigma = new double[nF];
			specEntry.fit.Power = new double[nF];
			specEntry.fit.UncertaintyPower = new double[nF];
			specEntry.fit.RMSError = new double[nF];
			specEntry.fit.inferredQuantities = null;
		}
		
		if(specEntry.fit.inferredQuantities == null){
			specEntry.fit.inferredQuantities = new HashMap<String, double[]>();			
			//config.inferredQuantities.clear();
		}
		

		abortCalc = false;
		
		ArrayList<FastSpecEntry> entriesOrig;
		
		if(config.multiGauss){
			entriesOrig = getOverlappingEntries(specEntry, iF);
		}else{
			entriesOrig = new ArrayList<FastSpecEntry>();
			entriesOrig.add(specEntry);
		}
		
		//copy the entries in order to do the init without killing the current params
		ArrayList<FastSpecEntry> entriesCopy = new ArrayList<FastSpecEntry>(entriesOrig.size());
		for(FastSpecEntry entry : entriesOrig){
			FastSpecEntry newEntry = new FastSpecEntry(entry.id);			
			newEntry.fit.Amplitude = new double[nF];
			newEntry.fit.UncertaintyAmpltiude = new double[nF];
			newEntry.fit.X0 = new double[nF];
			newEntry.fit.UncertaintyX0 = new double[nF];
			newEntry.fit.Slope = new double[nF];
			//newEntry.fitUncertaintySlope = new double[nF];
			newEntry.fit.Y0 = new double[nF];
			newEntry.fit.UncertaintyY0 = new double[nF];
			newEntry.fit.Sigma = new double[nF];
			newEntry.fit.UncertaintySigma = new double[nF];
			newEntry.fit.Power = new double[nF];
			newEntry.fit.UncertaintyPower = new double[nF];
			newEntry.fit.RMSError = new double[nF];
			newEntry.fit.inferredQuantities = new HashMap<String, double[]>();	
			
			//minimal copy for the init to work
			newEntry.x0 = entry.x0;
			newEntry.x1 = entry.x1;
			newEntry.y0 = entry.y0;
			newEntry.y1 = entry.y1;
			newEntry.amplitudeInit = entry.amplitudeInit;
			newEntry.amplitudeMin = entry.amplitudeMin;
			newEntry.amplitudeMax = entry.amplitudeMax;
			newEntry.x0Init = entry.x0Init;
			newEntry.x0Min = entry.x0Min;
			newEntry.x0Max = entry.x0Max;
			newEntry.y0Init = entry.y0Init;
			newEntry.y0Min = entry.y0Min;
			newEntry.y0Max = entry.y0Max;
			newEntry.sigmaInit = entry.sigmaInit;
			newEntry.sigmaMin = entry.sigmaMin;
			newEntry.sigmaMax = entry.sigmaMax;
			newEntry.powerMin = entry.powerMin;
			newEntry.powerMax = entry.powerMax;
			newEntry.slopeInit = entry.slopeInit;
			newEntry.slopeMin = entry.slopeMin;
			newEntry.slopeMax = entry.slopeMax;
			newEntry.nominalWavelength = entry.nominalWavelength;
			newEntry.ignoreX0Ranges = entry.ignoreX0Ranges == null ? null : entry.ignoreX0Ranges.clone();
			newEntry.enable = entry.enable;
			
			newEntry.fit.frameAmplitudeInit = entry.fit.frameAmplitudeInit;
			newEntry.fit.frameSigmaInit = entry.fit.frameSigmaInit;
			newEntry.fit.framePowerInit = entry.fit.framePowerInit;
			
			//config.inferredQuantities.clear();
			entriesCopy.add(newEntry);
		}		
		
		//fill the frameData structure using the copied 
		FrameData frameD = fillData(entriesCopy, iF);
		double baselineInfo[] = findBaseline(frameD); //find baseline and estimate point error from it
		applyIgnoreRanges(entriesCopy, frameD); //add ignored ranges			
		initFrameFit(entriesCopy, frameD, iF, baselineInfo); //clobbers entries fitXXX with init values
		fillBaselineAndInit(entriesCopy, frameD, iF); //generate baseline and init from entryCopies
		
		//now fill the 'current' fit from the original entries (which doesn't modify them)
		CostF costF = new CostF(frameD.x, frameD.data, frameD.error, entriesOrig.size());
		costF.baselineX0 = baselineInfo[0];
		
		double p[] = buildParamsArray(entriesOrig, costF, iF, frameD, false);
		fillFrameFitFromParams(costF, frameD, p);
		
		return frameD;
	}

	public FastSpecEntry findEntry(String id) {
		for (FastSpecEntry specEntry : config.specEntries)
			if (id.equals(specEntry.id))
				return specEntry;
		//return null;
		throw new RuntimeException("Unable to find matched entry '"+id+"'");
	}

	@Override
	public boolean wasLastCalcComplete() {
		return lastCalcComplete;
	}

	@Override
	public void event(Event event) {
		if(event == Event.GlobalAbort)
			abortCalc();
	}
	
	@Override
	public void abortCalc() {
		abortCalc = true;
	}

	@Override
	public FastSpecProcessor clone() {
		return new FastSpecProcessor(connectedSource, getSelectedSourceIndex());
	}

	@Override
	public boolean isIdle() {
		return !ImageProcUtil.isUpdateInProgress(this);
	}

	public void mapModified() {
		updateAllControllers();
		if (autoCalc)
			calc();
	}

	public FastSpecConfig getConfig() {
		return config;
	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if (interfacingClass == Composite.class) {
			FastSpecSWTController controller = new FastSpecSWTController((Composite) args[0], (Integer) args[1], this);
			controllers.add(controller);
			return controller;
		}
		return null;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
		updateAllControllers();
	}
	
	/** Override to convert from json to specific derived types of FastSpecConfig */
	protected FastSpecConfig configFromJson(Gson gson, String jsonString){
		return gson.fromJson(jsonString, FastSpecConfig.class);
	}

	@Override
	public void loadConfig(String id) {
		if (id.startsWith("jsonfile:")) {
			loadConfigJSON(id.substring(9));
		}else {
			throw new RuntimeException("Unknown config type " + id);
		}
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);

		Gson gson = new Gson();

		config = configFromJson(gson, jsonString);

		// old config files that didn't have the limits load them as 0, so
		// if it's clear all 0, set them to NaN
		for (FastSpecEntry specEntry : config.specEntries) {
			if (specEntry.x0Min == 0 && specEntry.x0Max == 0) {
				specEntry.x0Min = specEntry.x0;
				specEntry.x0Max = specEntry.x1;
				specEntry.x0Init = Double.NaN;
				specEntry.x0PriorMean = Double.NaN;
				specEntry.x0PriorSigma = Double.NaN;
			}
			if (specEntry.y0Min == 0 && specEntry.y0Max == 0) {
				specEntry.y0Min = Double.NEGATIVE_INFINITY;
				specEntry.y0Max = Double.POSITIVE_INFINITY;
				specEntry.y0Init = Double.NaN;
				specEntry.y0PriorMean = Double.NaN;
				specEntry.y0PriorSigma = Double.NaN;
			}
			if (specEntry.amplitudeMin == 0 && specEntry.amplitudeMax == 0) {
				specEntry.amplitudeMin = 0;
				specEntry.amplitudeMax = Double.POSITIVE_INFINITY;
				specEntry.amplitudeInit = Double.NaN;
				specEntry.amplitudePriorMean = Double.NaN;
				specEntry.amplitudePriorSigma = Double.NaN;
			}
			if (specEntry.sigmaMin == 0 && specEntry.sigmaMax == 0) {
				specEntry.sigmaMin = 0;
				specEntry.sigmaMax = Double.POSITIVE_INFINITY;
				specEntry.sigmaInit = Double.NaN;
				specEntry.sigmaPriorMean = Double.NaN;
				specEntry.sigmaPriorSigma = Double.NaN;
			}
			if (specEntry.powerMin == 0 && specEntry.powerMax == 0) {
				specEntry.powerMin = 2.0;
				specEntry.powerMax = 2.0;
				specEntry.powerInit = 2.0;
			}
			if (specEntry.powerPriorMean == 0 && specEntry.powerPriorSigma == 0) {
				specEntry.powerPriorMean = Double.NaN;
				specEntry.powerPriorSigma = Double.NaN;
			}
			if (specEntry.slopeMin == 0 && specEntry.slopeMax == 0) {
				specEntry.slopeMin = Double.NEGATIVE_INFINITY;
				specEntry.slopeMax = Double.POSITIVE_INFINITY;
				specEntry.slopeInit = Double.NaN;
				specEntry.slopePriorMean = Double.NaN;
				specEntry.slopePriorSigma = Double.NaN;
			}
			if (specEntry.amplitudeInitFraction == 0){
				specEntry.amplitudeInitFraction = 1.0;
			}
		}

		if (config.numIterations <= 0) // probably old version without this
										// field
			config.numIterations = 20;

		updateAllControllers();
	}

	@Override
	public void saveConfig(String id) {
		if (id.startsWith("jsonfile:")) {
			String fileName = id.substring(9);

			Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls()
					.setPrettyPrinting()
					.addSerializationExclusionStrategy(new ExclusionStrategy() {
						@Override
						public boolean shouldSkipField(FieldAttributes attrbs) {
							return attrbs.getName().startsWith("fit")
								&& !attrbs.getName().startsWith("fitWidth");
						}

						@Override
						public boolean shouldSkipClass(Class<?> arg0) {
							return false;
						}
					}).create();

			String json = gson.toJson(config);

			// make vaguely readable
			json = json.replaceAll("},", "},\n");

			OneLiners.textToFile(fileName, json);
			lastLoadedConfig = id;
		}
	}

	/** Trigger calc without any actual fitting, so that results are saved */
	public void saveCurrent() {
		forceSaveOnly = true;
		calc();
	}

	protected void doSaveResults() {
		// placeholder		
		setStatus("Done (but no saving available)");
	}

	/** 
	 * @param specEntryOnly specEntryOnly != null: Update only this entry
	 * @param iF
	 *    iF = -2: preparation
	 *    iF = -1: post
	 *    iF >= 0: Update only this frame
	 */
	public enum CalcStage { Prepare, PostProcess, SingleFrame };	
	protected void inferredQuantities(CalcStage stage, FastSpecEntry specEntryOnly, int iF) {
		//calculate dispersion
		if(stage != CalcStage.SingleFrame && config.coordType == FastSpecConfig.COORDTYPE_WAVELENGTH_AXIS){
			for (FastSpecEntry specEntry : config.specEntries) {
				if(specEntryOnly != null && specEntryOnly != specEntry)
					continue;
				
				double xCentre = (specEntry.x0 + specEntry.x1) / 2;
				double yCentre = (specEntry.y0 + specEntry.y1) / 2;
				double der[] = new double[1];
				if(config.wavelenIsY){
					double px0[] = wavelengthToPixel[(int)xCentre].eval(new double[]{ yCentre }, der);
				}else{
					if((int)yCentre >= wavelengthToPixel.length)
						throw new RuntimeException("Entry" + specEntry.id + " has centre y="+yCentre+", but there are only " + wavelengthToPixel.length + " rows in the wavelength calibration array" );
					double px0[] = wavelengthToPixel[(int)yCentre].eval(new double[]{ xCentre }, der);
					
				}
				specEntry.dispersion = 1.0 / der[0]; // wavelength per px
			}
		}else{
			for (FastSpecEntry specEntry : config.specEntries)
				specEntry.dispersion = Double.NaN;
				
		}

		if(stage != CalcStage.SingleFrame){ //pre and post
			// look for line of sight defs
			String losNames[] = (String[]) connectedSource.getSeriesMetaData("SpecCal/linesOfSight/names");
			String lightPaths[] = (String[]) connectedSource.getSeriesMetaData("SpecCal/linesOfSight/lightPaths");
			for (FastSpecEntry specEntry : config.specEntries) {
				int row = (int) specEntry.y0;
				specEntry.lightPath = (lightPaths == null) ? null : lightPaths[row];
				specEntry.losName = (losNames == null) ? null : losNames[row];
			}
		}

	}

	public void autoBound(String entryName) {
		FastSpecEntry entry = config.getPointByName(entryName);
	
		if(entry.id.contains("_PCX")){
			//special case for active/passive charge exchange fitting
			//find frames with passive only and range to them
			
			boolean[] goodFrames = (boolean[])connectedSource.getSeriesMetaData("isGoodData");
			if(config.processOnlyGoodFrames || config.saveOnlyGoodFrames){					
				if(goodFrames == null)
					throw new RuntimeException("Config set to fit/save only good frame but no isGoodData metadata found.");
			}
			
			double ampSum = 0, amp2Sum = 0;
			double sigmaSum = 0, sigma2Sum = 0;
			double x0Sum = 0, x02Sum = 0;
			int n=0;
			for(int iF=0; iF < connectedSource.getNumImages(); iF++){			
				if(!goodFrames[iF]){
					ampSum += entry.fit.Amplitude[iF];
					sigmaSum += entry.fit.Sigma[iF];
					x0Sum += entry.fit.X0[iF];
					
					amp2Sum += entry.fit.Amplitude[iF]*entry.fit.Amplitude[iF];
					sigma2Sum += entry.fit.Sigma[iF]*entry.fit.Sigma[iF];
					x02Sum += entry.fit.X0[iF]*entry.fit.X0[iF];
					
					n++;
				}
			}
			double m, s;
			
			m = ampSum/n;
		    s = FastMath.sqrt((amp2Sum - ampSum*ampSum/n)/n);
		    entry.amplitudeMin = FastMath.max(m - 5 * s, 0);
			entry.amplitudeMax = m + 5 * s;
			entry.amplitudeInit = m;
			entry.amplitudePriorMean = m;
			entry.amplitudePriorSigma = 2 * s;
					        
			m = sigmaSum/n;
			s = FastMath.sqrt((sigma2Sum - sigmaSum*sigmaSum/n)/n);
			//s = FastMath.max(s, 0.005);
			entry.sigmaMin = FastMath.min(FastMath.max(m - 10 * s, 0.01), 0.05);
			entry.sigmaMax = m + 5 * s;
			entry.sigmaInit = m;
			entry.sigmaPriorMean = m;
			entry.sigmaPriorSigma = 2 * s;
					        
			m = x0Sum/n;
			s = FastMath.sqrt((x02Sum - x0Sum*x0Sum/n)/n);
			s = FastMath.max(s, 0.001);
			entry.x0Min = m - 10 * s;
			entry.x0Max = m + 10 * s;
			entry.x0Init = m;
			entry.x0PriorMean = m;
			entry.x0PriorSigma = 3 * s;
		        
	        
		}else{
			
			double mm[];
			mm = autoBound(entry.fit.Sigma); entry.sigmaMin=mm[0]; entry.sigmaMax=mm[1]; entry.sigmaInit=(mm[0]+mm[1])/2;
			mm = autoBound(entry.fit.X0); entry.x0Min=mm[0]; entry.x0Max=mm[1]; entry.x0Init=(mm[0]+mm[1])/2;
			mm = autoBound(entry.fit.Amplitude); entry.amplitudeMin=Math.max(0, mm[0]); entry.amplitudeMax=mm[1]; entry.amplitudeInit=(mm[0]+mm[1])/2;
		}			
		
		//special 
		
			
		
		updateAllControllers();		
	}

	private double[] autoBound(double x[]) {
		
		//ignoring fit uncertainties for now
		
		int n=0;
		double m = 0;
        double m2 = 0;
        for(int i=0;i<x.length;++i) {
        	if(Double.isFinite(x[i])){
        		m += x[i];
        		m2 += x[i]*x[i];
        		n++;
        	}
        }

        double e = (m2-m*m/n);
        double mean = m/n;
        double std = FastMath.sqrt(e/n);
        
		return new double[]{ mean - 4 * std, mean + 4*std };
		
	}
	
	public boolean isAborting(){ return abortCalc;	}
	
	public int getPixelForWavelength(int row, double fitWavelength) {
		return (int)wavelengthToPixel[row].eval(fitWavelength);
	}
	
	public double getDispersionAtPixel(int y, int x) {
		return wavelengthMetadata[y][x+1] - wavelengthMetadata[y][x];
	}

	public void setNThreads(int nThreads){ this.nThreads = nThreads; }
	public int getNThreads(){ return this.nThreads; }

}
