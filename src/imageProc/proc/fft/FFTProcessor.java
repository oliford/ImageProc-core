package imageProc.proc.fft;

import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import descriptors.gmds.GMDSSignalDesc;
import edu.emory.mathcs.jtransforms.fft.DoubleFFT_2D;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ComplexImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.database.gmds.GMDSUtil;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.gmds.GMDSSignal;

/** FFT Processor
 * 
 * Seems to be thread safe, but not sure.
 *
 */
public class FFTProcessor extends ImgProcPipeMultithread implements ConfigurableByID  {
	
	private FFTSWTController controller;

	private BulkImageAllocation<ComplexImage> bulkAlloc = new BulkImageAllocation<ComplexImage>(this);
	
	double gaussWinX0, gaussWinY0, gaussWinSigmaX, gaussWinSigmaY;
	
	/** Crop output images to +/- this. 0 means don't crop */
	private int cropX = 0, cropY = 0;

	private String lastLoadedConfig;
	
	public FFTProcessor() {
		super(ComplexImage.class);
	}
	
	public FFTProcessor(ImgSource source, int selectedIndex) {
		super(ComplexImage.class, source, selectedIndex);
	}
	
	protected boolean checkOutputSet(int nImagesIn){
		
		 outWidth = (cropX > 0) ? Math.min(2*cropX, inWidth) : inWidth;
         outHeight = (cropY > 0) ? Math.min(2*cropY, inHeight) : inHeight;
         int nImagesOut = nImagesIn;
         
         setSeriesMetaData("FFT/cropX", cropX, false);
         setSeriesMetaData("FFT/cropY", cropY, false);
         setSeriesMetaData("FFT/fftWidth", outWidth, false); //store crop info for later use in AOI calculations
         setSeriesMetaData("FFT/fftHeight", outHeight, false);
         
         //allocate or re-use
         ComplexImage templateImage = new ComplexImage(null, -1, outWidth, outHeight, false);
         if(bulkAlloc.reallocate(templateImage, nImagesOut)){
        	 imagesOut = bulkAlloc.getImages();
        	 return true;
         }          
         return false;
	}
	
	protected int[] sourceIndices(int outIdx){
		return new int[]{ outIdx };
	}

	@Override
	public boolean doCalc(Img imageOutGen, WriteLock writeLock, Img sourceSet[], boolean settingsHadChanged) throws InterruptedException {
		DoubleFFT_2D fft = new DoubleFFT_2D(inHeight, inWidth);
		
		Img imageIn = sourceSet[0];
		ComplexImage imageOut = (ComplexImage)imageOutGen;

		int n = imageIn.getHeight() * imageIn.getWidth();
		
		double data[] = new double[2*n];

		boolean gaussValid = false;
		double gaussWin = 1;
		if(gaussWinX0 > 0 && gaussWinY0 > 0 && gaussWinSigmaX > 0 && gaussWinSigmaY > 0){
			gaussValid = true;
		}
	
				
		try{
			ReadLock readLockIn = imageIn.readLock();
			readLockIn.lockInterruptibly();
			try{
	
				for(int y=0; y < imageIn.getHeight(); y++){
					for(int x=0; x < imageIn.getWidth(); x++){
						if(gaussValid){
							double argX = - FastMath.pow2(((double)x - gaussWinX0) / gaussWinSigmaX) / 2; 
							double argY = - FastMath.pow2(((double)y - gaussWinY0) / gaussWinSigmaY) / 2; 
							gaussWin = FastMath.exp(argX + argY);
						}
						data[y*inWidth+x] = imageIn.getPixelValue(readLockIn, x, y) * gaussWin;
						if(Double.isNaN(data[y*inWidth+x]))
							data[y*inWidth+x] = 0;
					}
				}
			}finally{ readLockIn.unlock(); }
		}catch(InterruptedException err){ } 

		fft.realForwardFull(data);

		//otherwise change the image
		imageOut.fillFromJTransformsPackedData(writeLock, data, true, inWidth, inHeight);
		
		return true;
	}

	@Override
	public FFTProcessor clone() { return new FFTProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			controller = new FFTSWTController((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	@Override
	public void loadConfig(String id){
		
		if(id.startsWith("gmds")){
			String parts[] = id.split("/");
			loadConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
		}else if(id.startsWith("jsonfile:")){
			loadConfigJSON(id.substring(9));
		}else {
			throw new RuntimeException("Unknown config type " + id);
		}
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
		
	
	public void loadConfigJSON(String fileName){
		String jsonString = OneLiners.fileToText(fileName);
		
		JsonObject jobj = (new JsonParser()).parse(jsonString).getAsJsonObject();
		cropX = ((JsonPrimitive) jobj.get("cropX")).getAsInt();
		cropY = ((JsonPrimitive) jobj.get("cropY")).getAsInt();
		autoCalc = ((JsonPrimitive) jobj.get("autoCalc")).getAsBoolean();
		
		updateAllControllers();
	}
	
	public void loadConfigGMDS(String experiment, int pulse){
		if(pulse == -1 && connectedSource != null){
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		}
		
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "FFT/cropX");		
		GMDSSignal sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(sigDesc);
		cropX = ((int[])sig.getData())[0];
		
		sigDesc = new GMDSSignalDesc(pulse, experiment, "FFT/cropY");		
		sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(sigDesc);
		cropY = ((int[])sig.getData())[0];
				
		updateAllControllers();
		if(autoCalc)
			calc();
	}
	
	@Override
	public void saveConfig(String id){
		if(id.startsWith("gmds")){
			String parts[] = id.split("/");
			saveConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
			lastLoadedConfig = id;
		}else if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		
		JsonObject jobj = new JsonObject();
		jobj.add("cropX", new JsonPrimitive(cropX));
		jobj.add("cropY", new JsonPrimitive(cropY));
		jobj.add("autoCalc", new JsonPrimitive(autoCalc));
			
		OneLiners.textToFile(fileName, jobj.toString());
	}
	
	public void saveConfigGMDS(String experiment, int pulse){
		if(connectedSource == null) return;
		
		if(experiment == null)
			experiment = GMDSUtil.getMetaExp(connectedSource);
		if(pulse < 0)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "FFT/cropX");		
		GMDSSignal sig = new GMDSSignal(sigDesc, new int[]{ cropX });
		GMDSUtil.globalGMDS().writeToCache(sig);

		sigDesc = new GMDSSignalDesc(pulse, experiment, "FFT/cropY");		
		sig = new GMDSSignal(sigDesc, new int[]{ cropY });
		GMDSUtil.globalGMDS().writeToCache(sig);
	}
	

	public void setGaussWindow(double x0, double y0, double fwhmX, double fwhmY) {
		this.gaussWinX0 = x0;
		this.gaussWinY0 = y0;
		this.gaussWinSigmaX = fwhmX / 2.35;
		this.gaussWinSigmaY = fwhmY / 2.35;
		settingsChanged = true;
		updateAllControllers();
		if(autoCalc)
			calc();
	}
	
	public double getGaussWindowX0(){ return gaussWinX0; }
	public double getGaussWindowY0(){ return gaussWinY0; }
	public double getGaussWindowFWHMX(){ return gaussWinSigmaX * 2.35; }
	public double getGaussWindowFWHMY(){ return gaussWinSigmaY * 2.35; }


	
	public void setCrop(int cropX, int cropY) {
		this.cropX = cropX;
		this.cropY = cropY;
		this.settingsChanged = true;
		updateAllControllers();
		if(autoCalc)
			calc();
	}
	
	public int getCropX(){ return this.cropX; }
	public int getCropY(){ return this.cropY; }

	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "FFT[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}
}
