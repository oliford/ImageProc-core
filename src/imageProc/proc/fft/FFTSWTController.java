package imageProc.proc.fft;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.gmds.GMDSSettingsControl;

public class FFTSWTController extends ImgProcPipeSWTController implements SWTControllerInfoDraw, ImagePipeControllerROISettable {
	
	private FFTProcessor proc;
	
	private Spinner cropXSpinner;
	private Spinner cropYSpinner;
	private Button setWindowing;

	private SWTSettingsControl settingsCtrl;
	
	
	public FFTSWTController(Composite parent, int style, FFTProcessor proc) {
		this.proc = proc;
		swtGroup = new Group(parent, style);
		swtGroup.setText("FFT Control (" + proc.toShortString() + ")");		
		swtGroup.setLayout(new GridLayout(5, false));
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		addCommonPipeControls(swtGroup);
		
		Label lCX = new Label(swtGroup, SWT.NONE); lCX.setText("Crop X:");		
		cropXSpinner = new Spinner(swtGroup, SWT.NONE);
		cropXSpinner.setValues(128, 0, Integer.MAX_VALUE, 0, 16, 128);
		cropXSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		cropXSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { cropSpinnerEvent(event); } });

		Label lCY = new Label(swtGroup, SWT.NONE); lCY.setText("Y:");		
		cropYSpinner = new Spinner(swtGroup, SWT.NONE);
		cropYSpinner.setValues(128, 0, Integer.MAX_VALUE, 0, 16, 128);
		cropYSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		cropYSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { cropSpinnerEvent(event); } });

		setWindowing = new Button(swtGroup, SWT.CHECK);
		setWindowing.setText("Set windowing");
		//setWindowing.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { ?? } });
		setWindowing.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		settingsCtrl = ImageProcUtil.createPreferredSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		swtGroup.pack();
		doUpdate();
	}
	
	private void cropSpinnerEvent(Event e){
		proc.setCrop(cropXSpinner.getSelection(), cropYSpinner.getSelection());
	}
		
	protected void doUpdate(){
		super.doUpdate();
		
		settingsCtrl.doUpdate();
		
		cropXSpinner.setSelection(proc.getCropX());	
		cropYSpinner.setSelection(proc.getCropY());	
	}

	@Override
	public void movingPos(int x, int y) {	}

	@Override
	public void fixedPos(int x, int y) {	}

	@Override
	public void setRect(int x0, int y0, int width, int height) {
		if(setWindowing.getSelection())
			proc.setGaussWindow(x0, y0, width, height);
	}

	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth, int imageHeight, boolean asSource) {
		double gx0 = proc.getGaussWindowX0();
		double gy0 = proc.getGaussWindowY0();
		double gwx = proc.getGaussWindowFWHMX();
		double gwy = proc.getGaussWindowFWHMY();
		
		gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
		gc.drawOval(
				(int)((gx0 - gwx/2) * scale[0]), 
				(int)((gy0 - gwy/2) * scale[1]), 
				(int)(gwx * scale[0]), 
				(int)(gwy * scale[1]));
		
		gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_DARK_RED));
		gc.drawOval(
				(int)((gx0 - gwx) * scale[0]), 
				(int)((gy0 - gwy) * scale[1]), 
				(int)(gwx*2 * scale[0]), 
				(int)(gwy*2 * scale[1]));
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
		//proc.destroy(); //and actively kill it, for good measure
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public FFTProcessor getPipe() { return proc;	}
}
