package imageProc.proc.example;

import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSource;
import imageProc.proc.example.swt.ExampleProcSWTController;
import oneLiners.OneLiners;

/** Example processor pipe for ImageProc.
 * 
 * Produces a set of images that are another set of images with some basic arithmetic.
 *  Further examples:
 *   SoftwareROIsProcessor - Output images of different size
 *   SeriesProcessor - Making output images from multiple input images 
 * 
 * 
 * @author oliford
 *
 */
public class ExampleProcessor extends ImgProcPipeMultithread implements ConfigurableByID {

	/** Bulk allocation of image memory */ 
	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
	
	/** The pipe configuration, save/loaded from e.g. JSON files */
	protected ExampleProcConfig config = new ExampleProcConfig();
	
	/** The ID of the last configuration loaded */ 
	private String lastLoadedConfig;
	
	public ExampleProcessor() {
		this(null, -1);
	}
	
	public ExampleProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);
		autoCalc = false;
		setNumThreads(1);
	}
	
	/** Determine the number and size etc of output image set.
	 * Allocate the memory. */
	@Override
	protected boolean checkOutputSet(int nImagesIn){
		int nImagesOut = nImagesIn;
		if(config == null)
			throw new RuntimeException("config is null. Still in construction??");
		
		//Determine image size from first input image
		Img img = connectedSource.getImage(0);
		if(img == null)
			throw new RuntimeException("First image of input set is null, can't determine size.");
		
		//find  how many active rows and the most pixels in a single row
		outWidth = img.getWidth();
		outHeight = img.getHeight();
	    
		HashMap<String,Object> configMetadata = new HashMap<String, Object>(); 
	    config.addArraysToMap(configMetadata, "ExampleProcessor/config", true);
	    addNonTimeSeriesMetaDataMap(configMetadata);
	    
	    int nFields = getConnectedSource().getImage(0).getNFields();
	    
	    //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, nFields, ByteBufferImage.DEPTH_FLOAT, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }          
        return false;        
	}
	
	@Override
	/** Which source images are needed to produce the given output image */
	protected int[] sourceIndices(int outIdx){	
		// here it's 1:1
		return new int[]{ outIdx };	
	}
	
	@Override
	/** Calculate a specific output image. The write lock has already been obtained
	 * Read locks should be obtained on the input images as they are needed.
	 */
	protected boolean doCalc(Img imageOutG, WriteLock writeLockOut, Img sourceSet[], boolean settingsHadChanged) throws InterruptedException {
				
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		
		try{			
			//we only have a single input image
			Img imageIn = sourceSet[0];
			if(imageIn == null)
				return true;
			
			int imageWidth = imageIn.getWidth();
			int imageHeight = imageIn.getHeight();
			
			int nextOutputStartRow = 0;
						
			ReadLock readLockIn = imageIn.readLock();
			readLockIn.lockInterruptibly();
			try{
				
				for(int iOY=0; iOY < outHeight; iOY++){
					for(int iOX=0; iOX < outWidth; iOX++){
						double val = imageIn.getPixelValue(readLockIn, iOX, iOY);
						
						val = val * config.multiplyValue + config.addValue;
						
						imageOut.setPixelValue(writeLockOut, iOX, iOY, val);
						
					}
				}
				
			}finally{ readLockIn.unlock(); }
			
		}catch(InterruptedException err){ }
			
		return true;
	}
	
	
	@Override
	public ExampleProcessor clone() { return new ExampleProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			ExampleProcSWTController controller = new ExampleProcSWTController((Composite)args[0], (Integer)args[1], this, true);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public ExampleProcConfig getConfig(){ return config; }
	
	public void configModified(){
		settingsChanged = true;
		updateAllControllers();
		if(autoCalc)
			calc();
	}

	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, ExampleProcConfig.class);
		if(config == null) {
			logr.log(Level.SEVERE, "Invalid or empty configuration");
			status = "ERROR: Invalid or empty configuration";
			config = new ExampleProcConfig();
		}
		
		updateAllControllers();	
	}

	
	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "Example[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}

	public void configChanged() {
		updateAllControllers();
	}
	
	@Override
	public void updateAllControllers() {
		// TODO Auto-generated method stub
		super.updateAllControllers();
	}
}
