package imageProc.proc.example;

import java.util.HashMap;

/** Configuration of ExampleProcessor */
public class ExampleProcConfig {
	
	/** Thing to add */
	public double addValue;
	
	/** Thing to multiply */
	public double multiplyValue;
	
	/** Adds settings to Map. Used for adding the configuration to metadata */
	public void addArraysToMap(HashMap<String, Object> map, String prefix, boolean enabledOnly) {
			
		map.put(prefix + "/addValue", addValue);
		map.put(prefix + "/multiplyValue", multiplyValue);			
		
	}

}
