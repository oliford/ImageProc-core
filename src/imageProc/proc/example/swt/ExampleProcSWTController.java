package imageProc.proc.example.swt;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import algorithmrepository.Algorithms;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.EditableTable;
import imageProc.core.swt.EditableTable.EditType;
import imageProc.core.swt.EditableTable.TableModifyListener;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.proc.example.ExampleProcConfig;
import imageProc.proc.example.ExampleProcessor;
import imageProc.proc.softwareBinning.SoftwareROIsConfig.SoftwareROI;

/** The table directly relfects the config data in the processor's map
* Edits are made into that cfg immediately.
*  
* @author oliford
*/
public class ExampleProcSWTController extends ImgProcPipeSWTController {

	
	private Spinner addValueSpinner;	
	private Spinner multiplyValueSpinner;	
	
	
	private SWTSettingsControl settingsCtrl;
	
	protected ExampleProcessor proc;
	
	public boolean asSink;
				
	public ExampleProcSWTController(Composite parent, int style, ExampleProcessor proc, boolean asSink) {
		this.proc = proc;
		this.asSink = asSink;
				
		swtGroup = new Group(parent, style);
		swtGroup.setLayout(new GridLayout(6, false));

		addCommonPipeControls(swtGroup);

		Label lAX = new Label(swtGroup, SWT.NONE); lAX.setText("Add value:");
		addValueSpinner = new Spinner(swtGroup, SWT.NONE);
		addValueSpinner.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 100, 1000);
		addValueSpinner.setToolTipText("Value to add to every pixel");
		addValueSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		addValueSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lAY = new Label(swtGroup, SWT.NONE); lAY.setText("Multiply value:");
		multiplyValueSpinner = new Spinner(swtGroup, SWT.NONE);
		multiplyValueSpinner.setValues(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 3, 100, 1000);
		multiplyValueSpinner.setToolTipText("Value to multiply every pixel by");
		multiplyValueSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		multiplyValueSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		swtGroup.pack();
	}

	protected void settingsChangedEvent(Event event) {
		ExampleProcConfig config = proc.getConfig();
		
		config.addValue = addValueSpinner.getSelection() / 1000.0;
		config.multiplyValue = multiplyValueSpinner.getSelection() / 1000.0;

		proc.configChanged();
	}
	
	void configToGUI(){
		ExampleProcConfig config = proc.getConfig();

		addValueSpinner.setSelection((int)(config.addValue * 1000.0));
		multiplyValueSpinner.setSelection((int)(config.multiplyValue * 1000.0));

	}
		

	public Control getSWTGroup() { return swtGroup; }

	
	protected void doUpdate(){
		super.doUpdate();
		
		settingsCtrl.doUpdate();
		
		configToGUI();
		
	}
	
	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public ExampleProcessor getPipe() { return proc;	}
	
}
