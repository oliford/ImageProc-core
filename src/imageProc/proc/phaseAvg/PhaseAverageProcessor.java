package imageProc.proc.phaseAvg;

import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgSource;

public class PhaseAverageProcessor extends ImgProcPipe {
	
	/** Thinking about writing the phase averaging (e.g. for sawteeth) into IMSEProc.
	 * Hmmm, still a lot of effort
	 */
	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
		
	/*** config */
	private double phaseMin;
	private double phaseMax;
	private int nPhaseRegions;
	
	private double boundaryPhases[]; // [nPhaseRegions+1]
	
	private String phaseSignalPath;
	
	private double phaseTime[];
	private double phaseSignal[];
		
	public PhaseAverageProcessor() {
		this(null, -1);
	}
	
	public PhaseAverageProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);		
		
	}

	@Override
	protected boolean doCalc(Img imageOut, WriteLock writeLock,
			Img[] sourceSet, boolean settingsHadChanged)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected int[] sourceIndices(int outIdx) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean checkOutputSet(int nImagesIn) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImgProcPipe clone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }
}
