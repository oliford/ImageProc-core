package imageProc.proc.transform;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.gmds.GMDSSettingsControl;
import fusionDefs.transform.FeatureTransform;
import fusionDefs.transform.FeatureTransformCubic;
import fusionDefs.transform.PhysicalROIGeometry;
import fusionDefs.transform.TransformPoint;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** The table directly relfects the config data in the processor's map
 * Edits are made into that cfg immediately.
 * 
 * @author oliford
 */
public class TransformSWTController extends ImgProcPipeSWTController implements SWTControllerInfoDraw, ImagePipeControllerROISettable {

	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.###");
	
	public static final String colNames[] = new String[] {
			"-", "ID", "ImgX", "ImgY",  "AUG X", "AUG Y", "AUG Z", "θ", "φ", "Enable/Mode", "Beam R", "Beam Z", "Idiot X", "Idiot Y",  };
	
	public static final String beamSelNames[] = new String[]{ 
			"Beam Autoselect", "Latitute/Longitude", 
			"Beam Q1", "Beam Q2", "Beam Q3", "Beam Q4", 
			"Beam Q5", "Beam Q6", "Beam Q7", "Beam Q8"   
	};

	protected TransformProcessor proc;
	private Button setPositionsCheckbox;
	
	private Combo beamSelectionCombo;
	private Button doFitCheckbox;
	private Button cubicTransformCheckbox;
	private Spinner cubicNumXSpinner;
	private Spinner cubicNumYSpinner;	
	
	private Button fullImageCheckbox;
	private Button generateLOSVectorsButton;
	
	private SWTSettingsControl settingsCtrl;
	
	private boolean isSinkController;
	
	private Table pointsTable;
	private TableEditor pointsTableEditor;
	private TableItem tableItemEditing = null;
	
	public TransformSWTController(Composite parent, int style, TransformProcessor proc, boolean isSinkController) {
		this.isSinkController = isSinkController;
		this.proc = proc;
		swtGroup = new Group(parent, style);
		swtGroup.setText("Transform Control (" + proc.toShortString() + ")");		
		swtGroup.setLayout(new GridLayout(6, false));
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); }});
		
		addCommonPipeControls(swtGroup);

		Label lBS = new Label(swtGroup, SWT.NONE); lBS.setText("Coordinates:");
		beamSelectionCombo = new Combo(swtGroup, SWT.NONE);
		beamSelectionCombo.setItems(beamSelNames);
		beamSelectionCombo.setToolTipText("Sets the coordinate space of the output image as (R,Z) at the plane of the given neutral beam, or as (longitude, latitude) from the camera"); 
		beamSelectionCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		beamSelectionCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		doFitCheckbox = new Button(swtGroup, SWT.CHECK);
		doFitCheckbox.setText("Refit transform");
		doFitCheckbox.setSelection(proc.getDoTransformFit());
		doFitCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		doFitCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		fullImageCheckbox = new Button(swtGroup, SWT.CHECK);
		fullImageCheckbox.setText("Full image");
		fullImageCheckbox.setSelection(proc.getTransformFullImage());
		fullImageCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		fullImageCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		setPositionsCheckbox = new Button(swtGroup, SWT.CHECK);
		setPositionsCheckbox.setText("Set points");
		setPositionsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		generateLOSVectorsButton = new Button(swtGroup, SWT.PUSH);
		generateLOSVectorsButton.setText("Generate LOS vectors");
		generateLOSVectorsButton.setToolTipText("Generates files containing the 3D unit vector away rom the camera position of each pixel in the camera image (might be slow). Writes to TEMP/pixelVectors/{x,y,z}.txt");
		generateLOSVectorsButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		generateLOSVectorsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { generateLOSVectorsEvent(event); } });
				
		
		cubicTransformCheckbox = new Button(swtGroup, SWT.CHECK);
		cubicTransformCheckbox.setText("Cubic");
		cubicTransformCheckbox.setSelection(false);
		cubicTransformCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		cubicTransformCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		Label lNX = new Label(swtGroup, SWT.NONE); lNX.setText("nKnots X:");
		cubicNumXSpinner = new Spinner(swtGroup, SWT.NONE);
		cubicNumXSpinner.setValues(3, 2, 100, 0, 1, 4);
		cubicNumXSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		cubicNumXSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNY = new Label(swtGroup, SWT.NONE); lNY.setText("nKnots Y:");
		cubicNumYSpinner = new Spinner(swtGroup, SWT.NONE);
		cubicNumYSpinner.setValues(3, 2, 100, 0, 1, 4);
		cubicNumYSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		cubicNumYSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
				
		
		pointsTable = new Table(swtGroup, SWT.BORDER | SWT.MULTI);				
		pointsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		pointsTable.setHeaderVisible(true);
		pointsTable.setLinesVisible(true);
		
		TableColumn cols[] = new TableColumn[colNames.length];
		for(int i=0; i < colNames.length; i++){
			cols[i] = new TableColumn(pointsTable, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]); 
		}

		pointsToGUI();

		for(int i=0; i < colNames.length; i++)
			cols[i].pack();
		
		pointsTable.addListener(SWT.MouseDoubleClick, new Listener() {		//and the user can do that by double clicking 
			@Override
			public void handleEvent(Event event) {
				for(int i=0; i < colNames.length; i++)
					cols[i].pack();		
			}
		});
		
		pointsTableEditor = new TableEditor(pointsTable);
		pointsTableEditor.horizontalAlignment = SWT.LEFT;
		pointsTableEditor.grabHorizontal = true;
		pointsTable.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		pointsTable.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceImageRedraw(); } });
		
		settingsCtrl = ImageProcUtil.createPreferredSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		GMDSSettingsControl settingsCtrl2 = new GMDSSettingsControl();
		settingsCtrl2.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl2.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		swtGroup.pack();
		doUpdate();
	}
	
	private void generateLOSVectorsEvent(Event e){
		proc.generateLOSVectors();
	}
	
	private void settingsChangedEvent(Event e){
		int bSel = beamSelectionCombo.getSelectionIndex();
		if(bSel == 0)
			proc.setBeamSelection(TransformProcessor.BEAMSEL_AUTO);
		else if(bSel == 1)
			proc.setBeamSelection(TransformProcessor.BEAMSEL_LATLON);
		else
			proc.setBeamSelection(bSel - 2);
			
		proc.setDoTransformFit(doFitCheckbox.getSelection());
		proc.setCubicInterp(cubicTransformCheckbox.getSelection(), 
								cubicNumXSpinner.getSelection(), 
								cubicNumYSpinner.getSelection());
		proc.setTransformFullImage(fullImageCheckbox.getSelection());
	}
	
	protected void doUpdate(){
		super.doUpdate();		

		settingsCtrl.doUpdate();
		
		int bSel = proc.getBeamSelection();
		if(bSel == TransformProcessor.BEAMSEL_AUTO) //auto
			beamSelectionCombo.setText(beamSelNames[0]);
		else if(bSel == TransformProcessor.BEAMSEL_LATLON) //lat/lon
			beamSelectionCombo.setText(beamSelNames[1]);
		else if(bSel >= 0) //auto
			beamSelectionCombo.setText("Q" + (bSel+1));
		
		boolean doFit = proc.getDoTransformFit();
		if(doFitCheckbox.getSelection() != doFit)
			doFitCheckbox.setSelection(doFit);
		
		boolean fullImage = proc.getTransformFullImage();
		if(fullImageCheckbox.getSelection() != fullImage)
			fullImageCheckbox.setSelection(fullImage);
	
		int nX = proc.getCubicNX(), nY = proc.getCubicNY();
		if(nX <= 0 && nY <= 0){
			if(cubicTransformCheckbox.getSelection())
				cubicTransformCheckbox.setSelection(false);
			
		}else{
			if(!cubicTransformCheckbox.getSelection())
				cubicTransformCheckbox.setSelection(true);
			
			if(cubicNumXSpinner.getSelection() != nX)
				cubicNumXSpinner.setSelection(nX);
			
			if(cubicNumYSpinner.getSelection() != nY)
				cubicNumYSpinner.setSelection(nY);
		}
				
		
		if(tableItemEditing == null)
			pointsToGUI();

		//wtGroup.getParent().forceFocus()
		//swtGroup.getParent().redraw();
		//swtGroup.getParent().update();
		
		
	}

	@Override
	public void movingPos(int x, int y) {	}

	@Override
	public void fixedPos(int x, int y) {	
		if(swtGroup.isDisposed() || !isSinkController || !setPositionsCheckbox.getSelection())
			return;
		
		Img img = proc.getSelectedImageFromConnectedSource();
		if(img == null)
			return;
		int imageWidth = img.getWidth();
		int imageHeight = img.getHeight();
		TableItem[] selected = pointsTable.getSelection();
		if(selected.length == 1){
			String pointName = selected[0].getText(1);
			
			FeatureTransform xform = proc.getTransform();
			PhysicalROIGeometry ccdGeom = proc.getCCDGeometry();
			TransformPoint p = xform.getPointByName(pointName);
			p.imgX = ccdGeom.getImgPhysX0() + x * (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0()) / imageWidth;
			p.imgY = ccdGeom.getImgPhysY0() + y * (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0()) / imageHeight;
			
			//we have to edit the item because the update won't actually take place immediately
			selected[0].setText(2, tableValueFormat.format(x));
			selected[0].setText(3, tableValueFormat.format(y));
			
			proc.pointsMapModified();
		}
		
	}

	@Override
	public void setRect(int x0, int y0, int width, int height) {
		
	}
	
	private final String formatTableValue(double val){
		return Double.isNaN(val) ? "NaN" : tableValueFormat.format(val);
	}
	
	private void pointsToGUI() {
		boolean convertFromIdiotCoords = false; 
		
		ArrayList<TransformPoint> points = proc.getTransform().getPoints();
		synchronized (points) {
			points = new ArrayList<TransformPoint>(points); //clone to release lock but avoid concurrent modification
		}
			
		//convert map to list and sort
		Collections.sort(points, new Comparator<TransformPoint>() {
			@Override
			public int compare(TransformPoint p1, TransformPoint p2) {				
				return p1.name.compareTo(p2.name);
			}
		});
		
		//clear and repopulate table
		int topIndex = pointsTable.getTopIndex();
		TableItem selected[] = pointsTable.getSelection();
		String selectedPoint = (selected.length >= 1) ? selected[0].getText(1) : null; 
		pointsTable.removeAll();
		for(TransformPoint p : points){
			
			if(p.name.length() <= 0)
				continue;
			TableItem item = new TableItem(pointsTable, SWT.NONE);
			item.setText(0, "o");
			item.setText(1, p.name);
			item.setText(2, formatTableValue(p.imgX * 1e3));
			item.setText(3, formatTableValue(p.imgY * 1e3));
			
			double trueX, trueY;
			if(convertFromIdiotCoords){
				double rotAng = -(90 + 22.5) * Math.PI / 180;
				trueX = p.x * FastMath.cos(rotAng) + p.y * FastMath.sin(rotAng);
				trueY = -p.x * FastMath.sin(rotAng) + p.y * FastMath.cos(rotAng);
			}else{
				trueX = p.x;
				trueY = p.y;
			}
			
			item.setText(4, formatTableValue(trueX));
			item.setText(5, formatTableValue(trueY));
			item.setText(6, formatTableValue(p.z));
			
			item.setText(7, formatTableValue(p.lat));
			item.setText(8, formatTableValue(p.lon));
			
			item.setText(9, TransformPoint.modeNames[p.mode]);
			
			item.setText(10, formatTableValue(p.R));
			item.setText(11, formatTableValue(p.Z));
			
			item.setText(12, formatTableValue(p.x));
			item.setText(13, formatTableValue(p.y));
			
			
			/*
			double data[] = p.toArray();
			if(data == null)
				data = new double[colNames.length-2];
			for(int i=0; i < Math.min(data.length, colNames.length-2); i++){
				double mul = (i < 2) ? 1e3 : 1; // img(ccd) coords in mm
				if(i == 7){
					item.setText(i+2, TransformPoint.modeNames[(int)data[i]]);
				}else if(i >= 4 && i <= 6){
					
				}else{
					item.setText(i+2, Double.isNaN(data[i]) ? "NaN" : tableValueFormat.format(mul * data[i]));
				}
					
			}
			*/
			
		}
		
		//and finally a blank item for creating new point
		TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
		blankItem.setText(0, "o");
		blankItem.setText(1, "<new>");
		
		//pointsTable.setTopIndex(topIndex);
		
		if(selectedPoint != null){
			for(int i=0; i < pointsTable.getItemCount(); i++){
				if(selectedPoint.equals(pointsTable.getItem(i).getText(1))){
					pointsTable.select(i);
					break;
				}
			}
		}

		pointsTable.showSelection();
		
	}
		
	@Override
	public void drawOnImage(GC gc, double scale[], int imageWidth, int imageHeight, boolean asSource) {
		
		ArrayList<TransformPoint> points = proc.getTransform().getPoints();
		synchronized (points) {
			points = new ArrayList<TransformPoint>(points); //clone to release lock but avoid concurrent modification
		}
		
		int r = Math.min(imageWidth, imageHeight) / 200;
		
		HashMap<TransformPoint, double[]> remapped = proc.getBackConvertedXY();
		PhysicalROIGeometry ccdGeom = proc.getCCDGeometry();
		
		int i=0;
		for(TransformPoint p : points){
			
			if(p.mode == TransformPoint.MODE_IGNORE || p.mode == TransformPoint.MODE_VIEW)
				continue;
				
			TableItem[] selectedItems = pointsTable.getSelection();
			boolean isSelected = false;
			for(TableItem item : selectedItems){
				if(item.getText(1).equals(p.name))
					isSelected = true;
			}
			if(isSelected){
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_MAGENTA));
				gc.setLineWidth(3);
			}else{
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
				gc.setLineWidth(1);
			}
			
			double imgX, imgY;
			if(asSource){
				if(proc.getBeamSelection() == TransformProcessor.BEAMSEL_LATLON){
					imgX = p.lon;
					imgY = p.lat;
				}else{
					imgX = p.R;
					imgY = p.Z;
				}
				imgX = (imgX - proc.getA0()) / (proc.getA1() - proc.getA0());
				imgY = 1.0 - (imgY - proc.getB0()) / (proc.getB1() - proc.getB0());
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_BLUE));		
			}else{
				if(ccdGeom == null)
					return;
				
				imgX = (p.imgX - ccdGeom.getImgPhysX0()) / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
				imgY = (p.imgY - ccdGeom.getImgPhysY0()) / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
			}
			
			if(imgX >= 0 && imgY >= 0 && 
					imgX < 1.0 && imgY < 1.0){
					
				gc.drawOval(
						(int)((imgX*imageWidth - r) * scale[0]), 
						(int)((imgY*imageHeight - r) * scale[1]), 
						(int)(r*2 * scale[0]), 
						(int)(r*2 * scale[1]));
			}
			
			
			
			if(remapped != null){
				double remappedPos[] = remapped.get(p);
				if(!asSource && remappedPos != null) {
					double remappedX = (remappedPos[0] - ccdGeom.getImgPhysX0()) / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
					double remappedY = (remappedPos[1] - ccdGeom.getImgPhysY0()) / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
					gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_RED));			
					gc.drawOval(
							(int)((remappedX*imageWidth - r) * scale[0]), 
							(int)((remappedY*imageHeight - r) * scale[1]), 
							(int)(r*2 * scale[0]), 
							(int)(r*2 * scale[1]));
					
					if(isSelected){
						gc.drawLine((int)(imgX*imageWidth * scale[0]),
								(int)(imgY*imageHeight * scale[1]),
								(int)(remappedX*imageWidth * scale[0]),
								(int)(remappedY*imageHeight * scale[1]));
					}
				}
			}
			
			i++;
		}
	
		if(!asSource){

			gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
			
			//draw limit circles
			double circles[][] = proc.getLimitCircles();
			int nPointsInCirc = 200;
			for(int iC=0;iC < circles.length; iC++){
				
				for(int j=0; j < (nPointsInCirc-1); j++){
					double thetaA = j * 2 * Math.PI / nPointsInCirc;
					double thetaB = (j+1) * 2 * Math.PI / nPointsInCirc;
					
					double ccdXa = circles[iC][0] + circles[iC][2] * FastMath.cos(thetaA);
					double ccdYa = circles[iC][1] + circles[iC][2] * FastMath.sin(thetaA);
					double ccdXb = circles[iC][0] + circles[iC][2] * FastMath.cos(thetaB);
					double ccdYb = circles[iC][1] + circles[iC][2] * FastMath.sin(thetaB);
				
					double imgXa, imgYa, imgXb, imgYb;
					//if(!asSource){
						imgXa = (ccdXa - ccdGeom.getImgPhysX0()) / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
						imgYa = (ccdYa - ccdGeom.getImgPhysY0()) / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
						imgXb = (ccdXb - ccdGeom.getImgPhysX0()) / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
						imgYb = (ccdYb - ccdGeom.getImgPhysY0()) / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
						
					//}
					//else{ ???? }

					gc.drawLine((int)(imgXa*imageWidth * scale[0]),
							(int)(imgYa*imageHeight * scale[1]),
							(int)(imgXb*imageWidth * scale[0]),
							(int)(imgYb*imageHeight * scale[1]));
				}
			}
		}
		
	}
	
	private void forceImageRedraw() {
		//force update of GUI windows connected to the image source
		for(ImgSink sink : proc.getConnectedSource().getConnectedSinks()){
			if(sink instanceof ImagePanel){
				((ImagePanel)sink).redraw();
			}
		}
	}
	
	private Text editBox = null;
	/** Copied from 'Snippet123'  Copyright (c) 2000, 2004 IBM Corporation and others. 
	 * [ org/eclipse/swt/snippets/Snippet124.java ] */
	private void tableMouseDownEvent(Event event){
		Rectangle clientArea = pointsTable.getClientArea ();
		Point pt = new Point (clientArea.x + event.x, event.y);
		int index = pointsTable.getTopIndex ();
		while (index < pointsTable.getItemCount ()) {
			boolean visible = false;
			final TableItem item = pointsTable.getItem (index);
			for (int i=0; i<pointsTable.getColumnCount (); i++) {
				Rectangle rect = item.getBounds (i);
				if (rect.contains (pt)) {
					final int column = i;
					if(column == 0){
						//pointsTable.setSelection(index); //make sure its selected
						forceImageRedraw();
						return;
					}
					if(editBox != null){
						editBox.dispose(); //oops, one left over
					}
					editBox = new Text(pointsTable.getParent(), SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener () {
						public void handleEvent (final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, editBox.getText());								
								editBox.dispose();
								editBox = null;
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, editBox.getText());
									//FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									editBox.dispose ();
									editBox = null;
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					editBox.addListener (SWT.FocusOut, textListener);
					editBox.addListener (SWT.Traverse, textListener);
					pointsTableEditor.setEditor (editBox, item, i);
					editBox.setText (item.getText (i));
					editBox.selectAll ();
					editBox.setFocus ();
					//hacks for SWT4.4 (under linux GTK at least)
					//Textbox won't display if it's a child of the table
					//so we make it a child of the table's parent, but now need to adjust the location
					{
						editBox.moveAbove(pointsTable);
						final Point p0 = editBox.getLocation();
						Point p1 = pointsTable.getLocation();
						p0.x += p1.x - clientArea.x;
						p0.y += p1.y + editBox.getSize().y;
						editBox.setLocation(p0);	
						editBox.addListener (SWT.Move, new Listener() {						
							@Override
							public void handleEvent(Event event) {
								editBox.setLocation(p0); //TableEditor keeps moving it to relative to the table, so move it back
							}
						});
					}
					return;
				}
				if (!visible && rect.intersects (clientArea)) {
					visible = true;
				}
			}
			if (!visible) return;
			index++;
		}
	}
	
	private void tableColumnModified(TableItem item, int column, String text){
		boolean convertFromIdiotCoords = false;
		
		ArrayList<TransformPoint> points = proc.getTransform().getPoints();
		synchronized (points) {
			
			String pointName = item.getText(1);
			
			TransformPoint point = (pointName.length() > 0) 
									? proc.getTransform().getPointByName(pointName)
									: null;
	
			if(column == 1){
				
				if(pointName.length() <= 0 || pointName.equals("<new>")){ //creating new point
					point = new TransformPoint(text);
					points.add(point);
					
				}else if(text.length() <= 0){ //deleting point
					points.remove(point);
					
				}else{ //just changing name
					point.name = text;
				}
				
				item.setText(column, text);
			}else if(column == 9){ //for point type, we accept any part of the string, or the value 
				if(pointName.length() > 0 && !pointName.equals("<new>") && point != null){ //can only edit values for existing points
	
					int bestMatchIdx = OneLiners.findBestMatchingString(TransformPoint.modeNames, text);
					
					if(bestMatchIdx > 0){ 
						point.mode = bestMatchIdx;
					}else{//otherwise, fall back to interpreting as a number
						point.mode = Algorithms.mustParseInt(text);
						
					}
					
					item.setText(column, TransformPoint.modeNames[point.mode]);
					
				}
			}else{
				if(pointName.length() > 0 && point != null){ //can only edit values for existing points
					double val = Algorithms.mustParseDouble(text);
					
					//special handling of AUG vs stupid coordinates
					if(column == 4 || column == 5){ //AUG X/Y coords
						double trueX, trueY;
						double rotAng = -(90 + 22.5) * Math.PI / 180;
						if(convertFromIdiotCoords){
							trueX = point.x * FastMath.cos(rotAng) + point.y * FastMath.sin(rotAng);						
							trueY = -point.x * FastMath.sin(rotAng) + point.y * FastMath.cos(rotAng);
						}else{
							trueX = point.x;
							trueY = point.y;
						}
						
						if(column == 4) 
							trueX = val;
						else 
							trueY = val;
						
						if(convertFromIdiotCoords){
							point.x = trueX * FastMath.cos(rotAng) - trueY * FastMath.sin(rotAng);						
							point.y = trueX * FastMath.sin(rotAng) + trueY * FastMath.cos(rotAng);
						}else{
							point.x = trueX;						
							point.y = trueY;
						}
						
					}else if(column == 12){
						point.x = val;
					}else if(column == 13){
						point.y = val;
						
					}else{
						point.setField(column-2, val);
					}
					item.setText(column,  Double.isNaN(val) ? "NaN" :tableValueFormat.format(val));
				}
			}
	
			TableItem lastItem = pointsTable.getItem(pointsTable.getItemCount()-1);
			if(!lastItem.getText(1).equals("<new>")){
				TableItem blankItem = new TableItem(pointsTable, SWT.NONE);
				blankItem.setText(0, "o");
				blankItem.setText(1, "<new>");
			}
			
			//proc.saveMapPoints();
			proc.pointsMapModified();
		}
	}

	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
		//proc.destroy(); //and actively kill it, for good measure
	}

	@Override
	public Object getInterfacingObject() { return swtGroup;	}

	@Override
	public TransformProcessor getPipe() { return proc;	}
}
