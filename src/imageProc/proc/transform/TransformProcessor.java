package imageProc.proc.transform;

import java.lang.reflect.Array;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import algorithmrepository.Algorithms;
import algorithmrepository.CubicInterpolation2D;
import algorithmrepository.Interpolation2D;
import algorithmrepository.InterpolationMode;
import binaryMatrixFile.AsciiMatrixFile;
import descriptors.aug.AUGSignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcPipeMultithread;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.database.gmds.GMDSUtil;
import fusionDefs.neutralBeams.SimpleBeamGeometry;
import fusionDefs.transform.FeatureTransform;
import fusionDefs.transform.FeatureTransformCubic;
import fusionDefs.transform.PhysicalROIGeometry;
import fusionDefs.transform.TransformPoint;
import fusionOptics.Util;
import net.jafama.FastMath;
import mds.AugShotfileFetcher;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.aug.AUGSignal;
import signals.gmds.GMDSSignal;

/** Image transform processor.
 * 
 * Translate an image from some image (and physical CCD) coordinates to (lat,lon) (using FeatureTransform)
 * and then (possibly) R,Z of nearest intersection of a pixel's LOS to a given beam axis. 
 * 
 * The LL --> RZ part is done here, not in FeatureTransform.
 * 
 * The SWT controller is used to select points on the image which can
 * be given 3D points of the vessel. Along with a single 3D observation point
 * on the mirror, and the beam definitions, this should be used to map those
 * points to that line's intersection on the beam plane. 
 * 
 * Not sure about thread safety
 * 
 * @author oliford
 */
public class TransformProcessor extends ImgProcPipeMultithread implements ConfigurableByID {
	private static final double electronCharge = 1.60217662e-19;
	private static final double protonMass = 1.6726219e-27;
	
	/** The transform - holds the data of the points to fit, and the fit result */
	protected FeatureTransformCubic xform = new FeatureTransformCubic();
	
	/** Holds the transform between physical CCD coordinates and the image (via ROI settings and CCD properties etc) */
	private PhysicalROIGeometry ccdGeom;
	
	/** The transform fitter that does the work */
	private TransformFitter transformFitter;
	
	protected SimpleBeamGeometry beamGeom;
			
	private BulkImageAllocation<ByteBufferImage> bulkAlloc = new BulkImageAllocation<ByteBufferImage>(this);
			
	/** Automatic beam selection. Ranges are done to the maximum of the tranform with all beams */
	public static final int BEAMSEL_AUTO = -2;
	/** Don't do LL --> RZ transform, just output in lat/lon space. */
	public static final int BEAMSEL_LATLON = -1;
	/** Beams indices 0-7 for Q1-Q8, -1 for lat/lon, -2 for auto */
	public int beamSelection = BEAMSEL_AUTO;
	
	/** Image A,B coordinates (corners are not necessarily the same as the transform corners) */
	private double A0, B0, A1, B1, dA, dB;
	
	private int cubicNX = 2, cubicNY = 2;
	
	/** Unused points, converted from R,Z back to image X,Y */
	private HashMap<TransformPoint, double[]> backConvertedXY;
	
	/** The average view position.
	 * This is the bit that the transform can't know so has to be entered
	 * as a point in the points table.
	 * 
	 * It can be calculated from the fitted ray-tracer. (see FusionOptics-imse/seed.minerva.apps.imse/TransformMatch) */
	private double viewPos[];
	
	/** If true, transformed image is created to cover all of input image, otherwise only the points */
	public boolean transformFullImage = true;
	
	/** Actually fit the linear/cubic transform to the stored points. If false, it is just used as it was */
	boolean fitTransform = true;
	
	//Dimensions of the cubic interpolators outputImageCCD{XY}
	// This should be quite a lot, because the edges are NaN and anything in a grid cell
	// touching an edge will also be NaN (but really shouldn't) However doing it for every pixel 
	// is just silly
	private int outputImageCCDConversionGridSizeX = 102; 
	private int outputImageCCDConversionGridSizeY = 98;
	/** Physical XY coords on the CCD of a given pixel on the output image [beamIdx](ioX, ioY) */
	private Interpolation2D outputImageCCDX[], outputImageCCDY[];
	private String lastLoadedConfig;
		
	public TransformProcessor() { 
		super(ByteBufferImage.class);
		//bulkAlloc.setMaxMemoryBufferAlloc(Long.MIN_VALUE);
	}
	
	public TransformProcessor(ImgSource source, int selectedIndex) {
		super(ByteBufferImage.class, source, selectedIndex);	
		//bulkAlloc.setMaxMemoryBufferAlloc(Long.MIN_VALUE);
	}
	
	@Override
	protected int[] sourceIndices(int outIdx) {		
		return new int[]{ outIdx }; //always 1:1
	}
	
	@Override
	protected void preCalc(boolean settingsHadChanged) {
		super.preCalc(settingsHadChanged);
		
	}	
	
	/** Image allocation */
	protected boolean checkOutputSet(int nImagesIn){
		int nImagesOut;
		
		if(connectedSource == null || nImagesIn <= 0){
			System.err.println("TransformProcessor: No source, or no images");
			nImagesOut = 0;
			if(bulkAlloc != null) //Should never be null, but sometimes is
				bulkAlloc.destroy();
			return true;
		}
		
		//need to do the transform now, so we know how big the output images have to be
		try{
			status = "Calculating transform";
			updateAllControllers();
			
			if(beamSelection != BEAMSEL_LATLON)
				calcBeamVectors();
			prepTransform();
			if(!xform.isValid())
				throw new RuntimeException("Transform invalid");
			viewPos = xform.getViewPosition();
			
			status = "Calculating image coordinates";
			updateAllControllers();
			
			calcOutputImageAxes(transformFullImage);
			calcOutputPixelXY(); //can take ages, also calculates the 'A factors'
			if(abortCalc){
				nImagesOut = 0;
				bulkAlloc.destroy();				
				return true;
			}
			
			nImagesOut = nImagesIn;
		}catch(RuntimeException err){			
			nImagesOut = 0;
			bulkAlloc.destroy();
			imagesOut = new ByteBufferImage[0];
			RuntimeException err2 = new RuntimeException("Error in transform init/fit: " + err);
			err2.initCause(err);
			throw(err2);
		}		
		
		
		Img anImage = null;
		for(int i=0; i < connectedSource.getNumImages(); i++){
			anImage = connectedSource.getImage(i);
			if(anImage != null)
				break;
		}
		if(anImage == null)
			throw new RuntimeException("No input images");
		int nFields = anImage.getNFields();				
		
		if(outWidth == 0 || outHeight == 0)
			throw new RuntimeException("Invalid output image size " + outWidth + " x " + outHeight);
		
		 //allocate or re-use
	    ByteBufferImage templateImage = new ByteBufferImage(null, -1, outWidth, outHeight, nFields, ByteBufferImage.DEPTH_DOUBLE, false);
	    templateImage.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	    
	    status = "Allocating";
		updateAllControllers();
        if(bulkAlloc.reallocate(templateImage, nImagesOut)){
	       	imagesOut = bulkAlloc.getImages();
	       	return true;
        }          
        return false;
	}
	
	protected void calcBeamVectors() {
		throw new RuntimeException("Not generally supported. This was moved to TransformProcessorAUG");
	}

	@Override
	protected boolean doCalc(Img imageOutG, WriteLock writeLockOut, Img[] sourceSet, boolean settingsHadChanged) throws InterruptedException {
		Img imageIn = sourceSet[0];		
		ByteBufferImage imageOut = (ByteBufferImage)imageOutG;
		
		int beamIdx;
		if(beamSelection == BEAMSEL_LATLON){
			beamIdx = 0;
		}else if(beamSelection == BEAMSEL_AUTO){
			//beamIdx = getBeamIndex(imageIn.getSourceIndex());
			beamIdx = (Integer)getImageMetaData("beamIndex", imageIn.getSourceIndex());
			
		}else{
			beamIdx = beamSelection;
		}
		
		if(outputImageCCDX[beamIdx] == null){
			throw new RuntimeException("No outputImage coordinates for beam " + (beamIdx+1));
		}
		
		try{
			ReadLock readLockIn = imageIn.readLock();
			readLockIn.lockInterruptibly();
			try{
				for(int oY=0; oY < outHeight; oY++) {
					for(int oX=0; oX < outWidth; oX++) {						
						//get physical coords on CCD corresponding to this pixel on the transformed image
						double physX = outputImageCCDX[beamIdx].eval((double)oX/outWidth, (double)oY/outHeight);
						double physY = outputImageCCDY[beamIdx].eval((double)oX/outWidth, (double)oY/outHeight);
						
						//convert to input image coordinate
						double fX = (physX - ccdGeom.getImgPhysX0()) * inWidth / (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0());
						double fY = (physY - ccdGeom.getImgPhysY0()) * inHeight / (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0());
					
						if(fX < 0 || fX >= (inWidth-2) ||
							fY < 0 || fY >= (inHeight-2) ){
								//out of limits
							for(int iF=0;iF < imageIn.getNFields(); iF++)
								imageOut.setPixelValue(writeLockOut, iF, oX, oY, Double.NaN);
							continue;
						}
						
						int iiX = (int)fX;
						int iiY = (int)fY;
						
						fX -= iiX;
						fY -= iiY;
						
						if(fX < 0 || fY < 0 || fX >= 1 || fY >= 1){
							System.err.println("Transformprocessor.doCalc(): Interpolation out of range.");
						}
						
						for(int iF=0;iF < imageIn.getNFields(); iF++){
							double val = 	(1 - fX) * (1 - fY) * imageIn.getPixelValue(readLockIn, iF, iiX,   iiY) + 
										         fX  * (1 - fY) * imageIn.getPixelValue(readLockIn, iF, iiX+1, iiY) + 
											(1 - fX) *      fY  * imageIn.getPixelValue(readLockIn, iF, iiX,   iiY+1) + 
										         fX  *      fY  * imageIn.getPixelValue(readLockIn, iF, iiX+1, iiY+1) ;
							
							imageOut.setPixelValue(writeLockOut, iF, oX, oY, val);
						}
					}
				}
			}finally{
				readLockIn.unlock();
			}
			
		}catch(InterruptedException err){ }
		
		return true;
	}
	
	
	/** Create the linear and non-linear mapping of physical CCD position to lat / lon
	 * (and save that info to the INPUT image metadata) */
	private void prepTransform() {
		
		//have to do these anyway. We still need to deal with ROIs etc even if we're not fitting
		calcCCDGeom();			//work out the corners of the image in physical CCD space, from the camera MetaData
		xform.calcPointsLatLon();	//Calculate the lat/lon angles of the transform points with 3D data
		
		if(fitTransform){
			xform.calcLinear();			//Calculate the linear (imgX,imgY) <--> (lon,lat) tranform
			xform.calcLLRanges(ccdGeom);		//Calculate range of transform for the input image
		}
		
		//put these in source metadata list so that any saving of the transform also stores the transform used to generate it
		//this stuff is relevant for the input image, so save it to our source (output relevant stuff is saved here downwards)
		connectedSource.setSeriesMetaData("/Transform/linearXYtoLL", xform.getXYtoLLMat(), false);
		connectedSource.setSeriesMetaData("/Transform/linearLLtoXY", xform.getLLtoXYMat(), false);
				
		if(cubicNX >= 1){
			xform.initCubic(cubicNX, cubicNY);
			
			transformFitter = new TransformFitter(xform);
			transformFitter.fit();
			
			connectedSource.setSeriesMetaData("/Transform/cubicKnotsLon", xform.getKnotsLon(), false);
			connectedSource.setSeriesMetaData("/Transform/cubicKnotsLat", xform.getKnotsLat(), false);
			connectedSource.setSeriesMetaData("/Transform/cubicKnotsX", xform.getKnotValsX(), false);
			connectedSource.setSeriesMetaData("/Transform/cubicKnotsY", xform.getKnotValsY(), false);			
		}
		
		viewPos = xform.getViewPosition();

		List<TransformPoint> points = xform.getPoints();
		synchronized (points) {
			backConvertedXY = new HashMap<TransformPoint, double[]>();
			for(TransformPoint p : points){
				if(p.mode == TransformPoint.MODE_LIMIT && Double.isNaN(p.x)){
					double ll[] = xform.xyToLatLon(p.imgX, p.imgY);
					p.lon = ll[0];
					p.lat = ll[1];
				}
				if(!p.name.equals("minRZ") && !p.name.equals("maxRZ")){
					if(beamSelection >= 0){
						double RZ[] = ABtoRZ(new double[]{ p.lon, p.lat }, beamSelection);
						p.R = RZ[0];
						p.Z = RZ[1];
					}else{
						p.R = Double.NaN;
						p.Z = Double.NaN;
					}		
				}
				if(p.mode != TransformPoint.MODE_IGNORE){
					System.out.println("POINT '" + p.name + "': ");
					
					//calc theoretical image position from CAD (lon,lat)
					double backXY[] = xform.latlonToXY(p.lon, p.lat);
					double AB2[] = xform.xyToLatLon(backXY[0], backXY[1]); //and go back to ll again					
					double lin[] = xform.latlonToXYLinear(p.lon, p.lat);
					System.out.println("\tLon: " + p.lon + " --> x="+backXY[0]+" --> lon="+AB2[0] + ", linX = " + lin[0]);
					System.out.println("\tLat: " + p.lat + " --> y="+backXY[1]+" --> lat="+AB2[1] + ", linY = " + lin[1]);
					backConvertedXY.put(p, backXY);
					
					//calculate (lon,lat) from clicked image position (measured)
					double ll2[] = xform.xyToLatLon(p.imgX, p.imgY);
					System.out.println("\tFwd LL: lon=" + ll2[0]*180/3.14 + ", lat=" + ll2[1]*180/3.14);
					p.lonImg = ll2[0]; p.latImg=ll2[1];
					
					double viewPos[] = xform.getViewPosition();
					//double l = 2.2;
					double l = Util.length(Util.minus(new double[]{p.x, p.y, p.z}, viewPos));
					double u[] = { 
							FastMath.cos(ll2[0]) * FastMath.cos(ll2[1]),
							FastMath.sin(ll2[0]) * FastMath.cos(ll2[1]),
							FastMath.sin(ll2[1]),
						};
					double posOnLineM[] = Mat.add(viewPos, Mat.mul(u, l)); 
					System.out.println("\tPosition at "+l+"m from viewPos (Image) = { " + posOnLineM[0] + ", " +posOnLineM[1] + ", " +posOnLineM[2] + "}");
					u = new double[]{ 
							FastMath.cos(p.lon) * FastMath.cos(p.lat),
							FastMath.sin(p.lon) * FastMath.cos(p.lat),
							FastMath.sin(p.lat),
						};
					double posOnLineC[] = Mat.add(viewPos, Mat.mul(u, l)); 
					System.out.println("\tPosition at "+l+"m from viewPos (CAD) = { " + posOnLineC[0] + ", " +posOnLineC[1] + ", " +posOnLineC[2] + "}");
					
					double diff[] = Util.minus(posOnLineM, posOnLineC);
					System.out.println("\tDiff at "+l+"m = " + diff[0]*1e3 + ", " + diff[1]*1e3 + ", " + diff[2]*1e3 + " mm");
					
					//make a line from the real QSK view position
					double losStart[] = { 2.122, 6.106, 0.377 };
					
					/*
					// Hackery for beam intersection for W7X CXRS 
					double beamStartQ7[] = {0.27869976749402436,  6.194684785564924,  0.305}, beamUVecQ7[] = { -0.10846899866975897,    -0.9904233567365652, -0.08541692313736746 };
					double beamStartQ8[] = {0.2083656846072355,   6.207530069936235,  0.305}, beamUVecQ8[] = { -0.2486230639620658,    -0.9648266794133848, -0.08541692313736746 };
					//double beamStartK21[] = Util.mul(Util.plus(beamStartQ7, beamStartQ8), 0.5);
					//double beamUVecK21[] = Util.reNorm(Util.plus(beamStartQ7, beamStartQ8));
					double beamStartK21[] = beamStartQ8;					
					double beamUVecK21[] = beamUVecQ8;
										
					//find intersection to CAD point
					//hacked in for beam 
					//should probably add this properly for a given l
					double uLosC[] = Util.reNorm(Util.minus(posOnLineC, losStart));
					double t = Algorithms.pointOnLineNearestAnotherLine(losStart, uLosC, beamStartK21, beamUVecK21);
					double beamPosC[] = Util.plus(losStart, Util.mul(uLosC, t));
					l = Util.length(Util.minus(losStart, beamPosC)); //which is probably t, you idiot
					System.out.println(p.name + ": Position at "+l+"m from QSK losStart (CAD) = { " + beamPosC[0] + ", " +beamPosC[1] + ", " +beamPosC[2] + "}");
					
					double uLosM[] = Util.reNorm(Util.minus(posOnLineM, losStart));
					t = Algorithms.pointOnLineNearestAnotherLine(losStart, uLosM, beamStartK21, beamUVecK21);
					
					double beamPosM[] = Util.plus(losStart, Util.mul(uLosM, t));
					l = Util.length(Util.minus(losStart, beamPosC));
					System.out.println(p.name + ": Position at "+l+"m from losStart (Image) = { " + beamPosM[0] + ", " +beamPosM[1] + ", " +beamPosM[2] + "}");
					
					diff = Util.minus(beamPosM, beamPosC);
					System.out.println(p.name + ": Diff obs: " + diff[0]*1e3 + ", " + diff[1]*1e3 + ", " + diff[2]*1e3 + " mm, "
							+ "dR=" + (FastMath.sqrt(beamPosM[0]*beamPosM[0] + beamPosM[1]*beamPosM[1]) 
										- FastMath.sqrt(beamPosC[0]*beamPosC[0] + beamPosC[1]*beamPosC[1]))*1e3 + " mm");
					*/
					
					// Make the FreeCAD python command to build a LOS
					System.out.println("\tFreeCAD: Part.show(Part.makeCylinder(5.0,"+(l*1000)
							+ ",FreeCAD.Vector("+viewPos[0]*1000+","+viewPos[1]*1000+","+viewPos[2]*1000+")"
							+ ",FreeCAD.Vector("+u[0]+","+u[1]+","+u[2]+")));"
							+ " FreeCAD.ActiveDocument.ActiveObject.Label=\""+p.name+"\"");
					//*/
					System.out.println();
					
				}			
			}
		
			// we may have changed the R,Z data
			//saveMapPoints();
			
			String pointNames[] = new String[points.size()];
			double pointData[][] = new double[points.size()][];		
			int i=0;
			for(TransformPoint p : points){
				pointNames[i] = p.name;
				pointData[i] = p.toArray();
				i++;
			}		
			connectedSource.setSeriesMetaData("/Transform/pointNames", pointNames, false);
			connectedSource.setSeriesMetaData("/Transform/pointData", pointData, false);
			connectedSource.addNonTimeSeriesMetaDataMap(ccdGeom.toMap());
			
			double viewPos[] = xform.getViewPosition();
			System.out.println("ViewPos = { " + viewPos[0] + ", " +viewPos[1] + ", " +viewPos[2] + "}");
			
		}	
		
		
	}
	
	
	/** Calculate the physical X,Y on the CCD of each output image pixel */	
	private void calcOutputPixelXY(){
		//ConversionGrid: 
		// these will be out grid to the outputImageCCD{X/Y} interpolators
		//  they are fractional position in the transformed image space (oX,oY)
		double cgX[] = Mat.linspace(0.0, 1.0, outputImageCCDConversionGridSizeX);
		double cgY[] = Mat.linspace(0.0, 1.0, outputImageCCDConversionGridSizeY);
		
		//linear output coords on conversion grid
		double cgA[] = new double[cgX.length];
 		double cgB[] = new double[cgY.length];
 		for(int iX=0; iX < cgX.length; iX++)
			cgA[iX] = A0 + cgX[iX]*outWidth * dA;
 		for(int iY=0; iY < cgY.length; iY++)
 			cgB[iY] = B1 - cgY[iY]*outHeight * dB;
			
 		//If output coords are just lat/lon of camera, this is easy
		if(beamSelection == BEAMSEL_LATLON){
			outputImageCCDX = new Interpolation2D[1];
			outputImageCCDY = new Interpolation2D[1];
			
			double gridCCDX[][] = new double[outputImageCCDConversionGridSizeX][outputImageCCDConversionGridSizeY];
			double gridCCDY[][] = new double[outputImageCCDConversionGridSizeX][outputImageCCDConversionGridSizeY];
			
			for(int iY=0; iY < cgY.length; iY++){
				for(int iX=0; iX < cgX.length; iX++){
					double A = A0 + cgX[iX]*outWidth * dA;
					double B = B1 - cgY[iY]*outHeight * dB;
					
			        double fXY[] = xform.latlonToXY(A, B);					
			        gridCCDX[iX][iY] = fXY[0];
			        gridCCDY[iX][iY] = fXY[1];
				}
			}
			outputImageCCDX[0] = new Interpolation2D(cgX, cgY, gridCCDX, InterpolationMode.CUBIC);
			outputImageCCDY[0] = new Interpolation2D(cgX, cgY, gridCCDY, InterpolationMode.CUBIC);
			
			setSeriesMetaData("Transform/convGridLon", cgA, false);
			setSeriesMetaData("Transform/convGridLat", cgB, false);
			setSeriesMetaData("Transform/convGridR", null, false);
			setSeriesMetaData("Transform/convGridZ", null, false);
			
			return;
		}
		
		//output is (A,B) = (R,Z) nearest the given beam
		outputImageCCDX = new Interpolation2D[8];
		outputImageCCDY = new Interpolation2D[8];
		double outputPhiClosest[][][] = new double[8][][];
		double gammaAFactors[][][][] = new double[8][][][];
		
		double avgLon = (xform.getMinLon() + xform.getMaxLon()) / 2; 
		double avgLat = (xform.getMinLat() + xform.getMaxLat()) / 2;
	
 		double avgLosVec[] = new double[]{
				FastMath.cos(avgLat) * FastMath.cos(avgLon),
				FastMath.cos(avgLat) * FastMath.sin(avgLon),
				FastMath.sin(avgLat),
		};
 		
 		double virtMeasPlane[][] = initVirtualMeasurementPlane(xform);
 		
 		//horrible hack to detect which beam box we're looking at
 		boolean isBox2 = xform.getViewPosition()[1] > 0; //+ve y means we're probably the permIMSE system
		 
		for(int iB=0; iB < 8; iB++){ //for each beam
			if((isBox2 && iB < 4) || (!isBox2 && iB >= 4))
				continue;
			
			//only calculate for necessarry beams
			if(beamSelection == BEAMSEL_AUTO || beamSelection == iB){
				double gridCCDX[][] = new double[outputImageCCDConversionGridSizeX][outputImageCCDConversionGridSizeY];
				double gridCCDY[][] = new double[outputImageCCDConversionGridSizeX][outputImageCCDConversionGridSizeY];
					
				outputPhiClosest[iB] = new double[cgY.length][cgX.length];
				gammaAFactors[iB] = new double[cgY.length][cgX.length][];
				
				//find point along beam closest to central view chord
				double s = Algorithms.pointOnLineNearestAnotherLine(beamGeom.start(iB), beamGeom.uVec(iB), viewPos, avgLosVec);
				double pos[] = Mat.add(beamGeom.start(iB), Mat.mul(beamGeom.uVec(iB), s));  // beamStart + s * beamVec
						
				//toroidal angle of that point
				double phi0 = FastMath.atan2(pos[1], pos[0]);
		     
				for(int iY=0; iY < cgY.length; iY++){
					status = "Calculating image coordinates, beam = " + iB + " / 8, y = " + iY + " / " + cgY.length;
					updateAllControllers();
					if(abortCalc)
						return;
					
					
					for(int iX=0; iX < cgX.length; iX++){
						//output image coordinates secify (R,Z) exactly (a ring) 
						double R = A0 + cgX[iX]*outWidth * dA;
						double Z = B1 - cgY[iY]*outHeight * dB;
						
						//this used to work using the last LOS's phi as the start point, but it can go crazy. 
						//It's safer to do it every time
						double phi = findPhiAtBeam(iB, R, Z, phi0);
										    
						pos = new double[]{ R * FastMath.cos(phi), R * FastMath.sin(phi), Z };
				        double obsVec[] = Mat.normaliseVector(Mat.subtract(pos, viewPos));
				        double lon = FastMath.atan2(obsVec[1], obsVec[0]);
				        double lat = FastMath.asin(obsVec[2]);
				        
				        outputPhiClosest[iB][iY][iX] = phi;
							
				        double fXY[] = xform.latlonToXY(lon, lat);					
						//outputImageX[iB][oY][oX] = fXY[0];
						//outputImageY[iB][oY][oX] = fXY[1];
						gridCCDX[iX][iY] = fXY[0];
				        gridCCDY[iX][iY] = fXY[1];
				        
				        //if(iB==2 && iY==(cgY.length/2) && iX==(cgX.length/2))
				        	//System.out.println("TransformProcessor.calcOutputPixelXYU(): Break");
				        gammaAFactors[iB][iY][iX] = calcAFactors(virtMeasPlane, viewPos, pos, beamGeom, iB);
				        
					}
				}

				outputImageCCDX[iB] = new Interpolation2D(cgX, cgY, gridCCDX, InterpolationMode.CUBIC);
				outputImageCCDY[iB] = new Interpolation2D(cgX, cgY, gridCCDY, InterpolationMode.CUBIC);
				
			}
		}
		
		//the phi and the view pos is then enough information for anyone else to reconstruct the full 3D geometry
		//although they have to kind-of guess that outputPhiClosest should be interpolated out
		//to the full image size (which we're not writing here)
		for(int iB=0; iB < outputPhiClosest.length; iB++){
			setSeriesMetaData("Transform/outputPhiClosest-Q" + (iB+1), outputPhiClosest[iB], false); //this is only relevant to the output image
			setSeriesMetaData("Transform/gammaFactors/A-Q" + (iB+1), gammaAFactors[iB], false); //this is only relevant to the output image
			
			//GMDSSignalDesc sigDesc = new GMDSSignalDesc(0, "TEST", "A-Q" + (iB+1));			
			//GMDSSignal sig = new GMDSSignal(sigDesc, (gammaAFactors[iB] != null) ? gammaAFactors[iB] : new double[]{ Double.NaN }, new Object[]{  cgA, cgB });
			//GMDSOneLiners.globalGMDS().writeToCache(sig);	
			
		}
		connectedSource.setSeriesMetaData("Transform/gammaFactors/measPlaneNormal", virtMeasPlane[0], false);
		connectedSource.setSeriesMetaData("Transform/gammaFactors/measPlaneRight", virtMeasPlane[1], false);
		connectedSource.setSeriesMetaData("Transform/gammaFactors/measPlaneUp", virtMeasPlane[2], false);
		setSeriesMetaData("Transform/convGridR", cgA, false);
		setSeriesMetaData("Transform/convGridZ", cgB, false);
		setSeriesMetaData("Transform/convGridLon", null, false);
		setSeriesMetaData("Transform/convGridLat", null, false);
		
		connectedSource.setSeriesMetaData("Transform/viewPosition", viewPos, false); //this is general
		connectedSource.setSeriesMetaData("Transform/targetPosition", xform.getTargetPosition(), false); //this is general
	}
	
	private double[][] initVirtualMeasurementPlane(FeatureTransformCubic xform2) {
		throw new RuntimeException("Not generally supported. This was moved to TransformProcessorAUG");
	}

	/** Find phi value on the ring (R,Z) that is nearest the axis of the given beam */ 
	private double findPhiAtBeam(int iB, double R, double Z, double phi){
		
		//iterate until we find the phi on this (R,Z) ring that is closest to the beam
		double d = Double.NaN;
        for(int k=0; k < 100; k++){
        	double curPos[] = new double[]{
        			R * FastMath.cos(phi),
        			R * FastMath.sin(phi),
        			Z
        	};
        	
        	double u[] = Mat.normaliseVector(Mat.subtract(curPos, viewPos));
        	
        	double s = Algorithms.pointOnLineNearestAnotherLine(viewPos, u, beamGeom.start(iB), beamGeom.uVec(iB));
        	
        	double pos[] = Mat.add(viewPos, Mat.mul(u, s));
        	
        	double r = FastMath.sqrt(pos[0]*pos[0] + pos[1]*pos[1]);
        	phi = FastMath.atan2(pos[1], pos[0]);
        	
        	d = FastMath.sqrt(FastMath.pow2(R - r) + FastMath.pow2(Z - pos[2]));
        	
        	if(d < 1e-4)
        		return phi;
        }
       // System.err.println("WARNING: Didn't accurately find phi for R=" + R + ", Z=" + Z + "on beam "+(iB+1) + ". Delta = " + d);
        
        return phi;
	}
	
	
	/** Calculate a single set of the 'A factors' everyone else uses, for the given beam and observation position 
	 * 
	 * tan(polAngle) =   (A0*Br + A1*Bphi + A2*Bz + A3*ER/v + A4*EZ/v)
	 *                 / (A5*Br + A6*Bphi + A7*Bz + A8*ER/v + A9*EZ/v)
	 */
	public static double[] calcAFactors(double virtMeasPlane[][], double viewPos[], double obsPos[], SimpleBeamGeometry beamGeom, int beamIndex){
		
		
		double l[] = Mat.normaliseVector(Mat.subtract(obsPos, viewPos));	//line of sight
		double v[] = Mat.normaliseVector(beamGeom.uVec(beamIndex));	//beam
		
		// cylindrical unit vectors
		double R[] = Mat.normaliseVector(new double[]{ obsPos[0], obsPos[1], 0 }); //Radial
		double phi[] = Mat.normaliseVector(new double[]{ -obsPos[1], obsPos[0], 0 }); //Radial
		double Z[] = Mat.normaliseVector(new double[]{ 0, 0, 1 }); //vertical
				
		//the measurement plane
		double n[] = virtMeasPlane[0];
		double r[] = virtMeasPlane[1];
		double u[] = virtMeasPlane[2];
		
		//get polarisation projected components
		double cVxR[] = vectorCoefficients(l, n, u, r, Mat.cross3d(v, R));	// for BR
		double cVxP[] = vectorCoefficients(l, n, u, r, Mat.cross3d(v, phi)); // for Bphi
		double cVxZ[] = vectorCoefficients(l, n, u, r, Mat.cross3d(v, Z)); // for BZ
		double cR[] = vectorCoefficients(l, n, u, r, R); // for ER
		double cZ[] = vectorCoefficients(l, n, u, r, Z); // for EZ
		// added minus signs in front of the first 5 A factors because I couldn't figure out how to do 
		// it properly.... (aburckha)
		// oliford: presumably he has modified them to match the CLISTE-IDE ones, which is 
		// fine as it's pretty arbitrary anyway
		return new double[]{
				-cVxR[0], -cVxP[0], -cVxZ[0], -cR[0], -cZ[0],
				cVxR[1], cVxP[1], cVxZ[1], cR[1], cZ[1],
		};

	}
	
	/** Calculate amplitudes (Ar, Au) of polarisation arriving at a plane (n,u,r)
	 * from line of sight l, emitted by a dipole-like thing a
	 *  
	 * @return Ar, Au
	 */
	public static double[] vectorCoefficients(double l[], double n[], double u[], double r[], double a[]){
		
		//polarision vector on ray is the original, minus the part parallel to the ray
		//...but we're going to take dot products with s and p anyway, so this bit doesnt matter
		//double b[] = Mat.normaliseVector(Mat.subtract(a, Mat.mul(l, Mat.dot(a, l)))); // b = a - (a.l)l
		//double b[] = Mat.normaliseVector(a);
		//double b[] = Mat.subtract(a, Mat.mul(l, Mat.dot(a, l)));
		
		//calculate the s dir and p dirs on plane and ray
		double s[] = Mat.normaliseVector(Mat.cross3d(l, n));
		double pPlane[] = Mat.normaliseVector(Mat.cross3d(n, s));
		double pRay[] = Mat.normaliseVector(Mat.cross3d(l, s));
		
		//amplitude of (s,p) components on ray = (b.s), (b.pRay)
		double As = Mat.dot(a, s);
		double Ap = Mat.dot(a, pRay);

		//rebuild into vector using s and p in Plane
		double Au = As*Mat.dot(s, u) + Ap*Mat.dot(pPlane, u);
		double Ar = As*Mat.dot(s, r) + Ap*Mat.dot(pPlane, r);
	
		return new double[]{ Ar, Au };
	}
	
	private double[] ABtoRZ(double AB[], int beamIdx){
		
		double u[] = new double[]{
				FastMath.cos(AB[1]) * FastMath.cos(AB[0]),
				FastMath.cos(AB[1]) * FastMath.sin(AB[0]),
				FastMath.sin(AB[1]),		
		};

		double s = Algorithms.pointOnLineNearestAnotherLine(viewPos, u, 
								beamGeom.start(beamIdx),
								beamGeom.uVec(beamIdx));
			
		double intersectionPos[] = new double[]{
				viewPos[0] + s * u[0],
				viewPos[1] + s * u[1],
				viewPos[2] + s * u[2],
		};
		
		return new double[]{
				FastMath.sqrt(intersectionPos[0]*intersectionPos[0] +
								intersectionPos[1]*intersectionPos[1]),
				intersectionPos[2]
		};		
	}
	
	
	/* don't use this, it's awful
	 * private double[] RZtoAB(double RZ[]){
		double B[] = AugMSESystem.nbiStartAll[beamSelection];
		double v[] = AugMSESystem.nbiUnitAll[beamSelection];
		
		double t[] = Algorithms.cylinderLineIntersection(
				B, v, 
				new double[]{0, 0, 0}, 
				new double[]{0,0,1}, 
				RZ[0]*RZ[0]);
		
		double t0 = Math.min(t[0], t[1]);
		
		double p[] = new double[]{
				B[0] + t0 * v[0],
				B[1] + t0 * v[1],
				B[2] + t0 * v[2],
		};
		
		double phi = FastMath.atan2(p[1], p[0]);

        double u[] = null;
        
        for(int k=0; k < 100; k++){
        	double curPos[] = new double[]{
        			RZ[0] * FastMath.cos(phi),
        			RZ[0] * FastMath.sin(phi),
        			RZ[1]
        	};
        	
        	u = Mat.normaliseVector(Mat.subtract(curPos, cameraPos));
        	
        	double s = Algorithms.pointOnLineNearestAnotherLine(cameraPos, u, B, v);
        	
        	p = Util.plus(cameraPos, Util.mul(u, s));
        	
        	double r = FastMath.sqrt(p[0]*p[0] + p[1]*p[1]);
        	phi = FastMath.atan2(p[1], p[0]);
        	
        	double d = FastMath.sqrt(FastMath.pow2(RZ[0] - r) + FastMath.pow2(RZ[1] - p[2]));
        			
        	//System.out.println(k+ "\t" + phi*180/Math.PI +"\t" + d);
        	
        	if(d < 1e-4)
        		break;
        }
        
        p = Util.reNorm(Util.minus(p, cameraPos));
        
        return new double[]{ 
        	FastMath.atan2(p[1], p[0]),
        	FastMath.asin(p[2])
        };
	}*/
	
	/** Calculates the min/max of the output image space and then divides
	 * that up to make a 'reasonable' output image roughly the same
	 * resolution as the input one.
	 * AB here might be LL, or RZ if a beam is selected.
	 * @param full If true create an image out to the corners of the transformed original image (can be very large if tranform is very bendy)
	 * 				otherwise, uses min/max lat/lon or R,Z of points
	 */
	private void calcOutputImageAxes(boolean full){
		//def A,B of a pixel as A,B in top left corner of that pixel
		//double dAB = 0.001; //desired size of pixel
		//dA = dAB; //force isometric
		//dB = dAB;
		double pixelRatio = 1.5;
		
		double lon0,lat0,lon1,lat1;
		if(full){
			//use transform corners as a starting point
			//R1,Z1 is coord of top left corner of the top right pixel  
			lon0 = xform.getMinLon();
			lon1 = xform.getMaxLon(); 
			lat0 = xform.getMinLat();
			lat1 = xform.getMaxLat();
		}else{
			lon0 = Double.POSITIVE_INFINITY;
			lon1 = Double.NEGATIVE_INFINITY;
			lat0 = Double.POSITIVE_INFINITY;
			lat1 = Double.NEGATIVE_INFINITY;
			
			ArrayList<TransformPoint> points = xform.getPoints();
			synchronized (points) {
				for(TransformPoint point : points){
					if(!point.includeInFit())
						continue;
					if(point.lon < lon0) lon0 = point.lon;
					if(point.lon > lon1) lon1 = point.lon;
					if(point.lat < lat0) lat0 = point.lat;
					if(point.lat > lat1) lat1 = point.lat;
				}
			}
			
			double rangeLon = lon1 - lon0;
			lon0 -= rangeLon / 4;
			lon1 += rangeLon / 4;
			double rangeLat = lat1 - lat0;
			lat0 -= rangeLat / 4;
			lat1 += rangeLat / 4;
		} 
		
		if(beamSelection == BEAMSEL_LATLON){
			A0 = lon0; A1 = lon1;
			B0 = lat0; B1 = lat1;
		}else{
			
			//look for specified minRZ and maxRZ
			TransformPoint minRZ = null, maxRZ = null;
			for(TransformPoint p : xform.getPoints()){
				if(p.name.equals("minRZ"))
					minRZ = p;
				else if(p.name.equals("maxRZ"))
					maxRZ = p;
			}
			
			//use user specified override
			if(minRZ != null && maxRZ != null){
				A0 = minRZ.R; A1 = maxRZ.R;
				B0 = minRZ.Z; B1 = maxRZ.Z;
				pixelRatio = minRZ.x; //erm... hackery
				
			}else{ //other wise calculate from min/max of the selected beam (or all of them if it's on auto)
				
				//one, or possible all beams. If automatic, do min/max of all
				A0 = Double.POSITIVE_INFINITY; A1 = Double.NEGATIVE_INFINITY;
				B0 = Double.POSITIVE_INFINITY; B1 = Double.NEGATIVE_INFINITY;
				
				boolean isBox2 = xform.getViewPosition()[1] > 0;
				
				for(int iB=0; iB < 8;  iB++){
					if(beamSelection == iB || (beamSelection == BEAMSEL_AUTO && ((!isBox2 && iB < 4) || (isBox2 && iB >= 4)))){
						
						//if we're going all the way to R,Z on a particular beam....
						double RZ0[] = ABtoRZ(new double[]{ lon0, lat0 }, iB);
						double RZ1[] = ABtoRZ(new double[]{ lon0, lat1 }, iB);
						double RZ2[] = ABtoRZ(new double[]{ lon1, lat0 }, iB);
						double RZ3[] = ABtoRZ(new double[]{ lon1, lat1 }, iB);
						A0 = Math.min(A0, Math.min(Math.min(RZ0[0], RZ1[0]), Math.min(RZ2[0], RZ3[0])));
						A1 = Math.max(A1, Math.max(Math.max(RZ0[0], RZ1[0]), Math.max(RZ2[0], RZ3[0])));
						B0 = Math.min(B0, Math.min(Math.min(RZ0[1], RZ1[1]), Math.min(RZ2[1], RZ3[1])));
						B1 = Math.max(B1, Math.max(Math.max(RZ0[1], RZ1[1]), Math.max(RZ2[1], RZ3[1])));
					}
				}
			}
		}
		
		//we want roughly pixelRatio pixels in output image for each pixel in input image
		//output range = (A1 - A0)
		//input range = inWidth
		dA = (A1 - A0) / (inWidth*pixelRatio);
		dB = (B1 - B0) / (inHeight*pixelRatio);
		dA = (dA + dB) / 2; //average and make isometric
		dB = dA;
		
		//image size ought to be 32-bit aligned, for some reason
		outWidth = 4*(int)(((A1 - A0) / dA + 1) / 4);
		outHeight = 4*(int)(((B1 - B0) / dB + 1) / 4);
		A1 = A0 + (outWidth-1) * dA;
		B1 = B0 + (outHeight-1) * dB;
		
		if(Double.isNaN(A0) || Double.isNaN(A1) || Double.isNaN(B0) || Double.isNaN(B1)){
			throw new RuntimeException("TransformProcessor:  ERROR: Output image space is Nan");
		}
		
		//make axes and write that to the metadata
		double A[] = new double[outWidth];
		for(int i=0; i < outWidth; i++){
			A[i] = A0 + i*dA;
		}
		double B[] = new double[outHeight];
		for(int i=0; i < outHeight; i++){
			B[i] = B1 - i*dB;
		}
		
		if(beamSelection == BEAMSEL_LATLON){
			//all of these are only relevant for the output image
			setSeriesMetaData("/Transform/imageOutR", new double[]{ Double.NaN }, false);
			setSeriesMetaData("/Transform/imageOutZ", new double[]{ Double.NaN }, false);
			setSeriesMetaData("/R", new double[]{ Double.NaN }, false);
			setSeriesMetaData("/Z", new double[]{ Double.NaN }, false);
			for(int i=0; i < outWidth; i++){ A[i] *= 180 / Math.PI; };
			for(int i=0; i < outHeight; i++){ B[i] *= 180 / Math.PI; };
			setSeriesMetaData("/Transform/imageOutLon", A, false);
			setSeriesMetaData("/Transform/imageOutLat", B, false);
		}else{
			
			setSeriesMetaData("/Transform/imageOutR", A, false);
			setSeriesMetaData("/Transform/imageOutZ", B, false);
			setSeriesMetaData("/R", A, false); //simple form
			setSeriesMetaData("/Z", B, false);
			setSeriesMetaData("/Transform/imageOutLon", new double[]{ Double.NaN }, false);
			setSeriesMetaData("/Transform/imageOutLat", new double[]{ Double.NaN }, false);
		}
	}
	

	@Override
	public void notifySourceChanged() {
		super.notifySourceChanged();
		if(autoCalc)
			calc();
	}

	@Override
	public void imageChangedRangeComplete(int idx) { 
 		if(autoCalc)
			calc();
 	}
	
	@Override
	public int getNumImages() {
		return (imagesOut != null) ? imagesOut.length : 0;
	}

	@Override
	public Img getImage(int imgIdx) {
		return (imgIdx >= 0 && imgIdx < imagesOut.length) ? imagesOut[imgIdx] : null;
	}
	
	@Override
	public Img[] getImageSet() { return imagesOut; }

	@Override
	public TransformProcessor clone() { return new TransformProcessor(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			TransformSWTController controller = new TransformSWTController((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	public double getA0(){ return A0; }
	public double getA1(){ return A1; }
	public double getB0(){ return B0; }
	public double getB1(){ return B1; }
		
	public void setAutoUpdate(boolean autoCalc) {
		if(!this.autoCalc && autoCalc){
			this.autoCalc = true;
			updateAllControllers();
			calc();			
		}else{
			this.autoCalc = autoCalc;
			updateAllControllers();
		}
	}
	public boolean getAutoUpdate() { return this.autoCalc; }

	@Override
	public void setSource(ImgSource source) {
		super.setSource(source);
		if(autoCalc)
			calc();
	}
	
	@Override
	public void destroy() {
	    abortCalc = true;
		super.destroy();
	}
	
	public FeatureTransform getTransform(){ return xform; }
	public void pointsMapModified(){
		settingsChanged = true;
		updateAllControllers();
		if(autoCalc)
			calc();
	}

	@Override
	public void saveConfig(String id){
		if(id.startsWith("gmds")){
			String parts[] = id.split("/");
			saveConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
			lastLoadedConfig = id;
		}else if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}		
	}
	
	public void saveConfigJSON(String fileName){

		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.setPrettyPrinting()
				.create(); 
		
		String jsonText = gson.toJson(xform);
		
		OneLiners.textToFile(fileName, jsonText);
	}
	
	public void saveConfigGMDS(String experiment, int pulse){
		if(connectedSource == null) return;
		
		if(experiment ==null)
			experiment = GMDSUtil.getMetaExp(connectedSource);
		if(pulse < 0)
			pulse = GMDSUtil.getMetaDatabaseID(connectedSource);
		
		saveTransformSettings(experiment, pulse);		
	}
	
	public void saveTransformSettings(String experiment, int pulse){
		
		try{
			xform.saveToSignal(GMDSUtil.globalGMDS(), experiment, pulse);
			
		}catch(RuntimeException e){
			System.err.println("Transform: Error saving points to exp " + experiment + 
								", pulse " + pulse + " because: " + e);
		}
		
		try{
			ccdGeom.saveToSignal(GMDSUtil.globalGMDS(), experiment, pulse);			
		}catch(RuntimeException e){
			System.err.println("Transform: Error saving CCD/ROI geometry to exp " + experiment + 
								", pulse " + pulse + " because: " + e);
		}
	}

	@Override
	public void loadConfig(String id){
		
		if(id.startsWith("gmds")){
			String parts[] = id.split("/");
			loadConfigGMDS(parts[1].length() > 0 ? parts[1] : null, Algorithms.mustParseInt(parts[2]));
		}else if(id.startsWith("jsonfile:")){
			loadConfigJSON(id.substring(9));
		}else
			throw new RuntimeException("Unknown config type " + id);
		
		lastLoadedConfig = id;
		
		//fix stupid coords
		if(id.contains("_IDIOT_COORDS")){
			for(TransformPoint point : xform.getPoints()){
				double rotAng = -(90 + 22.5) * Math.PI / 180;		
				double trueX = point.x * FastMath.cos(rotAng) + point.y * FastMath.sin(rotAng);						
				double trueY = -point.x * FastMath.sin(rotAng) + point.y * FastMath.cos(rotAng);
				point.x = trueX;
				point.y = trueY;
			}
		}
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
			
		xform = gson.fromJson(jsonString, FeatureTransformCubic.class);
			
		updateAllControllers();
	}
	
	public void loadConfigGMDS(String experiment, int overridePulse) {		
		if(experiment == null || experiment.length() <= 0){
			experiment = GMDSUtil.getMetaExp(connectedSource);
		}
		
		int pulse ;
		try{
			pulse = overridePulse >= 0 ? overridePulse : GMDSUtil.getMetaDatabaseID(connectedSource);
		}catch(RuntimeException e){
			pulse = 0;
		}
		
		//try reading from this specific pulse first
		try{			
			xform.loadPoints(GMDSUtil.globalGMDS(), experiment, pulse);
			for(TransformPoint p : xform.getPoints()){
				if(p.name.equals("<new>")){ //can't have this, it messes up the GUI
					p.name = "{new}";
				}
				
			}
			
		}catch(RuntimeException err){
			System.err.println("Transform: Couldn't load points for exp " + experiment +
					", pulse " + pulse + " because: "+ err + ".");
		}
		
		try{
			// and load the cublic X/Y grid size, and 'full image' setting
			GMDSSignalDesc sigDesc = new GMDSSignalDesc(pulse, experiment, "Transform/cubicKnotsX");		
			GMDSSignal sig = (GMDSSignal)GMDSUtil.globalGMDS().getSig(sigDesc);
			double cubicKnotsX[][] = (double[][])sig.getDataAsType(double.class);
			cubicNX = cubicKnotsX.length;
			cubicNY = cubicKnotsX[0].length;
		}catch(RuntimeException e3){
			cubicNX = -1;
			cubicNY = -1;
		}
				
		//load the transform too, in case they want to use it without fitting again
		try{
			xform.loadLinear(GMDSUtil.globalGMDS(), experiment, pulse);
			xform.loadKnots(GMDSUtil.globalGMDS(), experiment, pulse);
			
		}catch(RuntimeException err){
			System.err.println("Transform: Couldn't load transform fit for exp " + experiment +
					", pulse " + pulse + " because: "+ err + ". Might still work by re-fitting.");
		}

		updateAllControllers();
		if(autoCalc)
			calc();
		
	}
	
	public HashMap<TransformPoint, double[]> getBackConvertedXY(){ return backConvertedXY; }

	public void invalidate() {
		settingsChanged = true;
	}

	/* beamSelection: -2=auto, -1=lat/lon, 0-7 = Q1-8 */
	public void setBeamSelection(int beamSelection) {
		this.beamSelection = beamSelection;
		settingsChanged = true;
		if(autoCalc)
			calc();
		updateAllControllers();
	}

	public void setCubicInterp(boolean enable, int nKnotsX, int nKnotsY) {
		if(enable){
			cubicNX = nKnotsX;
			cubicNY = nKnotsY;
		}else{
			cubicNX = -1;
			cubicNY = -1;
		}
		settingsChanged = true;
		if(autoCalc)
			calc();		
		updateAllControllers();
	}

	public int getCubicNX() { return this.cubicNX; }
	public int getCubicNY() { return this.cubicNY; }
	
	/** Work out the corners of the image in physical CCD space, from the camera MetaData */
	private void calcCCDGeom(){
		
		if(ccdGeom == null)
			ccdGeom = new PhysicalROIGeometry();

		//build a simple map containing all the necessary metadata and give it to ccdGeom to work out the CCD and ROI geometry
		
		HashMap<String, Object> cameraMetaData = new HashMap<String, Object>();
		for(String metaName : PhysicalROIGeometry.cameraMetadataNames){
			Object o = connectedSource.getSeriesMetaData(metaName);
			if(o == null)
				o = connectedSource.getSeriesMetaData("/" + metaName);
			
			if(o != null && o.getClass().isArray()){
				o = Array.get(o, 0);
			}
			
			cameraMetaData.put(metaName, o);
		}
		
		ccdGeom.setFromCameraMetadata(cameraMetaData);
		ccdGeom.setImageSize(inWidth, inHeight);
	}
	
	/** @return Approximate circles in CCD space fitting through 
	 *               limit points with similar names double[idx][z/y/R] */
	public double[][] getLimitCircles(){
		HashMap<String, ArrayList<TransformPoint>> circData = new HashMap<String, ArrayList<TransformPoint>>();
		
		//match first 5 letters
		for(TransformPoint p : xform.getPoints()){
			if(p.name.length() < 5 || !p.name.contains("circle"))
				continue;
			String matchStr = p.name.substring(0, 5);
			ArrayList<TransformPoint> pointsInCirc = circData.get(matchStr);
			if(pointsInCirc != null){
				pointsInCirc.add(p);
			}else{
				pointsInCirc = new ArrayList<TransformPoint>();
				pointsInCirc.add(p);
				circData.put(matchStr, pointsInCirc);
			}
		}

		ArrayList<double[]> circles = new ArrayList<double[]>();
		
		for(Entry<String, ArrayList<TransformPoint>> entry : circData.entrySet()){
			ArrayList<TransformPoint> pointsInCirc = entry.getValue();
			int n = pointsInCirc.size();
			
			//need at least 5 points
			if(n > 5){
				double pos[] = new double[2];
				for(TransformPoint p : pointsInCirc){
					pos[0] += p.imgX;
					pos[1] += p.imgY;
				}
				pos[0] /= n; 
				pos[1] /= n;
				
				double R = 0;
				for(TransformPoint p : pointsInCirc){
					R += FastMath.sqrt(FastMath.pow2(p.imgX - pos[0]) + FastMath.pow2(p.imgY - pos[1]));
				}
				R /= n;
				
				circles.add(new double[]{ pos[0], pos[1], R });
			}
		}
		
		return circles.toArray(new double[0][]);
		
	}

	@Override
	public BulkImageAllocation getBulkAlloc() { return bulkAlloc; }

	public PhysicalROIGeometry getCCDGeometry() { return ccdGeom; }

	public void setTransformFullImage(boolean fullImage) { 
		transformFullImage = fullImage; 
		settingsChanged = true;
		if(autoCalc)
			calc();
	}
	
	public boolean getTransformFullImage(){
		return transformFullImage;
	}
	
	public boolean getDoTransformFit(){ return fitTransform; }
	public void setDoTransformFit(boolean enable){ this.fitTransform = enable; }

	public int getBeamSelection() { return beamSelection; }
	
	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "Transform[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}

	/** Dumps unit vectors in 3D of each pixel in camera image */
	private String generateLOSVectorsSync = new String("generateLOSVectorsSync");
	public void generateLOSVectors() {
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doGenerateLOSVectors(); } });
	}
	
	/** Dumps unit vectors in 3D of each pixel in camera image */
	private void doGenerateLOSVectors() {
		abortCalc = false;		
		status = "Generating LOS vectors...";
		updateAllControllers();
		
		boolean runningFit = false;
		
		double pixelVectors[][][] = new double[ccdGeom.getImgPixelH()][ccdGeom.getImgPixelW()][3];
		
		double dX = (ccdGeom.getImgPhysX1() - ccdGeom.getImgPhysX0()) / ccdGeom.getImgPixelW();
		double dY = (ccdGeom.getImgPhysY1() - ccdGeom.getImgPhysY0()) / ccdGeom.getImgPixelH();
		double LL[] = new double[]{ Double.NaN, Double.NaN };
		for(int ipy = 0; ipy < ccdGeom.getImgPixelH(); ipy++){
			for(int ipx = 0; ipx < ccdGeom.getImgPixelW(); ipx++){
				if(abortCalc){
					status = "Aborted generating LOS vectors";
					return;
				}
				
				LL = xform.xyToLatLon(ccdGeom.getImgPhysX0() + ipx * dX, 
										ccdGeom.getImgPhysY0() + ipy * dY,
										runningFit ? LL[0] : Double.NaN, LL[1]);
				
				if(!Double.isNaN(LL[0]) && !Double.isNaN(LL[1])){
					
			        pixelVectors[ipy][ipx] = new double[]{
							FastMath.cos(LL[1]) * FastMath.cos(LL[0]),
							FastMath.cos(LL[1]) * FastMath.sin(LL[0]),
							FastMath.sin(LL[1]),
					};

				}else{					
					pixelVectors[ipy][ipx] = new double[]{ Double.NaN, Double.NaN, Double.NaN };
				}
			}

			status = "Generating LOS vectors (" + ipy + " / " + ccdGeom.getImgPixelH() + ")...";
			updateAllControllers();
		}
		
		String outPath = System.getProperty("java.io.tmpdir") + "/pixelVectors/";

		int nY = pixelVectors.length;
		int nX = pixelVectors[0].length;
		
		double uX[][] = new double[nY][nX];
		double uY[][] = new double[nY][nX];
		double uZ[][] = new double[nY][nX];
		
		for(int iY=0; iY < nY; iY++){
			for(int iX=0; iX < nX; iX++){
				uX[iY][iX] = pixelVectors[iY][iX][0];
				uY[iY][iX] = pixelVectors[iY][iX][1];
				uZ[iY][iX] = pixelVectors[iY][iX][2];
			}		
		}

		status = "Writing uX.txt..."; updateAllControllers(); Mat.mustWriteAscii(outPath + "/uX.txt", uX);
		status = "Writing uY.txt..."; updateAllControllers(); Mat.mustWriteAscii(outPath + "/uY.txt", uY);
		status = "Writing uZ.txt..."; updateAllControllers(); Mat.mustWriteAscii(outPath + "/uZ.txt", uZ);
		Mat.mustWriteAscii(outPath + "/cameraPos.txt", xform.getViewPosition());
		
		status = "Completed writing LOS vectors to " + outPath;
		updateAllControllers();
	}	
}
