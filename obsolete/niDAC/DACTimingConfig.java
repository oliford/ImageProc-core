package imageProc.control.niDAC;

import java.util.HashMap;
import java.util.Map;

/** Runtime config and static defines for stepper, camera and FLC drive sequence */
public class DACTimingConfig {
	
	
	/** Digital I/O Pin configuration */
	public static final int CH_CAM_TRIG = 0;
	public static final int CH_FLC_DRIVE = 1;
	
	public static final int CH_STEP[] = new int[]{ 2, 5 };
	public static final int CH_DIRECTION[] = new int[]{ 3, 6 };
	public static final int CH_SWITCH[] = new int[]{ 4, 7 };
	
	/** FLC drive line low for entire scan */
	public static final int FLC_LOW = 0;
	/** FLC drive line high for entire scan */
	public static final int FLC_HIGH = 1;
	/** One exposure low and one exposure high in each frame, before stepping */
	public static final int FLC_INTERLACE = 2;

	public int trigSource = -1; // -1 for software
	
	/** Clock period in NS. This needs to be smaller than the smallest period */
	public long clockPeriodNS;
	
	/** Number of frames per cycle. If flcMode == FLC_INTERLACE then this is half the number of exposures. */
	public int nFramesPerCycle;
	
	/** Number of cycles, nFrames should be divisible by this */
	public int nCycles;
	
	/** Full frame period in ns */
	public long framePeriodNS;
	
	/** Time in ns between raise and fall of camera exposure */
	public long exposeHighPeriodNS;
	
	/** Time in ns between fall of camera exposure and doing something else */
	public long exposeLowPeriodNS;
	
	/** Stepper to use */ 
	public int frameStepperSelect, cycleStepperSelect;
	
	/** Number of steps of each stepper to do after each frame, or each cycle */
	public int nStepsPerFrame, nStepsPerCycle;
	
	/** Period of stepper steps, in ns */
	public int stepperPeriodNS;
		
	/** Do one exposure in each FLC mode before stepping */
	public int flcMode = FLC_LOW;

	/** Starting direction for frame and cycle steppers */
	public boolean frameStepReverse, cycleStepReverse;
	
	/** Flip the direction of the frame stepper on each cycle */
	public boolean alternateFrameStepDir;
	
	/** H/W Start trigger Source */
	public int trigSourcePFI = -1; // -1 for software
	
	public Map<String, Object> toMap(String prefix) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put(prefix + "/clockPeriodNS", clockPeriodNS);
		map.put(prefix + "/nFramesPerCycle", nFramesPerCycle);
		map.put(prefix + "/nCycles", nCycles);
		map.put(prefix + "/framePeriodNS", framePeriodNS);
		map.put(prefix + "/exposeHighPeriodNS", exposeHighPeriodNS);
		map.put(prefix + "/exposeLowPeriodNS", exposeLowPeriodNS);
		map.put(prefix + "/frameStepperSelect", frameStepperSelect);
		map.put(prefix + "/cycleStepperSelect", cycleStepperSelect);
		map.put(prefix + "/nStepsPerFrame", nStepsPerFrame);
		map.put(prefix + "/nStepsPerCycle", nStepsPerCycle);
		map.put(prefix + "/stepperPeriodNS", stepperPeriodNS);
		map.put(prefix + "/flcMode", flcMode);
		map.put(prefix + "/frameStepReverse", frameStepReverse);
		map.put(prefix + "/cycleStepReverse", cycleStepReverse);
		map.put(prefix + "/alternateFrameStepDir", alternateFrameStepDir);
		map.put(prefix + "/trigSourcePFI", trigSourcePFI);
		
		return map;
	}
		
}
