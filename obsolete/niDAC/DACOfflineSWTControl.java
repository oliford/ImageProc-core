package imageProc.control.niDAC;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

public class DACOfflineSWTControl {
	
	public Group swtGroup;
	
	private Button stepper1Button;
	private Button stepper2Button;
	private Button reverseButton;
	private Button stopHomeButton;
	private Spinner nStepsSpinner;
	private Spinner periodUSSpinner;
	
	private Button startButton;
	private Button stopButton;
	
	private Button cameraSnapButton;
	private Button flcLowButton;
	private Button flcHighButton;
	
	private OfflineDACDrive activeDrive;
	
	public DACOfflineSWTControl(Composite parent) {
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Offline Motor Control");
		swtGroup.setLayout(new GridLayout(6, false));

		Label lNS = new Label(swtGroup, SWT.NONE); lNS.setText("Num Steps:");
		nStepsSpinner = new Spinner(swtGroup, SWT.NONE);
		nStepsSpinner.setValues(36000, -1, Integer.MAX_VALUE, 0, 100, 1000);
		nStepsSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		stepper1Button = new Button(swtGroup, SWT.CHECK);
		stepper1Button.setText("Stepper 1");
		stepper1Button.setSelection(true);
		stepper1Button.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		stepper2Button = new Button(swtGroup, SWT.CHECK);
		stepper2Button.setText("Stepper 2");
		stepper2Button.setSelection(true);
		stepper2Button.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		reverseButton = new Button(swtGroup, SWT.CHECK);
		reverseButton.setText("Reverse");
		reverseButton.setSelection(true);
		reverseButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		stopHomeButton = new Button(swtGroup, SWT.CHECK);
		stopHomeButton.setText("Home stop");
		stopHomeButton.setSelection(true);
		stopHomeButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Period/μs:");
		periodUSSpinner = new Spinner(swtGroup, SWT.NONE);
		periodUSSpinner.setValues(1500, 0, Integer.MAX_VALUE, 0, 1, 1000);
    	periodUSSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
    	
		startButton = new Button(swtGroup, SWT.PUSH);
		startButton.setText("Start");
		startButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startButtonEvent(event); } });   

		stopButton = new Button(swtGroup, SWT.PUSH);
		stopButton.setText("Stop");
		stopButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		stopButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { stopButtonEvent(event); } });   

		 
        cameraSnapButton = new Button(swtGroup, SWT.PUSH);
        cameraSnapButton.setText("Camera Snap");
        cameraSnapButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { cameraSnapButtonEvent(event); } });        
        cameraSnapButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
        flcLowButton = new Button(swtGroup, SWT.PUSH);
        flcLowButton.setText("FLC Low");
        flcLowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { flcButtonEvent(event, false); } });        
        flcLowButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
        flcHighButton = new Button(swtGroup, SWT.PUSH);
        flcHighButton.setText("FLC High");
        flcHighButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { flcButtonEvent(event, true); } });        
        flcHighButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
	}
	
	private void cameraSnapButtonEvent(Event event) {
		OfflineDACDrive.cameraTrig(10);
	}
	
	private void flcButtonEvent(Event event, boolean high) {
		OfflineDACDrive.setFLC(high);
	}
	
	private void startButtonEvent(Event event) {
		if(activeDrive != null)
			activeDrive.abort();			
		
		boolean en1 = stepper1Button.getSelection();
		boolean en2 = stepper2Button.getSelection();
		if(!en1 & !en2)
			return; // nothing to do
		
		int stepperSelect = en1 ? (en2 ? 3 : 0) : 1; 
		int nSteps = nStepsSpinner.getSelection();
		int periodNS = periodUSSpinner.getSelection() * 1000;
		boolean reverse = reverseButton.getSelection();
		boolean homeStop = stopHomeButton.getSelection();
		
		activeDrive = new OfflineDACDrive(stepperSelect, nSteps, homeStop, periodNS, reverse);

	}
	
	private void stopButtonEvent(Event event) {
		if(activeDrive != null)
			activeDrive.abort();	
	}
}
