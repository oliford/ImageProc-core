/*      */ package com.oceanoptics.omnidriver.api.wrapper;
/*      */ 
/*      */ import com.oceanoptics.highrestiming.HighResTimeStamp;
/*      */ import com.oceanoptics.omnidriver.features.analogin.AnalogIn;
/*      */ import com.oceanoptics.omnidriver.features.analogin.AnalogInImpl;
/*      */ import com.oceanoptics.omnidriver.features.analogout.AnalogOut;
/*      */ import com.oceanoptics.omnidriver.features.analogout.AnalogOutImpl;
/*      */ import com.oceanoptics.omnidriver.features.boardtemperature.BoardTemperature;
/*      */ import com.oceanoptics.omnidriver.features.boardtemperature.BoardTemperatureImpl;
/*      */ import com.oceanoptics.omnidriver.features.buffer.DataBuffer;
/*      */ import com.oceanoptics.omnidriver.features.buffer.DataBufferImpl;
/*      */ import com.oceanoptics.omnidriver.features.continuousstrobe.ContinuousStrobe;
/*      */ import com.oceanoptics.omnidriver.features.continuousstrobe.ContinuousStrobeImpl;
/*      */ import com.oceanoptics.omnidriver.features.currentout.CurrentOut;
/*      */ import com.oceanoptics.omnidriver.features.currentout.CurrentOutImpl_LS450;
/*      */ import com.oceanoptics.omnidriver.features.detectortemperature.DetectorTemperature;
/*      */ import com.oceanoptics.omnidriver.features.detectortemperature.DetectorTemperatureImpl;
/*      */ import com.oceanoptics.omnidriver.features.externaltemperature.ExternalTemperature;
/*      */ import com.oceanoptics.omnidriver.features.externaltemperature.ExternalTemperatureWrapper;
/*      */ import com.oceanoptics.omnidriver.features.externaltriggerdelay.ExternalTriggerDelay;
/*      */ import com.oceanoptics.omnidriver.features.externaltriggerdelay.ExternalTriggerDelayImpl;
/*      */ import com.oceanoptics.omnidriver.features.gpio.GPIO;
/*      */ import com.oceanoptics.omnidriver.features.gpio.GPIOImpl;
/*      */ import com.oceanoptics.omnidriver.features.hardwaretrigger.HardwareTrigger;
/*      */ import com.oceanoptics.omnidriver.features.hardwaretrigger.HardwareTriggerImpl;
/*      */ import com.oceanoptics.omnidriver.features.highgainmode.HighGainMode;
/*      */ import com.oceanoptics.omnidriver.features.highgainmode.HighGainModeImpl;
/*      */ import com.oceanoptics.omnidriver.features.i2cbus.I2CBus;
/*      */ import com.oceanoptics.omnidriver.features.i2cbus.I2CBusImpl;
/*      */ import com.oceanoptics.omnidriver.features.indy.Indy;
/*      */ import com.oceanoptics.omnidriver.features.indy.IndyImpl;
/*      */ import com.oceanoptics.omnidriver.features.internaltrigger.InternalTrigger;
/*      */ import com.oceanoptics.omnidriver.features.internaltrigger.InternalTriggerImpl_Jaz;
/*      */ import com.oceanoptics.omnidriver.features.internaltrigger.InternalTriggerWrapper;
/*      */ import com.oceanoptics.omnidriver.features.irradiancecalibrationfactor.IrradianceCalibrationFactor;
/*      */ import com.oceanoptics.omnidriver.features.irradiancecalibrationfactor.IrradianceCalibrationFactorImpl;
/*      */ import com.oceanoptics.omnidriver.features.lightsource.LightSource;
/*      */ import com.oceanoptics.omnidriver.features.lightsource.LightSourceImpl;
/*      */ import com.oceanoptics.omnidriver.features.ls450functions.LS450_Functions;
/*      */ import com.oceanoptics.omnidriver.features.ls450functions.LS450_FunctionsImpl;
/*      */ import com.oceanoptics.omnidriver.features.nonlinearitycorrection.NonlinearityCorrectionImpl;
/*      */ import com.oceanoptics.omnidriver.features.nonlinearitycorrection.NonlinearityCorrectionProvider;
/*      */ import com.oceanoptics.omnidriver.features.pluginprovider.PlugInProviderImpl;
/*      */ import com.oceanoptics.omnidriver.features.resolution.PixelBinning;
/*      */ import com.oceanoptics.omnidriver.features.resolution.PixelBinningImpl_STSBase;
/*      */ import com.oceanoptics.omnidriver.features.resolution.PixelBinningWrapper;
/*      */ import com.oceanoptics.omnidriver.features.saturationthreshold.SaturationThreshold;
/*      */ import com.oceanoptics.omnidriver.features.saturationthreshold.SaturationThresholdImpl;
/*      */ import com.oceanoptics.omnidriver.features.singlestrobe.SingleStrobe;
/*      */ import com.oceanoptics.omnidriver.features.singlestrobe.SingleStrobeImpl;
/*      */ import com.oceanoptics.omnidriver.features.spectrumtype.SpectrumTypeSelect;
/*      */ import com.oceanoptics.omnidriver.features.spectrumtype.SpectrumTypeSelectImpl_STSBase;
/*      */ import com.oceanoptics.omnidriver.features.spectrumtype.SpectrumTypeWrapper;
/*      */ import com.oceanoptics.omnidriver.features.spibus.SPIBus;
/*      */ import com.oceanoptics.omnidriver.features.spibus.SPIBusImpl;
/*      */ import com.oceanoptics.omnidriver.features.straylightcorrection.StrayLightCorrection;
/*      */ import com.oceanoptics.omnidriver.features.straylightcorrection.StrayLightCorrectionImpl;
/*      */ import com.oceanoptics.omnidriver.features.thermoelectric.ThermoElectric;
/*      */ import com.oceanoptics.omnidriver.features.thermoelectric.ThermoElectricImpl;
/*      */ import com.oceanoptics.omnidriver.features.thermoelectric.ThermoElectricWrapper;
/*      */ import com.oceanoptics.omnidriver.features.uvvislightsource.UV_VIS_LightSource;
/*      */ import com.oceanoptics.omnidriver.features.version.Version;
/*      */ import com.oceanoptics.omnidriver.features.version.VersionImpl;
/*      */ import com.oceanoptics.omnidriver.features.wavelengthcalibration.WavelengthCalibrationImpl;
/*      */ import com.oceanoptics.omnidriver.features.wavelengthcalibration.WavelengthCalibrationProvider;
/*      */ import com.oceanoptics.omnidriver.gramsspc.GramsSPC;
/*      */ import com.oceanoptics.omnidriver.plugin.SpectrometerPlugIn;
/*      */ import com.oceanoptics.omnidriver.spectra.AnnotatedSpectrum;
/*      */ import com.oceanoptics.omnidriver.spectra.Spectrum;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Bench;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Coefficients;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Configuration;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Detector;
/*      */ import com.oceanoptics.omnidriver.spectrometer.NetworkSpectrometer;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Spectrometer;
/*      */ import com.oceanoptics.omnidriver.spectrometer.SpectrometerAssembly;
/*      */ import com.oceanoptics.omnidriver.spectrometer.SpectrometerChannel;
/*      */ import com.oceanoptics.omnidriver.spectrometer.SpectrometerFactory;
/*      */ import com.oceanoptics.omnidriver.spectrometer.USBSpectrometer;
/*      */ import com.oceanoptics.omnidriver.spectrometer.jaz.JazNetwork;
/*      */ import com.oceanoptics.omnidriver.spectrometer.sts.Pancake;
/*      */ import com.oceanoptics.omnidriver.spectrometer.sts.STS;
/*      */ import com.oceanoptics.omnidriver.spectrometer.sts.STSBench;
/*      */ import com.oceanoptics.omnidriver.spectrometer.sts.STSConfigurationBase;
/*      */ import com.oceanoptics.spectralprocessing.SpectralProcessor;
/*      */ import com.oceanoptics.utilities.Jars;
/*      */ import java.io.File;
/*      */ import java.io.FileOutputStream;
/*      */ import java.io.IOException;
/*      */ import java.io.PrintStream;
/*      */ import java.io.PrintWriter;
/*      */ import java.io.StringWriter;
/*      */ import java.net.Socket;
/*      */ import java.net.UnknownHostException;
/*      */ import java.util.ArrayList;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class Wrapper
/*      */ {
/*      */   private static final String API_VERSION = "2.38";
/*      */   private static final int BUILD_NUMBER = 2380;
/*      */   int numberOfSpectraAcquired;
/*      */   boolean stopCollectingSpectra;
/*      */   private Spectrum[] spectrumBuffer;
/*      */   private boolean[] isSaturatedBuffer;
/*      */   HighResTimeStamp[] timeStampBuffer;
/*      */   boolean keyIsInserted;
/*      */   public Exception lastException;
/*      */   int privilegeLevel;
/*      */   boolean dragonIsAccessible;
/*      */   boolean rawSpectrumMode;
/*      */   ArrayList spectrometerAssemblyCollection;
/*      */   private WrapperExtensions wrapperExtensions;
/*      */   
/*      */   public Wrapper()
/*      */   {
/*  222 */     this.spectrometerAssemblyCollection = new ArrayList();
/*      */     
/*  224 */     this.keyIsInserted = false;
/*  225 */     this.privilegeLevel = 0;
/*  226 */     this.dragonIsAccessible = false;
/*  227 */     this.lastException = null;
/*  228 */     this.spectrumBuffer = null;
/*  229 */     this.isSaturatedBuffer = null;
/*  230 */     this.timeStampBuffer = null;
/*  231 */     this.numberOfSpectraAcquired = 0;
/*  232 */     this.rawSpectrumMode = false;
/*      */   }
/*      */   
/*      */   public WrapperExtensions getWrapperExtensions() {
/*  236 */     if (this.wrapperExtensions == null) {
/*  237 */       this.wrapperExtensions = new WrapperExtensions();
/*  238 */       this.wrapperExtensions.setWrapper(this);
/*      */     }
/*  240 */     return this.wrapperExtensions;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private AnnotatedSpectrum createAnnotatedSpectrum(int spectrometerIndex, int channelIndex, double[] pixels, String username)
/*      */   {
/*  255 */     HighResTimeStamp timestamp = new HighResTimeStamp();
/*  256 */     boolean rotationEnabled = false;
/*      */     
/*  258 */     AnnotatedSpectrum annotatedSpectrum = new AnnotatedSpectrum(pixels, getWavelengths(spectrometerIndex, channelIndex), isSaturated(spectrometerIndex, channelIndex), getIntegrationTime(spectrometerIndex, channelIndex), getStrobeEnable(spectrometerIndex, channelIndex) == 1, timestamp, getBoxcarWidth(spectrometerIndex, channelIndex), getScansToAverage(spectrometerIndex, channelIndex), getCorrectForElectricalDark(spectrometerIndex, channelIndex), getCorrectForDetectorNonlinearity(spectrometerIndex, channelIndex), getCorrectForStrayLight(spectrometerIndex, channelIndex), rotationEnabled, getSerialNumber(spectrometerIndex), channelIndex, username);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  275 */     return annotatedSpectrum;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean exportToGramsSPC(int spectrometerIndex, String outputPathname, double[] pixels, String username)
/*      */   {
/*  304 */     return exportToGramsSPC(spectrometerIndex, 0, outputPathname, pixels, username);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean exportToGramsSPC(int spectrometerIndex, int channelIndex, String outputPathname, double[] pixels, String username)
/*      */   {
/*  312 */     this.lastException = null;
/*      */     
/*  314 */     AnnotatedSpectrum annotatedSpectrum = createAnnotatedSpectrum(spectrometerIndex, channelIndex, pixels, username);
/*      */     try
/*      */     {
/*  317 */       File outputFile = new File(outputPathname);
/*  318 */       FileOutputStream outputStream = new FileOutputStream(outputFile);
/*      */       
/*  320 */       byte[] byteArray = GramsSPC.getSPCByteStream(annotatedSpectrum);
/*      */       
/*  322 */       outputStream.write(byteArray);
/*  323 */       outputStream.flush();
/*  324 */       outputStream.close();
/*      */     } catch (Exception exception) {
/*  326 */       this.lastException = exception;
/*  327 */       return false;
/*      */     }
/*  329 */     return true;
/*      */   }
/*      */   
/*      */   public String getApiVersion()
/*      */   {
/*  334 */     return "2.38";
/*      */   }
/*      */   
/*      */   public int getBuildNumber()
/*      */   {
/*  339 */     return 2380;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLastException()
/*      */   {
/*  348 */     if (this.lastException == null)
/*  349 */       return "no exception";
/*  350 */     return this.lastException.toString();
/*      */   }
/*      */   
/*      */   public String getLastExceptionStackTrace() {
/*  354 */     if (this.lastException == null)
/*  355 */       return "no exception";
/*  356 */     StringWriter stringWriter = new StringWriter();
/*  357 */     PrintWriter printWriter = new PrintWriter(stringWriter);
/*  358 */     this.lastException.printStackTrace(printWriter);
/*  359 */     return stringWriter.toString();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int openNetworkSpectrometer(String ipAddress)
/*      */   {
/*  382 */     this.lastException = null;
/*      */     
/*      */ 
/*  385 */     for (int index = 0; index < this.spectrometerAssemblyCollection.size(); index++) {
/*  386 */       Spectrometer spectrometer = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(index)).getSpectrometer();
/*  387 */       if (((spectrometer instanceof NetworkSpectrometer)) && 
/*  388 */         (ipAddress.compareTo(((NetworkSpectrometer)spectrometer).getAddress()) == 0))
/*      */       {
/*      */ 
/*  391 */         return index;
/*      */       }
/*      */     }
/*      */     
/*      */     Socket socket;
/*      */     try
/*      */     {
/*  398 */       socket = new Socket(ipAddress, 7654);
/*      */     } catch (UnknownHostException exception) {
/*  400 */       this.lastException = exception;
/*  401 */       return -1;
/*      */     } catch (IOException exception) {
/*  403 */       this.lastException = exception;
/*  404 */       return -1;
/*      */     }
/*      */     
/*      */ 
/*      */     try
/*      */     {
/*  410 */       socket.setSoTimeout(0);
/*  411 */       socket.setSoLinger(false, 1);
/*  412 */       JazNetwork jazNetwork = new JazNetwork(socket);
/*  413 */       this.spectrometerAssemblyCollection.add(new SpectrometerAssembly(jazNetwork));
/*      */     } catch (Exception exception) {
/*  415 */       this.lastException = exception;
/*      */       try {
/*  417 */         if (socket.isConnected()) {
/*  418 */           socket.close();
/*      */         }
/*      */       } catch (Exception exceptionToBeIgnored) {}
/*  421 */       return -1;
/*      */     }
/*      */     
/*      */ 
/*  425 */     return this.spectrometerAssemblyCollection.size() - 1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNetworkCommunicationParameters(int spectrometerIndex, int socketTimeoutMilliseconds, boolean checkForBytesAvailable, int spectrumReadThrottleMilliseconds, int spectrumReadRetryLimit)
/*      */   {
/*  462 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().setSocketTimeoutMilliseconds(socketTimeoutMilliseconds);
/*      */     
/*  464 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().setCheckForBytesAvailable(checkForBytesAvailable);
/*      */     
/*  466 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().setSpectrumReadThrottleMilliseconds(spectrumReadThrottleMilliseconds);
/*      */     
/*  468 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().setReadSpectrumRetryLimit(spectrumReadRetryLimit);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int openAllSpectrometers()
/*      */   {
/*      */     try
/*      */     {
/*  500 */       for (int index = 0; index < this.spectrometerAssemblyCollection.size(); index++) {
/*  501 */         SpectrometerAssembly spectrometerAssembly = (SpectrometerAssembly)this.spectrometerAssemblyCollection.get(index);
/*  502 */         Spectrometer spectrometer = spectrometerAssembly.getSpectrometer();
/*  503 */         if (!spectrometer.isNetworkSpectrometer()) {
/*  504 */           return this.spectrometerAssemblyCollection.size();
/*      */         }
/*      */       }
/*      */       
/*  508 */       Spectrometer[] spectrometerArray = SpectrometerFactory.getAllSpectrometers();
/*  509 */       for (int index = 0; index < spectrometerArray.length; index++)
/*      */       {
/*  511 */         this.spectrometerAssemblyCollection.add(new SpectrometerAssembly(spectrometerArray[index]));
/*      */       }
/*  513 */       return this.spectrometerAssemblyCollection.size();
/*      */     } catch (Exception ee) {
/*  515 */       this.lastException = ee; }
/*  516 */     return -1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getName(int spectrometerIndex)
/*      */   {
/*  526 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getName();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getMaximumIntegrationTime(int spectrometerIndex)
/*      */   {
/*  537 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getIntegrationTimeMaximum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getMinimumIntegrationTime(int spectrometerIndex)
/*      */   {
/*  548 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getIntegrationTimeMinimum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getMaximumIntensity(int spectrometerIndex)
/*      */   {
/*  558 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getMaxIntensity();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfSpectrometersFound()
/*      */   {
/*  569 */     return this.spectrometerAssemblyCollection.size();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFirmwareVersion(int spectrometerIndex)
/*      */   {
/*      */     String firmwareVersionString;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  585 */       firmwareVersionString = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getFirmwareVersion();
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*  590 */       firmwareVersionString = firmwareVersionString.substring(firmwareVersionString.indexOf(' ') + 1);
/*      */     } catch (IOException ee) {
/*  592 */       this.lastException = ee;
/*  593 */       return "I/O error";
/*      */     }
/*  595 */     return firmwareVersionString;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFirmwareModel(int spectrometerIndex)
/*      */   {
/*  608 */     String firmwareModelString = "";
/*      */     try
/*      */     {
/*  611 */       String[] firmwareVersion = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getFirmwareVersion().split(" ");
/*      */       
/*      */ 
/*  614 */       if (firmwareVersion.length > 0) {
/*  615 */         firmwareModelString = firmwareVersion[0];
/*      */       }
/*      */     } catch (IOException ee) {
/*  618 */       this.lastException = ee;
/*  619 */       return "I/O error";
/*      */     }
/*  621 */     return firmwareModelString;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSerialNumber(int spectrometerIndex)
/*      */   {
/*  634 */     USBSpectrometer usb = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     String str;
/*      */     try {
/*  637 */       str = usb.getSerialNumber();
/*      */     } catch (Exception ee) {
/*  639 */       this.lastException = ee;
/*  640 */       str = "Not Found";
/*      */     }
/*  642 */     return str;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfPixels(int spectrometerIndex)
/*      */   {
/*  652 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfPixels();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfPixels(int spectrometerIndex, int channelIndex)
/*      */   {
/*  664 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getChannels()[channelIndex].getNumberOfPixels();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfDarkPixels(int spectrometerIndex)
/*      */   {
/*  675 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfDarkPixels();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfDarkPixels(int spectrometerIndex, int channelIndex)
/*      */   {
/*  687 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getChannels()[channelIndex].getNumberOfDarkPixels();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public byte[] readRawUSB(int spectrometerIndex, int inEP, int length)
/*      */     throws IOException
/*      */   {
/*  702 */     byte[] buffer = new byte['Ȁ'];
/*      */     try {
/*  704 */       buffer = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().readRawUSB(inEP, length);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  710 */       return buffer;
/*      */     }
/*      */     catch (IOException ee)
/*      */     {
/*  706 */       	this.lastException = ee;
					byte[] arrayOfByte1 = buffer;
					return buffer; 
				} finally {}
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int writeRawUSB(int spectrometerIndex, int outEP, byte[] message, int length)
/*      */     throws IOException
/*      */   {
/*  726 */     int val = 0;
/*      */     try {
/*  728 */       val = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().writeRawUSB(outEP, message, length);
/*      */     }
/*      */     catch (IOException ee) {
/*  731 */       this.lastException = ee;
/*  732 */       return -1;
/*      */     }
/*  734 */     return val;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIntegrationTime(int spectrometerIndex, int usec)
/*      */   {
/*  744 */     setIntegrationTime(spectrometerIndex, 0, usec);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIntegrationTime(int spectrometerIndex, int channelIndex, int usec)
/*      */   {
/*  773 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setIntegrationTime(usec);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getIntegrationTime(int spectrometerIndex)
/*      */   {
/*  784 */     return getIntegrationTime(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getIntegrationTime(int spectrometerIndex, int channelIndex)
/*      */   {
/*  794 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).getIntegrationTime();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setScansToAverage(int spectrometerIndex, int numberOfScansToAverage)
/*      */   {
/*  806 */     setScansToAverage(spectrometerIndex, 0, numberOfScansToAverage);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setScansToAverage(int spectrometerIndex, int channelIndex, int numberOfScansToAverage)
/*      */   {
/*  817 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setScansToAverage(numberOfScansToAverage);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getScansToAverage(int spectrometerIndex)
/*      */   {
/*  827 */     return getScansToAverage(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getScansToAverage(int spectrometerIndex, int channelIndex)
/*      */   {
/*  836 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).getScansToAverage();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Bench getBench(int spectrometerIndex)
/*      */   {
/*      */     try
/*      */     {
/*  851 */       return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getConfiguration().getBench();
/*      */     }
/*      */     catch (IOException exception) {
/*  854 */       this.lastException = exception; }
/*  855 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Bench getBench(int spectrometerIndex, int channelIndex)
/*      */   {
/*  867 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getConfiguration(channelIndex).getBench();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Detector getDetector(int spectrometerIndex, int channelIndex)
/*      */   {
/*  879 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getConfiguration(channelIndex).getDetector();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean saveSTSConfiguration(int spectrometerIndex)
/*      */   {
/*  892 */     if (!this.keyIsInserted)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  897 */       return false;
/*      */     }
/*      */     
/*  900 */     Spectrometer s = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*  902 */     if ((s instanceof STS))
/*      */       try {
/*  904 */         STSConfigurationBase config = (STSConfigurationBase)((STS)s).getConfiguration();
/*  905 */         config.setConfigurationToSpectrometer();
/*  906 */         return true;
/*      */       } catch (IOException ioe) {
/*  908 */         this.lastException = ioe;
/*  909 */         return false;
/*      */       }
/*  911 */     if ((s instanceof Pancake)) {
/*      */       try {
/*  913 */         STSConfigurationBase config = (STSConfigurationBase)((Pancake)s).getConfiguration();
/*  914 */         config.setConfigurationToSpectrometer();
/*  915 */         return true;
/*      */       } catch (IOException ioe) {
/*  917 */         this.lastException = ioe;
/*  918 */         return false;
/*      */       }
/*      */     }
/*  921 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public STSBench getSTSBench(int spectrometerIndex)
/*      */   {
/*  932 */     Spectrometer s = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*  934 */     if ((s instanceof STS))
/*      */       try {
/*  936 */         STSConfigurationBase config = (STSConfigurationBase)((STS)s).getConfiguration();
/*  937 */         return (STSBench)config.getBench();
/*      */       } catch (IOException ioe) {
/*  939 */         this.lastException = ioe;
/*  940 */         return null;
/*      */       }
/*  942 */     if ((s instanceof Pancake)) {
/*      */       try {
/*  944 */         STSConfigurationBase config = (STSConfigurationBase)((Pancake)s).getConfiguration();
/*  945 */         return (STSBench)config.getBench();
/*      */       } catch (IOException ioe) {
/*  947 */         this.lastException = ioe;
/*  948 */         return null;
/*      */       }
/*      */     }
/*  951 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBoxcarWidth(int spectrometerIndex, int numberOfPixelsOnEitherSideOfCenter)
/*      */   {
/*  966 */     setBoxcarWidth(spectrometerIndex, 0, numberOfPixelsOnEitherSideOfCenter);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBoxcarWidth(int spectrometerIndex, int channelIndex, int numberOfPixelsOnEitherSideOfCenter)
/*      */   {
/*  982 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setSmoothingWindowSize(numberOfPixelsOnEitherSideOfCenter);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getBoxcarWidth(int spectrometerIndex)
/*      */   {
/*  995 */     return getBoxcarWidth(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getBoxcarWidth(int spectrometerIndex, int channelIndex)
/*      */   {
/* 1005 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).getSmoothingWindowSize();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean insertKey(String keyValue)
/*      */   {
/* 1033 */     this.keyIsInserted = false;
/*      */     
/* 1035 */     if (keyValue.length() != 9) {
/* 1036 */       return false;
/*      */     }
/*      */     
/*      */ 
/* 1040 */     if ((keyValue.charAt(0) == 'w') && (keyValue.charAt(1) == 'p') && (keyValue.charAt(2) == 'T') && (keyValue.charAt(3) == '3') && (keyValue.charAt(4) == '7') && (keyValue.charAt(5) == '9') && (keyValue.charAt(6) == 'h') && (keyValue.charAt(7) == 'L') && (keyValue.charAt(8) == 'w'))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1050 */       this.keyIsInserted = true;
/* 1051 */       this.privilegeLevel = 0;
/*      */     }
/*      */     
/*      */ 
/* 1055 */     if ((keyValue.charAt(0) == 'M') && (keyValue.charAt(1) == 'a') && (keyValue.charAt(2) == 't') && (keyValue.charAt(3) == '4') && (keyValue.charAt(4) == '2') && (keyValue.charAt(5) == '9') && (keyValue.charAt(6) == 's') && (keyValue.charAt(7) == 'k') && (keyValue.charAt(8) == 'y'))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1065 */       this.keyIsInserted = true;
/* 1066 */       this.privilegeLevel = 1;
/*      */     }
/*      */     
/*      */ 
/* 1070 */     if ((keyValue.charAt(0) == 'd') && (keyValue.charAt(1) == 'r') && (keyValue.charAt(2) == 'a') && (keyValue.charAt(3) == 'g') && (keyValue.charAt(4) == 'o') && (keyValue.charAt(5) == 'n') && (keyValue.charAt(6) == '_') && (keyValue.charAt(7) == '2') && (keyValue.charAt(8) == '5'))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1080 */       this.keyIsInserted = true;
/* 1081 */       this.dragonIsAccessible = true;
/*      */     }
/*      */     
/* 1084 */     return this.keyIsInserted;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void removeKey()
/*      */   {
/* 1091 */     this.keyIsInserted = false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Coefficients getCalibrationCoefficientsFromBuffer(int spectrometerIndex)
/*      */   {
/* 1101 */     return getCalibrationCoefficientsFromBuffer(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Coefficients getCalibrationCoefficientsFromBuffer(int spectrometerIndex, int channelIndex)
/*      */   {
/* 1117 */     Coefficients coefficients = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1123 */     Coefficients newCoefficients = new Coefficients(coefficients);
/*      */     
/* 1125 */     return newCoefficients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Coefficients getCalibrationCoefficientsFromEEProm(int spectrometerIndex)
/*      */   {
/* 1135 */     return getCalibrationCoefficientsFromEEProm(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Coefficients getCalibrationCoefficientsFromEEProm(int spectrometerIndex, int channelIndex)
/*      */   {
/*      */     try
/*      */     {
/* 1155 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficientsFromSpectrometer();
/*      */ 
/*      */     }
/*      */     catch (IOException exception)
/*      */     {
/* 1160 */       this.lastException = exception;
/* 1161 */       return null;
/*      */     }
/*      */     
/*      */ 
/* 1165 */     Coefficients coefficients = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1171 */     Coefficients newCoefficients = new Coefficients(coefficients);
/*      */     
/* 1173 */     return newCoefficients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean setCalibrationCoefficientsIntoBuffer(int spectrometerIndex, Coefficients coefficients, boolean applyWavelengthCoefficients, boolean applyStrayLightConstant, boolean applyNonlinearityCoefficients)
/*      */   {
/* 1185 */     return setCalibrationCoefficientsIntoBuffer(spectrometerIndex, 0, coefficients, applyWavelengthCoefficients, applyStrayLightConstant, applyNonlinearityCoefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean setCalibrationCoefficientsIntoBuffer(int spectrometerIndex, int channelIndex, Coefficients newCoefficients, boolean applyWavelengthCoefficients, boolean applyStrayLightConstant, boolean applyNonlinearityCoefficients)
/*      */   {
/* 1245 */     if (!this.keyIsInserted) {
/* 1246 */       return false;
/*      */     }
/*      */     
/*      */ 
/* 1250 */     Coefficients coefficients = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1258 */     if (applyWavelengthCoefficients == true) {
/* 1259 */       coefficients.setWlIntercept(newCoefficients.getWlIntercept());
/* 1260 */       coefficients.setWlFirst(newCoefficients.getWlFirst());
/* 1261 */       coefficients.setWlSecond(newCoefficients.getWlSecond());
/* 1262 */       coefficients.setWlThird(newCoefficients.getWlThird());
/*      */     }
/*      */     
/* 1265 */     if (applyStrayLightConstant == true) {
/* 1266 */       coefficients.setStrayLight(newCoefficients.getStrayLight(), newCoefficients.getStrayLightSlope());
/*      */     }
/*      */     
/* 1269 */     if (applyNonlinearityCoefficients == true) {
/* 1270 */       coefficients.setNlCoef0(newCoefficients.getNlCoef0());
/* 1271 */       coefficients.setNlCoef1(newCoefficients.getNlCoef1());
/* 1272 */       coefficients.setNlCoef2(newCoefficients.getNlCoef2());
/* 1273 */       coefficients.setNlCoef3(newCoefficients.getNlCoef3());
/* 1274 */       coefficients.setNlCoef4(newCoefficients.getNlCoef4());
/* 1275 */       coefficients.setNlCoef5(newCoefficients.getNlCoef5());
/* 1276 */       coefficients.setNlCoef6(newCoefficients.getNlCoef6());
/* 1277 */       coefficients.setNlCoef7(newCoefficients.getNlCoef7());
/* 1278 */       coefficients.setNlOrder(newCoefficients.getNlOrder());
/*      */     }
/*      */     
/* 1281 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean setCalibrationCoefficientsIntoEEProm(int spectrometerIndex, Coefficients coefficients, boolean applyWavelengthCoefficients, boolean applyStrayLightConstant, boolean applyNonlinearityCoefficients)
/*      */   {
/* 1293 */     return setCalibrationCoefficientsIntoEEProm(spectrometerIndex, 0, coefficients, applyWavelengthCoefficients, applyStrayLightConstant, applyNonlinearityCoefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean setCalibrationCoefficientsIntoEEProm(int spectrometerIndex, int channelIndex, Coefficients newCoefficients, boolean applyWavelengthCoefficients, boolean applyStrayLightConstants, boolean applyNonlinearityCoefficients)
/*      */   {
/* 1311 */     if (!this.keyIsInserted) {
/* 1312 */       return false;
/*      */     }
/*      */     
/*      */ 
/* 1316 */     Coefficients coefficients = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1322 */     if (applyWavelengthCoefficients == true) {
/* 1323 */       coefficients.setWlIntercept(newCoefficients.getWlIntercept());
/* 1324 */       coefficients.setWlFirst(newCoefficients.getWlFirst());
/* 1325 */       coefficients.setWlSecond(newCoefficients.getWlSecond());
/* 1326 */       coefficients.setWlThird(newCoefficients.getWlThird());
/*      */     }
/*      */     
/* 1329 */     if (applyStrayLightConstants == true) {
/* 1330 */       coefficients.setStrayLight(newCoefficients.getStrayLight(), newCoefficients.getStrayLightSlope());
/*      */     }
/*      */     
/* 1333 */     if (applyNonlinearityCoefficients == true) {
/* 1334 */       coefficients.setNlCoef0(newCoefficients.getNlCoef0());
/* 1335 */       coefficients.setNlCoef1(newCoefficients.getNlCoef1());
/* 1336 */       coefficients.setNlCoef2(newCoefficients.getNlCoef2());
/* 1337 */       coefficients.setNlCoef3(newCoefficients.getNlCoef3());
/* 1338 */       coefficients.setNlCoef4(newCoefficients.getNlCoef4());
/* 1339 */       coefficients.setNlCoef5(newCoefficients.getNlCoef5());
/* 1340 */       coefficients.setNlCoef6(newCoefficients.getNlCoef6());
/* 1341 */       coefficients.setNlCoef7(newCoefficients.getNlCoef7());
/* 1342 */       coefficients.setNlOrder(newCoefficients.getNlOrder());
/*      */     }
/*      */     
/*      */     try
/*      */     {
/* 1347 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).setCoefficients();
/*      */ 
/*      */     }
/*      */     catch (IOException exception)
/*      */     {
/* 1352 */       this.lastException = exception;
/* 1353 */       return false;
/*      */     }
/*      */     
/* 1356 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getEEPromInfo(int spectrometerIndex, int slot)
/*      */   {
/* 1371 */     if (slot < 0) {
/* 1372 */       return null;
/*      */     }
/*      */     
/*      */     try
/*      */     {
/* 1377 */       Spectrometer spectrometer = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/* 1378 */       if (!(spectrometer instanceof USBSpectrometer))
/* 1379 */         return null;
/* 1380 */       return ((USBSpectrometer)spectrometer).getInfo(slot);
/*      */     } catch (IOException exception) {
/* 1382 */       this.lastException = exception; }
/* 1383 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean setEEPromInfo(int spectrometerIndex, int slot, String str)
/*      */   {
/* 1469 */     if (!this.keyIsInserted) {
/* 1470 */       return false;
/*      */     }
/*      */     
/* 1473 */     if (slot < 0) {
/* 1474 */       return false;
/*      */     }
/*      */     
/* 1477 */     Spectrometer spectrometer = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 1480 */     if (!spectrometer.allowWriteToEEPROM(this.privilegeLevel, slot)) {
/* 1481 */       return false;
/*      */     }
/*      */     try
/*      */     {
/* 1485 */       if (!(spectrometer instanceof USBSpectrometer))
/* 1486 */         return false;
/* 1487 */       ((USBSpectrometer)spectrometer).setInfo(slot, str);
/*      */     } catch (IOException exception) {
/* 1489 */       this.lastException = exception;
/* 1490 */       return false;
/*      */     }
/* 1492 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean setDetectorSetPointCelsius(int spectrometerIndex, double temperatureCelsius)
/*      */   {
/* 1512 */     if (!isFeatureSupportedThermoElectric(spectrometerIndex)) {
/* 1513 */       return false;
/*      */     }
/* 1515 */     ThermoElectricWrapper thermoElectricController = getFeatureControllerThermoElectric(spectrometerIndex);
/*      */     try
/*      */     {
/* 1518 */       thermoElectricController.setDetectorSetPointCelsius(temperatureCelsius);
/*      */     } catch (IOException exception) {
/* 1520 */       return false;
/*      */     }
/* 1522 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExternalTriggerMode(int spectrometerIndex, int mode)
/*      */   {
/* 1596 */     setExternalTriggerMode(spectrometerIndex, 0, mode);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExternalTriggerMode(int spectrometerIndex, int channelIndex, int mode)
/*      */   {
/* 1610 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setExternalTriggerMode(mode);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getExternalTriggerMode(int spectrometerIndex)
/*      */   {
/* 1625 */     return getExternalTriggerMode(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getExternalTriggerMode(int spectrometerIndex, int channelIndex)
/*      */   {
/* 1639 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).getExternalTriggerMode();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCorrectForElectricalDark(int spectrometerIndex, int enable)
/*      */   {
/* 1660 */     setCorrectForElectricalDark(spectrometerIndex, 0, enable);
/*      */   }
/*      */   
/*      */   public void setCorrectForElectricalDark(int spectrometerIndex, int channelIndex, int enable)
/*      */   {
/* 1665 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setCorrectForElectricalDark(enable != 0, false);
/*      */   }
/*      */   
/*      */ 
/*      */   public boolean getCorrectForElectricalDark(int spectrometerIndex)
/*      */   {
/* 1671 */     return getCorrectForElectricalDark(spectrometerIndex, 0);
/*      */   }
/*      */   
/* 1674 */   public boolean getCorrectForElectricalDark(int spectrometerIndex, int channelIndex) { return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).isCorrectForElectricalDark(); }
/*      */   
/*      */ 
/*      */ 
/*      */   public boolean getCorrectForStrayLight(int spectrometerIndex)
/*      */   {
/* 1680 */     return getCorrectForStrayLight(spectrometerIndex, 0);
/*      */   }
/*      */   
/* 1683 */   public boolean getCorrectForStrayLight(int spectrometerIndex, int channelIndex) { return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).isCorrectForStrayLight(); }
/*      */   
/*      */ 
/*      */ 
/*      */   public boolean setCorrectForDetectorNonlinearity(int spectrometerIndex, int enable)
/*      */   {
/* 1689 */     return setCorrectForDetectorNonlinearity(spectrometerIndex, 0, enable);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean setCorrectForDetectorNonlinearity(int spectrometerIndex, int channelIndex, int enable)
/*      */   {
/* 1705 */     if (enable != 0)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/* 1710 */       if (!((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).hasNonlinearityCorrectionCoefficients())
/*      */       {
/* 1712 */         return false;
/*      */       }
/*      */     }
/*      */     
/* 1716 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setCorrectForDetectorNonlinearity(enable != 0);
/*      */     
/*      */ 
/* 1719 */     return true;
/*      */   }
/*      */   
/*      */   public boolean getCorrectForDetectorNonlinearity(int spectrometerIndex) {
/* 1723 */     return getCorrectForDetectorNonlinearity(spectrometerIndex, 0);
/*      */   }
/*      */   
/* 1726 */   public boolean getCorrectForDetectorNonlinearity(int spectrometerIndex, int channelIndex) { return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).isCorrectForDetectorNonlinearity(); }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStrobeEnable(int spectrometerIndex, int lampOn)
/*      */   {
/* 1751 */     setStrobeEnable(spectrometerIndex, 0, lampOn);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStrobeEnable(int spectrometerIndex, int channelIndex, int lampOn)
/*      */   {
/* 1765 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setStrobeEnable(lampOn != 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getStrobeEnable(int spectrometerIndex)
/*      */   {
/* 1777 */     return getStrobeEnable(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */   public int getStrobeEnable(int spectrometerIndex, int channelIndex)
/*      */   {
/* 1783 */     boolean strobeEnabled = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).getStrobeEnable();
/*      */     
/*      */ 
/* 1786 */     return strobeEnabled == true ? 1 : 0;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private String getDriverVersion(String directory)
/*      */   {
/* 1794 */     File[] f = Jars.getAllJarsInDirectory(directory);
/* 1795 */     String str = "";
/* 1796 */     for (int i = 0; i < f.length; i++) {
/* 1797 */       str = str + f[i].getName() + " " + Jars.getJarAttribute(f[i], "OpenIDE-Module-Specification-Version") + "\n";
/*      */     }
/* 1799 */     return str;
/*      */   }
/*      */   
/*      */   public double getWavelength(int spectrometerIndex, int pixel)
/*      */   {
/* 1804 */     return getWavelength(spectrometerIndex, 0, pixel);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double getWavelength(int spectrometerIndex, int channelIndex, int pixel)
/*      */   {
/* 1817 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getWavelength(pixel);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double[] getWavelengths(int spectrometerIndex)
/*      */   {
/* 1838 */     return getWavelengths(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */   public double[] getWavelengths(int spectrometerIndex, int channelIndex) {
/* 1842 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getAllWavelengths();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */    @Deprecated
/*      */   public double getWavelengthIntercept(int spectrometerIndex)
/*      */   {
/* 1858 */     return getWavelengthIntercept(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */    @Deprecated
/*      */   public double getWavelengthIntercept(int spectrometerIndex, int channelIndex) {
/* 1865 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients().getWlIntercept();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 	@Deprecated
/*      */   public double getWavelengthFirst(int spectrometerIndex)
/*      */   {
/* 1881 */     return getWavelengthFirst(spectrometerIndex, 0);
/*      */   }
/*      */    @Deprecated
/*      */   public double getWavelengthFirst(int spectrometerIndex, int channelIndex) {
/* 1888 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients().getWlFirst();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */    @Deprecated
/*      */   public double getWavelengthSecond(int spectrometerIndex)
/*      */   {
/* 1904 */     return getWavelengthSecond(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */    @Deprecated
/*      */   public double getWavelengthSecond(int spectrometerIndex, int channelIndex) {
/* 1911 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients().getWlSecond();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @Deprecated
/*      */   public double getWavelengthThird(int spectrometerIndex)
/*      */   {
/* 1927 */     return getWavelengthThird(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */    @Deprecated
/*      */   public double getWavelengthThird(int spectrometerIndex, int channelIndex) {
/* 1934 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getChannel(channelIndex).getCoefficients().getWlThird();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int setTimeout(int spectrometerIndex, int timeoutMilliseconds)
/*      */   {
/*      */     try
/*      */     {
/* 1955 */       return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(0).setTimeout(timeoutMilliseconds);
/*      */     }
/*      */     catch (Exception exception) {
/* 1958 */       this.lastException = exception;
/*      */     }
/* 1960 */     return -1;
/*      */   }
/*      */   
/*      */   public boolean isTimeout(int spectrometerIndex)
/*      */   {
/* 1965 */     return isTimeout(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */   public boolean isTimeout(int spectrometerIndex, int channelIndex)
/*      */   {
/* 1970 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getTimeoutState(channelIndex);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isSpectrumValid(int spectrometerIndex)
/*      */   {
/* 1988 */     return isSpectrumValid(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */   public boolean isSpectrumValid(int spectrometerIndex, int channelIndex)
/*      */   {
/* 1993 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).isSpectrumValid(channelIndex);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double[] getSpectrum(int spectrometerIndex)
/*      */   {
/* 2011 */     return getSpectrum(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public double[] getSpectrum(int spectrometerIndex, int channelIndex)
/*      */   {
/* 2019 */     if (this.rawSpectrumMode == true) {
/* 2020 */       return getSpectrumRaw(spectrometerIndex, channelIndex);
/*      */     }
/*      */     
/* 2023 */     int numberOfPixels = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfPixels();
/*      */     
/*      */ 
/* 2026 */     int numberOfDarkPixels = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfDarkPixels();
/*      */     
/*      */ 
/*      */ 
/* 2030 */     Spectrum ds = new Spectrum(numberOfPixels, numberOfDarkPixels);
/*      */     
/* 2032 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, false);
/* 2033 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setTimeoutState(channelIndex, false);
/*      */     try
/*      */     {
/* 2036 */       ds = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).getSpectrum(ds);
/*      */     }
/*      */     catch (Exception ee)
/*      */     {
/* 2040 */       this.lastException = ee;
/*      */       
/*      */ 
/* 2043 */       boolean timeoutOccurred = false;
/* 2044 */       Spectrometer temporarySpectrometerReference = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */       
/* 2046 */       if ((temporarySpectrometerReference instanceof USBSpectrometer))
/*      */       {
/* 2048 */         timeoutOccurred = ((USBSpectrometer)temporarySpectrometerReference).isTimeout();
/*      */       }
/* 2050 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setTimeoutState(channelIndex, timeoutOccurred);
/*      */       
/* 2052 */       ds = new Spectrum(numberOfPixels, numberOfDarkPixels);
/* 2053 */       double[] pixels = ds.getSpectrum();
/* 2054 */       for (int index = 0; index < numberOfPixels; index++) {
/* 2055 */         pixels[index] = 0.0D;
/*      */       }
/* 2057 */       ds.setSpectrum(pixels);
/* 2058 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, false);
/* 2059 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSpectrumValidity(channelIndex, false);
/* 2060 */       return ds.getSpectrum();
/*      */     }
/*      */     
/*      */ 
/* 2064 */     boolean timeoutOccurred = false;
/* 2065 */     Spectrometer temporarySpectrometerReference = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/* 2067 */     if ((temporarySpectrometerReference instanceof USBSpectrometer))
/*      */     {
/* 2069 */       timeoutOccurred = ((USBSpectrometer)temporarySpectrometerReference).isTimeout();
/*      */     }
/* 2071 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setTimeoutState(channelIndex, timeoutOccurred);
/*      */     
/*      */ 
/* 2074 */     if ((ds == null) || (timeoutOccurred == true))
/*      */     {
/* 2076 */       ds = new Spectrum(numberOfPixels, numberOfDarkPixels);
/* 2077 */       double[] pixels = ds.getSpectrum();
/* 2078 */       for (int index = 0; index < numberOfPixels; index++) {
/* 2079 */         pixels[index] = 0.0D;
/*      */       }
/* 2081 */       ds.setSpectrum(pixels);
/* 2082 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, false);
/* 2083 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSpectrumValidity(channelIndex, false);
/* 2084 */       return ds.getSpectrum();
/*      */     }
/*      */     
/* 2087 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, ds.isSaturated());
/* 2088 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSpectrumValidity(channelIndex, true);
/*      */     
/* 2090 */     return ds.getSpectrum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private double[] getSpectrumRaw(int spectrometerIndex, int channelIndex)
/*      */   {
/* 2097 */     int numberOfPixels = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfRawPixels();
/*      */     
/*      */ 
/* 2100 */     int numberOfDarkPixels = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfDarkPixels();
/*      */     
/*      */ 
/*      */ 
/* 2104 */     Spectrum ds = new Spectrum(numberOfPixels, numberOfDarkPixels);
/*      */     
/* 2106 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, false);
/*      */     
/*      */     try
/*      */     {
/* 2110 */       ds = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).getSpectrumRaw(ds);
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     catch (Exception ee)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/* 2120 */       this.lastException = ee;
/* 2121 */       ds = new Spectrum(numberOfPixels, numberOfDarkPixels);
/* 2122 */       double[] pixels = ds.getSpectrum();
/* 2123 */       for (int index = 0; index < numberOfPixels; index++) {
/* 2124 */         pixels[index] = 0.0D;
/*      */       }
/* 2126 */       ds.setSpectrum(pixels);
/* 2127 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, false);
/*      */       
/* 2129 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSpectrumValidity(channelIndex, false);
/*      */       
/* 2131 */       return ds.getSpectrum();
/*      */     }
/*      */     
/* 2134 */     if (ds == null)
/*      */     {
/*      */ 
/* 2137 */       ds = new Spectrum(numberOfPixels, numberOfDarkPixels);
/* 2138 */       double[] pixels = ds.getSpectrum();
/* 2139 */       for (int index = 0; index < numberOfPixels; index++) {
/* 2140 */         pixels[index] = 0.0D;
/*      */       }
/* 2142 */       ds.setSpectrum(pixels);
/* 2143 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, false);
/*      */       
/* 2145 */       ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSpectrumValidity(channelIndex, false);
/*      */       
/* 2147 */       return ds.getSpectrum();
/*      */     }
/*      */     
/* 2150 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSaturationState(channelIndex, ds.isSaturated());
/*      */     
/* 2152 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).setSpectrumValidity(channelIndex, true);
/*      */     
/*      */ 
/* 2155 */     return ds.getSpectrum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void stopAveraging(int spectrometerIndex)
/*      */   {
/* 2333 */     stopAveraging(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void stopAveraging(int spectrometerIndex, int channelIndex)
/*      */   {
/* 2354 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).stopAveraging();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isSaturated(int spectrometerIndex)
/*      */   {
/* 2453 */     return isSaturated(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */   public boolean isSaturated(int spectrometerIndex, int channelIndex) {
/* 2457 */     return ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSaturationState(channelIndex);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void closeAllSpectrometers()
/*      */   {
/* 2469 */     for (int spectrometerIndex = 0; spectrometerIndex < this.spectrometerAssemblyCollection.size(); 
/* 2470 */         spectrometerIndex++)
/*      */     {
/* 2472 */       closeSpectrometer(spectrometerIndex);
/*      */     }
/*      */     
/* 2475 */     this.spectrometerAssemblyCollection = new ArrayList();
/*      */   }
/*      */   
/*      */    @Deprecated
/*      */   public void closeSpectrometer(int spectrometerIndex)
/*      */   {
/* 2483 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).closeSpectrometer();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GPIO getFeatureControllerGPIO(int spectrometerIndex)
/*      */   {
/* 2537 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2540 */     return (GPIO)usbSpectrometer.getFeatureController(GPIOImpl.class);
/*      */   }
/*      */   
/* 2543 */   public boolean isFeatureSupportedGPIO(int spectrometerIndex) { USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2546 */     GPIO gpio = (GPIO)usbSpectrometer.getFeatureController(GPIOImpl.class);
/* 2547 */     return gpio != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SaturationThreshold getFeatureControllerSaturationThreshold(int spectrometerIndex)
/*      */   {
/* 2558 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2561 */     return (SaturationThreshold)usbSpectrometer.getFeatureController(SaturationThresholdImpl.class);
/*      */   }
/*      */   
/* 2564 */   public boolean isFeatureSupportedSaturationThreshold(int spectrometerIndex) { USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2567 */     SaturationThreshold saturationThreshold = (SaturationThreshold)usbSpectrometer.getFeatureController(SaturationThresholdImpl.class);
/* 2568 */     return saturationThreshold != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SPIBus getFeatureControllerSPIBus(int spectrometerIndex)
/*      */   {
/* 2579 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2582 */     return (SPIBus)usbSpectrometer.getFeatureController(SPIBusImpl.class);
/*      */   }
/*      */   
/* 2585 */   public boolean isFeatureSupportedSPIBus(int spectrometerIndex) { USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2588 */     SPIBus spiBus = (SPIBus)usbSpectrometer.getFeatureController(SPIBusImpl.class);
/* 2589 */     return spiBus != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private HardwareTrigger getFeatureControllerHardwareTrigger(int spectrometerIndex)
/*      */   {
/* 2600 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2603 */     return (HardwareTrigger)usbSpectrometer.getFeatureController(HardwareTriggerImpl.class);
/*      */   }
/*      */   
/*      */   private boolean isFeatureSupportedHardwareTrigger(int spectrometerIndex) {
/* 2607 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2610 */     HardwareTrigger hardwareTrigger = (HardwareTrigger)usbSpectrometer.getFeatureController(HardwareTriggerImpl.class);
/* 2611 */     return hardwareTrigger != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean sendSimulatedTriggerSignal(int spectrometerIndex)
/*      */   {
/* 2642 */     if (!isFeatureSupportedHardwareTrigger(spectrometerIndex))
/* 2643 */       return false;
/* 2644 */     HardwareTrigger hardwareTrigger = getFeatureControllerHardwareTrigger(spectrometerIndex);
/*      */     try {
/* 2646 */       hardwareTrigger.sendSimulatedTriggerSignal();
/*      */     } catch (IOException exception) {
/* 2648 */       return false;
/*      */     }
/* 2650 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean flushSpectrum(int spectrometerIndex)
/*      */   {
/* 2683 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/* 2687 */     if ((usbSpectrometer instanceof STS))
/*      */       try {
/* 2689 */         ((STS)usbSpectrometer).readSpectrum();
/* 2690 */         return true;
/*      */       } catch (IOException ex) {
/* 2692 */         ex.printStackTrace();
/* 2693 */         return false;
/*      */       }
/* 2695 */     if ((usbSpectrometer instanceof Pancake)) {
/*      */       try {
/* 2697 */         ((Pancake)usbSpectrometer).readSpectrum();
/* 2698 */         return true;
/*      */       } catch (IOException ex) {
/* 2700 */         ex.printStackTrace();
/* 2701 */         return false;
/*      */       }
/*      */     }
/* 2704 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public LightSource getFeatureControllerLightSource(int spectrometerIndex)
/*      */   {
/* 2715 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2718 */     return (LightSource)usbSpectrometer.getFeatureController(LightSourceImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedLightSource(int spectrometerIndex) {
/* 2722 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2725 */     LightSource lightSource = (LightSource)usbSpectrometer.getFeatureController(LightSourceImpl.class);
/* 2726 */     return lightSource != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SingleStrobe getFeatureControllerSingleStrobe(int spectrometerIndex)
/*      */   {
/* 2737 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2740 */     return (SingleStrobe)usbSpectrometer.getFeatureController(SingleStrobeImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedSingleStrobe(int spectrometerIndex) {
/* 2744 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2747 */     SingleStrobe singleStrobe = (SingleStrobe)usbSpectrometer.getFeatureController(SingleStrobeImpl.class);
/* 2748 */     return singleStrobe != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ContinuousStrobe getFeatureControllerContinuousStrobe(int spectrometerIndex)
/*      */   {
/* 2759 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2762 */     return (ContinuousStrobe)usbSpectrometer.getFeatureController(ContinuousStrobeImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedContinuousStrobe(int spectrometerIndex) {
/* 2766 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2769 */     ContinuousStrobe continuousStrobe = (ContinuousStrobe)usbSpectrometer.getFeatureController(ContinuousStrobeImpl.class);
/*      */     
/* 2771 */     return continuousStrobe != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CurrentOut getFeatureControllerCurrentOut(int spectrometerIndex)
/*      */   {
/* 2782 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2785 */     return (CurrentOut)usbSpectrometer.getFeatureController(CurrentOutImpl_LS450.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedCurrentOut(int spectrometerIndex) {
/* 2789 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2792 */     CurrentOut currentOut = (CurrentOut)usbSpectrometer.getFeatureController(CurrentOutImpl_LS450.class);
/*      */     
/* 2794 */     return currentOut != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public BoardTemperature getFeatureControllerBoardTemperature(int spectrometerIndex)
/*      */   {
/* 2805 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2808 */     return (BoardTemperature)usbSpectrometer.getFeatureController(BoardTemperatureImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedBoardTemperature(int spectrometerIndex) {
/* 2812 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2815 */     BoardTemperature boardTemperature = (BoardTemperature)usbSpectrometer.getFeatureController(BoardTemperatureImpl.class);
/*      */     
/* 2817 */     return boardTemperature != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DetectorTemperature getFeatureControllerDetectorTemperature(int spectrometerIndex)
/*      */   {
/* 2828 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2831 */     return (DetectorTemperature)usbSpectrometer.getFeatureController(DetectorTemperatureImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedDetectorTemperature(int spectrometerIndex) {
/* 2835 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2838 */     DetectorTemperature detectorTemperature = (DetectorTemperature)usbSpectrometer.getFeatureController(DetectorTemperatureImpl.class);
/*      */     
/* 2840 */     return detectorTemperature != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AnalogIn getFeatureControllerAnalogIn(int spectrometerIndex)
/*      */   {
/* 2851 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2854 */     return (AnalogIn)usbSpectrometer.getFeatureController(AnalogInImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedAnalogIn(int spectrometerIndex) {
/* 2858 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2861 */     AnalogIn analogIn = (AnalogIn)usbSpectrometer.getFeatureController(AnalogInImpl.class);
/* 2862 */     return analogIn != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AnalogOut getFeatureControllerAnalogOut(int spectrometerIndex)
/*      */   {
/* 2873 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2876 */     return (AnalogOut)usbSpectrometer.getFeatureController(AnalogOutImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedAnalogOut(int spectrometerIndex) {
/* 2880 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2883 */     AnalogOut analogOut = (AnalogOut)usbSpectrometer.getFeatureController(AnalogOutImpl.class);
/* 2884 */     return analogOut != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public LS450_Functions getFeatureControllerLS450(int spectrometerIndex)
/*      */   {
/* 2895 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2898 */     return (LS450_Functions)usbSpectrometer.getFeatureController(LS450_FunctionsImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedLS450(int spectrometerIndex) {
/* 2902 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 2905 */     LS450_Functions ls450Functions = (LS450_Functions)usbSpectrometer.getFeatureController(LS450_FunctionsImpl.class);
/* 2906 */     if (ls450Functions == null) {
/* 2907 */       return false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 2912 */     PlugInProviderImpl pluginProvider = (PlugInProviderImpl)usbSpectrometer.getFeatureController(PlugInProviderImpl.class);
/* 2913 */     if (pluginProvider == null) {
/* 2914 */       return false;
/*      */     }
/*      */     try
/*      */     {
/* 2918 */       SpectrometerPlugIn[] spectrometerPluginArray = pluginProvider.getPlugIns();
/* 2919 */       for (int jj = 0; jj < spectrometerPluginArray.length; jj++)
/*      */       {
/* 2921 */         if (spectrometerPluginArray[jj].getName().compareTo("USB-LS-450") == 0)
/* 2922 */           return true;
/*      */       }
/*      */     } catch (IOException exception) {
/* 2925 */       System.out.println(exception);
/* 2926 */       return false;
/*      */     }
/*      */     
/* 2929 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ExternalTemperatureWrapper getFeatureController_USB_LS450_ExternalTemperature(int spectrometerIndex)
/*      */   {
/* 2946 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2958 */     PlugInProviderImpl pluginProvider = (PlugInProviderImpl)usbSpectrometer.getFeatureController(PlugInProviderImpl.class);
/* 2959 */     if (pluginProvider == null) {
/* 2960 */       return null;
/*      */     }
/*      */     try
/*      */     {
/* 2964 */       SpectrometerPlugIn[] spectrometerPluginArray = pluginProvider.getPlugIns();
/* 2965 */       for (int jj = 0; jj < spectrometerPluginArray.length; jj++)
/*      */       {
/* 2967 */         if (spectrometerPluginArray[jj].getName().compareTo("USB-LS-450") == 0)
/*      */         {
/*      */ 
/* 2970 */           ExternalTemperature externalTemperature = (ExternalTemperature)spectrometerPluginArray[jj];
/* 2971 */           return new ExternalTemperatureWrapper(externalTemperature);
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (IOException exception) {
/* 2976 */       System.out.println(exception);
/* 2977 */       return null;
/*      */     }
/*      */     
/* 2980 */     return null;
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupported_USB_LS450_ExternalTemperature(int spectrometerIndex) {
/* 2984 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2990 */     PlugInProviderImpl pluginProvider = (PlugInProviderImpl)usbSpectrometer.getFeatureController(PlugInProviderImpl.class);
/* 2991 */     if (pluginProvider == null) {
/* 2992 */       return false;
/*      */     }
/*      */     try
/*      */     {
/* 2996 */       SpectrometerPlugIn[] spectrometerPluginArray = pluginProvider.getPlugIns();
/* 2997 */       for (int jj = 0; jj < spectrometerPluginArray.length; jj++)
/*      */       {
/*      */ 
/* 3000 */         if (spectrometerPluginArray[jj].getName().compareTo("USB-LS-450") == 0)
/* 3001 */           return true;
/*      */       }
/*      */     } catch (IOException exception) {
/* 3004 */       System.out.println(exception);
/* 3005 */       return false;
/*      */     }
/*      */     
/* 3008 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UV_VIS_LightSource getFeatureController_UV_VIS_LightSource(int spectrometerIndex)
/*      */   {
/* 3019 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 3031 */     PlugInProviderImpl pluginProvider = (PlugInProviderImpl)usbSpectrometer.getFeatureController(PlugInProviderImpl.class);
/* 3032 */     if (pluginProvider == null) {
/* 3033 */       return null;
/*      */     }
/*      */     try
/*      */     {
/* 3037 */       SpectrometerPlugIn[] spectrometerPluginArray = pluginProvider.getPlugIns();
/* 3038 */       for (int jj = 0; jj < spectrometerPluginArray.length; jj++)
/*      */       {
/* 3040 */         if (spectrometerPluginArray[jj].getName().compareTo("USB-ISS-UV") == 0)
/* 3041 */           return (UV_VIS_LightSource)spectrometerPluginArray[jj];
/*      */       }
/*      */     } catch (IOException exception) {
/* 3044 */       System.out.println(exception);
/* 3045 */       return null;
/*      */     }
/*      */     
/* 3048 */     return null;
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupported_UV_VIS_LightSource(int spectrometerIndex) {
/* 3052 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 3058 */     PlugInProviderImpl pluginProvider = (PlugInProviderImpl)usbSpectrometer.getFeatureController(PlugInProviderImpl.class);
/* 3059 */     if (pluginProvider == null) {
/* 3060 */       return false;
/*      */     }
/*      */     try
/*      */     {
/* 3064 */       SpectrometerPlugIn[] spectrometerPluginArray = pluginProvider.getPlugIns();
/* 3065 */       for (int jj = 0; jj < spectrometerPluginArray.length; jj++)
/*      */       {
/* 3067 */         if (spectrometerPluginArray[jj].getName().compareTo("USB-ISS-UV") == 0)
/* 3068 */           return true;
/*      */       }
/*      */     } catch (IOException exception) {
/* 3071 */       System.out.println(exception);
/* 3072 */       return false;
/*      */     }
/*      */     
/* 3075 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PixelBinningWrapper getFeatureControllerPixelBinning(int spectrometerIndex)
/*      */   {
/* 3086 */     PixelBinning pixelBinning = null;
/*      */     
/* 3088 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/* 3092 */     pixelBinning = (PixelBinning)usbSpectrometer.getFeatureController(PixelBinningImpl_STSBase.class);
/* 3093 */     if (pixelBinning == null) {
/* 3094 */       return null;
/*      */     }
/*      */     
/*      */ 
/* 3098 */     return new PixelBinningWrapper(pixelBinning);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isFeatureSupportedPixelBinning(int spectrometerIndex)
/*      */   {
/* 3110 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/* 3114 */     PixelBinning pixelBinning = (PixelBinning)usbSpectrometer.getFeatureController(PixelBinningImpl_STSBase.class);
/*      */     
/* 3116 */     return pixelBinning != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SpectrumTypeWrapper getFeatureControllerSpectrumType(int spectrometerIndex)
/*      */   {
/* 3128 */     SpectrumTypeSelect spectrumTypeSelect = null;
/*      */     
/* 3130 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/*      */ 
/* 3134 */     spectrumTypeSelect = (SpectrumTypeSelect)usbSpectrometer.getFeatureController(SpectrumTypeSelectImpl_STSBase.class);
/*      */     
/* 3136 */     if (spectrumTypeSelect == null) {
/* 3137 */       return null;
/*      */     }
/* 3139 */     return new SpectrumTypeWrapper(spectrumTypeSelect);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedSpectrumType(int spectrometerIndex)
/*      */   {
/* 3144 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3147 */     SpectrumTypeSelect spectrumTypeSelect = (SpectrumTypeSelect)usbSpectrometer.getFeatureController(SpectrumTypeSelectImpl_STSBase.class);
/*      */     
/* 3149 */     return spectrumTypeSelect != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ExternalTriggerDelay getFeatureControllerExternalTriggerDelay(int spectrometerIndex)
/*      */   {
/* 3161 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3164 */     return (ExternalTriggerDelay)usbSpectrometer.getFeatureController(ExternalTriggerDelayImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedExternalTriggerDelay(int spectrometerIndex) {
/* 3168 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3171 */     ExternalTriggerDelay externalTriggerDelay = (ExternalTriggerDelay)usbSpectrometer.getFeatureController(ExternalTriggerDelayImpl.class);
/*      */     
/* 3173 */     return externalTriggerDelay != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public I2CBus getFeatureControllerI2CBus(int spectrometerIndex)
/*      */   {
/* 3198 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3201 */     return (I2CBus)usbSpectrometer.getFeatureController(I2CBusImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedI2CBus(int spectrometerIndex)
/*      */   {
/* 3206 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3209 */     I2CBus i2cbus = (I2CBus)usbSpectrometer.getFeatureController(I2CBusImpl.class);
/*      */     
/* 3211 */     return i2cbus != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public HighGainMode getFeatureControllerHighGainMode(int spectrometerIndex)
/*      */   {
/* 3225 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3228 */     return (HighGainMode)usbSpectrometer.getFeatureController(HighGainModeImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedHighGainMode(int spectrometerIndex)
/*      */   {
/* 3233 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3236 */     HighGainMode featureController = (HighGainMode)usbSpectrometer.getFeatureController(HighGainModeImpl.class);
/*      */     
/* 3238 */     return featureController != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public IrradianceCalibrationFactor getFeatureControllerIrradianceCalibrationFactor(int spectrometerIndex)
/*      */   {
/* 3249 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3252 */     return (IrradianceCalibrationFactor)usbSpectrometer.getFeatureController(IrradianceCalibrationFactorImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedIrradianceCalibrationFactor(int spectrometerIndex)
/*      */   {
/* 3257 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3260 */     IrradianceCalibrationFactor irradianceCalibrationFactor = (IrradianceCalibrationFactor)usbSpectrometer.getFeatureController(IrradianceCalibrationFactorImpl.class);
/*      */     
/* 3262 */     return irradianceCalibrationFactor != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public NonlinearityCorrectionProvider getFeatureControllerNonlinearityCorrectionProvider(int spectrometerIndex)
/*      */   {
/* 3273 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3276 */     return (NonlinearityCorrectionProvider)usbSpectrometer.getFeatureController(NonlinearityCorrectionImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedNonlinearityCorrectionProvider(int spectrometerIndex) {
/* 3280 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3283 */     NonlinearityCorrectionProvider nonlinearityCorrectionProvider = (NonlinearityCorrectionProvider)usbSpectrometer.getFeatureController(NonlinearityCorrectionImpl.class);
/*      */     
/* 3285 */     return nonlinearityCorrectionProvider != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public StrayLightCorrection getFeatureControllerStrayLightCorrection(int spectrometerIndex)
/*      */   {
/* 3296 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3299 */     return (StrayLightCorrection)usbSpectrometer.getFeatureController(StrayLightCorrectionImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedStrayLightCorrection(int spectrometerIndex) {
/* 3303 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3306 */     StrayLightCorrection strayLightCorrection = (StrayLightCorrection)usbSpectrometer.getFeatureController(StrayLightCorrectionImpl.class);
/*      */     
/* 3308 */     return strayLightCorrection != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Version getFeatureControllerVersion(int spectrometerIndex)
/*      */   {
/* 3322 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3325 */     Version version = (Version)usbSpectrometer.getFeatureController(VersionImpl.class);
/*      */     String fv;
/* 3327 */     try { fv = version.getFirmwareVersion();
/*      */     } catch (Exception ee) {}
/* 3329 */     return version;
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedVersion(int spectrometerIndex) {
/* 3333 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3336 */     Version version = (Version)usbSpectrometer.getFeatureController(VersionImpl.class);
/* 3337 */     return version != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public WavelengthCalibrationProvider getFeatureControllerWavelengthCalibrationProvider(int spectrometerIndex)
/*      */   {
/* 3348 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3351 */     return (WavelengthCalibrationProvider)usbSpectrometer.getFeatureController(WavelengthCalibrationImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedWavelengthCalibrationProvider(int spectrometerIndex) {
/* 3355 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3358 */     WavelengthCalibrationProvider wavelengthCalibrationProvider = (WavelengthCalibrationProvider)usbSpectrometer.getFeatureController(WavelengthCalibrationImpl.class);
/*      */     
/* 3360 */     return wavelengthCalibrationProvider != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ThermoElectricWrapper getFeatureControllerThermoElectric(int spectrometerIndex)
/*      */   {
/* 3385 */     ThermoElectric thermoElectric = null;
/*      */     
/* 3387 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3390 */     Object featureController = usbSpectrometer.getFeatureControllerUncast(ThermoElectricImpl.class);
/* 3391 */     thermoElectric = (ThermoElectric)featureController;
/* 3392 */     return new ThermoElectricWrapper(thermoElectric);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedThermoElectric(int spectrometerIndex) {
/* 3396 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3399 */     ThermoElectric thermoElectric = (ThermoElectric)usbSpectrometer.getFeatureController(ThermoElectricImpl.class);
/* 3400 */     return thermoElectric != null;
/*      */   }
/*      */   
/*      */ 
/*      */   public Indy getFeatureControllerIndy(int spectrometerIndex)
/*      */   {
/* 3406 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3409 */     return (Indy)usbSpectrometer.getFeatureController(IndyImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedIndy(int spectrometerIndex) {
/* 3413 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3416 */     Indy indy = (Indy)usbSpectrometer.getFeatureController(IndyImpl.class);
/* 3417 */     return indy != null;
/*      */   }
/*      */   
/*      */ 
/*      */   public InternalTriggerWrapper getFeatureControllerInternalTrigger(int spectrometerIndex)
/*      */   {
/* 3423 */     InternalTrigger internalTrigger = null;
/*      */     
/* 3425 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3428 */     Object featureController = usbSpectrometer.getFeatureControllerUncast(InternalTriggerImpl_Jaz.class);
/* 3429 */     internalTrigger = (InternalTrigger)featureController;
/* 3430 */     return new InternalTriggerWrapper(internalTrigger);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public boolean isFeatureSupportedInternalTrigger(int spectrometerIndex)
/*      */   {
/* 3437 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3440 */     Object featureController = usbSpectrometer.getFeatureControllerUncast(InternalTriggerImpl_Jaz.class);
/* 3441 */     return featureController != null;
/*      */   }
/*      */   
/*      */ 
/*      */   public DataBuffer getFeatureControllerDataBuffer(int spectrometerIndex)
/*      */   {
/* 3447 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3450 */     return (DataBuffer)usbSpectrometer.getFeatureController(DataBufferImpl.class);
/*      */   }
/*      */   
/*      */   public boolean isFeatureSupportedDataBuffer(int spectrometerIndex)
/*      */   {
/* 3455 */     USBSpectrometer usbSpectrometer = (USBSpectrometer)((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer();
/*      */     
/*      */ 
/* 3458 */     DataBuffer dataBuffer = (DataBuffer)usbSpectrometer.getFeatureController(DataBufferImpl.class);
/* 3459 */     return dataBuffer != null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAutoToggleStrobeLampEnable(int spectrometerIndex, boolean enable)
/*      */   {
/* 3474 */     setAutoToggleStrobeLampEnable(spectrometerIndex, 0, enable);
/*      */   }
/*      */   
/*      */   public void setAutoToggleStrobeLampEnable(int spectrometerIndex, int channelIndex, boolean enable) {
/* 3478 */     ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex).setAutoToggleStrobeLampEnable(enable);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void highSpdAcq_AllocateBuffer(int spectrometerIndex, int numberOfSpectra)
/*      */   {
/* 3499 */     this.numberOfSpectraAcquired = 0;
/* 3500 */     this.stopCollectingSpectra = false;
/*      */     
/* 3502 */     int numberOfPixels = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfPixels();
/*      */     
/*      */ 
/* 3505 */     int numberOfDarkPixels = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectrometer().getNumberOfDarkPixels();
/*      */     
/*      */ 
/*      */ 
/* 3509 */     this.spectrumBuffer = new Spectrum[numberOfSpectra];
/* 3510 */     this.isSaturatedBuffer = new boolean[numberOfSpectra];
/* 3511 */     this.timeStampBuffer = new HighResTimeStamp[numberOfSpectra];
/* 3512 */     for (int index = 0; index < numberOfSpectra; index++) {
/* 3513 */       this.spectrumBuffer[index] = new Spectrum(numberOfPixels, numberOfDarkPixels);
/* 3514 */       this.isSaturatedBuffer[index] = false;
/* 3515 */       this.timeStampBuffer[index] = null;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void highSpdAcq_StartAcquisition(int spectrometerIndex)
/*      */   {
/* 3527 */     highSpdAcq_StartAcquisition(spectrometerIndex, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void highSpdAcq_StartAcquisition(int spectrometerIndex, int channelIndex)
/*      */   {
/* 3547 */     this.numberOfSpectraAcquired = 0;
/* 3548 */     int maximumNumberOfSpectraToAcquire = this.spectrumBuffer.length;
/* 3549 */     SpectralProcessor spectralProcessor = ((SpectrometerAssembly)this.spectrometerAssemblyCollection.get(spectrometerIndex)).getSpectralProcessor(channelIndex);
/*      */     
/*      */ 
/* 3552 */     this.stopCollectingSpectra = false;
/*      */     
/*      */     try
/*      */     {
/* 3556 */       for (int loopCounter = 0; loopCounter < maximumNumberOfSpectraToAcquire; loopCounter++) {
/* 3557 */         if (this.stopCollectingSpectra == true) {
/*      */           break;
/*      */         }
/* 3560 */         spectralProcessor.getSpectrum(this.spectrumBuffer[loopCounter]);
/* 3561 */         this.timeStampBuffer[loopCounter] = new HighResTimeStamp();
/* 3562 */         this.isSaturatedBuffer[loopCounter] = this.spectrumBuffer[loopCounter].isSaturated();
/* 3563 */         this.numberOfSpectraAcquired += 1;
/*      */       }
/*      */     } catch (IOException ee) {
/* 3566 */       this.numberOfSpectraAcquired = 0;
/* 3567 */       this.lastException = ee;
/* 3568 */       return;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void highSpdAcq_StopAcquisition()
/*      */   {
/* 3586 */     this.stopCollectingSpectra = true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int highSpdAcq_GetNumberOfSpectraAcquired()
/*      */   {
/* 3596 */     return this.numberOfSpectraAcquired;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double[] highSpdAcq_GetSpectrum(int spectrumNumber)
/*      */   {
/* 3609 */     if (spectrumNumber >= this.numberOfSpectraAcquired)
/* 3610 */       return null;
/* 3611 */     if (this.spectrumBuffer == null)
/* 3612 */       return null;
/* 3613 */     return this.spectrumBuffer[spectrumNumber].getSpectrum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean highSpdAcq_IsSaturated(int spectrumNumber)
/*      */   {
/* 3625 */     if (spectrumNumber >= this.numberOfSpectraAcquired)
/* 3626 */       return true;
/* 3627 */     if (this.isSaturatedBuffer == null)
/* 3628 */       return true;
/* 3629 */     return this.isSaturatedBuffer[spectrumNumber];
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public HighResTimeStamp highSpdAcq_GetTimeStamp(int spectrumNumber)
/*      */   {
/* 3642 */     if (spectrumNumber >= this.numberOfSpectraAcquired)
/* 3643 */       return null;
/* 3644 */     if (this.timeStampBuffer == null)
/* 3645 */       return null;
/* 3646 */     return this.timeStampBuffer[spectrumNumber];
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void mainOBSOLETE()
/*      */   {
/* 3665 */     Wrapper wrapper = new Wrapper();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 3674 */     int numberOfSpectrometers = wrapper.openAllSpectrometers();
/* 3675 */     System.out.println("Number of spectrometers detected = " + numberOfSpectrometers);
/* 3676 */     if (numberOfSpectrometers == 0) {
/* 3677 */       System.out.println("No attached spectrometers, exiting.");
/* 3678 */       return;
/*      */     }
/* 3680 */     int spectrometerIndex = 0;
/*      */     
/* 3682 */     wrapper.getSerialNumber(spectrometerIndex);
/*      */     
/* 3684 */     int numberOfChannels = wrapper.getWrapperExtensions().getNumberOfChannels(spectrometerIndex);
/* 3685 */     System.out.println("number of channels = " + numberOfChannels);
/*      */     
/* 3687 */     System.out.println("number of enabled channels = " + wrapper.getWrapperExtensions().getNumberOfEnabledChannels(spectrometerIndex));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 4387 */   private static String __extern__ = "__extern__\n<init>,()V\ngetWrapperExtensions,()Lcom/oceanoptics/omnidriver/api/wrapper/WrapperExtensions;\nexportToGramsSPC,(ILjava/lang/String;[DLjava/lang/String;)Z\nexportToGramsSPC,(IILjava/lang/String;[DLjava/lang/String;)Z\ngetApiVersion,()Ljava/lang/String;\ngetBuildNumber,()I\ngetLastException,()Ljava/lang/String;\ngetLastExceptionStackTrace,()Ljava/lang/String;\nopenNetworkSpectrometer,(Ljava/lang/String;)I\nsetNetworkCommunicationParameters,(IIZII)V\nopenAllSpectrometers,()I\ngetName,(I)Ljava/lang/String;\ngetMaximumIntegrationTime,(I)I\ngetMinimumIntegrationTime,(I)I\ngetMaximumIntensity,(I)I\ngetNumberOfSpectrometersFound,()I\ngetFirmwareVersion,(I)Ljava/lang/String;\ngetFirmwareModel,(I)Ljava/lang/String;\ngetSerialNumber,(I)Ljava/lang/String;\ngetNumberOfPixels,(I)I\ngetNumberOfPixels,(II)I\ngetNumberOfDarkPixels,(I)I\ngetNumberOfDarkPixels,(II)I\nreadRawUSB,(III)[B\nwriteRawUSB,(II[BI)I\nsetIntegrationTime,(II)V\nsetIntegrationTime,(III)V\ngetIntegrationTime,(I)I\ngetIntegrationTime,(II)I\nsetScansToAverage,(II)V\nsetScansToAverage,(III)V\ngetScansToAverage,(I)I\ngetScansToAverage,(II)I\ngetBench,(I)Lcom/oceanoptics/omnidriver/spectrometer/Bench;\ngetBench,(II)Lcom/oceanoptics/omnidriver/spectrometer/Bench;\ngetDetector,(II)Lcom/oceanoptics/omnidriver/spectrometer/Detector;\nsaveSTSConfiguration,(I)Z\ngetSTSBench,(I)Lcom/oceanoptics/omnidriver/spectrometer/sts/STSBench;\nsetBoxcarWidth,(II)V\nsetBoxcarWidth,(III)V\ngetBoxcarWidth,(I)I\ngetBoxcarWidth,(II)I\ninsertKey,(Ljava/lang/String;)Z\nremoveKey,()V\ngetCalibrationCoefficientsFromBuffer,(I)Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\ngetCalibrationCoefficientsFromBuffer,(II)Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\ngetCalibrationCoefficientsFromEEProm,(I)Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\ngetCalibrationCoefficientsFromEEProm,(II)Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\nsetCalibrationCoefficientsIntoBuffer,(ILcom/oceanoptics/omnidriver/spectrometer/Coefficients;ZZZ)Z\nsetCalibrationCoefficientsIntoBuffer,(IILcom/oceanoptics/omnidriver/spectrometer/Coefficients;ZZZ)Z\nsetCalibrationCoefficientsIntoEEProm,(ILcom/oceanoptics/omnidriver/spectrometer/Coefficients;ZZZ)Z\nsetCalibrationCoefficientsIntoEEProm,(IILcom/oceanoptics/omnidriver/spectrometer/Coefficients;ZZZ)Z\ngetEEPromInfo,(II)Ljava/lang/String;\nsetEEPromInfo,(IILjava/lang/String;)Z\nsetDetectorSetPointCelsius,(ID)Z\nsetExternalTriggerMode,(II)V\nsetExternalTriggerMode,(III)V\ngetExternalTriggerMode,(I)I\ngetExternalTriggerMode,(II)I\nsetCorrectForElectricalDark,(II)V\nsetCorrectForElectricalDark,(III)V\ngetCorrectForElectricalDark,(I)Z\ngetCorrectForElectricalDark,(II)Z\ngetCorrectForStrayLight,(I)Z\ngetCorrectForStrayLight,(II)Z\nsetCorrectForDetectorNonlinearity,(II)Z\nsetCorrectForDetectorNonlinearity,(III)Z\ngetCorrectForDetectorNonlinearity,(I)Z\ngetCorrectForDetectorNonlinearity,(II)Z\nsetStrobeEnable,(II)V\nsetStrobeEnable,(III)V\ngetStrobeEnable,(I)I\ngetStrobeEnable,(II)I\ngetWavelength,(II)D\ngetWavelength,(III)D\ngetWavelengths,(I)[D\ngetWavelengths,(II)[D\ngetWavelengthIntercept,(I)D\ngetWavelengthIntercept,(II)D\ngetWavelengthFirst,(I)D\ngetWavelengthFirst,(II)D\ngetWavelengthSecond,(I)D\ngetWavelengthSecond,(II)D\ngetWavelengthThird,(I)D\ngetWavelengthThird,(II)D\nsetTimeout,(II)I\nisTimeout,(I)Z\nisTimeout,(II)Z\nisSpectrumValid,(I)Z\nisSpectrumValid,(II)Z\ngetSpectrum,(I)[D\ngetSpectrum,(II)[D\nstopAveraging,(I)V\nstopAveraging,(II)V\nisSaturated,(I)Z\nisSaturated,(II)Z\ncloseAllSpectrometers,()V\ncloseSpectrometer,(I)V\ngetFeatureControllerGPIO,(I)Lcom/oceanoptics/omnidriver/features/gpio/GPIO;\nisFeatureSupportedGPIO,(I)Z\ngetFeatureControllerSaturationThreshold,(I)Lcom/oceanoptics/omnidriver/features/saturationthreshold/SaturationThreshold;\nisFeatureSupportedSaturationThreshold,(I)Z\ngetFeatureControllerSPIBus,(I)Lcom/oceanoptics/omnidriver/features/spibus/SPIBus;\nisFeatureSupportedSPIBus,(I)Z\nsendSimulatedTriggerSignal,(I)Z\nflushSpectrum,(I)Z\ngetFeatureControllerLightSource,(I)Lcom/oceanoptics/omnidriver/features/lightsource/LightSource;\nisFeatureSupportedLightSource,(I)Z\ngetFeatureControllerSingleStrobe,(I)Lcom/oceanoptics/omnidriver/features/singlestrobe/SingleStrobe;\nisFeatureSupportedSingleStrobe,(I)Z\ngetFeatureControllerContinuousStrobe,(I)Lcom/oceanoptics/omnidriver/features/continuousstrobe/ContinuousStrobe;\nisFeatureSupportedContinuousStrobe,(I)Z\ngetFeatureControllerCurrentOut,(I)Lcom/oceanoptics/omnidriver/features/currentout/CurrentOut;\nisFeatureSupportedCurrentOut,(I)Z\ngetFeatureControllerBoardTemperature,(I)Lcom/oceanoptics/omnidriver/features/boardtemperature/BoardTemperature;\nisFeatureSupportedBoardTemperature,(I)Z\ngetFeatureControllerDetectorTemperature,(I)Lcom/oceanoptics/omnidriver/features/detectortemperature/DetectorTemperature;\nisFeatureSupportedDetectorTemperature,(I)Z\ngetFeatureControllerAnalogIn,(I)Lcom/oceanoptics/omnidriver/features/analogin/AnalogIn;\nisFeatureSupportedAnalogIn,(I)Z\ngetFeatureControllerAnalogOut,(I)Lcom/oceanoptics/omnidriver/features/analogout/AnalogOut;\nisFeatureSupportedAnalogOut,(I)Z\ngetFeatureControllerLS450,(I)Lcom/oceanoptics/omnidriver/features/ls450functions/LS450_Functions;\nisFeatureSupportedLS450,(I)Z\ngetFeatureController_USB_LS450_ExternalTemperature,(I)Lcom/oceanoptics/omnidriver/features/externaltemperature/ExternalTemperatureWrapper;\nisFeatureSupported_USB_LS450_ExternalTemperature,(I)Z\ngetFeatureController_UV_VIS_LightSource,(I)Lcom/oceanoptics/omnidriver/features/uvvislightsource/UV_VIS_LightSource;\nisFeatureSupported_UV_VIS_LightSource,(I)Z\ngetFeatureControllerPixelBinning,(I)Lcom/oceanoptics/omnidriver/features/resolution/PixelBinningWrapper;\nisFeatureSupportedPixelBinning,(I)Z\ngetFeatureControllerSpectrumType,(I)Lcom/oceanoptics/omnidriver/features/spectrumtype/SpectrumTypeWrapper;\nisFeatureSupportedSpectrumType,(I)Z\ngetFeatureControllerExternalTriggerDelay,(I)Lcom/oceanoptics/omnidriver/features/externaltriggerdelay/ExternalTriggerDelay;\nisFeatureSupportedExternalTriggerDelay,(I)Z\ngetFeatureControllerI2CBus,(I)Lcom/oceanoptics/omnidriver/features/i2cbus/I2CBus;\nisFeatureSupportedI2CBus,(I)Z\ngetFeatureControllerHighGainMode,(I)Lcom/oceanoptics/omnidriver/features/highgainmode/HighGainMode;\nisFeatureSupportedHighGainMode,(I)Z\ngetFeatureControllerIrradianceCalibrationFactor,(I)Lcom/oceanoptics/omnidriver/features/irradiancecalibrationfactor/IrradianceCalibrationFactor;\nisFeatureSupportedIrradianceCalibrationFactor,(I)Z\ngetFeatureControllerNonlinearityCorrectionProvider,(I)Lcom/oceanoptics/omnidriver/features/nonlinearitycorrection/NonlinearityCorrectionProvider;\nisFeatureSupportedNonlinearityCorrectionProvider,(I)Z\ngetFeatureControllerStrayLightCorrection,(I)Lcom/oceanoptics/omnidriver/features/straylightcorrection/StrayLightCorrection;\nisFeatureSupportedStrayLightCorrection,(I)Z\ngetFeatureControllerVersion,(I)Lcom/oceanoptics/omnidriver/features/version/Version;\nisFeatureSupportedVersion,(I)Z\ngetFeatureControllerWavelengthCalibrationProvider,(I)Lcom/oceanoptics/omnidriver/features/wavelengthcalibration/WavelengthCalibrationProvider;\nisFeatureSupportedWavelengthCalibrationProvider,(I)Z\ngetFeatureControllerThermoElectric,(I)Lcom/oceanoptics/omnidriver/features/thermoelectric/ThermoElectricWrapper;\nisFeatureSupportedThermoElectric,(I)Z\ngetFeatureControllerIndy,(I)Lcom/oceanoptics/omnidriver/features/indy/Indy;\nisFeatureSupportedIndy,(I)Z\ngetFeatureControllerInternalTrigger,(I)Lcom/oceanoptics/omnidriver/features/internaltrigger/InternalTriggerWrapper;\nisFeatureSupportedInternalTrigger,(I)Z\ngetFeatureControllerDataBuffer,(I)Lcom/oceanoptics/omnidriver/features/buffer/DataBuffer;\nisFeatureSupportedDataBuffer,(I)Z\nsetAutoToggleStrobeLampEnable,(IZ)V\nsetAutoToggleStrobeLampEnable,(IIZ)V\nhighSpdAcq_AllocateBuffer,(II)V\nhighSpdAcq_StartAcquisition,(I)V\nhighSpdAcq_StartAcquisition,(II)V\nhighSpdAcq_StopAcquisition,()V\nhighSpdAcq_GetNumberOfSpectraAcquired,()I\nhighSpdAcq_GetSpectrum,(I)[D\nhighSpdAcq_IsSaturated,(I)Z\nhighSpdAcq_GetTimeStamp,(I)Lcom/oceanoptics/highrestiming/HighResTimeStamp;\nmainOBSOLETE,()V\n";
/*      */ }


/* Location:              /opt/omniDriver/OOI_HOME/OmniDriver.jar!/com/oceanoptics/omnidriver/api/wrapper/Wrapper.class
 * Java compiler version: 4 (48.0)
 * JD-Core Version:       0.7.1
 */