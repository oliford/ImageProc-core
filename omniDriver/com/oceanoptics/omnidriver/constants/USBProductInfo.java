/*     */ package com.oceanoptics.omnidriver.constants;
/*     */ 
/*     */ import java.util.HashMap;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class USBProductInfo
/*     */ {
/*     */   public final String name;
/*     */   public final String className;
/*     */   public final int vendorID;
/*     */   public final int productID;
/*     */   public static final int OOI_VENDOR_ID = 9303;
/*     */   public static final int USB2000_PRODUCT_ID = 4098;
/*     */   public static final int ADC1000USB_PRODUCT_ID = 4100;
/*     */   public static final int SAS_PRODUCT_ID = 4102;
/*     */   public static final int HR2000_PRODUCT_ID = 4106;
/*     */   public static final int NIR512_PRODUCT_ID = 4108;
/*     */   public static final int NIR256_PRODUCT_ID = 4112;
/*     */   public static final int HR4000_PRODUCT_ID = 4114;
/*     */   public static final int USB650_PRODUCT_ID = 4116;
/*     */   public static final int HR2000Plus_PRODUCT_ID = 4118;
/*     */   public static final int QE65000_PRODUCT_ID = 4120;
/*     */   public static final int USB2000Plus_PRODUCT_ID = 4126;
/*     */   public static final int FLAME_NIR128B_PRODUCT_ID = 4171;
/*     */   public static final int USB4000_PRODUCT_ID = 4130;
/*     */   public static final int USB325_PRODUCT_ID = 4132;
/*     */   public static final int USB250_PRODUCT_ID = 4144;
/*     */   public static final int USB500_PRODUCT_ID = 4150;
/*     */   public static final int S8378_PRODUCT_ID = 4146;
/*     */   public static final int DRAGON_PRODUCT_ID = 4156;
/*     */   public static final int TORUS_USB_PRODUCT_ID = 4160;
/*     */   public static final int MAYA_PRO_2000_PRODUCT_ID = 4138;
/*     */   public static final int MAYA2000_PRODUCT_ID = 4140;
/*     */   public static final int APEX_PRODUCT_ID = 4164;
/*     */   public static final int MAYA_LSL_PRODUCT_ID = 4166;
/*     */   public static final int JAZ_PRODUCT_ID = 8192;
/*     */   public static final int FLAMEX_PRODUCT_ID = 8193;
/*     */   public static final int BLAZE_PRODUCT_ID = 8194;
/*     */   public static final int NIRQUEST256_PRODUCT_ID = 4136;
/*     */   public static final int NIRQUEST512_PRODUCT_ID = 4134;
/*     */   public static final int STS_PRODUCT_ID = 16384;
/*     */   public static final int QEPRO_PRODUCT_ID = 16388;
/*     */   public static final int PANCAKE_PRODUCT_ID = 16896;
/*     */   public static final int VENTANA_PRODUCT_ID = 20480;
/*     */   public static final int W1064M_PRODUCT_ID = 20481;
/*     */   public static final int OCEAN_OPTICS_1064VNIR_PID = 20482;
/*     */   public static final int DEFAULT_PRODUCT_ID = 0;
/*     */   public static final int CENTICE_VENDOR_ID = 6220;
/*     */   public static final int MMSRAMAN_PRODUCT_ID = 0;
/*     */   public static final int FLUXDATA_VENDOR_ID = 11047;
/*     */   public static final int FLUXDATA_PRODUCT_ID = 55297;
/*     */   public static final int ISS_ID = 1;
/*     */   public static final int LS450_ID = 2;
/*     */   public static final int ISS_UV_ID = 3;
/*     */   
/*     */   public USBProductInfo(String name, String className, int vendorID, int productID)
/*     */   {
/* 177 */     this.name = name;
/* 178 */     this.className = className;
/* 179 */     this.vendorID = vendorID;
/* 180 */     this.productID = productID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void addToMap(USBProductInfo upi)
/*     */   {
/* 189 */     Long key = new Long(upi.vendorID << 16 | upi.productID);
/* 190 */     products.put(key, upi);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static USBProductInfo getProductInfo(int vendorID, int productID)
/*     */   {
/* 198 */     Long key = new Long(vendorID << 16 | productID);
/* 199 */     return (USBProductInfo)products.get(key);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 213 */   public static final HashMap products = new HashMap();
/*     */   
/*     */   static {
				
	addToMap(new USBProductInfo("HR4000", "com.oceanoptics.omnidriver.spectrometer.hr4000.HR4000", 9303, 4114));
	addToMap(new USBProductInfo("USB650", "com.oceanoptics.omnidriver.spectrometer.usb650.USB650", 9303, 4116));
	addToMap(new USBProductInfo("USB4000", "com.oceanoptics.omnidriver.spectrometer.usb4000.USB4000", 9303, 4130));
	/*
     addToMap(new USBProductInfo("USB2000", "com.oceanoptics.omnidriver.spectrometer.usb2000.USB2000", 9303, 4098));
     addToMap(new USBProductInfo("QE65000", "com.oceanoptics.omnidriver.spectrometer.qe65000.QE65000", 9303, 4120));
     addToMap(new USBProductInfo("ADC1000-USB", "com.oceanoptics.omnidriver.spectrometer.adc1000usb.ADC1000USB", 9303, 4100));
     addToMap(new USBProductInfo("HR2000", "com.oceanoptics.omnidriver.spectrometer.hr2000.HR2000", 9303, 4106));
     addToMap(new USBProductInfo("HR2000+", "com.oceanoptics.omnidriver.spectrometer.hr2000plus.HR2000Plus", 9303, 4118));
     addToMap(new USBProductInfo("NIR512", "com.oceanoptics.omnidriver.spectrometer.nir.nir512.NIR512", 9303, 4108));
      addToMap(new USBProductInfo("NIR256", "com.oceanoptics.omnidriver.spectrometer.nir.nir256.NIR256", 9303, 4112));
      addToMap(new USBProductInfo("SAS", "com.oceanoptics.omnidriver.spectrometer.sas.SAS", 9303, 4102));
      addToMap(new USBProductInfo("USB2000+", "com.oceanoptics.omnidriver.spectrometer.usb2000plus.USB2000Plus", 9303, 4126));
      addToMap(new USBProductInfo("FLAME-NIR", "com.oceanoptics.omnidriver.spectrometer.flamenir.FlameNIR", 9303, 4171));
      addToMap(new USBProductInfo("USB325", "com.oceanoptics.omnidriver.spectrometer.usb325.USB325", 9303, 4132));
      addToMap(new USBProductInfo("USB250", "com.oceanoptics.omnidriver.spectrometer.usb250.USB250", 9303, 4144));
      addToMap(new USBProductInfo("USB500", "com.oceanoptics.omnidriver.spectrometer.usb500.USB500", 9303, 4150));
      addToMap(new USBProductInfo("Jaz", "com.oceanoptics.omnidriver.spectrometer.jaz.JazUSB", 9303, 8192));
      addToMap(new USBProductInfo("OceanFX", "com.oceanoptics.omnidriver.spectrometer.flamex.FlameXUSB", 9303, 8193));
      //BROKEN! //addToMap(new USBProductInfo("Blaze", "com.oceanoptics.omnidriver.spectrometer.blaze.BlazeUSB", 9303, 8194));
      addToMap(new USBProductInfo("S8378", "com.oceanoptics.omnidriver.spectrometer.s8378Prototype.S8378", 9303, 4146));
      addToMap(new USBProductInfo("MMS-Raman", "com.oceanoptics.omnidriver.spectrometer.mmsraman.ramanspectrometer.MMSRaman", 6220, 0));
      addToMap(new USBProductInfo("MayaPro2000", "com.oceanoptics.omnidriver.spectrometer.mayapro2000.MayaPro2000", 9303, 4138));
      addToMap(new USBProductInfo("Maya2000", "com.oceanoptics.omnidriver.spectrometer.maya2000.Maya2000", 9303, 4140));
      addToMap(new USBProductInfo("NIRQuest-256", "com.oceanoptics.omnidriver.spectrometer.nirquest.nirquest256.NIRQuest256", 9303, 4136));
      addToMap(new USBProductInfo("NIRQuest-512", "com.oceanoptics.omnidriver.spectrometer.nirquest.nirquest512.NIRQuest512", 9303, 4134));
      addToMap(new USBProductInfo("STS", "com.oceanoptics.omnidriver.spectrometer.sts.STS", 9303, 16384));
      addToMap(new USBProductInfo("Pancake", "com.oceanoptics.omnidriver.spectrometer.sts.Pancake", 9303, 16896));
      addToMap(new USBProductInfo("FluxData", "com.oceanoptics.omnidriver.spectrometer.sts.FluxData", 11047, 55297));
      addToMap(new USBProductInfo("QE-PRO", "com.oceanoptics.omnidriver.spectrometer.qepro.QEPro", 9303, 16388));
      addToMap(new USBProductInfo("Torus", "com.oceanoptics.omnidriver.spectrometer.torus.Torus", 9303, 4160));
      addToMap(new USBProductInfo("Apex", "com.oceanoptics.omnidriver.spectrometer.apex.Apex", 9303, 4164));
      addToMap(new USBProductInfo("Ventana", "com.oceanoptics.omnidriver.spectrometer.ventana.Ventana", 9303, 20480));
      addToMap(new USBProductInfo("Maya LSL", "com.oceanoptics.omnidriver.spectrometer.mayalsl.MayaLSL", 9303, 4166));
      addToMap(new USBProductInfo("1064M", "com.oceanoptics.omnidriver.spectrometer.w1064m.W1064m", 9303, 20481));
      addToMap(new USBProductInfo("1064VNIR", "com.oceanoptics.omnidriver.spectrometer.w1064visnir.W1064VisNir", 9303, 20482));
*/
/*     */   }
/*     */   
/*     */ 
/* 328 */   private static String __extern__ = "__extern__\n<init>,(Ljava/lang/String;Ljava/lang/String;II)V\ngetProductInfo,(II)Lcom/oceanoptics/omnidriver/constants/USBProductInfo;\n";
/*     */ }


/* Location:              /opt/omniDriver/OOI_HOME/OmniDriver.jar!/com/oceanoptics/omnidriver/constants/USBProductInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */