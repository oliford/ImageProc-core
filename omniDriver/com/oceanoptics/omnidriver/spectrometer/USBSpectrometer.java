/*      */ package com.oceanoptics.omnidriver.spectrometer;
/*      */ 
/*      */ import com.oceanoptics.omnidriver.constants.USBProductInfo;
/*      */ import com.oceanoptics.omnidriver.features.USBFeature;
/*      */ import com.oceanoptics.omnidriver.features.USBImpl;
/*      */ import com.oceanoptics.omnidriver.features.hardwaretrigger.HardwareTrigger;
/*      */ import com.oceanoptics.omnidriver.features.pluginprovider.PlugInProvider;
/*      */ import com.oceanoptics.omnidriver.features.rawusb.RawUSB;
/*      */ import com.oceanoptics.omnidriver.features.rawusb.RawUSBImpl;
/*      */ import com.oceanoptics.omnidriver.features.rawusb.RawUsbGUIProvider;
/*      */ import com.oceanoptics.omnidriver.interfaces.USBInterface;
/*      */ import com.oceanoptics.omnidriver.plugin.SpectrometerPlugIn;
/*      */ import com.oceanoptics.omnidriver.spectra.SpectrometerInfo;
/*      */ import com.oceanoptics.omnidriver.spectra.Spectrum;
/*      */ import com.oceanoptics.omnidriver.spectrometer.qepro.QEPro;
/*      */ import com.oceanoptics.omnidriver.spectrometer.sts.FluxData;
/*      */ import com.oceanoptics.omnidriver.spectrometer.sts.Pancake;
/*      */ import com.oceanoptics.uniusb.USBEndpointDescriptor;
/*      */ import com.oceanoptics.uniusb.UniUSBPipeManager;
/*      */ import com.oceanoptics.utilities.ByteRoutines;
/*      */ import com.oceanoptics.utilities.OSInformation;
/*      */ import java.io.File;
/*      */ import java.io.IOException;
/*      */ import java.io.PrintStream;
/*      */ import java.io.UnsupportedEncodingException;
/*      */ import java.util.HashMap;
/*      */ import java.util.LinkedList;
/*      */ import java.util.Vector;
/*      */ import java.util.logging.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public abstract class USBSpectrometer extends Spectrometer  implements RawUSB
/*      */ {
/*      */   protected static final int MAX_USB_DEVICES = 64;
/*   54 */   private static int MAX_PLUGINS = 6;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected SpectrometerPlugIn[] plugIn;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*   64 */   protected int deviceIndex = -1;
/*      */   
/*      */ 
/*      */   protected USBInterface usb;
/*      */   
/*      */ 
/*      */   protected byte[] out;
/*      */   
/*      */ 
/*      */   protected byte[] in;
/*      */   
/*      */ 
/*      */   protected boolean timeoutOccurredFlag;
/*      */   
/*   78 */   protected int vendorID = 9303;
/*      */   
/*      */ 
/*      */   protected int productID;
/*      */   
/*      */ 
/*      */   PollingThread[] pollingThreads;
/*      */   
/*      */ 
/*   87 */   protected HashMap featureMap = new HashMap();
/*      */   
/*      */ 
/*      */ 
/*   91 */   protected USBEndpointDescriptor dataOutEndPoint = null;
/*   92 */   protected USBEndpointDescriptor dataOutEndPoint2 = null;
/*   93 */   protected USBEndpointDescriptor highSpeedInEndPoint1 = null;
/*   94 */   protected USBEndpointDescriptor highSpeedInEndPoint2 = null;
/*   95 */   protected USBEndpointDescriptor lowSpeedInEndPoint = null;
/*   96 */   protected USBEndpointDescriptor lowSpeedInEndPoint2 = null;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected RawUsbGUIProvider raw;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public USBSpectrometer(boolean driver)
/*      */     throws IOException
/*      */   {
/*  110 */     if (((this instanceof QEPro)) || ((this instanceof Pancake)) || ((this instanceof FluxData)))
/*      */     {
/*      */ 
/*      */ 
/*  114 */       driver = false;
/*  115 */       if (OSInformation.isWindows()) {
/*  116 */         driver = true;
/*      */       }
/*      */     }
				if(driver == true)
					System.out.println("rar!");
	
/*  119 */     this.usb = new USBImpl(driver);
/*  120 */     this.in = this.usb.getInputBuffer();
/*  121 */     this.out = this.usb.getOutputBuffer();
/*  122 */     this.timeoutOccurredFlag = false;
/*  123 */     this.raw = new RawUSBImpl(this.usb);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void finishConstruction()
/*      */     throws IOException
/*      */   {
/*  132 */     super.finishConstruction();
/*      */     
/*  134 */     getScoreboard()[this.deviceIndex] = this;
/*  135 */     getCoefficientsFromSpectrometer();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  142 */     this.formattingThread = new Spectrometer.FormattingThread(getName() + " FormattingThread");
/*  143 */     this.formattingThread.setDaemon(true);
/*  144 */     this.formattingThread.start();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  155 */     this.pollingThreads = new PollingThread[this.channels.length];
/*      */     try
/*      */     {
/*  158 */       Class[] pluginClasses = null;
/*  159 */       if ((this instanceof PlugInProvider)) {
/*  160 */         SpectrometerPlugIn[] plugins = ((PlugInProvider)this).getPlugIns();
/*  161 */         if (null != plugins) {
/*  162 */           pluginClasses = new Class[plugins.length];
/*  163 */           for (int i = 0; i < plugins.length; i++) {
/*  164 */             pluginClasses[i] = plugins[i].getClass();
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  180 */       this.spectrumBase = new SpectrometerInfo(getSerialNumber(), getFirmwareVersion(), getClass(), pluginClasses, this.numChannels, getNumberOfCCDPixels(), getNumberOfDarkCCDPixels(), getMaxIntensity(), getIntegrationTimeMinimum(), getIntegrationTimeMaximum(), getIntegrationTimeIncrement(), getIntegrationTimeBase());
/*      */     } catch (IOException e) {
/*  182 */       e.printStackTrace();
/*      */     }
/*      */     
/*  185 */     for (int i = 0; i < this.numChannels; i++) {
/*  186 */       if (null != this.channels[i]) {
/*  187 */         this.channels[i].generateMetadata(this.spectrumBase, i);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public byte[] getInputBuffer()
/*      */   {
/*  197 */     return this.in;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public byte[] getOutputBuffer()
/*      */   {
/*  206 */     return this.out;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected abstract Spectrometer[] getScoreboard();
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void openNextUnclaimed()
/*      */     throws IOException
/*      */   {
/*  230 */     openNextUnclaimedUSB();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void openNextUnclaimedUSB()
/*      */     throws IOException
/*      */   {
/*  239 */     Spectrometer[] scoreboard = getScoreboard();
/*      */     
/*  241 */     for (int i = 0; i < 64; i++)
/*  242 */       if (scoreboard[i] != null)
/*      */       {
/*      */         try {
/*  245 */           this.logger.finest("Testing if " + scoreboard[i].getName() + " at " + i + " is still open/alive.");
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  251 */           scoreboard[i].getSerialNumber();
/*      */         }
/*      */         catch (IOException e)
/*      */         {
/*      */           try {
/*  256 */             scoreboard[i].closeSpectrometer();
/*      */           } catch (IOException localIOException1) {}
/*  258 */           scoreboard[i] = null;
/*      */         }
/*      */         
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */         try
/*      */         {
/*  267 */           openSpectrometer(i);
/*      */         }
/*      */         catch (IOException e)
/*      */         {
/*      */           continue;
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*  276 */         this.logger.finer("Found spectrometer: " + getName() + ", at USB Location " + i);
/*      */         
/*  278 */         scoreboard[i] = this;
/*  279 */         return;
/*      */       }
/*  281 */     this.logger.finest("No additional spectrometers of type " + getName() + " found.");
/*      */     
/*      */ 
/*  284 */     throw new IOException("No additional spectrometers of type " + getName() + " found.");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public USBSpectrometer[] getOpenSpectrometersOfThisType()
/*      */   {
/*  294 */     Vector specs = new Vector();
/*  295 */     Spectrometer[] scoreboard = getScoreboard();
/*  296 */     for (int i = 0; i < scoreboard.length; i++) {
/*  297 */       if (scoreboard[i] != null) {
/*  298 */         specs.add(scoreboard[i]);
/*      */       }
/*      */     }
/*      */     
/*  302 */     return (USBSpectrometer[])specs.toArray(new USBSpectrometer[0]);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void initialize()
/*      */     throws IOException
/*      */   {
/*  329 */     synchronized (this.out) {
/*  330 */       this.out[0] = 1;
/*  331 */       this.usb.bulkOut(this.dataOutEndPoint, this.out, 1);
/*  332 */       this.logger.finest("Spectrometer initialized");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public byte[] readRawUSB(int inEP, int length)
/*      */     throws IOException
/*      */   {
/*  348 */     return this.raw.readRawUSB(inEP, length);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int writeRawUSB(int outEP, byte[] message, int length)
/*      */     throws IOException
/*      */   {
/*  364 */     return this.raw.writeRawUSB(outEP, message, length);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIntegrationTime(int intTime)
/*      */     throws IOException
/*      */   {
/*  385 */     synchronized (this.out)
/*      */     {
/*      */ 
/*  388 */       boolean needStabilityScan = this.integrationTime != intTime;
/*      */       
/*      */ 
/*  391 */       if (!needStabilityScan) {
/*  392 */         this.logger.fine("Desired integration time already set, not pushing to spectrometer");
/*  393 */         return;
/*      */       }
/*      */       
/*  396 */       int maxTime = getIntegrationTimeMaximum();
/*  397 */       int minTime = getIntegrationTimeMinimum();
/*      */       
/*  399 */       if (intTime < minTime) {
/*  400 */         intTime = minTime;
/*  401 */       } else if (intTime > maxTime) {
/*  402 */         intTime = maxTime;
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  409 */       this.integrationTime = intTime;
/*  410 */       intTime /= getIntegrationTimeBase();
/*      */       
/*      */ 
/*      */ 
/*  414 */       this.out[0] = 2;
/*  415 */       this.out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(intTime));
/*  416 */       this.out[2] = ByteRoutines.getHighByte(ByteRoutines.getLowWord(intTime));
/*  417 */       this.out[3] = ByteRoutines.getLowByte(ByteRoutines.getHighWord(intTime));
/*  418 */       this.out[4] = ByteRoutines.getHighByte(ByteRoutines.getHighWord(intTime));
/*      */       
/*  420 */       this.usb.bulkOut(this.dataOutEndPoint, this.out, 5);
/*      */       
/*  422 */       if ((needStabilityScan) && (isStabilityScan())) {
/*  423 */         doStabilityScan(1);
/*      */       }
/*  425 */       this.logger.fine("Integration time set to: " + intTime);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public int setTimeout(int timeoutMilliseconds)
/*      */   {
/*      */     int returnCode;
/*      */     
/*  434 */     synchronized (this.out)
/*      */     {
/*      */       try {
/*  437 */         returnCode = this.usb.setTimeout(this.highSpeedInEndPoint1, timeoutMilliseconds);
/*  438 */         if (returnCode == 1)
/*  439 */           returnCode = this.usb.setTimeout(this.highSpeedInEndPoint2, timeoutMilliseconds);
/*  440 */         if (returnCode == 1) {
/*  441 */           returnCode = this.usb.setTimeout(this.lowSpeedInEndPoint, timeoutMilliseconds);
/*      */         }
/*      */       }
/*      */       catch (IOException exception) {
/*  445 */         return -1;
/*      */       } }
/*  448 */     return returnCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStrobeEnable(boolean strobe)
/*      */     throws IOException
/*      */   {
/*  466 */     synchronized (this.out)
/*      */     {
/*  468 */       if (this.strobeOn == null) {
/*  469 */         this.strobeOn = new Boolean(strobe);
/*      */ 
/*      */       }
/*  472 */       else if (this.strobeOn.booleanValue() == strobe) {
/*  473 */         this.logger.fine("Desired strobe enable state already set, not pushing to spectrometer");
/*      */         
/*  475 */         return;
/*      */       }
/*      */       
/*      */ 
/*  479 */       int on = !strobe ? 0 : 1;
/*  480 */       this.out[0] = 3;
/*  481 */       this.out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(on));
/*  482 */       this.out[2] = ByteRoutines.getHighByte(ByteRoutines.getLowWord(on));
/*      */       
/*  484 */       this.usb.bulkOut(this.dataOutEndPoint, this.out, 3);
/*  485 */       if ((strobe != this.strobeOn.booleanValue()) && (isStabilityScan())) {
/*  486 */         doStabilityScan(1);
/*      */       }
/*  488 */       if (strobe == true) {
/*  489 */         this.strobeOn = Boolean.TRUE;
/*      */       } else {
/*  491 */         this.strobeOn = Boolean.FALSE;
/*      */       }
/*  493 */       this.logger.fine("Strobe enabled: " + strobe);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStrobeDelay(int delay)
/*      */     throws IOException
/*      */   {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPowerState(boolean power)
/*      */     throws IOException
/*      */   {
/*  536 */     synchronized (this.out) {
/*  537 */       int sd = !power ? 0 : 1;
/*  538 */       this.out[0] = 4;
/*  539 */       this.out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(sd));
/*  540 */       this.out[2] = ByteRoutines.getHighByte(ByteRoutines.getLowWord(sd));
/*  541 */       this.usb.bulkOut(this.dataOutEndPoint, this.out, 5);
/*  542 */       this.logger.fine("Power mode (0-off, 1 on): " + sd);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getInfo(int slot)
/*      */     throws IOException
/*      */   {
/*  568 */     synchronized (this.in) {
/*  569 */       synchronized (this.out) {
/*  570 */         this.out[0] = 5;
/*  571 */         this.out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(slot));
/*  572 */         String strRet = "";
/*  573 */         this.usb.bulkOut(this.dataOutEndPoint, this.out, 2);
/*  574 */         this.usb.bulkIn(this.lowSpeedInEndPoint, this.in, 17);
/*  575 */         int i = 2;
/*      */         
/*  577 */         while ((this.in[i] != 0) && (i < 17)) {
/*  578 */           strRet = strRet + (char)this.in[i];
/*  579 */           i++;
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*  584 */         if ((5 == slot) && 
/*  585 */           (i < 16) && (this.in[i] == 0) && 
/*  586 */           (this.in[(i + 1)] != 0))
/*      */         {
/*  588 */           i++;
/*  589 */           strRet = strRet + " ";
/*  590 */           while ((this.in[i] != 0) && (i < 17)) {
/*  591 */             strRet = strRet + (char)this.in[i];
/*  592 */             i++;
/*      */           }
/*      */         }
/*      */         
/*      */ 
/*  597 */         return strRet;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInfo(int slot, String str)
/*      */     throws IOException
/*      */   {
/*  621 */     clearOutBuffer();
/*  622 */     synchronized (this.out) {
/*  623 */       this.out[0] = 6;
/*  624 */       this.out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(slot));
/*      */       try {
/*  626 */         byte[] bytes = str.getBytes("US-ASCII");
/*      */         
/*  628 */         for (int i = 0; i < bytes.length; i++) {
/*  629 */           this.out[(2 + i)] = bytes[i];
/*      */         }
/*      */       } catch (UnsupportedEncodingException e) {
/*  632 */         this.logger.severe("Error encoding configuration variables.");
/*      */         
/*      */ 
/*  635 */         throw new IOException("Error encoding configuration variables: " + e.getMessage());
/*      */       }
/*      */       
/*  638 */       this.usb.bulkOut(this.dataOutEndPoint, this.out, 17);
/*  639 */       this.logger.fine("Configuration variable written to EEPROM.");
/*      */       try
/*      */       {
/*  642 */         Thread.sleep(200L);
/*      */       } catch (InterruptedException e) {
/*  644 */         this.logger.severe("EEPROM write might not have completed. Please verify.");
/*      */         
/*  646 */         throw new IOException("EEPROM write might not have completed. Please verify.");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public byte[] getInfoBytes(int slot)
/*      */     throws IOException
/*      */   {
/*  674 */     synchronized (this.in) {
/*  675 */       synchronized (this.out) {
/*  676 */         byte[] byteArray = new byte[15];
/*  677 */         this.out[0] = 5;
/*  678 */         this.out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(slot));
/*  679 */         String strRet = "";
/*  680 */         this.usb.bulkOut(this.dataOutEndPoint, this.out, 2);
/*  681 */         this.usb.bulkIn(this.lowSpeedInEndPoint, this.in, 17);
/*  682 */         for (int i = 0; i < 15; i++) {
/*  683 */           byteArray[i] = this.in[(i + 2)];
/*      */         }
/*  685 */         return byteArray;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInfoBytes(int slot, byte[] byteArray)
/*      */     throws IOException
/*      */   {
/*  709 */     clearOutBuffer();
/*  710 */     synchronized (this.out) {
/*  711 */       this.out[0] = 6;
/*  712 */       this.out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(slot));
/*  713 */       if (byteArray.length > 15) {
/*  714 */         throw new IOException("Byte array longer than 15 bytes");
/*      */       }
/*  716 */       for (int i = 0; i < byteArray.length; i++) {
/*  717 */         this.out[(i + 2)] = byteArray[i];
/*      */       }
/*      */       
/*      */ 
/*  721 */       this.usb.bulkOut(this.dataOutEndPoint, this.out, 17);
/*  722 */       this.logger.fine("Configuration variable written to EEPROM.");
/*      */       try
/*      */       {
/*  725 */         Thread.sleep(200L);
/*      */       } catch (InterruptedException e) {
/*  727 */         this.logger.severe("EEPROM write might not have completed. Please verify.");
/*      */         
/*  729 */         throw new IOException("EEPROM write might not have completed. Please verify.");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSerialNumber()
/*      */     throws IOException
/*      */   {
/*  742 */     String serialNumber = getInfo(0);
/*  743 */     this.logger.finest("Serial number: " + serialNumber);
/*  744 */     return serialNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSerialNumber(String serialNumber)
/*      */     throws IOException
/*      */   {
/*  767 */     setInfo(0, serialNumber);
/*  768 */     this.logger.fine("New serial number " + serialNumber + " written to EEPROM.");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void requestSpectrum()
/*      */     throws IOException
/*      */   {
/*  780 */     synchronized (this.out) {
/*  781 */       this.logger.finer("Spectrum requested.");
/*  782 */       this.out[0] = 9;
/*  783 */       this.usb.bulkOut(this.dataOutEndPoint, this.out, 1);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void doStabilityScan(int numScans)
/*      */     throws IOException
/*      */   {
/*  795 */     if ((this instanceof HardwareTrigger)) {
/*  796 */       HardwareTrigger ht = (HardwareTrigger)this;
/*  797 */       Integer mode = ht.getExternalTriggerMode();
/*  798 */       if ((mode != null) && (mode.intValue() == 4)) {
/*  799 */         System.err.println("Not doing stability scan!");
/*  800 */         return;
/*      */       }
/*      */     }
/*      */     
/*  804 */     synchronized (this.in) {
/*  805 */       synchronized (this.out) {
/*  806 */         for (int i = 0; i < numScans; i++) {
/*  807 */           requestSpectrum();
/*  808 */           readSpectrum();
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public byte[] getStatusArray()
/*      */     throws IOException
/*      */   {
/*  825 */     synchronized (this.in) {
/*  826 */       synchronized (this.out) {
/*  827 */         this.out[0] = -2;
/*  828 */         byte[] b = new byte[16];
/*  829 */         this.usb.bulkOut(this.dataOutEndPoint, this.out, 1);
/*  830 */         this.usb.bulkIn(this.lowSpeedInEndPoint, b, 16);
/*  831 */         return b;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getDeviceIndex()
/*      */   {
/*  841 */     this.logger.fine("Device index: " + this.deviceIndex);
/*  842 */     return this.deviceIndex;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void closeSpectrometer()
/*      */     throws IOException
/*      */   {
/*  854 */     setStrobeEnable(false);
/*  855 */     getScoreboard()[this.deviceIndex] = null;
/*  856 */     SpectrometerFactory.closeSpectrometer(this);
/*  857 */     this.usb.closeDevice();
/*      */     
/*  859 */     if (this.pollingThreads != null) {
/*  860 */       for (int i = 0; i < this.pollingThreads.length; i++) {
/*  861 */         if (this.pollingThreads[i] != null)
/*      */         {
/*  863 */           this.pollingThreads[i].quit();
/*  864 */           this.pollingThreads[i] = null;
/*      */         }
/*      */       }
/*  867 */       this.pollingThreads = null;
/*      */     }
/*      */     
/*  870 */     if (this.formattingThread != null) {
/*  871 */       this.formattingThread.quit();
/*  872 */       this.formattingThread = null;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  877 */     UniUSBPipeManager.removeDeviceMapping(this.deviceIndex);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFirmwareVersion()
/*      */     throws IOException
/*      */   {
/*  914 */     this.firmwareVersion = this.usb.getUSBStringDescriptor(1);
/*  915 */     if (this.firmwareVersion.length() == 0) {
/*  916 */       this.logger.warning("Attached spectrometer does NOT have a valid firmware version.");
/*  917 */       this.firmwareVersion = "UNKNOWN 0.00.0";
/*      */     }
/*  919 */     parseFirmwareVersion(this.firmwareVersion);
/*  920 */     this.logger.fine("Firmware version: " + this.firmwareVersion);
/*  921 */     return this.firmwareVersion;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Spectrum getSpectrum(Spectrum spectrum)
/*      */     throws IOException
/*      */   {
/*  935 */     this.logger.finest("Getting spectrum...");
/*  936 */     synchronized (this.in) {
/*  937 */       synchronized (this.out) {
/*  938 */         this.timeoutOccurredFlag = false;
/*      */         try {
/*  940 */           requestSpectrum();
/*      */           
/*  942 */           readSpectrum();
/*      */         }
/*      */         catch (IOException exception) {
/*  945 */           this.timeoutOccurredFlag = determineWhetherTimeoutOccurred(exception);
/*  946 */           return null;
/*      */         }
/*      */       }
/*      */     }
/*  950 */     spectrum.setSaturated(false);
/*  951 */     formatData(this.rawData, spectrum);
/*  952 */     return spectrum;
/*      */   }
/*      */   
/*      */   public Spectrum getSpectrumRaw(Spectrum spectrum) throws IOException
/*      */   {
/*  957 */     this.logger.finest("Getting spectrum...");
/*      */     
/*  959 */     synchronized (this.in) {
/*  960 */       synchronized (this.out) {
/*      */         try {
/*  962 */           requestSpectrum();
/*      */           
/*  964 */           readSpectrum();
/*      */         }
/*      */         catch (IOException e) {
/*  967 */           return null;
/*      */         }
/*      */       }
/*      */     }
/*  971 */     spectrum.setSaturated(false);
/*  972 */     formatDataRaw(this.rawData, spectrum);
/*      */     
/*  974 */     return spectrum;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected Spectrum formatDataRaw(byte[] data, Spectrum doubleSpectrum)
/*      */     throws IOException
/*      */   {
/*  988 */     return formatData(data, doubleSpectrum);
/*      */   }
/*      */   
/*      */   public boolean initiateSpectrumAcquisition() {
/*  992 */     synchronized (this.in) {
/*  993 */       synchronized (this.out) {
/*      */         try {
/*  995 */           requestSpectrum();
/*      */         } catch (IOException e) {
/*  997 */           return false;
/*      */         }
/*      */       }
/*      */     }
/* 1001 */     return true;
/*      */   }
/*      */   
/*      */   public Spectrum getAcquiredSpectrum(Spectrum spectrum) throws IOException {
/* 1005 */     synchronized (this.in) {
/* 1006 */       synchronized (this.out) {
/* 1007 */         readSpectrum();
/* 1008 */         spectrum.setSaturated(false);
/* 1009 */         formatData(this.rawData, spectrum);
/*      */       }
/*      */     }
/* 1012 */     return spectrum;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void readSpectrum(byte[] data)
/*      */     throws IOException
/*      */   {
/* 1029 */     synchronized (data) {
/* 1030 */       this.usb.bulkIn(this.highSpeedInEndPoint1, data, this.pipeSize);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void readSpectrum()
/*      */     throws IOException
/*      */   {
/* 1046 */     synchronized (this.rawData) {
/* 1047 */       this.usb.bulkIn(this.highSpeedInEndPoint1, this.rawData, this.pipeSize);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getName()
/*      */   {
/*      */     String str;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 1071 */       str = USBProductInfo.getProductInfo(this.vendorID, this.productID).name;
/*      */     } catch (NullPointerException ne) {
/* 1073 */       str = "Simulation";
/*      */     }
/* 1075 */     return str;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassName()
/*      */   {
/* 1099 */     return USBProductInfo.getProductInfo(this.vendorID, this.productID).name;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   protected void clearInBuffer()
/*      */   {
/* 1106 */     synchronized (this.in) {
/* 1107 */       this.logger.finest("Clearing in buffer.");
/* 1108 */       for (int i = 0; i < this.in.length; i++) {
/* 1109 */         this.in[i] = 0;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   protected void clearOutBuffer()
/*      */   {
/* 1117 */     synchronized (this.out) {
/* 1118 */       this.logger.finest("Clearing out buffer.");
/* 1119 */       for (int i = 0; i < this.out.length; i++) {
/* 1120 */         this.out[i] = 0;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void writeConfigurationToFile(File file) {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfPixels(int index)
/*      */   {
/* 1142 */     return this.channels[index].getNumberOfPixels();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfDarkPixels(int index)
/*      */   {
/* 1155 */     return this.channels[index].getNumberOfDarkPixels();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfDarkCCDPixels()
/*      */   {
/* 1170 */     return this.numberOfDarkCCDPixels;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getNumberOfCCDPixels()
/*      */   {
/* 1183 */     return this.numberOfCCDPixels;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void sendSimulatedTriggerSignal()
/*      */     throws IOException
/*      */   {}
/*      */   
/*      */ 
/*      */ 
/*      */   public void close()
/*      */     throws IOException
/*      */   {
/* 1197 */     if (this.pollingThreads != null) {
/* 1198 */       for (int i = 0; i < this.pollingThreads.length; i++) {
/* 1199 */         if (this.pollingThreads[i] != null)
/*      */         {
/* 1201 */           this.pollingThreads[i].quit();
/* 1202 */           this.pollingThreads[i] = null;
/*      */         }
/*      */       }
/* 1205 */       this.pollingThreads = null;
/*      */     }
/*      */     
/* 1208 */     if (this.formattingThread != null)
/*      */     {
/* 1210 */       this.formattingThread.quit();
/* 1211 */       this.formattingThread = null;
/*      */     }
/*      */     
/*      */     try
/*      */     {
/* 1216 */       Thread.currentThread();Thread.sleep(500L);
/*      */     }
/*      */     catch (InterruptedException localInterruptedException) {}
/* 1219 */     closeSpectrometer();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected class PollingThread
/*      */     extends Thread
/*      */   {
/*      */     private boolean quit;
/*      */     
/*      */ 
/*      */     private int channel;
/*      */     
/*      */ 
/* 1234 */     private long pollingInterval = -1L;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public PollingThread(String name, int index)
/*      */     {
/* 1242 */       super();
/* 1243 */       this.channel = index;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public void run()
/*      */     {
/* 1257 */       while (!this.quit) {
					long startTime=0;
/*      */         try {
/* 1259 */           RawData rawData = new RawData(USBSpectrometer.this.pipeSize);
/* 1260 */           synchronized (this) {
/* 1261 */             USBSpectrometer.this.requestSpectrum();
/* 1262 */             startTime = System.currentTimeMillis();

/* 1263 */             USBSpectrometer.this.readSpectrum(rawData.getData());
/* 1264 */             rawData.setEndTime(System.currentTimeMillis()); 
					}
/*      */           
/* 1266 */           rawData.setStartTime(startTime);
/* 1267 */           rawData.setRequestingChannel(this.channel);
/* 1268 */           synchronized (USBSpectrometer.this.formattingQueue) {
/* 1269 */             USBSpectrometer.this.formattingQueue.add(rawData);
/* 1270 */             USBSpectrometer.this.formattingQueue.notify();
/*      */           }
/*      */           
/* 1273 */           long sleepTime = startTime + this.pollingInterval - System.currentTimeMillis();
/* 1274 */           if (sleepTime > 0L) {
/*      */             try {
/* 1276 */               Thread.currentThread();Thread.sleep(sleepTime);
/*      */             } catch (InterruptedException localInterruptedException) {}
/*      */           }
/*      */         } catch (IOException e) {
/* 1280 */           USBSpectrometer.this.logger.warning("IOException in " + getName());
/* 1281 */           System.out.print("E");
/* 1282 */           System.out.flush();
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public void quit()
/*      */     {
/* 1293 */       this.quit = true;
/* 1294 */       interrupt();
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     public long getPollingInterval()
/*      */     {
/* 1302 */       return this.pollingInterval;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     public void setPollingInterval(long pollingInterval)
/*      */     {
/* 1310 */       if (pollingInterval < 0L) {
/* 1311 */         throw new IllegalArgumentException("Polling interval must be >= 0.");
/*      */       }
/*      */       
/* 1314 */       this.pollingInterval = pollingInterval;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   synchronized long getPollingInterval(int index)
/*      */   {
/* 1324 */     PollingThread pollingThread = this.pollingThreads[index];
/*      */     
/* 1326 */     if (pollingThread == null) {
/* 1327 */       return -1L;
/*      */     }
/* 1329 */     return pollingThread.getPollingInterval();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   synchronized void setPollingInterval(int index, long pollingInterval)
/*      */   {
/* 1342 */     PollingThread pollingThread = this.pollingThreads[index];
/*      */     
/* 1344 */     if (pollingThread == null)
/*      */     {
/* 1346 */       this.pollingThreads[index] = (pollingThread = new PollingThread(getName() + " channel " + index + " PollingThread", index));
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1351 */     long originalInterval = pollingThread.getPollingInterval();
/* 1352 */     pollingThread.setPollingInterval(pollingInterval);
/*      */     
/* 1354 */     if ((originalInterval < 0L) && (pollingInterval >= 0L)) {
/* 1355 */       pollingThread.setDaemon(true);
/* 1356 */       pollingThread.setPriority(7);
/*      */       
/*      */ 
/* 1359 */       pollingThread.start();
/* 1360 */     } else if ((originalInterval >= 0L) && (pollingInterval < 0L)) {
/* 1361 */       pollingThread.quit();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public USBFeature getFeatureController(Class myClass)
/*      */   {
/* 1375 */     return (USBFeature)this.featureMap.get(myClass);
/*      */   }
/*      */   
/* 1378 */   public Object getFeatureControllerUncast(Class myClass) { return this.featureMap.get(myClass); }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isCommunicatingSuccessfully()
/*      */   {
/*      */     try
/*      */     {
/* 1389 */       testSpectrometerCommunication();
/* 1390 */       return true;
/*      */     }
/*      */     catch (Exception e) {}
/* 1393 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String testSpectrometerCommunication()
/*      */     throws IOException
/*      */   {
/* 1407 */     String s = null;
/*      */     
/* 1409 */     if (this.deviceIndex < 0)
/*      */     {
/* 1411 */       return "Unopened " + USBProductInfo.getProductInfo(this.vendorID, this.productID).name;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1416 */     s = getName() + "\n\tSerial number: " + getSerialNumber() + "\n\tFirmware version: " + getFirmwareVersion() + "\n";
/* 1417 */     for (int i = 0; i < this.numChannels; i++) {
/* 1418 */       if (this.channels[i] != null)
/*      */       {
/* 1420 */         s = s + "\nChannel " + i + " Coefficients:\n" + this.channels[i].getCoefficients();
/*      */       }
/*      */     }
/* 1423 */     s = s + "\nConfiguration:\n" + this.configuration;
/*      */     
/* 1425 */     if (this.plugIn != null) {
/* 1426 */       for (int i = 0; i < this.plugIn.length; i++) {
/* 1427 */         if (this.plugIn[i] != null)
/* 1428 */           s = s + this.plugIn[i].toString();
/*      */       }
/*      */     }
/* 1431 */     return s;
/*      */   }
/*      */   
/*      */ 
/*      */   protected boolean determineWhetherTimeoutOccurred(Exception exception)
/*      */   {
/* 1437 */     if (exception.getMessage().indexOf("cause:timeout") != -1) {
/* 1438 */       return true;
/*      */     }
/* 1440 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isTimeout()
/*      */   {
/* 1451 */     return this.timeoutOccurredFlag;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String toString()
/*      */   {
/* 1460 */     String s = null;
/*      */     
/* 1462 */     if (this.deviceIndex < 0)
/*      */     {
/* 1464 */       return "Unopened " + USBProductInfo.getProductInfo(this.vendorID, this.productID).name;
/*      */     }
/*      */     
/*      */ 
/*      */     try
/*      */     {
/* 1470 */       s = getName() + "\n\tSerial number: " + getSerialNumber() + "\n\tFirmware version: " + getFirmwareVersion() + "\n";
/* 1471 */       for (int i = 0; i < this.numChannels; i++) {
/* 1472 */         if (this.channels[i] != null)
/*      */         {
/* 1474 */           s = s + "\nChannel " + i + " Coefficients:\n" + this.channels[i].getCoefficients();
/*      */         }
/*      */       }
/* 1477 */       s = s + "\nConfiguration:\n" + this.configuration;
/*      */     } catch (IOException e) {
/* 1479 */       this.logger.warning("Spectrometer: error getting spectrometer info.");
/* 1480 */       e.printStackTrace();
/* 1481 */       return e.getMessage();
/*      */     }
/* 1483 */     return s;
/*      */   }
/*      */   
/*      */ 
/* 1487 */   private static String __extern__ = "__extern__\n<init>,(Z)V\ngetInputBuffer,()[B\ngetOutputBuffer,()[B\nopenNextUnclaimed,()V\nopenNextUnclaimedUSB,()V\ngetOpenSpectrometersOfThisType,()[Lcom/oceanoptics/omnidriver/spectrometer/USBSpectrometer;\ninitialize,()V\nreadRawUSB,(II)[B\nwriteRawUSB,(I[BI)I\nsetIntegrationTime,(I)V\nsetTimeout,(I)I\nsetStrobeEnable,(Z)V\nsetStrobeDelay,(I)V\nsetPowerState,(Z)V\ngetInfo,(I)Ljava/lang/String;\nsetInfo,(ILjava/lang/String;)V\ngetInfoBytes,(I)[B\nsetInfoBytes,(I[B)V\ngetSerialNumber,()Ljava/lang/String;\nsetSerialNumber,(Ljava/lang/String;)V\ngetStatusArray,()[B\ngetDeviceIndex,()I\ncloseSpectrometer,()V\ngetFirmwareVersion,()Ljava/lang/String;\ngetSpectrum,(Lcom/oceanoptics/omnidriver/spectra/Spectrum;)Lcom/oceanoptics/omnidriver/spectra/Spectrum;\ngetSpectrumRaw,(Lcom/oceanoptics/omnidriver/spectra/Spectrum;)Lcom/oceanoptics/omnidriver/spectra/Spectrum;\ninitiateSpectrumAcquisition,()Z\ngetAcquiredSpectrum,(Lcom/oceanoptics/omnidriver/spectra/Spectrum;)Lcom/oceanoptics/omnidriver/spectra/Spectrum;\ngetName,()Ljava/lang/String;\ngetClassName,()Ljava/lang/String;\nwriteConfigurationToFile,(Ljava/io/File;)V\ngetNumberOfPixels,(I)I\ngetNumberOfDarkPixels,(I)I\ngetNumberOfDarkCCDPixels,()I\ngetNumberOfCCDPixels,()I\nsendSimulatedTriggerSignal,()V\nclose,()V\ngetFeatureController,(Ljava/lang/Class;)Lcom/oceanoptics/omnidriver/features/USBFeature;\ngetFeatureControllerUncast,(Ljava/lang/Class;)Ljava/lang/Object;\nisCommunicatingSuccessfully,()Z\ntestSpectrometerCommunication,()Ljava/lang/String;\nisTimeout,()Z\ntoString,()Ljava/lang/String;\n";
/*      */ }


/* Location:              /opt/omniDriver/OOI_HOME/OmniDriver.jar!/com/oceanoptics/omnidriver/spectrometer/USBSpectrometer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */