/*     */ package com.oceanoptics.omnidriver.spectrometer;
/*     */ 
/*     */ import com.oceanoptics.omnidriver.constants.USBProductInfo;
/*     */ import com.oceanoptics.omnidriver.features.USBImpl;
/*     */ import com.oceanoptics.omnidriver.spectrometer.hr4000.HR4000;
/*     */ import com.oceanoptics.uniusb.USBEndpointDescriptor;
/*     */ import com.oceanoptics.utilities.ByteRoutines;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import java.util.Vector;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SpectrometerFactory
/*     */ {
/*  37 */   static USBProductInfo[] products = (USBProductInfo[])USBProductInfo.products.values().toArray(new USBProductInfo[0]);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  52 */   private static USBLogicalPortStatus[] portStatusArray = new USBLogicalPortStatus[64];
/*  53 */   static { for (int index = 0; index < 64; index++) {
/*  54 */       portStatusArray[index] = new USBLogicalPortStatus();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static synchronized void closeSpectrometer_NOT_USED_YET(Spectrometer spectrometer)
/*     */   {
/*  67 */     for (int portIndex = 0; portIndex < 64; portIndex++)
/*     */     {
/*  69 */       if (portStatusArray[portIndex].isDevicePresent() == true)
/*     */       {
/*  71 */         if (portStatusArray[portIndex].getSpectrometer() == spectrometer)
/*     */         {
/*  73 */           portStatusArray[portIndex].setDeviceNotPresent();
/*  74 */           break;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public static synchronized Spectrometer[] getAllAttachedSpectrometers_NOT_USED_YET()
/*     */   {
/*  82 */     Class clazz = null;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  91 */     for (int portIndex = 0; portIndex < 64; portIndex++)
/*     */     {
/*  93 */       if (portStatusArray[portIndex].isDevicePresent() == true)
/*     */       {
/*     */         try
/*     */         {
/*  97 */           portStatusArray[portIndex].getSpectrometer().getSerialNumber();
/*     */ 
/*     */         }
/*     */         catch (IOException exception)
/*     */         {
/* 102 */           portStatusArray[portIndex].setDeviceNotPresent();
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 110 */     for (int portIndex = 0; portIndex < 64; portIndex++)
/*     */     {
/* 112 */       if (portStatusArray[portIndex].isDevicePresent() != true)
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 118 */         for (int productIndex = 0; productIndex < products.length; productIndex++) {
/*     */           try {
/* 120 */             clazz = Class.forName(products[productIndex].className);
/*     */           } catch (ClassNotFoundException exception) { continue;
/*     */           }
/* 123 */           if (clazz.getName().compareToIgnoreCase("com.oceanoptics.omnidriver.spectrometer.usb2000plus.USB2000Plus") == 0)
/*     */           {
/*     */ 
/*     */ 
/*     */             try
/*     */             {
/*     */ 
/*     */ 
/*     */ 
/* 132 */               short DATA_OUT = 1;
/* 133 */               short LOW_SPEED_DATA_IN = 129;
/* 134 */               short MAX_PACKET_SIZE = 512;
/*     */               
/*     */ 
/* 137 */               USBImpl usbImpl = new USBImpl(false);
/* 138 */               byte[] in = usbImpl.getInputBuffer();
/* 139 */               byte[] out = usbImpl.getOutputBuffer();
/* 140 */               usbImpl.openDevice(9303, 4126, portIndex);
/* 141 */               synchronized (in) {
/* 142 */                 synchronized (out) {
/* 143 */                   out[0] = 5;
/* 144 */                   out[1] = ByteRoutines.getLowByte(ByteRoutines.getLowWord(0));
/* 145 */                   USBEndpointDescriptor dataOutEndPoint = new USBEndpointDescriptor((byte)7, (byte)5, DATA_OUT, (byte)2, MAX_PACKET_SIZE, (byte)0);
/*     */                   
/* 147 */                   USBEndpointDescriptor lowSpeedInEndPoint = new USBEndpointDescriptor((byte)7, (byte)5, LOW_SPEED_DATA_IN, (byte)2, MAX_PACKET_SIZE, (byte)0);
/*     */                   
/*     */ 
/* 150 */                   usbImpl.bulkOut(dataOutEndPoint, out, 2);
/* 151 */                   usbImpl.bulkIn(lowSpeedInEndPoint, in, 17);
/*     */                 }
/*     */               }
/* 154 */               usbImpl.closeDevice();
/* 155 */               USBSpectrometer usbSpectrometer = (USBSpectrometer)clazz.newInstance();
/* 156 */               portStatusArray[portIndex].setDevicePresent(usbSpectrometer);
/*     */             }
/*     */             catch (IOException exception) {}catch (InstantiationException exception) {}catch (IllegalAccessException exception) {}
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 166 */     Vector spectrometers = new Vector();
/* 167 */     for (int portIndex = 0; portIndex < 64; portIndex++)
/*     */     {
/* 169 */       if (portStatusArray[portIndex].isDevicePresent() == true) {
/* 170 */         spectrometers.add(portStatusArray[portIndex].getSpectrometer());
/*     */       }
/*     */     }
/* 173 */     return (Spectrometer[])spectrometers.toArray(new Spectrometer[0]);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Spectrometer[] getAllSpectrometers()
/*     */   {
/* 185 */     Vector spectrometers = new Vector();
/*     */     
/* 187 */     Class clazz = null;
/* 188 */     USBSpectrometer s = null;
/*     */     USBSpectrometer[] ofType;
/* 190 */     for (int i = 0; i < products.length; i++) {
/*     */       for (;;) {
/*     */         try {
/* 193 */           clazz = Class.forName(products[i].className);
/* 194 */           s = (USBSpectrometer)clazz.newInstance();
/*     */           try {
/* 196 */             s.getSerialNumber();
/*     */           }
/*     */           catch (IOException e) {
/* 199 */              ofType = s.getOpenSpectrometersOfThisType();
/* 200 */             int j = 0; if (j < ofType.length) {
/* 201 */               spectrometers.add(ofType[j]);j++; continue;
/*     */             }
/* 203 */             break;
/*     */           }
/*     */         } catch (ClassNotFoundException ex) {
/* 206 */           ex.printStackTrace();
/*     */         } catch (InstantiationException ex) {
/* 208 */           ex.printStackTrace();
/*     */         } catch (IllegalAccessException ex) {
/* 210 */           ex.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/* 214 */     Spectrometer[] spp = (Spectrometer[])spectrometers.toArray(new Spectrometer[0]);
/* 215 */     return spp;
/*     */   }
/*     */   
/*     */   public static Spectrometer[] getNoSpectrometersLinux64()
/*     */   {
/* 220 */     Vector spectrometers = new Vector();
/* 221 */     return (Spectrometer[])spectrometers.toArray(new Spectrometer[0]);
/*     */   }
/*     */   
/*     */   public static Spectrometer[] getAllSpectrometersLinux64() {
/* 225 */     Vector spectrometers = new Vector();
/*     */     
/* 227 */     Class clazz = null;
/* 228 */     USBSpectrometer s = null;
/*     */     
/* 230 */     System.out.println("SpectrometerFactory.getAllSpectrometersLinux64() entered");
/* 231 */     int numberOfSpectrometerTypes = 1;
/*     */     
/*     */     USBSpectrometer[] ofType;
/* 234 */     for (int i = 0; i < numberOfSpectrometerTypes; i++) {
/*     */       try {
/*     */         for (;;) {
/* 237 */           switch (i)
/*     */           {
/*     */           case 0: 
/* 240 */             s = new HR4000();
/* 241 */             break;
/*     */           default: 
/* 243 */             s = null;
/*     */           }
/*     */           
/*     */           try
/*     */           {
/* 248 */             s.getSerialNumber();
/*     */           }
/*     */           catch (IOException e) {
/* 251 */             ofType = s.getOpenSpectrometersOfThisType();
/* 252 */             for (int j = 0; j < ofType.length; j++) {
/* 253 */               spectrometers.add(ofType[j]);
/*     */             }
/* 255 */             break;
/*     */           }
/*     */         }
/* 258 */       } catch (IOException ex) { ex.printStackTrace();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 268 */     return (Spectrometer[])spectrometers.toArray(new Spectrometer[0]);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Spectrometer[] getAllUnclaimedSpectrometers()
/*     */   {
/* 280 */     Vector spectrometers = new Vector();
/* 281 */     Class clazz = null;
/* 282 */     Spectrometer s = null;
/*     */     
/* 284 */     for (int i = 0; i < products.length; i++) {
/*     */       for (;;) {
/*     */         try {
/* 287 */           clazz = Class.forName(products[i].className);
/* 288 */           s = (Spectrometer)clazz.newInstance();
/*     */           try {
/* 290 */             s.getSerialNumber();
/* 291 */             spectrometers.add(s);
/*     */           } catch (IOException e) {
/*     */             break;
/*     */           }
/*     */         } catch (ClassNotFoundException ex) {
/* 296 */           ex.printStackTrace();
/*     */         } catch (InstantiationException ex) {
/* 298 */           ex.printStackTrace();
/*     */         } catch (IllegalAccessException ex) {
/* 300 */           ex.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/* 304 */     return (Spectrometer[])spectrometers.toArray(new Spectrometer[0]);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 500 */   private static String __extern__ = "__extern__\ncloseSpectrometer,(Lcom/oceanoptics/omnidriver/spectrometer/Spectrometer;)V\ncloseSpectrometer_NOT_USED_YET,(Lcom/oceanoptics/omnidriver/spectrometer/Spectrometer;)V\ngetAllAttachedSpectrometers_NOT_USED_YET,()[Lcom/oceanoptics/omnidriver/spectrometer/Spectrometer;\ngetAllSpectrometers,()[Lcom/oceanoptics/omnidriver/spectrometer/Spectrometer;\ngetNoSpectrometersLinux64,()[Lcom/oceanoptics/omnidriver/spectrometer/Spectrometer;\ngetAllSpectrometersLinux64,()[Lcom/oceanoptics/omnidriver/spectrometer/Spectrometer;\ngetAllUnclaimedSpectrometers,()[Lcom/oceanoptics/omnidriver/spectrometer/Spectrometer;\n";
/*     */   
/*     */   public static synchronized void closeSpectrometer(Spectrometer spectrometer) {}
/*     */ }


/* Location:              /opt/omniDriver/OOI_HOME/OmniDriver.jar!/com/oceanoptics/omnidriver/spectrometer/SpectrometerFactory.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */