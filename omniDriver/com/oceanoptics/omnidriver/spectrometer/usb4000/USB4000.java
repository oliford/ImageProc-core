/*      */ package com.oceanoptics.omnidriver.spectrometer.usb4000;
/*      */ 
/*      */ import com.oceanoptics.omnidriver.constants.ExternalTriggerMode;
/*      */ import com.oceanoptics.omnidriver.features.advancedintegrationclock.AdvancedIntegrationClock;
/*      */ import com.oceanoptics.omnidriver.features.advancedintegrationclock.AdvancedIntegrationClockImpl;
/*      */ import com.oceanoptics.omnidriver.features.advancedversion.AdvancedVersion;
/*      */ import com.oceanoptics.omnidriver.features.advancedversion.AdvancedVersionImpl;
/*      */ import com.oceanoptics.omnidriver.features.autonull.AutoNull;
/*      */ import com.oceanoptics.omnidriver.features.autonull.AutoNullFX2;
/*      */ import com.oceanoptics.omnidriver.features.autonull.AutoNullGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.autonull.AutoNullImpl;
/*      */ import com.oceanoptics.omnidriver.features.boardtemperature.BoardTemperature;
/*      */ import com.oceanoptics.omnidriver.features.boardtemperature.BoardTemperatureGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.boardtemperature.BoardTemperatureImpl;
/*      */ import com.oceanoptics.omnidriver.features.continuousstrobe.ContinuousStrobe;
/*      */ import com.oceanoptics.omnidriver.features.continuousstrobe.ContinuousStrobeGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.continuousstrobe.ContinuousStrobeImpl;
/*      */ import com.oceanoptics.omnidriver.features.continuousstrobe.ContinuousStrobeImpl_FPGA;
/*      */ import com.oceanoptics.omnidriver.features.continuousstrobe.ContinuousStrobeImpl_FPGA_32bit;
/*      */ import com.oceanoptics.omnidriver.features.errorprovider.GatingErrorProvider;
/*      */ import com.oceanoptics.omnidriver.features.errorprovider.GatingErrorProviderImpl;
/*      */ import com.oceanoptics.omnidriver.features.externaltriggerdelay.ExternalTriggerDelay;
/*      */ import com.oceanoptics.omnidriver.features.externaltriggerdelay.ExternalTriggerDelayGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.externaltriggerdelay.ExternalTriggerDelayImpl;
/*      */ import com.oceanoptics.omnidriver.features.externaltriggerdelay.ExternalTriggerDelayImpl_USB2kp;
/*      */ import com.oceanoptics.omnidriver.features.gpio.GPIO;
/*      */ import com.oceanoptics.omnidriver.features.gpio.GPIOGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.gpio.GPIOImpl;
/*      */ import com.oceanoptics.omnidriver.features.hardwaretrigger.HardwareTrigger;
/*      */ import com.oceanoptics.omnidriver.features.hardwaretrigger.HardwareTriggerImpl;
/*      */ import com.oceanoptics.omnidriver.features.hardwaretrigger.HardwareTriggerImplFPGA;
/*      */ import com.oceanoptics.omnidriver.features.i2cbus.I2CBus;
/*      */ import com.oceanoptics.omnidriver.features.i2cbus.I2CBusGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.i2cbus.I2CBusImpl;
/*      */ import com.oceanoptics.omnidriver.features.i2cbus.I2CBusImplFPGA;
/*      */ import com.oceanoptics.omnidriver.features.irradiancecalibrationfactor.IrradianceCalibrationFactor;
/*      */ import com.oceanoptics.omnidriver.features.irradiancecalibrationfactor.IrradianceCalibrationFactorGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.irradiancecalibrationfactor.IrradianceCalibrationFactorImpl;
/*      */ import com.oceanoptics.omnidriver.features.irradiancecalibrationfactor.IrradianceCalibrationFactorImplFPGA;
/*      */ import com.oceanoptics.omnidriver.features.masterclockdivisor.MasterClockDivisor;
/*      */ import com.oceanoptics.omnidriver.features.masterclockdivisor.MasterClockDivisorImpl;
/*      */ import com.oceanoptics.omnidriver.features.nonlinearitycorrection.NonlinearityCorrectionGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.nonlinearitycorrection.NonlinearityCorrectionImpl;
/*      */ import com.oceanoptics.omnidriver.features.nonlinearitycorrection.NonlinearityCorrectionProvider;
/*      */ import com.oceanoptics.omnidriver.features.pluginprovider.PlugInProvider;
/*      */ import com.oceanoptics.omnidriver.features.pluginprovider.PlugInProviderImpl;
/*      */ import com.oceanoptics.omnidriver.features.pluginprovider.PlugInProviderImplFPGA;
/*      */ import com.oceanoptics.omnidriver.features.shutterclock.ShutterClock;
/*      */ import com.oceanoptics.omnidriver.features.shutterclock.ShutterClockImpl;
/*      */ import com.oceanoptics.omnidriver.features.singlestrobe.SingleStrobe;
/*      */ import com.oceanoptics.omnidriver.features.singlestrobe.SingleStrobeGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.singlestrobe.SingleStrobeImpl;
/*      */ import com.oceanoptics.omnidriver.features.spibus.SPIBus;
/*      */ import com.oceanoptics.omnidriver.features.spibus.SPIBusGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.spibus.SPIBusImpl;
/*      */ import com.oceanoptics.omnidriver.features.spibus.SPIBusImplFPGA;
/*      */ import com.oceanoptics.omnidriver.features.statusprovider.StatusProvider;
/*      */ import com.oceanoptics.omnidriver.features.straylightcorrection.StrayLightCorrection;
/*      */ import com.oceanoptics.omnidriver.features.straylightcorrection.StrayLightCorrectionGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.straylightcorrection.StrayLightCorrectionImpl;
/*      */ import com.oceanoptics.omnidriver.features.version.Version;
/*      */ import com.oceanoptics.omnidriver.features.version.VersionGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.version.VersionImpl;
/*      */ import com.oceanoptics.omnidriver.features.wavelengthcalibration.WavelengthCalibrationGUIProvider;
/*      */ import com.oceanoptics.omnidriver.features.wavelengthcalibration.WavelengthCalibrationImpl;
/*      */ import com.oceanoptics.omnidriver.features.wavelengthcalibration.WavelengthCalibrationProvider;
/*      */ import com.oceanoptics.omnidriver.interfaces.AcquisitionListener;
/*      */ import com.oceanoptics.omnidriver.interfaces.GUIProvider;
/*      */ import com.oceanoptics.omnidriver.interfaces.OmniDriverDispatchListener;
/*      */ import com.oceanoptics.omnidriver.interfaces.USBEndpointDevice;
/*      */ import com.oceanoptics.omnidriver.interfaces.USBInterface;
/*      */ import com.oceanoptics.omnidriver.plugin.SpectrometerPlugIn;
/*      */ import com.oceanoptics.omnidriver.spectra.Spectrum;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Coefficients;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Configuration;
/*      */ import com.oceanoptics.omnidriver.spectrometer.Spectrometer;
/*      */ import com.oceanoptics.omnidriver.spectrometer.SpectrometerChannel;
/*      */ import com.oceanoptics.omnidriver.spectrometer.SpectrometerStatus;
/*      */ import com.oceanoptics.omnidriver.spectrometer.USBSpectrometer;
/*      */ import com.oceanoptics.uniusb.USBEndpointDescriptor;
/*      */ import com.oceanoptics.utilities.ByteRoutines;
/*      */ import java.io.File;
/*      */ import java.io.IOException;
/*      */ import java.util.BitSet;
/*      */ import java.util.HashMap;
/*      */ import java.util.Vector;
/*      */ import java.util.logging.Logger;
/*      */ 
/*      */ public class USB4000 extends USBSpectrometer implements PlugInProvider, I2CBus, SPIBus, AdvancedVersion, StatusProvider, ContinuousStrobe, AdvancedIntegrationClock, HardwareTrigger, ShutterClock, SingleStrobe, GPIO, BoardTemperature, IrradianceCalibrationFactor, MasterClockDivisor, ExternalTriggerDelay, NonlinearityCorrectionProvider, WavelengthCalibrationProvider, StrayLightCorrection, AutoNull, Version, GatingErrorProvider, USBEndpointDevice
/*      */ {
/*   91 */   private final int PIPE_SPECTRUM_1 = 1;
/*      */   
/*      */ 
/*      */ 
/*   95 */   private final int PIPE_SPECTRUM_2 = 2;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  103 */   int PACKET_SIZE_1 = 2048;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  111 */   int PACKET_SIZE_2 = 5633;
/*      */   
/*      */ 
/*  114 */   private byte[] inb2 = new byte[this.PACKET_SIZE_2];
/*      */   
/*      */ 
/*      */ 
/*      */   private static final short DATA_OUT = 1;
/*      */   
/*      */ 
/*      */ 
/*      */   private static final short HIGH_SPEED_DATA_IN_1 = 130;
/*      */   
/*      */ 
/*      */   private static final short HIGH_SPEED_DATA_IN_2 = 134;
/*      */   
/*      */ 
/*      */   private static final short LOW_SPEED_DATA_IN = 129;
/*      */   
/*      */ 
/*      */   private static final short MAX_PACKET_SIZE = 512;
/*      */   
/*      */ 
/*  134 */   static Spectrometer[] scoreboard = new Spectrometer[64];
/*      */   
/*      */   private int gatingModeIntegrationTime;
/*      */   
/*  138 */   private boolean freeRunStabilityOnly = false;
/*      */   
/*      */   private PlugInProvider plugInProvider;
/*      */   
/*      */   private AdvancedVersion advancedVersion;
/*      */   
/*      */   private AdvancedIntegrationClock advancedIntegrationClock;
/*      */   
/*      */   private ShutterClock shutterClock;
/*      */   
/*      */   private MasterClockDivisor masterClockDivisor;
/*      */   
/*      */   private I2CBusGUIProvider i2cBus;
/*      */   
/*      */   private SPIBusGUIProvider spiBus;
/*      */   
/*      */   private GPIOGUIProvider gpio;
/*      */   private SingleStrobeGUIProvider singleStrobe;
/*      */   private HardwareTriggerImpl hardwareTrigger;
/*      */   private ContinuousStrobeGUIProvider continuousStrobe;
/*      */   private BoardTemperatureGUIProvider boardTemperature;
/*      */   private IrradianceCalibrationFactorGUIProvider irradianceCalibrationFactor;
/*      */   private ExternalTriggerDelayGUIProvider triggerDelay;
/*      */   private NonlinearityCorrectionGUIProvider nonlinearity;
/*      */   private WavelengthCalibrationGUIProvider wavelength;
/*      */   private StrayLightCorrectionGUIProvider straylight;
/*      */   private AutoNullGUIProvider autonulling;
/*      */   private VersionGUIProvider version;
/*      */   private GatingErrorProviderImpl gatingError;
/*  167 */   private int NUMBER_GPIO_PINS = 8;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected Spectrometer[] getScoreboard()
/*      */   {
/*  179 */     return scoreboard;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public USB4000()
/*      */     throws IOException
/*      */   {
/*  189 */     super(false);
/*      */     try {
/*  191 */       setEndpoints();
/*  192 */       openNextUnclaimedUSB();
/*      */     }
/*      */     catch (Exception localException) {}
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public USB4000(int i)
/*      */     throws IOException
/*      */   {
/*  212 */     super(false);
/*  213 */     setEndpoints();
/*  214 */     openSpectrometer(i);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEndpoints()
/*      */   {
/*  222 */     this.dataOutEndPoint = new USBEndpointDescriptor((byte)7, (byte)5, (short)1, (byte)2, (short)512, (byte)0);
/*      */     
/*  224 */     this.highSpeedInEndPoint1 = new USBEndpointDescriptor((byte)7, (byte)5, (short)130, (byte)2, (short)512, (byte)0);
/*      */     
/*  226 */     this.highSpeedInEndPoint2 = new USBEndpointDescriptor((byte)7, (byte)5, (short)134, (byte)2, (short)512, (byte)0);
/*      */     
/*  228 */     this.lowSpeedInEndPoint = new USBEndpointDescriptor((byte)7, (byte)5, (short)129, (byte)2, (short)512, (byte)0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public USBEndpointDescriptor getEndpoint(int endPoint)
/*      */     throws IllegalArgumentException
/*      */   {
/*  245 */     switch (endPoint)
/*      */     {
/*      */     case 0: 
/*  248 */       return this.dataOutEndPoint;
/*      */     case 1: 
/*  250 */       return this.highSpeedInEndPoint1;
/*      */     case 2: 
/*  252 */       return this.highSpeedInEndPoint2;
/*      */     case 3: 
/*  254 */       return this.lowSpeedInEndPoint;
/*      */     }
/*      */     
/*      */     
/*      */ 
/*  259 */     throw new IllegalArgumentException("End Point number invalid.");
/*      */   }
/*      */   
/*      */ 
/*      */   public boolean allowWriteToEEPROM(int privilegeLevel, int slot)
/*      */   {
/*  265 */     if (slot < 0) {
/*  266 */       return false;
/*      */     }
/*  268 */     if (privilegeLevel == 0)
/*  269 */       return true;
/*  270 */     if (privilegeLevel == 1)
/*      */     {
/*  272 */       if (slot == 0) {
/*  273 */         return false;
/*      */       }
/*  275 */       if ((slot >= 19) && (slot <= 30)) {
/*  276 */         return true;
/*      */       }
/*  278 */       if (slot > 14)
/*  279 */         return false;
/*  280 */       return true;
/*      */     }
/*  282 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private boolean parseFPGAFirmwareVersion()
/*      */   {
/*  290 */     double FPGA = 0.0D;
/*      */     try
/*      */     {
/*  293 */       String fpgaVersion = getFPGAFirmwareVersion();
/*      */       
/*      */ 
/*  296 */       int index3 = fpgaVersion.lastIndexOf('.');
/*  297 */       FPGA = Double.parseDouble(fpgaVersion.substring(0, index3));
/*      */       
/*      */ 
/*      */ 
/*  301 */       if (FPGA >= 3.0D) {
/*  302 */         return true;
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*  307 */       return false;
/*      */     }
/*      */     catch (Exception e) {}
/*      */     
/*      */ 
/*  312 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void openSpectrometer(int index)
/*      */     throws IOException
/*      */   {
/*  341 */     this.integrationTimeMinimum = 10;
/*  342 */     this.integrationTimeMaximum = 65535000;
/*  343 */     this.integrationTimeIncrement = 10;
/*  344 */     this.integrationTimeBase = 1;
/*  345 */     this.gatingModeIntegrationTime = 3800;
/*  346 */     this.numberOfCCDPixels = 3648;
/*  347 */     this.numberOfDarkCCDPixels = 12;
/*  348 */     this.maxIntensity = 65535;
/*  349 */     this.pipeSize = (this.PACKET_SIZE_1 + this.PACKET_SIZE_2);
/*  350 */     this.rawData = new byte[this.PACKET_SIZE_1 + this.PACKET_SIZE_2];
/*  351 */     this.benchSlot = 15;
/*  352 */     this.spectrometerConfigSlot = 16;
/*  353 */     this.productID = 4130;
/*      */     
/*      */ 
/*      */ 
/*  357 */     this.usb.openDevice(this.vendorID, this.productID, index);
/*      */     
/*  359 */     this.deviceIndex = index;
/*  360 */     initialize();
/*      */     
/*  362 */     getFirmwareVersion();
/*  363 */     this.channels = new SpectrometerChannel[1];
/*  364 */     this.channels[this.channelIndex] = new SpectrometerChannel(this, new Coefficients(), this.channelIndex);
/*      */     
/*  366 */     this.logger.fine("USB4000 has been opened at index " + index);
/*      */     
/*      */ 
/*  369 */     initFeatures(this.usb);
/*  370 */     finishConstruction();
/*  371 */     this.configuration = new Configuration(this);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void initFeatures(USBInterface usb)
/*      */     throws IOException
/*      */   {
/*  383 */     this.plugInProvider = new PlugInProviderImplFPGA(usb, this);
/*  384 */     this.advancedVersion = new AdvancedVersionImpl(usb);
/*  385 */     this.advancedIntegrationClock = new AdvancedIntegrationClockImpl(usb);
/*  386 */     this.shutterClock = new ShutterClockImpl(usb);
/*  387 */     this.masterClockDivisor = new MasterClockDivisorImpl(usb);
/*      */     
/*  389 */     this.i2cBus = new I2CBusImplFPGA(usb);
/*  390 */     this.spiBus = new SPIBusImplFPGA(usb);
/*  391 */     this.gpio = new GPIOImpl(usb, this.NUMBER_GPIO_PINS);
/*  392 */     this.singleStrobe = new SingleStrobeImpl(usb);
/*      */     
/*  394 */     if (parseFPGAFirmwareVersion()) {
/*  395 */       this.hardwareTrigger = new HardwareTriggerImplFPGA(usb, new USB4000TriggerMode[] { new USB4000TriggerMode(0), new USB4000TriggerMode(1), new USB4000TriggerMode(2), new USB4000TriggerMode(3), new USB4000TriggerMode(4) });
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  403 */       this.continuousStrobe = new ContinuousStrobeImpl_FPGA_32bit(usb);
/*  404 */       this.triggerDelay = new ExternalTriggerDelayImpl_USB2kp(usb);
/*  405 */       this.integrationTimeIncrement = 1;
/*  406 */       this.freeRunStabilityOnly = true;
/*      */     } else {
/*  408 */       this.hardwareTrigger = new HardwareTriggerImplFPGA(usb, new ExternalTriggerMode[] { new ExternalTriggerMode(0), new ExternalTriggerMode(1), new ExternalTriggerMode(2), new ExternalTriggerMode(3) });
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  416 */       this.continuousStrobe = new ContinuousStrobeImpl_FPGA(usb);
/*  417 */       this.triggerDelay = new ExternalTriggerDelayImpl(usb);
/*      */     }
/*      */     
/*  420 */     this.boardTemperature = new BoardTemperatureImpl(usb);
/*  421 */     this.irradianceCalibrationFactor = new IrradianceCalibrationFactorImplFPGA(usb, this.numberOfCCDPixels);
/*  422 */     this.nonlinearity = new NonlinearityCorrectionImpl(usb, this);
/*  423 */     this.wavelength = new WavelengthCalibrationImpl(usb, this);
/*  424 */     this.straylight = new StrayLightCorrectionImpl(usb, this);
/*  425 */     this.autonulling = new AutoNullFX2(usb);
/*  426 */     this.autonulling.recordSettings();
/*  427 */     this.version = new VersionImpl(usb, this);
/*  428 */     this.gatingError = new GatingErrorProviderImpl(usb, this);
/*  429 */     this.featureMap.put(PlugInProviderImpl.class, (PlugInProviderImpl)this.plugInProvider);
/*  430 */     this.featureMap.put(AdvancedVersionImpl.class, (AdvancedVersionImpl)this.advancedVersion);
/*  431 */     this.featureMap.put(AdvancedIntegrationClockImpl.class, (AdvancedIntegrationClockImpl)this.advancedIntegrationClock);
/*  432 */     this.featureMap.put(ShutterClockImpl.class, (ShutterClockImpl)this.shutterClock);
/*  433 */     this.featureMap.put(MasterClockDivisorImpl.class, (MasterClockDivisorImpl)this.masterClockDivisor);
/*  434 */     this.featureMap.put(I2CBusImpl.class, (I2CBusImpl)this.i2cBus);
/*  435 */     this.featureMap.put(SPIBusImpl.class, (SPIBusImpl)this.spiBus);
/*  436 */     this.featureMap.put(GPIOImpl.class, (GPIOImpl)this.gpio);
/*  437 */     this.featureMap.put(SingleStrobeImpl.class, (SingleStrobeImpl)this.singleStrobe);
/*  438 */     this.featureMap.put(HardwareTriggerImpl.class, this.hardwareTrigger);
/*  439 */     this.featureMap.put(ContinuousStrobeImpl.class, (ContinuousStrobeImpl)this.continuousStrobe);
/*  440 */     this.featureMap.put(BoardTemperatureImpl.class, (BoardTemperatureImpl)this.boardTemperature);
/*  441 */     this.featureMap.put(IrradianceCalibrationFactorImpl.class, (IrradianceCalibrationFactorImpl)this.irradianceCalibrationFactor);
/*  442 */     this.featureMap.put(ExternalTriggerDelayImpl.class, (ExternalTriggerDelayImpl)this.triggerDelay);
/*  443 */     this.featureMap.put(NonlinearityCorrectionImpl.class, (NonlinearityCorrectionImpl)this.nonlinearity);
/*  444 */     this.featureMap.put(WavelengthCalibrationImpl.class, (WavelengthCalibrationImpl)this.wavelength);
/*  445 */     this.featureMap.put(StrayLightCorrectionImpl.class, (StrayLightCorrectionImpl)this.straylight);
/*  446 */     this.featureMap.put(AutoNullImpl.class, (AutoNullImpl)this.autonulling);
/*  447 */     this.featureMap.put(VersionImpl.class, (VersionImpl)this.version);
/*  448 */     this.featureMap.put(GatingErrorProviderImpl.class, this.gatingError);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GUIProvider[] getGUIFeatures()
/*      */   {
/*  459 */     Vector features = new Vector();
/*      */     
/*  461 */     features.add(this.wavelength);
/*  462 */     features.add(this.straylight);
/*  463 */     features.add(this.nonlinearity);
/*  464 */     features.add(this.singleStrobe);
/*  465 */     features.add(this.continuousStrobe);
/*  466 */     features.add(this.triggerDelay);
/*  467 */     features.add(this.boardTemperature);
/*  468 */     features.add(this.irradianceCalibrationFactor);
/*  469 */     features.add(this.spiBus);
/*  470 */     features.add(this.gpio);
/*  471 */     features.add(this.autonulling);
/*  472 */     features.add(this.i2cBus);
/*  473 */     features.add(this.version);
/*      */     
/*  475 */     return (GUIProvider[])features.toArray(new GUIProvider[0]);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void readSpectrum(byte[] data)
/*      */     throws IOException
/*      */   {
/*  489 */     synchronized (data)
/*      */     {
/*      */ 
/*  492 */       this.logger.finest("Reading spectrum.");
/*  493 */       if (this.usb.isUSB2Mode())
/*      */       {
/*      */ 
/*      */ 
/*  497 */         this.logger.finer("USB4000 is connected to a USB 2.0 port");
/*  498 */         this.usb.bulkIn(this.highSpeedInEndPoint2, data, this.PACKET_SIZE_1);
/*  499 */         this.usb.bulkIn(this.highSpeedInEndPoint1, this.inb2, this.PACKET_SIZE_2);
/*      */         
/*  501 */         System.arraycopy(this.inb2, 0, data, this.PACKET_SIZE_1, this.PACKET_SIZE_2);
/*      */       }
/*      */       else
/*      */       {
/*  505 */         this.usb.bulkIn(this.highSpeedInEndPoint1, data, this.PACKET_SIZE_1 + this.PACKET_SIZE_2);
/*  506 */         this.logger.finer("USB4000 is connected to a USB 1.1 port");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void readSpectrum()
/*      */     throws IOException
/*      */   {
/*  522 */     synchronized (this.rawData)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  527 */       this.logger.finest("Reading spectrum.");
/*  528 */       if (this.usb.isUSB2Mode())
/*      */       {
/*      */ 
/*      */ 
/*  532 */         this.logger.finer("USB4000 is connected to a USB 2.0 port");
/*  533 */         this.usb.bulkIn(this.highSpeedInEndPoint2, this.rawData, this.PACKET_SIZE_1);
/*  534 */         this.usb.bulkIn(this.highSpeedInEndPoint1, this.inb2, this.PACKET_SIZE_2);
/*      */         
/*  536 */         System.arraycopy(this.inb2, 0, this.rawData, this.PACKET_SIZE_1, this.PACKET_SIZE_2);
/*      */       }
/*      */       else
/*      */       {
/*  540 */         this.usb.bulkIn(this.highSpeedInEndPoint1, this.rawData, this.PACKET_SIZE_1 + this.PACKET_SIZE_2);
/*  541 */         this.logger.finer("USB4000 is connected to a USB 1.1 port");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected Spectrum formatData(byte[] data, Spectrum doubleSpectrum)
/*      */     throws IOException
/*      */   {
/*  559 */     byte zero = 0;
/*  560 */     double[] spectrum = doubleSpectrum.getSpectrum();
/*      */     
/*  562 */     this.logger.finest("Formatting data");
/*      */     
/*  564 */     doubleSpectrum.setSaturated(false);
/*      */     
/*  566 */     if (data[(this.pipeSize - 1)] != 105) {
/*  567 */       this.logger.severe("Lost synchroniztion");
/*  568 */       throw new IOException("Lost synchronization");
/*      */     }
/*      */     
/*  571 */     double saturationValue = getSaturationValue();
/*      */     
/*  573 */     for (int i = 0; i < this.numberOfCCDPixels; i++) {
/*  574 */       byte LSB = data[(2 * i)];
/*  575 */       byte MSB = data[(2 * i + 1)];
/*  576 */       int pixel = ByteRoutines.makeDWord(zero, zero, MSB, LSB);
/*  577 */       if (pixel >= saturationValue) {
/*  578 */         doubleSpectrum.setSaturated(true);
/*      */       }
/*      */       
/*  581 */       spectrum[i] = (pixel * this.maxIntensity / getSaturationValue());
/*      */     }
/*  583 */     spectrum[0] = spectrum[2];
/*  584 */     spectrum[1] = spectrum[2];
/*      */     
/*  586 */     return doubleSpectrum;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SpectrometerStatus getStatus()
/*      */     throws IOException
/*      */   {
/*  600 */     byte[] sb = super.getStatusArray();
/*  601 */     this.logger.finest("USB4000 Status.");
/*  602 */     USB4000Status stat = new USB4000Status();
/*  603 */     stat.numPixels = ByteRoutines.makeWord(sb[1], sb[0]);
/*      */     
/*  605 */     stat.integrationTime = ByteRoutines.makeDWord(sb[5], sb[4], sb[3], sb[2]);
/*  606 */     stat.lampEnabled = (sb[6] != 0);
/*  607 */     stat.externalTriggerMode = sb[7];
/*  608 */     stat.takingScan = (sb[8] != 0);
/*  609 */     stat.numPacketsInSpectra = sb[9];
/*  610 */     stat.powerDownFlag = sb[10];
/*  611 */     stat.packetCount = sb[11];
/*  612 */     if (Math.abs(sb[14]) == 128) {
/*  613 */       this.logger.finer("USB4000 connected to a USB 2.0 por (480Mbs)t.");
/*  614 */       stat.usbSpeed = 480;
/*  615 */     } else if (Math.abs(sb[14]) == 0) {
/*  616 */       this.logger.finer("USB4000 connected to a USB 1.1 port (12Mbs).");
/*  617 */       stat.usbSpeed = 12;
/*      */     } else {
/*  619 */       stat.usbSpeed = 0;
/*  620 */       this.logger.severe("Error determining USB Communication Speed.");
/*  621 */       throw new IOException("Error determining USB Communication Speed.");
/*      */     }
/*  623 */     return stat;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int readIntegrationTime()
/*      */     throws IOException
/*      */   {
/*  635 */     USB4000Status stat = (USB4000Status)getStatus();
/*  636 */     return stat.integrationTime;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIntegrationTime(int intTime)
/*      */     throws IOException
/*      */   {
/*  648 */     this.gatingError.checkIntegrationTime(intTime);
/*  649 */     super.setIntegrationTime(intTime);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getGatingModeIntegrationTime()
/*      */   {
/*  656 */     return this.gatingModeIntegrationTime;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void addOmniDriverDispatchListener(OmniDriverDispatchListener listener)
/*      */   {
/*  663 */     this.gatingError.addOmniDriverDispatchListener(listener);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void removeOmniDriverDispatchListener(OmniDriverDispatchListener listener)
/*      */   {
/*  670 */     this.gatingError.removeOmniDriverDispatchListener(listener);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStrobeDelay(int delay)
/*      */     throws IOException
/*      */   {
/*  680 */     setContinuousStrobeDelay(delay);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String toString()
/*      */   {
/*      */     try
/*      */     {
/*  693 */       return super.toString() + "\n" + getStatus();
/*      */     } catch (IOException e) {
/*  695 */       e.printStackTrace();
/*  696 */       return e.getMessage();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SpectrometerPlugIn[] getPlugIns()
/*      */     throws IOException
/*      */   {
/*  706 */     return this.plugInProvider.getPlugIns();
/*      */   }
/*      */   
/*      */ 
/*      */   public int getNumberOfPlugIns()
/*      */     throws IOException
/*      */   {
/*  713 */     return this.plugInProvider.getNumberOfPlugIns();
/*      */   }
/*      */   
/*      */ 
/*      */   public boolean isPlugInDetected(int id)
/*      */     throws IOException
/*      */   {
/*  720 */     return this.plugInProvider.isPlugInDetected(id);
/*      */   }
/*      */   
/*      */ 
/*      */   public byte[] initializePlugIns()
/*      */     throws IOException
/*      */   {
/*  727 */     return this.plugInProvider.initializePlugIns();
/*      */   }
/*      */   
/*      */ 
/*      */   public void detectPlugIns()
/*      */     throws IOException
/*      */   {
/*  734 */     this.plugInProvider.detectPlugIns();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int setI2CBytes(byte address, byte numBytes, byte[] i2C)
/*      */     throws IOException
/*      */   {
/*  744 */     return this.i2cBus.setI2CBytes(address, numBytes, i2C);
/*      */   }
/*      */   
/*      */ 
/*      */   public byte[] getI2CBytes(byte address, byte numBytes)
/*      */     throws IOException
/*      */   {
/*  751 */     return this.i2cBus.getI2CBytes(address, numBytes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public byte[] getSPIBytes(byte[] message, int length)
/*      */     throws IOException
/*      */   {
/*  760 */     return this.spiBus.getSPIBytes(message, length);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPSOCVersion()
/*      */     throws IOException
/*      */   {
/*  769 */     return this.advancedVersion.getPSOCVersion();
/*      */   }
/*      */   
/*      */ 
/*      */   public String getFPGAFirmwareVersion()
/*      */     throws IOException
/*      */   {
/*  776 */     return this.advancedVersion.getFPGAFirmwareVersion();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double continuousStrobeCountsToMicros(int counts)
/*      */   {
/*  785 */     return this.continuousStrobe.continuousStrobeCountsToMicros(counts);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setContinuousStrobeDelay(int delayMicros)
/*      */     throws IOException
/*      */   {
/*  792 */     this.continuousStrobe.setContinuousStrobeDelay(delayMicros);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public Integer getContinuousStrobeDelay()
/*      */   {
/*  799 */     return this.continuousStrobe.getContinuousStrobeDelay();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getContinuousStrobeDelayMinimum()
/*      */   {
/*  806 */     return this.continuousStrobe.getContinuousStrobeDelayMinimum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getContinuousStrobeDelayMaximum()
/*      */   {
/*  813 */     return this.continuousStrobe.getContinuousStrobeDelayMaximum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getContinuousStrobeDelayIncrement(int magnitude)
/*      */   {
/*  820 */     return this.continuousStrobe.getContinuousStrobeDelayIncrement(magnitude);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setContinuousModeType(boolean mode)
/*      */     throws IOException
/*      */   {
/*  827 */     this.continuousStrobe.setContinuousModeType(mode);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setDelayAfterIntegration(int delay)
/*      */     throws IOException
/*      */   {
/*  834 */     this.continuousStrobe.setDelayAfterIntegration(delay);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public boolean getContinuousModeType()
/*      */   {
/*  841 */     return this.continuousStrobe.getContinuousModeType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getDelayAfterIntegration()
/*      */   {
/*  848 */     return this.continuousStrobe.getDelayAfterIntegration();
/*      */   }
/*      */   
/*      */   public void setContinuousEnable(boolean value) throws IOException {
/*  852 */     this.continuousStrobe.setContinuousEnable(value);
/*      */   }
/*      */   
/*      */   public boolean getContinuousEnable() {
/*  856 */     return this.continuousStrobe.getContinuousEnable();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAdvancedIntegrationTime(long delayMicros)
/*      */     throws IOException
/*      */   {
/*  865 */     this.advancedIntegrationClock.setAdvancedIntegrationTime(delayMicros);
/*      */   }
/*      */   
/*      */ 
/*      */   public int getIntegrationTimeBaseClock()
/*      */     throws IOException
/*      */   {
/*  872 */     return this.advancedIntegrationClock.getIntegrationTimeBaseClock();
/*      */   }
/*      */   
/*      */ 
/*      */   public int getIntegrationClockTimer()
/*      */     throws IOException
/*      */   {
/*  879 */     return this.advancedIntegrationClock.getIntegrationClockTimer();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public long getAdvancedIntegrationTimeMinimum()
/*      */   {
/*  886 */     return this.advancedIntegrationClock.getAdvancedIntegrationTimeMinimum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public long getAdvancedIntegrationTimeMaximum()
/*      */   {
/*  893 */     return this.advancedIntegrationClock.getAdvancedIntegrationTimeMaximum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public long getAdvancedIntegrationTimeIncrement()
/*      */   {
/*  900 */     return this.advancedIntegrationClock.getAdvancedIntegrationTimeIncrement();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExternalTriggerMode(int mode)
/*      */     throws IOException
/*      */   {
/*  909 */     this.gatingError.checkTriggerNotification(mode);
/*  910 */     this.hardwareTrigger.setExternalTriggerMode(mode);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  919 */     if (true == this.freeRunStabilityOnly) {
/*  920 */       if (0 == mode) {
/*  921 */         this.stabilityScan = true;
/*      */       } else {
/*  923 */         this.stabilityScan = false;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public ExternalTriggerMode[] getExternalTriggerModes()
/*      */   {
/*  932 */     return this.hardwareTrigger.getExternalTriggerModes();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public Integer getExternalTriggerMode()
/*      */   {
/*  939 */     return this.hardwareTrigger.getExternalTriggerMode();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setShutterClock(int value)
/*      */     throws IOException
/*      */   {
/*  948 */     this.shutterClock.setShutterClock(value);
/*      */   }
/*      */   
/*      */ 
/*      */   public int getShutterClock()
/*      */     throws IOException
/*      */   {
/*  955 */     return this.shutterClock.getShutterClock();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSingleStrobeLow(int value)
/*      */     throws IOException
/*      */   {
/*  964 */     this.singleStrobe.setSingleStrobeLow(value);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setSingleStrobeHigh(int value)
/*      */     throws IOException
/*      */   {
/*  971 */     this.singleStrobe.setSingleStrobeHigh(value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public double getSingleStrobeCountsToMicros(int counts)
/*      */   {
/*  978 */     return this.singleStrobe.getSingleStrobeCountsToMicros(counts);
/*      */   }
/*      */   
/*      */ 
/*      */   public int getSingleStrobeLow()
/*      */     throws IOException
/*      */   {
/*  985 */     return this.singleStrobe.getSingleStrobeLow();
/*      */   }
/*      */   
/*      */ 
/*      */   public int getSingleStrobeHigh()
/*      */     throws IOException
/*      */   {
/*  992 */     return this.singleStrobe.getSingleStrobeHigh();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getSingleStrobeMinimum()
/*      */   {
/*  999 */     return this.singleStrobe.getSingleStrobeMinimum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getSingleStrobeMaximum()
/*      */   {
/* 1006 */     return this.singleStrobe.getSingleStrobeMaximum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getSingleStrobeIncrement()
/*      */   {
/* 1013 */     return this.singleStrobe.getSingleStrobeIncrement();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDirectionAllBits(BitSet bitSet)
/*      */     throws IOException
/*      */   {
/* 1022 */     this.gpio.setDirectionAllBits(bitSet);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setMuxAllBits(BitSet bitSet)
/*      */     throws IOException
/*      */   {
/* 1029 */     this.gpio.setMuxAllBits(bitSet);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setValueAllBits(BitSet bitSet)
/*      */     throws IOException
/*      */   {
/* 1036 */     this.gpio.setValueAllBits(bitSet);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setDirectionBitmask(short bitmask)
/*      */     throws IOException
/*      */   {
/* 1043 */     this.gpio.setDirectionBitmask(bitmask);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setMuxBitmask(short bitmask)
/*      */     throws IOException
/*      */   {
/* 1050 */     this.gpio.setMuxBitmask(bitmask);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setValueBitmask(short bitmask)
/*      */     throws IOException
/*      */   {
/* 1057 */     this.gpio.setValueBitmask(bitmask);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setDirectionBit(int bit, boolean value)
/*      */     throws IOException
/*      */   {
/* 1064 */     this.gpio.setDirectionBit(bit, value);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setMuxBit(int bit, boolean value)
/*      */     throws IOException
/*      */   {
/* 1071 */     this.gpio.setMuxBit(bit, value);
/*      */   }
/*      */   
/*      */ 
/*      */   public void setValueBit(int bit, boolean value)
/*      */     throws IOException
/*      */   {
/* 1078 */     this.gpio.setValueBit(bit, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getTotalGPIOBits()
/*      */   {
/* 1085 */     return this.gpio.getTotalGPIOBits();
/*      */   }
/*      */   
/*      */ 
/*      */   public BitSet getDirectionBits()
/*      */     throws IOException
/*      */   {
/* 1092 */     return this.gpio.getDirectionBits();
/*      */   }
/*      */   
/*      */ 
/*      */   public BitSet getMuxBits()
/*      */     throws IOException
/*      */   {
/* 1099 */     return this.gpio.getMuxBits();
/*      */   }
/*      */   
/*      */ 
/*      */   public int getValueBit(int bitNumber)
/*      */     throws IOException
/*      */   {
/* 1106 */     return this.gpio.getValueBit(bitNumber);
/*      */   }
/*      */   
/*      */ 
/*      */   public BitSet getValueBits()
/*      */     throws IOException
/*      */   {
/* 1113 */     return this.gpio.getValueBits();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getNumberOfPins()
/*      */   {
/* 1120 */     return this.gpio.getNumberOfPins();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public double getBoardTemperatureCelsius()
/*      */     throws IOException
/*      */   {
/* 1129 */     return this.boardTemperature.getBoardTemperatureCelsius();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double[] getIrradianceCalibrationFactors()
/*      */     throws IOException
/*      */   {
/* 1139 */     return this.irradianceCalibrationFactor.getIrradianceCalibrationFactors();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void setIrradianceCalibrationFactors(double[] data)
/*      */     throws IOException
/*      */   {
/* 1147 */     this.irradianceCalibrationFactor.setIrradianceCalibrationFactors(data);
/*      */   }
/*      */   
/*      */   public double getCollectionArea() throws IOException {
/* 1151 */     return this.irradianceCalibrationFactor.getCollectionArea();
/*      */   }
/*      */   
/*      */   public boolean hasCollectionArea() {
/* 1155 */     return this.irradianceCalibrationFactor.hasCollectionArea();
/*      */   }
/*      */   
/*      */   public void setCollectionArea(double area) throws IOException {
/* 1159 */     this.irradianceCalibrationFactor.setCollectionArea(area);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMasterClockDivisor(int value)
/*      */     throws IOException
/*      */   {
/* 1168 */     this.masterClockDivisor.setMasterClockDivisor(value);
/*      */   }
/*      */   
/*      */ 
/*      */   public int getMasterClockDivisor()
/*      */     throws IOException
/*      */   {
/* 1175 */     return this.masterClockDivisor.getMasterClockDivisor();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExternalTriggerDelay(int counts)
/*      */     throws IOException
/*      */   {
/* 1184 */     this.triggerDelay.setExternalTriggerDelay(counts);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public double triggerDelayCountsToMicroseconds(int counts)
/*      */   {
/* 1191 */     return this.triggerDelay.triggerDelayCountsToMicroseconds(counts);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public Integer getExternalTriggerDelay()
/*      */   {
/* 1198 */     return this.triggerDelay.getExternalTriggerDelay();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getExternalTriggerDelayMinimum()
/*      */   {
/* 1205 */     return this.triggerDelay.getExternalTriggerDelayMinimum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getExternalTriggerDelayMaximum()
/*      */   {
/* 1212 */     return this.triggerDelay.getExternalTriggerDelayMaximum();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public int getExternalTriggerDelayIncrement()
/*      */   {
/* 1219 */     return this.triggerDelay.getExternalTriggerDelayIncrement();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Coefficients[] readNonlinearityCoefficientsFromSpectrometer()
/*      */   {
/* 1228 */     return this.nonlinearity.readNonlinearityCoefficientsFromSpectrometer();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void writeNonlinearityCoefficientsToSpectrometer(Coefficients[] coefficients)
/*      */     throws IOException
/*      */   {
/* 1236 */     this.nonlinearity.writeNonlinearityCoefficientsToSpectrometer(coefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public Coefficients[] getNonlinearityCoefficients()
/*      */   {
/* 1243 */     return this.nonlinearity.getNonlinearityCoefficients();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void setNonlinearityCoefficients(Coefficients[] coefficients)
/*      */   {
/* 1250 */     this.nonlinearity.setNonlinearityCoefficients(coefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public double[] getNonlinearityCoefficientsSingleChannel(int index)
/*      */   {
/* 1257 */     return this.nonlinearity.getNonlinearityCoefficientsSingleChannel(index);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void setNonlinearityCoefficientsSingleChannel(double[] nl, int index)
/*      */   {
/* 1264 */     this.nonlinearity.setNonlinearityCoefficientsSingleChannel(nl, index);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Coefficients[] readWavelengthCalibrationCoefficientsFromSpectrometer()
/*      */   {
/* 1273 */     return this.wavelength.readWavelengthCalibrationCoefficientsFromSpectrometer();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void writeWavelengthCoefficientsToSpectrometer(Coefficients[] coefficients)
/*      */     throws IOException
/*      */   {
/* 1281 */     this.wavelength.writeWavelengthCoefficientsToSpectrometer(coefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public Coefficients[] getWavelengthCalibrationCoefficients()
/*      */   {
/* 1288 */     return this.wavelength.getWavelengthCalibrationCoefficients();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void setWavelengthCalibrationCoefficients(Coefficients[] coefficients)
/*      */   {
/* 1295 */     this.wavelength.setWavelengthCalibrationCoefficients(coefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public double[] getWavelengths(int index)
/*      */   {
/* 1302 */     return this.wavelength.getWavelengths(index);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void setWavelengths(double[] wl, int index)
/*      */   {
/* 1309 */     this.wavelength.setWavelengths(wl, index);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Coefficients[] readStrayLightCorrectionCoefficientFromSpectrometer()
/*      */   {
/* 1318 */     return this.straylight.readStrayLightCorrectionCoefficientFromSpectrometer();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void writeStrayLightCoefficientToSpectrometer(Coefficients[] coefficients)
/*      */     throws IOException
/*      */   {
/* 1326 */     this.straylight.writeStrayLightCoefficientToSpectrometer(coefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void setStrayLightCorrectionCoefficient(Coefficients[] coefficients)
/*      */   {
/* 1333 */     this.straylight.setStrayLightCorrectionCoefficient(coefficients);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public Coefficients[] getStrayLightCorrectionCoefficient()
/*      */   {
/* 1340 */     return this.straylight.getStrayLightCorrectionCoefficient();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void setStrayLight(double strayLight, int index)
/*      */   {
/* 1347 */     this.straylight.setStrayLight(strayLight, index);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public double getStrayLight(int index)
/*      */   {
/* 1354 */     return this.straylight.getStrayLight(index);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getDarkValue()
/*      */   {
/* 1364 */     return this.autonulling.getDarkValue();
/*      */   }
/*      */   
/*      */   public void setDarkValue(int value) {
/* 1368 */     this.autonulling.setDarkValue(value);
/*      */   }
/*      */   
/*      */   public double getSaturationValue() {
/* 1372 */     return this.autonulling.getSaturationValue();
/*      */   }
/*      */   
/*      */   public void setSaturationValue(double value) {
/* 1376 */     this.autonulling.setSaturationValue(value);
/*      */   }
/*      */   
/*      */   public void recordSettings() {
/* 1380 */     this.autonulling.recordSettings();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isAdvancedVersion()
/*      */   {
/* 1389 */     return this instanceof AdvancedVersion;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void uploadFirmware(File file, long fileSize)
/*      */     throws IOException
/*      */   {
/* 1397 */     this.version.uploadFirmware(file, fileSize);
/*      */   }
/*      */   
/*      */ 
/*      */   public void uploadFPGA(File file, long fileSize)
/*      */     throws IOException
/*      */   {
/* 1404 */     this.version.uploadFPGA(file, fileSize);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void addAcquisitionListener(AcquisitionListener listener)
/*      */   {
/* 1411 */     this.version.addAcquisitionListener(listener);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void removeAcquisitionListener(AcquisitionListener listener)
/*      */   {
/* 1418 */     this.version.removeAcquisitionListener(listener);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1549 */   private static String __extern__ = "__extern__\n<init>,()V\n<init>,(I)V\nsetEndpoints,()V\ngetEndpoint,(I)Lcom/oceanoptics/uniusb/USBEndpointDescriptor;\nallowWriteToEEPROM,(II)Z\nopenSpectrometer,(I)V\ngetGUIFeatures,()[Lcom/oceanoptics/omnidriver/interfaces/GUIProvider;\nreadSpectrum,([B)V\nreadSpectrum,()V\ngetStatus,()Lcom/oceanoptics/omnidriver/spectrometer/SpectrometerStatus;\nreadIntegrationTime,()I\nsetIntegrationTime,(I)V\ngetGatingModeIntegrationTime,()I\naddOmniDriverDispatchListener,(Lcom/oceanoptics/omnidriver/interfaces/OmniDriverDispatchListener;)V\nremoveOmniDriverDispatchListener,(Lcom/oceanoptics/omnidriver/interfaces/OmniDriverDispatchListener;)V\nsetStrobeDelay,(I)V\ntoString,()Ljava/lang/String;\ngetPlugIns,()[Lcom/oceanoptics/omnidriver/plugin/SpectrometerPlugIn;\ngetNumberOfPlugIns,()I\nisPlugInDetected,(I)Z\ninitializePlugIns,()[B\ndetectPlugIns,()V\nsetI2CBytes,(BB[B)I\ngetI2CBytes,(BB)[B\ngetSPIBytes,([BI)[B\ngetPSOCVersion,()Ljava/lang/String;\ngetFPGAFirmwareVersion,()Ljava/lang/String;\ncontinuousStrobeCountsToMicros,(I)D\nsetContinuousStrobeDelay,(I)V\ngetContinuousStrobeDelay,()Ljava/lang/Integer;\ngetContinuousStrobeDelayMinimum,()I\ngetContinuousStrobeDelayMaximum,()I\ngetContinuousStrobeDelayIncrement,(I)I\nsetContinuousModeType,(Z)V\nsetDelayAfterIntegration,(I)V\ngetContinuousModeType,()Z\ngetDelayAfterIntegration,()I\nsetContinuousEnable,(Z)V\ngetContinuousEnable,()Z\nsetAdvancedIntegrationTime,(J)V\ngetIntegrationTimeBaseClock,()I\ngetIntegrationClockTimer,()I\ngetAdvancedIntegrationTimeMinimum,()J\ngetAdvancedIntegrationTimeMaximum,()J\ngetAdvancedIntegrationTimeIncrement,()J\nsetExternalTriggerMode,(I)V\ngetExternalTriggerModes,()[Lcom/oceanoptics/omnidriver/constants/ExternalTriggerMode;\ngetExternalTriggerMode,()Ljava/lang/Integer;\nsetShutterClock,(I)V\ngetShutterClock,()I\nsetSingleStrobeLow,(I)V\nsetSingleStrobeHigh,(I)V\ngetSingleStrobeCountsToMicros,(I)D\ngetSingleStrobeLow,()I\ngetSingleStrobeHigh,()I\ngetSingleStrobeMinimum,()I\ngetSingleStrobeMaximum,()I\ngetSingleStrobeIncrement,()I\nsetDirectionAllBits,(Ljava/util/BitSet;)V\nsetMuxAllBits,(Ljava/util/BitSet;)V\nsetValueAllBits,(Ljava/util/BitSet;)V\nsetDirectionBitmask,(S)V\nsetMuxBitmask,(S)V\nsetValueBitmask,(S)V\nsetDirectionBit,(IZ)V\nsetMuxBit,(IZ)V\nsetValueBit,(IZ)V\ngetTotalGPIOBits,()I\ngetDirectionBits,()Ljava/util/BitSet;\ngetMuxBits,()Ljava/util/BitSet;\ngetValueBit,(I)I\ngetValueBits,()Ljava/util/BitSet;\ngetNumberOfPins,()I\ngetBoardTemperatureCelsius,()D\ngetIrradianceCalibrationFactors,()[D\nsetIrradianceCalibrationFactors,([D)V\ngetCollectionArea,()D\nhasCollectionArea,()Z\nsetCollectionArea,(D)V\nsetMasterClockDivisor,(I)V\ngetMasterClockDivisor,()I\nsetExternalTriggerDelay,(I)V\ntriggerDelayCountsToMicroseconds,(I)D\ngetExternalTriggerDelay,()Ljava/lang/Integer;\ngetExternalTriggerDelayMinimum,()I\ngetExternalTriggerDelayMaximum,()I\ngetExternalTriggerDelayIncrement,()I\nreadNonlinearityCoefficientsFromSpectrometer,()[Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\nwriteNonlinearityCoefficientsToSpectrometer,([Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;)V\ngetNonlinearityCoefficients,()[Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\nsetNonlinearityCoefficients,([Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;)V\ngetNonlinearityCoefficientsSingleChannel,(I)[D\nsetNonlinearityCoefficientsSingleChannel,([DI)V\nreadWavelengthCalibrationCoefficientsFromSpectrometer,()[Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\nwriteWavelengthCoefficientsToSpectrometer,([Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;)V\ngetWavelengthCalibrationCoefficients,()[Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\nsetWavelengthCalibrationCoefficients,([Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;)V\ngetWavelengths,(I)[D\nsetWavelengths,([DI)V\nreadStrayLightCorrectionCoefficientFromSpectrometer,()[Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\nwriteStrayLightCoefficientToSpectrometer,([Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;)V\nsetStrayLightCorrectionCoefficient,([Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;)V\ngetStrayLightCorrectionCoefficient,()[Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\nsetStrayLight,(DI)V\ngetStrayLight,(I)D\ngetDarkValue,()I\nsetDarkValue,(I)V\ngetSaturationValue,()D\nsetSaturationValue,(D)V\nrecordSettings,()V\nisAdvancedVersion,()Z\nuploadFirmware,(Ljava/io/File;J)V\nuploadFPGA,(Ljava/io/File;J)V\naddAcquisitionListener,(Lcom/oceanoptics/omnidriver/interfaces/AcquisitionListener;)V\nremoveAcquisitionListener,(Lcom/oceanoptics/omnidriver/interfaces/AcquisitionListener;)V\n";
/*      */ }


/* Location:              /opt/omniDriver/OOI_HOME/OmniDriver.jar!/com/oceanoptics/omnidriver/spectrometer/usb4000/USB4000.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */