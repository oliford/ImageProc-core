/*     */ package com.oceanoptics.omnidriver.spectrometer;
/*     */ 
/*     */ import com.oceanoptics.omnidriver.interfaces.BaseSpecFunctionality;
/*     */ import com.oceanoptics.omnidriver.interfaces.FeatureProvider;
/*     */ import com.oceanoptics.omnidriver.interfaces.SpectrumProducer;
/*     */ import com.oceanoptics.omnidriver.spectra.SpectrometerInfo;
/*     */ import com.oceanoptics.omnidriver.spectra.Spectrum;
/*     */ import com.oceanoptics.utilities.FileVersion;
/*     */ import java.io.IOException;
/*     */ import java.util.LinkedList;
/*     */ import java.util.logging.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public abstract class Spectrometer
/*     */   extends SpectrumProducerBase
/*     */   implements SpectrumProducer, BaseSpecFunctionality, FeatureProvider
/*     */ {
/*  39 */   protected int integrationTimeMinimum = 0;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  44 */   protected int integrationTimeMaximum = 0;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  49 */   protected int integrationTimeIncrement = 0;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  54 */   protected int integrationTimeBase = 0;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  59 */   protected int integrationTime = -1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  64 */   protected Boolean strobeOn = Boolean.FALSE;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  70 */   protected boolean stabilityScan = true;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  76 */   protected SpectrometerInfo spectrumBase = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  84 */   protected int numberOfCCDPixels = -1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected int numberOfDarkCCDPixels;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected byte[] rawData;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected Spectrum tempSpectrum;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected Spectrum tempRawSpectrum;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected int maxIntensity;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected int pipeSize;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/* 120 */   protected int benchSlot = -1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/* 125 */   protected int spectrometerConfigSlot = -1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 131 */   protected int detectorSerialNumberSlot = -1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 137 */   protected int cpldVersionSlot = -1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected String firmwareVersion;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected int firmwareVersionNumber;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/* 153 */   protected String firmwareVersionString = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 159 */   protected Logger logger = Logger.getLogger(getClass().getName());
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected Configuration configuration;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public SpectrometerChannel[] channels;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/* 175 */   protected int numChannels = 1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/* 180 */   protected int enabledChannels = 1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 186 */   protected int channelIndex = 0;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 193 */   protected boolean strobeDelayEnabled = false;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 205 */   protected int[] channelIndices = new int[this.numChannels];
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean rotatorEnabled;
/*     */   
/*     */ 
/*     */ 
/* 214 */   protected LinkedList formattingQueue = new LinkedList();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   FormattingThread formattingThread;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public abstract void openSpectrometer(int paramInt)
/*     */     throws IOException;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Spectrometer()
/*     */     throws IOException
/*     */   {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getSpectrumReadThrottleMilliseconds()
/*     */   {
/* 286 */     return 0;
/*     */   }
/*     */   
/*     */   public void setSpectrumReadThrottleMilliseconds(int value) {}
/*     */   
/* 291 */   public int getSocketTimeoutMilliseconds() { return 0; }
/*     */   
/*     */   public void setSocketTimeoutMilliseconds(int value) {}
/*     */   
/*     */   public boolean isCheckForBytesAvailableEnabled() {
/* 296 */     return false;
/*     */   }
/*     */   
/*     */   public void setCheckForBytesAvailable(boolean value) {}
/*     */   
/* 301 */   public int getReadSpectrumRetryLimit() { return 0; }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReadSpectrumRetryLimit(int value) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean allowWriteToEEPROM(int privilegeLevel, int slot)
/*     */   {
/* 326 */     if (slot < 0) {
/* 327 */       return false;
/*     */     }
/* 329 */     if (privilegeLevel == 0)
/* 330 */       return true;
/* 331 */     if (privilegeLevel == 1)
/*     */     {
/* 333 */       if (slot == 0)
/* 334 */         return false;
/* 335 */       if (slot > 14)
/* 336 */         return false;
/* 337 */       return true;
/*     */     }
/* 339 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void finishConstruction()
/*     */     throws IOException
/*     */   {
/* 352 */     if (this.numberOfPixels < 0) {
/* 353 */       this.numberOfPixels = this.numberOfCCDPixels;
/* 354 */       this.numberOfDarkPixels = this.numberOfDarkCCDPixels;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SpectrometerInfo getSpectrumBase()
/*     */   {
/* 365 */     return this.spectrumBase;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStabilityScan(boolean on)
/*     */   {
/* 374 */     this.stabilityScan = on;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isStabilityScan()
/*     */   {
/* 383 */     return this.stabilityScan;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getIntegrationTimeMinimum()
/*     */   {
/* 391 */     return this.integrationTimeMinimum;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getIntegrationTimeMaximum()
/*     */   {
/* 399 */     return this.integrationTimeMaximum;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getIntegrationTimeIncrement()
/*     */   {
/* 407 */     return this.integrationTimeIncrement;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getIntegrationTimeBase()
/*     */   {
/* 415 */     return this.integrationTimeBase;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getActualIntegrationTime()
/*     */   {
/* 426 */     return this.integrationTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SpectrometerChannel[] getChannels()
/*     */   {
/* 439 */     this.logger.fine("Channels: " + this.channels);
/* 440 */     return this.channels;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumberOfChannels()
/*     */   {
/* 450 */     this.logger.fine("Number of Channels: " + this.numChannels);
/* 451 */     return this.numChannels;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getNumberOfEnabledChannels()
/*     */   {
/* 461 */     this.logger.fine("Number of enabled Channels: " + this.enabledChannels);
/* 462 */     return this.enabledChannels;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int[] getChannelIndices()
/*     */   {
/* 477 */     if (1 == this.numChannels) {
/* 478 */       this.channelIndices[0] = 0;
/*     */     }
/* 480 */     return this.channelIndices;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isNetworkSpectrometer()
/*     */   {
/* 490 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isRotatorEnabled()
/*     */   {
/* 501 */     return this.rotatorEnabled;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRotatorEnabled(boolean rotator)
/*     */   {
/* 510 */     this.rotatorEnabled = rotator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getBenchSlot()
/*     */   {
/* 522 */     this.logger.fine("Bench slot: " + this.benchSlot);
/* 523 */     return this.benchSlot;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getSpectrometerConfigSlot()
/*     */   {
/* 532 */     this.logger.fine("Spectrometer configuration slot: " + this.spectrometerConfigSlot);
/*     */     
/* 534 */     return this.spectrometerConfigSlot;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getDetectorSerialNumberSlot()
/*     */   {
/* 543 */     this.logger.fine("Detector serial number: " + this.detectorSerialNumberSlot);
/* 544 */     return this.detectorSerialNumberSlot;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getCPLDVersionSlot()
/*     */   {
/* 553 */     this.logger.fine("CPLD version slot: " + this.cpldVersionSlot);
/*     */     
/* 555 */     return this.cpldVersionSlot;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Configuration getConfiguration()
/*     */     throws IOException
/*     */   {
/* 581 */     return this.configuration;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Configuration getConfiguration(int channelIndex)
/*     */   {
/* 592 */     return this.configuration;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void setConfiguration(Configuration configuration)
/*     */   {
/* 602 */     this.configuration = configuration;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void getConfigurationFromSpectrometer()
/*     */     throws IOException
/*     */   {
/* 612 */     this.configuration.getConfigurationFromSpectrometer();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setConfiguration()
/*     */     throws IOException
/*     */   {
/* 622 */     this.configuration.setConfigurationToSpectrometer();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Coefficients getNewCoefficients(int index)
/*     */   {
/* 632 */     return new Coefficients();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void getCoefficientsFromSpectrometer()
/*     */     throws IOException
/*     */   {
/* 641 */     for (int i = 0; i < this.channels.length; i++) {
/* 642 */       if (null != this.channels[i])
/*     */       {
/*     */ 
/* 645 */         this.channels[i].getCoefficientsFromSpectrometer();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCoefficients()
/*     */     throws IOException
/*     */   {
/* 656 */     for (int i = 0; i < this.channels.length; i++) {
/* 657 */       this.channels[i].setCoefficients();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getFirmwareVersionNumber()
/*     */   {
/* 666 */     this.logger.finer("Firmware version number: " + this.firmwareVersionNumber);
/* 667 */     return this.firmwareVersionNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private String getFirmwareVersionString()
/*     */   {
/* 675 */     this.logger.finer("Firmware version string: " + this.firmwareVersionString);
/* 676 */     return this.firmwareVersionString;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void parseFirmwareVersion(String fw)
/*     */   {
/* 686 */     this.logger.finest("Parsing firmware...");
/* 687 */     int v1 = 0;int v2 = 0;int v3 = 0;
/* 688 */     String v4 = null;
/* 689 */     String temp = fw.substring(this.firmwareVersion.indexOf(" ") + 1).trim();
/* 690 */     String[] strArr = temp.split("[.]", 3);
/* 691 */     if (strArr.length == 3) {
/*     */       try {
/* 693 */         v1 = Integer.parseInt(strArr[0]);
/* 694 */         v2 = Integer.parseInt(strArr[1]);
/*     */       }
/*     */       catch (NumberFormatException localNumberFormatException1) {}
/*     */       
/* 698 */       for (int i = 0; i < strArr[2].length(); i++) {
/*     */         try {
/* 700 */           v3 += Integer.parseInt(strArr[2].substring(i, i + 1));
/*     */         } catch (NumberFormatException ne) {
/* 702 */           v4 = v4 + strArr[2].substring(i, i + 1);
/*     */         }
/*     */       }
/*     */     }
/* 706 */     this.firmwareVersionNumber = makeVersionNumber(v1, v2, v3);
/* 707 */     if (this.firmwareVersionString != null) {
/* 708 */       this.firmwareVersionString = makeVersionString(v1, v2, v3, v4);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected int makeVersionNumber(int v1, int v2, int v3)
/*     */   {
/* 724 */     this.logger.finest("Making version number...");
/* 725 */     return v1 * 1000000 + v2 * 1000 + v3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected String makeVersionString(int v1, int v2, int v3, String v4)
/*     */   {
/* 740 */     this.logger.finest("Making version string.");
/* 741 */     String version = String.valueOf(v1 * 1000000 + v2 * 1000 + v3) + v4;
/* 742 */     return version;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCodeVersion(String fileName)
/*     */   {
/* 751 */     this.logger.finer("Code version: " + FileVersion.getFileVersion(fileName));
/* 752 */     return FileVersion.getFileVersion(fileName);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getMaxIntensity()
/*     */   {
/* 760 */     return this.maxIntensity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isStrobeDelayEnabled()
/*     */   {
/* 768 */     return this.strobeDelayEnabled;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void close()
/*     */     throws IOException
/*     */   {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected abstract Spectrum formatData(byte[] paramArrayOfByte, Spectrum paramSpectrum)
/*     */     throws IOException;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected class FormattingThread
/*     */     extends Thread
/*     */   {
/*     */     public volatile boolean quit;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public FormattingThread(String name)
/*     */     {
/* 813 */       super();
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     public void run()
/*     */     {
/* 821 */       RawData rawData = null;
/* 822 */       Spectrum spectrum = null;
/*     */       
/*     */ 
/* 825 */       while (!this.quit) {
/*     */         label64:
/* 827 */         synchronized (Spectrometer.this.formattingQueue) {
/* 828 */           for (;;) { if (Spectrometer.this.formattingQueue.isEmpty()) {
/*     */               try
/*     */               {
/* 831 */                 Spectrometer.this.formattingQueue.wait();
/*     */                 
/* 833 */                 if (this.quit == true) {
/*     */                   break label64;
/*     */                 }
/*     */               }
/*     */               catch (InterruptedException e) {}
/*     */             }
/*     */           }
/*     */         }
/*     */         
/* 842 */         if (!this.quit)
/*     */         {
/* 844 */           rawData = (RawData)Spectrometer.this.formattingQueue.removeFirst();
/* 845 */           if (rawData != null)
/*     */           {
/*     */ 
/* 848 */             spectrum = Spectrometer.this.getUnfilledSpectrum();
/*     */             try
/*     */             {
/* 851 */               Spectrometer.this.formatData(rawData.getData(), spectrum);
/*     */             } catch (IOException e) {
/* 853 */               Spectrometer.this.logger.fine("Lost sync");
/*     */             }
/*     */             
/*     */ 
/* 857 */             int channel = rawData.getRequestingChannel();
/*     */             
/*     */ 
/* 860 */             Spectrometer.this.fireSpectrumToChannel(channel, spectrum);
/* 861 */             rawData = null;
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 866 */       Spectrometer.this.logger.fine("Formatting thread stopped.");
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     public void quit()
/*     */     {
/* 874 */       this.quit = true;
/*     */       try
/*     */       {
/* 877 */         interrupt();
/* 878 */         Spectrometer.this.formattingQueue.notifyAll();
/* 879 */         interrupt();
/*     */       }
/*     */       catch (Exception localException) {}
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void fireSpectrumToChannel(int channel, Spectrum spectrum)
/*     */   {
/* 897 */     this.channels[channel].newSpectrum(spectrum);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 914 */   private static String __extern__ = "__extern__\nopenSpectrometer,(I)V\n<init>,()V\ngetSpectrumReadThrottleMilliseconds,()I\nsetSpectrumReadThrottleMilliseconds,(I)V\ngetSocketTimeoutMilliseconds,()I\nsetSocketTimeoutMilliseconds,(I)V\nisCheckForBytesAvailableEnabled,()Z\nsetCheckForBytesAvailable,(Z)V\ngetReadSpectrumRetryLimit,()I\nsetReadSpectrumRetryLimit,(I)V\nallowWriteToEEPROM,(II)Z\ngetSpectrumBase,()Lcom/oceanoptics/omnidriver/spectra/SpectrometerInfo;\nsetStabilityScan,(Z)V\nisStabilityScan,()Z\ngetIntegrationTimeMinimum,()I\ngetIntegrationTimeMaximum,()I\ngetIntegrationTimeIncrement,()I\ngetIntegrationTimeBase,()I\ngetActualIntegrationTime,()I\ngetChannels,()[Lcom/oceanoptics/omnidriver/spectrometer/SpectrometerChannel;\ngetNumberOfChannels,()I\ngetNumberOfEnabledChannels,()I\ngetChannelIndices,()[I\nisNetworkSpectrometer,()Z\nisRotatorEnabled,()Z\nsetRotatorEnabled,(Z)V\ngetBenchSlot,()I\ngetSpectrometerConfigSlot,()I\ngetDetectorSerialNumberSlot,()I\ngetCPLDVersionSlot,()I\ngetConfiguration,()Lcom/oceanoptics/omnidriver/spectrometer/Configuration;\ngetConfiguration,(I)Lcom/oceanoptics/omnidriver/spectrometer/Configuration;\ngetConfigurationFromSpectrometer,()V\nsetConfiguration,()V\ngetNewCoefficients,(I)Lcom/oceanoptics/omnidriver/spectrometer/Coefficients;\ngetCoefficientsFromSpectrometer,()V\nsetCoefficients,()V\ngetFirmwareVersionNumber,()I\ngetCodeVersion,(Ljava/lang/String;)Ljava/lang/String;\ngetMaxIntensity,()I\nisStrobeDelayEnabled,()Z\nclose,()V\n";
/*     */ }


/* Location:              /opt/omniDriver/OOI_HOME/OmniDriver.jar!/com/oceanoptics/omnidriver/spectrometer/Spectrometer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */