/*     */ package com.oceanoptics.uniusb;
/*     */ 
/*     */ import com.oceanoptics.utilities.OSInformation;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UniUSB
/*     */   implements USBDriver
/*     */ {
/*     */   private boolean USB2Mode;
/*     */   
/*     */   static
/*     */   {
/*  52 */     String[] suffixes = OSInformation.getNativeSuffixOrder();
/*  53 */     String basename = "NatUSB";
/*     */     
/*  55 */     boolean success = false;
/*  56 */     for (int i = 0; (i < suffixes.length) && (false == success); i++) {
/*     */       try {
/*  58 */         System.loadLibrary(basename + suffixes[i]);
/*  59 */         System.out.println(basename + suffixes[i]);
/*  60 */         success = true;
/*     */       }
/*     */       catch (Throwable localThrowable) {}
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  67 */     if (false == success) {
/*  68 */       String attempts = new String();
/*  69 */       for (int i = 0; i < suffixes.length; i++) {
/*  70 */         attempts = attempts + " " + basename + suffixes[i];
/*  71 */         if (i + 1 < suffixes.length) {
/*  72 */           attempts = attempts + ",";
/*     */         }
/*     */       }
/*  75 */       throw new RuntimeException("Failed to load any native library for USB.  Tried base filenames:" + attempts);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isUSB2Mode()
/*     */   {
/*  87 */     return this.USB2Mode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public byte USBBuildEndpointAddress(int address, int direction)
/*     */   {
/*  98 */     return (byte)(address | direction << 7);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long openDevice(int vendID, int prodID, int number)
/*     */     throws IOException
/*     */   {
/* 117 */     long devID = NatUSBOpenDevice(vendID, prodID, number);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 128 */     EndpointToPipeMapping mapping = new EndpointToPipeMapping();
/* 129 */     USBInterfaceDescriptor interfaceDescriptor = new USBInterfaceDescriptor();
/* 130 */     NatUSBGetInterfaceDescriptor(devID, interfaceDescriptor);
/* 131 */     for (int i = 0; i < interfaceDescriptor.getNumEndpoints(); i++)
/*     */     {
/* 133 */       USBEndpointDescriptor endpointDescriptor = new USBEndpointDescriptor();
/* 134 */       NatUSBGetEndpointDescriptor(devID, i, endpointDescriptor);
/* 135 */       if (endpointDescriptor.getMaxPacketSize() > 64)
/*     */       {
/* 137 */         this.USB2Mode = true;
/*     */       }
/* 139 */       mapping.addMapping(i, endpointDescriptor.getEndpointAddress());
/*     */     }
/* 141 */     UniUSBPipeManager.addDeviceMapping(devID, mapping);
/*     */     
/* 143 */     return devID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void closeDevice(long devID)
/*     */   {
/* 153 */     NatUSBCloseDevice(devID);
/* 154 */     UniUSBPipeManager.removeDeviceMapping(devID);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int setTimeout(long devID, USBEndpointDescriptor endPointDescriptor, int timeoutMilliseconds)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 175 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 176 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 177 */       return NatUSBSetTimeout(devID, pipe, endpoint, timeoutMilliseconds);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 184 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int bulkIn(long devID, USBEndpointDescriptor endPointDescriptor, byte[] data, int size)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 203 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 204 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 205 */       return NatUSBBulkIn(devID, pipe, endpoint, data, size);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 212 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int bulkOut(long devID, USBEndpointDescriptor endPointDescriptor, byte[] data, int size)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 228 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 229 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 230 */       return NatUSBBulkOut(devID, pipe, endpoint, data, size);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 237 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void resetPipe(long devID, USBEndpointDescriptor endPointDescriptor)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 252 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 253 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 254 */       NatUSBResetPipe(devID, pipe, endpoint);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void abortPipe(long devID, USBEndpointDescriptor endPointDescriptor)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 275 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 276 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 277 */       NatUSBAbortPipe(devID, pipe, endpoint);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public USBConfigurationDescriptor getUSBConfigurationDescriptor(long devID, int configNum)
/*     */     throws IOException
/*     */   {
/* 298 */     USBConfigurationDescriptor cd = new USBConfigurationDescriptor();
/*     */     
/* 300 */     NatUSBGetConfigurationDescriptor(devID, configNum, cd);
/*     */     
/* 302 */     return cd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public USBDeviceDescriptor getUSBDeviceDescriptor(long devID)
/*     */     throws IOException
/*     */   {
/* 316 */     USBDeviceDescriptor dd = new USBDeviceDescriptor();
/*     */     
/* 318 */     NatUSBGetDeviceDescriptor(devID, dd);
/*     */     
/* 320 */     return dd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public USBInterfaceDescriptor getUSBInterfaceDescriptor(long devID)
/*     */     throws IOException
/*     */   {
/* 333 */     USBInterfaceDescriptor id = new USBInterfaceDescriptor();
/* 334 */     NatUSBGetInterfaceDescriptor(devID, id);
/* 335 */     return id;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getUSBStringDescriptor(long devID, int index)
/*     */     throws IOException
/*     */   {
/* 350 */     return NatUSBGetStringDescriptor(devID, index);
/*     */   }
/*     */   
/*     */   native int NatUSBSetTimeout(long paramLong, int paramInt1, int paramInt2, int paramInt3)
/*     */     throws IOException;
/*     */   
/*     */   native long NatUSBOpenDevice(int paramInt1, int paramInt2, int paramInt3);
/*     */   
/*     */   native void NatUSBCloseDevice(long paramLong);
/*     */   
/*     */   native int NatUSBBulkIn(long paramLong, int paramInt1, int paramInt2, byte[] paramArrayOfByte, int paramInt3)
/*     */     throws IOException;
/*     */   
/*     */   native int NatUSBBulkOut(long paramLong, int paramInt1, int paramInt2, byte[] paramArrayOfByte, int paramInt3)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBResetPipe(long paramLong, int paramInt1, int paramInt2)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBAbortPipe(long paramLong, int paramInt1, int paramInt2)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetConfigurationDescriptor(long paramLong, int paramInt, USBConfigurationDescriptor paramUSBConfigurationDescriptor)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetDeviceDescriptor(long paramLong, USBDeviceDescriptor paramUSBDeviceDescriptor)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetInterfaceDescriptor(long paramLong, USBInterfaceDescriptor paramUSBInterfaceDescriptor)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetEndpointDescriptor(long paramLong, int paramInt, USBEndpointDescriptor paramUSBEndpointDescriptor);
/*     */   
/*     */   native String NatUSBGetStringDescriptor(long paramLong, int paramInt)
/*     */     throws IOException;
/*     */   
/*     */   native int NatUSBGetPipe(byte paramByte);
/*     */ }


/* Location:              /opt/omniDriver/OOI_HOME/UniUSB.jar!/com/oceanoptics/uniusb/UniUSB.class
 * Java compiler version: 4 (48.0)
 * JD-Core Version:       0.7.1
 */