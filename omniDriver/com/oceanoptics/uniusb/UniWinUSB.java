/*     */ package com.oceanoptics.uniusb;
/*     */ 
/*     */ import com.oceanoptics.utilities.OSInformation;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UniWinUSB
/*     */   implements USBDriver
/*     */ {
/*     */   private boolean USB2Mode;
/*     */   
/*     */   static
/*     */   {
/*  49 */     boolean success = false;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  59 */     String[] suffixes = OSInformation.getNativeSuffixOrder();
/*  60 */     String basename = "NatUSBWin";
/*  61 */     for (int i = 0; (i < suffixes.length) && (false == success); i++) {
/*     */       try
/*     */       {
/*  64 */         System.loadLibrary(basename + suffixes[i]);
/*  65 */         System.out.println("Driver: " + basename + suffixes[i]);
/*  66 */         success = true;
/*     */       }
/*     */       catch (Throwable localThrowable) {}
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  73 */     if (false == success) {
/*  74 */       String attempts = new String();
/*  75 */       for (int i = 0; i < suffixes.length; i++) {
/*  76 */         attempts = attempts + " " + basename + suffixes[i];
/*  77 */         if (i + 1 < suffixes.length) {
/*  78 */           attempts = attempts + ",";
/*     */         }
/*     */       }
/*  81 */       throw new RuntimeException("Failed to load any native library for USB.  Tried base filenames:" + attempts);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isUSB2Mode()
/*     */   {
/*  93 */     return this.USB2Mode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public byte USBBuildEndpointAddress(int address, int direction)
/*     */   {
/* 104 */     return (byte)(address | direction << 7);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long openDevice(int vendID, int prodID, int number)
/*     */     throws IOException
/*     */   {
/* 123 */     long devID = NatUSBOpenDevice(vendID, prodID, number);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 134 */     EndpointToPipeMapping mapping = new EndpointToPipeMapping();
/* 135 */     USBInterfaceDescriptor interfaceDescriptor = new USBInterfaceDescriptor();
/* 136 */     NatUSBGetInterfaceDescriptor(devID, interfaceDescriptor);
/* 137 */     for (int i = 0; i < interfaceDescriptor.getNumEndpoints(); i++)
/*     */     {
/* 139 */       USBEndpointDescriptor endpointDescriptor = new USBEndpointDescriptor();
/* 140 */       NatUSBGetEndpointDescriptor(devID, i, endpointDescriptor);
/* 141 */       if (endpointDescriptor.getMaxPacketSize() > 64)
/*     */       {
/* 143 */         this.USB2Mode = true;
/*     */       }
/* 145 */       mapping.addMapping(i, endpointDescriptor.getEndpointAddress());
/*     */     }
/* 147 */     UniUSBPipeManager.addDeviceMapping(devID, mapping);
/*     */     
/* 149 */     return devID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void closeDevice(long devID)
/*     */   {
/* 159 */     NatUSBCloseDevice(devID);
/* 160 */     UniUSBPipeManager.removeDeviceMapping(devID);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int setTimeout(long devID, USBEndpointDescriptor endPointDescriptor, int timeoutMilliseconds)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 181 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 182 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 183 */       return NatUSBSetTimeout(devID, pipe, endpoint, timeoutMilliseconds);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 190 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int bulkIn(long devID, USBEndpointDescriptor endPointDescriptor, byte[] data, int size)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 209 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 210 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 211 */       return NatUSBBulkIn(devID, pipe, endpoint, data, size);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 218 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int bulkOut(long devID, USBEndpointDescriptor endPointDescriptor, byte[] data, int size)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 234 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 235 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 236 */       return NatUSBBulkOut(devID, pipe, endpoint, data, size);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 243 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void resetPipe(long devID, USBEndpointDescriptor endPointDescriptor)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 258 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 259 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 260 */       NatUSBResetPipe(devID, pipe, endpoint);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void abortPipe(long devID, USBEndpointDescriptor endPointDescriptor)
/*     */     throws IOException
/*     */   {
/*     */     try
/*     */     {
/* 281 */       short endpoint = endPointDescriptor.getEndpointAddress();
/* 282 */       int pipe = UniUSBPipeManager.getPipe(devID, endpoint);
/* 283 */       NatUSBAbortPipe(devID, pipe, endpoint);
/*     */     }
/*     */     catch (NullPointerException localNullPointerException) {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public USBConfigurationDescriptor getUSBConfigurationDescriptor(long devID, int configNum)
/*     */     throws IOException
/*     */   {
/* 304 */     USBConfigurationDescriptor cd = new USBConfigurationDescriptor();
/*     */     
/* 306 */     NatUSBGetConfigurationDescriptor(devID, configNum, cd);
/*     */     
/* 308 */     return cd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public USBDeviceDescriptor getUSBDeviceDescriptor(long devID)
/*     */     throws IOException
/*     */   {
/* 322 */     USBDeviceDescriptor dd = new USBDeviceDescriptor();
/*     */     
/* 324 */     NatUSBGetDeviceDescriptor(devID, dd);
/*     */     
/* 326 */     return dd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public USBInterfaceDescriptor getUSBInterfaceDescriptor(long devID)
/*     */     throws IOException
/*     */   {
/* 339 */     USBInterfaceDescriptor id = new USBInterfaceDescriptor();
/* 340 */     NatUSBGetInterfaceDescriptor(devID, id);
/* 341 */     return id;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getUSBStringDescriptor(long devID, int index)
/*     */     throws IOException
/*     */   {
/* 356 */     return NatUSBGetStringDescriptor(devID, index);
/*     */   }
/*     */   
/*     */   native int NatUSBSetTimeout(long paramLong, int paramInt1, int paramInt2, int paramInt3)
/*     */     throws IOException;
/*     */   
/*     */   native long NatUSBOpenDevice(int paramInt1, int paramInt2, int paramInt3);
/*     */   
/*     */   native void NatUSBCloseDevice(long paramLong);
/*     */   
/*     */   native int NatUSBBulkIn(long paramLong, int paramInt1, int paramInt2, byte[] paramArrayOfByte, int paramInt3)
/*     */     throws IOException;
/*     */   
/*     */   native int NatUSBBulkOut(long paramLong, int paramInt1, int paramInt2, byte[] paramArrayOfByte, int paramInt3)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBResetPipe(long paramLong, int paramInt1, int paramInt2)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBAbortPipe(long paramLong, int paramInt1, int paramInt2)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetConfigurationDescriptor(long paramLong, int paramInt, USBConfigurationDescriptor paramUSBConfigurationDescriptor)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetDeviceDescriptor(long paramLong, USBDeviceDescriptor paramUSBDeviceDescriptor)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetInterfaceDescriptor(long paramLong, USBInterfaceDescriptor paramUSBInterfaceDescriptor)
/*     */     throws IOException;
/*     */   
/*     */   native void NatUSBGetEndpointDescriptor(long paramLong, int paramInt, USBEndpointDescriptor paramUSBEndpointDescriptor);
/*     */   
/*     */   native String NatUSBGetStringDescriptor(long paramLong, int paramInt)
/*     */     throws IOException;
/*     */   
/*     */   native int NatUSBGetPipe(byte paramByte);
/*     */ }


/* Location:              /opt/omniDriver/OOI_HOME/UniUSB.jar!/com/oceanoptics/uniusb/UniWinUSB.class
 * Java compiler version: 4 (48.0)
 * JD-Core Version:       0.7.1
 */